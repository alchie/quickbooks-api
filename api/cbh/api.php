<?php

/**
 * An example of how to mirror parts (or all of) the QuickBooks database to a MySQL database
 * 
 * 
 * *REALLY FREAKING IMPORTANT* WARNING WARNING WARNING WARNING
 * 
 * THE SQL MIRROR CODE IS BETA CODE, AND IS KNOWN TO HAVE BUGS!
 * 
 * With that said:
 * - If you're planning on using it in a production environment, you better be ready to do a lot of testing and debugging.
 * - Use a nightly build. There are known problems with the mirror code in the v1.5.2 or v1.5.3 releases of the code.
 * - There is absolutely no way I can troubleshoot problems for you without you posting your code. Dumps of the quickbooks_queue and quickbooks_log tables and/or phpMyAdmin access to your MySQL database is helpful also.
 * 
 * Nightly builds are available here:
 * https://code.intuit.com/sf/frs/do/viewRelease/projects.php_devkit/frs.php_devkit.latest_sources
 * 
 * IN ALL LIKELIHOOD, YOU SHOULD *NOT* BE USING THIS CODE. YOU SHOULD INSTEAD 
 * LOOK AT THE FOLLOWING SCRIPTS AND IMPLEMENT YOUR REQUEST/RESPONSE HANDLERS 
 * YOURSELF:
 * 	docs/example_web_connector.php
 * 	docs/example_web_connector_import.php
 * 
 * *REALLY FREAKING IMPORTANT* WARNING WARNING WARNING WARNING
 *
 *  
 * The SQL mirror functionality makes it easy to extract information from 
 * QuickBooks into an SQL database, and, if so desired, write changes to the 
 * SQL records back to QuickBooks automatically. 
 * 
 * You should look at my wiki for more information about mirroring QuickBooks 
 * data into SQL databases:
 * 	http://wiki.consolibyte.com/wiki/doku.php/quickbooks_integration_php_consolibyte_sqlmirror
 * 
 * You should also read this forum post before even thinking about using this:
 * 	http://consolibyte.com/forum/viewtopic.php?id=20
 * 
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

// I always program in E_STRICT error mode with error reporting turned on... 
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 0);
ini_set('always_populate_raw_post_data', -1);

// Set the include path
require_once '../_api/QuickBooks.php';

// You should make sure this matches the time-zone QuickBooks is running in
if (function_exists('date_default_timezone_set'))
{
	date_default_timezone_set('America/New_York');
}

// The username and password the Web Connector will use to connect with
$username = 'Alchie';
$password = 'TAGU71186PLOK';

// Database connection string
//
// You *MUST* start with a fresh database! If the database you use has any 
//	quickbooks_* or qb_* related tables in it, then the schema *WILL NOT* build 
//	correctly! 
// 	
// 	Currently, only MySQL is supported/tested. 
$dsn = 'mysqli://root@localhost/trokisqb';

// If the database has not been initialized, we need to initialize it (create 
//	schema and set up the username/password, etc.)
if (!QuickBooks_Utilities::initialized($dsn))
{
	header('Content-Type: text/plain');
	
	// It takes a really long time to build the schema... 
	set_time_limit(0);
	
	$driver_options = array(
		);
		
	$init_options = array(
		'quickbooks_sql_enabled' => true, 
		);
		
	QuickBooks_Utilities::initialize($dsn, $driver_options, $init_options);
	QuickBooks_Utilities::createUser($dsn, $username, $password);
	
	exit;
}

// What mode do we want to run the mirror in? 
$mode = QuickBooks_WebConnector_Server_SQL::MODE_READONLY;		// Read from QuickBooks only (no data will be pushed back to QuickBooks)
//$mode = QuickBooks_WebConnector_Server_SQL::MODE_WRITEONLY;		// Write to QuickBooks only (no data will be copied into the SQL database)
//$mode = QuickBooks_WebConnector_Server_SQL::MODE_READWRITE;		// Keep both QuickBooks and the database in sync, reading and writing changes back and forth)

// What should we do if a conflict is found? (a record has been changed by another user or process that we're trying to update)
$conflicts = QuickBooks_WebConnector_Server_SQL::CONFLICT_LOG;

// What should we do with records deleted from QuickBooks? 
//$delete = QuickBooks_WebConnector_Server_SQL::DELETE_REMOVE;		// Delete the record from the database too
$delete = QuickBooks_WebConnector_Server_SQL::DELETE_FLAG; 		// Just flag it as deleted

// Hooks (optional stuff)
$hooks = array();

// 
$soap_options = array();

// 
$handler_options = array(
	'deny_concurrent_logins' => false,
	'deny_reallyfast_logins' => false, 
	);

// 
$driver_options = array();

$ops = array(
//QUICKBOOKS_OBJECT_HOST	,
//QUICKBOOKS_OBJECT_PREFERENCES	,
//QUICKBOOKS_OBJECT_ACCOUNT	,
//QUICKBOOKS_OBJECT_BILL	,
//QUICKBOOKS_OBJECT_BILLINGRATE	,
//QUICKBOOKS_OBJECT_BILLTOPAY	,
//QUICKBOOKS_OBJECT_BILLPAYMENTCHECK	,
//QUICKBOOKS_OBJECT_BILLPAYMENTCREDITCARD	,
//QUICKBOOKS_OBJECT_CHARGE	,
//QUICKBOOKS_OBJECT_CHECK	,
//QUICKBOOKS_OBJECT_CLASS	,
//QUICKBOOKS_OBJECT_COMPANY	,
//QUICKBOOKS_OBJECT_CREDITCARDCREDIT	,
//QUICKBOOKS_OBJECT_CREDITCARDREFUND	,
//QUICKBOOKS_OBJECT_CREDITCARDCHARGE	,
//QUICKBOOKS_OBJECT_CREDITCARDMEMO	,
//QUICKBOOKS_OBJECT_CREDITMEMO	,
//QUICKBOOKS_OBJECT_CURRENCY	,
//QUICKBOOKS_OBJECT_CUSTOMER	,
//QUICKBOOKS_OBJECT_CUSTOMERMSG	,
//QUICKBOOKS_OBJECT_CUSTOMERTYPE	,
//QUICKBOOKS_OBJECT_DATAEXT	,
//QUICKBOOKS_OBJECT_DATAEXTDEF	,
//QUICKBOOKS_OBJECT_DATEDRIVENTERMS	,
//QUICKBOOKS_OBJECT_DEPOSIT	,
//QUICKBOOKS_OBJECT_DEPARTMENT	,
QUICKBOOKS_OBJECT_EMPLOYEE	,
//QUICKBOOKS_OBJECT_ESTIMATE	,
//QUICKBOOKS_OBJECT_INVENTORYADJUSTMENT	,
//QUICKBOOKS_OBJECT_JOB	,
//QUICKBOOKS_OBJECT_ITEM	,
//QUICKBOOKS_OBJECT_INVENTORYITEM	,
//QUICKBOOKS_OBJECT_INVENTORYASSEMBLYITEM	,
//QUICKBOOKS_OBJECT_GROUPITEM	,
//QUICKBOOKS_OBJECT_NONINVENTORYITEM	,
//QUICKBOOKS_OBJECT_DISCOUNTITEM	,
//QUICKBOOKS_OBJECT_FIXEDASSETITEM	,
//QUICKBOOKS_OBJECT_PAYMENTITEM	,
//QUICKBOOKS_OBJECT_SERVICEITEM	,
//QUICKBOOKS_OBJECT_SALESTAXITEM	,
//QUICKBOOKS_OBJECT_SALESTAXGROUPITEM	,
//QUICKBOOKS_OBJECT_OTHERCHARGEITEM	,
//QUICKBOOKS_OBJECT_PAYROLLITEMWAGE	,
//QUICKBOOKS_OBJECT_PAYROLLITEMNONWAGE	,
//QUICKBOOKS_OBJECT_ITEMRECEIPT	,
//QUICKBOOKS_OBJECT_SUBTOTALITEM	,
//QUICKBOOKS_OBJECT_INVENTORYSITE	,
//QUICKBOOKS_OBJECT_JOBTYPE	,
//QUICKBOOKS_OBJECT_JOURNALENTRY	,
//QUICKBOOKS_OBJECT_INVOICE	,
//QUICKBOOKS_OBJECT_RECEIVEPAYMENT	,
QUICKBOOKS_OBJECT_OTHERNAME	,
//QUICKBOOKS_OBJECT_PAYMENTMETHOD	,
//QUICKBOOKS_OBJECT_PRICELEVEL	,
//QUICKBOOKS_OBJECT_PURCHASEORDER	,
//QUICKBOOKS_OBJECT_SALESORDER	,
//QUICKBOOKS_OBJECT_SALESRECEIPT	,
//QUICKBOOKS_OBJECT_SALESREP	,
//QUICKBOOKS_OBJECT_SALESTAXCODE	,
//QUICKBOOKS_OBJECT_SHIPMETHOD	,
//QUICKBOOKS_OBJECT_SPECIALACCOUNT	,
//QUICKBOOKS_OBJECT_SPECIALITEM	,
//QUICKBOOKS_OBJECT_STANDARDTERMS	,
//QUICKBOOKS_OBJECT_TEMPLATE	,
//QUICKBOOKS_OBJECT_TERMS	,
//QUICKBOOKS_OBJECT_TIMETRACKING	,
//QUICKBOOKS_OBJECT_TRANSACTION	,
//QUICKBOOKS_OBJECT_VEHICLE	,
//QUICKBOOKS_OBJECT_VEHICLEMILEAGE	,
QUICKBOOKS_OBJECT_VENDOR	,
//QUICKBOOKS_OBJECT_VENDORCREDIT	,
//QUICKBOOKS_OBJECT_VENDORTYPE	,
//QUICKBOOKS_OBJECT_WORKERSCOMPCODE	,
//QUICKBOOKS_OBJECT_UNITOFMEASURESET	
	);

$ops_misc = array(		// For fetching inventory levels, deleted transactions, etc. 
	//QUICKBOOKS_DERIVE_INVENTORYLEVELS,
	//QUICKBOOKS_QUERY_DELETEDLISTS,
	//QUICKBOOKS_QUERY_DELETEDTRANSACTIONS
	);

// 
$sql_options = array(
	'only_import' => $ops,
	'only_add' => $ops, 
	'only_modify' => $ops, 
	'only_misc' => $ops_misc, 
	);

// 
$callback_options = array();

// $dsn_or_conn, $how_often, $mode, $conflicts, $users = null, 
//	$map = array(), $onerror = array(), $hooks = array(), $log_level, $soap = QUICKBOOKS_SOAPSERVER_BUILTIN, $wsdl = QUICKBOOKS_WSDL, $soap_options = array(), $handler_options = array(), $driver_options = array()
$Server = new QuickBooks_WebConnector_Server_SQL(
	$dsn, 
	'1 minute', 
	$mode, 
	$conflicts, 
	$delete,
	$username, 
	array(), 
	array(), 
	$hooks, 
	QUICKBOOKS_LOG_DEVELOP, 
	QUICKBOOKS_SOAPSERVER_BUILTIN, 
	QUICKBOOKS_WSDL,
	$soap_options, 
	$handler_options, 
	$driver_options,
	$sql_options, 
	$callback_options);
$Server->handle(true, true);

