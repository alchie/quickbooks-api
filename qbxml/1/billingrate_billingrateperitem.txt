
// save_items
                $model->setBillingrateListid($item_obj->BillingRate_ListID);
                $model->setBillingrateFullname($item_obj->BillingRate_FullName);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setCustomrate($item_obj->CustomRate);
                $model->setCustomratepercent($item_obj->CustomRatePercent);


// populate_items
            'BillingRate_ListID' => $this->get_text_content($item, array('BillingRateRef','ListID')),
            'BillingRate_FullName' => $this->get_text_content($item, array('BillingRateRef','FullName')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'CustomRate' => $this->get_text_content($item, array('CustomRate')),
            'CustomRatePercent' => $this->get_text_content($item, array('CustomRatePercent')),


