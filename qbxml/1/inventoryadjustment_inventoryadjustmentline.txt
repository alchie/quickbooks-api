
// save_items
                $model->setInventoryadjustmentTxnid($item_obj->InventoryAdjustment_TxnID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setQuantitydifference($item_obj->QuantityDifference);
                $model->setValuedifference($item_obj->ValueDifference);
                $model->setQuantityadjustmentNewquantity($item_obj->QuantityAdjustment_NewQuantity);
                $model->setQuantityadjustmentQuantitydifference($item_obj->QuantityAdjustment_QuantityDifference);
                $model->setValueadjustmentNewquantity($item_obj->ValueAdjustment_NewQuantity);
                $model->setValueadjustmentQuantitydifference($item_obj->ValueAdjustment_QuantityDifference);
                $model->setValueadjustmentNewvalue($item_obj->ValueAdjustment_NewValue);
                $model->setValueadjustmentValuedifference($item_obj->ValueAdjustment_ValueDifference);


// populate_items
            'InventoryAdjustment_TxnID' => $this->get_text_content($item, array('InventoryAdjustmentRef','TxnID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'QuantityDifference' => $this->get_text_content($item, array('QuantityDifference')),
            'ValueDifference' => $this->get_text_content($item, array('ValueDifference')),
            'QuantityAdjustment_NewQuantity' => $this->get_text_content($item, array('QuantityAdjustmentRef','NewQuantity')),
            'QuantityAdjustment_QuantityDifference' => $this->get_text_content($item, array('QuantityAdjustmentRef','QuantityDifference')),
            'ValueAdjustment_NewQuantity' => $this->get_text_content($item, array('ValueAdjustmentRef','NewQuantity')),
            'ValueAdjustment_QuantityDifference' => $this->get_text_content($item, array('ValueAdjustmentRef','QuantityDifference')),
            'ValueAdjustment_NewValue' => $this->get_text_content($item, array('ValueAdjustmentRef','NewValue')),
            'ValueAdjustment_ValueDifference' => $this->get_text_content($item, array('ValueAdjustmentRef','ValueDifference')),


