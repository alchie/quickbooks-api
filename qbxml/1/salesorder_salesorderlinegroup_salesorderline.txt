
// save_items
                $model->setSalesorderTxnid($item_obj->SalesOrder_TxnID);
                $model->setSalesorderSalesorderlinegroupTxnlineid($item_obj->SalesOrder_SalesOrderLineGroup_TxnLineID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setDescrip($item_obj->Descrip);
                $model->setQuantity($item_obj->Quantity);
                $model->setUnitofmeasure($item_obj->UnitOfMeasure);
                $model->setOverrideuomsetListid($item_obj->OverrideUOMSet_ListID);
                $model->setOverrideuomsetFullname($item_obj->OverrideUOMSet_FullName);
                $model->setRate($item_obj->Rate);
                $model->setRatepercent($item_obj->RatePercent);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setAmount($item_obj->Amount);
                $model->setSalestaxcodeListid($item_obj->SalesTaxCode_ListID);
                $model->setSalestaxcodeFullname($item_obj->SalesTaxCode_FullName);
                $model->setInvoiced($item_obj->Invoiced);
                $model->setIsmanuallyclosed($item_obj->IsManuallyClosed);
                $model->setOther1($item_obj->Other1);
                $model->setOther2($item_obj->Other2);


// populate_items
            'SalesOrder_TxnID' => $this->get_text_content($item, array('SalesOrderRef','TxnID')),
            'SalesOrder_SalesOrderLineGroup_TxnLineID' => $this->get_text_content($item, array('SalesOrderRef','SalesOrderLineGroupRef','TxnLineID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'Quantity' => $this->get_text_content($item, array('Quantity')),
            'UnitOfMeasure' => $this->get_text_content($item, array('UnitOfMeasure')),
            'OverrideUOMSet_ListID' => $this->get_text_content($item, array('OverrideUOMSetRef','ListID')),
            'OverrideUOMSet_FullName' => $this->get_text_content($item, array('OverrideUOMSetRef','FullName')),
            'Rate' => $this->get_text_content($item, array('Rate')),
            'RatePercent' => $this->get_text_content($item, array('RatePercent')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'SalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxCodeRef','ListID')),
            'SalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxCodeRef','FullName')),
            'Invoiced' => $this->get_text_content($item, array('Invoiced')),
            'IsManuallyClosed' => $this->get_text_content($item, array('IsManuallyClosed')),
            'Other1' => $this->get_text_content($item, array('Other1')),
            'Other2' => $this->get_text_content($item, array('Other2')),


