
// save_items
                $model->setSalesreceiptTxnid($item_obj->SalesReceipt_TxnID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setItemgroupListid($item_obj->ItemGroup_ListID);
                $model->setItemgroupFullname($item_obj->ItemGroup_FullName);
                $model->setDescrip($item_obj->Descrip);
                $model->setQuantity($item_obj->Quantity);
                $model->setUnitofmeasure($item_obj->UnitOfMeasure);
                $model->setOverrideuomsetListid($item_obj->OverrideUOMSet_ListID);
                $model->setOverrideuomsetFullname($item_obj->OverrideUOMSet_FullName);
                $model->setIsprintitemsingroup($item_obj->IsPrintItemsInGroup);
                $model->setTotalamount($item_obj->TotalAmount);


// populate_items
            'SalesReceipt_TxnID' => $this->get_text_content($item, array('SalesReceiptRef','TxnID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'ItemGroup_ListID' => $this->get_text_content($item, array('ItemGroupRef','ListID')),
            'ItemGroup_FullName' => $this->get_text_content($item, array('ItemGroupRef','FullName')),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'Quantity' => $this->get_text_content($item, array('Quantity')),
            'UnitOfMeasure' => $this->get_text_content($item, array('UnitOfMeasure')),
            'OverrideUOMSet_ListID' => $this->get_text_content($item, array('OverrideUOMSetRef','ListID')),
            'OverrideUOMSet_FullName' => $this->get_text_content($item, array('OverrideUOMSetRef','FullName')),
            'IsPrintItemsInGroup' => $this->get_text_content($item, array('IsPrintItemsInGroup')),
            'TotalAmount' => $this->get_text_content($item, array('TotalAmount')),


