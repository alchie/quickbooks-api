
// save_items
                $model->setPurchaseorderTxnid($item_obj->PurchaseOrder_TxnID);
                $model->setPurchaseorderPurchaseorderlinegroupTxnlineid($item_obj->PurchaseOrder_PurchaseOrderLineGroup_TxnLineID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setManufacturerpartnumber($item_obj->ManufacturerPartNumber);
                $model->setDescrip($item_obj->Descrip);
                $model->setQuantity($item_obj->Quantity);
                $model->setUnitofmeasure($item_obj->UnitOfMeasure);
                $model->setOverrideuomsetListid($item_obj->OverrideUOMSet_ListID);
                $model->setOverrideuomsetFullname($item_obj->OverrideUOMSet_FullName);
                $model->setRate($item_obj->Rate);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setAmount($item_obj->Amount);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setServicedate($item_obj->ServiceDate);
                $model->setReceivedquantity($item_obj->ReceivedQuantity);
                $model->setIsmanuallyclosed($item_obj->IsManuallyClosed);
                $model->setOther1($item_obj->Other1);
                $model->setOther2($item_obj->Other2);


// populate_items
            'PurchaseOrder_TxnID' => $this->get_text_content($item, array('PurchaseOrderRef','TxnID')),
            'PurchaseOrder_PurchaseOrderLineGroup_TxnLineID' => $this->get_text_content($item, array('PurchaseOrderRef','PurchaseOrderLineGroupRef','TxnLineID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'ManufacturerPartNumber' => $this->get_text_content($item, array('ManufacturerPartNumber')),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'Quantity' => $this->get_text_content($item, array('Quantity')),
            'UnitOfMeasure' => $this->get_text_content($item, array('UnitOfMeasure')),
            'OverrideUOMSet_ListID' => $this->get_text_content($item, array('OverrideUOMSetRef','ListID')),
            'OverrideUOMSet_FullName' => $this->get_text_content($item, array('OverrideUOMSetRef','FullName')),
            'Rate' => $this->get_text_content($item, array('Rate')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'ServiceDate' => $this->get_text_content($item, array('ServiceDate')),
            'ReceivedQuantity' => $this->get_text_content($item, array('ReceivedQuantity')),
            'IsManuallyClosed' => $this->get_text_content($item, array('IsManuallyClosed')),
            'Other1' => $this->get_text_content($item, array('Other1')),
            'Other2' => $this->get_text_content($item, array('Other2')),


