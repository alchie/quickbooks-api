
// save_items
                $model->setFromtxnid($item_obj->FromTxnID);
                $model->setInvoiceTxnid($item_obj->Invoice_TxnID);
                $model->setLinktype($item_obj->LinkType);
                $model->setTotxnid($item_obj->ToTxnID);
                $model->setTxntype($item_obj->TxnType);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setAmount($item_obj->Amount);


// populate_items
            'FromTxnID' => $this->get_text_content($item, array('FromTxnID')),
            'Invoice_TxnID' => $this->get_text_content($item, array('InvoiceRef','TxnID')),
            'LinkType' => $this->get_text_content($item, array('LinkType')),
            'ToTxnID' => $this->get_text_content($item, array('ToTxnID')),
            'TxnType' => $this->get_text_content($item, array('TxnType')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'Amount' => $this->get_text_content($item, array('Amount')),


