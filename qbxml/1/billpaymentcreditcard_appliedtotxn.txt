
// save_items
                $model->setFromtxnid($item_obj->FromTxnID);
                $model->setBillpaymentcreditcardTxnid($item_obj->BillPaymentCreditCard_TxnID);
                $model->setTotxnid($item_obj->ToTxnID);
                $model->setTxntype($item_obj->TxnType);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setBalanceremaining($item_obj->BalanceRemaining);
                $model->setAmount($item_obj->Amount);
                $model->setDiscountamount($item_obj->DiscountAmount);
                $model->setDiscountaccountListid($item_obj->DiscountAccount_ListID);
                $model->setDiscountaccountFullname($item_obj->DiscountAccount_FullName);


// populate_items
            'FromTxnID' => $this->get_text_content($item, array('FromTxnID')),
            'BillPaymentCreditCard_TxnID' => $this->get_text_content($item, array('BillPaymentCreditCardRef','TxnID')),
            'ToTxnID' => $this->get_text_content($item, array('ToTxnID')),
            'TxnType' => $this->get_text_content($item, array('TxnType')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'BalanceRemaining' => $this->get_text_content($item, array('BalanceRemaining')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'DiscountAmount' => $this->get_text_content($item, array('DiscountAmount')),
            'DiscountAccount_ListID' => $this->get_text_content($item, array('DiscountAccountRef','ListID')),
            'DiscountAccount_FullName' => $this->get_text_content($item, array('DiscountAccountRef','FullName')),


