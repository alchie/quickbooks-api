
// save_items
                $model->setCheckTxnid($item_obj->Check_TxnID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setAccountListid($item_obj->Account_ListID);
                $model->setAccountFullname($item_obj->Account_FullName);
                $model->setAmount($item_obj->Amount);
                $model->setCurrencyref($item_obj->CurrencyRef);
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setAmountinhomecurrency($item_obj->AmountInHomeCurrency);
                $model->setMemo($item_obj->Memo);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setBillablestatus($item_obj->BillableStatus);


// populate_items
            'Check_TxnID' => $this->get_text_content($item, array('CheckRef','TxnID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'Account_ListID' => $this->get_text_content($item, array('AccountRef','ListID')),
            'Account_FullName' => $this->get_text_content($item, array('AccountRef','FullName')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'CurrencyRef' => $this->get_text_content($item, array('CurrencyRef')),
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'AmountInHomeCurrency' => $this->get_text_content($item, array('AmountInHomeCurrency')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'BillableStatus' => $this->get_text_content($item, array('BillableStatus')),


