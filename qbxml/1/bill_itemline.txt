
// save_items
                $model->setBillTxnid($item_obj->Bill_TxnID);
                $model->setSortorder($item_obj->SortOrder);
                $model->setTxnlineid($item_obj->TxnLineID);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setDescrip($item_obj->Descrip);
                $model->setQuantity($item_obj->Quantity);
                $model->setCost($item_obj->Cost);
                $model->setAmount($item_obj->Amount);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setBillablestatus($item_obj->BillableStatus);


// populate_items
            'Bill_TxnID' => $this->get_text_content($item, array('BillRef','TxnID')),
            'SortOrder' => $this->get_text_content($item, array('SortOrder')),
            'TxnLineID' => $this->get_text_content($item, array('TxnLineID')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'Quantity' => $this->get_text_content($item, array('Quantity')),
            'Cost' => $this->get_text_content($item, array('Cost')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'BillableStatus' => $this->get_text_content($item, array('BillableStatus')),


