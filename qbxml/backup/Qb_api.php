<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api {
    
    protected $api_version = '1.0';
    protected $CI;

    public function __construct()
    {
       $this->CI =& get_instance();

       // load config
       $this->CI->load->config('quickbooks_api');
       
       // load queue model
       $this->CI->load->model('Qb_api_queue_model');

       $this->CI->load->library('Qb_api_queue');
       $this->CI->load->library('Qb_api_responses');
       $this->CI->load->library('Qb_api_requests');
       $this->CI->load->library('Qb_api_xml');

    }
    
    public function handle() {

    	$inputs = $this->CI->input->raw_input_stream;
        
        if( $inputs ) {
    	       
            $this->CI->load->library('Qb_api_parser');
            
            //php_error_log($inputs, 'inputs');

            $parser = new $this->CI->qb_api_parser( $inputs );

            $soapBody = $parser->setTagName('soapBody')->get();

        	$firstChild = $soapBody->firstChild;

            $ticket = $parser->setTagName('ticket')->get();

            //php_error_log($soapBody, 'soapBody');

        	if( $firstChild ) {

                $this->CI->load->library('Qb_api_callbacks');
                $callbacks = new $this->CI->qb_api_callbacks( $firstChild->nodeName );

        		switch ( $firstChild->nodeName ) {
        			case 'serverVersion':
                        
                        //php_error_log($soapBody, 'serverVersion');

                        $callbacks->setData(array(
                            'serverVersionResult' => array('CodeIgniter Quickbooks API v'.$this->api_version.'.' . $this->CI->config->item('QB_XML_VERSION') . ' at ' . base_url())
                        ));

        			break;

        			case 'clientVersion':

                        //php_error_log($soapBody, 'clientVersion');

                        $callbacks->setData(array(
                            'clientVersionResult' => ''
                        ));

        			break;

        			case 'authenticate':

                        //php_error_log($soapBody, 'clientVersion');

        				//$strUserName = $parser->setTagName('strUserName')->getValue();
    					//$strPassword =  $parser->setTagName('strPassword')->getValue();

                        $qb_file_path = ($this->CI->config->item('QB_FILE_PATH')) ? $this->CI->config->item('QB_FILE_PATH') : '';

                        $callbacks->setData(array(
                            'authenticateResult' => array($this->_generate_ticket(),$qb_file_path)
                        ));

        			break;

                    case 'sendRequestXML':

                        if( $this->CI->config->item('QB_API_DEBUG_MODE') ) {
                            ob_start();
                        }

                        $strHCPResponse = $parser->setTagName('strHCPResponse')->get();

                        if( $strHCPResponse ) {
                            
                            $response_object = new $this->CI->qb_api_parser( $strHCPResponse->nodeValue );
                            $QBXMLMsgsRs = $response_object->setTagName('QBXMLMsgsRs')->get();
                        
                            if( isset($QBXMLMsgsRs->childNodes) ) {

                                foreach( $QBXMLMsgsRs->childNodes as $childNode ) {
                                    switch( $childNode->nodeName ) {
                                        case 'HostQueryRs':
                                            //$HostRet = $response_object->setTagName('HostRet')->get();
                                                //php_error_log($CompanyQueryRs);
                                        break;
                                        case 'CompanyQueryRs':
                                            //$CompanyRet = $response_object->setTagName('CompanyRet')->get();
                                                //php_error_log($CompanyQueryRs);
                                        break;
                                        case 'PreferencesQueryRs':
                                            $PreferencesRet = $response_object->setTagName('PreferencesRet')->get();
                                            if( $PreferencesRet ) {
                                                $this->CI->qb_api_queue->init( $ticket );
                                            }
                                        break;
                                    }
                                }
                            }

                        } 

                        $callbacks = $this->_queue_requests( $ticket->nodeValue, $callbacks );

                        if( $this->CI->config->item('QB_API_DEBUG_MODE') ) {
                            $results = ob_get_clean();
                            error_log($results,0);
                            //php_error_log($callbacks->output(), 'sendRequestXML');
                        }

                    break;

                    case 'receiveResponseXML':

                        if( $this->CI->config->item('QB_API_DEBUG_MODE') ) {
                            ob_start();
                        }

                        $response = $parser->setTagName('response')->get();
                        $response_object = new $this->CI->qb_api_parser( $response->nodeValue );
                        $QBXMLMsgsRs = $response_object->setTagName('QBXMLMsgsRs')->get();
                        
                        if( !$QBXMLMsgsRs ) {
                            $this->_delete_queue_id( $ticket->nodeValue );
                        }

                        if(($QBXMLMsgsRs) && isset($QBXMLMsgsRs->childNodes)) { 
                            foreach( $QBXMLMsgsRs->childNodes as $childNode ) {
                                $this->CI->qb_api_responses->init( $childNode->nodeName );
                            }
                        }
                    } 

                    if( $this->CI->config->item('QB_API_DEBUG_MODE') ) {
                        $results = ob_get_clean();
                        error_log($results,0);
                        //php_error_log($inputs, 'receiveResponseXML');
                    }

                    break;

                    case 'getLastError':
                        //php_error_log($inputs, 'getLastError');
                    break;

                    case 'closeConnection':
                        //php_error_log($inputs, 'closeConnection');
                        //$this->_clear_queue();
                    break;


        		}
                
                if( $this->CI->config->item('QB_API_DEBUG_MODE') ) {
                    php_error_log($inputs, 'inputs');
                    php_error_log($callbacks->output(), 'callbacks');
                }

                $this->CI->output->set_content_type('text/xml')->set_output($callbacks->output());

            }
        
    	}
		
    }

    protected function _queue_requests($ticket_id, $callbacks) {
            // fetch queue
            $queue = $this->CI->qb_api_queue->_fetch_queue( $ticket_id );

            if( $queue ) {
                $callbacks->setData(array(
                    'sendRequestXMLResult' => $this->CI->Qb_api_requests->init( $queue->request_method )
                ));
            } 

            return $callbacks;
    }

    protected function _generate_ticket($code='') {
        $code = ($code!='') ? $code : time();
        $set1 = substr(md5($code), 0, 8);
        $set2 = substr(md5($set1), 0, 4);
        $set3 = substr(md5($set2), 0, 4);
        $set4 = substr(md5($set3), 0, 4);
        $set5 = substr(md5($set4), 0, 12);
        return $set1."-".$set2."-".$set3."-".$set4."-".$set5;
    }

    public function qwc($options=array())
    {

$filename = (isset($options['filename'])) ? $options['filename'] : 'qb_api';
$app_name = (isset($options['app_name'])) ? $options['app_name'] : 'CI_QB_API_V_' . $this->api_version;
$app_id = (isset($options['app_id'])) ? $options['app_id'] : 'CI_QB_API_V_' . $this->api_version;
$app_description = (isset($options['app_description'])) ? $options['app_description'] : 'CodeIgniter Quickbooks API v' . $this->api_version;
$app_url = (isset($options['app_url'])) ? $options['app_url'] : site_url("quickbooks_api");
$app_support = (isset($options['app_support'])) ? $options['app_support'] : site_url("quickbooks_api/support");
$username = (isset($options['username'])) ? $options['username'] : 'qb_api';
$owner_id = (isset($options['owner_id'])) ? $options['owner_id'] : '{6850aef4-53f1-3af4-818e-408ccaf4521e}';
$file_id = (isset($options['file_id'])) ? $options['file_id'] : '{66e91551-980c-b214-7d1d-43ac5905a58e}';
$notify = (isset($options['notify']) && ($options['notify'])) ? 'true' : 'false';
$interval = (isset($options['interval'])) ? $options['interval'] : 10;
$read_only = (isset($options['read_only']) && ($options['read_only'])) ? 'true' : 'false';

header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="'.$filename.'.qwc"');

echo <<<QWC
<?xml version="1.0"?>
<QBWCXML>
    <AppName>{$app_name}</AppName>
    <AppID></AppID>
    <AppURL>{$app_url}</AppURL>
    <AppDescription>{$app_description}</AppDescription>
    <AppSupport>{$app_support}</AppSupport>
    <UserName>{$username}</UserName>
    <OwnerID>{$owner_id}</OwnerID>
    <FileID>{$file_id}</FileID>
    <QBType>QBFS</QBType>
    <Notify>{$notify}</Notify>
    <Scheduler>
        <RunEveryNMinutes>{$interval}</RunEveryNMinutes>
    </Scheduler>
    <IsReadOnly>{$read_only}</IsReadOnly>
</QBWCXML>
QWC;

    }

}

/* End of file Global_variables.php */