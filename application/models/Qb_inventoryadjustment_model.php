<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_inventoryadjustment_model Class
 *
 * Manipulates `qb_inventoryadjustment` table on database

CREATE TABLE `qb_inventoryadjustment` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Account_ListID` varchar(40) DEFAULT NULL,
  `Account_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Memo` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `Account_ListID` (`Account_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_inventoryadjustment` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_inventoryadjustment` ADD  `Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_inventoryadjustment` ADD  `Memo` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_inventoryadjustment_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Account_ListID;
	protected $Account_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Memo;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_inventoryadjustment';
		$this->_short_name = 'qb_inventoryadjustment';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Account_ListID","Account_FullName","TxnDate","RefNumber","Customer_ListID","Customer_FullName","Class_ListID","Class_FullName","Memo");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `Account_ListID` variable
	* @access public
	*/

	public function setAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_ListID` variable
	* @access public
	*/

	public function getAccountListid() {
		return $this->Account_ListID;
	}

	public function get_Account_ListID() {
		return $this->Account_ListID;
	}

	
// ------------------------------ End Field: Account_ListID --------------------------------------


// ---------------------------- Start Field: Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `Account_FullName` variable
	* @access public
	*/

	public function setAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_FullName` variable
	* @access public
	*/

	public function getAccountFullname() {
		return $this->Account_FullName;
	}

	public function get_Account_FullName() {
		return $this->Account_FullName;
	}

	
// ------------------------------ End Field: Account_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Account_ListID' => (object) array(
										'Field'=>'Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_FullName' => (object) array(
										'Field'=>'Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Account_ListID' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Account_ListID` varchar(40) NULL   ;",
			'Account_FullName' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Account_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `RefNumber` varchar(11) NULL   ;",
			'Customer_ListID' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_inventoryadjustment` ADD  `Memo` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setAccountListid() - Account_ListID
//setAccountFullname() - Account_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setMemo() - Memo

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Account_ListID() - Account_ListID
//set_Account_FullName() - Account_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Memo() - Memo

*/
/* End of file Qb_inventoryadjustment_model.php */
/* Location: ./application/models/Qb_inventoryadjustment_model.php */
