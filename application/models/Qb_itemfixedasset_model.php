<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_itemfixedasset_model Class
 *
 * Manipulates `qb_itemfixedasset` table on database

CREATE TABLE `qb_itemfixedasset` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `AcquiredAs` varchar(40) DEFAULT NULL,
  `PurchaseDesc` varchar(50) DEFAULT NULL,
  `PurchaseDate` date DEFAULT NULL,
  `PurchaseCost` decimal(13,5) DEFAULT NULL,
  `VendorOrPayeeName` varchar(50) DEFAULT NULL,
  `AssetAccount_ListID` varchar(40) DEFAULT NULL,
  `AssetAccount_FullName` varchar(255) DEFAULT NULL,
  `FixedAssetSalesInfo_SalesDesc` varchar(50) DEFAULT NULL,
  `FixedAssetSalesInfo_SalesDate` date DEFAULT NULL,
  `FixedAssetSalesInfo_SalesPrice` decimal(13,5) DEFAULT NULL,
  `FixedAssetSalesInfo_SalesExpense` decimal(13,5) DEFAULT NULL,
  `AssetDesc` varchar(50) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `PONumber` varchar(30) DEFAULT NULL,
  `SerialNumber` varchar(30) DEFAULT NULL,
  `WarrantyExpDate` date DEFAULT NULL,
  `Notes` text,
  `AssetNumber` varchar(10) DEFAULT NULL,
  `CostBasis` decimal(10,2) DEFAULT NULL,
  `YearEndAccumulatedDepreciation` decimal(10,2) DEFAULT NULL,
  `YearEndBookValue` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `AssetAccount_ListID` (`AssetAccount_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_itemfixedasset` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_itemfixedasset` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_itemfixedasset` ADD  `AcquiredAs` varchar(40) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseDesc` varchar(50) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseDate` date NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseCost` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `VendorOrPayeeName` varchar(50) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `AssetAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `AssetAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesDesc` varchar(50) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesDate` date NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesPrice` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesExpense` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `AssetDesc` varchar(50) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `Location` varchar(50) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `PONumber` varchar(30) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `SerialNumber` varchar(30) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `WarrantyExpDate` date NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `AssetNumber` varchar(10) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `CostBasis` decimal(10,2) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `YearEndAccumulatedDepreciation` decimal(10,2) NULL   ;
ALTER TABLE  `qb_itemfixedasset` ADD  `YearEndBookValue` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_itemfixedasset_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $AcquiredAs;
	protected $PurchaseDesc;
	protected $PurchaseDate;
	protected $PurchaseCost;
	protected $VendorOrPayeeName;
	protected $AssetAccount_ListID;
	protected $AssetAccount_FullName;
	protected $FixedAssetSalesInfo_SalesDesc;
	protected $FixedAssetSalesInfo_SalesDate;
	protected $FixedAssetSalesInfo_SalesPrice;
	protected $FixedAssetSalesInfo_SalesExpense;
	protected $AssetDesc;
	protected $Location;
	protected $PONumber;
	protected $SerialNumber;
	protected $WarrantyExpDate;
	protected $Notes;
	protected $AssetNumber;
	protected $CostBasis;
	protected $YearEndAccumulatedDepreciation;
	protected $YearEndBookValue;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_itemfixedasset';
		$this->_short_name = 'qb_itemfixedasset';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","AcquiredAs","PurchaseDesc","PurchaseDate","PurchaseCost","VendorOrPayeeName","AssetAccount_ListID","AssetAccount_FullName","FixedAssetSalesInfo_SalesDesc","FixedAssetSalesInfo_SalesDate","FixedAssetSalesInfo_SalesPrice","FixedAssetSalesInfo_SalesExpense","AssetDesc","Location","PONumber","SerialNumber","WarrantyExpDate","Notes","AssetNumber","CostBasis","YearEndAccumulatedDepreciation","YearEndBookValue");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: AcquiredAs -------------------------------------- 

	/** 
	* Sets a value to `AcquiredAs` variable
	* @access public
	*/

	public function setAcquiredas($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AcquiredAs', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AcquiredAs($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AcquiredAs', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AcquiredAs` variable
	* @access public
	*/

	public function getAcquiredas() {
		return $this->AcquiredAs;
	}

	public function get_AcquiredAs() {
		return $this->AcquiredAs;
	}

	
// ------------------------------ End Field: AcquiredAs --------------------------------------


// ---------------------------- Start Field: PurchaseDesc -------------------------------------- 

	/** 
	* Sets a value to `PurchaseDesc` variable
	* @access public
	*/

	public function setPurchasedesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseDesc` variable
	* @access public
	*/

	public function getPurchasedesc() {
		return $this->PurchaseDesc;
	}

	public function get_PurchaseDesc() {
		return $this->PurchaseDesc;
	}

	
// ------------------------------ End Field: PurchaseDesc --------------------------------------


// ---------------------------- Start Field: PurchaseDate -------------------------------------- 

	/** 
	* Sets a value to `PurchaseDate` variable
	* @access public
	*/

	public function setPurchasedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseDate` variable
	* @access public
	*/

	public function getPurchasedate() {
		return $this->PurchaseDate;
	}

	public function get_PurchaseDate() {
		return $this->PurchaseDate;
	}

	
// ------------------------------ End Field: PurchaseDate --------------------------------------


// ---------------------------- Start Field: PurchaseCost -------------------------------------- 

	/** 
	* Sets a value to `PurchaseCost` variable
	* @access public
	*/

	public function setPurchasecost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseCost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseCost` variable
	* @access public
	*/

	public function getPurchasecost() {
		return $this->PurchaseCost;
	}

	public function get_PurchaseCost() {
		return $this->PurchaseCost;
	}

	
// ------------------------------ End Field: PurchaseCost --------------------------------------


// ---------------------------- Start Field: VendorOrPayeeName -------------------------------------- 

	/** 
	* Sets a value to `VendorOrPayeeName` variable
	* @access public
	*/

	public function setVendororpayeename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorOrPayeeName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorOrPayeeName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorOrPayeeName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorOrPayeeName` variable
	* @access public
	*/

	public function getVendororpayeename() {
		return $this->VendorOrPayeeName;
	}

	public function get_VendorOrPayeeName() {
		return $this->VendorOrPayeeName;
	}

	
// ------------------------------ End Field: VendorOrPayeeName --------------------------------------


// ---------------------------- Start Field: AssetAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `AssetAccount_ListID` variable
	* @access public
	*/

	public function setAssetaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetAccount_ListID` variable
	* @access public
	*/

	public function getAssetaccountListid() {
		return $this->AssetAccount_ListID;
	}

	public function get_AssetAccount_ListID() {
		return $this->AssetAccount_ListID;
	}

	
// ------------------------------ End Field: AssetAccount_ListID --------------------------------------


// ---------------------------- Start Field: AssetAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `AssetAccount_FullName` variable
	* @access public
	*/

	public function setAssetaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetAccount_FullName` variable
	* @access public
	*/

	public function getAssetaccountFullname() {
		return $this->AssetAccount_FullName;
	}

	public function get_AssetAccount_FullName() {
		return $this->AssetAccount_FullName;
	}

	
// ------------------------------ End Field: AssetAccount_FullName --------------------------------------


// ---------------------------- Start Field: FixedAssetSalesInfo_SalesDesc -------------------------------------- 

	/** 
	* Sets a value to `FixedAssetSalesInfo_SalesDesc` variable
	* @access public
	*/

	public function setFixedassetsalesinfoSalesdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FixedAssetSalesInfo_SalesDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FixedAssetSalesInfo_SalesDesc` variable
	* @access public
	*/

	public function getFixedassetsalesinfoSalesdesc() {
		return $this->FixedAssetSalesInfo_SalesDesc;
	}

	public function get_FixedAssetSalesInfo_SalesDesc() {
		return $this->FixedAssetSalesInfo_SalesDesc;
	}

	
// ------------------------------ End Field: FixedAssetSalesInfo_SalesDesc --------------------------------------


// ---------------------------- Start Field: FixedAssetSalesInfo_SalesDate -------------------------------------- 

	/** 
	* Sets a value to `FixedAssetSalesInfo_SalesDate` variable
	* @access public
	*/

	public function setFixedassetsalesinfoSalesdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FixedAssetSalesInfo_SalesDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FixedAssetSalesInfo_SalesDate` variable
	* @access public
	*/

	public function getFixedassetsalesinfoSalesdate() {
		return $this->FixedAssetSalesInfo_SalesDate;
	}

	public function get_FixedAssetSalesInfo_SalesDate() {
		return $this->FixedAssetSalesInfo_SalesDate;
	}

	
// ------------------------------ End Field: FixedAssetSalesInfo_SalesDate --------------------------------------


// ---------------------------- Start Field: FixedAssetSalesInfo_SalesPrice -------------------------------------- 

	/** 
	* Sets a value to `FixedAssetSalesInfo_SalesPrice` variable
	* @access public
	*/

	public function setFixedassetsalesinfoSalesprice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FixedAssetSalesInfo_SalesPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FixedAssetSalesInfo_SalesPrice` variable
	* @access public
	*/

	public function getFixedassetsalesinfoSalesprice() {
		return $this->FixedAssetSalesInfo_SalesPrice;
	}

	public function get_FixedAssetSalesInfo_SalesPrice() {
		return $this->FixedAssetSalesInfo_SalesPrice;
	}

	
// ------------------------------ End Field: FixedAssetSalesInfo_SalesPrice --------------------------------------


// ---------------------------- Start Field: FixedAssetSalesInfo_SalesExpense -------------------------------------- 

	/** 
	* Sets a value to `FixedAssetSalesInfo_SalesExpense` variable
	* @access public
	*/

	public function setFixedassetsalesinfoSalesexpense($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesExpense', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FixedAssetSalesInfo_SalesExpense($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FixedAssetSalesInfo_SalesExpense', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FixedAssetSalesInfo_SalesExpense` variable
	* @access public
	*/

	public function getFixedassetsalesinfoSalesexpense() {
		return $this->FixedAssetSalesInfo_SalesExpense;
	}

	public function get_FixedAssetSalesInfo_SalesExpense() {
		return $this->FixedAssetSalesInfo_SalesExpense;
	}

	
// ------------------------------ End Field: FixedAssetSalesInfo_SalesExpense --------------------------------------


// ---------------------------- Start Field: AssetDesc -------------------------------------- 

	/** 
	* Sets a value to `AssetDesc` variable
	* @access public
	*/

	public function setAssetdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetDesc` variable
	* @access public
	*/

	public function getAssetdesc() {
		return $this->AssetDesc;
	}

	public function get_AssetDesc() {
		return $this->AssetDesc;
	}

	
// ------------------------------ End Field: AssetDesc --------------------------------------


// ---------------------------- Start Field: Location -------------------------------------- 

	/** 
	* Sets a value to `Location` variable
	* @access public
	*/

	public function setLocation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Location', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Location($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Location', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Location` variable
	* @access public
	*/

	public function getLocation() {
		return $this->Location;
	}

	public function get_Location() {
		return $this->Location;
	}

	
// ------------------------------ End Field: Location --------------------------------------


// ---------------------------- Start Field: PONumber -------------------------------------- 

	/** 
	* Sets a value to `PONumber` variable
	* @access public
	*/

	public function setPonumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PONumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PONumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PONumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PONumber` variable
	* @access public
	*/

	public function getPonumber() {
		return $this->PONumber;
	}

	public function get_PONumber() {
		return $this->PONumber;
	}

	
// ------------------------------ End Field: PONumber --------------------------------------


// ---------------------------- Start Field: SerialNumber -------------------------------------- 

	/** 
	* Sets a value to `SerialNumber` variable
	* @access public
	*/

	public function setSerialnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SerialNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SerialNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SerialNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SerialNumber` variable
	* @access public
	*/

	public function getSerialnumber() {
		return $this->SerialNumber;
	}

	public function get_SerialNumber() {
		return $this->SerialNumber;
	}

	
// ------------------------------ End Field: SerialNumber --------------------------------------


// ---------------------------- Start Field: WarrantyExpDate -------------------------------------- 

	/** 
	* Sets a value to `WarrantyExpDate` variable
	* @access public
	*/

	public function setWarrantyexpdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('WarrantyExpDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_WarrantyExpDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('WarrantyExpDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `WarrantyExpDate` variable
	* @access public
	*/

	public function getWarrantyexpdate() {
		return $this->WarrantyExpDate;
	}

	public function get_WarrantyExpDate() {
		return $this->WarrantyExpDate;
	}

	
// ------------------------------ End Field: WarrantyExpDate --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: AssetNumber -------------------------------------- 

	/** 
	* Sets a value to `AssetNumber` variable
	* @access public
	*/

	public function setAssetnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetNumber` variable
	* @access public
	*/

	public function getAssetnumber() {
		return $this->AssetNumber;
	}

	public function get_AssetNumber() {
		return $this->AssetNumber;
	}

	
// ------------------------------ End Field: AssetNumber --------------------------------------


// ---------------------------- Start Field: CostBasis -------------------------------------- 

	/** 
	* Sets a value to `CostBasis` variable
	* @access public
	*/

	public function setCostbasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CostBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CostBasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CostBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CostBasis` variable
	* @access public
	*/

	public function getCostbasis() {
		return $this->CostBasis;
	}

	public function get_CostBasis() {
		return $this->CostBasis;
	}

	
// ------------------------------ End Field: CostBasis --------------------------------------


// ---------------------------- Start Field: YearEndAccumulatedDepreciation -------------------------------------- 

	/** 
	* Sets a value to `YearEndAccumulatedDepreciation` variable
	* @access public
	*/

	public function setYearendaccumulateddepreciation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('YearEndAccumulatedDepreciation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_YearEndAccumulatedDepreciation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('YearEndAccumulatedDepreciation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `YearEndAccumulatedDepreciation` variable
	* @access public
	*/

	public function getYearendaccumulateddepreciation() {
		return $this->YearEndAccumulatedDepreciation;
	}

	public function get_YearEndAccumulatedDepreciation() {
		return $this->YearEndAccumulatedDepreciation;
	}

	
// ------------------------------ End Field: YearEndAccumulatedDepreciation --------------------------------------


// ---------------------------- Start Field: YearEndBookValue -------------------------------------- 

	/** 
	* Sets a value to `YearEndBookValue` variable
	* @access public
	*/

	public function setYearendbookvalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('YearEndBookValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_YearEndBookValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('YearEndBookValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `YearEndBookValue` variable
	* @access public
	*/

	public function getYearendbookvalue() {
		return $this->YearEndBookValue;
	}

	public function get_YearEndBookValue() {
		return $this->YearEndBookValue;
	}

	
// ------------------------------ End Field: YearEndBookValue --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'AcquiredAs' => (object) array(
										'Field'=>'AcquiredAs',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchaseDesc' => (object) array(
										'Field'=>'PurchaseDesc',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchaseDate' => (object) array(
										'Field'=>'PurchaseDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchaseCost' => (object) array(
										'Field'=>'PurchaseCost',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorOrPayeeName' => (object) array(
										'Field'=>'VendorOrPayeeName',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetAccount_ListID' => (object) array(
										'Field'=>'AssetAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetAccount_FullName' => (object) array(
										'Field'=>'AssetAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FixedAssetSalesInfo_SalesDesc' => (object) array(
										'Field'=>'FixedAssetSalesInfo_SalesDesc',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FixedAssetSalesInfo_SalesDate' => (object) array(
										'Field'=>'FixedAssetSalesInfo_SalesDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FixedAssetSalesInfo_SalesPrice' => (object) array(
										'Field'=>'FixedAssetSalesInfo_SalesPrice',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FixedAssetSalesInfo_SalesExpense' => (object) array(
										'Field'=>'FixedAssetSalesInfo_SalesExpense',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetDesc' => (object) array(
										'Field'=>'AssetDesc',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Location' => (object) array(
										'Field'=>'Location',
										'Type'=>'varchar(50)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PONumber' => (object) array(
										'Field'=>'PONumber',
										'Type'=>'varchar(30)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SerialNumber' => (object) array(
										'Field'=>'SerialNumber',
										'Type'=>'varchar(30)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'WarrantyExpDate' => (object) array(
										'Field'=>'WarrantyExpDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetNumber' => (object) array(
										'Field'=>'AssetNumber',
										'Type'=>'varchar(10)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CostBasis' => (object) array(
										'Field'=>'CostBasis',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'YearEndAccumulatedDepreciation' => (object) array(
										'Field'=>'YearEndAccumulatedDepreciation',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'YearEndBookValue' => (object) array(
										'Field'=>'YearEndBookValue',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_itemfixedasset` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_itemfixedasset` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_itemfixedasset` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_itemfixedasset` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_itemfixedasset` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_itemfixedasset` ADD  `Name` varchar(31) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_itemfixedasset` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'AcquiredAs' => "ALTER TABLE  `qb_itemfixedasset` ADD  `AcquiredAs` varchar(40) NULL   ;",
			'PurchaseDesc' => "ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseDesc` varchar(50) NULL   ;",
			'PurchaseDate' => "ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseDate` date NULL   ;",
			'PurchaseCost' => "ALTER TABLE  `qb_itemfixedasset` ADD  `PurchaseCost` decimal(13,5) NULL   ;",
			'VendorOrPayeeName' => "ALTER TABLE  `qb_itemfixedasset` ADD  `VendorOrPayeeName` varchar(50) NULL   ;",
			'AssetAccount_ListID' => "ALTER TABLE  `qb_itemfixedasset` ADD  `AssetAccount_ListID` varchar(40) NULL   ;",
			'AssetAccount_FullName' => "ALTER TABLE  `qb_itemfixedasset` ADD  `AssetAccount_FullName` varchar(255) NULL   ;",
			'FixedAssetSalesInfo_SalesDesc' => "ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesDesc` varchar(50) NULL   ;",
			'FixedAssetSalesInfo_SalesDate' => "ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesDate` date NULL   ;",
			'FixedAssetSalesInfo_SalesPrice' => "ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesPrice` decimal(13,5) NULL   ;",
			'FixedAssetSalesInfo_SalesExpense' => "ALTER TABLE  `qb_itemfixedasset` ADD  `FixedAssetSalesInfo_SalesExpense` decimal(13,5) NULL   ;",
			'AssetDesc' => "ALTER TABLE  `qb_itemfixedasset` ADD  `AssetDesc` varchar(50) NULL   ;",
			'Location' => "ALTER TABLE  `qb_itemfixedasset` ADD  `Location` varchar(50) NULL   ;",
			'PONumber' => "ALTER TABLE  `qb_itemfixedasset` ADD  `PONumber` varchar(30) NULL   ;",
			'SerialNumber' => "ALTER TABLE  `qb_itemfixedasset` ADD  `SerialNumber` varchar(30) NULL   ;",
			'WarrantyExpDate' => "ALTER TABLE  `qb_itemfixedasset` ADD  `WarrantyExpDate` date NULL   ;",
			'Notes' => "ALTER TABLE  `qb_itemfixedasset` ADD  `Notes` text NULL   ;",
			'AssetNumber' => "ALTER TABLE  `qb_itemfixedasset` ADD  `AssetNumber` varchar(10) NULL   ;",
			'CostBasis' => "ALTER TABLE  `qb_itemfixedasset` ADD  `CostBasis` decimal(10,2) NULL   ;",
			'YearEndAccumulatedDepreciation' => "ALTER TABLE  `qb_itemfixedasset` ADD  `YearEndAccumulatedDepreciation` decimal(10,2) NULL   ;",
			'YearEndBookValue' => "ALTER TABLE  `qb_itemfixedasset` ADD  `YearEndBookValue` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setAcquiredas() - AcquiredAs
//setPurchasedesc() - PurchaseDesc
//setPurchasedate() - PurchaseDate
//setPurchasecost() - PurchaseCost
//setVendororpayeename() - VendorOrPayeeName
//setAssetaccountListid() - AssetAccount_ListID
//setAssetaccountFullname() - AssetAccount_FullName
//setFixedassetsalesinfoSalesdesc() - FixedAssetSalesInfo_SalesDesc
//setFixedassetsalesinfoSalesdate() - FixedAssetSalesInfo_SalesDate
//setFixedassetsalesinfoSalesprice() - FixedAssetSalesInfo_SalesPrice
//setFixedassetsalesinfoSalesexpense() - FixedAssetSalesInfo_SalesExpense
//setAssetdesc() - AssetDesc
//setLocation() - Location
//setPonumber() - PONumber
//setSerialnumber() - SerialNumber
//setWarrantyexpdate() - WarrantyExpDate
//setNotes() - Notes
//setAssetnumber() - AssetNumber
//setCostbasis() - CostBasis
//setYearendaccumulateddepreciation() - YearEndAccumulatedDepreciation
//setYearendbookvalue() - YearEndBookValue

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_AcquiredAs() - AcquiredAs
//set_PurchaseDesc() - PurchaseDesc
//set_PurchaseDate() - PurchaseDate
//set_PurchaseCost() - PurchaseCost
//set_VendorOrPayeeName() - VendorOrPayeeName
//set_AssetAccount_ListID() - AssetAccount_ListID
//set_AssetAccount_FullName() - AssetAccount_FullName
//set_FixedAssetSalesInfo_SalesDesc() - FixedAssetSalesInfo_SalesDesc
//set_FixedAssetSalesInfo_SalesDate() - FixedAssetSalesInfo_SalesDate
//set_FixedAssetSalesInfo_SalesPrice() - FixedAssetSalesInfo_SalesPrice
//set_FixedAssetSalesInfo_SalesExpense() - FixedAssetSalesInfo_SalesExpense
//set_AssetDesc() - AssetDesc
//set_Location() - Location
//set_PONumber() - PONumber
//set_SerialNumber() - SerialNumber
//set_WarrantyExpDate() - WarrantyExpDate
//set_Notes() - Notes
//set_AssetNumber() - AssetNumber
//set_CostBasis() - CostBasis
//set_YearEndAccumulatedDepreciation() - YearEndAccumulatedDepreciation
//set_YearEndBookValue() - YearEndBookValue

*/
/* End of file Qb_itemfixedasset_model.php */
/* Location: ./application/models/Qb_itemfixedasset_model.php */
