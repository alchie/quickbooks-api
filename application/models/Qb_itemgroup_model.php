<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_itemgroup_model Class
 *
 * Manipulates `qb_itemgroup` table on database

CREATE TABLE `qb_itemgroup` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `ItemDesc` text,
  `UnitOfMeasureSet_ListID` varchar(40) DEFAULT NULL,
  `UnitOfMeasureSet_FullName` varchar(255) DEFAULT NULL,
  `IsPrintItemsInGroup` tinyint(1) DEFAULT '0',
  `SpecialItemType` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `UnitOfMeasureSet_ListID` (`UnitOfMeasureSet_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_itemgroup` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_itemgroup` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_itemgroup` ADD  `ItemDesc` text NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemgroup` ADD  `IsPrintItemsInGroup` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_itemgroup` ADD  `SpecialItemType` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_itemgroup_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $ItemDesc;
	protected $UnitOfMeasureSet_ListID;
	protected $UnitOfMeasureSet_FullName;
	protected $IsPrintItemsInGroup;
	protected $SpecialItemType;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_itemgroup';
		$this->_short_name = 'qb_itemgroup';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","ItemDesc","UnitOfMeasureSet_ListID","UnitOfMeasureSet_FullName","IsPrintItemsInGroup","SpecialItemType");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: ItemDesc -------------------------------------- 

	/** 
	* Sets a value to `ItemDesc` variable
	* @access public
	*/

	public function setItemdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemDesc` variable
	* @access public
	*/

	public function getItemdesc() {
		return $this->ItemDesc;
	}

	public function get_ItemDesc() {
		return $this->ItemDesc;
	}

	
// ------------------------------ End Field: ItemDesc --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function setUnitofmeasuresetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function getUnitofmeasuresetListid() {
		return $this->UnitOfMeasureSet_ListID;
	}

	public function get_UnitOfMeasureSet_ListID() {
		return $this->UnitOfMeasureSet_ListID;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_ListID --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function setUnitofmeasuresetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function getUnitofmeasuresetFullname() {
		return $this->UnitOfMeasureSet_FullName;
	}

	public function get_UnitOfMeasureSet_FullName() {
		return $this->UnitOfMeasureSet_FullName;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_FullName --------------------------------------


// ---------------------------- Start Field: IsPrintItemsInGroup -------------------------------------- 

	/** 
	* Sets a value to `IsPrintItemsInGroup` variable
	* @access public
	*/

	public function setIsprintitemsingroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPrintItemsInGroup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPrintItemsInGroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPrintItemsInGroup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPrintItemsInGroup` variable
	* @access public
	*/

	public function getIsprintitemsingroup() {
		return $this->IsPrintItemsInGroup;
	}

	public function get_IsPrintItemsInGroup() {
		return $this->IsPrintItemsInGroup;
	}

	
// ------------------------------ End Field: IsPrintItemsInGroup --------------------------------------


// ---------------------------- Start Field: SpecialItemType -------------------------------------- 

	/** 
	* Sets a value to `SpecialItemType` variable
	* @access public
	*/

	public function setSpecialitemtype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SpecialItemType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SpecialItemType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SpecialItemType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SpecialItemType` variable
	* @access public
	*/

	public function getSpecialitemtype() {
		return $this->SpecialItemType;
	}

	public function get_SpecialItemType() {
		return $this->SpecialItemType;
	}

	
// ------------------------------ End Field: SpecialItemType --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'ItemDesc' => (object) array(
										'Field'=>'ItemDesc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasureSet_ListID' => (object) array(
										'Field'=>'UnitOfMeasureSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasureSet_FullName' => (object) array(
										'Field'=>'UnitOfMeasureSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPrintItemsInGroup' => (object) array(
										'Field'=>'IsPrintItemsInGroup',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SpecialItemType' => (object) array(
										'Field'=>'SpecialItemType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_itemgroup` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_itemgroup` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_itemgroup` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_itemgroup` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_itemgroup` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_itemgroup` ADD  `Name` varchar(31) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_itemgroup` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'ItemDesc' => "ALTER TABLE  `qb_itemgroup` ADD  `ItemDesc` text NULL   ;",
			'UnitOfMeasureSet_ListID' => "ALTER TABLE  `qb_itemgroup` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;",
			'UnitOfMeasureSet_FullName' => "ALTER TABLE  `qb_itemgroup` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;",
			'IsPrintItemsInGroup' => "ALTER TABLE  `qb_itemgroup` ADD  `IsPrintItemsInGroup` tinyint(1) NULL   DEFAULT '0';",
			'SpecialItemType' => "ALTER TABLE  `qb_itemgroup` ADD  `SpecialItemType` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setItemdesc() - ItemDesc
//setUnitofmeasuresetListid() - UnitOfMeasureSet_ListID
//setUnitofmeasuresetFullname() - UnitOfMeasureSet_FullName
//setIsprintitemsingroup() - IsPrintItemsInGroup
//setSpecialitemtype() - SpecialItemType

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_ItemDesc() - ItemDesc
//set_UnitOfMeasureSet_ListID() - UnitOfMeasureSet_ListID
//set_UnitOfMeasureSet_FullName() - UnitOfMeasureSet_FullName
//set_IsPrintItemsInGroup() - IsPrintItemsInGroup
//set_SpecialItemType() - SpecialItemType

*/
/* End of file Qb_itemgroup_model.php */
/* Location: ./application/models/Qb_itemgroup_model.php */
