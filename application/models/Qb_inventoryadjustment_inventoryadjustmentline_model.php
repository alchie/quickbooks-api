<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_inventoryadjustment_inventoryadjustmentline_model Class
 *
 * Manipulates `qb_inventoryadjustment_inventoryadjustmentline` table on database

CREATE TABLE `qb_inventoryadjustment_inventoryadjustmentline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `InventoryAdjustment_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `QuantityDifference` decimal(12,5) DEFAULT '0.00000',
  `ValueDifference` decimal(10,2) DEFAULT NULL,
  `QuantityAdjustment_NewQuantity` decimal(12,5) DEFAULT NULL,
  `QuantityAdjustment_QuantityDifference` decimal(12,5) DEFAULT NULL,
  `ValueAdjustment_NewQuantity` decimal(12,5) DEFAULT NULL,
  `ValueAdjustment_QuantityDifference` decimal(12,5) DEFAULT NULL,
  `ValueAdjustment_NewValue` decimal(10,2) DEFAULT NULL,
  `ValueAdjustment_ValueDifference` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `InventoryAdjustment_TxnID` (`InventoryAdjustment_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Item_ListID` (`Item_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `InventoryAdjustment_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityDifference` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueDifference` decimal(10,2) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityAdjustment_NewQuantity` decimal(12,5) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityAdjustment_QuantityDifference` decimal(12,5) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_NewQuantity` decimal(12,5) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_QuantityDifference` decimal(12,5) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_NewValue` decimal(10,2) NULL   ;
ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_ValueDifference` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_inventoryadjustment_inventoryadjustmentline_model extends MY_Model {

	protected $qbxml_id;
	protected $InventoryAdjustment_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $QuantityDifference;
	protected $ValueDifference;
	protected $QuantityAdjustment_NewQuantity;
	protected $QuantityAdjustment_QuantityDifference;
	protected $ValueAdjustment_NewQuantity;
	protected $ValueAdjustment_QuantityDifference;
	protected $ValueAdjustment_NewValue;
	protected $ValueAdjustment_ValueDifference;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_inventoryadjustment_inventoryadjustmentline';
		$this->_short_name = 'qb_inventoryadjustment_inventoryadjustmentline';
		$this->_fields = array("qbxml_id","InventoryAdjustment_TxnID","SortOrder","TxnLineID","Item_ListID","Item_FullName","QuantityDifference","ValueDifference","QuantityAdjustment_NewQuantity","QuantityAdjustment_QuantityDifference","ValueAdjustment_NewQuantity","ValueAdjustment_QuantityDifference","ValueAdjustment_NewValue","ValueAdjustment_ValueDifference");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: InventoryAdjustment_TxnID -------------------------------------- 

	/** 
	* Sets a value to `InventoryAdjustment_TxnID` variable
	* @access public
	*/

	public function setInventoryadjustmentTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('InventoryAdjustment_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_InventoryAdjustment_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('InventoryAdjustment_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `InventoryAdjustment_TxnID` variable
	* @access public
	*/

	public function getInventoryadjustmentTxnid() {
		return $this->InventoryAdjustment_TxnID;
	}

	public function get_InventoryAdjustment_TxnID() {
		return $this->InventoryAdjustment_TxnID;
	}

	
// ------------------------------ End Field: InventoryAdjustment_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: QuantityDifference -------------------------------------- 

	/** 
	* Sets a value to `QuantityDifference` variable
	* @access public
	*/

	public function setQuantitydifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityDifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityDifference` variable
	* @access public
	*/

	public function getQuantitydifference() {
		return $this->QuantityDifference;
	}

	public function get_QuantityDifference() {
		return $this->QuantityDifference;
	}

	
// ------------------------------ End Field: QuantityDifference --------------------------------------


// ---------------------------- Start Field: ValueDifference -------------------------------------- 

	/** 
	* Sets a value to `ValueDifference` variable
	* @access public
	*/

	public function setValuedifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ValueDifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ValueDifference` variable
	* @access public
	*/

	public function getValuedifference() {
		return $this->ValueDifference;
	}

	public function get_ValueDifference() {
		return $this->ValueDifference;
	}

	
// ------------------------------ End Field: ValueDifference --------------------------------------


// ---------------------------- Start Field: QuantityAdjustment_NewQuantity -------------------------------------- 

	/** 
	* Sets a value to `QuantityAdjustment_NewQuantity` variable
	* @access public
	*/

	public function setQuantityadjustmentNewquantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityAdjustment_NewQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityAdjustment_NewQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityAdjustment_NewQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityAdjustment_NewQuantity` variable
	* @access public
	*/

	public function getQuantityadjustmentNewquantity() {
		return $this->QuantityAdjustment_NewQuantity;
	}

	public function get_QuantityAdjustment_NewQuantity() {
		return $this->QuantityAdjustment_NewQuantity;
	}

	
// ------------------------------ End Field: QuantityAdjustment_NewQuantity --------------------------------------


// ---------------------------- Start Field: QuantityAdjustment_QuantityDifference -------------------------------------- 

	/** 
	* Sets a value to `QuantityAdjustment_QuantityDifference` variable
	* @access public
	*/

	public function setQuantityadjustmentQuantitydifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityAdjustment_QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityAdjustment_QuantityDifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityAdjustment_QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityAdjustment_QuantityDifference` variable
	* @access public
	*/

	public function getQuantityadjustmentQuantitydifference() {
		return $this->QuantityAdjustment_QuantityDifference;
	}

	public function get_QuantityAdjustment_QuantityDifference() {
		return $this->QuantityAdjustment_QuantityDifference;
	}

	
// ------------------------------ End Field: QuantityAdjustment_QuantityDifference --------------------------------------


// ---------------------------- Start Field: ValueAdjustment_NewQuantity -------------------------------------- 

	/** 
	* Sets a value to `ValueAdjustment_NewQuantity` variable
	* @access public
	*/

	public function setValueadjustmentNewquantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_NewQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ValueAdjustment_NewQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_NewQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ValueAdjustment_NewQuantity` variable
	* @access public
	*/

	public function getValueadjustmentNewquantity() {
		return $this->ValueAdjustment_NewQuantity;
	}

	public function get_ValueAdjustment_NewQuantity() {
		return $this->ValueAdjustment_NewQuantity;
	}

	
// ------------------------------ End Field: ValueAdjustment_NewQuantity --------------------------------------


// ---------------------------- Start Field: ValueAdjustment_QuantityDifference -------------------------------------- 

	/** 
	* Sets a value to `ValueAdjustment_QuantityDifference` variable
	* @access public
	*/

	public function setValueadjustmentQuantitydifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ValueAdjustment_QuantityDifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_QuantityDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ValueAdjustment_QuantityDifference` variable
	* @access public
	*/

	public function getValueadjustmentQuantitydifference() {
		return $this->ValueAdjustment_QuantityDifference;
	}

	public function get_ValueAdjustment_QuantityDifference() {
		return $this->ValueAdjustment_QuantityDifference;
	}

	
// ------------------------------ End Field: ValueAdjustment_QuantityDifference --------------------------------------


// ---------------------------- Start Field: ValueAdjustment_NewValue -------------------------------------- 

	/** 
	* Sets a value to `ValueAdjustment_NewValue` variable
	* @access public
	*/

	public function setValueadjustmentNewvalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_NewValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ValueAdjustment_NewValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_NewValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ValueAdjustment_NewValue` variable
	* @access public
	*/

	public function getValueadjustmentNewvalue() {
		return $this->ValueAdjustment_NewValue;
	}

	public function get_ValueAdjustment_NewValue() {
		return $this->ValueAdjustment_NewValue;
	}

	
// ------------------------------ End Field: ValueAdjustment_NewValue --------------------------------------


// ---------------------------- Start Field: ValueAdjustment_ValueDifference -------------------------------------- 

	/** 
	* Sets a value to `ValueAdjustment_ValueDifference` variable
	* @access public
	*/

	public function setValueadjustmentValuedifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_ValueDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ValueAdjustment_ValueDifference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ValueAdjustment_ValueDifference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ValueAdjustment_ValueDifference` variable
	* @access public
	*/

	public function getValueadjustmentValuedifference() {
		return $this->ValueAdjustment_ValueDifference;
	}

	public function get_ValueAdjustment_ValueDifference() {
		return $this->ValueAdjustment_ValueDifference;
	}

	
// ------------------------------ End Field: ValueAdjustment_ValueDifference --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'InventoryAdjustment_TxnID' => (object) array(
										'Field'=>'InventoryAdjustment_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityDifference' => (object) array(
										'Field'=>'QuantityDifference',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'ValueDifference' => (object) array(
										'Field'=>'ValueDifference',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityAdjustment_NewQuantity' => (object) array(
										'Field'=>'QuantityAdjustment_NewQuantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityAdjustment_QuantityDifference' => (object) array(
										'Field'=>'QuantityAdjustment_QuantityDifference',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ValueAdjustment_NewQuantity' => (object) array(
										'Field'=>'ValueAdjustment_NewQuantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ValueAdjustment_QuantityDifference' => (object) array(
										'Field'=>'ValueAdjustment_QuantityDifference',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ValueAdjustment_NewValue' => (object) array(
										'Field'=>'ValueAdjustment_NewValue',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ValueAdjustment_ValueDifference' => (object) array(
										'Field'=>'ValueAdjustment_ValueDifference',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'InventoryAdjustment_TxnID' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `InventoryAdjustment_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'QuantityDifference' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityDifference` decimal(12,5) NULL   DEFAULT '0.00000';",
			'ValueDifference' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueDifference` decimal(10,2) NULL   ;",
			'QuantityAdjustment_NewQuantity' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityAdjustment_NewQuantity` decimal(12,5) NULL   ;",
			'QuantityAdjustment_QuantityDifference' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `QuantityAdjustment_QuantityDifference` decimal(12,5) NULL   ;",
			'ValueAdjustment_NewQuantity' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_NewQuantity` decimal(12,5) NULL   ;",
			'ValueAdjustment_QuantityDifference' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_QuantityDifference` decimal(12,5) NULL   ;",
			'ValueAdjustment_NewValue' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_NewValue` decimal(10,2) NULL   ;",
			'ValueAdjustment_ValueDifference' => "ALTER TABLE  `qb_inventoryadjustment_inventoryadjustmentline` ADD  `ValueAdjustment_ValueDifference` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setInventoryadjustmentTxnid() - InventoryAdjustment_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setQuantitydifference() - QuantityDifference
//setValuedifference() - ValueDifference
//setQuantityadjustmentNewquantity() - QuantityAdjustment_NewQuantity
//setQuantityadjustmentQuantitydifference() - QuantityAdjustment_QuantityDifference
//setValueadjustmentNewquantity() - ValueAdjustment_NewQuantity
//setValueadjustmentQuantitydifference() - ValueAdjustment_QuantityDifference
//setValueadjustmentNewvalue() - ValueAdjustment_NewValue
//setValueadjustmentValuedifference() - ValueAdjustment_ValueDifference

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_InventoryAdjustment_TxnID() - InventoryAdjustment_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_QuantityDifference() - QuantityDifference
//set_ValueDifference() - ValueDifference
//set_QuantityAdjustment_NewQuantity() - QuantityAdjustment_NewQuantity
//set_QuantityAdjustment_QuantityDifference() - QuantityAdjustment_QuantityDifference
//set_ValueAdjustment_NewQuantity() - ValueAdjustment_NewQuantity
//set_ValueAdjustment_QuantityDifference() - ValueAdjustment_QuantityDifference
//set_ValueAdjustment_NewValue() - ValueAdjustment_NewValue
//set_ValueAdjustment_ValueDifference() - ValueAdjustment_ValueDifference

*/
/* End of file Qb_inventoryadjustment_inventoryadjustmentline_model.php */
/* Location: ./application/models/Qb_inventoryadjustment_inventoryadjustmentline_model.php */
