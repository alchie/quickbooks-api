<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_vehiclemileage_model Class
 *
 * Manipulates `qb_vehiclemileage` table on database

CREATE TABLE `qb_vehiclemileage` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Vehicle_ListID` varchar(40) DEFAULT NULL,
  `Vehicle_FullName` varchar(255) DEFAULT NULL,
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `TripStartDate` date DEFAULT NULL,
  `TripEndDate` date DEFAULT NULL,
  `OdometerStart` decimal(12,5) DEFAULT '0.00000',
  `OdometerEnd` decimal(12,5) DEFAULT '0.00000',
  `TotalMiles` decimal(12,5) DEFAULT '0.00000',
  `Notes` text,
  `BillableStatus` varchar(40) DEFAULT NULL,
  `StandardMileageRate` decimal(12,5) DEFAULT NULL,
  `StandardMileageTotalAmount` decimal(10,2) DEFAULT NULL,
  `BillableRate` decimal(13,5) DEFAULT NULL,
  `BillableAmount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Vehicle_ListID` (`Vehicle_ListID`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_vehiclemileage` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_vehiclemileage` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Vehicle_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Vehicle_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `TripStartDate` date NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `TripEndDate` date NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `OdometerStart` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_vehiclemileage` ADD  `OdometerEnd` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_vehiclemileage` ADD  `TotalMiles` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_vehiclemileage` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `BillableStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `StandardMileageRate` decimal(12,5) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `StandardMileageTotalAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `BillableRate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_vehiclemileage` ADD  `BillableAmount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_vehiclemileage_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Vehicle_ListID;
	protected $Vehicle_FullName;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $TripStartDate;
	protected $TripEndDate;
	protected $OdometerStart;
	protected $OdometerEnd;
	protected $TotalMiles;
	protected $Notes;
	protected $BillableStatus;
	protected $StandardMileageRate;
	protected $StandardMileageTotalAmount;
	protected $BillableRate;
	protected $BillableAmount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_vehiclemileage';
		$this->_short_name = 'qb_vehiclemileage';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","Vehicle_ListID","Vehicle_FullName","Customer_ListID","Customer_FullName","Item_ListID","Item_FullName","Class_ListID","Class_FullName","TripStartDate","TripEndDate","OdometerStart","OdometerEnd","TotalMiles","Notes","BillableStatus","StandardMileageRate","StandardMileageTotalAmount","BillableRate","BillableAmount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Vehicle_ListID -------------------------------------- 

	/** 
	* Sets a value to `Vehicle_ListID` variable
	* @access public
	*/

	public function setVehicleListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vehicle_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vehicle_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vehicle_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vehicle_ListID` variable
	* @access public
	*/

	public function getVehicleListid() {
		return $this->Vehicle_ListID;
	}

	public function get_Vehicle_ListID() {
		return $this->Vehicle_ListID;
	}

	
// ------------------------------ End Field: Vehicle_ListID --------------------------------------


// ---------------------------- Start Field: Vehicle_FullName -------------------------------------- 

	/** 
	* Sets a value to `Vehicle_FullName` variable
	* @access public
	*/

	public function setVehicleFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vehicle_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vehicle_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vehicle_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vehicle_FullName` variable
	* @access public
	*/

	public function getVehicleFullname() {
		return $this->Vehicle_FullName;
	}

	public function get_Vehicle_FullName() {
		return $this->Vehicle_FullName;
	}

	
// ------------------------------ End Field: Vehicle_FullName --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: TripStartDate -------------------------------------- 

	/** 
	* Sets a value to `TripStartDate` variable
	* @access public
	*/

	public function setTripstartdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TripStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TripStartDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TripStartDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TripStartDate` variable
	* @access public
	*/

	public function getTripstartdate() {
		return $this->TripStartDate;
	}

	public function get_TripStartDate() {
		return $this->TripStartDate;
	}

	
// ------------------------------ End Field: TripStartDate --------------------------------------


// ---------------------------- Start Field: TripEndDate -------------------------------------- 

	/** 
	* Sets a value to `TripEndDate` variable
	* @access public
	*/

	public function setTripenddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TripEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TripEndDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TripEndDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TripEndDate` variable
	* @access public
	*/

	public function getTripenddate() {
		return $this->TripEndDate;
	}

	public function get_TripEndDate() {
		return $this->TripEndDate;
	}

	
// ------------------------------ End Field: TripEndDate --------------------------------------


// ---------------------------- Start Field: OdometerStart -------------------------------------- 

	/** 
	* Sets a value to `OdometerStart` variable
	* @access public
	*/

	public function setOdometerstart($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OdometerStart', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OdometerStart($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OdometerStart', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OdometerStart` variable
	* @access public
	*/

	public function getOdometerstart() {
		return $this->OdometerStart;
	}

	public function get_OdometerStart() {
		return $this->OdometerStart;
	}

	
// ------------------------------ End Field: OdometerStart --------------------------------------


// ---------------------------- Start Field: OdometerEnd -------------------------------------- 

	/** 
	* Sets a value to `OdometerEnd` variable
	* @access public
	*/

	public function setOdometerend($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OdometerEnd', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OdometerEnd($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OdometerEnd', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OdometerEnd` variable
	* @access public
	*/

	public function getOdometerend() {
		return $this->OdometerEnd;
	}

	public function get_OdometerEnd() {
		return $this->OdometerEnd;
	}

	
// ------------------------------ End Field: OdometerEnd --------------------------------------


// ---------------------------- Start Field: TotalMiles -------------------------------------- 

	/** 
	* Sets a value to `TotalMiles` variable
	* @access public
	*/

	public function setTotalmiles($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalMiles', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalMiles($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalMiles', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalMiles` variable
	* @access public
	*/

	public function getTotalmiles() {
		return $this->TotalMiles;
	}

	public function get_TotalMiles() {
		return $this->TotalMiles;
	}

	
// ------------------------------ End Field: TotalMiles --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: BillableStatus -------------------------------------- 

	/** 
	* Sets a value to `BillableStatus` variable
	* @access public
	*/

	public function setBillablestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableStatus` variable
	* @access public
	*/

	public function getBillablestatus() {
		return $this->BillableStatus;
	}

	public function get_BillableStatus() {
		return $this->BillableStatus;
	}

	
// ------------------------------ End Field: BillableStatus --------------------------------------


// ---------------------------- Start Field: StandardMileageRate -------------------------------------- 

	/** 
	* Sets a value to `StandardMileageRate` variable
	* @access public
	*/

	public function setStandardmileagerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('StandardMileageRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_StandardMileageRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('StandardMileageRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `StandardMileageRate` variable
	* @access public
	*/

	public function getStandardmileagerate() {
		return $this->StandardMileageRate;
	}

	public function get_StandardMileageRate() {
		return $this->StandardMileageRate;
	}

	
// ------------------------------ End Field: StandardMileageRate --------------------------------------


// ---------------------------- Start Field: StandardMileageTotalAmount -------------------------------------- 

	/** 
	* Sets a value to `StandardMileageTotalAmount` variable
	* @access public
	*/

	public function setStandardmileagetotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('StandardMileageTotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_StandardMileageTotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('StandardMileageTotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `StandardMileageTotalAmount` variable
	* @access public
	*/

	public function getStandardmileagetotalamount() {
		return $this->StandardMileageTotalAmount;
	}

	public function get_StandardMileageTotalAmount() {
		return $this->StandardMileageTotalAmount;
	}

	
// ------------------------------ End Field: StandardMileageTotalAmount --------------------------------------


// ---------------------------- Start Field: BillableRate -------------------------------------- 

	/** 
	* Sets a value to `BillableRate` variable
	* @access public
	*/

	public function setBillablerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableRate` variable
	* @access public
	*/

	public function getBillablerate() {
		return $this->BillableRate;
	}

	public function get_BillableRate() {
		return $this->BillableRate;
	}

	
// ------------------------------ End Field: BillableRate --------------------------------------


// ---------------------------- Start Field: BillableAmount -------------------------------------- 

	/** 
	* Sets a value to `BillableAmount` variable
	* @access public
	*/

	public function setBillableamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableAmount` variable
	* @access public
	*/

	public function getBillableamount() {
		return $this->BillableAmount;
	}

	public function get_BillableAmount() {
		return $this->BillableAmount;
	}

	
// ------------------------------ End Field: BillableAmount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Vehicle_ListID' => (object) array(
										'Field'=>'Vehicle_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Vehicle_FullName' => (object) array(
										'Field'=>'Vehicle_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TripStartDate' => (object) array(
										'Field'=>'TripStartDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TripEndDate' => (object) array(
										'Field'=>'TripEndDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OdometerStart' => (object) array(
										'Field'=>'OdometerStart',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'OdometerEnd' => (object) array(
										'Field'=>'OdometerEnd',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'TotalMiles' => (object) array(
										'Field'=>'TotalMiles',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableStatus' => (object) array(
										'Field'=>'BillableStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'StandardMileageRate' => (object) array(
										'Field'=>'StandardMileageRate',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'StandardMileageTotalAmount' => (object) array(
										'Field'=>'StandardMileageTotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableRate' => (object) array(
										'Field'=>'BillableRate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableAmount' => (object) array(
										'Field'=>'BillableAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_vehiclemileage` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_vehiclemileage` ADD  `EditSequence` text NULL   ;",
			'Vehicle_ListID' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Vehicle_ListID` varchar(40) NULL   ;",
			'Vehicle_FullName' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Vehicle_FullName` varchar(255) NULL   ;",
			'Customer_ListID' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Class_FullName` varchar(255) NULL   ;",
			'TripStartDate' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TripStartDate` date NULL   ;",
			'TripEndDate' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TripEndDate` date NULL   ;",
			'OdometerStart' => "ALTER TABLE  `qb_vehiclemileage` ADD  `OdometerStart` decimal(12,5) NULL   DEFAULT '0.00000';",
			'OdometerEnd' => "ALTER TABLE  `qb_vehiclemileage` ADD  `OdometerEnd` decimal(12,5) NULL   DEFAULT '0.00000';",
			'TotalMiles' => "ALTER TABLE  `qb_vehiclemileage` ADD  `TotalMiles` decimal(12,5) NULL   DEFAULT '0.00000';",
			'Notes' => "ALTER TABLE  `qb_vehiclemileage` ADD  `Notes` text NULL   ;",
			'BillableStatus' => "ALTER TABLE  `qb_vehiclemileage` ADD  `BillableStatus` varchar(40) NULL   ;",
			'StandardMileageRate' => "ALTER TABLE  `qb_vehiclemileage` ADD  `StandardMileageRate` decimal(12,5) NULL   ;",
			'StandardMileageTotalAmount' => "ALTER TABLE  `qb_vehiclemileage` ADD  `StandardMileageTotalAmount` decimal(10,2) NULL   ;",
			'BillableRate' => "ALTER TABLE  `qb_vehiclemileage` ADD  `BillableRate` decimal(13,5) NULL   ;",
			'BillableAmount' => "ALTER TABLE  `qb_vehiclemileage` ADD  `BillableAmount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setVehicleListid() - Vehicle_ListID
//setVehicleFullname() - Vehicle_FullName
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setTripstartdate() - TripStartDate
//setTripenddate() - TripEndDate
//setOdometerstart() - OdometerStart
//setOdometerend() - OdometerEnd
//setTotalmiles() - TotalMiles
//setNotes() - Notes
//setBillablestatus() - BillableStatus
//setStandardmileagerate() - StandardMileageRate
//setStandardmileagetotalamount() - StandardMileageTotalAmount
//setBillablerate() - BillableRate
//setBillableamount() - BillableAmount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Vehicle_ListID() - Vehicle_ListID
//set_Vehicle_FullName() - Vehicle_FullName
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_TripStartDate() - TripStartDate
//set_TripEndDate() - TripEndDate
//set_OdometerStart() - OdometerStart
//set_OdometerEnd() - OdometerEnd
//set_TotalMiles() - TotalMiles
//set_Notes() - Notes
//set_BillableStatus() - BillableStatus
//set_StandardMileageRate() - StandardMileageRate
//set_StandardMileageTotalAmount() - StandardMileageTotalAmount
//set_BillableRate() - BillableRate
//set_BillableAmount() - BillableAmount

*/
/* End of file Qb_vehiclemileage_model.php */
/* Location: ./application/models/Qb_vehiclemileage_model.php */
