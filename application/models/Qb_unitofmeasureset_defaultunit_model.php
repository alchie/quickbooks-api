<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_unitofmeasureset_defaultunit_model Class
 *
 * Manipulates `qb_unitofmeasureset_defaultunit` table on database

CREATE TABLE `qb_unitofmeasureset_defaultunit` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UnitOfMeasureSet_ListID` varchar(40) DEFAULT NULL,
  `UnitUsedFor` varchar(40) DEFAULT NULL,
  `Unit` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `UnitOfMeasureSet_ListID` (`UnitOfMeasureSet_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `UnitUsedFor` varchar(40) NULL   ;
ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `Unit` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_unitofmeasureset_defaultunit_model extends MY_Model {

	protected $qbxml_id;
	protected $UnitOfMeasureSet_ListID;
	protected $UnitUsedFor;
	protected $Unit;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_unitofmeasureset_defaultunit';
		$this->_short_name = 'qb_unitofmeasureset_defaultunit';
		$this->_fields = array("qbxml_id","UnitOfMeasureSet_ListID","UnitUsedFor","Unit");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function setUnitofmeasuresetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function getUnitofmeasuresetListid() {
		return $this->UnitOfMeasureSet_ListID;
	}

	public function get_UnitOfMeasureSet_ListID() {
		return $this->UnitOfMeasureSet_ListID;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_ListID --------------------------------------


// ---------------------------- Start Field: UnitUsedFor -------------------------------------- 

	/** 
	* Sets a value to `UnitUsedFor` variable
	* @access public
	*/

	public function setUnitusedfor($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitUsedFor', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitUsedFor($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitUsedFor', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitUsedFor` variable
	* @access public
	*/

	public function getUnitusedfor() {
		return $this->UnitUsedFor;
	}

	public function get_UnitUsedFor() {
		return $this->UnitUsedFor;
	}

	
// ------------------------------ End Field: UnitUsedFor --------------------------------------


// ---------------------------- Start Field: Unit -------------------------------------- 

	/** 
	* Sets a value to `Unit` variable
	* @access public
	*/

	public function setUnit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Unit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Unit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Unit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Unit` variable
	* @access public
	*/

	public function getUnit() {
		return $this->Unit;
	}

	public function get_Unit() {
		return $this->Unit;
	}

	
// ------------------------------ End Field: Unit --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'UnitOfMeasureSet_ListID' => (object) array(
										'Field'=>'UnitOfMeasureSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitUsedFor' => (object) array(
										'Field'=>'UnitUsedFor',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Unit' => (object) array(
										'Field'=>'Unit',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'UnitOfMeasureSet_ListID' => "ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;",
			'UnitUsedFor' => "ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `UnitUsedFor` varchar(40) NULL   ;",
			'Unit' => "ALTER TABLE  `qb_unitofmeasureset_defaultunit` ADD  `Unit` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setUnitofmeasuresetListid() - UnitOfMeasureSet_ListID
//setUnitusedfor() - UnitUsedFor
//setUnit() - Unit

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_UnitOfMeasureSet_ListID() - UnitOfMeasureSet_ListID
//set_UnitUsedFor() - UnitUsedFor
//set_Unit() - Unit

*/
/* End of file Qb_unitofmeasureset_defaultunit_model.php */
/* Location: ./application/models/Qb_unitofmeasureset_defaultunit_model.php */
