<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_unitofmeasureset_model Class
 *
 * Manipulates `qb_unitofmeasureset` table on database

CREATE TABLE `qb_unitofmeasureset` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `UnitOfMeasureType` varchar(40) DEFAULT NULL,
  `BaseUnit_Name` varchar(31) DEFAULT NULL,
  `BaseUnit_Abbreviation` varchar(31) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_unitofmeasureset` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_unitofmeasureset` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_unitofmeasureset` ADD  `UnitOfMeasureType` varchar(40) NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `BaseUnit_Name` varchar(31) NULL   ;
ALTER TABLE  `qb_unitofmeasureset` ADD  `BaseUnit_Abbreviation` varchar(31) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_unitofmeasureset_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $UnitOfMeasureType;
	protected $BaseUnit_Name;
	protected $BaseUnit_Abbreviation;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_unitofmeasureset';
		$this->_short_name = 'qb_unitofmeasureset';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","UnitOfMeasureType","BaseUnit_Name","BaseUnit_Abbreviation");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureType -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureType` variable
	* @access public
	*/

	public function setUnitofmeasuretype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureType` variable
	* @access public
	*/

	public function getUnitofmeasuretype() {
		return $this->UnitOfMeasureType;
	}

	public function get_UnitOfMeasureType() {
		return $this->UnitOfMeasureType;
	}

	
// ------------------------------ End Field: UnitOfMeasureType --------------------------------------


// ---------------------------- Start Field: BaseUnit_Name -------------------------------------- 

	/** 
	* Sets a value to `BaseUnit_Name` variable
	* @access public
	*/

	public function setBaseunitName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BaseUnit_Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BaseUnit_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BaseUnit_Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BaseUnit_Name` variable
	* @access public
	*/

	public function getBaseunitName() {
		return $this->BaseUnit_Name;
	}

	public function get_BaseUnit_Name() {
		return $this->BaseUnit_Name;
	}

	
// ------------------------------ End Field: BaseUnit_Name --------------------------------------


// ---------------------------- Start Field: BaseUnit_Abbreviation -------------------------------------- 

	/** 
	* Sets a value to `BaseUnit_Abbreviation` variable
	* @access public
	*/

	public function setBaseunitAbbreviation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BaseUnit_Abbreviation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BaseUnit_Abbreviation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BaseUnit_Abbreviation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BaseUnit_Abbreviation` variable
	* @access public
	*/

	public function getBaseunitAbbreviation() {
		return $this->BaseUnit_Abbreviation;
	}

	public function get_BaseUnit_Abbreviation() {
		return $this->BaseUnit_Abbreviation;
	}

	
// ------------------------------ End Field: BaseUnit_Abbreviation --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'UnitOfMeasureType' => (object) array(
										'Field'=>'UnitOfMeasureType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BaseUnit_Name' => (object) array(
										'Field'=>'BaseUnit_Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BaseUnit_Abbreviation' => (object) array(
										'Field'=>'BaseUnit_Abbreviation',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `Name` varchar(31) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'UnitOfMeasureType' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `UnitOfMeasureType` varchar(40) NULL   ;",
			'BaseUnit_Name' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `BaseUnit_Name` varchar(31) NULL   ;",
			'BaseUnit_Abbreviation' => "ALTER TABLE  `qb_unitofmeasureset` ADD  `BaseUnit_Abbreviation` varchar(31) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setUnitofmeasuretype() - UnitOfMeasureType
//setBaseunitName() - BaseUnit_Name
//setBaseunitAbbreviation() - BaseUnit_Abbreviation

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_UnitOfMeasureType() - UnitOfMeasureType
//set_BaseUnit_Name() - BaseUnit_Name
//set_BaseUnit_Abbreviation() - BaseUnit_Abbreviation

*/
/* End of file Qb_unitofmeasureset_model.php */
/* Location: ./application/models/Qb_unitofmeasureset_model.php */
