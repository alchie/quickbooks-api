<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_unitofmeasureset_relatedunit_model Class
 *
 * Manipulates `qb_unitofmeasureset_relatedunit` table on database

CREATE TABLE `qb_unitofmeasureset_relatedunit` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UnitOfMeasureSet_ListID` varchar(40) DEFAULT NULL,
  `Name` text,
  `Abbreviation` text,
  `ConversionRatio` decimal(13,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `UnitOfMeasureSet_ListID` (`UnitOfMeasureSet_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `Name` text NULL   ;
ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `Abbreviation` text NULL   ;
ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `ConversionRatio` decimal(13,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_unitofmeasureset_relatedunit_model extends MY_Model {

	protected $qbxml_id;
	protected $UnitOfMeasureSet_ListID;
	protected $Name;
	protected $Abbreviation;
	protected $ConversionRatio;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_unitofmeasureset_relatedunit';
		$this->_short_name = 'qb_unitofmeasureset_relatedunit';
		$this->_fields = array("qbxml_id","UnitOfMeasureSet_ListID","Name","Abbreviation","ConversionRatio");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function setUnitofmeasuresetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function getUnitofmeasuresetListid() {
		return $this->UnitOfMeasureSet_ListID;
	}

	public function get_UnitOfMeasureSet_ListID() {
		return $this->UnitOfMeasureSet_ListID;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_ListID --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: Abbreviation -------------------------------------- 

	/** 
	* Sets a value to `Abbreviation` variable
	* @access public
	*/

	public function setAbbreviation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Abbreviation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Abbreviation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Abbreviation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Abbreviation` variable
	* @access public
	*/

	public function getAbbreviation() {
		return $this->Abbreviation;
	}

	public function get_Abbreviation() {
		return $this->Abbreviation;
	}

	
// ------------------------------ End Field: Abbreviation --------------------------------------


// ---------------------------- Start Field: ConversionRatio -------------------------------------- 

	/** 
	* Sets a value to `ConversionRatio` variable
	* @access public
	*/

	public function setConversionratio($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ConversionRatio', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ConversionRatio($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ConversionRatio', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ConversionRatio` variable
	* @access public
	*/

	public function getConversionratio() {
		return $this->ConversionRatio;
	}

	public function get_ConversionRatio() {
		return $this->ConversionRatio;
	}

	
// ------------------------------ End Field: ConversionRatio --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'UnitOfMeasureSet_ListID' => (object) array(
										'Field'=>'UnitOfMeasureSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Abbreviation' => (object) array(
										'Field'=>'Abbreviation',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ConversionRatio' => (object) array(
										'Field'=>'ConversionRatio',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'UnitOfMeasureSet_ListID' => "ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;",
			'Name' => "ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `Name` text NULL   ;",
			'Abbreviation' => "ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `Abbreviation` text NULL   ;",
			'ConversionRatio' => "ALTER TABLE  `qb_unitofmeasureset_relatedunit` ADD  `ConversionRatio` decimal(13,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setUnitofmeasuresetListid() - UnitOfMeasureSet_ListID
//setName() - Name
//setAbbreviation() - Abbreviation
//setConversionratio() - ConversionRatio

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_UnitOfMeasureSet_ListID() - UnitOfMeasureSet_ListID
//set_Name() - Name
//set_Abbreviation() - Abbreviation
//set_ConversionRatio() - ConversionRatio

*/
/* End of file Qb_unitofmeasureset_relatedunit_model.php */
/* Location: ./application/models/Qb_unitofmeasureset_relatedunit_model.php */
