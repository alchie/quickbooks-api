<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_api_settings_model Class
 *
 * Manipulates `qb_api_settings` table on database

CREATE TABLE `qb_api_settings` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `section` varchar(50) NOT NULL,
  `skey` varchar(50) NOT NULL,
  `svalue` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_api_settings` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_api_settings` ADD  `section` varchar(50) NOT NULL   ;
ALTER TABLE  `qb_api_settings` ADD  `skey` varchar(50) NOT NULL   ;
ALTER TABLE  `qb_api_settings` ADD  `svalue` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_api_settings_model extends MY_Model {

	protected $id;
	protected $section;
	protected $skey;
	protected $svalue;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_api_settings';
		$this->_short_name = 'qb_api_settings';
		$this->_fields = array("id","section","skey","svalue");
		$this->_required = array("section","skey");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: section -------------------------------------- 

	/** 
	* Sets a value to `section` variable
	* @access public
	*/

	public function setSection($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('section', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_section($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('section', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `section` variable
	* @access public
	*/

	public function getSection() {
		return $this->section;
	}

	public function get_section() {
		return $this->section;
	}

	
// ------------------------------ End Field: section --------------------------------------


// ---------------------------- Start Field: skey -------------------------------------- 

	/** 
	* Sets a value to `skey` variable
	* @access public
	*/

	public function setSkey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('skey', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_skey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('skey', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `skey` variable
	* @access public
	*/

	public function getSkey() {
		return $this->skey;
	}

	public function get_skey() {
		return $this->skey;
	}

	
// ------------------------------ End Field: skey --------------------------------------


// ---------------------------- Start Field: svalue -------------------------------------- 

	/** 
	* Sets a value to `svalue` variable
	* @access public
	*/

	public function setSvalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('svalue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_svalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('svalue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `svalue` variable
	* @access public
	*/

	public function getSvalue() {
		return $this->svalue;
	}

	public function get_svalue() {
		return $this->svalue;
	}

	
// ------------------------------ End Field: svalue --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'section' => (object) array(
										'Field'=>'section',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'skey' => (object) array(
										'Field'=>'skey',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'svalue' => (object) array(
										'Field'=>'svalue',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `qb_api_settings` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'section' => "ALTER TABLE  `qb_api_settings` ADD  `section` varchar(50) NOT NULL   ;",
			'skey' => "ALTER TABLE  `qb_api_settings` ADD  `skey` varchar(50) NOT NULL   ;",
			'svalue' => "ALTER TABLE  `qb_api_settings` ADD  `svalue` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setSection() - section
//setSkey() - skey
//setSvalue() - svalue

--------------------------------------

//set_id() - id
//set_section() - section
//set_skey() - skey
//set_svalue() - svalue

*/
/* End of file Qb_api_settings_model.php */
/* Location: ./application/models/Qb_api_settings_model.php */
