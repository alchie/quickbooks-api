<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_check_linkedtxn_model Class
 *
 * Manipulates `qb_check_linkedtxn` table on database

CREATE TABLE `qb_check_linkedtxn` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FromTxnID` varchar(40) DEFAULT NULL,
  `Check_TxnID` varchar(40) DEFAULT NULL,
  `LinkType` text,
  `ToTxnID` varchar(40) DEFAULT NULL,
  `TxnType` varchar(40) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` text,
  `Amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `FromTxnID` (`FromTxnID`),
  KEY `Check_TxnID` (`Check_TxnID`),
  KEY `ToTxnID` (`ToTxnID`),
  KEY `TxnType` (`TxnType`),
  KEY `TxnDate` (`TxnDate`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_check_linkedtxn` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_check_linkedtxn` ADD  `FromTxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `Check_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `LinkType` text NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `ToTxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `TxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `RefNumber` text NULL   ;
ALTER TABLE  `qb_check_linkedtxn` ADD  `Amount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_check_linkedtxn_model extends MY_Model {

	protected $qbxml_id;
	protected $FromTxnID;
	protected $Check_TxnID;
	protected $LinkType;
	protected $ToTxnID;
	protected $TxnType;
	protected $TxnDate;
	protected $RefNumber;
	protected $Amount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_check_linkedtxn';
		$this->_short_name = 'qb_check_linkedtxn';
		$this->_fields = array("qbxml_id","FromTxnID","Check_TxnID","LinkType","ToTxnID","TxnType","TxnDate","RefNumber","Amount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: FromTxnID -------------------------------------- 

	/** 
	* Sets a value to `FromTxnID` variable
	* @access public
	*/

	public function setFromtxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FromTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FromTxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FromTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FromTxnID` variable
	* @access public
	*/

	public function getFromtxnid() {
		return $this->FromTxnID;
	}

	public function get_FromTxnID() {
		return $this->FromTxnID;
	}

	
// ------------------------------ End Field: FromTxnID --------------------------------------


// ---------------------------- Start Field: Check_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Check_TxnID` variable
	* @access public
	*/

	public function setCheckTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Check_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Check_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Check_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Check_TxnID` variable
	* @access public
	*/

	public function getCheckTxnid() {
		return $this->Check_TxnID;
	}

	public function get_Check_TxnID() {
		return $this->Check_TxnID;
	}

	
// ------------------------------ End Field: Check_TxnID --------------------------------------


// ---------------------------- Start Field: LinkType -------------------------------------- 

	/** 
	* Sets a value to `LinkType` variable
	* @access public
	*/

	public function setLinktype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LinkType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LinkType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LinkType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LinkType` variable
	* @access public
	*/

	public function getLinktype() {
		return $this->LinkType;
	}

	public function get_LinkType() {
		return $this->LinkType;
	}

	
// ------------------------------ End Field: LinkType --------------------------------------


// ---------------------------- Start Field: ToTxnID -------------------------------------- 

	/** 
	* Sets a value to `ToTxnID` variable
	* @access public
	*/

	public function setTotxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ToTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ToTxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ToTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ToTxnID` variable
	* @access public
	*/

	public function getTotxnid() {
		return $this->ToTxnID;
	}

	public function get_ToTxnID() {
		return $this->ToTxnID;
	}

	
// ------------------------------ End Field: ToTxnID --------------------------------------


// ---------------------------- Start Field: TxnType -------------------------------------- 

	/** 
	* Sets a value to `TxnType` variable
	* @access public
	*/

	public function setTxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnType` variable
	* @access public
	*/

	public function getTxntype() {
		return $this->TxnType;
	}

	public function get_TxnType() {
		return $this->TxnType;
	}

	
// ------------------------------ End Field: TxnType --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'FromTxnID' => (object) array(
										'Field'=>'FromTxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Check_TxnID' => (object) array(
										'Field'=>'Check_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'LinkType' => (object) array(
										'Field'=>'LinkType',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ToTxnID' => (object) array(
										'Field'=>'ToTxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnType' => (object) array(
										'Field'=>'TxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'FromTxnID' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `FromTxnID` varchar(40) NULL   ;",
			'Check_TxnID' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `Check_TxnID` varchar(40) NULL   ;",
			'LinkType' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `LinkType` text NULL   ;",
			'ToTxnID' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `ToTxnID` varchar(40) NULL   ;",
			'TxnType' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `TxnType` varchar(40) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `RefNumber` text NULL   ;",
			'Amount' => "ALTER TABLE  `qb_check_linkedtxn` ADD  `Amount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setFromtxnid() - FromTxnID
//setCheckTxnid() - Check_TxnID
//setLinktype() - LinkType
//setTotxnid() - ToTxnID
//setTxntype() - TxnType
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setAmount() - Amount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_FromTxnID() - FromTxnID
//set_Check_TxnID() - Check_TxnID
//set_LinkType() - LinkType
//set_ToTxnID() - ToTxnID
//set_TxnType() - TxnType
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_Amount() - Amount

*/
/* End of file Qb_check_linkedtxn_model.php */
/* Location: ./application/models/Qb_check_linkedtxn_model.php */
