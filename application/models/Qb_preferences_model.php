<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_preferences_model Class
 *
 * Manipulates `qb_preferences` table on database

CREATE TABLE `qb_preferences` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `AccountingPrefs_IsUsingAccountNumbers` tinyint(1) DEFAULT '0',
  `AccountingPrefs_IsRequiringAccounts` tinyint(1) DEFAULT '0',
  `AccountingPrefs_IsUsingClassTracking` tinyint(1) DEFAULT '0',
  `AccountingPrefs_IsUsingAuditTrail` tinyint(1) DEFAULT '0',
  `AccountingPrefs_IsAssigningJournalEntryNumbers` tinyint(1) DEFAULT '0',
  `AccountingPrefs_ClosingDate` date DEFAULT NULL,
  `FinanceChargePrefs_AnnualInterestRate` decimal(12,5) DEFAULT NULL,
  `FinanceChargePrefs_MinFinanceCharge` decimal(10,2) DEFAULT NULL,
  `FinanceChargePrefs_GracePeriod` int(10) unsigned DEFAULT '0',
  `FinanceChargePrefs_FinanceChargeAccount_ListID` varchar(40) DEFAULT NULL,
  `FinanceChargePrefs_FinanceChargeAccount_FullName` varchar(255) DEFAULT NULL,
  `FinanceChargePrefs_IsAssessingForOverdueCharges` tinyint(1) DEFAULT '0',
  `FinanceChargePrefs_CalculateChargesFrom` varchar(40) DEFAULT NULL,
  `FinanceChargePrefs_IsMarkedToBePrinted` tinyint(1) DEFAULT '0',
  `JobsAndEstimatesPrefs_IsUsingEstimates` tinyint(1) DEFAULT '0',
  `JobsAndEstimatesPrefs_IsUsingProgressInvoicing` tinyint(1) DEFAULT '0',
  `JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts` tinyint(1) DEFAULT '0',
  `MultiCurrencyPrefs_IsMultiCurrencyOn` tinyint(1) DEFAULT '0',
  `MultiCurrencyPrefs_HomeCurrency_ListID` varchar(40) DEFAULT NULL,
  `MultiCurrencyPrefs_HomeCurrency_FullName` varchar(255) DEFAULT NULL,
  `MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable` tinyint(1) DEFAULT '0',
  `MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled` tinyint(1) DEFAULT '0',
  `PurchasesAndVendorsPrefs_IsUsingInventory` tinyint(1) DEFAULT '0',
  `PurchasesAndVendorsPrefs_DaysBillsAreDue` int(10) unsigned DEFAULT '0',
  `PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts` tinyint(1) DEFAULT '0',
  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` varchar(40) DEFAULT NULL,
  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName` varchar(255) DEFAULT NULL,
  `ReportsPrefs_AgingReportBasis` varchar(40) DEFAULT NULL,
  `ReportsPrefs_SummaryReportBasis` varchar(40) DEFAULT NULL,
  `SalesAndCustomersPrefs_DefaultShipMethod_ListID` varchar(40) DEFAULT NULL,
  `SalesAndCustomersPrefs_DefaultShipMethod_FullName` varchar(255) DEFAULT NULL,
  `SalesAndCustomersPrefs_DefaultFOB` text,
  `SalesAndCustomersPrefs_DefaultMarkup` decimal(12,5) DEFAULT NULL,
  `SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome` tinyint(1) DEFAULT '0',
  `SalesAndCustomersPrefs_IsAutoApplyingPayments` tinyint(1) DEFAULT '0',
  `SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels` tinyint(1) DEFAULT '0',
  `SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp` tinyint(1) DEFAULT '0',
  `SalesTaxPrefs_DefaultItemSalesTax_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxPrefs_DefaultItemSalesTax_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxPrefs_PaySalesTax` varchar(40) DEFAULT NULL,
  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxPrefs_IsUsingVendorTaxCode` tinyint(1) DEFAULT '0',
  `SalesTaxPrefs_IsUsingCustomerTaxCode` tinyint(1) DEFAULT '0',
  `SalesTaxPrefs_IsUsingAmountsIncludeTax` tinyint(1) DEFAULT '0',
  `TimeTrackingPrefs_FirstDayOfWeek` varchar(40) DEFAULT NULL,
  `CurrentAppAccessRights_IsAutomaticLoginAllowed` tinyint(1) DEFAULT '0',
  `CurrentAppAccessRights_AutomaticLoginUserName` text,
  `CurrentAppAccessRights_IsPersonalDataAccessAllowed` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`qbxml_id`),
  KEY `FinanceChargePrefs_FinanceChargeAccount_ListID` (`FinanceChargePrefs_FinanceChargeAccount_ListID`),
  KEY `MultiCurrencyPrefs_HomeCurrency_ListID` (`MultiCurrencyPrefs_HomeCurrency_ListID`),
  KEY `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` (`PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID`),
  KEY `SalesAndCustomersPrefs_DefaultShipMethod_ListID` (`SalesAndCustomersPrefs_DefaultShipMethod_ListID`),
  KEY `SalesTaxPrefs_DefaultItemSalesTax_ListID` (`SalesTaxPrefs_DefaultItemSalesTax_ListID`),
  KEY `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` (`SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID`),
  KEY `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` (`SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_preferences` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingAccountNumbers` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsRequiringAccounts` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingClassTracking` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingAuditTrail` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsAssigningJournalEntryNumbers` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_ClosingDate` date NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_AnnualInterestRate` decimal(12,5) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_MinFinanceCharge` decimal(10,2) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_GracePeriod` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_FinanceChargeAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_FinanceChargeAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_IsAssessingForOverdueCharges` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_CalculateChargesFrom` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_IsMarkedToBePrinted` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsUsingEstimates` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsUsingProgressInvoicing` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_IsMultiCurrencyOn` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_HomeCurrency_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_HomeCurrency_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_IsUsingInventory` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DaysBillsAreDue` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `ReportsPrefs_AgingReportBasis` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `ReportsPrefs_SummaryReportBasis` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultShipMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultShipMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultFOB` text NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultMarkup` decimal(12,5) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_IsAutoApplyingPayments` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultItemSalesTax_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultItemSalesTax_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_PaySalesTax` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingVendorTaxCode` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingCustomerTaxCode` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingAmountsIncludeTax` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `TimeTrackingPrefs_FirstDayOfWeek` varchar(40) NULL   ;
ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_IsAutomaticLoginAllowed` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_AutomaticLoginUserName` text NULL   ;
ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_IsPersonalDataAccessAllowed` tinyint(1) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_preferences_model extends MY_Model {

	protected $qbxml_id;
	protected $AccountingPrefs_IsUsingAccountNumbers;
	protected $AccountingPrefs_IsRequiringAccounts;
	protected $AccountingPrefs_IsUsingClassTracking;
	protected $AccountingPrefs_IsUsingAuditTrail;
	protected $AccountingPrefs_IsAssigningJournalEntryNumbers;
	protected $AccountingPrefs_ClosingDate;
	protected $FinanceChargePrefs_AnnualInterestRate;
	protected $FinanceChargePrefs_MinFinanceCharge;
	protected $FinanceChargePrefs_GracePeriod;
	protected $FinanceChargePrefs_FinanceChargeAccount_ListID;
	protected $FinanceChargePrefs_FinanceChargeAccount_FullName;
	protected $FinanceChargePrefs_IsAssessingForOverdueCharges;
	protected $FinanceChargePrefs_CalculateChargesFrom;
	protected $FinanceChargePrefs_IsMarkedToBePrinted;
	protected $JobsAndEstimatesPrefs_IsUsingEstimates;
	protected $JobsAndEstimatesPrefs_IsUsingProgressInvoicing;
	protected $JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts;
	protected $MultiCurrencyPrefs_IsMultiCurrencyOn;
	protected $MultiCurrencyPrefs_HomeCurrency_ListID;
	protected $MultiCurrencyPrefs_HomeCurrency_FullName;
	protected $MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable;
	protected $MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled;
	protected $PurchasesAndVendorsPrefs_IsUsingInventory;
	protected $PurchasesAndVendorsPrefs_DaysBillsAreDue;
	protected $PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts;
	protected $PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID;
	protected $PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName;
	protected $ReportsPrefs_AgingReportBasis;
	protected $ReportsPrefs_SummaryReportBasis;
	protected $SalesAndCustomersPrefs_DefaultShipMethod_ListID;
	protected $SalesAndCustomersPrefs_DefaultShipMethod_FullName;
	protected $SalesAndCustomersPrefs_DefaultFOB;
	protected $SalesAndCustomersPrefs_DefaultMarkup;
	protected $SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome;
	protected $SalesAndCustomersPrefs_IsAutoApplyingPayments;
	protected $SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels;
	protected $SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp;
	protected $SalesTaxPrefs_DefaultItemSalesTax_ListID;
	protected $SalesTaxPrefs_DefaultItemSalesTax_FullName;
	protected $SalesTaxPrefs_PaySalesTax;
	protected $SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID;
	protected $SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName;
	protected $SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID;
	protected $SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName;
	protected $SalesTaxPrefs_IsUsingVendorTaxCode;
	protected $SalesTaxPrefs_IsUsingCustomerTaxCode;
	protected $SalesTaxPrefs_IsUsingAmountsIncludeTax;
	protected $TimeTrackingPrefs_FirstDayOfWeek;
	protected $CurrentAppAccessRights_IsAutomaticLoginAllowed;
	protected $CurrentAppAccessRights_AutomaticLoginUserName;
	protected $CurrentAppAccessRights_IsPersonalDataAccessAllowed;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_preferences';
		$this->_short_name = 'qb_preferences';
		$this->_fields = array("qbxml_id","AccountingPrefs_IsUsingAccountNumbers","AccountingPrefs_IsRequiringAccounts","AccountingPrefs_IsUsingClassTracking","AccountingPrefs_IsUsingAuditTrail","AccountingPrefs_IsAssigningJournalEntryNumbers","AccountingPrefs_ClosingDate","FinanceChargePrefs_AnnualInterestRate","FinanceChargePrefs_MinFinanceCharge","FinanceChargePrefs_GracePeriod","FinanceChargePrefs_FinanceChargeAccount_ListID","FinanceChargePrefs_FinanceChargeAccount_FullName","FinanceChargePrefs_IsAssessingForOverdueCharges","FinanceChargePrefs_CalculateChargesFrom","FinanceChargePrefs_IsMarkedToBePrinted","JobsAndEstimatesPrefs_IsUsingEstimates","JobsAndEstimatesPrefs_IsUsingProgressInvoicing","JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts","MultiCurrencyPrefs_IsMultiCurrencyOn","MultiCurrencyPrefs_HomeCurrency_ListID","MultiCurrencyPrefs_HomeCurrency_FullName","MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable","MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled","PurchasesAndVendorsPrefs_IsUsingInventory","PurchasesAndVendorsPrefs_DaysBillsAreDue","PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts","PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID","PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName","ReportsPrefs_AgingReportBasis","ReportsPrefs_SummaryReportBasis","SalesAndCustomersPrefs_DefaultShipMethod_ListID","SalesAndCustomersPrefs_DefaultShipMethod_FullName","SalesAndCustomersPrefs_DefaultFOB","SalesAndCustomersPrefs_DefaultMarkup","SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome","SalesAndCustomersPrefs_IsAutoApplyingPayments","SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels","SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp","SalesTaxPrefs_DefaultItemSalesTax_ListID","SalesTaxPrefs_DefaultItemSalesTax_FullName","SalesTaxPrefs_PaySalesTax","SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID","SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName","SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID","SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName","SalesTaxPrefs_IsUsingVendorTaxCode","SalesTaxPrefs_IsUsingCustomerTaxCode","SalesTaxPrefs_IsUsingAmountsIncludeTax","TimeTrackingPrefs_FirstDayOfWeek","CurrentAppAccessRights_IsAutomaticLoginAllowed","CurrentAppAccessRights_AutomaticLoginUserName","CurrentAppAccessRights_IsPersonalDataAccessAllowed");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_IsUsingAccountNumbers -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_IsUsingAccountNumbers` variable
	* @access public
	*/

	public function setAccountingprefsIsusingaccountnumbers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingAccountNumbers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_IsUsingAccountNumbers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingAccountNumbers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_IsUsingAccountNumbers` variable
	* @access public
	*/

	public function getAccountingprefsIsusingaccountnumbers() {
		return $this->AccountingPrefs_IsUsingAccountNumbers;
	}

	public function get_AccountingPrefs_IsUsingAccountNumbers() {
		return $this->AccountingPrefs_IsUsingAccountNumbers;
	}

	
// ------------------------------ End Field: AccountingPrefs_IsUsingAccountNumbers --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_IsRequiringAccounts -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_IsRequiringAccounts` variable
	* @access public
	*/

	public function setAccountingprefsIsrequiringaccounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsRequiringAccounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_IsRequiringAccounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsRequiringAccounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_IsRequiringAccounts` variable
	* @access public
	*/

	public function getAccountingprefsIsrequiringaccounts() {
		return $this->AccountingPrefs_IsRequiringAccounts;
	}

	public function get_AccountingPrefs_IsRequiringAccounts() {
		return $this->AccountingPrefs_IsRequiringAccounts;
	}

	
// ------------------------------ End Field: AccountingPrefs_IsRequiringAccounts --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_IsUsingClassTracking -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_IsUsingClassTracking` variable
	* @access public
	*/

	public function setAccountingprefsIsusingclasstracking($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingClassTracking', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_IsUsingClassTracking($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingClassTracking', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_IsUsingClassTracking` variable
	* @access public
	*/

	public function getAccountingprefsIsusingclasstracking() {
		return $this->AccountingPrefs_IsUsingClassTracking;
	}

	public function get_AccountingPrefs_IsUsingClassTracking() {
		return $this->AccountingPrefs_IsUsingClassTracking;
	}

	
// ------------------------------ End Field: AccountingPrefs_IsUsingClassTracking --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_IsUsingAuditTrail -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_IsUsingAuditTrail` variable
	* @access public
	*/

	public function setAccountingprefsIsusingaudittrail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingAuditTrail', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_IsUsingAuditTrail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsUsingAuditTrail', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_IsUsingAuditTrail` variable
	* @access public
	*/

	public function getAccountingprefsIsusingaudittrail() {
		return $this->AccountingPrefs_IsUsingAuditTrail;
	}

	public function get_AccountingPrefs_IsUsingAuditTrail() {
		return $this->AccountingPrefs_IsUsingAuditTrail;
	}

	
// ------------------------------ End Field: AccountingPrefs_IsUsingAuditTrail --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_IsAssigningJournalEntryNumbers -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_IsAssigningJournalEntryNumbers` variable
	* @access public
	*/

	public function setAccountingprefsIsassigningjournalentrynumbers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsAssigningJournalEntryNumbers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_IsAssigningJournalEntryNumbers($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_IsAssigningJournalEntryNumbers', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_IsAssigningJournalEntryNumbers` variable
	* @access public
	*/

	public function getAccountingprefsIsassigningjournalentrynumbers() {
		return $this->AccountingPrefs_IsAssigningJournalEntryNumbers;
	}

	public function get_AccountingPrefs_IsAssigningJournalEntryNumbers() {
		return $this->AccountingPrefs_IsAssigningJournalEntryNumbers;
	}

	
// ------------------------------ End Field: AccountingPrefs_IsAssigningJournalEntryNumbers --------------------------------------


// ---------------------------- Start Field: AccountingPrefs_ClosingDate -------------------------------------- 

	/** 
	* Sets a value to `AccountingPrefs_ClosingDate` variable
	* @access public
	*/

	public function setAccountingprefsClosingdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_ClosingDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountingPrefs_ClosingDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountingPrefs_ClosingDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountingPrefs_ClosingDate` variable
	* @access public
	*/

	public function getAccountingprefsClosingdate() {
		return $this->AccountingPrefs_ClosingDate;
	}

	public function get_AccountingPrefs_ClosingDate() {
		return $this->AccountingPrefs_ClosingDate;
	}

	
// ------------------------------ End Field: AccountingPrefs_ClosingDate --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_AnnualInterestRate -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_AnnualInterestRate` variable
	* @access public
	*/

	public function setFinancechargeprefsAnnualinterestrate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_AnnualInterestRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_AnnualInterestRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_AnnualInterestRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_AnnualInterestRate` variable
	* @access public
	*/

	public function getFinancechargeprefsAnnualinterestrate() {
		return $this->FinanceChargePrefs_AnnualInterestRate;
	}

	public function get_FinanceChargePrefs_AnnualInterestRate() {
		return $this->FinanceChargePrefs_AnnualInterestRate;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_AnnualInterestRate --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_MinFinanceCharge -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_MinFinanceCharge` variable
	* @access public
	*/

	public function setFinancechargeprefsMinfinancecharge($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_MinFinanceCharge', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_MinFinanceCharge($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_MinFinanceCharge', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_MinFinanceCharge` variable
	* @access public
	*/

	public function getFinancechargeprefsMinfinancecharge() {
		return $this->FinanceChargePrefs_MinFinanceCharge;
	}

	public function get_FinanceChargePrefs_MinFinanceCharge() {
		return $this->FinanceChargePrefs_MinFinanceCharge;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_MinFinanceCharge --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_GracePeriod -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_GracePeriod` variable
	* @access public
	*/

	public function setFinancechargeprefsGraceperiod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_GracePeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_GracePeriod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_GracePeriod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_GracePeriod` variable
	* @access public
	*/

	public function getFinancechargeprefsGraceperiod() {
		return $this->FinanceChargePrefs_GracePeriod;
	}

	public function get_FinanceChargePrefs_GracePeriod() {
		return $this->FinanceChargePrefs_GracePeriod;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_GracePeriod --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_FinanceChargeAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_FinanceChargeAccount_ListID` variable
	* @access public
	*/

	public function setFinancechargeprefsFinancechargeaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_FinanceChargeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_FinanceChargeAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_FinanceChargeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_FinanceChargeAccount_ListID` variable
	* @access public
	*/

	public function getFinancechargeprefsFinancechargeaccountListid() {
		return $this->FinanceChargePrefs_FinanceChargeAccount_ListID;
	}

	public function get_FinanceChargePrefs_FinanceChargeAccount_ListID() {
		return $this->FinanceChargePrefs_FinanceChargeAccount_ListID;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_FinanceChargeAccount_ListID --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_FinanceChargeAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_FinanceChargeAccount_FullName` variable
	* @access public
	*/

	public function setFinancechargeprefsFinancechargeaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_FinanceChargeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_FinanceChargeAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_FinanceChargeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_FinanceChargeAccount_FullName` variable
	* @access public
	*/

	public function getFinancechargeprefsFinancechargeaccountFullname() {
		return $this->FinanceChargePrefs_FinanceChargeAccount_FullName;
	}

	public function get_FinanceChargePrefs_FinanceChargeAccount_FullName() {
		return $this->FinanceChargePrefs_FinanceChargeAccount_FullName;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_FinanceChargeAccount_FullName --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_IsAssessingForOverdueCharges -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_IsAssessingForOverdueCharges` variable
	* @access public
	*/

	public function setFinancechargeprefsIsassessingforoverduecharges($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_IsAssessingForOverdueCharges', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_IsAssessingForOverdueCharges($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_IsAssessingForOverdueCharges', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_IsAssessingForOverdueCharges` variable
	* @access public
	*/

	public function getFinancechargeprefsIsassessingforoverduecharges() {
		return $this->FinanceChargePrefs_IsAssessingForOverdueCharges;
	}

	public function get_FinanceChargePrefs_IsAssessingForOverdueCharges() {
		return $this->FinanceChargePrefs_IsAssessingForOverdueCharges;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_IsAssessingForOverdueCharges --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_CalculateChargesFrom -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_CalculateChargesFrom` variable
	* @access public
	*/

	public function setFinancechargeprefsCalculatechargesfrom($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_CalculateChargesFrom', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_CalculateChargesFrom($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_CalculateChargesFrom', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_CalculateChargesFrom` variable
	* @access public
	*/

	public function getFinancechargeprefsCalculatechargesfrom() {
		return $this->FinanceChargePrefs_CalculateChargesFrom;
	}

	public function get_FinanceChargePrefs_CalculateChargesFrom() {
		return $this->FinanceChargePrefs_CalculateChargesFrom;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_CalculateChargesFrom --------------------------------------


// ---------------------------- Start Field: FinanceChargePrefs_IsMarkedToBePrinted -------------------------------------- 

	/** 
	* Sets a value to `FinanceChargePrefs_IsMarkedToBePrinted` variable
	* @access public
	*/

	public function setFinancechargeprefsIsmarkedtobeprinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_IsMarkedToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FinanceChargePrefs_IsMarkedToBePrinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FinanceChargePrefs_IsMarkedToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FinanceChargePrefs_IsMarkedToBePrinted` variable
	* @access public
	*/

	public function getFinancechargeprefsIsmarkedtobeprinted() {
		return $this->FinanceChargePrefs_IsMarkedToBePrinted;
	}

	public function get_FinanceChargePrefs_IsMarkedToBePrinted() {
		return $this->FinanceChargePrefs_IsMarkedToBePrinted;
	}

	
// ------------------------------ End Field: FinanceChargePrefs_IsMarkedToBePrinted --------------------------------------


// ---------------------------- Start Field: JobsAndEstimatesPrefs_IsUsingEstimates -------------------------------------- 

	/** 
	* Sets a value to `JobsAndEstimatesPrefs_IsUsingEstimates` variable
	* @access public
	*/

	public function setJobsandestimatesprefsIsusingestimates($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsUsingEstimates', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobsAndEstimatesPrefs_IsUsingEstimates($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsUsingEstimates', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobsAndEstimatesPrefs_IsUsingEstimates` variable
	* @access public
	*/

	public function getJobsandestimatesprefsIsusingestimates() {
		return $this->JobsAndEstimatesPrefs_IsUsingEstimates;
	}

	public function get_JobsAndEstimatesPrefs_IsUsingEstimates() {
		return $this->JobsAndEstimatesPrefs_IsUsingEstimates;
	}

	
// ------------------------------ End Field: JobsAndEstimatesPrefs_IsUsingEstimates --------------------------------------


// ---------------------------- Start Field: JobsAndEstimatesPrefs_IsUsingProgressInvoicing -------------------------------------- 

	/** 
	* Sets a value to `JobsAndEstimatesPrefs_IsUsingProgressInvoicing` variable
	* @access public
	*/

	public function setJobsandestimatesprefsIsusingprogressinvoicing($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsUsingProgressInvoicing', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobsAndEstimatesPrefs_IsUsingProgressInvoicing($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsUsingProgressInvoicing', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobsAndEstimatesPrefs_IsUsingProgressInvoicing` variable
	* @access public
	*/

	public function getJobsandestimatesprefsIsusingprogressinvoicing() {
		return $this->JobsAndEstimatesPrefs_IsUsingProgressInvoicing;
	}

	public function get_JobsAndEstimatesPrefs_IsUsingProgressInvoicing() {
		return $this->JobsAndEstimatesPrefs_IsUsingProgressInvoicing;
	}

	
// ------------------------------ End Field: JobsAndEstimatesPrefs_IsUsingProgressInvoicing --------------------------------------


// ---------------------------- Start Field: JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts -------------------------------------- 

	/** 
	* Sets a value to `JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts` variable
	* @access public
	*/

	public function setJobsandestimatesprefsIsprintingitemswithzeroamounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts` variable
	* @access public
	*/

	public function getJobsandestimatesprefsIsprintingitemswithzeroamounts() {
		return $this->JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts;
	}

	public function get_JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts() {
		return $this->JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts;
	}

	
// ------------------------------ End Field: JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts --------------------------------------


// ---------------------------- Start Field: MultiCurrencyPrefs_IsMultiCurrencyOn -------------------------------------- 

	/** 
	* Sets a value to `MultiCurrencyPrefs_IsMultiCurrencyOn` variable
	* @access public
	*/

	public function setMulticurrencyprefsIsmulticurrencyon($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_IsMultiCurrencyOn', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MultiCurrencyPrefs_IsMultiCurrencyOn($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_IsMultiCurrencyOn', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MultiCurrencyPrefs_IsMultiCurrencyOn` variable
	* @access public
	*/

	public function getMulticurrencyprefsIsmulticurrencyon() {
		return $this->MultiCurrencyPrefs_IsMultiCurrencyOn;
	}

	public function get_MultiCurrencyPrefs_IsMultiCurrencyOn() {
		return $this->MultiCurrencyPrefs_IsMultiCurrencyOn;
	}

	
// ------------------------------ End Field: MultiCurrencyPrefs_IsMultiCurrencyOn --------------------------------------


// ---------------------------- Start Field: MultiCurrencyPrefs_HomeCurrency_ListID -------------------------------------- 

	/** 
	* Sets a value to `MultiCurrencyPrefs_HomeCurrency_ListID` variable
	* @access public
	*/

	public function setMulticurrencyprefsHomecurrencyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_HomeCurrency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MultiCurrencyPrefs_HomeCurrency_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_HomeCurrency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MultiCurrencyPrefs_HomeCurrency_ListID` variable
	* @access public
	*/

	public function getMulticurrencyprefsHomecurrencyListid() {
		return $this->MultiCurrencyPrefs_HomeCurrency_ListID;
	}

	public function get_MultiCurrencyPrefs_HomeCurrency_ListID() {
		return $this->MultiCurrencyPrefs_HomeCurrency_ListID;
	}

	
// ------------------------------ End Field: MultiCurrencyPrefs_HomeCurrency_ListID --------------------------------------


// ---------------------------- Start Field: MultiCurrencyPrefs_HomeCurrency_FullName -------------------------------------- 

	/** 
	* Sets a value to `MultiCurrencyPrefs_HomeCurrency_FullName` variable
	* @access public
	*/

	public function setMulticurrencyprefsHomecurrencyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_HomeCurrency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MultiCurrencyPrefs_HomeCurrency_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiCurrencyPrefs_HomeCurrency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MultiCurrencyPrefs_HomeCurrency_FullName` variable
	* @access public
	*/

	public function getMulticurrencyprefsHomecurrencyFullname() {
		return $this->MultiCurrencyPrefs_HomeCurrency_FullName;
	}

	public function get_MultiCurrencyPrefs_HomeCurrency_FullName() {
		return $this->MultiCurrencyPrefs_HomeCurrency_FullName;
	}

	
// ------------------------------ End Field: MultiCurrencyPrefs_HomeCurrency_FullName --------------------------------------


// ---------------------------- Start Field: MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable -------------------------------------- 

	/** 
	* Sets a value to `MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable` variable
	* @access public
	*/

	public function setMultilocationinventoryprefsIsmultilocationinventoryavailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable` variable
	* @access public
	*/

	public function getMultilocationinventoryprefsIsmultilocationinventoryavailable() {
		return $this->MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable;
	}

	public function get_MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable() {
		return $this->MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable;
	}

	
// ------------------------------ End Field: MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable --------------------------------------


// ---------------------------- Start Field: MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled -------------------------------------- 

	/** 
	* Sets a value to `MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled` variable
	* @access public
	*/

	public function setMultilocationinventoryprefsIsmultilocationinventoryenabled($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled` variable
	* @access public
	*/

	public function getMultilocationinventoryprefsIsmultilocationinventoryenabled() {
		return $this->MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled;
	}

	public function get_MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled() {
		return $this->MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled;
	}

	
// ------------------------------ End Field: MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled --------------------------------------


// ---------------------------- Start Field: PurchasesAndVendorsPrefs_IsUsingInventory -------------------------------------- 

	/** 
	* Sets a value to `PurchasesAndVendorsPrefs_IsUsingInventory` variable
	* @access public
	*/

	public function setPurchasesandvendorsprefsIsusinginventory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_IsUsingInventory', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchasesAndVendorsPrefs_IsUsingInventory($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_IsUsingInventory', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchasesAndVendorsPrefs_IsUsingInventory` variable
	* @access public
	*/

	public function getPurchasesandvendorsprefsIsusinginventory() {
		return $this->PurchasesAndVendorsPrefs_IsUsingInventory;
	}

	public function get_PurchasesAndVendorsPrefs_IsUsingInventory() {
		return $this->PurchasesAndVendorsPrefs_IsUsingInventory;
	}

	
// ------------------------------ End Field: PurchasesAndVendorsPrefs_IsUsingInventory --------------------------------------


// ---------------------------- Start Field: PurchasesAndVendorsPrefs_DaysBillsAreDue -------------------------------------- 

	/** 
	* Sets a value to `PurchasesAndVendorsPrefs_DaysBillsAreDue` variable
	* @access public
	*/

	public function setPurchasesandvendorsprefsDaysbillsaredue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DaysBillsAreDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchasesAndVendorsPrefs_DaysBillsAreDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DaysBillsAreDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchasesAndVendorsPrefs_DaysBillsAreDue` variable
	* @access public
	*/

	public function getPurchasesandvendorsprefsDaysbillsaredue() {
		return $this->PurchasesAndVendorsPrefs_DaysBillsAreDue;
	}

	public function get_PurchasesAndVendorsPrefs_DaysBillsAreDue() {
		return $this->PurchasesAndVendorsPrefs_DaysBillsAreDue;
	}

	
// ------------------------------ End Field: PurchasesAndVendorsPrefs_DaysBillsAreDue --------------------------------------


// ---------------------------- Start Field: PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts -------------------------------------- 

	/** 
	* Sets a value to `PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts` variable
	* @access public
	*/

	public function setPurchasesandvendorsprefsIsautomaticallyusingdiscounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts` variable
	* @access public
	*/

	public function getPurchasesandvendorsprefsIsautomaticallyusingdiscounts() {
		return $this->PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts;
	}

	public function get_PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts() {
		return $this->PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts;
	}

	
// ------------------------------ End Field: PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts --------------------------------------


// ---------------------------- Start Field: PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` variable
	* @access public
	*/

	public function setPurchasesandvendorsprefsDefaultdiscountaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` variable
	* @access public
	*/

	public function getPurchasesandvendorsprefsDefaultdiscountaccountListid() {
		return $this->PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID;
	}

	public function get_PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID() {
		return $this->PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID;
	}

	
// ------------------------------ End Field: PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID --------------------------------------


// ---------------------------- Start Field: PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName` variable
	* @access public
	*/

	public function setPurchasesandvendorsprefsDefaultdiscountaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName` variable
	* @access public
	*/

	public function getPurchasesandvendorsprefsDefaultdiscountaccountFullname() {
		return $this->PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName;
	}

	public function get_PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName() {
		return $this->PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName;
	}

	
// ------------------------------ End Field: PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName --------------------------------------


// ---------------------------- Start Field: ReportsPrefs_AgingReportBasis -------------------------------------- 

	/** 
	* Sets a value to `ReportsPrefs_AgingReportBasis` variable
	* @access public
	*/

	public function setReportsprefsAgingreportbasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReportsPrefs_AgingReportBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ReportsPrefs_AgingReportBasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReportsPrefs_AgingReportBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ReportsPrefs_AgingReportBasis` variable
	* @access public
	*/

	public function getReportsprefsAgingreportbasis() {
		return $this->ReportsPrefs_AgingReportBasis;
	}

	public function get_ReportsPrefs_AgingReportBasis() {
		return $this->ReportsPrefs_AgingReportBasis;
	}

	
// ------------------------------ End Field: ReportsPrefs_AgingReportBasis --------------------------------------


// ---------------------------- Start Field: ReportsPrefs_SummaryReportBasis -------------------------------------- 

	/** 
	* Sets a value to `ReportsPrefs_SummaryReportBasis` variable
	* @access public
	*/

	public function setReportsprefsSummaryreportbasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReportsPrefs_SummaryReportBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ReportsPrefs_SummaryReportBasis($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReportsPrefs_SummaryReportBasis', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ReportsPrefs_SummaryReportBasis` variable
	* @access public
	*/

	public function getReportsprefsSummaryreportbasis() {
		return $this->ReportsPrefs_SummaryReportBasis;
	}

	public function get_ReportsPrefs_SummaryReportBasis() {
		return $this->ReportsPrefs_SummaryReportBasis;
	}

	
// ------------------------------ End Field: ReportsPrefs_SummaryReportBasis --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_DefaultShipMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_DefaultShipMethod_ListID` variable
	* @access public
	*/

	public function setSalesandcustomersprefsDefaultshipmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_DefaultShipMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_DefaultShipMethod_ListID` variable
	* @access public
	*/

	public function getSalesandcustomersprefsDefaultshipmethodListid() {
		return $this->SalesAndCustomersPrefs_DefaultShipMethod_ListID;
	}

	public function get_SalesAndCustomersPrefs_DefaultShipMethod_ListID() {
		return $this->SalesAndCustomersPrefs_DefaultShipMethod_ListID;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_DefaultShipMethod_ListID --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_DefaultShipMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_DefaultShipMethod_FullName` variable
	* @access public
	*/

	public function setSalesandcustomersprefsDefaultshipmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_DefaultShipMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_DefaultShipMethod_FullName` variable
	* @access public
	*/

	public function getSalesandcustomersprefsDefaultshipmethodFullname() {
		return $this->SalesAndCustomersPrefs_DefaultShipMethod_FullName;
	}

	public function get_SalesAndCustomersPrefs_DefaultShipMethod_FullName() {
		return $this->SalesAndCustomersPrefs_DefaultShipMethod_FullName;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_DefaultShipMethod_FullName --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_DefaultFOB -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_DefaultFOB` variable
	* @access public
	*/

	public function setSalesandcustomersprefsDefaultfob($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultFOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_DefaultFOB($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultFOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_DefaultFOB` variable
	* @access public
	*/

	public function getSalesandcustomersprefsDefaultfob() {
		return $this->SalesAndCustomersPrefs_DefaultFOB;
	}

	public function get_SalesAndCustomersPrefs_DefaultFOB() {
		return $this->SalesAndCustomersPrefs_DefaultFOB;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_DefaultFOB --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_DefaultMarkup -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_DefaultMarkup` variable
	* @access public
	*/

	public function setSalesandcustomersprefsDefaultmarkup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultMarkup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_DefaultMarkup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_DefaultMarkup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_DefaultMarkup` variable
	* @access public
	*/

	public function getSalesandcustomersprefsDefaultmarkup() {
		return $this->SalesAndCustomersPrefs_DefaultMarkup;
	}

	public function get_SalesAndCustomersPrefs_DefaultMarkup() {
		return $this->SalesAndCustomersPrefs_DefaultMarkup;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_DefaultMarkup --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome` variable
	* @access public
	*/

	public function setSalesandcustomersprefsIstrackingreimbursedexpensesasincome($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome` variable
	* @access public
	*/

	public function getSalesandcustomersprefsIstrackingreimbursedexpensesasincome() {
		return $this->SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome;
	}

	public function get_SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome() {
		return $this->SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_IsAutoApplyingPayments -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_IsAutoApplyingPayments` variable
	* @access public
	*/

	public function setSalesandcustomersprefsIsautoapplyingpayments($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_IsAutoApplyingPayments', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_IsAutoApplyingPayments($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_IsAutoApplyingPayments', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_IsAutoApplyingPayments` variable
	* @access public
	*/

	public function getSalesandcustomersprefsIsautoapplyingpayments() {
		return $this->SalesAndCustomersPrefs_IsAutoApplyingPayments;
	}

	public function get_SalesAndCustomersPrefs_IsAutoApplyingPayments() {
		return $this->SalesAndCustomersPrefs_IsAutoApplyingPayments;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_IsAutoApplyingPayments --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels` variable
	* @access public
	*/

	public function setSalesandcustomersprefsPricelevelsIsusingpricelevels($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels` variable
	* @access public
	*/

	public function getSalesandcustomersprefsPricelevelsIsusingpricelevels() {
		return $this->SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels;
	}

	public function get_SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels() {
		return $this->SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels --------------------------------------


// ---------------------------- Start Field: SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp -------------------------------------- 

	/** 
	* Sets a value to `SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp` variable
	* @access public
	*/

	public function setSalesandcustomersprefsPricelevelsIsroundingsalespriceup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp` variable
	* @access public
	*/

	public function getSalesandcustomersprefsPricelevelsIsroundingsalespriceup() {
		return $this->SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp;
	}

	public function get_SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp() {
		return $this->SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp;
	}

	
// ------------------------------ End Field: SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultItemSalesTax_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultItemSalesTax_ListID` variable
	* @access public
	*/

	public function setSalestaxprefsDefaultitemsalestaxListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultItemSalesTax_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultItemSalesTax_ListID` variable
	* @access public
	*/

	public function getSalestaxprefsDefaultitemsalestaxListid() {
		return $this->SalesTaxPrefs_DefaultItemSalesTax_ListID;
	}

	public function get_SalesTaxPrefs_DefaultItemSalesTax_ListID() {
		return $this->SalesTaxPrefs_DefaultItemSalesTax_ListID;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultItemSalesTax_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultItemSalesTax_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultItemSalesTax_FullName` variable
	* @access public
	*/

	public function setSalestaxprefsDefaultitemsalestaxFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultItemSalesTax_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultItemSalesTax_FullName` variable
	* @access public
	*/

	public function getSalestaxprefsDefaultitemsalestaxFullname() {
		return $this->SalesTaxPrefs_DefaultItemSalesTax_FullName;
	}

	public function get_SalesTaxPrefs_DefaultItemSalesTax_FullName() {
		return $this->SalesTaxPrefs_DefaultItemSalesTax_FullName;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultItemSalesTax_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_PaySalesTax -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_PaySalesTax` variable
	* @access public
	*/

	public function setSalestaxprefsPaysalestax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_PaySalesTax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_PaySalesTax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_PaySalesTax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_PaySalesTax` variable
	* @access public
	*/

	public function getSalestaxprefsPaysalestax() {
		return $this->SalesTaxPrefs_PaySalesTax;
	}

	public function get_SalesTaxPrefs_PaySalesTax() {
		return $this->SalesTaxPrefs_PaySalesTax;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_PaySalesTax --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxprefsDefaulttaxablesalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxprefsDefaulttaxablesalestaxcodeListid() {
		return $this->SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID;
	}

	public function get_SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID() {
		return $this->SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxprefsDefaulttaxablesalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxprefsDefaulttaxablesalestaxcodeFullname() {
		return $this->SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName;
	}

	public function get_SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName() {
		return $this->SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxprefsDefaultnontaxablesalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxprefsDefaultnontaxablesalestaxcodeListid() {
		return $this->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID;
	}

	public function get_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID() {
		return $this->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxprefsDefaultnontaxablesalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxprefsDefaultnontaxablesalestaxcodeFullname() {
		return $this->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName;
	}

	public function get_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName() {
		return $this->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_IsUsingVendorTaxCode -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_IsUsingVendorTaxCode` variable
	* @access public
	*/

	public function setSalestaxprefsIsusingvendortaxcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingVendorTaxCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_IsUsingVendorTaxCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingVendorTaxCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_IsUsingVendorTaxCode` variable
	* @access public
	*/

	public function getSalestaxprefsIsusingvendortaxcode() {
		return $this->SalesTaxPrefs_IsUsingVendorTaxCode;
	}

	public function get_SalesTaxPrefs_IsUsingVendorTaxCode() {
		return $this->SalesTaxPrefs_IsUsingVendorTaxCode;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_IsUsingVendorTaxCode --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_IsUsingCustomerTaxCode -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_IsUsingCustomerTaxCode` variable
	* @access public
	*/

	public function setSalestaxprefsIsusingcustomertaxcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingCustomerTaxCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_IsUsingCustomerTaxCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingCustomerTaxCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_IsUsingCustomerTaxCode` variable
	* @access public
	*/

	public function getSalestaxprefsIsusingcustomertaxcode() {
		return $this->SalesTaxPrefs_IsUsingCustomerTaxCode;
	}

	public function get_SalesTaxPrefs_IsUsingCustomerTaxCode() {
		return $this->SalesTaxPrefs_IsUsingCustomerTaxCode;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_IsUsingCustomerTaxCode --------------------------------------


// ---------------------------- Start Field: SalesTaxPrefs_IsUsingAmountsIncludeTax -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPrefs_IsUsingAmountsIncludeTax` variable
	* @access public
	*/

	public function setSalestaxprefsIsusingamountsincludetax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingAmountsIncludeTax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPrefs_IsUsingAmountsIncludeTax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPrefs_IsUsingAmountsIncludeTax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPrefs_IsUsingAmountsIncludeTax` variable
	* @access public
	*/

	public function getSalestaxprefsIsusingamountsincludetax() {
		return $this->SalesTaxPrefs_IsUsingAmountsIncludeTax;
	}

	public function get_SalesTaxPrefs_IsUsingAmountsIncludeTax() {
		return $this->SalesTaxPrefs_IsUsingAmountsIncludeTax;
	}

	
// ------------------------------ End Field: SalesTaxPrefs_IsUsingAmountsIncludeTax --------------------------------------


// ---------------------------- Start Field: TimeTrackingPrefs_FirstDayOfWeek -------------------------------------- 

	/** 
	* Sets a value to `TimeTrackingPrefs_FirstDayOfWeek` variable
	* @access public
	*/

	public function setTimetrackingprefsFirstdayofweek($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeTrackingPrefs_FirstDayOfWeek', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeTrackingPrefs_FirstDayOfWeek($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeTrackingPrefs_FirstDayOfWeek', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeTrackingPrefs_FirstDayOfWeek` variable
	* @access public
	*/

	public function getTimetrackingprefsFirstdayofweek() {
		return $this->TimeTrackingPrefs_FirstDayOfWeek;
	}

	public function get_TimeTrackingPrefs_FirstDayOfWeek() {
		return $this->TimeTrackingPrefs_FirstDayOfWeek;
	}

	
// ------------------------------ End Field: TimeTrackingPrefs_FirstDayOfWeek --------------------------------------


// ---------------------------- Start Field: CurrentAppAccessRights_IsAutomaticLoginAllowed -------------------------------------- 

	/** 
	* Sets a value to `CurrentAppAccessRights_IsAutomaticLoginAllowed` variable
	* @access public
	*/

	public function setCurrentappaccessrightsIsautomaticloginallowed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_IsAutomaticLoginAllowed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CurrentAppAccessRights_IsAutomaticLoginAllowed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_IsAutomaticLoginAllowed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CurrentAppAccessRights_IsAutomaticLoginAllowed` variable
	* @access public
	*/

	public function getCurrentappaccessrightsIsautomaticloginallowed() {
		return $this->CurrentAppAccessRights_IsAutomaticLoginAllowed;
	}

	public function get_CurrentAppAccessRights_IsAutomaticLoginAllowed() {
		return $this->CurrentAppAccessRights_IsAutomaticLoginAllowed;
	}

	
// ------------------------------ End Field: CurrentAppAccessRights_IsAutomaticLoginAllowed --------------------------------------


// ---------------------------- Start Field: CurrentAppAccessRights_AutomaticLoginUserName -------------------------------------- 

	/** 
	* Sets a value to `CurrentAppAccessRights_AutomaticLoginUserName` variable
	* @access public
	*/

	public function setCurrentappaccessrightsAutomaticloginusername($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_AutomaticLoginUserName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CurrentAppAccessRights_AutomaticLoginUserName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_AutomaticLoginUserName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CurrentAppAccessRights_AutomaticLoginUserName` variable
	* @access public
	*/

	public function getCurrentappaccessrightsAutomaticloginusername() {
		return $this->CurrentAppAccessRights_AutomaticLoginUserName;
	}

	public function get_CurrentAppAccessRights_AutomaticLoginUserName() {
		return $this->CurrentAppAccessRights_AutomaticLoginUserName;
	}

	
// ------------------------------ End Field: CurrentAppAccessRights_AutomaticLoginUserName --------------------------------------


// ---------------------------- Start Field: CurrentAppAccessRights_IsPersonalDataAccessAllowed -------------------------------------- 

	/** 
	* Sets a value to `CurrentAppAccessRights_IsPersonalDataAccessAllowed` variable
	* @access public
	*/

	public function setCurrentappaccessrightsIspersonaldataaccessallowed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_IsPersonalDataAccessAllowed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CurrentAppAccessRights_IsPersonalDataAccessAllowed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrentAppAccessRights_IsPersonalDataAccessAllowed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CurrentAppAccessRights_IsPersonalDataAccessAllowed` variable
	* @access public
	*/

	public function getCurrentappaccessrightsIspersonaldataaccessallowed() {
		return $this->CurrentAppAccessRights_IsPersonalDataAccessAllowed;
	}

	public function get_CurrentAppAccessRights_IsPersonalDataAccessAllowed() {
		return $this->CurrentAppAccessRights_IsPersonalDataAccessAllowed;
	}

	
// ------------------------------ End Field: CurrentAppAccessRights_IsPersonalDataAccessAllowed --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'AccountingPrefs_IsUsingAccountNumbers' => (object) array(
										'Field'=>'AccountingPrefs_IsUsingAccountNumbers',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountingPrefs_IsRequiringAccounts' => (object) array(
										'Field'=>'AccountingPrefs_IsRequiringAccounts',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountingPrefs_IsUsingClassTracking' => (object) array(
										'Field'=>'AccountingPrefs_IsUsingClassTracking',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountingPrefs_IsUsingAuditTrail' => (object) array(
										'Field'=>'AccountingPrefs_IsUsingAuditTrail',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountingPrefs_IsAssigningJournalEntryNumbers' => (object) array(
										'Field'=>'AccountingPrefs_IsAssigningJournalEntryNumbers',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'AccountingPrefs_ClosingDate' => (object) array(
										'Field'=>'AccountingPrefs_ClosingDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_AnnualInterestRate' => (object) array(
										'Field'=>'FinanceChargePrefs_AnnualInterestRate',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_MinFinanceCharge' => (object) array(
										'Field'=>'FinanceChargePrefs_MinFinanceCharge',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_GracePeriod' => (object) array(
										'Field'=>'FinanceChargePrefs_GracePeriod',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'FinanceChargePrefs_FinanceChargeAccount_ListID' => (object) array(
										'Field'=>'FinanceChargePrefs_FinanceChargeAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_FinanceChargeAccount_FullName' => (object) array(
										'Field'=>'FinanceChargePrefs_FinanceChargeAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_IsAssessingForOverdueCharges' => (object) array(
										'Field'=>'FinanceChargePrefs_IsAssessingForOverdueCharges',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'FinanceChargePrefs_CalculateChargesFrom' => (object) array(
										'Field'=>'FinanceChargePrefs_CalculateChargesFrom',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FinanceChargePrefs_IsMarkedToBePrinted' => (object) array(
										'Field'=>'FinanceChargePrefs_IsMarkedToBePrinted',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'JobsAndEstimatesPrefs_IsUsingEstimates' => (object) array(
										'Field'=>'JobsAndEstimatesPrefs_IsUsingEstimates',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'JobsAndEstimatesPrefs_IsUsingProgressInvoicing' => (object) array(
										'Field'=>'JobsAndEstimatesPrefs_IsUsingProgressInvoicing',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts' => (object) array(
										'Field'=>'JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'MultiCurrencyPrefs_IsMultiCurrencyOn' => (object) array(
										'Field'=>'MultiCurrencyPrefs_IsMultiCurrencyOn',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'MultiCurrencyPrefs_HomeCurrency_ListID' => (object) array(
										'Field'=>'MultiCurrencyPrefs_HomeCurrency_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'MultiCurrencyPrefs_HomeCurrency_FullName' => (object) array(
										'Field'=>'MultiCurrencyPrefs_HomeCurrency_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable' => (object) array(
										'Field'=>'MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled' => (object) array(
										'Field'=>'MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PurchasesAndVendorsPrefs_IsUsingInventory' => (object) array(
										'Field'=>'PurchasesAndVendorsPrefs_IsUsingInventory',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PurchasesAndVendorsPrefs_DaysBillsAreDue' => (object) array(
										'Field'=>'PurchasesAndVendorsPrefs_DaysBillsAreDue',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts' => (object) array(
										'Field'=>'PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID' => (object) array(
										'Field'=>'PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName' => (object) array(
										'Field'=>'PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ReportsPrefs_AgingReportBasis' => (object) array(
										'Field'=>'ReportsPrefs_AgingReportBasis',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ReportsPrefs_SummaryReportBasis' => (object) array(
										'Field'=>'ReportsPrefs_SummaryReportBasis',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_DefaultShipMethod_ListID' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_DefaultShipMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_DefaultShipMethod_FullName' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_DefaultShipMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_DefaultFOB' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_DefaultFOB',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_DefaultMarkup' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_DefaultMarkup',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_IsAutoApplyingPayments' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_IsAutoApplyingPayments',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp' => (object) array(
										'Field'=>'SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultItemSalesTax_ListID' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultItemSalesTax_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultItemSalesTax_FullName' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultItemSalesTax_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_PaySalesTax' => (object) array(
										'Field'=>'SalesTaxPrefs_PaySalesTax',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPrefs_IsUsingVendorTaxCode' => (object) array(
										'Field'=>'SalesTaxPrefs_IsUsingVendorTaxCode',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesTaxPrefs_IsUsingCustomerTaxCode' => (object) array(
										'Field'=>'SalesTaxPrefs_IsUsingCustomerTaxCode',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'SalesTaxPrefs_IsUsingAmountsIncludeTax' => (object) array(
										'Field'=>'SalesTaxPrefs_IsUsingAmountsIncludeTax',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TimeTrackingPrefs_FirstDayOfWeek' => (object) array(
										'Field'=>'TimeTrackingPrefs_FirstDayOfWeek',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CurrentAppAccessRights_IsAutomaticLoginAllowed' => (object) array(
										'Field'=>'CurrentAppAccessRights_IsAutomaticLoginAllowed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CurrentAppAccessRights_AutomaticLoginUserName' => (object) array(
										'Field'=>'CurrentAppAccessRights_AutomaticLoginUserName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CurrentAppAccessRights_IsPersonalDataAccessAllowed' => (object) array(
										'Field'=>'CurrentAppAccessRights_IsPersonalDataAccessAllowed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_preferences` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'AccountingPrefs_IsUsingAccountNumbers' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingAccountNumbers` tinyint(1) NULL   DEFAULT '0';",
			'AccountingPrefs_IsRequiringAccounts' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsRequiringAccounts` tinyint(1) NULL   DEFAULT '0';",
			'AccountingPrefs_IsUsingClassTracking' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingClassTracking` tinyint(1) NULL   DEFAULT '0';",
			'AccountingPrefs_IsUsingAuditTrail' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsUsingAuditTrail` tinyint(1) NULL   DEFAULT '0';",
			'AccountingPrefs_IsAssigningJournalEntryNumbers' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_IsAssigningJournalEntryNumbers` tinyint(1) NULL   DEFAULT '0';",
			'AccountingPrefs_ClosingDate' => "ALTER TABLE  `qb_preferences` ADD  `AccountingPrefs_ClosingDate` date NULL   ;",
			'FinanceChargePrefs_AnnualInterestRate' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_AnnualInterestRate` decimal(12,5) NULL   ;",
			'FinanceChargePrefs_MinFinanceCharge' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_MinFinanceCharge` decimal(10,2) NULL   ;",
			'FinanceChargePrefs_GracePeriod' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_GracePeriod` int(10) unsigned NULL   DEFAULT '0';",
			'FinanceChargePrefs_FinanceChargeAccount_ListID' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_FinanceChargeAccount_ListID` varchar(40) NULL   ;",
			'FinanceChargePrefs_FinanceChargeAccount_FullName' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_FinanceChargeAccount_FullName` varchar(255) NULL   ;",
			'FinanceChargePrefs_IsAssessingForOverdueCharges' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_IsAssessingForOverdueCharges` tinyint(1) NULL   DEFAULT '0';",
			'FinanceChargePrefs_CalculateChargesFrom' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_CalculateChargesFrom` varchar(40) NULL   ;",
			'FinanceChargePrefs_IsMarkedToBePrinted' => "ALTER TABLE  `qb_preferences` ADD  `FinanceChargePrefs_IsMarkedToBePrinted` tinyint(1) NULL   DEFAULT '0';",
			'JobsAndEstimatesPrefs_IsUsingEstimates' => "ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsUsingEstimates` tinyint(1) NULL   DEFAULT '0';",
			'JobsAndEstimatesPrefs_IsUsingProgressInvoicing' => "ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsUsingProgressInvoicing` tinyint(1) NULL   DEFAULT '0';",
			'JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts' => "ALTER TABLE  `qb_preferences` ADD  `JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts` tinyint(1) NULL   DEFAULT '0';",
			'MultiCurrencyPrefs_IsMultiCurrencyOn' => "ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_IsMultiCurrencyOn` tinyint(1) NULL   DEFAULT '0';",
			'MultiCurrencyPrefs_HomeCurrency_ListID' => "ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_HomeCurrency_ListID` varchar(40) NULL   ;",
			'MultiCurrencyPrefs_HomeCurrency_FullName' => "ALTER TABLE  `qb_preferences` ADD  `MultiCurrencyPrefs_HomeCurrency_FullName` varchar(255) NULL   ;",
			'MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable' => "ALTER TABLE  `qb_preferences` ADD  `MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable` tinyint(1) NULL   DEFAULT '0';",
			'MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled' => "ALTER TABLE  `qb_preferences` ADD  `MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled` tinyint(1) NULL   DEFAULT '0';",
			'PurchasesAndVendorsPrefs_IsUsingInventory' => "ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_IsUsingInventory` tinyint(1) NULL   DEFAULT '0';",
			'PurchasesAndVendorsPrefs_DaysBillsAreDue' => "ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DaysBillsAreDue` int(10) unsigned NULL   DEFAULT '0';",
			'PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts' => "ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts` tinyint(1) NULL   DEFAULT '0';",
			'PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID' => "ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID` varchar(40) NULL   ;",
			'PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName' => "ALTER TABLE  `qb_preferences` ADD  `PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName` varchar(255) NULL   ;",
			'ReportsPrefs_AgingReportBasis' => "ALTER TABLE  `qb_preferences` ADD  `ReportsPrefs_AgingReportBasis` varchar(40) NULL   ;",
			'ReportsPrefs_SummaryReportBasis' => "ALTER TABLE  `qb_preferences` ADD  `ReportsPrefs_SummaryReportBasis` varchar(40) NULL   ;",
			'SalesAndCustomersPrefs_DefaultShipMethod_ListID' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultShipMethod_ListID` varchar(40) NULL   ;",
			'SalesAndCustomersPrefs_DefaultShipMethod_FullName' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultShipMethod_FullName` varchar(255) NULL   ;",
			'SalesAndCustomersPrefs_DefaultFOB' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultFOB` text NULL   ;",
			'SalesAndCustomersPrefs_DefaultMarkup' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_DefaultMarkup` decimal(12,5) NULL   ;",
			'SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome` tinyint(1) NULL   DEFAULT '0';",
			'SalesAndCustomersPrefs_IsAutoApplyingPayments' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_IsAutoApplyingPayments` tinyint(1) NULL   DEFAULT '0';",
			'SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels` tinyint(1) NULL   DEFAULT '0';",
			'SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp' => "ALTER TABLE  `qb_preferences` ADD  `SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp` tinyint(1) NULL   DEFAULT '0';",
			'SalesTaxPrefs_DefaultItemSalesTax_ListID' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultItemSalesTax_ListID` varchar(40) NULL   ;",
			'SalesTaxPrefs_DefaultItemSalesTax_FullName' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultItemSalesTax_FullName` varchar(255) NULL   ;",
			'SalesTaxPrefs_PaySalesTax' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_PaySalesTax` varchar(40) NULL   ;",
			'SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName` varchar(255) NULL   ;",
			'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName` varchar(255) NULL   ;",
			'SalesTaxPrefs_IsUsingVendorTaxCode' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingVendorTaxCode` tinyint(1) NULL   DEFAULT '0';",
			'SalesTaxPrefs_IsUsingCustomerTaxCode' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingCustomerTaxCode` tinyint(1) NULL   DEFAULT '0';",
			'SalesTaxPrefs_IsUsingAmountsIncludeTax' => "ALTER TABLE  `qb_preferences` ADD  `SalesTaxPrefs_IsUsingAmountsIncludeTax` tinyint(1) NULL   DEFAULT '0';",
			'TimeTrackingPrefs_FirstDayOfWeek' => "ALTER TABLE  `qb_preferences` ADD  `TimeTrackingPrefs_FirstDayOfWeek` varchar(40) NULL   ;",
			'CurrentAppAccessRights_IsAutomaticLoginAllowed' => "ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_IsAutomaticLoginAllowed` tinyint(1) NULL   DEFAULT '0';",
			'CurrentAppAccessRights_AutomaticLoginUserName' => "ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_AutomaticLoginUserName` text NULL   ;",
			'CurrentAppAccessRights_IsPersonalDataAccessAllowed' => "ALTER TABLE  `qb_preferences` ADD  `CurrentAppAccessRights_IsPersonalDataAccessAllowed` tinyint(1) NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setAccountingprefsIsusingaccountnumbers() - AccountingPrefs_IsUsingAccountNumbers
//setAccountingprefsIsrequiringaccounts() - AccountingPrefs_IsRequiringAccounts
//setAccountingprefsIsusingclasstracking() - AccountingPrefs_IsUsingClassTracking
//setAccountingprefsIsusingaudittrail() - AccountingPrefs_IsUsingAuditTrail
//setAccountingprefsIsassigningjournalentrynumbers() - AccountingPrefs_IsAssigningJournalEntryNumbers
//setAccountingprefsClosingdate() - AccountingPrefs_ClosingDate
//setFinancechargeprefsAnnualinterestrate() - FinanceChargePrefs_AnnualInterestRate
//setFinancechargeprefsMinfinancecharge() - FinanceChargePrefs_MinFinanceCharge
//setFinancechargeprefsGraceperiod() - FinanceChargePrefs_GracePeriod
//setFinancechargeprefsFinancechargeaccountListid() - FinanceChargePrefs_FinanceChargeAccount_ListID
//setFinancechargeprefsFinancechargeaccountFullname() - FinanceChargePrefs_FinanceChargeAccount_FullName
//setFinancechargeprefsIsassessingforoverduecharges() - FinanceChargePrefs_IsAssessingForOverdueCharges
//setFinancechargeprefsCalculatechargesfrom() - FinanceChargePrefs_CalculateChargesFrom
//setFinancechargeprefsIsmarkedtobeprinted() - FinanceChargePrefs_IsMarkedToBePrinted
//setJobsandestimatesprefsIsusingestimates() - JobsAndEstimatesPrefs_IsUsingEstimates
//setJobsandestimatesprefsIsusingprogressinvoicing() - JobsAndEstimatesPrefs_IsUsingProgressInvoicing
//setJobsandestimatesprefsIsprintingitemswithzeroamounts() - JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts
//setMulticurrencyprefsIsmulticurrencyon() - MultiCurrencyPrefs_IsMultiCurrencyOn
//setMulticurrencyprefsHomecurrencyListid() - MultiCurrencyPrefs_HomeCurrency_ListID
//setMulticurrencyprefsHomecurrencyFullname() - MultiCurrencyPrefs_HomeCurrency_FullName
//setMultilocationinventoryprefsIsmultilocationinventoryavailable() - MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable
//setMultilocationinventoryprefsIsmultilocationinventoryenabled() - MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled
//setPurchasesandvendorsprefsIsusinginventory() - PurchasesAndVendorsPrefs_IsUsingInventory
//setPurchasesandvendorsprefsDaysbillsaredue() - PurchasesAndVendorsPrefs_DaysBillsAreDue
//setPurchasesandvendorsprefsIsautomaticallyusingdiscounts() - PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts
//setPurchasesandvendorsprefsDefaultdiscountaccountListid() - PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID
//setPurchasesandvendorsprefsDefaultdiscountaccountFullname() - PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName
//setReportsprefsAgingreportbasis() - ReportsPrefs_AgingReportBasis
//setReportsprefsSummaryreportbasis() - ReportsPrefs_SummaryReportBasis
//setSalesandcustomersprefsDefaultshipmethodListid() - SalesAndCustomersPrefs_DefaultShipMethod_ListID
//setSalesandcustomersprefsDefaultshipmethodFullname() - SalesAndCustomersPrefs_DefaultShipMethod_FullName
//setSalesandcustomersprefsDefaultfob() - SalesAndCustomersPrefs_DefaultFOB
//setSalesandcustomersprefsDefaultmarkup() - SalesAndCustomersPrefs_DefaultMarkup
//setSalesandcustomersprefsIstrackingreimbursedexpensesasincome() - SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome
//setSalesandcustomersprefsIsautoapplyingpayments() - SalesAndCustomersPrefs_IsAutoApplyingPayments
//setSalesandcustomersprefsPricelevelsIsusingpricelevels() - SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels
//setSalesandcustomersprefsPricelevelsIsroundingsalespriceup() - SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp
//setSalestaxprefsDefaultitemsalestaxListid() - SalesTaxPrefs_DefaultItemSalesTax_ListID
//setSalestaxprefsDefaultitemsalestaxFullname() - SalesTaxPrefs_DefaultItemSalesTax_FullName
//setSalestaxprefsPaysalestax() - SalesTaxPrefs_PaySalesTax
//setSalestaxprefsDefaulttaxablesalestaxcodeListid() - SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID
//setSalestaxprefsDefaulttaxablesalestaxcodeFullname() - SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName
//setSalestaxprefsDefaultnontaxablesalestaxcodeListid() - SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID
//setSalestaxprefsDefaultnontaxablesalestaxcodeFullname() - SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName
//setSalestaxprefsIsusingvendortaxcode() - SalesTaxPrefs_IsUsingVendorTaxCode
//setSalestaxprefsIsusingcustomertaxcode() - SalesTaxPrefs_IsUsingCustomerTaxCode
//setSalestaxprefsIsusingamountsincludetax() - SalesTaxPrefs_IsUsingAmountsIncludeTax
//setTimetrackingprefsFirstdayofweek() - TimeTrackingPrefs_FirstDayOfWeek
//setCurrentappaccessrightsIsautomaticloginallowed() - CurrentAppAccessRights_IsAutomaticLoginAllowed
//setCurrentappaccessrightsAutomaticloginusername() - CurrentAppAccessRights_AutomaticLoginUserName
//setCurrentappaccessrightsIspersonaldataaccessallowed() - CurrentAppAccessRights_IsPersonalDataAccessAllowed

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_AccountingPrefs_IsUsingAccountNumbers() - AccountingPrefs_IsUsingAccountNumbers
//set_AccountingPrefs_IsRequiringAccounts() - AccountingPrefs_IsRequiringAccounts
//set_AccountingPrefs_IsUsingClassTracking() - AccountingPrefs_IsUsingClassTracking
//set_AccountingPrefs_IsUsingAuditTrail() - AccountingPrefs_IsUsingAuditTrail
//set_AccountingPrefs_IsAssigningJournalEntryNumbers() - AccountingPrefs_IsAssigningJournalEntryNumbers
//set_AccountingPrefs_ClosingDate() - AccountingPrefs_ClosingDate
//set_FinanceChargePrefs_AnnualInterestRate() - FinanceChargePrefs_AnnualInterestRate
//set_FinanceChargePrefs_MinFinanceCharge() - FinanceChargePrefs_MinFinanceCharge
//set_FinanceChargePrefs_GracePeriod() - FinanceChargePrefs_GracePeriod
//set_FinanceChargePrefs_FinanceChargeAccount_ListID() - FinanceChargePrefs_FinanceChargeAccount_ListID
//set_FinanceChargePrefs_FinanceChargeAccount_FullName() - FinanceChargePrefs_FinanceChargeAccount_FullName
//set_FinanceChargePrefs_IsAssessingForOverdueCharges() - FinanceChargePrefs_IsAssessingForOverdueCharges
//set_FinanceChargePrefs_CalculateChargesFrom() - FinanceChargePrefs_CalculateChargesFrom
//set_FinanceChargePrefs_IsMarkedToBePrinted() - FinanceChargePrefs_IsMarkedToBePrinted
//set_JobsAndEstimatesPrefs_IsUsingEstimates() - JobsAndEstimatesPrefs_IsUsingEstimates
//set_JobsAndEstimatesPrefs_IsUsingProgressInvoicing() - JobsAndEstimatesPrefs_IsUsingProgressInvoicing
//set_JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts() - JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts
//set_MultiCurrencyPrefs_IsMultiCurrencyOn() - MultiCurrencyPrefs_IsMultiCurrencyOn
//set_MultiCurrencyPrefs_HomeCurrency_ListID() - MultiCurrencyPrefs_HomeCurrency_ListID
//set_MultiCurrencyPrefs_HomeCurrency_FullName() - MultiCurrencyPrefs_HomeCurrency_FullName
//set_MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable() - MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable
//set_MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled() - MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled
//set_PurchasesAndVendorsPrefs_IsUsingInventory() - PurchasesAndVendorsPrefs_IsUsingInventory
//set_PurchasesAndVendorsPrefs_DaysBillsAreDue() - PurchasesAndVendorsPrefs_DaysBillsAreDue
//set_PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts() - PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts
//set_PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID() - PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID
//set_PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName() - PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName
//set_ReportsPrefs_AgingReportBasis() - ReportsPrefs_AgingReportBasis
//set_ReportsPrefs_SummaryReportBasis() - ReportsPrefs_SummaryReportBasis
//set_SalesAndCustomersPrefs_DefaultShipMethod_ListID() - SalesAndCustomersPrefs_DefaultShipMethod_ListID
//set_SalesAndCustomersPrefs_DefaultShipMethod_FullName() - SalesAndCustomersPrefs_DefaultShipMethod_FullName
//set_SalesAndCustomersPrefs_DefaultFOB() - SalesAndCustomersPrefs_DefaultFOB
//set_SalesAndCustomersPrefs_DefaultMarkup() - SalesAndCustomersPrefs_DefaultMarkup
//set_SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome() - SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome
//set_SalesAndCustomersPrefs_IsAutoApplyingPayments() - SalesAndCustomersPrefs_IsAutoApplyingPayments
//set_SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels() - SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels
//set_SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp() - SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp
//set_SalesTaxPrefs_DefaultItemSalesTax_ListID() - SalesTaxPrefs_DefaultItemSalesTax_ListID
//set_SalesTaxPrefs_DefaultItemSalesTax_FullName() - SalesTaxPrefs_DefaultItemSalesTax_FullName
//set_SalesTaxPrefs_PaySalesTax() - SalesTaxPrefs_PaySalesTax
//set_SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID() - SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID
//set_SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName() - SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName
//set_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID() - SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID
//set_SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName() - SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName
//set_SalesTaxPrefs_IsUsingVendorTaxCode() - SalesTaxPrefs_IsUsingVendorTaxCode
//set_SalesTaxPrefs_IsUsingCustomerTaxCode() - SalesTaxPrefs_IsUsingCustomerTaxCode
//set_SalesTaxPrefs_IsUsingAmountsIncludeTax() - SalesTaxPrefs_IsUsingAmountsIncludeTax
//set_TimeTrackingPrefs_FirstDayOfWeek() - TimeTrackingPrefs_FirstDayOfWeek
//set_CurrentAppAccessRights_IsAutomaticLoginAllowed() - CurrentAppAccessRights_IsAutomaticLoginAllowed
//set_CurrentAppAccessRights_AutomaticLoginUserName() - CurrentAppAccessRights_AutomaticLoginUserName
//set_CurrentAppAccessRights_IsPersonalDataAccessAllowed() - CurrentAppAccessRights_IsPersonalDataAccessAllowed

*/
/* End of file Qb_preferences_model.php */
/* Location: ./application/models/Qb_preferences_model.php */
