<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_datedriventerms_model Class
 *
 * Manipulates `qb_datedriventerms` table on database

CREATE TABLE `qb_datedriventerms` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` text,
  `IsActive` tinyint(1) DEFAULT '0',
  `DayOfMonthDue` int(10) unsigned DEFAULT '0',
  `DueNextMonthDays` int(10) unsigned DEFAULT '0',
  `DiscountDayOfMonth` int(10) unsigned DEFAULT '0',
  `DiscountPct` decimal(12,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `ListID` (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_datedriventerms` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_datedriventerms` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_datedriventerms` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_datedriventerms` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_datedriventerms` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_datedriventerms` ADD  `Name` text NULL   ;
ALTER TABLE  `qb_datedriventerms` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_datedriventerms` ADD  `DayOfMonthDue` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_datedriventerms` ADD  `DueNextMonthDays` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_datedriventerms` ADD  `DiscountDayOfMonth` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_datedriventerms` ADD  `DiscountPct` decimal(12,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_datedriventerms_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $DayOfMonthDue;
	protected $DueNextMonthDays;
	protected $DiscountDayOfMonth;
	protected $DiscountPct;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_datedriventerms';
		$this->_short_name = 'qb_datedriventerms';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","DayOfMonthDue","DueNextMonthDays","DiscountDayOfMonth","DiscountPct");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: DayOfMonthDue -------------------------------------- 

	/** 
	* Sets a value to `DayOfMonthDue` variable
	* @access public
	*/

	public function setDayofmonthdue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DayOfMonthDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DayOfMonthDue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DayOfMonthDue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DayOfMonthDue` variable
	* @access public
	*/

	public function getDayofmonthdue() {
		return $this->DayOfMonthDue;
	}

	public function get_DayOfMonthDue() {
		return $this->DayOfMonthDue;
	}

	
// ------------------------------ End Field: DayOfMonthDue --------------------------------------


// ---------------------------- Start Field: DueNextMonthDays -------------------------------------- 

	/** 
	* Sets a value to `DueNextMonthDays` variable
	* @access public
	*/

	public function setDuenextmonthdays($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueNextMonthDays', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueNextMonthDays($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueNextMonthDays', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueNextMonthDays` variable
	* @access public
	*/

	public function getDuenextmonthdays() {
		return $this->DueNextMonthDays;
	}

	public function get_DueNextMonthDays() {
		return $this->DueNextMonthDays;
	}

	
// ------------------------------ End Field: DueNextMonthDays --------------------------------------


// ---------------------------- Start Field: DiscountDayOfMonth -------------------------------------- 

	/** 
	* Sets a value to `DiscountDayOfMonth` variable
	* @access public
	*/

	public function setDiscountdayofmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountDayOfMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DiscountDayOfMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountDayOfMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DiscountDayOfMonth` variable
	* @access public
	*/

	public function getDiscountdayofmonth() {
		return $this->DiscountDayOfMonth;
	}

	public function get_DiscountDayOfMonth() {
		return $this->DiscountDayOfMonth;
	}

	
// ------------------------------ End Field: DiscountDayOfMonth --------------------------------------


// ---------------------------- Start Field: DiscountPct -------------------------------------- 

	/** 
	* Sets a value to `DiscountPct` variable
	* @access public
	*/

	public function setDiscountpct($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountPct', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DiscountPct($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountPct', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DiscountPct` variable
	* @access public
	*/

	public function getDiscountpct() {
		return $this->DiscountPct;
	}

	public function get_DiscountPct() {
		return $this->DiscountPct;
	}

	
// ------------------------------ End Field: DiscountPct --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'DayOfMonthDue' => (object) array(
										'Field'=>'DayOfMonthDue',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'DueNextMonthDays' => (object) array(
										'Field'=>'DueNextMonthDays',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'DiscountDayOfMonth' => (object) array(
										'Field'=>'DiscountDayOfMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'DiscountPct' => (object) array(
										'Field'=>'DiscountPct',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_datedriventerms` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_datedriventerms` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_datedriventerms` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_datedriventerms` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_datedriventerms` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_datedriventerms` ADD  `Name` text NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_datedriventerms` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'DayOfMonthDue' => "ALTER TABLE  `qb_datedriventerms` ADD  `DayOfMonthDue` int(10) unsigned NULL   DEFAULT '0';",
			'DueNextMonthDays' => "ALTER TABLE  `qb_datedriventerms` ADD  `DueNextMonthDays` int(10) unsigned NULL   DEFAULT '0';",
			'DiscountDayOfMonth' => "ALTER TABLE  `qb_datedriventerms` ADD  `DiscountDayOfMonth` int(10) unsigned NULL   DEFAULT '0';",
			'DiscountPct' => "ALTER TABLE  `qb_datedriventerms` ADD  `DiscountPct` decimal(12,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setDayofmonthdue() - DayOfMonthDue
//setDuenextmonthdays() - DueNextMonthDays
//setDiscountdayofmonth() - DiscountDayOfMonth
//setDiscountpct() - DiscountPct

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_DayOfMonthDue() - DayOfMonthDue
//set_DueNextMonthDays() - DueNextMonthDays
//set_DiscountDayOfMonth() - DiscountDayOfMonth
//set_DiscountPct() - DiscountPct

*/
/* End of file Qb_datedriventerms_model.php */
/* Location: ./application/models/Qb_datedriventerms_model.php */
