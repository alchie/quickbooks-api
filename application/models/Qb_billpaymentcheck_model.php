<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_billpaymentcheck_model Class
 *
 * Manipulates `qb_billpaymentcheck` table on database

CREATE TABLE `qb_billpaymentcheck` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ExchangeRate` text,
  `AmountInHomeCurrency` text,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `PayeeEntity_ListID` varchar(40) DEFAULT NULL,
  `PayeeEntity_FullName` varchar(255) DEFAULT NULL,
  `APAccount_ListID` varchar(40) DEFAULT NULL,
  `APAccount_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `BankAccount_ListID` varchar(40) DEFAULT NULL,
  `BankAccount_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `Memo` text,
  `Address_Addr1` text,
  `Address_Addr2` text,
  `Address_Addr3` text,
  `Address_Addr4` text,
  `Address_Note` text,
  `Address_City` text,
  `Address_State` text,
  `Address_PostalCode` text,
  `Address_Country` text,
  `AddressBlock_Addr1` text,
  `AddressBlock_Addr2` text,
  `AddressBlock_Addr3` text,
  `AddressBlock_Addr4` text,
  `AddressBlock_Addr5` text,
  `IsToBePrinted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `PayeeEntity_ListID` (`PayeeEntity_ListID`),
  KEY `APAccount_ListID` (`APAccount_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `BankAccount_ListID` (`BankAccount_ListID`),
  KEY `RefNumber` (`RefNumber`),
  KEY `IsToBePrinted` (`IsToBePrinted`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_billpaymentcheck` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_billpaymentcheck` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AmountInHomeCurrency` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_billpaymentcheck` ADD  `PayeeEntity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `PayeeEntity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `APAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `APAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `BankAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `BankAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr1` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr2` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr3` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr4` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Note` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_City` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_State` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_PostalCode` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Country` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_billpaymentcheck` ADD  `IsToBePrinted` tinyint(1) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_billpaymentcheck_model extends MY_Model {

	protected $qbxml_id;
	protected $ExchangeRate;
	protected $AmountInHomeCurrency;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $PayeeEntity_ListID;
	protected $PayeeEntity_FullName;
	protected $APAccount_ListID;
	protected $APAccount_FullName;
	protected $TxnDate;
	protected $BankAccount_ListID;
	protected $BankAccount_FullName;
	protected $Amount;
	protected $RefNumber;
	protected $Memo;
	protected $Address_Addr1;
	protected $Address_Addr2;
	protected $Address_Addr3;
	protected $Address_Addr4;
	protected $Address_Note;
	protected $Address_City;
	protected $Address_State;
	protected $Address_PostalCode;
	protected $Address_Country;
	protected $AddressBlock_Addr1;
	protected $AddressBlock_Addr2;
	protected $AddressBlock_Addr3;
	protected $AddressBlock_Addr4;
	protected $AddressBlock_Addr5;
	protected $IsToBePrinted;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_billpaymentcheck';
		$this->_short_name = 'qb_billpaymentcheck';
		$this->_fields = array("qbxml_id","ExchangeRate","AmountInHomeCurrency","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","PayeeEntity_ListID","PayeeEntity_FullName","APAccount_ListID","APAccount_FullName","TxnDate","BankAccount_ListID","BankAccount_FullName","Amount","RefNumber","Memo","Address_Addr1","Address_Addr2","Address_Addr3","Address_Addr4","Address_Note","Address_City","Address_State","Address_PostalCode","Address_Country","AddressBlock_Addr1","AddressBlock_Addr2","AddressBlock_Addr3","AddressBlock_Addr4","AddressBlock_Addr5","IsToBePrinted");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: AmountInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `AmountInHomeCurrency` variable
	* @access public
	*/

	public function setAmountinhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AmountInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AmountInHomeCurrency` variable
	* @access public
	*/

	public function getAmountinhomecurrency() {
		return $this->AmountInHomeCurrency;
	}

	public function get_AmountInHomeCurrency() {
		return $this->AmountInHomeCurrency;
	}

	
// ------------------------------ End Field: AmountInHomeCurrency --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: PayeeEntity_ListID -------------------------------------- 

	/** 
	* Sets a value to `PayeeEntity_ListID` variable
	* @access public
	*/

	public function setPayeeentityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayeeEntity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayeeEntity_ListID` variable
	* @access public
	*/

	public function getPayeeentityListid() {
		return $this->PayeeEntity_ListID;
	}

	public function get_PayeeEntity_ListID() {
		return $this->PayeeEntity_ListID;
	}

	
// ------------------------------ End Field: PayeeEntity_ListID --------------------------------------


// ---------------------------- Start Field: PayeeEntity_FullName -------------------------------------- 

	/** 
	* Sets a value to `PayeeEntity_FullName` variable
	* @access public
	*/

	public function setPayeeentityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayeeEntity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayeeEntity_FullName` variable
	* @access public
	*/

	public function getPayeeentityFullname() {
		return $this->PayeeEntity_FullName;
	}

	public function get_PayeeEntity_FullName() {
		return $this->PayeeEntity_FullName;
	}

	
// ------------------------------ End Field: PayeeEntity_FullName --------------------------------------


// ---------------------------- Start Field: APAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `APAccount_ListID` variable
	* @access public
	*/

	public function setApaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_ListID` variable
	* @access public
	*/

	public function getApaccountListid() {
		return $this->APAccount_ListID;
	}

	public function get_APAccount_ListID() {
		return $this->APAccount_ListID;
	}

	
// ------------------------------ End Field: APAccount_ListID --------------------------------------


// ---------------------------- Start Field: APAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `APAccount_FullName` variable
	* @access public
	*/

	public function setApaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_FullName` variable
	* @access public
	*/

	public function getApaccountFullname() {
		return $this->APAccount_FullName;
	}

	public function get_APAccount_FullName() {
		return $this->APAccount_FullName;
	}

	
// ------------------------------ End Field: APAccount_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: BankAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `BankAccount_ListID` variable
	* @access public
	*/

	public function setBankaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BankAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BankAccount_ListID` variable
	* @access public
	*/

	public function getBankaccountListid() {
		return $this->BankAccount_ListID;
	}

	public function get_BankAccount_ListID() {
		return $this->BankAccount_ListID;
	}

	
// ------------------------------ End Field: BankAccount_ListID --------------------------------------


// ---------------------------- Start Field: BankAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `BankAccount_FullName` variable
	* @access public
	*/

	public function setBankaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BankAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BankAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BankAccount_FullName` variable
	* @access public
	*/

	public function getBankaccountFullname() {
		return $this->BankAccount_FullName;
	}

	public function get_BankAccount_FullName() {
		return $this->BankAccount_FullName;
	}

	
// ------------------------------ End Field: BankAccount_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: Address_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr1` variable
	* @access public
	*/

	public function setAddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr1` variable
	* @access public
	*/

	public function getAddressAddr1() {
		return $this->Address_Addr1;
	}

	public function get_Address_Addr1() {
		return $this->Address_Addr1;
	}

	
// ------------------------------ End Field: Address_Addr1 --------------------------------------


// ---------------------------- Start Field: Address_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr2` variable
	* @access public
	*/

	public function setAddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr2` variable
	* @access public
	*/

	public function getAddressAddr2() {
		return $this->Address_Addr2;
	}

	public function get_Address_Addr2() {
		return $this->Address_Addr2;
	}

	
// ------------------------------ End Field: Address_Addr2 --------------------------------------


// ---------------------------- Start Field: Address_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr3` variable
	* @access public
	*/

	public function setAddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr3` variable
	* @access public
	*/

	public function getAddressAddr3() {
		return $this->Address_Addr3;
	}

	public function get_Address_Addr3() {
		return $this->Address_Addr3;
	}

	
// ------------------------------ End Field: Address_Addr3 --------------------------------------


// ---------------------------- Start Field: Address_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr4` variable
	* @access public
	*/

	public function setAddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr4` variable
	* @access public
	*/

	public function getAddressAddr4() {
		return $this->Address_Addr4;
	}

	public function get_Address_Addr4() {
		return $this->Address_Addr4;
	}

	
// ------------------------------ End Field: Address_Addr4 --------------------------------------


// ---------------------------- Start Field: Address_Note -------------------------------------- 

	/** 
	* Sets a value to `Address_Note` variable
	* @access public
	*/

	public function setAddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Note` variable
	* @access public
	*/

	public function getAddressNote() {
		return $this->Address_Note;
	}

	public function get_Address_Note() {
		return $this->Address_Note;
	}

	
// ------------------------------ End Field: Address_Note --------------------------------------


// ---------------------------- Start Field: Address_City -------------------------------------- 

	/** 
	* Sets a value to `Address_City` variable
	* @access public
	*/

	public function setAddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_City` variable
	* @access public
	*/

	public function getAddressCity() {
		return $this->Address_City;
	}

	public function get_Address_City() {
		return $this->Address_City;
	}

	
// ------------------------------ End Field: Address_City --------------------------------------


// ---------------------------- Start Field: Address_State -------------------------------------- 

	/** 
	* Sets a value to `Address_State` variable
	* @access public
	*/

	public function setAddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_State` variable
	* @access public
	*/

	public function getAddressState() {
		return $this->Address_State;
	}

	public function get_Address_State() {
		return $this->Address_State;
	}

	
// ------------------------------ End Field: Address_State --------------------------------------


// ---------------------------- Start Field: Address_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `Address_PostalCode` variable
	* @access public
	*/

	public function setAddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_PostalCode` variable
	* @access public
	*/

	public function getAddressPostalcode() {
		return $this->Address_PostalCode;
	}

	public function get_Address_PostalCode() {
		return $this->Address_PostalCode;
	}

	
// ------------------------------ End Field: Address_PostalCode --------------------------------------


// ---------------------------- Start Field: Address_Country -------------------------------------- 

	/** 
	* Sets a value to `Address_Country` variable
	* @access public
	*/

	public function setAddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Country` variable
	* @access public
	*/

	public function getAddressCountry() {
		return $this->Address_Country;
	}

	public function get_Address_Country() {
		return $this->Address_Country;
	}

	
// ------------------------------ End Field: Address_Country --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr1` variable
	* @access public
	*/

	public function setAddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr1` variable
	* @access public
	*/

	public function getAddressblockAddr1() {
		return $this->AddressBlock_Addr1;
	}

	public function get_AddressBlock_Addr1() {
		return $this->AddressBlock_Addr1;
	}

	
// ------------------------------ End Field: AddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr2` variable
	* @access public
	*/

	public function setAddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr2` variable
	* @access public
	*/

	public function getAddressblockAddr2() {
		return $this->AddressBlock_Addr2;
	}

	public function get_AddressBlock_Addr2() {
		return $this->AddressBlock_Addr2;
	}

	
// ------------------------------ End Field: AddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr3` variable
	* @access public
	*/

	public function setAddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr3` variable
	* @access public
	*/

	public function getAddressblockAddr3() {
		return $this->AddressBlock_Addr3;
	}

	public function get_AddressBlock_Addr3() {
		return $this->AddressBlock_Addr3;
	}

	
// ------------------------------ End Field: AddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr4` variable
	* @access public
	*/

	public function setAddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr4` variable
	* @access public
	*/

	public function getAddressblockAddr4() {
		return $this->AddressBlock_Addr4;
	}

	public function get_AddressBlock_Addr4() {
		return $this->AddressBlock_Addr4;
	}

	
// ------------------------------ End Field: AddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr5` variable
	* @access public
	*/

	public function setAddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr5` variable
	* @access public
	*/

	public function getAddressblockAddr5() {
		return $this->AddressBlock_Addr5;
	}

	public function get_AddressBlock_Addr5() {
		return $this->AddressBlock_Addr5;
	}

	
// ------------------------------ End Field: AddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: IsToBePrinted -------------------------------------- 

	/** 
	* Sets a value to `IsToBePrinted` variable
	* @access public
	*/

	public function setIstobeprinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBePrinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBePrinted` variable
	* @access public
	*/

	public function getIstobeprinted() {
		return $this->IsToBePrinted;
	}

	public function get_IsToBePrinted() {
		return $this->IsToBePrinted;
	}

	
// ------------------------------ End Field: IsToBePrinted --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AmountInHomeCurrency' => (object) array(
										'Field'=>'AmountInHomeCurrency',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PayeeEntity_ListID' => (object) array(
										'Field'=>'PayeeEntity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PayeeEntity_FullName' => (object) array(
										'Field'=>'PayeeEntity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_ListID' => (object) array(
										'Field'=>'APAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_FullName' => (object) array(
										'Field'=>'APAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BankAccount_ListID' => (object) array(
										'Field'=>'BankAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BankAccount_FullName' => (object) array(
										'Field'=>'BankAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr1' => (object) array(
										'Field'=>'Address_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr2' => (object) array(
										'Field'=>'Address_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr3' => (object) array(
										'Field'=>'Address_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr4' => (object) array(
										'Field'=>'Address_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Note' => (object) array(
										'Field'=>'Address_Note',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_City' => (object) array(
										'Field'=>'Address_City',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_State' => (object) array(
										'Field'=>'Address_State',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_PostalCode' => (object) array(
										'Field'=>'Address_PostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Country' => (object) array(
										'Field'=>'Address_Country',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr1' => (object) array(
										'Field'=>'AddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr2' => (object) array(
										'Field'=>'AddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr3' => (object) array(
										'Field'=>'AddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr4' => (object) array(
										'Field'=>'AddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr5' => (object) array(
										'Field'=>'AddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBePrinted' => (object) array(
										'Field'=>'IsToBePrinted',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ExchangeRate' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `ExchangeRate` text NULL   ;",
			'AmountInHomeCurrency' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AmountInHomeCurrency` text NULL   ;",
			'TxnID' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'PayeeEntity_ListID' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `PayeeEntity_ListID` varchar(40) NULL   ;",
			'PayeeEntity_FullName' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `PayeeEntity_FullName` varchar(255) NULL   ;",
			'APAccount_ListID' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `APAccount_ListID` varchar(40) NULL   ;",
			'APAccount_FullName' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `APAccount_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `TxnDate` date NULL   ;",
			'BankAccount_ListID' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `BankAccount_ListID` varchar(40) NULL   ;",
			'BankAccount_FullName' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `BankAccount_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Amount` decimal(10,2) NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `RefNumber` varchar(11) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Memo` text NULL   ;",
			'Address_Addr1' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr1` text NULL   ;",
			'Address_Addr2' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr2` text NULL   ;",
			'Address_Addr3' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr3` text NULL   ;",
			'Address_Addr4' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Addr4` text NULL   ;",
			'Address_Note' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Note` text NULL   ;",
			'Address_City' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_City` text NULL   ;",
			'Address_State' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_State` text NULL   ;",
			'Address_PostalCode' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_PostalCode` text NULL   ;",
			'Address_Country' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `Address_Country` text NULL   ;",
			'AddressBlock_Addr1' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr1` text NULL   ;",
			'AddressBlock_Addr2' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr2` text NULL   ;",
			'AddressBlock_Addr3' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr3` text NULL   ;",
			'AddressBlock_Addr4' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr4` text NULL   ;",
			'AddressBlock_Addr5' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `AddressBlock_Addr5` text NULL   ;",
			'IsToBePrinted' => "ALTER TABLE  `qb_billpaymentcheck` ADD  `IsToBePrinted` tinyint(1) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setExchangerate() - ExchangeRate
//setAmountinhomecurrency() - AmountInHomeCurrency
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setPayeeentityListid() - PayeeEntity_ListID
//setPayeeentityFullname() - PayeeEntity_FullName
//setApaccountListid() - APAccount_ListID
//setApaccountFullname() - APAccount_FullName
//setTxndate() - TxnDate
//setBankaccountListid() - BankAccount_ListID
//setBankaccountFullname() - BankAccount_FullName
//setAmount() - Amount
//setRefnumber() - RefNumber
//setMemo() - Memo
//setAddressAddr1() - Address_Addr1
//setAddressAddr2() - Address_Addr2
//setAddressAddr3() - Address_Addr3
//setAddressAddr4() - Address_Addr4
//setAddressNote() - Address_Note
//setAddressCity() - Address_City
//setAddressState() - Address_State
//setAddressPostalcode() - Address_PostalCode
//setAddressCountry() - Address_Country
//setAddressblockAddr1() - AddressBlock_Addr1
//setAddressblockAddr2() - AddressBlock_Addr2
//setAddressblockAddr3() - AddressBlock_Addr3
//setAddressblockAddr4() - AddressBlock_Addr4
//setAddressblockAddr5() - AddressBlock_Addr5
//setIstobeprinted() - IsToBePrinted

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ExchangeRate() - ExchangeRate
//set_AmountInHomeCurrency() - AmountInHomeCurrency
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_PayeeEntity_ListID() - PayeeEntity_ListID
//set_PayeeEntity_FullName() - PayeeEntity_FullName
//set_APAccount_ListID() - APAccount_ListID
//set_APAccount_FullName() - APAccount_FullName
//set_TxnDate() - TxnDate
//set_BankAccount_ListID() - BankAccount_ListID
//set_BankAccount_FullName() - BankAccount_FullName
//set_Amount() - Amount
//set_RefNumber() - RefNumber
//set_Memo() - Memo
//set_Address_Addr1() - Address_Addr1
//set_Address_Addr2() - Address_Addr2
//set_Address_Addr3() - Address_Addr3
//set_Address_Addr4() - Address_Addr4
//set_Address_Note() - Address_Note
//set_Address_City() - Address_City
//set_Address_State() - Address_State
//set_Address_PostalCode() - Address_PostalCode
//set_Address_Country() - Address_Country
//set_AddressBlock_Addr1() - AddressBlock_Addr1
//set_AddressBlock_Addr2() - AddressBlock_Addr2
//set_AddressBlock_Addr3() - AddressBlock_Addr3
//set_AddressBlock_Addr4() - AddressBlock_Addr4
//set_AddressBlock_Addr5() - AddressBlock_Addr5
//set_IsToBePrinted() - IsToBePrinted

*/
/* End of file Qb_billpaymentcheck_model.php */
/* Location: ./application/models/Qb_billpaymentcheck_model.php */
