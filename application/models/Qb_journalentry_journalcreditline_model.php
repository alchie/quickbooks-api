<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_journalentry_journalcreditline_model Class
 *
 * Manipulates `qb_journalentry_journalcreditline` table on database

CREATE TABLE `qb_journalentry_journalcreditline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `JournalEntry_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Account_ListID` varchar(40) DEFAULT NULL,
  `Account_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `Memo` text,
  `Entity_ListID` varchar(40) DEFAULT NULL,
  `Entity_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `BillableStatus` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `JournalEntry_TxnID` (`JournalEntry_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Account_ListID` (`Account_ListID`),
  KEY `Entity_ListID` (`Entity_ListID`),
  KEY `Class_ListID` (`Class_ListID`)
) ENGINE=MyISAM AUTO_INCREMENT=417 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `JournalEntry_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Entity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Entity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `BillableStatus` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_journalentry_journalcreditline_model extends MY_Model {

	protected $qbxml_id;
	protected $JournalEntry_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Account_ListID;
	protected $Account_FullName;
	protected $Amount;
	protected $Memo;
	protected $Entity_ListID;
	protected $Entity_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $BillableStatus;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_journalentry_journalcreditline';
		$this->_short_name = 'qb_journalentry_journalcreditline';
		$this->_fields = array("qbxml_id","JournalEntry_TxnID","SortOrder","TxnLineID","Account_ListID","Account_FullName","Amount","Memo","Entity_ListID","Entity_FullName","Class_ListID","Class_FullName","BillableStatus");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: JournalEntry_TxnID -------------------------------------- 

	/** 
	* Sets a value to `JournalEntry_TxnID` variable
	* @access public
	*/

	public function setJournalentryTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JournalEntry_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_JournalEntry_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('JournalEntry_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `JournalEntry_TxnID` variable
	* @access public
	*/

	public function getJournalentryTxnid() {
		return $this->JournalEntry_TxnID;
	}

	public function get_JournalEntry_TxnID() {
		return $this->JournalEntry_TxnID;
	}

	
// ------------------------------ End Field: JournalEntry_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `Account_ListID` variable
	* @access public
	*/

	public function setAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_ListID` variable
	* @access public
	*/

	public function getAccountListid() {
		return $this->Account_ListID;
	}

	public function get_Account_ListID() {
		return $this->Account_ListID;
	}

	
// ------------------------------ End Field: Account_ListID --------------------------------------


// ---------------------------- Start Field: Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `Account_FullName` variable
	* @access public
	*/

	public function setAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_FullName` variable
	* @access public
	*/

	public function getAccountFullname() {
		return $this->Account_FullName;
	}

	public function get_Account_FullName() {
		return $this->Account_FullName;
	}

	
// ------------------------------ End Field: Account_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: Entity_ListID -------------------------------------- 

	/** 
	* Sets a value to `Entity_ListID` variable
	* @access public
	*/

	public function setEntityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_ListID` variable
	* @access public
	*/

	public function getEntityListid() {
		return $this->Entity_ListID;
	}

	public function get_Entity_ListID() {
		return $this->Entity_ListID;
	}

	
// ------------------------------ End Field: Entity_ListID --------------------------------------


// ---------------------------- Start Field: Entity_FullName -------------------------------------- 

	/** 
	* Sets a value to `Entity_FullName` variable
	* @access public
	*/

	public function setEntityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_FullName` variable
	* @access public
	*/

	public function getEntityFullname() {
		return $this->Entity_FullName;
	}

	public function get_Entity_FullName() {
		return $this->Entity_FullName;
	}

	
// ------------------------------ End Field: Entity_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: BillableStatus -------------------------------------- 

	/** 
	* Sets a value to `BillableStatus` variable
	* @access public
	*/

	public function setBillablestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableStatus` variable
	* @access public
	*/

	public function getBillablestatus() {
		return $this->BillableStatus;
	}

	public function get_BillableStatus() {
		return $this->BillableStatus;
	}

	
// ------------------------------ End Field: BillableStatus --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'JournalEntry_TxnID' => (object) array(
										'Field'=>'JournalEntry_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_ListID' => (object) array(
										'Field'=>'Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_FullName' => (object) array(
										'Field'=>'Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_ListID' => (object) array(
										'Field'=>'Entity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_FullName' => (object) array(
										'Field'=>'Entity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableStatus' => (object) array(
										'Field'=>'BillableStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'JournalEntry_TxnID' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `JournalEntry_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Account_ListID' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Account_ListID` varchar(40) NULL   ;",
			'Account_FullName' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Account_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Amount` decimal(10,2) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Memo` text NULL   ;",
			'Entity_ListID' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Entity_ListID` varchar(40) NULL   ;",
			'Entity_FullName' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Entity_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'BillableStatus' => "ALTER TABLE  `qb_journalentry_journalcreditline` ADD  `BillableStatus` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setJournalentryTxnid() - JournalEntry_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setAccountListid() - Account_ListID
//setAccountFullname() - Account_FullName
//setAmount() - Amount
//setMemo() - Memo
//setEntityListid() - Entity_ListID
//setEntityFullname() - Entity_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setBillablestatus() - BillableStatus

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_JournalEntry_TxnID() - JournalEntry_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Account_ListID() - Account_ListID
//set_Account_FullName() - Account_FullName
//set_Amount() - Amount
//set_Memo() - Memo
//set_Entity_ListID() - Entity_ListID
//set_Entity_FullName() - Entity_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_BillableStatus() - BillableStatus

*/
/* End of file Qb_journalentry_journalcreditline_model.php */
/* Location: ./application/models/Qb_journalentry_journalcreditline_model.php */
