<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_dataext_model Class
 *
 * Manipulates `qb_dataext` table on database

CREATE TABLE `qb_dataext` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EntityType` text,
  `TxnType` text,
  `Entity_ListID` varchar(40) DEFAULT NULL,
  `Txn_TxnID` varchar(40) DEFAULT NULL,
  `OwnerID` text,
  `DataExtName` varchar(31) DEFAULT NULL,
  `DataExtType` varchar(40) DEFAULT NULL,
  `DataExtValue` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `Entity_ListID` (`Entity_ListID`),
  KEY `Txn_TxnID` (`Txn_TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_dataext` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_dataext` ADD  `EntityType` text NULL   ;
ALTER TABLE  `qb_dataext` ADD  `TxnType` text NULL   ;
ALTER TABLE  `qb_dataext` ADD  `Entity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_dataext` ADD  `Txn_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_dataext` ADD  `OwnerID` text NULL   ;
ALTER TABLE  `qb_dataext` ADD  `DataExtName` varchar(31) NULL   ;
ALTER TABLE  `qb_dataext` ADD  `DataExtType` varchar(40) NULL   ;
ALTER TABLE  `qb_dataext` ADD  `DataExtValue` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_dataext_model extends MY_Model {

	protected $qbxml_id;
	protected $EntityType;
	protected $TxnType;
	protected $Entity_ListID;
	protected $Txn_TxnID;
	protected $OwnerID;
	protected $DataExtName;
	protected $DataExtType;
	protected $DataExtValue;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_dataext';
		$this->_short_name = 'qb_dataext';
		$this->_fields = array("qbxml_id","EntityType","TxnType","Entity_ListID","Txn_TxnID","OwnerID","DataExtName","DataExtType","DataExtValue");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: EntityType -------------------------------------- 

	/** 
	* Sets a value to `EntityType` variable
	* @access public
	*/

	public function setEntitytype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EntityType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EntityType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EntityType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EntityType` variable
	* @access public
	*/

	public function getEntitytype() {
		return $this->EntityType;
	}

	public function get_EntityType() {
		return $this->EntityType;
	}

	
// ------------------------------ End Field: EntityType --------------------------------------


// ---------------------------- Start Field: TxnType -------------------------------------- 

	/** 
	* Sets a value to `TxnType` variable
	* @access public
	*/

	public function setTxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnType` variable
	* @access public
	*/

	public function getTxntype() {
		return $this->TxnType;
	}

	public function get_TxnType() {
		return $this->TxnType;
	}

	
// ------------------------------ End Field: TxnType --------------------------------------


// ---------------------------- Start Field: Entity_ListID -------------------------------------- 

	/** 
	* Sets a value to `Entity_ListID` variable
	* @access public
	*/

	public function setEntityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_ListID` variable
	* @access public
	*/

	public function getEntityListid() {
		return $this->Entity_ListID;
	}

	public function get_Entity_ListID() {
		return $this->Entity_ListID;
	}

	
// ------------------------------ End Field: Entity_ListID --------------------------------------


// ---------------------------- Start Field: Txn_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Txn_TxnID` variable
	* @access public
	*/

	public function setTxnTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Txn_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Txn_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Txn_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Txn_TxnID` variable
	* @access public
	*/

	public function getTxnTxnid() {
		return $this->Txn_TxnID;
	}

	public function get_Txn_TxnID() {
		return $this->Txn_TxnID;
	}

	
// ------------------------------ End Field: Txn_TxnID --------------------------------------


// ---------------------------- Start Field: OwnerID -------------------------------------- 

	/** 
	* Sets a value to `OwnerID` variable
	* @access public
	*/

	public function setOwnerid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OwnerID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OwnerID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OwnerID` variable
	* @access public
	*/

	public function getOwnerid() {
		return $this->OwnerID;
	}

	public function get_OwnerID() {
		return $this->OwnerID;
	}

	
// ------------------------------ End Field: OwnerID --------------------------------------


// ---------------------------- Start Field: DataExtName -------------------------------------- 

	/** 
	* Sets a value to `DataExtName` variable
	* @access public
	*/

	public function setDataextname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtName` variable
	* @access public
	*/

	public function getDataextname() {
		return $this->DataExtName;
	}

	public function get_DataExtName() {
		return $this->DataExtName;
	}

	
// ------------------------------ End Field: DataExtName --------------------------------------


// ---------------------------- Start Field: DataExtType -------------------------------------- 

	/** 
	* Sets a value to `DataExtType` variable
	* @access public
	*/

	public function setDataexttype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtType` variable
	* @access public
	*/

	public function getDataexttype() {
		return $this->DataExtType;
	}

	public function get_DataExtType() {
		return $this->DataExtType;
	}

	
// ------------------------------ End Field: DataExtType --------------------------------------


// ---------------------------- Start Field: DataExtValue -------------------------------------- 

	/** 
	* Sets a value to `DataExtValue` variable
	* @access public
	*/

	public function setDataextvalue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DataExtValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DataExtValue', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DataExtValue` variable
	* @access public
	*/

	public function getDataextvalue() {
		return $this->DataExtValue;
	}

	public function get_DataExtValue() {
		return $this->DataExtValue;
	}

	
// ------------------------------ End Field: DataExtValue --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'EntityType' => (object) array(
										'Field'=>'EntityType',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnType' => (object) array(
										'Field'=>'TxnType',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_ListID' => (object) array(
										'Field'=>'Entity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Txn_TxnID' => (object) array(
										'Field'=>'Txn_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OwnerID' => (object) array(
										'Field'=>'OwnerID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtName' => (object) array(
										'Field'=>'DataExtName',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtType' => (object) array(
										'Field'=>'DataExtType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DataExtValue' => (object) array(
										'Field'=>'DataExtValue',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_dataext` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'EntityType' => "ALTER TABLE  `qb_dataext` ADD  `EntityType` text NULL   ;",
			'TxnType' => "ALTER TABLE  `qb_dataext` ADD  `TxnType` text NULL   ;",
			'Entity_ListID' => "ALTER TABLE  `qb_dataext` ADD  `Entity_ListID` varchar(40) NULL   ;",
			'Txn_TxnID' => "ALTER TABLE  `qb_dataext` ADD  `Txn_TxnID` varchar(40) NULL   ;",
			'OwnerID' => "ALTER TABLE  `qb_dataext` ADD  `OwnerID` text NULL   ;",
			'DataExtName' => "ALTER TABLE  `qb_dataext` ADD  `DataExtName` varchar(31) NULL   ;",
			'DataExtType' => "ALTER TABLE  `qb_dataext` ADD  `DataExtType` varchar(40) NULL   ;",
			'DataExtValue' => "ALTER TABLE  `qb_dataext` ADD  `DataExtValue` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setEntitytype() - EntityType
//setTxntype() - TxnType
//setEntityListid() - Entity_ListID
//setTxnTxnid() - Txn_TxnID
//setOwnerid() - OwnerID
//setDataextname() - DataExtName
//setDataexttype() - DataExtType
//setDataextvalue() - DataExtValue

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_EntityType() - EntityType
//set_TxnType() - TxnType
//set_Entity_ListID() - Entity_ListID
//set_Txn_TxnID() - Txn_TxnID
//set_OwnerID() - OwnerID
//set_DataExtName() - DataExtName
//set_DataExtType() - DataExtType
//set_DataExtValue() - DataExtValue

*/
/* End of file Qb_dataext_model.php */
/* Location: ./application/models/Qb_dataext_model.php */
