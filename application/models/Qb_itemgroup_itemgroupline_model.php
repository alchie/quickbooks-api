<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_itemgroup_itemgroupline_model Class
 *
 * Manipulates `qb_itemgroup_itemgroupline` table on database

CREATE TABLE `qb_itemgroup_itemgroupline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ItemGroup_ListID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `ItemGroup_ListID` (`ItemGroup_ListID`),
  KEY `Item_ListID` (`Item_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `ItemGroup_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `UnitOfMeasure` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_itemgroup_itemgroupline_model extends MY_Model {

	protected $qbxml_id;
	protected $ItemGroup_ListID;
	protected $SortOrder;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Quantity;
	protected $UnitOfMeasure;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_itemgroup_itemgroupline';
		$this->_short_name = 'qb_itemgroup_itemgroupline';
		$this->_fields = array("qbxml_id","ItemGroup_ListID","SortOrder","Item_ListID","Item_FullName","Quantity","UnitOfMeasure");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ItemGroup_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemGroup_ListID` variable
	* @access public
	*/

	public function setItemgroupListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemGroup_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemGroup_ListID` variable
	* @access public
	*/

	public function getItemgroupListid() {
		return $this->ItemGroup_ListID;
	}

	public function get_ItemGroup_ListID() {
		return $this->ItemGroup_ListID;
	}

	
// ------------------------------ End Field: ItemGroup_ListID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ItemGroup_ListID' => (object) array(
										'Field'=>'ItemGroup_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ItemGroup_ListID' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `ItemGroup_ListID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'Item_ListID' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_itemgroup_itemgroupline` ADD  `UnitOfMeasure` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setItemgroupListid() - ItemGroup_ListID
//setSortorder() - SortOrder
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ItemGroup_ListID() - ItemGroup_ListID
//set_SortOrder() - SortOrder
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure

*/
/* End of file Qb_itemgroup_itemgroupline_model.php */
/* Location: ./application/models/Qb_itemgroup_itemgroupline_model.php */
