<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_iteminventory_model Class
 *
 * Manipulates `qb_iteminventory` table on database

CREATE TABLE `qb_iteminventory` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `Parent_ListID` varchar(40) DEFAULT NULL,
  `Parent_FullName` varchar(255) DEFAULT NULL,
  `Sublevel` int(10) unsigned DEFAULT '0',
  `ManufacturerPartNumber` varchar(31) DEFAULT NULL,
  `UnitOfMeasureSet_ListID` varchar(40) DEFAULT NULL,
  `UnitOfMeasureSet_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `SalesDesc` text,
  `SalesPrice` decimal(13,5) DEFAULT NULL,
  `IncomeAccount_ListID` varchar(40) DEFAULT NULL,
  `IncomeAccount_FullName` varchar(255) DEFAULT NULL,
  `PurchaseDesc` text,
  `PurchaseCost` decimal(13,5) DEFAULT NULL,
  `COGSAccount_ListID` varchar(40) DEFAULT NULL,
  `COGSAccount_FullName` varchar(255) DEFAULT NULL,
  `PrefVendor_ListID` varchar(40) DEFAULT NULL,
  `PrefVendor_FullName` varchar(255) DEFAULT NULL,
  `AssetAccount_ListID` varchar(40) DEFAULT NULL,
  `AssetAccount_FullName` varchar(255) DEFAULT NULL,
  `ReorderPoint` decimal(12,5) DEFAULT '0.00000',
  `QuantityOnHand` decimal(12,5) DEFAULT '0.00000',
  `AverageCost` decimal(13,5) DEFAULT NULL,
  `QuantityOnOrder` decimal(12,5) DEFAULT '0.00000',
  `QuantityOnSalesOrder` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`qbxml_id`),
  KEY `FullName` (`FullName`),
  KEY `IsActive` (`IsActive`),
  KEY `Parent_ListID` (`Parent_ListID`),
  KEY `UnitOfMeasureSet_ListID` (`UnitOfMeasureSet_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`),
  KEY `IncomeAccount_ListID` (`IncomeAccount_ListID`),
  KEY `COGSAccount_ListID` (`COGSAccount_ListID`),
  KEY `PrefVendor_ListID` (`PrefVendor_ListID`),
  KEY `AssetAccount_ListID` (`AssetAccount_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_iteminventory` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_iteminventory` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_iteminventory` ADD  `Parent_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `Parent_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_iteminventory` ADD  `ManufacturerPartNumber` varchar(31) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `SalesDesc` text NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `SalesPrice` decimal(13,5) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `IncomeAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `IncomeAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `PurchaseDesc` text NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `PurchaseCost` decimal(13,5) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `COGSAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `COGSAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `PrefVendor_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `PrefVendor_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `AssetAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `AssetAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `ReorderPoint` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnHand` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_iteminventory` ADD  `AverageCost` decimal(13,5) NULL   ;
ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnOrder` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnSalesOrder` decimal(12,5) NULL   DEFAULT '0.00000';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_iteminventory_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $FullName;
	protected $IsActive;
	protected $Parent_ListID;
	protected $Parent_FullName;
	protected $Sublevel;
	protected $ManufacturerPartNumber;
	protected $UnitOfMeasureSet_ListID;
	protected $UnitOfMeasureSet_FullName;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $SalesDesc;
	protected $SalesPrice;
	protected $IncomeAccount_ListID;
	protected $IncomeAccount_FullName;
	protected $PurchaseDesc;
	protected $PurchaseCost;
	protected $COGSAccount_ListID;
	protected $COGSAccount_FullName;
	protected $PrefVendor_ListID;
	protected $PrefVendor_FullName;
	protected $AssetAccount_ListID;
	protected $AssetAccount_FullName;
	protected $ReorderPoint;
	protected $QuantityOnHand;
	protected $AverageCost;
	protected $QuantityOnOrder;
	protected $QuantityOnSalesOrder;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_iteminventory';
		$this->_short_name = 'qb_iteminventory';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","FullName","IsActive","Parent_ListID","Parent_FullName","Sublevel","ManufacturerPartNumber","UnitOfMeasureSet_ListID","UnitOfMeasureSet_FullName","SalesTaxCode_ListID","SalesTaxCode_FullName","SalesDesc","SalesPrice","IncomeAccount_ListID","IncomeAccount_FullName","PurchaseDesc","PurchaseCost","COGSAccount_ListID","COGSAccount_FullName","PrefVendor_ListID","PrefVendor_FullName","AssetAccount_ListID","AssetAccount_FullName","ReorderPoint","QuantityOnHand","AverageCost","QuantityOnOrder","QuantityOnSalesOrder");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: FullName -------------------------------------- 

	/** 
	* Sets a value to `FullName` variable
	* @access public
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FullName` variable
	* @access public
	*/

	public function getFullname() {
		return $this->FullName;
	}

	public function get_FullName() {
		return $this->FullName;
	}

	
// ------------------------------ End Field: FullName --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: Parent_ListID -------------------------------------- 

	/** 
	* Sets a value to `Parent_ListID` variable
	* @access public
	*/

	public function setParentListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_ListID` variable
	* @access public
	*/

	public function getParentListid() {
		return $this->Parent_ListID;
	}

	public function get_Parent_ListID() {
		return $this->Parent_ListID;
	}

	
// ------------------------------ End Field: Parent_ListID --------------------------------------


// ---------------------------- Start Field: Parent_FullName -------------------------------------- 

	/** 
	* Sets a value to `Parent_FullName` variable
	* @access public
	*/

	public function setParentFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_FullName` variable
	* @access public
	*/

	public function getParentFullname() {
		return $this->Parent_FullName;
	}

	public function get_Parent_FullName() {
		return $this->Parent_FullName;
	}

	
// ------------------------------ End Field: Parent_FullName --------------------------------------


// ---------------------------- Start Field: Sublevel -------------------------------------- 

	/** 
	* Sets a value to `Sublevel` variable
	* @access public
	*/

	public function setSublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Sublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Sublevel` variable
	* @access public
	*/

	public function getSublevel() {
		return $this->Sublevel;
	}

	public function get_Sublevel() {
		return $this->Sublevel;
	}

	
// ------------------------------ End Field: Sublevel --------------------------------------


// ---------------------------- Start Field: ManufacturerPartNumber -------------------------------------- 

	/** 
	* Sets a value to `ManufacturerPartNumber` variable
	* @access public
	*/

	public function setManufacturerpartnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ManufacturerPartNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ManufacturerPartNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ManufacturerPartNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ManufacturerPartNumber` variable
	* @access public
	*/

	public function getManufacturerpartnumber() {
		return $this->ManufacturerPartNumber;
	}

	public function get_ManufacturerPartNumber() {
		return $this->ManufacturerPartNumber;
	}

	
// ------------------------------ End Field: ManufacturerPartNumber --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function setUnitofmeasuresetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function getUnitofmeasuresetListid() {
		return $this->UnitOfMeasureSet_ListID;
	}

	public function get_UnitOfMeasureSet_ListID() {
		return $this->UnitOfMeasureSet_ListID;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_ListID --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function setUnitofmeasuresetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function getUnitofmeasuresetFullname() {
		return $this->UnitOfMeasureSet_FullName;
	}

	public function get_UnitOfMeasureSet_FullName() {
		return $this->UnitOfMeasureSet_FullName;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: SalesDesc -------------------------------------- 

	/** 
	* Sets a value to `SalesDesc` variable
	* @access public
	*/

	public function setSalesdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesDesc` variable
	* @access public
	*/

	public function getSalesdesc() {
		return $this->SalesDesc;
	}

	public function get_SalesDesc() {
		return $this->SalesDesc;
	}

	
// ------------------------------ End Field: SalesDesc --------------------------------------


// ---------------------------- Start Field: SalesPrice -------------------------------------- 

	/** 
	* Sets a value to `SalesPrice` variable
	* @access public
	*/

	public function setSalesprice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesPrice` variable
	* @access public
	*/

	public function getSalesprice() {
		return $this->SalesPrice;
	}

	public function get_SalesPrice() {
		return $this->SalesPrice;
	}

	
// ------------------------------ End Field: SalesPrice --------------------------------------


// ---------------------------- Start Field: IncomeAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `IncomeAccount_ListID` variable
	* @access public
	*/

	public function setIncomeaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IncomeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IncomeAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IncomeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IncomeAccount_ListID` variable
	* @access public
	*/

	public function getIncomeaccountListid() {
		return $this->IncomeAccount_ListID;
	}

	public function get_IncomeAccount_ListID() {
		return $this->IncomeAccount_ListID;
	}

	
// ------------------------------ End Field: IncomeAccount_ListID --------------------------------------


// ---------------------------- Start Field: IncomeAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `IncomeAccount_FullName` variable
	* @access public
	*/

	public function setIncomeaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IncomeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IncomeAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IncomeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IncomeAccount_FullName` variable
	* @access public
	*/

	public function getIncomeaccountFullname() {
		return $this->IncomeAccount_FullName;
	}

	public function get_IncomeAccount_FullName() {
		return $this->IncomeAccount_FullName;
	}

	
// ------------------------------ End Field: IncomeAccount_FullName --------------------------------------


// ---------------------------- Start Field: PurchaseDesc -------------------------------------- 

	/** 
	* Sets a value to `PurchaseDesc` variable
	* @access public
	*/

	public function setPurchasedesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseDesc` variable
	* @access public
	*/

	public function getPurchasedesc() {
		return $this->PurchaseDesc;
	}

	public function get_PurchaseDesc() {
		return $this->PurchaseDesc;
	}

	
// ------------------------------ End Field: PurchaseDesc --------------------------------------


// ---------------------------- Start Field: PurchaseCost -------------------------------------- 

	/** 
	* Sets a value to `PurchaseCost` variable
	* @access public
	*/

	public function setPurchasecost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseCost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseCost` variable
	* @access public
	*/

	public function getPurchasecost() {
		return $this->PurchaseCost;
	}

	public function get_PurchaseCost() {
		return $this->PurchaseCost;
	}

	
// ------------------------------ End Field: PurchaseCost --------------------------------------


// ---------------------------- Start Field: COGSAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `COGSAccount_ListID` variable
	* @access public
	*/

	public function setCogsaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('COGSAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_COGSAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('COGSAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `COGSAccount_ListID` variable
	* @access public
	*/

	public function getCogsaccountListid() {
		return $this->COGSAccount_ListID;
	}

	public function get_COGSAccount_ListID() {
		return $this->COGSAccount_ListID;
	}

	
// ------------------------------ End Field: COGSAccount_ListID --------------------------------------


// ---------------------------- Start Field: COGSAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `COGSAccount_FullName` variable
	* @access public
	*/

	public function setCogsaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('COGSAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_COGSAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('COGSAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `COGSAccount_FullName` variable
	* @access public
	*/

	public function getCogsaccountFullname() {
		return $this->COGSAccount_FullName;
	}

	public function get_COGSAccount_FullName() {
		return $this->COGSAccount_FullName;
	}

	
// ------------------------------ End Field: COGSAccount_FullName --------------------------------------


// ---------------------------- Start Field: PrefVendor_ListID -------------------------------------- 

	/** 
	* Sets a value to `PrefVendor_ListID` variable
	* @access public
	*/

	public function setPrefvendorListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrefVendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PrefVendor_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrefVendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PrefVendor_ListID` variable
	* @access public
	*/

	public function getPrefvendorListid() {
		return $this->PrefVendor_ListID;
	}

	public function get_PrefVendor_ListID() {
		return $this->PrefVendor_ListID;
	}

	
// ------------------------------ End Field: PrefVendor_ListID --------------------------------------


// ---------------------------- Start Field: PrefVendor_FullName -------------------------------------- 

	/** 
	* Sets a value to `PrefVendor_FullName` variable
	* @access public
	*/

	public function setPrefvendorFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrefVendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PrefVendor_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PrefVendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PrefVendor_FullName` variable
	* @access public
	*/

	public function getPrefvendorFullname() {
		return $this->PrefVendor_FullName;
	}

	public function get_PrefVendor_FullName() {
		return $this->PrefVendor_FullName;
	}

	
// ------------------------------ End Field: PrefVendor_FullName --------------------------------------


// ---------------------------- Start Field: AssetAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `AssetAccount_ListID` variable
	* @access public
	*/

	public function setAssetaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetAccount_ListID` variable
	* @access public
	*/

	public function getAssetaccountListid() {
		return $this->AssetAccount_ListID;
	}

	public function get_AssetAccount_ListID() {
		return $this->AssetAccount_ListID;
	}

	
// ------------------------------ End Field: AssetAccount_ListID --------------------------------------


// ---------------------------- Start Field: AssetAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `AssetAccount_FullName` variable
	* @access public
	*/

	public function setAssetaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AssetAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AssetAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AssetAccount_FullName` variable
	* @access public
	*/

	public function getAssetaccountFullname() {
		return $this->AssetAccount_FullName;
	}

	public function get_AssetAccount_FullName() {
		return $this->AssetAccount_FullName;
	}

	
// ------------------------------ End Field: AssetAccount_FullName --------------------------------------


// ---------------------------- Start Field: ReorderPoint -------------------------------------- 

	/** 
	* Sets a value to `ReorderPoint` variable
	* @access public
	*/

	public function setReorderpoint($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReorderPoint', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ReorderPoint($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReorderPoint', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ReorderPoint` variable
	* @access public
	*/

	public function getReorderpoint() {
		return $this->ReorderPoint;
	}

	public function get_ReorderPoint() {
		return $this->ReorderPoint;
	}

	
// ------------------------------ End Field: ReorderPoint --------------------------------------


// ---------------------------- Start Field: QuantityOnHand -------------------------------------- 

	/** 
	* Sets a value to `QuantityOnHand` variable
	* @access public
	*/

	public function setQuantityonhand($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnHand', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityOnHand($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnHand', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityOnHand` variable
	* @access public
	*/

	public function getQuantityonhand() {
		return $this->QuantityOnHand;
	}

	public function get_QuantityOnHand() {
		return $this->QuantityOnHand;
	}

	
// ------------------------------ End Field: QuantityOnHand --------------------------------------


// ---------------------------- Start Field: AverageCost -------------------------------------- 

	/** 
	* Sets a value to `AverageCost` variable
	* @access public
	*/

	public function setAveragecost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AverageCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AverageCost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AverageCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AverageCost` variable
	* @access public
	*/

	public function getAveragecost() {
		return $this->AverageCost;
	}

	public function get_AverageCost() {
		return $this->AverageCost;
	}

	
// ------------------------------ End Field: AverageCost --------------------------------------


// ---------------------------- Start Field: QuantityOnOrder -------------------------------------- 

	/** 
	* Sets a value to `QuantityOnOrder` variable
	* @access public
	*/

	public function setQuantityonorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityOnOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityOnOrder` variable
	* @access public
	*/

	public function getQuantityonorder() {
		return $this->QuantityOnOrder;
	}

	public function get_QuantityOnOrder() {
		return $this->QuantityOnOrder;
	}

	
// ------------------------------ End Field: QuantityOnOrder --------------------------------------


// ---------------------------- Start Field: QuantityOnSalesOrder -------------------------------------- 

	/** 
	* Sets a value to `QuantityOnSalesOrder` variable
	* @access public
	*/

	public function setQuantityonsalesorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnSalesOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QuantityOnSalesOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QuantityOnSalesOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QuantityOnSalesOrder` variable
	* @access public
	*/

	public function getQuantityonsalesorder() {
		return $this->QuantityOnSalesOrder;
	}

	public function get_QuantityOnSalesOrder() {
		return $this->QuantityOnSalesOrder;
	}

	
// ------------------------------ End Field: QuantityOnSalesOrder --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FullName' => (object) array(
										'Field'=>'FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'Parent_ListID' => (object) array(
										'Field'=>'Parent_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Parent_FullName' => (object) array(
										'Field'=>'Parent_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Sublevel' => (object) array(
										'Field'=>'Sublevel',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'ManufacturerPartNumber' => (object) array(
										'Field'=>'ManufacturerPartNumber',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasureSet_ListID' => (object) array(
										'Field'=>'UnitOfMeasureSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasureSet_FullName' => (object) array(
										'Field'=>'UnitOfMeasureSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesDesc' => (object) array(
										'Field'=>'SalesDesc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesPrice' => (object) array(
										'Field'=>'SalesPrice',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IncomeAccount_ListID' => (object) array(
										'Field'=>'IncomeAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IncomeAccount_FullName' => (object) array(
										'Field'=>'IncomeAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchaseDesc' => (object) array(
										'Field'=>'PurchaseDesc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PurchaseCost' => (object) array(
										'Field'=>'PurchaseCost',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'COGSAccount_ListID' => (object) array(
										'Field'=>'COGSAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'COGSAccount_FullName' => (object) array(
										'Field'=>'COGSAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PrefVendor_ListID' => (object) array(
										'Field'=>'PrefVendor_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PrefVendor_FullName' => (object) array(
										'Field'=>'PrefVendor_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetAccount_ListID' => (object) array(
										'Field'=>'AssetAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'AssetAccount_FullName' => (object) array(
										'Field'=>'AssetAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ReorderPoint' => (object) array(
										'Field'=>'ReorderPoint',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'QuantityOnHand' => (object) array(
										'Field'=>'QuantityOnHand',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'AverageCost' => (object) array(
										'Field'=>'AverageCost',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'QuantityOnOrder' => (object) array(
										'Field'=>'QuantityOnOrder',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'QuantityOnSalesOrder' => (object) array(
										'Field'=>'QuantityOnSalesOrder',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_iteminventory` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_iteminventory` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_iteminventory` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_iteminventory` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_iteminventory` ADD  `Name` varchar(31) NULL   ;",
			'FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `FullName` varchar(255) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_iteminventory` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'Parent_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `Parent_ListID` varchar(40) NULL   ;",
			'Parent_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `Parent_FullName` varchar(255) NULL   ;",
			'Sublevel' => "ALTER TABLE  `qb_iteminventory` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';",
			'ManufacturerPartNumber' => "ALTER TABLE  `qb_iteminventory` ADD  `ManufacturerPartNumber` varchar(31) NULL   ;",
			'UnitOfMeasureSet_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;",
			'UnitOfMeasureSet_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'SalesDesc' => "ALTER TABLE  `qb_iteminventory` ADD  `SalesDesc` text NULL   ;",
			'SalesPrice' => "ALTER TABLE  `qb_iteminventory` ADD  `SalesPrice` decimal(13,5) NULL   ;",
			'IncomeAccount_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `IncomeAccount_ListID` varchar(40) NULL   ;",
			'IncomeAccount_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `IncomeAccount_FullName` varchar(255) NULL   ;",
			'PurchaseDesc' => "ALTER TABLE  `qb_iteminventory` ADD  `PurchaseDesc` text NULL   ;",
			'PurchaseCost' => "ALTER TABLE  `qb_iteminventory` ADD  `PurchaseCost` decimal(13,5) NULL   ;",
			'COGSAccount_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `COGSAccount_ListID` varchar(40) NULL   ;",
			'COGSAccount_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `COGSAccount_FullName` varchar(255) NULL   ;",
			'PrefVendor_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `PrefVendor_ListID` varchar(40) NULL   ;",
			'PrefVendor_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `PrefVendor_FullName` varchar(255) NULL   ;",
			'AssetAccount_ListID' => "ALTER TABLE  `qb_iteminventory` ADD  `AssetAccount_ListID` varchar(40) NULL   ;",
			'AssetAccount_FullName' => "ALTER TABLE  `qb_iteminventory` ADD  `AssetAccount_FullName` varchar(255) NULL   ;",
			'ReorderPoint' => "ALTER TABLE  `qb_iteminventory` ADD  `ReorderPoint` decimal(12,5) NULL   DEFAULT '0.00000';",
			'QuantityOnHand' => "ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnHand` decimal(12,5) NULL   DEFAULT '0.00000';",
			'AverageCost' => "ALTER TABLE  `qb_iteminventory` ADD  `AverageCost` decimal(13,5) NULL   ;",
			'QuantityOnOrder' => "ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnOrder` decimal(12,5) NULL   DEFAULT '0.00000';",
			'QuantityOnSalesOrder' => "ALTER TABLE  `qb_iteminventory` ADD  `QuantityOnSalesOrder` decimal(12,5) NULL   DEFAULT '0.00000';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setFullname() - FullName
//setIsactive() - IsActive
//setParentListid() - Parent_ListID
//setParentFullname() - Parent_FullName
//setSublevel() - Sublevel
//setManufacturerpartnumber() - ManufacturerPartNumber
//setUnitofmeasuresetListid() - UnitOfMeasureSet_ListID
//setUnitofmeasuresetFullname() - UnitOfMeasureSet_FullName
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setSalesdesc() - SalesDesc
//setSalesprice() - SalesPrice
//setIncomeaccountListid() - IncomeAccount_ListID
//setIncomeaccountFullname() - IncomeAccount_FullName
//setPurchasedesc() - PurchaseDesc
//setPurchasecost() - PurchaseCost
//setCogsaccountListid() - COGSAccount_ListID
//setCogsaccountFullname() - COGSAccount_FullName
//setPrefvendorListid() - PrefVendor_ListID
//setPrefvendorFullname() - PrefVendor_FullName
//setAssetaccountListid() - AssetAccount_ListID
//setAssetaccountFullname() - AssetAccount_FullName
//setReorderpoint() - ReorderPoint
//setQuantityonhand() - QuantityOnHand
//setAveragecost() - AverageCost
//setQuantityonorder() - QuantityOnOrder
//setQuantityonsalesorder() - QuantityOnSalesOrder

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_FullName() - FullName
//set_IsActive() - IsActive
//set_Parent_ListID() - Parent_ListID
//set_Parent_FullName() - Parent_FullName
//set_Sublevel() - Sublevel
//set_ManufacturerPartNumber() - ManufacturerPartNumber
//set_UnitOfMeasureSet_ListID() - UnitOfMeasureSet_ListID
//set_UnitOfMeasureSet_FullName() - UnitOfMeasureSet_FullName
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_SalesDesc() - SalesDesc
//set_SalesPrice() - SalesPrice
//set_IncomeAccount_ListID() - IncomeAccount_ListID
//set_IncomeAccount_FullName() - IncomeAccount_FullName
//set_PurchaseDesc() - PurchaseDesc
//set_PurchaseCost() - PurchaseCost
//set_COGSAccount_ListID() - COGSAccount_ListID
//set_COGSAccount_FullName() - COGSAccount_FullName
//set_PrefVendor_ListID() - PrefVendor_ListID
//set_PrefVendor_FullName() - PrefVendor_FullName
//set_AssetAccount_ListID() - AssetAccount_ListID
//set_AssetAccount_FullName() - AssetAccount_FullName
//set_ReorderPoint() - ReorderPoint
//set_QuantityOnHand() - QuantityOnHand
//set_AverageCost() - AverageCost
//set_QuantityOnOrder() - QuantityOnOrder
//set_QuantityOnSalesOrder() - QuantityOnSalesOrder

*/
/* End of file Qb_iteminventory_model.php */
/* Location: ./application/models/Qb_iteminventory_model.php */
