<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_timetracking_model Class
 *
 * Manipulates `qb_timetracking` table on database

CREATE TABLE `qb_timetracking` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `TxnDate` date DEFAULT NULL,
  `Entity_ListID` varchar(40) DEFAULT NULL,
  `Entity_FullName` varchar(255) DEFAULT NULL,
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `ItemService_ListID` varchar(40) DEFAULT NULL,
  `ItemService_FullName` varchar(255) DEFAULT NULL,
  `Duration` text,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `PayrollItemWage_ListID` varchar(40) DEFAULT NULL,
  `PayrollItemWage_FullName` varchar(255) DEFAULT NULL,
  `Notes` text,
  `BillableStatus` varchar(40) DEFAULT NULL,
  `IsBillable` tinyint(1) DEFAULT '0',
  `IsBilled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`qbxml_id`),
  KEY `TxnDate` (`TxnDate`),
  KEY `Entity_ListID` (`Entity_ListID`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `ItemService_ListID` (`ItemService_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `PayrollItemWage_ListID` (`PayrollItemWage_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_timetracking` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_timetracking` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_timetracking` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Entity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Entity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `ItemService_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `ItemService_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Duration` text NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `PayrollItemWage_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `PayrollItemWage_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `BillableStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_timetracking` ADD  `IsBillable` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_timetracking` ADD  `IsBilled` tinyint(1) NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_timetracking_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $TxnDate;
	protected $Entity_ListID;
	protected $Entity_FullName;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $ItemService_ListID;
	protected $ItemService_FullName;
	protected $Duration;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $PayrollItemWage_ListID;
	protected $PayrollItemWage_FullName;
	protected $Notes;
	protected $BillableStatus;
	protected $IsBillable;
	protected $IsBilled;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_timetracking';
		$this->_short_name = 'qb_timetracking';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","TxnDate","Entity_ListID","Entity_FullName","Customer_ListID","Customer_FullName","ItemService_ListID","ItemService_FullName","Duration","Class_ListID","Class_FullName","PayrollItemWage_ListID","PayrollItemWage_FullName","Notes","BillableStatus","IsBillable","IsBilled");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: Entity_ListID -------------------------------------- 

	/** 
	* Sets a value to `Entity_ListID` variable
	* @access public
	*/

	public function setEntityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_ListID` variable
	* @access public
	*/

	public function getEntityListid() {
		return $this->Entity_ListID;
	}

	public function get_Entity_ListID() {
		return $this->Entity_ListID;
	}

	
// ------------------------------ End Field: Entity_ListID --------------------------------------


// ---------------------------- Start Field: Entity_FullName -------------------------------------- 

	/** 
	* Sets a value to `Entity_FullName` variable
	* @access public
	*/

	public function setEntityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Entity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Entity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Entity_FullName` variable
	* @access public
	*/

	public function getEntityFullname() {
		return $this->Entity_FullName;
	}

	public function get_Entity_FullName() {
		return $this->Entity_FullName;
	}

	
// ------------------------------ End Field: Entity_FullName --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: ItemService_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemService_ListID` variable
	* @access public
	*/

	public function setItemserviceListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemService_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemService_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemService_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemService_ListID` variable
	* @access public
	*/

	public function getItemserviceListid() {
		return $this->ItemService_ListID;
	}

	public function get_ItemService_ListID() {
		return $this->ItemService_ListID;
	}

	
// ------------------------------ End Field: ItemService_ListID --------------------------------------


// ---------------------------- Start Field: ItemService_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemService_FullName` variable
	* @access public
	*/

	public function setItemserviceFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemService_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemService_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemService_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemService_FullName` variable
	* @access public
	*/

	public function getItemserviceFullname() {
		return $this->ItemService_FullName;
	}

	public function get_ItemService_FullName() {
		return $this->ItemService_FullName;
	}

	
// ------------------------------ End Field: ItemService_FullName --------------------------------------


// ---------------------------- Start Field: Duration -------------------------------------- 

	/** 
	* Sets a value to `Duration` variable
	* @access public
	*/

	public function setDuration($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Duration', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Duration($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Duration', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Duration` variable
	* @access public
	*/

	public function getDuration() {
		return $this->Duration;
	}

	public function get_Duration() {
		return $this->Duration;
	}

	
// ------------------------------ End Field: Duration --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: PayrollItemWage_ListID -------------------------------------- 

	/** 
	* Sets a value to `PayrollItemWage_ListID` variable
	* @access public
	*/

	public function setPayrollitemwageListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayrollItemWage_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayrollItemWage_ListID` variable
	* @access public
	*/

	public function getPayrollitemwageListid() {
		return $this->PayrollItemWage_ListID;
	}

	public function get_PayrollItemWage_ListID() {
		return $this->PayrollItemWage_ListID;
	}

	
// ------------------------------ End Field: PayrollItemWage_ListID --------------------------------------


// ---------------------------- Start Field: PayrollItemWage_FullName -------------------------------------- 

	/** 
	* Sets a value to `PayrollItemWage_FullName` variable
	* @access public
	*/

	public function setPayrollitemwageFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayrollItemWage_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayrollItemWage_FullName` variable
	* @access public
	*/

	public function getPayrollitemwageFullname() {
		return $this->PayrollItemWage_FullName;
	}

	public function get_PayrollItemWage_FullName() {
		return $this->PayrollItemWage_FullName;
	}

	
// ------------------------------ End Field: PayrollItemWage_FullName --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: BillableStatus -------------------------------------- 

	/** 
	* Sets a value to `BillableStatus` variable
	* @access public
	*/

	public function setBillablestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableStatus` variable
	* @access public
	*/

	public function getBillablestatus() {
		return $this->BillableStatus;
	}

	public function get_BillableStatus() {
		return $this->BillableStatus;
	}

	
// ------------------------------ End Field: BillableStatus --------------------------------------


// ---------------------------- Start Field: IsBillable -------------------------------------- 

	/** 
	* Sets a value to `IsBillable` variable
	* @access public
	*/

	public function setIsbillable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsBillable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsBillable($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsBillable', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsBillable` variable
	* @access public
	*/

	public function getIsbillable() {
		return $this->IsBillable;
	}

	public function get_IsBillable() {
		return $this->IsBillable;
	}

	
// ------------------------------ End Field: IsBillable --------------------------------------


// ---------------------------- Start Field: IsBilled -------------------------------------- 

	/** 
	* Sets a value to `IsBilled` variable
	* @access public
	*/

	public function setIsbilled($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsBilled', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsBilled($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsBilled', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsBilled` variable
	* @access public
	*/

	public function getIsbilled() {
		return $this->IsBilled;
	}

	public function get_IsBilled() {
		return $this->IsBilled;
	}

	
// ------------------------------ End Field: IsBilled --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_ListID' => (object) array(
										'Field'=>'Entity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Entity_FullName' => (object) array(
										'Field'=>'Entity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemService_ListID' => (object) array(
										'Field'=>'ItemService_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemService_FullName' => (object) array(
										'Field'=>'ItemService_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Duration' => (object) array(
										'Field'=>'Duration',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PayrollItemWage_ListID' => (object) array(
										'Field'=>'PayrollItemWage_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PayrollItemWage_FullName' => (object) array(
										'Field'=>'PayrollItemWage_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableStatus' => (object) array(
										'Field'=>'BillableStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsBillable' => (object) array(
										'Field'=>'IsBillable',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'IsBilled' => (object) array(
										'Field'=>'IsBilled',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_timetracking` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_timetracking` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_timetracking` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_timetracking` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_timetracking` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_timetracking` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'TxnDate' => "ALTER TABLE  `qb_timetracking` ADD  `TxnDate` date NULL   ;",
			'Entity_ListID' => "ALTER TABLE  `qb_timetracking` ADD  `Entity_ListID` varchar(40) NULL   ;",
			'Entity_FullName' => "ALTER TABLE  `qb_timetracking` ADD  `Entity_FullName` varchar(255) NULL   ;",
			'Customer_ListID' => "ALTER TABLE  `qb_timetracking` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_timetracking` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'ItemService_ListID' => "ALTER TABLE  `qb_timetracking` ADD  `ItemService_ListID` varchar(40) NULL   ;",
			'ItemService_FullName' => "ALTER TABLE  `qb_timetracking` ADD  `ItemService_FullName` varchar(255) NULL   ;",
			'Duration' => "ALTER TABLE  `qb_timetracking` ADD  `Duration` text NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_timetracking` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_timetracking` ADD  `Class_FullName` varchar(255) NULL   ;",
			'PayrollItemWage_ListID' => "ALTER TABLE  `qb_timetracking` ADD  `PayrollItemWage_ListID` varchar(40) NULL   ;",
			'PayrollItemWage_FullName' => "ALTER TABLE  `qb_timetracking` ADD  `PayrollItemWage_FullName` varchar(255) NULL   ;",
			'Notes' => "ALTER TABLE  `qb_timetracking` ADD  `Notes` text NULL   ;",
			'BillableStatus' => "ALTER TABLE  `qb_timetracking` ADD  `BillableStatus` varchar(40) NULL   ;",
			'IsBillable' => "ALTER TABLE  `qb_timetracking` ADD  `IsBillable` tinyint(1) NULL   DEFAULT '0';",
			'IsBilled' => "ALTER TABLE  `qb_timetracking` ADD  `IsBilled` tinyint(1) NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setTxndate() - TxnDate
//setEntityListid() - Entity_ListID
//setEntityFullname() - Entity_FullName
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setItemserviceListid() - ItemService_ListID
//setItemserviceFullname() - ItemService_FullName
//setDuration() - Duration
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setPayrollitemwageListid() - PayrollItemWage_ListID
//setPayrollitemwageFullname() - PayrollItemWage_FullName
//setNotes() - Notes
//setBillablestatus() - BillableStatus
//setIsbillable() - IsBillable
//setIsbilled() - IsBilled

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_TxnDate() - TxnDate
//set_Entity_ListID() - Entity_ListID
//set_Entity_FullName() - Entity_FullName
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_ItemService_ListID() - ItemService_ListID
//set_ItemService_FullName() - ItemService_FullName
//set_Duration() - Duration
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_PayrollItemWage_ListID() - PayrollItemWage_ListID
//set_PayrollItemWage_FullName() - PayrollItemWage_FullName
//set_Notes() - Notes
//set_BillableStatus() - BillableStatus
//set_IsBillable() - IsBillable
//set_IsBilled() - IsBilled

*/
/* End of file Qb_timetracking_model.php */
/* Location: ./application/models/Qb_timetracking_model.php */
