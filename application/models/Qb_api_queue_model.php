<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_api_queue_model Class
 *
 * Manipulates `qb_api_queue` table on database

CREATE TABLE `qb_api_queue` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(100) NOT NULL,
  `request_method` varchar(100) NOT NULL,
  `done` int(1) NOT NULL DEFAULT '0',
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=341 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_api_queue` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_api_queue` ADD  `ticket_id` varchar(100) NOT NULL   ;
ALTER TABLE  `qb_api_queue` ADD  `request_method` varchar(100) NOT NULL   ;
ALTER TABLE  `qb_api_queue` ADD  `done` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `qb_api_queue` ADD  `options` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_api_queue_model extends MY_Model {

	protected $id;
	protected $ticket_id;
	protected $request_method;
	protected $done;
	protected $options;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_api_queue';
		$this->_short_name = 'qb_api_queue';
		$this->_fields = array("id","ticket_id","request_method","done","options");
		$this->_required = array("ticket_id","request_method","done");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: ticket_id -------------------------------------- 

	/** 
	* Sets a value to `ticket_id` variable
	* @access public
	*/

	public function setTicketId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ticket_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ticket_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ticket_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ticket_id` variable
	* @access public
	*/

	public function getTicketId() {
		return $this->ticket_id;
	}

	public function get_ticket_id() {
		return $this->ticket_id;
	}

	
// ------------------------------ End Field: ticket_id --------------------------------------


// ---------------------------- Start Field: request_method -------------------------------------- 

	/** 
	* Sets a value to `request_method` variable
	* @access public
	*/

	public function setRequestMethod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('request_method', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_request_method($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('request_method', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `request_method` variable
	* @access public
	*/

	public function getRequestMethod() {
		return $this->request_method;
	}

	public function get_request_method() {
		return $this->request_method;
	}

	
// ------------------------------ End Field: request_method --------------------------------------


// ---------------------------- Start Field: done -------------------------------------- 

	/** 
	* Sets a value to `done` variable
	* @access public
	*/

	public function setDone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('done', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_done($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('done', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `done` variable
	* @access public
	*/

	public function getDone() {
		return $this->done;
	}

	public function get_done() {
		return $this->done;
	}

	
// ------------------------------ End Field: done --------------------------------------


// ---------------------------- Start Field: options -------------------------------------- 

	/** 
	* Sets a value to `options` variable
	* @access public
	*/

	public function setOptions($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('options', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_options($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('options', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `options` variable
	* @access public
	*/

	public function getOptions() {
		return $this->options;
	}

	public function get_options() {
		return $this->options;
	}

	
// ------------------------------ End Field: options --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ticket_id' => (object) array(
										'Field'=>'ticket_id',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'request_method' => (object) array(
										'Field'=>'request_method',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'done' => (object) array(
										'Field'=>'done',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'options' => (object) array(
										'Field'=>'options',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `qb_api_queue` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ticket_id' => "ALTER TABLE  `qb_api_queue` ADD  `ticket_id` varchar(100) NOT NULL   ;",
			'request_method' => "ALTER TABLE  `qb_api_queue` ADD  `request_method` varchar(100) NOT NULL   ;",
			'done' => "ALTER TABLE  `qb_api_queue` ADD  `done` int(1) NOT NULL   DEFAULT '0';",
			'options' => "ALTER TABLE  `qb_api_queue` ADD  `options` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setTicketId() - ticket_id
//setRequestMethod() - request_method
//setDone() - done
//setOptions() - options

--------------------------------------

//set_id() - id
//set_ticket_id() - ticket_id
//set_request_method() - request_method
//set_done() - done
//set_options() - options

*/
/* End of file Qb_api_queue_model.php */
/* Location: ./application/models/Qb_api_queue_model.php */
