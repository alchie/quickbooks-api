<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_host_model Class
 *
 * Manipulates `qb_host` table on database

CREATE TABLE `qb_host` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductName` text,
  `MajorVersion` text,
  `MinorVersion` text,
  `Country` text,
  `SupportedQBXMLVersion` text,
  `IsAutomaticLogin` tinyint(1) DEFAULT '0',
  `QBFileMode` varchar(40) DEFAULT NULL,
  `ListMetaData` text,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_host` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_host` ADD  `ProductName` text NULL   ;
ALTER TABLE  `qb_host` ADD  `MajorVersion` text NULL   ;
ALTER TABLE  `qb_host` ADD  `MinorVersion` text NULL   ;
ALTER TABLE  `qb_host` ADD  `Country` text NULL   ;
ALTER TABLE  `qb_host` ADD  `SupportedQBXMLVersion` text NULL   ;
ALTER TABLE  `qb_host` ADD  `IsAutomaticLogin` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_host` ADD  `QBFileMode` varchar(40) NULL   ;
ALTER TABLE  `qb_host` ADD  `ListMetaData` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_host_model extends MY_Model {

	protected $qbxml_id;
	protected $ProductName;
	protected $MajorVersion;
	protected $MinorVersion;
	protected $Country;
	protected $SupportedQBXMLVersion;
	protected $IsAutomaticLogin;
	protected $QBFileMode;
	protected $ListMetaData;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_host';
		$this->_short_name = 'qb_host';
		$this->_fields = array("qbxml_id","ProductName","MajorVersion","MinorVersion","Country","SupportedQBXMLVersion","IsAutomaticLogin","QBFileMode","ListMetaData");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ProductName -------------------------------------- 

	/** 
	* Sets a value to `ProductName` variable
	* @access public
	*/

	public function setProductname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ProductName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ProductName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ProductName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ProductName` variable
	* @access public
	*/

	public function getProductname() {
		return $this->ProductName;
	}

	public function get_ProductName() {
		return $this->ProductName;
	}

	
// ------------------------------ End Field: ProductName --------------------------------------


// ---------------------------- Start Field: MajorVersion -------------------------------------- 

	/** 
	* Sets a value to `MajorVersion` variable
	* @access public
	*/

	public function setMajorversion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MajorVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MajorVersion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MajorVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MajorVersion` variable
	* @access public
	*/

	public function getMajorversion() {
		return $this->MajorVersion;
	}

	public function get_MajorVersion() {
		return $this->MajorVersion;
	}

	
// ------------------------------ End Field: MajorVersion --------------------------------------


// ---------------------------- Start Field: MinorVersion -------------------------------------- 

	/** 
	* Sets a value to `MinorVersion` variable
	* @access public
	*/

	public function setMinorversion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MinorVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MinorVersion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MinorVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MinorVersion` variable
	* @access public
	*/

	public function getMinorversion() {
		return $this->MinorVersion;
	}

	public function get_MinorVersion() {
		return $this->MinorVersion;
	}

	
// ------------------------------ End Field: MinorVersion --------------------------------------


// ---------------------------- Start Field: Country -------------------------------------- 

	/** 
	* Sets a value to `Country` variable
	* @access public
	*/

	public function setCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Country` variable
	* @access public
	*/

	public function getCountry() {
		return $this->Country;
	}

	public function get_Country() {
		return $this->Country;
	}

	
// ------------------------------ End Field: Country --------------------------------------


// ---------------------------- Start Field: SupportedQBXMLVersion -------------------------------------- 

	/** 
	* Sets a value to `SupportedQBXMLVersion` variable
	* @access public
	*/

	public function setSupportedqbxmlversion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SupportedQBXMLVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SupportedQBXMLVersion($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SupportedQBXMLVersion', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SupportedQBXMLVersion` variable
	* @access public
	*/

	public function getSupportedqbxmlversion() {
		return $this->SupportedQBXMLVersion;
	}

	public function get_SupportedQBXMLVersion() {
		return $this->SupportedQBXMLVersion;
	}

	
// ------------------------------ End Field: SupportedQBXMLVersion --------------------------------------


// ---------------------------- Start Field: IsAutomaticLogin -------------------------------------- 

	/** 
	* Sets a value to `IsAutomaticLogin` variable
	* @access public
	*/

	public function setIsautomaticlogin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsAutomaticLogin', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsAutomaticLogin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsAutomaticLogin', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsAutomaticLogin` variable
	* @access public
	*/

	public function getIsautomaticlogin() {
		return $this->IsAutomaticLogin;
	}

	public function get_IsAutomaticLogin() {
		return $this->IsAutomaticLogin;
	}

	
// ------------------------------ End Field: IsAutomaticLogin --------------------------------------


// ---------------------------- Start Field: QBFileMode -------------------------------------- 

	/** 
	* Sets a value to `QBFileMode` variable
	* @access public
	*/

	public function setQbfilemode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QBFileMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_QBFileMode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('QBFileMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `QBFileMode` variable
	* @access public
	*/

	public function getQbfilemode() {
		return $this->QBFileMode;
	}

	public function get_QBFileMode() {
		return $this->QBFileMode;
	}

	
// ------------------------------ End Field: QBFileMode --------------------------------------


// ---------------------------- Start Field: ListMetaData -------------------------------------- 

	/** 
	* Sets a value to `ListMetaData` variable
	* @access public
	*/

	public function setListmetadata($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListMetaData', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListMetaData($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListMetaData', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListMetaData` variable
	* @access public
	*/

	public function getListmetadata() {
		return $this->ListMetaData;
	}

	public function get_ListMetaData() {
		return $this->ListMetaData;
	}

	
// ------------------------------ End Field: ListMetaData --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ProductName' => (object) array(
										'Field'=>'ProductName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MajorVersion' => (object) array(
										'Field'=>'MajorVersion',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MinorVersion' => (object) array(
										'Field'=>'MinorVersion',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Country' => (object) array(
										'Field'=>'Country',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SupportedQBXMLVersion' => (object) array(
										'Field'=>'SupportedQBXMLVersion',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsAutomaticLogin' => (object) array(
										'Field'=>'IsAutomaticLogin',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'QBFileMode' => (object) array(
										'Field'=>'QBFileMode',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ListMetaData' => (object) array(
										'Field'=>'ListMetaData',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_host` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ProductName' => "ALTER TABLE  `qb_host` ADD  `ProductName` text NULL   ;",
			'MajorVersion' => "ALTER TABLE  `qb_host` ADD  `MajorVersion` text NULL   ;",
			'MinorVersion' => "ALTER TABLE  `qb_host` ADD  `MinorVersion` text NULL   ;",
			'Country' => "ALTER TABLE  `qb_host` ADD  `Country` text NULL   ;",
			'SupportedQBXMLVersion' => "ALTER TABLE  `qb_host` ADD  `SupportedQBXMLVersion` text NULL   ;",
			'IsAutomaticLogin' => "ALTER TABLE  `qb_host` ADD  `IsAutomaticLogin` tinyint(1) NULL   DEFAULT '0';",
			'QBFileMode' => "ALTER TABLE  `qb_host` ADD  `QBFileMode` varchar(40) NULL   ;",
			'ListMetaData' => "ALTER TABLE  `qb_host` ADD  `ListMetaData` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setProductname() - ProductName
//setMajorversion() - MajorVersion
//setMinorversion() - MinorVersion
//setCountry() - Country
//setSupportedqbxmlversion() - SupportedQBXMLVersion
//setIsautomaticlogin() - IsAutomaticLogin
//setQbfilemode() - QBFileMode
//setListmetadata() - ListMetaData

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ProductName() - ProductName
//set_MajorVersion() - MajorVersion
//set_MinorVersion() - MinorVersion
//set_Country() - Country
//set_SupportedQBXMLVersion() - SupportedQBXMLVersion
//set_IsAutomaticLogin() - IsAutomaticLogin
//set_QBFileMode() - QBFileMode
//set_ListMetaData() - ListMetaData

*/
/* End of file Qb_host_model.php */
/* Location: ./application/models/Qb_host_model.php */
