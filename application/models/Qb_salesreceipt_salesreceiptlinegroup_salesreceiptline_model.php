<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_salesreceipt_salesreceiptlinegroup_salesreceiptline_model Class
 *
 * Manipulates `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` table on database

CREATE TABLE `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesReceipt_TxnID` varchar(40) DEFAULT NULL,
  `SalesReceipt_SalesReceiptLineGroup_TxnLineID` text,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  `OverrideUOMSet_ListID` varchar(40) DEFAULT NULL,
  `OverrideUOMSet_FullName` varchar(255) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `RatePercent` decimal(12,5) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `ServiceDate` date DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `Other1` text,
  `Other2` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned DEFAULT '0',
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned DEFAULT '0',
  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` text,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned DEFAULT '0',
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` text,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` text,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` text,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` text,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` text,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned DEFAULT '0',
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned DEFAULT '0',
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `SalesReceipt_TxnID` (`SalesReceipt_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `OverrideUOMSet_ListID` (`OverrideUOMSet_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesReceipt_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesReceipt_SalesReceiptLineGroup_TxnLineID` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `RatePercent` decimal(12,5) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `ServiceDate` date NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Other1` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Other2` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` text NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_salesreceipt_salesreceiptlinegroup_salesreceiptline_model extends MY_Model {

	protected $qbxml_id;
	protected $SalesReceipt_TxnID;
	protected $SalesReceipt_SalesReceiptLineGroup_TxnLineID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSet_ListID;
	protected $OverrideUOMSet_FullName;
	protected $Rate;
	protected $RatePercent;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Amount;
	protected $ServiceDate;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $Other1;
	protected $Other2;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_salesreceipt_salesreceiptlinegroup_salesreceiptline';
		$this->_short_name = 'qb_salesreceipt_salesreceiptlinegroup_salesreceiptline';
		$this->_fields = array("qbxml_id","SalesReceipt_TxnID","SalesReceipt_SalesReceiptLineGroup_TxnLineID","SortOrder","TxnLineID","Item_ListID","Item_FullName","Descrip","Quantity","UnitOfMeasure","OverrideUOMSet_ListID","OverrideUOMSet_FullName","Rate","RatePercent","Class_ListID","Class_FullName","Amount","ServiceDate","SalesTaxCode_ListID","SalesTaxCode_FullName","Other1","Other2","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear","CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode","CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode","CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage","CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID","CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber","CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip","CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch","CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp","CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: SalesReceipt_TxnID -------------------------------------- 

	/** 
	* Sets a value to `SalesReceipt_TxnID` variable
	* @access public
	*/

	public function setSalesreceiptTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesReceipt_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesReceipt_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesReceipt_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesReceipt_TxnID` variable
	* @access public
	*/

	public function getSalesreceiptTxnid() {
		return $this->SalesReceipt_TxnID;
	}

	public function get_SalesReceipt_TxnID() {
		return $this->SalesReceipt_TxnID;
	}

	
// ------------------------------ End Field: SalesReceipt_TxnID --------------------------------------


// ---------------------------- Start Field: SalesReceipt_SalesReceiptLineGroup_TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `SalesReceipt_SalesReceiptLineGroup_TxnLineID` variable
	* @access public
	*/

	public function setSalesreceiptSalesreceiptlinegroupTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesReceipt_SalesReceiptLineGroup_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesReceipt_SalesReceiptLineGroup_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesReceipt_SalesReceiptLineGroup_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesReceipt_SalesReceiptLineGroup_TxnLineID` variable
	* @access public
	*/

	public function getSalesreceiptSalesreceiptlinegroupTxnlineid() {
		return $this->SalesReceipt_SalesReceiptLineGroup_TxnLineID;
	}

	public function get_SalesReceipt_SalesReceiptLineGroup_TxnLineID() {
		return $this->SalesReceipt_SalesReceiptLineGroup_TxnLineID;
	}

	
// ------------------------------ End Field: SalesReceipt_SalesReceiptLineGroup_TxnLineID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function setOverrideuomsetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function getOverrideuomsetListid() {
		return $this->OverrideUOMSet_ListID;
	}

	public function get_OverrideUOMSet_ListID() {
		return $this->OverrideUOMSet_ListID;
	}

	
// ------------------------------ End Field: OverrideUOMSet_ListID --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function setOverrideuomsetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function getOverrideuomsetFullname() {
		return $this->OverrideUOMSet_FullName;
	}

	public function get_OverrideUOMSet_FullName() {
		return $this->OverrideUOMSet_FullName;
	}

	
// ------------------------------ End Field: OverrideUOMSet_FullName --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: RatePercent -------------------------------------- 

	/** 
	* Sets a value to `RatePercent` variable
	* @access public
	*/

	public function setRatepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RatePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RatePercent` variable
	* @access public
	*/

	public function getRatepercent() {
		return $this->RatePercent;
	}

	public function get_RatePercent() {
		return $this->RatePercent;
	}

	
// ------------------------------ End Field: RatePercent --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: ServiceDate -------------------------------------- 

	/** 
	* Sets a value to `ServiceDate` variable
	* @access public
	*/

	public function setServicedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ServiceDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ServiceDate` variable
	* @access public
	*/

	public function getServicedate() {
		return $this->ServiceDate;
	}

	public function get_ServiceDate() {
		return $this->ServiceDate;
	}

	
// ------------------------------ End Field: ServiceDate --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: Other1 -------------------------------------- 

	/** 
	* Sets a value to `Other1` variable
	* @access public
	*/

	public function setOther1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other1` variable
	* @access public
	*/

	public function getOther1() {
		return $this->Other1;
	}

	public function get_Other1() {
		return $this->Other1;
	}

	
// ------------------------------ End Field: Other1 --------------------------------------


// ---------------------------- Start Field: Other2 -------------------------------------- 

	/** 
	* Sets a value to `Other2` variable
	* @access public
	*/

	public function setOther2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other2` variable
	* @access public
	*/

	public function getOther2() {
		return $this->Other2;
	}

	public function get_Other2() {
		return $this->Other2;
	}

	
// ------------------------------ End Field: Other2 --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationyear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoNameoncard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoNameoncard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoTransactionmode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoTransactionmode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultmessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultmessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvszip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvszip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoClienttransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoClienttransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'SalesReceipt_TxnID' => (object) array(
										'Field'=>'SalesReceipt_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesReceipt_SalesReceiptLineGroup_TxnLineID' => (object) array(
										'Field'=>'SalesReceipt_SalesReceiptLineGroup_TxnLineID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_ListID' => (object) array(
										'Field'=>'OverrideUOMSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_FullName' => (object) array(
										'Field'=>'OverrideUOMSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RatePercent' => (object) array(
										'Field'=>'RatePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ServiceDate' => (object) array(
										'Field'=>'ServiceDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other1' => (object) array(
										'Field'=>'Other1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other2' => (object) array(
										'Field'=>'Other2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'SalesReceipt_TxnID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesReceipt_TxnID` varchar(40) NULL   ;",
			'SalesReceipt_SalesReceiptLineGroup_TxnLineID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesReceipt_SalesReceiptLineGroup_TxnLineID` text NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSet_ListID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;",
			'OverrideUOMSet_FullName' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Rate` decimal(13,5) NULL   ;",
			'RatePercent' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `RatePercent` decimal(12,5) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Amount` decimal(10,2) NULL   ;",
			'ServiceDate' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `ServiceDate` date NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'Other1' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Other1` text NULL   ;",
			'Other2' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `Other2` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   DEFAULT '0';",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   DEFAULT '0';",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   DEFAULT '0';",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` text NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   DEFAULT '0';",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   DEFAULT '0';",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => "ALTER TABLE  `qb_salesreceipt_salesreceiptlinegroup_salesreceiptline` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setSalesreceiptTxnid() - SalesReceipt_TxnID
//setSalesreceiptSalesreceiptlinegroupTxnlineid() - SalesReceipt_SalesReceiptLineGroup_TxnLineID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetListid() - OverrideUOMSet_ListID
//setOverrideuomsetFullname() - OverrideUOMSet_FullName
//setRate() - Rate
//setRatepercent() - RatePercent
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAmount() - Amount
//setServicedate() - ServiceDate
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setOther1() - Other1
//setOther2() - Other2
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//setCreditcardtxninfoCreditcardtxninputinfoExpirationyear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//setCreditcardtxninfoCreditcardtxninputinfoNameoncard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//setCreditcardtxninfoCreditcardtxninputinfoTransactionmode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//setCreditcardtxninfoCreditcardtxnresultinfoResultcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//setCreditcardtxninfoCreditcardtxnresultinfoResultmessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//setCreditcardtxninfoCreditcardtxnresultinfoAvszip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//setCreditcardtxninfoCreditcardtxnresultinfoClienttransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_SalesReceipt_TxnID() - SalesReceipt_TxnID
//set_SalesReceipt_SalesReceiptLineGroup_TxnLineID() - SalesReceipt_SalesReceiptLineGroup_TxnLineID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSet_ListID() - OverrideUOMSet_ListID
//set_OverrideUOMSet_FullName() - OverrideUOMSet_FullName
//set_Rate() - Rate
//set_RatePercent() - RatePercent
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Amount() - Amount
//set_ServiceDate() - ServiceDate
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_Other1() - Other1
//set_Other2() - Other2
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID

*/
/* End of file Qb_salesreceipt_salesreceiptlinegroup_salesreceiptline_model.php */
/* Location: ./application/models/Qb_salesreceipt_salesreceiptlinegroup_salesreceiptline_model.php */
