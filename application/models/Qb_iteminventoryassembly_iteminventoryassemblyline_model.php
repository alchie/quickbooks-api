<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_iteminventoryassembly_iteminventoryassemblyline_model Class
 *
 * Manipulates `qb_iteminventoryassembly_iteminventoryassemblyline` table on database

CREATE TABLE `qb_iteminventoryassembly_iteminventoryassemblyline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ItemInventoryAssembly_ListID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `ItemInventory_ListID` varchar(40) DEFAULT NULL,
  `ItemInventory_FullName` varchar(255) DEFAULT NULL,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`qbxml_id`),
  KEY `ItemInventoryAssembly_ListID` (`ItemInventoryAssembly_ListID`),
  KEY `ItemInventory_ListID` (`ItemInventory_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventoryAssembly_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventory_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventory_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_iteminventoryassembly_iteminventoryassemblyline_model extends MY_Model {

	protected $qbxml_id;
	protected $ItemInventoryAssembly_ListID;
	protected $SortOrder;
	protected $ItemInventory_ListID;
	protected $ItemInventory_FullName;
	protected $Quantity;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_iteminventoryassembly_iteminventoryassemblyline';
		$this->_short_name = 'qb_iteminventoryassembly_iteminventoryassemblyline';
		$this->_fields = array("qbxml_id","ItemInventoryAssembly_ListID","SortOrder","ItemInventory_ListID","ItemInventory_FullName","Quantity");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ItemInventoryAssembly_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemInventoryAssembly_ListID` variable
	* @access public
	*/

	public function setIteminventoryassemblyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemInventoryAssembly_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventoryAssembly_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemInventoryAssembly_ListID` variable
	* @access public
	*/

	public function getIteminventoryassemblyListid() {
		return $this->ItemInventoryAssembly_ListID;
	}

	public function get_ItemInventoryAssembly_ListID() {
		return $this->ItemInventoryAssembly_ListID;
	}

	
// ------------------------------ End Field: ItemInventoryAssembly_ListID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: ItemInventory_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemInventory_ListID` variable
	* @access public
	*/

	public function setIteminventoryListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventory_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemInventory_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventory_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemInventory_ListID` variable
	* @access public
	*/

	public function getIteminventoryListid() {
		return $this->ItemInventory_ListID;
	}

	public function get_ItemInventory_ListID() {
		return $this->ItemInventory_ListID;
	}

	
// ------------------------------ End Field: ItemInventory_ListID --------------------------------------


// ---------------------------- Start Field: ItemInventory_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemInventory_FullName` variable
	* @access public
	*/

	public function setIteminventoryFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventory_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemInventory_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemInventory_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemInventory_FullName` variable
	* @access public
	*/

	public function getIteminventoryFullname() {
		return $this->ItemInventory_FullName;
	}

	public function get_ItemInventory_FullName() {
		return $this->ItemInventory_FullName;
	}

	
// ------------------------------ End Field: ItemInventory_FullName --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ItemInventoryAssembly_ListID' => (object) array(
										'Field'=>'ItemInventoryAssembly_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'ItemInventory_ListID' => (object) array(
										'Field'=>'ItemInventory_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemInventory_FullName' => (object) array(
										'Field'=>'ItemInventory_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ItemInventoryAssembly_ListID' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventoryAssembly_ListID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'ItemInventory_ListID' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventory_ListID` varchar(40) NULL   ;",
			'ItemInventory_FullName' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `ItemInventory_FullName` varchar(255) NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_iteminventoryassembly_iteminventoryassemblyline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setIteminventoryassemblyListid() - ItemInventoryAssembly_ListID
//setSortorder() - SortOrder
//setIteminventoryListid() - ItemInventory_ListID
//setIteminventoryFullname() - ItemInventory_FullName
//setQuantity() - Quantity

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ItemInventoryAssembly_ListID() - ItemInventoryAssembly_ListID
//set_SortOrder() - SortOrder
//set_ItemInventory_ListID() - ItemInventory_ListID
//set_ItemInventory_FullName() - ItemInventory_FullName
//set_Quantity() - Quantity

*/
/* End of file Qb_iteminventoryassembly_iteminventoryassemblyline_model.php */
/* Location: ./application/models/Qb_iteminventoryassembly_iteminventoryassemblyline_model.php */
