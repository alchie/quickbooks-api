<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_billpaymentcreditcard_appliedtotxn_model Class
 *
 * Manipulates `qb_billpaymentcreditcard_appliedtotxn` table on database

CREATE TABLE `qb_billpaymentcreditcard_appliedtotxn` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FromTxnID` varchar(40) DEFAULT NULL,
  `BillPaymentCreditCard_TxnID` varchar(40) DEFAULT NULL,
  `ToTxnID` varchar(40) DEFAULT NULL,
  `TxnType` varchar(40) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` text,
  `BalanceRemaining` decimal(10,2) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `DiscountAmount` decimal(10,2) DEFAULT NULL,
  `DiscountAccount_ListID` varchar(40) DEFAULT NULL,
  `DiscountAccount_FullName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `FromTxnID` (`FromTxnID`),
  KEY `BillPaymentCreditCard_TxnID` (`BillPaymentCreditCard_TxnID`),
  KEY `ToTxnID` (`ToTxnID`),
  KEY `TxnType` (`TxnType`),
  KEY `TxnDate` (`TxnDate`),
  KEY `DiscountAccount_ListID` (`DiscountAccount_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `FromTxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `BillPaymentCreditCard_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `ToTxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `TxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `RefNumber` text NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `BalanceRemaining` decimal(10,2) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAccount_FullName` varchar(255) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_billpaymentcreditcard_appliedtotxn_model extends MY_Model {

	protected $qbxml_id;
	protected $FromTxnID;
	protected $BillPaymentCreditCard_TxnID;
	protected $ToTxnID;
	protected $TxnType;
	protected $TxnDate;
	protected $RefNumber;
	protected $BalanceRemaining;
	protected $Amount;
	protected $DiscountAmount;
	protected $DiscountAccount_ListID;
	protected $DiscountAccount_FullName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_billpaymentcreditcard_appliedtotxn';
		$this->_short_name = 'qb_billpaymentcreditcard_appliedtotxn';
		$this->_fields = array("qbxml_id","FromTxnID","BillPaymentCreditCard_TxnID","ToTxnID","TxnType","TxnDate","RefNumber","BalanceRemaining","Amount","DiscountAmount","DiscountAccount_ListID","DiscountAccount_FullName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: FromTxnID -------------------------------------- 

	/** 
	* Sets a value to `FromTxnID` variable
	* @access public
	*/

	public function setFromtxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FromTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FromTxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FromTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FromTxnID` variable
	* @access public
	*/

	public function getFromtxnid() {
		return $this->FromTxnID;
	}

	public function get_FromTxnID() {
		return $this->FromTxnID;
	}

	
// ------------------------------ End Field: FromTxnID --------------------------------------


// ---------------------------- Start Field: BillPaymentCreditCard_TxnID -------------------------------------- 

	/** 
	* Sets a value to `BillPaymentCreditCard_TxnID` variable
	* @access public
	*/

	public function setBillpaymentcreditcardTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillPaymentCreditCard_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillPaymentCreditCard_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillPaymentCreditCard_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillPaymentCreditCard_TxnID` variable
	* @access public
	*/

	public function getBillpaymentcreditcardTxnid() {
		return $this->BillPaymentCreditCard_TxnID;
	}

	public function get_BillPaymentCreditCard_TxnID() {
		return $this->BillPaymentCreditCard_TxnID;
	}

	
// ------------------------------ End Field: BillPaymentCreditCard_TxnID --------------------------------------


// ---------------------------- Start Field: ToTxnID -------------------------------------- 

	/** 
	* Sets a value to `ToTxnID` variable
	* @access public
	*/

	public function setTotxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ToTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ToTxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ToTxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ToTxnID` variable
	* @access public
	*/

	public function getTotxnid() {
		return $this->ToTxnID;
	}

	public function get_ToTxnID() {
		return $this->ToTxnID;
	}

	
// ------------------------------ End Field: ToTxnID --------------------------------------


// ---------------------------- Start Field: TxnType -------------------------------------- 

	/** 
	* Sets a value to `TxnType` variable
	* @access public
	*/

	public function setTxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnType` variable
	* @access public
	*/

	public function getTxntype() {
		return $this->TxnType;
	}

	public function get_TxnType() {
		return $this->TxnType;
	}

	
// ------------------------------ End Field: TxnType --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: BalanceRemaining -------------------------------------- 

	/** 
	* Sets a value to `BalanceRemaining` variable
	* @access public
	*/

	public function setBalanceremaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BalanceRemaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BalanceRemaining` variable
	* @access public
	*/

	public function getBalanceremaining() {
		return $this->BalanceRemaining;
	}

	public function get_BalanceRemaining() {
		return $this->BalanceRemaining;
	}

	
// ------------------------------ End Field: BalanceRemaining --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: DiscountAmount -------------------------------------- 

	/** 
	* Sets a value to `DiscountAmount` variable
	* @access public
	*/

	public function setDiscountamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DiscountAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DiscountAmount` variable
	* @access public
	*/

	public function getDiscountamount() {
		return $this->DiscountAmount;
	}

	public function get_DiscountAmount() {
		return $this->DiscountAmount;
	}

	
// ------------------------------ End Field: DiscountAmount --------------------------------------


// ---------------------------- Start Field: DiscountAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `DiscountAccount_ListID` variable
	* @access public
	*/

	public function setDiscountaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DiscountAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DiscountAccount_ListID` variable
	* @access public
	*/

	public function getDiscountaccountListid() {
		return $this->DiscountAccount_ListID;
	}

	public function get_DiscountAccount_ListID() {
		return $this->DiscountAccount_ListID;
	}

	
// ------------------------------ End Field: DiscountAccount_ListID --------------------------------------


// ---------------------------- Start Field: DiscountAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `DiscountAccount_FullName` variable
	* @access public
	*/

	public function setDiscountaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DiscountAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DiscountAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DiscountAccount_FullName` variable
	* @access public
	*/

	public function getDiscountaccountFullname() {
		return $this->DiscountAccount_FullName;
	}

	public function get_DiscountAccount_FullName() {
		return $this->DiscountAccount_FullName;
	}

	
// ------------------------------ End Field: DiscountAccount_FullName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'FromTxnID' => (object) array(
										'Field'=>'FromTxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillPaymentCreditCard_TxnID' => (object) array(
										'Field'=>'BillPaymentCreditCard_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ToTxnID' => (object) array(
										'Field'=>'ToTxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnType' => (object) array(
										'Field'=>'TxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BalanceRemaining' => (object) array(
										'Field'=>'BalanceRemaining',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DiscountAmount' => (object) array(
										'Field'=>'DiscountAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DiscountAccount_ListID' => (object) array(
										'Field'=>'DiscountAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DiscountAccount_FullName' => (object) array(
										'Field'=>'DiscountAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'FromTxnID' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `FromTxnID` varchar(40) NULL   ;",
			'BillPaymentCreditCard_TxnID' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `BillPaymentCreditCard_TxnID` varchar(40) NULL   ;",
			'ToTxnID' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `ToTxnID` varchar(40) NULL   ;",
			'TxnType' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `TxnType` varchar(40) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `RefNumber` text NULL   ;",
			'BalanceRemaining' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `BalanceRemaining` decimal(10,2) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `Amount` decimal(10,2) NULL   ;",
			'DiscountAmount' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAmount` decimal(10,2) NULL   ;",
			'DiscountAccount_ListID' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAccount_ListID` varchar(40) NULL   ;",
			'DiscountAccount_FullName' => "ALTER TABLE  `qb_billpaymentcreditcard_appliedtotxn` ADD  `DiscountAccount_FullName` varchar(255) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setFromtxnid() - FromTxnID
//setBillpaymentcreditcardTxnid() - BillPaymentCreditCard_TxnID
//setTotxnid() - ToTxnID
//setTxntype() - TxnType
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setBalanceremaining() - BalanceRemaining
//setAmount() - Amount
//setDiscountamount() - DiscountAmount
//setDiscountaccountListid() - DiscountAccount_ListID
//setDiscountaccountFullname() - DiscountAccount_FullName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_FromTxnID() - FromTxnID
//set_BillPaymentCreditCard_TxnID() - BillPaymentCreditCard_TxnID
//set_ToTxnID() - ToTxnID
//set_TxnType() - TxnType
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_BalanceRemaining() - BalanceRemaining
//set_Amount() - Amount
//set_DiscountAmount() - DiscountAmount
//set_DiscountAccount_ListID() - DiscountAccount_ListID
//set_DiscountAccount_FullName() - DiscountAccount_FullName

*/
/* End of file Qb_billpaymentcreditcard_appliedtotxn_model.php */
/* Location: ./application/models/Qb_billpaymentcreditcard_appliedtotxn_model.php */
