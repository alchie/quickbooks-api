<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_employee_earnings_model Class
 *
 * Manipulates `qb_employee_earnings` table on database

CREATE TABLE `qb_employee_earnings` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Employee_ListID` varchar(40) DEFAULT NULL,
  `PayrollItemWage_ListID` varchar(40) DEFAULT NULL,
  `PayrollItemWage_FullName` varchar(255) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `RatePercent` decimal(12,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Employee_ListID` (`Employee_ListID`),
  KEY `PayrollItemWage_ListID` (`PayrollItemWage_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_employee_earnings` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_employee_earnings` ADD  `Employee_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_employee_earnings` ADD  `PayrollItemWage_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_employee_earnings` ADD  `PayrollItemWage_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_employee_earnings` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_employee_earnings` ADD  `RatePercent` decimal(12,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_employee_earnings_model extends MY_Model {

	protected $qbxml_id;
	protected $Employee_ListID;
	protected $PayrollItemWage_ListID;
	protected $PayrollItemWage_FullName;
	protected $Rate;
	protected $RatePercent;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_employee_earnings';
		$this->_short_name = 'qb_employee_earnings';
		$this->_fields = array("qbxml_id","Employee_ListID","PayrollItemWage_ListID","PayrollItemWage_FullName","Rate","RatePercent");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Employee_ListID -------------------------------------- 

	/** 
	* Sets a value to `Employee_ListID` variable
	* @access public
	*/

	public function setEmployeeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Employee_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Employee_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Employee_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Employee_ListID` variable
	* @access public
	*/

	public function getEmployeeListid() {
		return $this->Employee_ListID;
	}

	public function get_Employee_ListID() {
		return $this->Employee_ListID;
	}

	
// ------------------------------ End Field: Employee_ListID --------------------------------------


// ---------------------------- Start Field: PayrollItemWage_ListID -------------------------------------- 

	/** 
	* Sets a value to `PayrollItemWage_ListID` variable
	* @access public
	*/

	public function setPayrollitemwageListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayrollItemWage_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayrollItemWage_ListID` variable
	* @access public
	*/

	public function getPayrollitemwageListid() {
		return $this->PayrollItemWage_ListID;
	}

	public function get_PayrollItemWage_ListID() {
		return $this->PayrollItemWage_ListID;
	}

	
// ------------------------------ End Field: PayrollItemWage_ListID --------------------------------------


// ---------------------------- Start Field: PayrollItemWage_FullName -------------------------------------- 

	/** 
	* Sets a value to `PayrollItemWage_FullName` variable
	* @access public
	*/

	public function setPayrollitemwageFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayrollItemWage_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayrollItemWage_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayrollItemWage_FullName` variable
	* @access public
	*/

	public function getPayrollitemwageFullname() {
		return $this->PayrollItemWage_FullName;
	}

	public function get_PayrollItemWage_FullName() {
		return $this->PayrollItemWage_FullName;
	}

	
// ------------------------------ End Field: PayrollItemWage_FullName --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: RatePercent -------------------------------------- 

	/** 
	* Sets a value to `RatePercent` variable
	* @access public
	*/

	public function setRatepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RatePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RatePercent` variable
	* @access public
	*/

	public function getRatepercent() {
		return $this->RatePercent;
	}

	public function get_RatePercent() {
		return $this->RatePercent;
	}

	
// ------------------------------ End Field: RatePercent --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Employee_ListID' => (object) array(
										'Field'=>'Employee_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PayrollItemWage_ListID' => (object) array(
										'Field'=>'PayrollItemWage_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PayrollItemWage_FullName' => (object) array(
										'Field'=>'PayrollItemWage_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RatePercent' => (object) array(
										'Field'=>'RatePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_employee_earnings` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Employee_ListID' => "ALTER TABLE  `qb_employee_earnings` ADD  `Employee_ListID` varchar(40) NULL   ;",
			'PayrollItemWage_ListID' => "ALTER TABLE  `qb_employee_earnings` ADD  `PayrollItemWage_ListID` varchar(40) NULL   ;",
			'PayrollItemWage_FullName' => "ALTER TABLE  `qb_employee_earnings` ADD  `PayrollItemWage_FullName` varchar(255) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_employee_earnings` ADD  `Rate` decimal(13,5) NULL   ;",
			'RatePercent' => "ALTER TABLE  `qb_employee_earnings` ADD  `RatePercent` decimal(12,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setEmployeeListid() - Employee_ListID
//setPayrollitemwageListid() - PayrollItemWage_ListID
//setPayrollitemwageFullname() - PayrollItemWage_FullName
//setRate() - Rate
//setRatepercent() - RatePercent

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Employee_ListID() - Employee_ListID
//set_PayrollItemWage_ListID() - PayrollItemWage_ListID
//set_PayrollItemWage_FullName() - PayrollItemWage_FullName
//set_Rate() - Rate
//set_RatePercent() - RatePercent

*/
/* End of file Qb_employee_earnings_model.php */
/* Location: ./application/models/Qb_employee_earnings_model.php */
