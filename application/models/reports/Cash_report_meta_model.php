<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cash_report_meta_model Class
 *
 * Manipulates `cash_report_meta` table on database

CREATE TABLE `cash_report_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cr_id` int(20) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `cash_report_meta` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `cash_report_meta` ADD  `cr_id` int(20) NOT NULL   ;
ALTER TABLE  `cash_report_meta` ADD  `meta_key` varchar(100) NOT NULL   ;
ALTER TABLE  `cash_report_meta` ADD  `meta_value` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Cash_report_meta_model extends MY_Model {

	protected $id;
	protected $cr_id;
	protected $meta_key;
	protected $meta_value;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'cash_report_meta';
		$this->_short_name = 'cash_report_meta';
		$this->_fields = array("id","cr_id","meta_key","meta_value");
		$this->_required = array("cr_id","meta_key");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: cr_id -------------------------------------- 

	/** 
	* Sets a value to `cr_id` variable
	* @access public
	*/

	public function setCrId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('cr_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_cr_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('cr_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `cr_id` variable
	* @access public
	*/

	public function getCrId() {
		return $this->cr_id;
	}

	public function get_cr_id() {
		return $this->cr_id;
	}

	
// ------------------------------ End Field: cr_id --------------------------------------


// ---------------------------- Start Field: meta_key -------------------------------------- 

	/** 
	* Sets a value to `meta_key` variable
	* @access public
	*/

	public function setMetaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_meta_key($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `meta_key` variable
	* @access public
	*/

	public function getMetaKey() {
		return $this->meta_key;
	}

	public function get_meta_key() {
		return $this->meta_key;
	}

	
// ------------------------------ End Field: meta_key --------------------------------------


// ---------------------------- Start Field: meta_value -------------------------------------- 

	/** 
	* Sets a value to `meta_value` variable
	* @access public
	*/

	public function setMetaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_meta_value($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('meta_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `meta_value` variable
	* @access public
	*/

	public function getMetaValue() {
		return $this->meta_value;
	}

	public function get_meta_value() {
		return $this->meta_value;
	}

	
// ------------------------------ End Field: meta_value --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'cr_id' => (object) array(
										'Field'=>'cr_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'meta_key' => (object) array(
										'Field'=>'meta_key',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'meta_value' => (object) array(
										'Field'=>'meta_value',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `cash_report_meta` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'cr_id' => "ALTER TABLE  `cash_report_meta` ADD  `cr_id` int(20) NOT NULL   ;",
			'meta_key' => "ALTER TABLE  `cash_report_meta` ADD  `meta_key` varchar(100) NOT NULL   ;",
			'meta_value' => "ALTER TABLE  `cash_report_meta` ADD  `meta_value` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setCrId() - cr_id
//setMetaKey() - meta_key
//setMetaValue() - meta_value

--------------------------------------

//set_id() - id
//set_cr_id() - cr_id
//set_meta_key() - meta_key
//set_meta_value() - meta_value

*/
/* End of file Cash_report_meta_model.php */
/* Location: ./application/models/Cash_report_meta_model.php */
