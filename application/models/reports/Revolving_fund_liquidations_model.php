<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Revolving_fund_liquidations_model Class
 *
 * Manipulates `revolving_fund_liquidations` table on database

CREATE TABLE `revolving_fund_liquidations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `journal_id` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=264 DEFAULT CHARSET=latin;

ALTER TABLE  `revolving_fund_liquidations` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `revolving_fund_liquidations` ADD  `report_id` int(10) NOT NULL   ;
ALTER TABLE  `revolving_fund_liquidations` ADD  `journal_id` text NOT NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Revolving_fund_liquidations_model extends MY_Model {

	protected $id;
	protected $report_id;
	protected $journal_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'revolving_fund_liquidations';
		$this->_short_name = 'revolving_fund_liquidations';
		$this->_fields = array("id","report_id","journal_id");
		$this->_required = array("report_id","journal_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: report_id -------------------------------------- 

	/** 
	* Sets a value to `report_id` variable
	* @access public
	*/

	public function setReportId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_report_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_id` variable
	* @access public
	*/

	public function getReportId() {
		return $this->report_id;
	}

	public function get_report_id() {
		return $this->report_id;
	}

	
// ------------------------------ End Field: report_id --------------------------------------


// ---------------------------- Start Field: journal_id -------------------------------------- 

	/** 
	* Sets a value to `journal_id` variable
	* @access public
	*/

	public function setJournalId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('journal_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_journal_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('journal_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `journal_id` variable
	* @access public
	*/

	public function getJournalId() {
		return $this->journal_id;
	}

	public function get_journal_id() {
		return $this->journal_id;
	}

	
// ------------------------------ End Field: journal_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'report_id' => (object) array(
										'Field'=>'report_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'journal_id' => (object) array(
										'Field'=>'journal_id',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `revolving_fund_liquidations` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'report_id' => "ALTER TABLE  `revolving_fund_liquidations` ADD  `report_id` int(10) NOT NULL   ;",
			'journal_id' => "ALTER TABLE  `revolving_fund_liquidations` ADD  `journal_id` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setReportId() - report_id
//setJournalId() - journal_id

--------------------------------------

//set_id() - id
//set_report_id() - report_id
//set_journal_id() - journal_id

*/
/* End of file Revolving_fund_liquidations_model.php */
/* Location: ./application/models/Revolving_fund_liquidations_model.php */
