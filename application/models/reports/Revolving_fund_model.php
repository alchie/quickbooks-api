<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Revolving_fund_model Class
 *
 * Manipulates `revolving_fund` table on database

CREATE TABLE `revolving_fund` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` date NOT NULL,
  `revolving_fund` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=latin;

ALTER TABLE  `revolving_fund` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `revolving_fund` ADD  `request_date` date NOT NULL   ;
ALTER TABLE  `revolving_fund` ADD  `revolving_fund` decimal(10,2) NOT NULL   DEFAULT '0.00';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Revolving_fund_model extends MY_Model {

	protected $id;
	protected $request_date;
	protected $revolving_fund;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'revolving_fund';
		$this->_short_name = 'revolving_fund';
		$this->_fields = array("id","request_date","revolving_fund");
		$this->_required = array("request_date","revolving_fund");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: request_date -------------------------------------- 

	/** 
	* Sets a value to `request_date` variable
	* @access public
	*/

	public function setRequestDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('request_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_request_date($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('request_date', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `request_date` variable
	* @access public
	*/

	public function getRequestDate() {
		return $this->request_date;
	}

	public function get_request_date() {
		return $this->request_date;
	}

	
// ------------------------------ End Field: request_date --------------------------------------


// ---------------------------- Start Field: revolving_fund -------------------------------------- 

	/** 
	* Sets a value to `revolving_fund` variable
	* @access public
	*/

	public function setRevolvingFund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('revolving_fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_revolving_fund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('revolving_fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `revolving_fund` variable
	* @access public
	*/

	public function getRevolvingFund() {
		return $this->revolving_fund;
	}

	public function get_revolving_fund() {
		return $this->revolving_fund;
	}

	
// ------------------------------ End Field: revolving_fund --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'request_date' => (object) array(
										'Field'=>'request_date',
										'Type'=>'date',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'revolving_fund' => (object) array(
										'Field'=>'revolving_fund',
										'Type'=>'decimal(10,2)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0.00',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `revolving_fund` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'request_date' => "ALTER TABLE  `revolving_fund` ADD  `request_date` date NOT NULL   ;",
			'revolving_fund' => "ALTER TABLE  `revolving_fund` ADD  `revolving_fund` decimal(10,2) NOT NULL   DEFAULT '0.00';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setRequestDate() - request_date
//setRevolvingFund() - revolving_fund

--------------------------------------

//set_id() - id
//set_request_date() - request_date
//set_revolving_fund() - revolving_fund

*/
/* End of file Revolving_fund_model.php */
/* Location: ./application/models/Revolving_fund_model.php */
