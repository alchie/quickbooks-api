<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cash_report_expenses_model Class
 *
 * Manipulates `cash_report_expenses` table on database

CREATE TABLE `cash_report_expenses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `check_id` text NOT NULL,
  `type` text NOT NULL,
  `fund` varchar(50) NOT NULL DEFAULT 'operations',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=latin;

ALTER TABLE  `cash_report_expenses` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `cash_report_expenses` ADD  `report_id` int(10) NOT NULL   ;
ALTER TABLE  `cash_report_expenses` ADD  `check_id` text NOT NULL   ;
ALTER TABLE  `cash_report_expenses` ADD  `type` text NOT NULL   ;
ALTER TABLE  `cash_report_expenses` ADD  `fund` varchar(50) NOT NULL   DEFAULT 'operations';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Cash_report_expenses_model extends MY_Model {

	protected $id;
	protected $report_id;
	protected $check_id;
	protected $type;
	protected $fund;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'cash_report_expenses';
		$this->_short_name = 'cash_report_expenses';
		$this->_fields = array("id","report_id","check_id","type","fund");
		$this->_required = array("report_id","check_id","type","fund");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: report_id -------------------------------------- 

	/** 
	* Sets a value to `report_id` variable
	* @access public
	*/

	public function setReportId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_report_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_id` variable
	* @access public
	*/

	public function getReportId() {
		return $this->report_id;
	}

	public function get_report_id() {
		return $this->report_id;
	}

	
// ------------------------------ End Field: report_id --------------------------------------


// ---------------------------- Start Field: check_id -------------------------------------- 

	/** 
	* Sets a value to `check_id` variable
	* @access public
	*/

	public function setCheckId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('check_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_check_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('check_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `check_id` variable
	* @access public
	*/

	public function getCheckId() {
		return $this->check_id;
	}

	public function get_check_id() {
		return $this->check_id;
	}

	
// ------------------------------ End Field: check_id --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_type($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}

	public function get_type() {
		return $this->type;
	}

	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: fund -------------------------------------- 

	/** 
	* Sets a value to `fund` variable
	* @access public
	*/

	public function setFund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_fund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `fund` variable
	* @access public
	*/

	public function getFund() {
		return $this->fund;
	}

	public function get_fund() {
		return $this->fund;
	}

	
// ------------------------------ End Field: fund --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'report_id' => (object) array(
										'Field'=>'report_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'check_id' => (object) array(
										'Field'=>'check_id',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'fund' => (object) array(
										'Field'=>'fund',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'operations',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `cash_report_expenses` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'report_id' => "ALTER TABLE  `cash_report_expenses` ADD  `report_id` int(10) NOT NULL   ;",
			'check_id' => "ALTER TABLE  `cash_report_expenses` ADD  `check_id` text NOT NULL   ;",
			'type' => "ALTER TABLE  `cash_report_expenses` ADD  `type` text NOT NULL   ;",
			'fund' => "ALTER TABLE  `cash_report_expenses` ADD  `fund` varchar(50) NOT NULL   DEFAULT 'operations';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setReportId() - report_id
//setCheckId() - check_id
//setType() - type
//setFund() - fund

--------------------------------------

//set_id() - id
//set_report_id() - report_id
//set_check_id() - check_id
//set_type() - type
//set_fund() - fund

*/
/* End of file Cash_report_expenses_model.php */
/* Location: ./application/models/Cash_report_expenses_model.php */
