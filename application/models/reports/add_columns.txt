$models = array();
$models['cash_report'] = 'Cash_report_model';
$models['cash_report_collections'] = 'Cash_report_collections_model';
$models['cash_report_expenses'] = 'Cash_report_expenses_model';
$models['cash_report_meta'] = 'Cash_report_meta_model';
$models['revolving_fund'] = 'Revolving_fund_model';
$models['revolving_fund_liquidations'] = 'Revolving_fund_liquidations_model';
