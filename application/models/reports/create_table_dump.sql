CREATE TABLE `cash_report` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `beg_date` date DEFAULT NULL,
  `beg_operations` decimal(20,3) DEFAULT NULL,
  `beg_fundraising` decimal(20,3) NOT NULL DEFAULT '0.000',
  `beg_investments` decimal(20,3) DEFAULT NULL,
  `col_start_date` date DEFAULT NULL,
  `col_start_num` int(5) DEFAULT NULL,
  `col_end_date` date DEFAULT NULL,
  `col_end_num` int(5) DEFAULT NULL,
  `exp_start_date` date DEFAULT NULL,
  `exp_start_num` int(5) DEFAULT NULL,
  `exp_end_date` date DEFAULT NULL,
  `exp_end_num` int(5) DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `show_operations` int(1) NOT NULL DEFAULT '1',
  `show_fundraising` int(1) NOT NULL DEFAULT '1',
  `show_investments` int(1) NOT NULL DEFAULT '1',
  `show_total` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report` 

CREATE TABLE `cash_report_collections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `receipt_id` text NOT NULL,
  `type` text NOT NULL,
  `fund` varchar(20) NOT NULL DEFAULT 'operations',
  `postdated` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1054 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report_collections` 

CREATE TABLE `cash_report_expenses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `check_id` text NOT NULL,
  `type` text NOT NULL,
  `fund` varchar(50) NOT NULL DEFAULT 'operations',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=latin;

-- Table structure for table `cash_report_expenses` 

CREATE TABLE `cash_report_meta` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `cr_id` int(20) NOT NULL,
  `meta_key` varchar(100) NOT NULL,
  `meta_value` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

-- Table structure for table `cash_report_meta` 

CREATE TABLE `revolving_fund` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `request_date` date NOT NULL,
  `revolving_fund` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=latin;

-- Table structure for table `revolving_fund` 

CREATE TABLE `revolving_fund_liquidations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `journal_id` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=264 DEFAULT CHARSET=latin;

-- Table structure for table `revolving_fund_liquidations` 

