<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cash_report_collections_model Class
 *
 * Manipulates `cash_report_collections` table on database

CREATE TABLE `cash_report_collections` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `report_id` int(10) NOT NULL,
  `receipt_id` text NOT NULL,
  `type` text NOT NULL,
  `fund` varchar(20) NOT NULL DEFAULT 'operations',
  `postdated` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1054 DEFAULT CHARSET=latin;

ALTER TABLE  `cash_report_collections` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `cash_report_collections` ADD  `report_id` int(10) NOT NULL   ;
ALTER TABLE  `cash_report_collections` ADD  `receipt_id` text NOT NULL   ;
ALTER TABLE  `cash_report_collections` ADD  `type` text NOT NULL   ;
ALTER TABLE  `cash_report_collections` ADD  `fund` varchar(20) NOT NULL   DEFAULT 'operations';
ALTER TABLE  `cash_report_collections` ADD  `postdated` int(1) NOT NULL   DEFAULT '0';


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Cash_report_collections_model extends MY_Model {

	protected $id;
	protected $report_id;
	protected $receipt_id;
	protected $type;
	protected $fund;
	protected $postdated;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'cash_report_collections';
		$this->_short_name = 'cash_report_collections';
		$this->_fields = array("id","report_id","receipt_id","type","fund","postdated");
		$this->_required = array("report_id","receipt_id","type","fund","postdated");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}

	public function get_id() {
		return $this->id;
	}

	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: report_id -------------------------------------- 

	/** 
	* Sets a value to `report_id` variable
	* @access public
	*/

	public function setReportId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_report_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('report_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `report_id` variable
	* @access public
	*/

	public function getReportId() {
		return $this->report_id;
	}

	public function get_report_id() {
		return $this->report_id;
	}

	
// ------------------------------ End Field: report_id --------------------------------------


// ---------------------------- Start Field: receipt_id -------------------------------------- 

	/** 
	* Sets a value to `receipt_id` variable
	* @access public
	*/

	public function setReceiptId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('receipt_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_receipt_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('receipt_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `receipt_id` variable
	* @access public
	*/

	public function getReceiptId() {
		return $this->receipt_id;
	}

	public function get_receipt_id() {
		return $this->receipt_id;
	}

	
// ------------------------------ End Field: receipt_id --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_type($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}

	public function get_type() {
		return $this->type;
	}

	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: fund -------------------------------------- 

	/** 
	* Sets a value to `fund` variable
	* @access public
	*/

	public function setFund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_fund($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('fund', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `fund` variable
	* @access public
	*/

	public function getFund() {
		return $this->fund;
	}

	public function get_fund() {
		return $this->fund;
	}

	
// ------------------------------ End Field: fund --------------------------------------


// ---------------------------- Start Field: postdated -------------------------------------- 

	/** 
	* Sets a value to `postdated` variable
	* @access public
	*/

	public function setPostdated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('postdated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_postdated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('postdated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `postdated` variable
	* @access public
	*/

	public function getPostdated() {
		return $this->postdated;
	}

	public function get_postdated() {
		return $this->postdated;
	}

	
// ------------------------------ End Field: postdated --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'report_id' => (object) array(
										'Field'=>'report_id',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'receipt_id' => (object) array(
										'Field'=>'receipt_id',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'fund' => (object) array(
										'Field'=>'fund',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'operations',
										'Extra'=>''
									),

			'postdated' => (object) array(
										'Field'=>'postdated',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `cash_report_collections` ADD  `id` int(10) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'report_id' => "ALTER TABLE  `cash_report_collections` ADD  `report_id` int(10) NOT NULL   ;",
			'receipt_id' => "ALTER TABLE  `cash_report_collections` ADD  `receipt_id` text NOT NULL   ;",
			'type' => "ALTER TABLE  `cash_report_collections` ADD  `type` text NOT NULL   ;",
			'fund' => "ALTER TABLE  `cash_report_collections` ADD  `fund` varchar(20) NOT NULL   DEFAULT 'operations';",
			'postdated' => "ALTER TABLE  `cash_report_collections` ADD  `postdated` int(1) NOT NULL   DEFAULT '0';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setId() - id
//setReportId() - report_id
//setReceiptId() - receipt_id
//setType() - type
//setFund() - fund
//setPostdated() - postdated

--------------------------------------

//set_id() - id
//set_report_id() - report_id
//set_receipt_id() - receipt_id
//set_type() - type
//set_fund() - fund
//set_postdated() - postdated

*/
/* End of file Cash_report_collections_model.php */
/* Location: ./application/models/Cash_report_collections_model.php */
