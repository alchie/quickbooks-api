<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_salesreceipt_model Class
 *
 * Manipulates `qb_salesreceipt` table on database

CREATE TABLE `qb_salesreceipt` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Template_ListID` varchar(40) DEFAULT NULL,
  `Template_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `BillAddress_Addr1` varchar(41) DEFAULT NULL,
  `BillAddress_Addr2` varchar(41) DEFAULT NULL,
  `BillAddress_Addr3` varchar(41) DEFAULT NULL,
  `BillAddress_Addr4` varchar(41) DEFAULT NULL,
  `BillAddress_Addr5` varchar(41) DEFAULT NULL,
  `BillAddress_City` varchar(31) DEFAULT NULL,
  `BillAddress_State` varchar(21) DEFAULT NULL,
  `BillAddress_PostalCode` varchar(13) DEFAULT NULL,
  `BillAddress_Country` varchar(31) DEFAULT NULL,
  `BillAddress_Note` varchar(41) DEFAULT NULL,
  `BillAddressBlock_Addr1` text,
  `BillAddressBlock_Addr2` text,
  `BillAddressBlock_Addr3` text,
  `BillAddressBlock_Addr4` text,
  `BillAddressBlock_Addr5` text,
  `ShipAddress_Addr1` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr2` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr3` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr4` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr5` varchar(41) DEFAULT NULL,
  `ShipAddress_City` varchar(31) DEFAULT NULL,
  `ShipAddress_State` varchar(21) DEFAULT NULL,
  `ShipAddress_PostalCode` varchar(13) DEFAULT NULL,
  `ShipAddress_Country` varchar(31) DEFAULT NULL,
  `ShipAddress_Note` varchar(41) DEFAULT NULL,
  `ShipAddressBlock_Addr1` text,
  `ShipAddressBlock_Addr2` text,
  `ShipAddressBlock_Addr3` text,
  `ShipAddressBlock_Addr4` text,
  `ShipAddressBlock_Addr5` text,
  `IsPending` tinyint(1) DEFAULT NULL,
  `CheckNumber` varchar(25) DEFAULT NULL,
  `PaymentMethod_ListID` varchar(40) DEFAULT NULL,
  `PaymentMethod_FullName` varchar(255) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `SalesRep_ListID` varchar(40) DEFAULT NULL,
  `SalesRep_FullName` varchar(255) DEFAULT NULL,
  `ShipDate` date DEFAULT NULL,
  `ShipMethod_ListID` varchar(40) DEFAULT NULL,
  `ShipMethod_FullName` varchar(255) DEFAULT NULL,
  `FOB` varchar(13) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `ItemSalesTax_ListID` varchar(40) DEFAULT NULL,
  `ItemSalesTax_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxPercentage` decimal(12,5) DEFAULT NULL,
  `SalesTaxTotal` decimal(10,2) DEFAULT NULL,
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  `Currency_ListID` varchar(40) DEFAULT NULL,
  `Currency_FullName` varchar(255) DEFAULT NULL,
  `ExchangeRate` text,
  `TotalAmountInHomeCurrency` decimal(10,2) DEFAULT NULL,
  `Memo` text,
  `CustomerMsg_ListID` varchar(40) DEFAULT NULL,
  `CustomerMsg_FullName` varchar(255) DEFAULT NULL,
  `IsToBePrinted` tinyint(1) DEFAULT NULL,
  `IsToBeEmailed` tinyint(1) DEFAULT NULL,
  `CustomerSalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `CustomerSalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `DepositToAccount_ListID` varchar(40) DEFAULT NULL,
  `DepositToAccount_FullName` varchar(255) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned DEFAULT NULL,
  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) DEFAULT NULL,
  `Other` varchar(29) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `Template_ListID` (`Template_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `BillAddress_Country` (`BillAddress_Country`),
  KEY `ShipAddress_Country` (`ShipAddress_Country`),
  KEY `IsPending` (`IsPending`),
  KEY `PaymentMethod_ListID` (`PaymentMethod_ListID`),
  KEY `SalesRep_ListID` (`SalesRep_ListID`),
  KEY `ShipMethod_ListID` (`ShipMethod_ListID`),
  KEY `ItemSalesTax_ListID` (`ItemSalesTax_ListID`),
  KEY `Currency_ListID` (`Currency_ListID`),
  KEY `CustomerMsg_ListID` (`CustomerMsg_ListID`),
  KEY `IsToBePrinted` (`IsToBePrinted`),
  KEY `IsToBeEmailed` (`IsToBeEmailed`),
  KEY `CustomerSalesTaxCode_ListID` (`CustomerSalesTaxCode_ListID`),
  KEY `DepositToAccount_ListID` (`DepositToAccount_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM AUTO_INCREMENT=820 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_salesreceipt` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_salesreceipt` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesreceipt` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Template_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Template_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `IsPending` tinyint(1) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CheckNumber` varchar(25) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `DueDate` date NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `SalesRep_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `SalesRep_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipDate` date NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ShipMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `FOB` varchar(13) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Subtotal` decimal(10,2) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `SalesTaxPercentage` decimal(12,5) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `SalesTaxTotal` decimal(10,2) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TotalAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Currency_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Currency_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `TotalAmountInHomeCurrency` decimal(10,2) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CustomerMsg_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CustomerMsg_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `IsToBePrinted` tinyint(1) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `IsToBeEmailed` tinyint(1) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CustomerSalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CustomerSalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) NULL   ;
ALTER TABLE  `qb_salesreceipt` ADD  `Other` varchar(29) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_salesreceipt_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Template_ListID;
	protected $Template_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $BillAddress_Addr1;
	protected $BillAddress_Addr2;
	protected $BillAddress_Addr3;
	protected $BillAddress_Addr4;
	protected $BillAddress_Addr5;
	protected $BillAddress_City;
	protected $BillAddress_State;
	protected $BillAddress_PostalCode;
	protected $BillAddress_Country;
	protected $BillAddress_Note;
	protected $BillAddressBlock_Addr1;
	protected $BillAddressBlock_Addr2;
	protected $BillAddressBlock_Addr3;
	protected $BillAddressBlock_Addr4;
	protected $BillAddressBlock_Addr5;
	protected $ShipAddress_Addr1;
	protected $ShipAddress_Addr2;
	protected $ShipAddress_Addr3;
	protected $ShipAddress_Addr4;
	protected $ShipAddress_Addr5;
	protected $ShipAddress_City;
	protected $ShipAddress_State;
	protected $ShipAddress_PostalCode;
	protected $ShipAddress_Country;
	protected $ShipAddress_Note;
	protected $ShipAddressBlock_Addr1;
	protected $ShipAddressBlock_Addr2;
	protected $ShipAddressBlock_Addr3;
	protected $ShipAddressBlock_Addr4;
	protected $ShipAddressBlock_Addr5;
	protected $IsPending;
	protected $CheckNumber;
	protected $PaymentMethod_ListID;
	protected $PaymentMethod_FullName;
	protected $DueDate;
	protected $SalesRep_ListID;
	protected $SalesRep_FullName;
	protected $ShipDate;
	protected $ShipMethod_ListID;
	protected $ShipMethod_FullName;
	protected $FOB;
	protected $Subtotal;
	protected $ItemSalesTax_ListID;
	protected $ItemSalesTax_FullName;
	protected $SalesTaxPercentage;
	protected $SalesTaxTotal;
	protected $TotalAmount;
	protected $Currency_ListID;
	protected $Currency_FullName;
	protected $ExchangeRate;
	protected $TotalAmountInHomeCurrency;
	protected $Memo;
	protected $CustomerMsg_ListID;
	protected $CustomerMsg_FullName;
	protected $IsToBePrinted;
	protected $IsToBeEmailed;
	protected $CustomerSalesTaxCode_ListID;
	protected $CustomerSalesTaxCode_FullName;
	protected $DepositToAccount_ListID;
	protected $DepositToAccount_FullName;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	protected $CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	protected $CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	protected $Other;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_salesreceipt';
		$this->_short_name = 'qb_salesreceipt';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Customer_ListID","Customer_FullName","Class_ListID","Class_FullName","Template_ListID","Template_FullName","TxnDate","RefNumber","BillAddress_Addr1","BillAddress_Addr2","BillAddress_Addr3","BillAddress_Addr4","BillAddress_Addr5","BillAddress_City","BillAddress_State","BillAddress_PostalCode","BillAddress_Country","BillAddress_Note","BillAddressBlock_Addr1","BillAddressBlock_Addr2","BillAddressBlock_Addr3","BillAddressBlock_Addr4","BillAddressBlock_Addr5","ShipAddress_Addr1","ShipAddress_Addr2","ShipAddress_Addr3","ShipAddress_Addr4","ShipAddress_Addr5","ShipAddress_City","ShipAddress_State","ShipAddress_PostalCode","ShipAddress_Country","ShipAddress_Note","ShipAddressBlock_Addr1","ShipAddressBlock_Addr2","ShipAddressBlock_Addr3","ShipAddressBlock_Addr4","ShipAddressBlock_Addr5","IsPending","CheckNumber","PaymentMethod_ListID","PaymentMethod_FullName","DueDate","SalesRep_ListID","SalesRep_FullName","ShipDate","ShipMethod_ListID","ShipMethod_FullName","FOB","Subtotal","ItemSalesTax_ListID","ItemSalesTax_FullName","SalesTaxPercentage","SalesTaxTotal","TotalAmount","Currency_ListID","Currency_FullName","ExchangeRate","TotalAmountInHomeCurrency","Memo","CustomerMsg_ListID","CustomerMsg_FullName","IsToBePrinted","IsToBeEmailed","CustomerSalesTaxCode_ListID","CustomerSalesTaxCode_FullName","DepositToAccount_ListID","DepositToAccount_FullName","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth","CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear","CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode","CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode","CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode","CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode","CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage","CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID","CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber","CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet","CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip","CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch","CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode","CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime","CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp","CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID","Other");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Template_ListID -------------------------------------- 

	/** 
	* Sets a value to `Template_ListID` variable
	* @access public
	*/

	public function setTemplateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_ListID` variable
	* @access public
	*/

	public function getTemplateListid() {
		return $this->Template_ListID;
	}

	public function get_Template_ListID() {
		return $this->Template_ListID;
	}

	
// ------------------------------ End Field: Template_ListID --------------------------------------


// ---------------------------- Start Field: Template_FullName -------------------------------------- 

	/** 
	* Sets a value to `Template_FullName` variable
	* @access public
	*/

	public function setTemplateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_FullName` variable
	* @access public
	*/

	public function getTemplateFullname() {
		return $this->Template_FullName;
	}

	public function get_Template_FullName() {
		return $this->Template_FullName;
	}

	
// ------------------------------ End Field: Template_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr1` variable
	* @access public
	*/

	public function setBilladdressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr1` variable
	* @access public
	*/

	public function getBilladdressAddr1() {
		return $this->BillAddress_Addr1;
	}

	public function get_BillAddress_Addr1() {
		return $this->BillAddress_Addr1;
	}

	
// ------------------------------ End Field: BillAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr2` variable
	* @access public
	*/

	public function setBilladdressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr2` variable
	* @access public
	*/

	public function getBilladdressAddr2() {
		return $this->BillAddress_Addr2;
	}

	public function get_BillAddress_Addr2() {
		return $this->BillAddress_Addr2;
	}

	
// ------------------------------ End Field: BillAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr3` variable
	* @access public
	*/

	public function setBilladdressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr3` variable
	* @access public
	*/

	public function getBilladdressAddr3() {
		return $this->BillAddress_Addr3;
	}

	public function get_BillAddress_Addr3() {
		return $this->BillAddress_Addr3;
	}

	
// ------------------------------ End Field: BillAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr4` variable
	* @access public
	*/

	public function setBilladdressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr4` variable
	* @access public
	*/

	public function getBilladdressAddr4() {
		return $this->BillAddress_Addr4;
	}

	public function get_BillAddress_Addr4() {
		return $this->BillAddress_Addr4;
	}

	
// ------------------------------ End Field: BillAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr5` variable
	* @access public
	*/

	public function setBilladdressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr5` variable
	* @access public
	*/

	public function getBilladdressAddr5() {
		return $this->BillAddress_Addr5;
	}

	public function get_BillAddress_Addr5() {
		return $this->BillAddress_Addr5;
	}

	
// ------------------------------ End Field: BillAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: BillAddress_City -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_City` variable
	* @access public
	*/

	public function setBilladdressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_City` variable
	* @access public
	*/

	public function getBilladdressCity() {
		return $this->BillAddress_City;
	}

	public function get_BillAddress_City() {
		return $this->BillAddress_City;
	}

	
// ------------------------------ End Field: BillAddress_City --------------------------------------


// ---------------------------- Start Field: BillAddress_State -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_State` variable
	* @access public
	*/

	public function setBilladdressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_State` variable
	* @access public
	*/

	public function getBilladdressState() {
		return $this->BillAddress_State;
	}

	public function get_BillAddress_State() {
		return $this->BillAddress_State;
	}

	
// ------------------------------ End Field: BillAddress_State --------------------------------------


// ---------------------------- Start Field: BillAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_PostalCode` variable
	* @access public
	*/

	public function setBilladdressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_PostalCode` variable
	* @access public
	*/

	public function getBilladdressPostalcode() {
		return $this->BillAddress_PostalCode;
	}

	public function get_BillAddress_PostalCode() {
		return $this->BillAddress_PostalCode;
	}

	
// ------------------------------ End Field: BillAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: BillAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Country` variable
	* @access public
	*/

	public function setBilladdressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Country` variable
	* @access public
	*/

	public function getBilladdressCountry() {
		return $this->BillAddress_Country;
	}

	public function get_BillAddress_Country() {
		return $this->BillAddress_Country;
	}

	
// ------------------------------ End Field: BillAddress_Country --------------------------------------


// ---------------------------- Start Field: BillAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Note` variable
	* @access public
	*/

	public function setBilladdressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Note` variable
	* @access public
	*/

	public function getBilladdressNote() {
		return $this->BillAddress_Note;
	}

	public function get_BillAddress_Note() {
		return $this->BillAddress_Note;
	}

	
// ------------------------------ End Field: BillAddress_Note --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function setBilladdressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function getBilladdressblockAddr1() {
		return $this->BillAddressBlock_Addr1;
	}

	public function get_BillAddressBlock_Addr1() {
		return $this->BillAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function setBilladdressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function getBilladdressblockAddr2() {
		return $this->BillAddressBlock_Addr2;
	}

	public function get_BillAddressBlock_Addr2() {
		return $this->BillAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function setBilladdressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function getBilladdressblockAddr3() {
		return $this->BillAddressBlock_Addr3;
	}

	public function get_BillAddressBlock_Addr3() {
		return $this->BillAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function setBilladdressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function getBilladdressblockAddr4() {
		return $this->BillAddressBlock_Addr4;
	}

	public function get_BillAddressBlock_Addr4() {
		return $this->BillAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function setBilladdressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function getBilladdressblockAddr5() {
		return $this->BillAddressBlock_Addr5;
	}

	public function get_BillAddressBlock_Addr5() {
		return $this->BillAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr1` variable
	* @access public
	*/

	public function setShipaddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr1` variable
	* @access public
	*/

	public function getShipaddressAddr1() {
		return $this->ShipAddress_Addr1;
	}

	public function get_ShipAddress_Addr1() {
		return $this->ShipAddress_Addr1;
	}

	
// ------------------------------ End Field: ShipAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr2` variable
	* @access public
	*/

	public function setShipaddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr2` variable
	* @access public
	*/

	public function getShipaddressAddr2() {
		return $this->ShipAddress_Addr2;
	}

	public function get_ShipAddress_Addr2() {
		return $this->ShipAddress_Addr2;
	}

	
// ------------------------------ End Field: ShipAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr3` variable
	* @access public
	*/

	public function setShipaddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr3` variable
	* @access public
	*/

	public function getShipaddressAddr3() {
		return $this->ShipAddress_Addr3;
	}

	public function get_ShipAddress_Addr3() {
		return $this->ShipAddress_Addr3;
	}

	
// ------------------------------ End Field: ShipAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr4` variable
	* @access public
	*/

	public function setShipaddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr4` variable
	* @access public
	*/

	public function getShipaddressAddr4() {
		return $this->ShipAddress_Addr4;
	}

	public function get_ShipAddress_Addr4() {
		return $this->ShipAddress_Addr4;
	}

	
// ------------------------------ End Field: ShipAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr5` variable
	* @access public
	*/

	public function setShipaddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr5` variable
	* @access public
	*/

	public function getShipaddressAddr5() {
		return $this->ShipAddress_Addr5;
	}

	public function get_ShipAddress_Addr5() {
		return $this->ShipAddress_Addr5;
	}

	
// ------------------------------ End Field: ShipAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_City -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_City` variable
	* @access public
	*/

	public function setShipaddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_City` variable
	* @access public
	*/

	public function getShipaddressCity() {
		return $this->ShipAddress_City;
	}

	public function get_ShipAddress_City() {
		return $this->ShipAddress_City;
	}

	
// ------------------------------ End Field: ShipAddress_City --------------------------------------


// ---------------------------- Start Field: ShipAddress_State -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_State` variable
	* @access public
	*/

	public function setShipaddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_State` variable
	* @access public
	*/

	public function getShipaddressState() {
		return $this->ShipAddress_State;
	}

	public function get_ShipAddress_State() {
		return $this->ShipAddress_State;
	}

	
// ------------------------------ End Field: ShipAddress_State --------------------------------------


// ---------------------------- Start Field: ShipAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function setShipaddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function getShipaddressPostalcode() {
		return $this->ShipAddress_PostalCode;
	}

	public function get_ShipAddress_PostalCode() {
		return $this->ShipAddress_PostalCode;
	}

	
// ------------------------------ End Field: ShipAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: ShipAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Country` variable
	* @access public
	*/

	public function setShipaddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Country` variable
	* @access public
	*/

	public function getShipaddressCountry() {
		return $this->ShipAddress_Country;
	}

	public function get_ShipAddress_Country() {
		return $this->ShipAddress_Country;
	}

	
// ------------------------------ End Field: ShipAddress_Country --------------------------------------


// ---------------------------- Start Field: ShipAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Note` variable
	* @access public
	*/

	public function setShipaddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Note` variable
	* @access public
	*/

	public function getShipaddressNote() {
		return $this->ShipAddress_Note;
	}

	public function get_ShipAddress_Note() {
		return $this->ShipAddress_Note;
	}

	
// ------------------------------ End Field: ShipAddress_Note --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function setShipaddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function getShipaddressblockAddr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	public function get_ShipAddressBlock_Addr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function setShipaddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function getShipaddressblockAddr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	public function get_ShipAddressBlock_Addr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function setShipaddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function getShipaddressblockAddr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	public function get_ShipAddressBlock_Addr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function setShipaddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function getShipaddressblockAddr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	public function get_ShipAddressBlock_Addr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function setShipaddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function getShipaddressblockAddr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	public function get_ShipAddressBlock_Addr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: IsPending -------------------------------------- 

	/** 
	* Sets a value to `IsPending` variable
	* @access public
	*/

	public function setIspending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPending` variable
	* @access public
	*/

	public function getIspending() {
		return $this->IsPending;
	}

	public function get_IsPending() {
		return $this->IsPending;
	}

	
// ------------------------------ End Field: IsPending --------------------------------------


// ---------------------------- Start Field: CheckNumber -------------------------------------- 

	/** 
	* Sets a value to `CheckNumber` variable
	* @access public
	*/

	public function setChecknumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CheckNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CheckNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CheckNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CheckNumber` variable
	* @access public
	*/

	public function getChecknumber() {
		return $this->CheckNumber;
	}

	public function get_CheckNumber() {
		return $this->CheckNumber;
	}

	
// ------------------------------ End Field: CheckNumber --------------------------------------


// ---------------------------- Start Field: PaymentMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_ListID` variable
	* @access public
	*/

	public function setPaymentmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_ListID` variable
	* @access public
	*/

	public function getPaymentmethodListid() {
		return $this->PaymentMethod_ListID;
	}

	public function get_PaymentMethod_ListID() {
		return $this->PaymentMethod_ListID;
	}

	
// ------------------------------ End Field: PaymentMethod_ListID --------------------------------------


// ---------------------------- Start Field: PaymentMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `PaymentMethod_FullName` variable
	* @access public
	*/

	public function setPaymentmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PaymentMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PaymentMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PaymentMethod_FullName` variable
	* @access public
	*/

	public function getPaymentmethodFullname() {
		return $this->PaymentMethod_FullName;
	}

	public function get_PaymentMethod_FullName() {
		return $this->PaymentMethod_FullName;
	}

	
// ------------------------------ End Field: PaymentMethod_FullName --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------


// ---------------------------- Start Field: SalesRep_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_ListID` variable
	* @access public
	*/

	public function setSalesrepListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_ListID` variable
	* @access public
	*/

	public function getSalesrepListid() {
		return $this->SalesRep_ListID;
	}

	public function get_SalesRep_ListID() {
		return $this->SalesRep_ListID;
	}

	
// ------------------------------ End Field: SalesRep_ListID --------------------------------------


// ---------------------------- Start Field: SalesRep_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_FullName` variable
	* @access public
	*/

	public function setSalesrepFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_FullName` variable
	* @access public
	*/

	public function getSalesrepFullname() {
		return $this->SalesRep_FullName;
	}

	public function get_SalesRep_FullName() {
		return $this->SalesRep_FullName;
	}

	
// ------------------------------ End Field: SalesRep_FullName --------------------------------------


// ---------------------------- Start Field: ShipDate -------------------------------------- 

	/** 
	* Sets a value to `ShipDate` variable
	* @access public
	*/

	public function setShipdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipDate` variable
	* @access public
	*/

	public function getShipdate() {
		return $this->ShipDate;
	}

	public function get_ShipDate() {
		return $this->ShipDate;
	}

	
// ------------------------------ End Field: ShipDate --------------------------------------


// ---------------------------- Start Field: ShipMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_ListID` variable
	* @access public
	*/

	public function setShipmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_ListID` variable
	* @access public
	*/

	public function getShipmethodListid() {
		return $this->ShipMethod_ListID;
	}

	public function get_ShipMethod_ListID() {
		return $this->ShipMethod_ListID;
	}

	
// ------------------------------ End Field: ShipMethod_ListID --------------------------------------


// ---------------------------- Start Field: ShipMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_FullName` variable
	* @access public
	*/

	public function setShipmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_FullName` variable
	* @access public
	*/

	public function getShipmethodFullname() {
		return $this->ShipMethod_FullName;
	}

	public function get_ShipMethod_FullName() {
		return $this->ShipMethod_FullName;
	}

	
// ------------------------------ End Field: ShipMethod_FullName --------------------------------------


// ---------------------------- Start Field: FOB -------------------------------------- 

	/** 
	* Sets a value to `FOB` variable
	* @access public
	*/

	public function setFob($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FOB($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FOB` variable
	* @access public
	*/

	public function getFob() {
		return $this->FOB;
	}

	public function get_FOB() {
		return $this->FOB;
	}

	
// ------------------------------ End Field: FOB --------------------------------------


// ---------------------------- Start Field: Subtotal -------------------------------------- 

	/** 
	* Sets a value to `Subtotal` variable
	* @access public
	*/

	public function setSubtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Subtotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Subtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Subtotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Subtotal` variable
	* @access public
	*/

	public function getSubtotal() {
		return $this->Subtotal;
	}

	public function get_Subtotal() {
		return $this->Subtotal;
	}

	
// ------------------------------ End Field: Subtotal --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function setItemsalestaxListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function getItemsalestaxListid() {
		return $this->ItemSalesTax_ListID;
	}

	public function get_ItemSalesTax_ListID() {
		return $this->ItemSalesTax_ListID;
	}

	
// ------------------------------ End Field: ItemSalesTax_ListID --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function setItemsalestaxFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function getItemsalestaxFullname() {
		return $this->ItemSalesTax_FullName;
	}

	public function get_ItemSalesTax_FullName() {
		return $this->ItemSalesTax_FullName;
	}

	
// ------------------------------ End Field: ItemSalesTax_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxPercentage -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPercentage` variable
	* @access public
	*/

	public function setSalestaxpercentage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPercentage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPercentage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPercentage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPercentage` variable
	* @access public
	*/

	public function getSalestaxpercentage() {
		return $this->SalesTaxPercentage;
	}

	public function get_SalesTaxPercentage() {
		return $this->SalesTaxPercentage;
	}

	
// ------------------------------ End Field: SalesTaxPercentage --------------------------------------


// ---------------------------- Start Field: SalesTaxTotal -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxTotal` variable
	* @access public
	*/

	public function setSalestaxtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxTotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxTotal` variable
	* @access public
	*/

	public function getSalestaxtotal() {
		return $this->SalesTaxTotal;
	}

	public function get_SalesTaxTotal() {
		return $this->SalesTaxTotal;
	}

	
// ------------------------------ End Field: SalesTaxTotal --------------------------------------


// ---------------------------- Start Field: TotalAmount -------------------------------------- 

	/** 
	* Sets a value to `TotalAmount` variable
	* @access public
	*/

	public function setTotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmount` variable
	* @access public
	*/

	public function getTotalamount() {
		return $this->TotalAmount;
	}

	public function get_TotalAmount() {
		return $this->TotalAmount;
	}

	
// ------------------------------ End Field: TotalAmount --------------------------------------


// ---------------------------- Start Field: Currency_ListID -------------------------------------- 

	/** 
	* Sets a value to `Currency_ListID` variable
	* @access public
	*/

	public function setCurrencyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_ListID` variable
	* @access public
	*/

	public function getCurrencyListid() {
		return $this->Currency_ListID;
	}

	public function get_Currency_ListID() {
		return $this->Currency_ListID;
	}

	
// ------------------------------ End Field: Currency_ListID --------------------------------------


// ---------------------------- Start Field: Currency_FullName -------------------------------------- 

	/** 
	* Sets a value to `Currency_FullName` variable
	* @access public
	*/

	public function setCurrencyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_FullName` variable
	* @access public
	*/

	public function getCurrencyFullname() {
		return $this->Currency_FullName;
	}

	public function get_Currency_FullName() {
		return $this->Currency_FullName;
	}

	
// ------------------------------ End Field: Currency_FullName --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: TotalAmountInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `TotalAmountInHomeCurrency` variable
	* @access public
	*/

	public function setTotalamountinhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmountInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmountInHomeCurrency` variable
	* @access public
	*/

	public function getTotalamountinhomecurrency() {
		return $this->TotalAmountInHomeCurrency;
	}

	public function get_TotalAmountInHomeCurrency() {
		return $this->TotalAmountInHomeCurrency;
	}

	
// ------------------------------ End Field: TotalAmountInHomeCurrency --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: CustomerMsg_ListID -------------------------------------- 

	/** 
	* Sets a value to `CustomerMsg_ListID` variable
	* @access public
	*/

	public function setCustomermsgListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerMsg_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerMsg_ListID` variable
	* @access public
	*/

	public function getCustomermsgListid() {
		return $this->CustomerMsg_ListID;
	}

	public function get_CustomerMsg_ListID() {
		return $this->CustomerMsg_ListID;
	}

	
// ------------------------------ End Field: CustomerMsg_ListID --------------------------------------


// ---------------------------- Start Field: CustomerMsg_FullName -------------------------------------- 

	/** 
	* Sets a value to `CustomerMsg_FullName` variable
	* @access public
	*/

	public function setCustomermsgFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerMsg_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerMsg_FullName` variable
	* @access public
	*/

	public function getCustomermsgFullname() {
		return $this->CustomerMsg_FullName;
	}

	public function get_CustomerMsg_FullName() {
		return $this->CustomerMsg_FullName;
	}

	
// ------------------------------ End Field: CustomerMsg_FullName --------------------------------------


// ---------------------------- Start Field: IsToBePrinted -------------------------------------- 

	/** 
	* Sets a value to `IsToBePrinted` variable
	* @access public
	*/

	public function setIstobeprinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBePrinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBePrinted` variable
	* @access public
	*/

	public function getIstobeprinted() {
		return $this->IsToBePrinted;
	}

	public function get_IsToBePrinted() {
		return $this->IsToBePrinted;
	}

	
// ------------------------------ End Field: IsToBePrinted --------------------------------------


// ---------------------------- Start Field: IsToBeEmailed -------------------------------------- 

	/** 
	* Sets a value to `IsToBeEmailed` variable
	* @access public
	*/

	public function setIstobeemailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBeEmailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBeEmailed` variable
	* @access public
	*/

	public function getIstobeemailed() {
		return $this->IsToBeEmailed;
	}

	public function get_IsToBeEmailed() {
		return $this->IsToBeEmailed;
	}

	
// ------------------------------ End Field: IsToBeEmailed --------------------------------------


// ---------------------------- Start Field: CustomerSalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `CustomerSalesTaxCode_ListID` variable
	* @access public
	*/

	public function setCustomersalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerSalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerSalesTaxCode_ListID` variable
	* @access public
	*/

	public function getCustomersalestaxcodeListid() {
		return $this->CustomerSalesTaxCode_ListID;
	}

	public function get_CustomerSalesTaxCode_ListID() {
		return $this->CustomerSalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: CustomerSalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: CustomerSalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `CustomerSalesTaxCode_FullName` variable
	* @access public
	*/

	public function setCustomersalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerSalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerSalesTaxCode_FullName` variable
	* @access public
	*/

	public function getCustomersalestaxcodeFullname() {
		return $this->CustomerSalesTaxCode_FullName;
	}

	public function get_CustomerSalesTaxCode_FullName() {
		return $this->CustomerSalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: CustomerSalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: DepositToAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_ListID` variable
	* @access public
	*/

	public function setDeposittoaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_ListID` variable
	* @access public
	*/

	public function getDeposittoaccountListid() {
		return $this->DepositToAccount_ListID;
	}

	public function get_DepositToAccount_ListID() {
		return $this->DepositToAccount_ListID;
	}

	
// ------------------------------ End Field: DepositToAccount_ListID --------------------------------------


// ---------------------------- Start Field: DepositToAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `DepositToAccount_FullName` variable
	* @access public
	*/

	public function setDeposittoaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DepositToAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DepositToAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DepositToAccount_FullName` variable
	* @access public
	*/

	public function getDeposittoaccountFullname() {
		return $this->DepositToAccount_FullName;
	}

	public function get_DepositToAccount_FullName() {
		return $this->DepositToAccount_FullName;
	}

	
// ------------------------------ End Field: DepositToAccount_FullName --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoExpirationyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoExpirationyear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoNameoncard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoNameoncard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoTransactionmode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoTransactionmode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() {
		return $this->CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoResultmessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoResultmessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoAvszip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoAvszip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp --------------------------------------


// ---------------------------- Start Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function setCreditcardtxninfoCreditcardtxnresultinfoClienttransid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` variable
	* @access public
	*/

	public function getCreditcardtxninfoCreditcardtxnresultinfoClienttransid() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	public function get_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() {
		return $this->CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID;
	}

	
// ------------------------------ End Field: CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID --------------------------------------


// ---------------------------- Start Field: Other -------------------------------------- 

	/** 
	* Sets a value to `Other` variable
	* @access public
	*/

	public function setOther($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other` variable
	* @access public
	*/

	public function getOther() {
		return $this->Other;
	}

	public function get_Other() {
		return $this->Other;
	}

	
// ------------------------------ End Field: Other --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_ListID' => (object) array(
										'Field'=>'Template_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_FullName' => (object) array(
										'Field'=>'Template_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr1' => (object) array(
										'Field'=>'BillAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr2' => (object) array(
										'Field'=>'BillAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr3' => (object) array(
										'Field'=>'BillAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr4' => (object) array(
										'Field'=>'BillAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr5' => (object) array(
										'Field'=>'BillAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_City' => (object) array(
										'Field'=>'BillAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_State' => (object) array(
										'Field'=>'BillAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_PostalCode' => (object) array(
										'Field'=>'BillAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Country' => (object) array(
										'Field'=>'BillAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Note' => (object) array(
										'Field'=>'BillAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr1' => (object) array(
										'Field'=>'BillAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr2' => (object) array(
										'Field'=>'BillAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr3' => (object) array(
										'Field'=>'BillAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr4' => (object) array(
										'Field'=>'BillAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr5' => (object) array(
										'Field'=>'BillAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr1' => (object) array(
										'Field'=>'ShipAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr2' => (object) array(
										'Field'=>'ShipAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr3' => (object) array(
										'Field'=>'ShipAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr4' => (object) array(
										'Field'=>'ShipAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr5' => (object) array(
										'Field'=>'ShipAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_City' => (object) array(
										'Field'=>'ShipAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_State' => (object) array(
										'Field'=>'ShipAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_PostalCode' => (object) array(
										'Field'=>'ShipAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Country' => (object) array(
										'Field'=>'ShipAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Note' => (object) array(
										'Field'=>'ShipAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr1' => (object) array(
										'Field'=>'ShipAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr2' => (object) array(
										'Field'=>'ShipAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr3' => (object) array(
										'Field'=>'ShipAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr4' => (object) array(
										'Field'=>'ShipAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr5' => (object) array(
										'Field'=>'ShipAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPending' => (object) array(
										'Field'=>'IsPending',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CheckNumber' => (object) array(
										'Field'=>'CheckNumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_ListID' => (object) array(
										'Field'=>'PaymentMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PaymentMethod_FullName' => (object) array(
										'Field'=>'PaymentMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_ListID' => (object) array(
										'Field'=>'SalesRep_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_FullName' => (object) array(
										'Field'=>'SalesRep_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipDate' => (object) array(
										'Field'=>'ShipDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_ListID' => (object) array(
										'Field'=>'ShipMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_FullName' => (object) array(
										'Field'=>'ShipMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FOB' => (object) array(
										'Field'=>'FOB',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Subtotal' => (object) array(
										'Field'=>'Subtotal',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_ListID' => (object) array(
										'Field'=>'ItemSalesTax_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_FullName' => (object) array(
										'Field'=>'ItemSalesTax_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPercentage' => (object) array(
										'Field'=>'SalesTaxPercentage',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxTotal' => (object) array(
										'Field'=>'SalesTaxTotal',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalAmount' => (object) array(
										'Field'=>'TotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_ListID' => (object) array(
										'Field'=>'Currency_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_FullName' => (object) array(
										'Field'=>'Currency_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalAmountInHomeCurrency' => (object) array(
										'Field'=>'TotalAmountInHomeCurrency',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerMsg_ListID' => (object) array(
										'Field'=>'CustomerMsg_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerMsg_FullName' => (object) array(
										'Field'=>'CustomerMsg_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBePrinted' => (object) array(
										'Field'=>'IsToBePrinted',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBeEmailed' => (object) array(
										'Field'=>'IsToBeEmailed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerSalesTaxCode_ListID' => (object) array(
										'Field'=>'CustomerSalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerSalesTaxCode_FullName' => (object) array(
										'Field'=>'CustomerSalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_ListID' => (object) array(
										'Field'=>'DepositToAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'DepositToAccount_FullName' => (object) array(
										'Field'=>'DepositToAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode',
										'Type'=>'varchar(18)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode',
										'Type'=>'varchar(24)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage',
										'Type'=>'varchar(60)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID',
										'Type'=>'varchar(24)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber',
										'Type'=>'varchar(32)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode',
										'Type'=>'varchar(12)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID',
										'Type'=>'varchar(84)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => (object) array(
										'Field'=>'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID',
										'Type'=>'varchar(16)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other' => (object) array(
										'Field'=>'Other',
										'Type'=>'varchar(29)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_salesreceipt` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_salesreceipt` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_salesreceipt` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_salesreceipt` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_salesreceipt` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_salesreceipt` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Customer_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Template_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `Template_ListID` varchar(40) NULL   ;",
			'Template_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `Template_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_salesreceipt` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_salesreceipt` ADD  `RefNumber` varchar(11) NULL   ;",
			'BillAddress_Addr1' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr1` varchar(41) NULL   ;",
			'BillAddress_Addr2' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr2` varchar(41) NULL   ;",
			'BillAddress_Addr3' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr3` varchar(41) NULL   ;",
			'BillAddress_Addr4' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr4` varchar(41) NULL   ;",
			'BillAddress_Addr5' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Addr5` varchar(41) NULL   ;",
			'BillAddress_City' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_City` varchar(31) NULL   ;",
			'BillAddress_State' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_State` varchar(21) NULL   ;",
			'BillAddress_PostalCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;",
			'BillAddress_Country' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Country` varchar(31) NULL   ;",
			'BillAddress_Note' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddress_Note` varchar(41) NULL   ;",
			'BillAddressBlock_Addr1' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr1` text NULL   ;",
			'BillAddressBlock_Addr2' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr2` text NULL   ;",
			'BillAddressBlock_Addr3' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr3` text NULL   ;",
			'BillAddressBlock_Addr4' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr4` text NULL   ;",
			'BillAddressBlock_Addr5' => "ALTER TABLE  `qb_salesreceipt` ADD  `BillAddressBlock_Addr5` text NULL   ;",
			'ShipAddress_Addr1' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;",
			'ShipAddress_Addr2' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;",
			'ShipAddress_Addr3' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;",
			'ShipAddress_Addr4' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;",
			'ShipAddress_Addr5' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;",
			'ShipAddress_City' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_City` varchar(31) NULL   ;",
			'ShipAddress_State' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_State` varchar(21) NULL   ;",
			'ShipAddress_PostalCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;",
			'ShipAddress_Country' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Country` varchar(31) NULL   ;",
			'ShipAddress_Note' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddress_Note` varchar(41) NULL   ;",
			'ShipAddressBlock_Addr1' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr1` text NULL   ;",
			'ShipAddressBlock_Addr2' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr2` text NULL   ;",
			'ShipAddressBlock_Addr3' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr3` text NULL   ;",
			'ShipAddressBlock_Addr4' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr4` text NULL   ;",
			'ShipAddressBlock_Addr5' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipAddressBlock_Addr5` text NULL   ;",
			'IsPending' => "ALTER TABLE  `qb_salesreceipt` ADD  `IsPending` tinyint(1) NULL   ;",
			'CheckNumber' => "ALTER TABLE  `qb_salesreceipt` ADD  `CheckNumber` varchar(25) NULL   ;",
			'PaymentMethod_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `PaymentMethod_ListID` varchar(40) NULL   ;",
			'PaymentMethod_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `PaymentMethod_FullName` varchar(255) NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_salesreceipt` ADD  `DueDate` date NULL   ;",
			'SalesRep_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `SalesRep_ListID` varchar(40) NULL   ;",
			'SalesRep_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `SalesRep_FullName` varchar(255) NULL   ;",
			'ShipDate' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipDate` date NULL   ;",
			'ShipMethod_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipMethod_ListID` varchar(40) NULL   ;",
			'ShipMethod_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `ShipMethod_FullName` varchar(255) NULL   ;",
			'FOB' => "ALTER TABLE  `qb_salesreceipt` ADD  `FOB` varchar(13) NULL   ;",
			'Subtotal' => "ALTER TABLE  `qb_salesreceipt` ADD  `Subtotal` decimal(10,2) NULL   ;",
			'ItemSalesTax_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;",
			'ItemSalesTax_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;",
			'SalesTaxPercentage' => "ALTER TABLE  `qb_salesreceipt` ADD  `SalesTaxPercentage` decimal(12,5) NULL   ;",
			'SalesTaxTotal' => "ALTER TABLE  `qb_salesreceipt` ADD  `SalesTaxTotal` decimal(10,2) NULL   ;",
			'TotalAmount' => "ALTER TABLE  `qb_salesreceipt` ADD  `TotalAmount` decimal(10,2) NULL   ;",
			'Currency_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `Currency_ListID` varchar(40) NULL   ;",
			'Currency_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `Currency_FullName` varchar(255) NULL   ;",
			'ExchangeRate' => "ALTER TABLE  `qb_salesreceipt` ADD  `ExchangeRate` text NULL   ;",
			'TotalAmountInHomeCurrency' => "ALTER TABLE  `qb_salesreceipt` ADD  `TotalAmountInHomeCurrency` decimal(10,2) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_salesreceipt` ADD  `Memo` text NULL   ;",
			'CustomerMsg_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `CustomerMsg_ListID` varchar(40) NULL   ;",
			'CustomerMsg_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `CustomerMsg_FullName` varchar(255) NULL   ;",
			'IsToBePrinted' => "ALTER TABLE  `qb_salesreceipt` ADD  `IsToBePrinted` tinyint(1) NULL   ;",
			'IsToBeEmailed' => "ALTER TABLE  `qb_salesreceipt` ADD  `IsToBeEmailed` tinyint(1) NULL   ;",
			'CustomerSalesTaxCode_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `CustomerSalesTaxCode_ListID` varchar(40) NULL   ;",
			'CustomerSalesTaxCode_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `CustomerSalesTaxCode_FullName` varchar(255) NULL   ;",
			'DepositToAccount_ListID' => "ALTER TABLE  `qb_salesreceipt` ADD  `DepositToAccount_ListID` varchar(40) NULL   ;",
			'DepositToAccount_FullName' => "ALTER TABLE  `qb_salesreceipt` ADD  `DepositToAccount_FullName` varchar(255) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber` varchar(25) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard` varchar(41) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress` varchar(41) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode` varchar(18) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode` varchar(24) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage` varchar(60) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID` varchar(24) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber` varchar(32) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode` varchar(12) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID` varchar(84) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus` varchar(40) NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime` datetime NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp` int(10) unsigned NULL   ;",
			'CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID' => "ALTER TABLE  `qb_salesreceipt` ADD  `CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID` varchar(16) NULL   ;",
			'Other' => "ALTER TABLE  `qb_salesreceipt` ADD  `Other` varchar(29) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setTemplateListid() - Template_ListID
//setTemplateFullname() - Template_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setBilladdressAddr1() - BillAddress_Addr1
//setBilladdressAddr2() - BillAddress_Addr2
//setBilladdressAddr3() - BillAddress_Addr3
//setBilladdressAddr4() - BillAddress_Addr4
//setBilladdressAddr5() - BillAddress_Addr5
//setBilladdressCity() - BillAddress_City
//setBilladdressState() - BillAddress_State
//setBilladdressPostalcode() - BillAddress_PostalCode
//setBilladdressCountry() - BillAddress_Country
//setBilladdressNote() - BillAddress_Note
//setBilladdressblockAddr1() - BillAddressBlock_Addr1
//setBilladdressblockAddr2() - BillAddressBlock_Addr2
//setBilladdressblockAddr3() - BillAddressBlock_Addr3
//setBilladdressblockAddr4() - BillAddressBlock_Addr4
//setBilladdressblockAddr5() - BillAddressBlock_Addr5
//setShipaddressAddr1() - ShipAddress_Addr1
//setShipaddressAddr2() - ShipAddress_Addr2
//setShipaddressAddr3() - ShipAddress_Addr3
//setShipaddressAddr4() - ShipAddress_Addr4
//setShipaddressAddr5() - ShipAddress_Addr5
//setShipaddressCity() - ShipAddress_City
//setShipaddressState() - ShipAddress_State
//setShipaddressPostalcode() - ShipAddress_PostalCode
//setShipaddressCountry() - ShipAddress_Country
//setShipaddressNote() - ShipAddress_Note
//setShipaddressblockAddr1() - ShipAddressBlock_Addr1
//setShipaddressblockAddr2() - ShipAddressBlock_Addr2
//setShipaddressblockAddr3() - ShipAddressBlock_Addr3
//setShipaddressblockAddr4() - ShipAddressBlock_Addr4
//setShipaddressblockAddr5() - ShipAddressBlock_Addr5
//setIspending() - IsPending
//setChecknumber() - CheckNumber
//setPaymentmethodListid() - PaymentMethod_ListID
//setPaymentmethodFullname() - PaymentMethod_FullName
//setDuedate() - DueDate
//setSalesrepListid() - SalesRep_ListID
//setSalesrepFullname() - SalesRep_FullName
//setShipdate() - ShipDate
//setShipmethodListid() - ShipMethod_ListID
//setShipmethodFullname() - ShipMethod_FullName
//setFob() - FOB
//setSubtotal() - Subtotal
//setItemsalestaxListid() - ItemSalesTax_ListID
//setItemsalestaxFullname() - ItemSalesTax_FullName
//setSalestaxpercentage() - SalesTaxPercentage
//setSalestaxtotal() - SalesTaxTotal
//setTotalamount() - TotalAmount
//setCurrencyListid() - Currency_ListID
//setCurrencyFullname() - Currency_FullName
//setExchangerate() - ExchangeRate
//setTotalamountinhomecurrency() - TotalAmountInHomeCurrency
//setMemo() - Memo
//setCustomermsgListid() - CustomerMsg_ListID
//setCustomermsgFullname() - CustomerMsg_FullName
//setIstobeprinted() - IsToBePrinted
//setIstobeemailed() - IsToBeEmailed
//setCustomersalestaxcodeListid() - CustomerSalesTaxCode_ListID
//setCustomersalestaxcodeFullname() - CustomerSalesTaxCode_FullName
//setDeposittoaccountListid() - DepositToAccount_ListID
//setDeposittoaccountFullname() - DepositToAccount_FullName
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardnumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//setCreditcardtxninfoCreditcardtxninputinfoExpirationmonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//setCreditcardtxninfoCreditcardtxninputinfoExpirationyear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//setCreditcardtxninfoCreditcardtxninputinfoNameoncard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardaddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardpostalcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//setCreditcardtxninfoCreditcardtxninputinfoCommercialcardcode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//setCreditcardtxninfoCreditcardtxninputinfoTransactionmode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//setCreditcardtxninfoCreditcardtxninputinfoCreditcardtxntype() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//setCreditcardtxninfoCreditcardtxnresultinfoResultcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//setCreditcardtxninfoCreditcardtxnresultinfoResultmessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//setCreditcardtxninfoCreditcardtxnresultinfoCreditcardtransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//setCreditcardtxninfoCreditcardtxnresultinfoMerchantaccountnumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//setCreditcardtxninfoCreditcardtxnresultinfoAuthorizationcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//setCreditcardtxninfoCreditcardtxnresultinfoAvsstreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//setCreditcardtxninfoCreditcardtxnresultinfoAvszip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//setCreditcardtxninfoCreditcardtxnresultinfoCardsecuritycodematch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//setCreditcardtxninfoCreditcardtxnresultinfoReconbatchid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentgroupingcode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//setCreditcardtxninfoCreditcardtxnresultinfoPaymentstatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationtime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//setCreditcardtxninfoCreditcardtxnresultinfoTxnauthorizationstamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//setCreditcardtxninfoCreditcardtxnresultinfoClienttransid() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID
//setOther() - Other

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Template_ListID() - Template_ListID
//set_Template_FullName() - Template_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_BillAddress_Addr1() - BillAddress_Addr1
//set_BillAddress_Addr2() - BillAddress_Addr2
//set_BillAddress_Addr3() - BillAddress_Addr3
//set_BillAddress_Addr4() - BillAddress_Addr4
//set_BillAddress_Addr5() - BillAddress_Addr5
//set_BillAddress_City() - BillAddress_City
//set_BillAddress_State() - BillAddress_State
//set_BillAddress_PostalCode() - BillAddress_PostalCode
//set_BillAddress_Country() - BillAddress_Country
//set_BillAddress_Note() - BillAddress_Note
//set_BillAddressBlock_Addr1() - BillAddressBlock_Addr1
//set_BillAddressBlock_Addr2() - BillAddressBlock_Addr2
//set_BillAddressBlock_Addr3() - BillAddressBlock_Addr3
//set_BillAddressBlock_Addr4() - BillAddressBlock_Addr4
//set_BillAddressBlock_Addr5() - BillAddressBlock_Addr5
//set_ShipAddress_Addr1() - ShipAddress_Addr1
//set_ShipAddress_Addr2() - ShipAddress_Addr2
//set_ShipAddress_Addr3() - ShipAddress_Addr3
//set_ShipAddress_Addr4() - ShipAddress_Addr4
//set_ShipAddress_Addr5() - ShipAddress_Addr5
//set_ShipAddress_City() - ShipAddress_City
//set_ShipAddress_State() - ShipAddress_State
//set_ShipAddress_PostalCode() - ShipAddress_PostalCode
//set_ShipAddress_Country() - ShipAddress_Country
//set_ShipAddress_Note() - ShipAddress_Note
//set_ShipAddressBlock_Addr1() - ShipAddressBlock_Addr1
//set_ShipAddressBlock_Addr2() - ShipAddressBlock_Addr2
//set_ShipAddressBlock_Addr3() - ShipAddressBlock_Addr3
//set_ShipAddressBlock_Addr4() - ShipAddressBlock_Addr4
//set_ShipAddressBlock_Addr5() - ShipAddressBlock_Addr5
//set_IsPending() - IsPending
//set_CheckNumber() - CheckNumber
//set_PaymentMethod_ListID() - PaymentMethod_ListID
//set_PaymentMethod_FullName() - PaymentMethod_FullName
//set_DueDate() - DueDate
//set_SalesRep_ListID() - SalesRep_ListID
//set_SalesRep_FullName() - SalesRep_FullName
//set_ShipDate() - ShipDate
//set_ShipMethod_ListID() - ShipMethod_ListID
//set_ShipMethod_FullName() - ShipMethod_FullName
//set_FOB() - FOB
//set_Subtotal() - Subtotal
//set_ItemSalesTax_ListID() - ItemSalesTax_ListID
//set_ItemSalesTax_FullName() - ItemSalesTax_FullName
//set_SalesTaxPercentage() - SalesTaxPercentage
//set_SalesTaxTotal() - SalesTaxTotal
//set_TotalAmount() - TotalAmount
//set_Currency_ListID() - Currency_ListID
//set_Currency_FullName() - Currency_FullName
//set_ExchangeRate() - ExchangeRate
//set_TotalAmountInHomeCurrency() - TotalAmountInHomeCurrency
//set_Memo() - Memo
//set_CustomerMsg_ListID() - CustomerMsg_ListID
//set_CustomerMsg_FullName() - CustomerMsg_FullName
//set_IsToBePrinted() - IsToBePrinted
//set_IsToBeEmailed() - IsToBeEmailed
//set_CustomerSalesTaxCode_ListID() - CustomerSalesTaxCode_ListID
//set_CustomerSalesTaxCode_FullName() - CustomerSalesTaxCode_FullName
//set_DepositToAccount_ListID() - DepositToAccount_ListID
//set_DepositToAccount_FullName() - DepositToAccount_FullName
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardNumber
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationMonth
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear() - CreditCardTxnInfo_CreditCardTxnInputInfo_ExpirationYear
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard() - CreditCardTxnInfo_CreditCardTxnInputInfo_NameOnCard
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardAddress
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardPostalCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode() - CreditCardTxnInfo_CreditCardTxnInputInfo_CommercialCardCode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode() - CreditCardTxnInfo_CreditCardTxnInputInfo_TransactionMode
//set_CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType() - CreditCardTxnInfo_CreditCardTxnInputInfo_CreditCardTxnType
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage() - CreditCardTxnInfo_CreditCardTxnResultInfo_ResultMessage
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_CreditCardTransID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber() - CreditCardTxnInfo_CreditCardTxnResultInfo_MerchantAccountNumber
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_AuthorizationCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSStreet
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip() - CreditCardTxnInfo_CreditCardTxnResultInfo_AVSZip
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch() - CreditCardTxnInfo_CreditCardTxnResultInfo_CardSecurityCodeMatch
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ReconBatchID
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentGroupingCode
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus() - CreditCardTxnInfo_CreditCardTxnResultInfo_PaymentStatus
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationTime
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp() - CreditCardTxnInfo_CreditCardTxnResultInfo_TxnAuthorizationStamp
//set_CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID() - CreditCardTxnInfo_CreditCardTxnResultInfo_ClientTransID
//set_Other() - Other

*/
/* End of file Qb_salesreceipt_model.php */
/* Location: ./application/models/Qb_salesreceipt_model.php */
