<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_company_subscribedservices_service_model Class
 *
 * Manipulates `qb_company_subscribedservices_service` table on database

CREATE TABLE `qb_company_subscribedservices_service` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Company_CompanyName` text,
  `Name` text,
  `Domain` text,
  `ServiceStatus` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_company_subscribedservices_service` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Company_CompanyName` text NULL   ;
ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Name` text NULL   ;
ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Domain` text NULL   ;
ALTER TABLE  `qb_company_subscribedservices_service` ADD  `ServiceStatus` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_company_subscribedservices_service_model extends MY_Model {

	protected $qbxml_id;
	protected $Company_CompanyName;
	protected $Name;
	protected $Domain;
	protected $ServiceStatus;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_company_subscribedservices_service';
		$this->_short_name = 'qb_company_subscribedservices_service';
		$this->_fields = array("qbxml_id","Company_CompanyName","Name","Domain","ServiceStatus");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Company_CompanyName -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyName` variable
	* @access public
	*/

	public function setCompanyCompanyname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyName` variable
	* @access public
	*/

	public function getCompanyCompanyname() {
		return $this->Company_CompanyName;
	}

	public function get_Company_CompanyName() {
		return $this->Company_CompanyName;
	}

	
// ------------------------------ End Field: Company_CompanyName --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: Domain -------------------------------------- 

	/** 
	* Sets a value to `Domain` variable
	* @access public
	*/

	public function setDomain($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Domain', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Domain($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Domain', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Domain` variable
	* @access public
	*/

	public function getDomain() {
		return $this->Domain;
	}

	public function get_Domain() {
		return $this->Domain;
	}

	
// ------------------------------ End Field: Domain --------------------------------------


// ---------------------------- Start Field: ServiceStatus -------------------------------------- 

	/** 
	* Sets a value to `ServiceStatus` variable
	* @access public
	*/

	public function setServicestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ServiceStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ServiceStatus` variable
	* @access public
	*/

	public function getServicestatus() {
		return $this->ServiceStatus;
	}

	public function get_ServiceStatus() {
		return $this->ServiceStatus;
	}

	
// ------------------------------ End Field: ServiceStatus --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Company_CompanyName' => (object) array(
										'Field'=>'Company_CompanyName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Domain' => (object) array(
										'Field'=>'Domain',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ServiceStatus' => (object) array(
										'Field'=>'ServiceStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_company_subscribedservices_service` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Company_CompanyName' => "ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Company_CompanyName` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Name` text NULL   ;",
			'Domain' => "ALTER TABLE  `qb_company_subscribedservices_service` ADD  `Domain` text NULL   ;",
			'ServiceStatus' => "ALTER TABLE  `qb_company_subscribedservices_service` ADD  `ServiceStatus` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setCompanyCompanyname() - Company_CompanyName
//setName() - Name
//setDomain() - Domain
//setServicestatus() - ServiceStatus

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Company_CompanyName() - Company_CompanyName
//set_Name() - Name
//set_Domain() - Domain
//set_ServiceStatus() - ServiceStatus

*/
/* End of file Qb_company_subscribedservices_service_model.php */
/* Location: ./application/models/Qb_company_subscribedservices_service_model.php */
