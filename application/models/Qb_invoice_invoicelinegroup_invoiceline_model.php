<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_invoice_invoicelinegroup_invoiceline_model Class
 *
 * Manipulates `qb_invoice_invoicelinegroup_invoiceline` table on database

CREATE TABLE `qb_invoice_invoicelinegroup_invoiceline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Invoice_TxnID` varchar(40) DEFAULT NULL,
  `Invoice_InvoiceLineGroup_TxnLineID` text,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  `OverrideUOMSet_ListID` varchar(40) DEFAULT NULL,
  `OverrideUOMSet_FullName` varchar(255) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `RatePercent` decimal(12,5) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `ServiceDate` date DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `Other1` text,
  `Other2` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `Invoice_TxnID` (`Invoice_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `OverrideUOMSet_ListID` (`OverrideUOMSet_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Invoice_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Invoice_InvoiceLineGroup_TxnLineID` text NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `RatePercent` decimal(12,5) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `ServiceDate` date NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Other1` text NULL   ;
ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Other2` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_invoice_invoicelinegroup_invoiceline_model extends MY_Model {

	protected $qbxml_id;
	protected $Invoice_TxnID;
	protected $Invoice_InvoiceLineGroup_TxnLineID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSet_ListID;
	protected $OverrideUOMSet_FullName;
	protected $Rate;
	protected $RatePercent;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Amount;
	protected $ServiceDate;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $Other1;
	protected $Other2;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_invoice_invoicelinegroup_invoiceline';
		$this->_short_name = 'qb_invoice_invoicelinegroup_invoiceline';
		$this->_fields = array("qbxml_id","Invoice_TxnID","Invoice_InvoiceLineGroup_TxnLineID","SortOrder","TxnLineID","Item_ListID","Item_FullName","Descrip","Quantity","UnitOfMeasure","OverrideUOMSet_ListID","OverrideUOMSet_FullName","Rate","RatePercent","Class_ListID","Class_FullName","Amount","ServiceDate","SalesTaxCode_ListID","SalesTaxCode_FullName","Other1","Other2");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Invoice_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Invoice_TxnID` variable
	* @access public
	*/

	public function setInvoiceTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Invoice_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Invoice_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Invoice_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Invoice_TxnID` variable
	* @access public
	*/

	public function getInvoiceTxnid() {
		return $this->Invoice_TxnID;
	}

	public function get_Invoice_TxnID() {
		return $this->Invoice_TxnID;
	}

	
// ------------------------------ End Field: Invoice_TxnID --------------------------------------


// ---------------------------- Start Field: Invoice_InvoiceLineGroup_TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `Invoice_InvoiceLineGroup_TxnLineID` variable
	* @access public
	*/

	public function setInvoiceInvoicelinegroupTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Invoice_InvoiceLineGroup_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Invoice_InvoiceLineGroup_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Invoice_InvoiceLineGroup_TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Invoice_InvoiceLineGroup_TxnLineID` variable
	* @access public
	*/

	public function getInvoiceInvoicelinegroupTxnlineid() {
		return $this->Invoice_InvoiceLineGroup_TxnLineID;
	}

	public function get_Invoice_InvoiceLineGroup_TxnLineID() {
		return $this->Invoice_InvoiceLineGroup_TxnLineID;
	}

	
// ------------------------------ End Field: Invoice_InvoiceLineGroup_TxnLineID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function setOverrideuomsetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function getOverrideuomsetListid() {
		return $this->OverrideUOMSet_ListID;
	}

	public function get_OverrideUOMSet_ListID() {
		return $this->OverrideUOMSet_ListID;
	}

	
// ------------------------------ End Field: OverrideUOMSet_ListID --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function setOverrideuomsetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function getOverrideuomsetFullname() {
		return $this->OverrideUOMSet_FullName;
	}

	public function get_OverrideUOMSet_FullName() {
		return $this->OverrideUOMSet_FullName;
	}

	
// ------------------------------ End Field: OverrideUOMSet_FullName --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: RatePercent -------------------------------------- 

	/** 
	* Sets a value to `RatePercent` variable
	* @access public
	*/

	public function setRatepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RatePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RatePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RatePercent` variable
	* @access public
	*/

	public function getRatepercent() {
		return $this->RatePercent;
	}

	public function get_RatePercent() {
		return $this->RatePercent;
	}

	
// ------------------------------ End Field: RatePercent --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: ServiceDate -------------------------------------- 

	/** 
	* Sets a value to `ServiceDate` variable
	* @access public
	*/

	public function setServicedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ServiceDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ServiceDate` variable
	* @access public
	*/

	public function getServicedate() {
		return $this->ServiceDate;
	}

	public function get_ServiceDate() {
		return $this->ServiceDate;
	}

	
// ------------------------------ End Field: ServiceDate --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: Other1 -------------------------------------- 

	/** 
	* Sets a value to `Other1` variable
	* @access public
	*/

	public function setOther1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other1` variable
	* @access public
	*/

	public function getOther1() {
		return $this->Other1;
	}

	public function get_Other1() {
		return $this->Other1;
	}

	
// ------------------------------ End Field: Other1 --------------------------------------


// ---------------------------- Start Field: Other2 -------------------------------------- 

	/** 
	* Sets a value to `Other2` variable
	* @access public
	*/

	public function setOther2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other2` variable
	* @access public
	*/

	public function getOther2() {
		return $this->Other2;
	}

	public function get_Other2() {
		return $this->Other2;
	}

	
// ------------------------------ End Field: Other2 --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Invoice_TxnID' => (object) array(
										'Field'=>'Invoice_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Invoice_InvoiceLineGroup_TxnLineID' => (object) array(
										'Field'=>'Invoice_InvoiceLineGroup_TxnLineID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_ListID' => (object) array(
										'Field'=>'OverrideUOMSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_FullName' => (object) array(
										'Field'=>'OverrideUOMSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RatePercent' => (object) array(
										'Field'=>'RatePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ServiceDate' => (object) array(
										'Field'=>'ServiceDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other1' => (object) array(
										'Field'=>'Other1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other2' => (object) array(
										'Field'=>'Other2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Invoice_TxnID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Invoice_TxnID` varchar(40) NULL   ;",
			'Invoice_InvoiceLineGroup_TxnLineID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Invoice_InvoiceLineGroup_TxnLineID` text NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSet_ListID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;",
			'OverrideUOMSet_FullName' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Rate` decimal(13,5) NULL   ;",
			'RatePercent' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `RatePercent` decimal(12,5) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Amount` decimal(10,2) NULL   ;",
			'ServiceDate' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `ServiceDate` date NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'Other1' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Other1` text NULL   ;",
			'Other2' => "ALTER TABLE  `qb_invoice_invoicelinegroup_invoiceline` ADD  `Other2` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setInvoiceTxnid() - Invoice_TxnID
//setInvoiceInvoicelinegroupTxnlineid() - Invoice_InvoiceLineGroup_TxnLineID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetListid() - OverrideUOMSet_ListID
//setOverrideuomsetFullname() - OverrideUOMSet_FullName
//setRate() - Rate
//setRatepercent() - RatePercent
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAmount() - Amount
//setServicedate() - ServiceDate
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setOther1() - Other1
//setOther2() - Other2

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Invoice_TxnID() - Invoice_TxnID
//set_Invoice_InvoiceLineGroup_TxnLineID() - Invoice_InvoiceLineGroup_TxnLineID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSet_ListID() - OverrideUOMSet_ListID
//set_OverrideUOMSet_FullName() - OverrideUOMSet_FullName
//set_Rate() - Rate
//set_RatePercent() - RatePercent
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Amount() - Amount
//set_ServiceDate() - ServiceDate
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_Other1() - Other1
//set_Other2() - Other2

*/
/* End of file Qb_invoice_invoicelinegroup_invoiceline_model.php */
/* Location: ./application/models/Qb_invoice_invoicelinegroup_invoiceline_model.php */
