<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_purchaseorder_purchaseorderline_model Class
 *
 * Manipulates `qb_purchaseorder_purchaseorderline` table on database

CREATE TABLE `qb_purchaseorder_purchaseorderline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PurchaseOrder_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `ManufacturerPartNumber` text,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT NULL,
  `UnitOfMeasure` text,
  `OverrideUOMSet_ListID` varchar(40) DEFAULT NULL,
  `OverrideUOMSet_FullName` varchar(255) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `ServiceDate` date DEFAULT NULL,
  `ReceivedQuantity` decimal(12,5) DEFAULT NULL,
  `IsManuallyClosed` tinyint(1) DEFAULT NULL,
  `Other1` text,
  `Other2` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `PurchaseOrder_TxnID` (`PurchaseOrder_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `OverrideUOMSet_ListID` (`OverrideUOMSet_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `IsManuallyClosed` (`IsManuallyClosed`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `PurchaseOrder_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ManufacturerPartNumber` text NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Quantity` decimal(12,5) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ServiceDate` date NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ReceivedQuantity` decimal(12,5) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `IsManuallyClosed` tinyint(1) NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Other1` text NULL   ;
ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Other2` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_purchaseorder_purchaseorderline_model extends MY_Model {

	protected $qbxml_id;
	protected $PurchaseOrder_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $ManufacturerPartNumber;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSet_ListID;
	protected $OverrideUOMSet_FullName;
	protected $Rate;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $Amount;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $ServiceDate;
	protected $ReceivedQuantity;
	protected $IsManuallyClosed;
	protected $Other1;
	protected $Other2;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_purchaseorder_purchaseorderline';
		$this->_short_name = 'qb_purchaseorder_purchaseorderline';
		$this->_fields = array("qbxml_id","PurchaseOrder_TxnID","SortOrder","TxnLineID","Item_ListID","Item_FullName","ManufacturerPartNumber","Descrip","Quantity","UnitOfMeasure","OverrideUOMSet_ListID","OverrideUOMSet_FullName","Rate","Class_ListID","Class_FullName","Amount","Customer_ListID","Customer_FullName","ServiceDate","ReceivedQuantity","IsManuallyClosed","Other1","Other2");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: PurchaseOrder_TxnID -------------------------------------- 

	/** 
	* Sets a value to `PurchaseOrder_TxnID` variable
	* @access public
	*/

	public function setPurchaseorderTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseOrder_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PurchaseOrder_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PurchaseOrder_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PurchaseOrder_TxnID` variable
	* @access public
	*/

	public function getPurchaseorderTxnid() {
		return $this->PurchaseOrder_TxnID;
	}

	public function get_PurchaseOrder_TxnID() {
		return $this->PurchaseOrder_TxnID;
	}

	
// ------------------------------ End Field: PurchaseOrder_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: ManufacturerPartNumber -------------------------------------- 

	/** 
	* Sets a value to `ManufacturerPartNumber` variable
	* @access public
	*/

	public function setManufacturerpartnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ManufacturerPartNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ManufacturerPartNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ManufacturerPartNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ManufacturerPartNumber` variable
	* @access public
	*/

	public function getManufacturerpartnumber() {
		return $this->ManufacturerPartNumber;
	}

	public function get_ManufacturerPartNumber() {
		return $this->ManufacturerPartNumber;
	}

	
// ------------------------------ End Field: ManufacturerPartNumber --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function setOverrideuomsetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function getOverrideuomsetListid() {
		return $this->OverrideUOMSet_ListID;
	}

	public function get_OverrideUOMSet_ListID() {
		return $this->OverrideUOMSet_ListID;
	}

	
// ------------------------------ End Field: OverrideUOMSet_ListID --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function setOverrideuomsetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function getOverrideuomsetFullname() {
		return $this->OverrideUOMSet_FullName;
	}

	public function get_OverrideUOMSet_FullName() {
		return $this->OverrideUOMSet_FullName;
	}

	
// ------------------------------ End Field: OverrideUOMSet_FullName --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: ServiceDate -------------------------------------- 

	/** 
	* Sets a value to `ServiceDate` variable
	* @access public
	*/

	public function setServicedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ServiceDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ServiceDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ServiceDate` variable
	* @access public
	*/

	public function getServicedate() {
		return $this->ServiceDate;
	}

	public function get_ServiceDate() {
		return $this->ServiceDate;
	}

	
// ------------------------------ End Field: ServiceDate --------------------------------------


// ---------------------------- Start Field: ReceivedQuantity -------------------------------------- 

	/** 
	* Sets a value to `ReceivedQuantity` variable
	* @access public
	*/

	public function setReceivedquantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReceivedQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ReceivedQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ReceivedQuantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ReceivedQuantity` variable
	* @access public
	*/

	public function getReceivedquantity() {
		return $this->ReceivedQuantity;
	}

	public function get_ReceivedQuantity() {
		return $this->ReceivedQuantity;
	}

	
// ------------------------------ End Field: ReceivedQuantity --------------------------------------


// ---------------------------- Start Field: IsManuallyClosed -------------------------------------- 

	/** 
	* Sets a value to `IsManuallyClosed` variable
	* @access public
	*/

	public function setIsmanuallyclosed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsManuallyClosed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsManuallyClosed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsManuallyClosed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsManuallyClosed` variable
	* @access public
	*/

	public function getIsmanuallyclosed() {
		return $this->IsManuallyClosed;
	}

	public function get_IsManuallyClosed() {
		return $this->IsManuallyClosed;
	}

	
// ------------------------------ End Field: IsManuallyClosed --------------------------------------


// ---------------------------- Start Field: Other1 -------------------------------------- 

	/** 
	* Sets a value to `Other1` variable
	* @access public
	*/

	public function setOther1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other1` variable
	* @access public
	*/

	public function getOther1() {
		return $this->Other1;
	}

	public function get_Other1() {
		return $this->Other1;
	}

	
// ------------------------------ End Field: Other1 --------------------------------------


// ---------------------------- Start Field: Other2 -------------------------------------- 

	/** 
	* Sets a value to `Other2` variable
	* @access public
	*/

	public function setOther2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other2` variable
	* @access public
	*/

	public function getOther2() {
		return $this->Other2;
	}

	public function get_Other2() {
		return $this->Other2;
	}

	
// ------------------------------ End Field: Other2 --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'PurchaseOrder_TxnID' => (object) array(
										'Field'=>'PurchaseOrder_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ManufacturerPartNumber' => (object) array(
										'Field'=>'ManufacturerPartNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_ListID' => (object) array(
										'Field'=>'OverrideUOMSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_FullName' => (object) array(
										'Field'=>'OverrideUOMSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ServiceDate' => (object) array(
										'Field'=>'ServiceDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ReceivedQuantity' => (object) array(
										'Field'=>'ReceivedQuantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsManuallyClosed' => (object) array(
										'Field'=>'IsManuallyClosed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Other1' => (object) array(
										'Field'=>'Other1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other2' => (object) array(
										'Field'=>'Other2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'PurchaseOrder_TxnID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `PurchaseOrder_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Item_FullName` varchar(255) NULL   ;",
			'ManufacturerPartNumber' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ManufacturerPartNumber` text NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Quantity` decimal(12,5) NULL   ;",
			'UnitOfMeasure' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSet_ListID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;",
			'OverrideUOMSet_FullName' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Rate` decimal(13,5) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Amount` decimal(10,2) NULL   ;",
			'Customer_ListID' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'ServiceDate' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ServiceDate` date NULL   ;",
			'ReceivedQuantity' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `ReceivedQuantity` decimal(12,5) NULL   ;",
			'IsManuallyClosed' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `IsManuallyClosed` tinyint(1) NULL   ;",
			'Other1' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Other1` text NULL   ;",
			'Other2' => "ALTER TABLE  `qb_purchaseorder_purchaseorderline` ADD  `Other2` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setPurchaseorderTxnid() - PurchaseOrder_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setManufacturerpartnumber() - ManufacturerPartNumber
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetListid() - OverrideUOMSet_ListID
//setOverrideuomsetFullname() - OverrideUOMSet_FullName
//setRate() - Rate
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAmount() - Amount
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setServicedate() - ServiceDate
//setReceivedquantity() - ReceivedQuantity
//setIsmanuallyclosed() - IsManuallyClosed
//setOther1() - Other1
//setOther2() - Other2

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_PurchaseOrder_TxnID() - PurchaseOrder_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_ManufacturerPartNumber() - ManufacturerPartNumber
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSet_ListID() - OverrideUOMSet_ListID
//set_OverrideUOMSet_FullName() - OverrideUOMSet_FullName
//set_Rate() - Rate
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_Amount() - Amount
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_ServiceDate() - ServiceDate
//set_ReceivedQuantity() - ReceivedQuantity
//set_IsManuallyClosed() - IsManuallyClosed
//set_Other1() - Other1
//set_Other2() - Other2

*/
/* End of file Qb_purchaseorder_purchaseorderline_model.php */
/* Location: ./application/models/Qb_purchaseorder_purchaseorderline_model.php */
