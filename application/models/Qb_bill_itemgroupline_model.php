<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_bill_itemgroupline_model Class
 *
 * Manipulates `qb_bill_itemgroupline` table on database

CREATE TABLE `qb_bill_itemgroupline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Bill_TxnID` varchar(40) DEFAULT NULL,
  `TxnLineID` text,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `ItemGroup_ListID` varchar(40) DEFAULT NULL,
  `ItemGroup_FullName` varchar(255) DEFAULT NULL,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Bill_TxnID` (`Bill_TxnID`),
  KEY `ItemGroup_ListID` (`ItemGroup_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_bill_itemgroupline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `Bill_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `TxnLineID` text NULL   ;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_bill_itemgroupline` ADD  `ItemGroup_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `ItemGroup_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_bill_itemgroupline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_bill_itemgroupline` ADD  `TotalAmount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_bill_itemgroupline_model extends MY_Model {

	protected $qbxml_id;
	protected $Bill_TxnID;
	protected $TxnLineID;
	protected $SortOrder;
	protected $ItemGroup_ListID;
	protected $ItemGroup_FullName;
	protected $Descrip;
	protected $Quantity;
	protected $TotalAmount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_bill_itemgroupline';
		$this->_short_name = 'qb_bill_itemgroupline';
		$this->_fields = array("qbxml_id","Bill_TxnID","TxnLineID","SortOrder","ItemGroup_ListID","ItemGroup_FullName","Descrip","Quantity","TotalAmount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Bill_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Bill_TxnID` variable
	* @access public
	*/

	public function setBillTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Bill_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Bill_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Bill_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Bill_TxnID` variable
	* @access public
	*/

	public function getBillTxnid() {
		return $this->Bill_TxnID;
	}

	public function get_Bill_TxnID() {
		return $this->Bill_TxnID;
	}

	
// ------------------------------ End Field: Bill_TxnID --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: ItemGroup_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemGroup_ListID` variable
	* @access public
	*/

	public function setItemgroupListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemGroup_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemGroup_ListID` variable
	* @access public
	*/

	public function getItemgroupListid() {
		return $this->ItemGroup_ListID;
	}

	public function get_ItemGroup_ListID() {
		return $this->ItemGroup_ListID;
	}

	
// ------------------------------ End Field: ItemGroup_ListID --------------------------------------


// ---------------------------- Start Field: ItemGroup_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemGroup_FullName` variable
	* @access public
	*/

	public function setItemgroupFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemGroup_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemGroup_FullName` variable
	* @access public
	*/

	public function getItemgroupFullname() {
		return $this->ItemGroup_FullName;
	}

	public function get_ItemGroup_FullName() {
		return $this->ItemGroup_FullName;
	}

	
// ------------------------------ End Field: ItemGroup_FullName --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: TotalAmount -------------------------------------- 

	/** 
	* Sets a value to `TotalAmount` variable
	* @access public
	*/

	public function setTotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmount` variable
	* @access public
	*/

	public function getTotalamount() {
		return $this->TotalAmount;
	}

	public function get_TotalAmount() {
		return $this->TotalAmount;
	}

	
// ------------------------------ End Field: TotalAmount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Bill_TxnID' => (object) array(
										'Field'=>'Bill_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'ItemGroup_ListID' => (object) array(
										'Field'=>'ItemGroup_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemGroup_FullName' => (object) array(
										'Field'=>'ItemGroup_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'TotalAmount' => (object) array(
										'Field'=>'TotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Bill_TxnID' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `Bill_TxnID` varchar(40) NULL   ;",
			'TxnLineID' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `TxnLineID` text NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'ItemGroup_ListID' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `ItemGroup_ListID` varchar(40) NULL   ;",
			'ItemGroup_FullName' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `ItemGroup_FullName` varchar(255) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'TotalAmount' => "ALTER TABLE  `qb_bill_itemgroupline` ADD  `TotalAmount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setBillTxnid() - Bill_TxnID
//setTxnlineid() - TxnLineID
//setSortorder() - SortOrder
//setItemgroupListid() - ItemGroup_ListID
//setItemgroupFullname() - ItemGroup_FullName
//setDescrip() - Descrip
//setQuantity() - Quantity
//setTotalamount() - TotalAmount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Bill_TxnID() - Bill_TxnID
//set_TxnLineID() - TxnLineID
//set_SortOrder() - SortOrder
//set_ItemGroup_ListID() - ItemGroup_ListID
//set_ItemGroup_FullName() - ItemGroup_FullName
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_TotalAmount() - TotalAmount

*/
/* End of file Qb_bill_itemgroupline_model.php */
/* Location: ./application/models/Qb_bill_itemgroupline_model.php */
