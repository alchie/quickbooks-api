<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_vendor_model Class
 *
 * Manipulates `qb_vendor` table on database

CREATE TABLE `qb_vendor` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(41) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `CompanyName` varchar(41) DEFAULT NULL,
  `Salutation` varchar(15) DEFAULT NULL,
  `FirstName` varchar(25) DEFAULT NULL,
  `MiddleName` varchar(5) DEFAULT NULL,
  `LastName` varchar(25) DEFAULT NULL,
  `VendorAddress_Addr1` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr2` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr3` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr4` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr5` varchar(41) DEFAULT NULL,
  `VendorAddress_City` varchar(31) DEFAULT NULL,
  `VendorAddress_State` varchar(21) DEFAULT NULL,
  `VendorAddress_PostalCode` varchar(13) DEFAULT NULL,
  `VendorAddress_Country` varchar(31) DEFAULT NULL,
  `VendorAddress_Note` varchar(41) DEFAULT NULL,
  `VendorAddressBlock_Addr1` text,
  `VendorAddressBlock_Addr2` text,
  `VendorAddressBlock_Addr3` text,
  `VendorAddressBlock_Addr4` text,
  `VendorAddressBlock_Addr5` text,
  `Phone` varchar(21) DEFAULT NULL,
  `AltPhone` varchar(21) DEFAULT NULL,
  `Fax` varchar(21) DEFAULT NULL,
  `Email` text,
  `Contact` varchar(41) DEFAULT NULL,
  `AltContact` varchar(41) DEFAULT NULL,
  `NameOnCheck` varchar(41) DEFAULT NULL,
  `AccountNumber` varchar(99) DEFAULT NULL,
  `Notes` text,
  `VendorType_ListID` varchar(40) DEFAULT NULL,
  `VendorType_FullName` varchar(255) DEFAULT NULL,
  `Terms_ListID` varchar(40) DEFAULT NULL,
  `Terms_FullName` varchar(255) DEFAULT NULL,
  `CreditLimit` decimal(10,2) DEFAULT NULL,
  `VendorTaxIdent` varchar(15) DEFAULT NULL,
  `IsVendorEligibleFor1099` tinyint(1) DEFAULT '0',
  `Balance` decimal(10,2) DEFAULT NULL,
  `BillingRate_ListID` varchar(40) DEFAULT NULL,
  `BillingRate_FullName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `CompanyName` (`CompanyName`),
  KEY `LastName` (`LastName`),
  KEY `VendorType_ListID` (`VendorType_ListID`),
  KEY `Terms_ListID` (`Terms_ListID`),
  KEY `BillingRate_ListID` (`BillingRate_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_vendor` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_vendor` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_vendor` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_vendor` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Name` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_vendor` ADD  `CompanyName` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Salutation` varchar(15) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `FirstName` varchar(25) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `MiddleName` varchar(5) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `LastName` varchar(25) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Phone` varchar(21) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `AltPhone` varchar(21) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Fax` varchar(21) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Email` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Contact` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `AltContact` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `NameOnCheck` varchar(41) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `AccountNumber` varchar(99) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Notes` text NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorType_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorType_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Terms_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `Terms_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `CreditLimit` decimal(10,2) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `VendorTaxIdent` varchar(15) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `IsVendorEligibleFor1099` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_vendor` ADD  `Balance` decimal(10,2) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `BillingRate_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_vendor` ADD  `BillingRate_FullName` varchar(255) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_vendor_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $CompanyName;
	protected $Salutation;
	protected $FirstName;
	protected $MiddleName;
	protected $LastName;
	protected $VendorAddress_Addr1;
	protected $VendorAddress_Addr2;
	protected $VendorAddress_Addr3;
	protected $VendorAddress_Addr4;
	protected $VendorAddress_Addr5;
	protected $VendorAddress_City;
	protected $VendorAddress_State;
	protected $VendorAddress_PostalCode;
	protected $VendorAddress_Country;
	protected $VendorAddress_Note;
	protected $VendorAddressBlock_Addr1;
	protected $VendorAddressBlock_Addr2;
	protected $VendorAddressBlock_Addr3;
	protected $VendorAddressBlock_Addr4;
	protected $VendorAddressBlock_Addr5;
	protected $Phone;
	protected $AltPhone;
	protected $Fax;
	protected $Email;
	protected $Contact;
	protected $AltContact;
	protected $NameOnCheck;
	protected $AccountNumber;
	protected $Notes;
	protected $VendorType_ListID;
	protected $VendorType_FullName;
	protected $Terms_ListID;
	protected $Terms_FullName;
	protected $CreditLimit;
	protected $VendorTaxIdent;
	protected $IsVendorEligibleFor1099;
	protected $Balance;
	protected $BillingRate_ListID;
	protected $BillingRate_FullName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_vendor';
		$this->_short_name = 'qb_vendor';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","CompanyName","Salutation","FirstName","MiddleName","LastName","VendorAddress_Addr1","VendorAddress_Addr2","VendorAddress_Addr3","VendorAddress_Addr4","VendorAddress_Addr5","VendorAddress_City","VendorAddress_State","VendorAddress_PostalCode","VendorAddress_Country","VendorAddress_Note","VendorAddressBlock_Addr1","VendorAddressBlock_Addr2","VendorAddressBlock_Addr3","VendorAddressBlock_Addr4","VendorAddressBlock_Addr5","Phone","AltPhone","Fax","Email","Contact","AltContact","NameOnCheck","AccountNumber","Notes","VendorType_ListID","VendorType_FullName","Terms_ListID","Terms_FullName","CreditLimit","VendorTaxIdent","IsVendorEligibleFor1099","Balance","BillingRate_ListID","BillingRate_FullName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: CompanyName -------------------------------------- 

	/** 
	* Sets a value to `CompanyName` variable
	* @access public
	*/

	public function setCompanyname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CompanyName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CompanyName` variable
	* @access public
	*/

	public function getCompanyname() {
		return $this->CompanyName;
	}

	public function get_CompanyName() {
		return $this->CompanyName;
	}

	
// ------------------------------ End Field: CompanyName --------------------------------------


// ---------------------------- Start Field: Salutation -------------------------------------- 

	/** 
	* Sets a value to `Salutation` variable
	* @access public
	*/

	public function setSalutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Salutation($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Salutation', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Salutation` variable
	* @access public
	*/

	public function getSalutation() {
		return $this->Salutation;
	}

	public function get_Salutation() {
		return $this->Salutation;
	}

	
// ------------------------------ End Field: Salutation --------------------------------------


// ---------------------------- Start Field: FirstName -------------------------------------- 

	/** 
	* Sets a value to `FirstName` variable
	* @access public
	*/

	public function setFirstname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FirstName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FirstName` variable
	* @access public
	*/

	public function getFirstname() {
		return $this->FirstName;
	}

	public function get_FirstName() {
		return $this->FirstName;
	}

	
// ------------------------------ End Field: FirstName --------------------------------------


// ---------------------------- Start Field: MiddleName -------------------------------------- 

	/** 
	* Sets a value to `MiddleName` variable
	* @access public
	*/

	public function setMiddlename($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_MiddleName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('MiddleName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `MiddleName` variable
	* @access public
	*/

	public function getMiddlename() {
		return $this->MiddleName;
	}

	public function get_MiddleName() {
		return $this->MiddleName;
	}

	
// ------------------------------ End Field: MiddleName --------------------------------------


// ---------------------------- Start Field: LastName -------------------------------------- 

	/** 
	* Sets a value to `LastName` variable
	* @access public
	*/

	public function setLastname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LastName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LastName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LastName` variable
	* @access public
	*/

	public function getLastname() {
		return $this->LastName;
	}

	public function get_LastName() {
		return $this->LastName;
	}

	
// ------------------------------ End Field: LastName --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr1` variable
	* @access public
	*/

	public function setVendoraddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr1` variable
	* @access public
	*/

	public function getVendoraddressAddr1() {
		return $this->VendorAddress_Addr1;
	}

	public function get_VendorAddress_Addr1() {
		return $this->VendorAddress_Addr1;
	}

	
// ------------------------------ End Field: VendorAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr2` variable
	* @access public
	*/

	public function setVendoraddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr2` variable
	* @access public
	*/

	public function getVendoraddressAddr2() {
		return $this->VendorAddress_Addr2;
	}

	public function get_VendorAddress_Addr2() {
		return $this->VendorAddress_Addr2;
	}

	
// ------------------------------ End Field: VendorAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr3` variable
	* @access public
	*/

	public function setVendoraddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr3` variable
	* @access public
	*/

	public function getVendoraddressAddr3() {
		return $this->VendorAddress_Addr3;
	}

	public function get_VendorAddress_Addr3() {
		return $this->VendorAddress_Addr3;
	}

	
// ------------------------------ End Field: VendorAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr4` variable
	* @access public
	*/

	public function setVendoraddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr4` variable
	* @access public
	*/

	public function getVendoraddressAddr4() {
		return $this->VendorAddress_Addr4;
	}

	public function get_VendorAddress_Addr4() {
		return $this->VendorAddress_Addr4;
	}

	
// ------------------------------ End Field: VendorAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr5` variable
	* @access public
	*/

	public function setVendoraddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr5` variable
	* @access public
	*/

	public function getVendoraddressAddr5() {
		return $this->VendorAddress_Addr5;
	}

	public function get_VendorAddress_Addr5() {
		return $this->VendorAddress_Addr5;
	}

	
// ------------------------------ End Field: VendorAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: VendorAddress_City -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_City` variable
	* @access public
	*/

	public function setVendoraddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_City` variable
	* @access public
	*/

	public function getVendoraddressCity() {
		return $this->VendorAddress_City;
	}

	public function get_VendorAddress_City() {
		return $this->VendorAddress_City;
	}

	
// ------------------------------ End Field: VendorAddress_City --------------------------------------


// ---------------------------- Start Field: VendorAddress_State -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_State` variable
	* @access public
	*/

	public function setVendoraddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_State` variable
	* @access public
	*/

	public function getVendoraddressState() {
		return $this->VendorAddress_State;
	}

	public function get_VendorAddress_State() {
		return $this->VendorAddress_State;
	}

	
// ------------------------------ End Field: VendorAddress_State --------------------------------------


// ---------------------------- Start Field: VendorAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_PostalCode` variable
	* @access public
	*/

	public function setVendoraddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_PostalCode` variable
	* @access public
	*/

	public function getVendoraddressPostalcode() {
		return $this->VendorAddress_PostalCode;
	}

	public function get_VendorAddress_PostalCode() {
		return $this->VendorAddress_PostalCode;
	}

	
// ------------------------------ End Field: VendorAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: VendorAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Country` variable
	* @access public
	*/

	public function setVendoraddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Country` variable
	* @access public
	*/

	public function getVendoraddressCountry() {
		return $this->VendorAddress_Country;
	}

	public function get_VendorAddress_Country() {
		return $this->VendorAddress_Country;
	}

	
// ------------------------------ End Field: VendorAddress_Country --------------------------------------


// ---------------------------- Start Field: VendorAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Note` variable
	* @access public
	*/

	public function setVendoraddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Note` variable
	* @access public
	*/

	public function getVendoraddressNote() {
		return $this->VendorAddress_Note;
	}

	public function get_VendorAddress_Note() {
		return $this->VendorAddress_Note;
	}

	
// ------------------------------ End Field: VendorAddress_Note --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr1` variable
	* @access public
	*/

	public function setVendoraddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr1` variable
	* @access public
	*/

	public function getVendoraddressblockAddr1() {
		return $this->VendorAddressBlock_Addr1;
	}

	public function get_VendorAddressBlock_Addr1() {
		return $this->VendorAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr2` variable
	* @access public
	*/

	public function setVendoraddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr2` variable
	* @access public
	*/

	public function getVendoraddressblockAddr2() {
		return $this->VendorAddressBlock_Addr2;
	}

	public function get_VendorAddressBlock_Addr2() {
		return $this->VendorAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr3` variable
	* @access public
	*/

	public function setVendoraddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr3` variable
	* @access public
	*/

	public function getVendoraddressblockAddr3() {
		return $this->VendorAddressBlock_Addr3;
	}

	public function get_VendorAddressBlock_Addr3() {
		return $this->VendorAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr4` variable
	* @access public
	*/

	public function setVendoraddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr4` variable
	* @access public
	*/

	public function getVendoraddressblockAddr4() {
		return $this->VendorAddressBlock_Addr4;
	}

	public function get_VendorAddressBlock_Addr4() {
		return $this->VendorAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr5` variable
	* @access public
	*/

	public function setVendoraddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr5` variable
	* @access public
	*/

	public function getVendoraddressblockAddr5() {
		return $this->VendorAddressBlock_Addr5;
	}

	public function get_VendorAddressBlock_Addr5() {
		return $this->VendorAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: Phone -------------------------------------- 

	/** 
	* Sets a value to `Phone` variable
	* @access public
	*/

	public function setPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Phone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Phone` variable
	* @access public
	*/

	public function getPhone() {
		return $this->Phone;
	}

	public function get_Phone() {
		return $this->Phone;
	}

	
// ------------------------------ End Field: Phone --------------------------------------


// ---------------------------- Start Field: AltPhone -------------------------------------- 

	/** 
	* Sets a value to `AltPhone` variable
	* @access public
	*/

	public function setAltphone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltPhone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltPhone` variable
	* @access public
	*/

	public function getAltphone() {
		return $this->AltPhone;
	}

	public function get_AltPhone() {
		return $this->AltPhone;
	}

	
// ------------------------------ End Field: AltPhone --------------------------------------


// ---------------------------- Start Field: Fax -------------------------------------- 

	/** 
	* Sets a value to `Fax` variable
	* @access public
	*/

	public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Fax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Fax` variable
	* @access public
	*/

	public function getFax() {
		return $this->Fax;
	}

	public function get_Fax() {
		return $this->Fax;
	}

	
// ------------------------------ End Field: Fax --------------------------------------


// ---------------------------- Start Field: Email -------------------------------------- 

	/** 
	* Sets a value to `Email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Email($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->Email;
	}

	public function get_Email() {
		return $this->Email;
	}

	
// ------------------------------ End Field: Email --------------------------------------


// ---------------------------- Start Field: Contact -------------------------------------- 

	/** 
	* Sets a value to `Contact` variable
	* @access public
	*/

	public function setContact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Contact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Contact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Contact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Contact` variable
	* @access public
	*/

	public function getContact() {
		return $this->Contact;
	}

	public function get_Contact() {
		return $this->Contact;
	}

	
// ------------------------------ End Field: Contact --------------------------------------


// ---------------------------- Start Field: AltContact -------------------------------------- 

	/** 
	* Sets a value to `AltContact` variable
	* @access public
	*/

	public function setAltcontact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltContact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AltContact($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AltContact', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AltContact` variable
	* @access public
	*/

	public function getAltcontact() {
		return $this->AltContact;
	}

	public function get_AltContact() {
		return $this->AltContact;
	}

	
// ------------------------------ End Field: AltContact --------------------------------------


// ---------------------------- Start Field: NameOnCheck -------------------------------------- 

	/** 
	* Sets a value to `NameOnCheck` variable
	* @access public
	*/

	public function setNameoncheck($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('NameOnCheck', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_NameOnCheck($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('NameOnCheck', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `NameOnCheck` variable
	* @access public
	*/

	public function getNameoncheck() {
		return $this->NameOnCheck;
	}

	public function get_NameOnCheck() {
		return $this->NameOnCheck;
	}

	
// ------------------------------ End Field: NameOnCheck --------------------------------------


// ---------------------------- Start Field: AccountNumber -------------------------------------- 

	/** 
	* Sets a value to `AccountNumber` variable
	* @access public
	*/

	public function setAccountnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AccountNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AccountNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AccountNumber` variable
	* @access public
	*/

	public function getAccountnumber() {
		return $this->AccountNumber;
	}

	public function get_AccountNumber() {
		return $this->AccountNumber;
	}

	
// ------------------------------ End Field: AccountNumber --------------------------------------


// ---------------------------- Start Field: Notes -------------------------------------- 

	/** 
	* Sets a value to `Notes` variable
	* @access public
	*/

	public function setNotes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Notes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Notes', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Notes` variable
	* @access public
	*/

	public function getNotes() {
		return $this->Notes;
	}

	public function get_Notes() {
		return $this->Notes;
	}

	
// ------------------------------ End Field: Notes --------------------------------------


// ---------------------------- Start Field: VendorType_ListID -------------------------------------- 

	/** 
	* Sets a value to `VendorType_ListID` variable
	* @access public
	*/

	public function setVendortypeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorType_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorType_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorType_ListID` variable
	* @access public
	*/

	public function getVendortypeListid() {
		return $this->VendorType_ListID;
	}

	public function get_VendorType_ListID() {
		return $this->VendorType_ListID;
	}

	
// ------------------------------ End Field: VendorType_ListID --------------------------------------


// ---------------------------- Start Field: VendorType_FullName -------------------------------------- 

	/** 
	* Sets a value to `VendorType_FullName` variable
	* @access public
	*/

	public function setVendortypeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorType_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorType_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorType_FullName` variable
	* @access public
	*/

	public function getVendortypeFullname() {
		return $this->VendorType_FullName;
	}

	public function get_VendorType_FullName() {
		return $this->VendorType_FullName;
	}

	
// ------------------------------ End Field: VendorType_FullName --------------------------------------


// ---------------------------- Start Field: Terms_ListID -------------------------------------- 

	/** 
	* Sets a value to `Terms_ListID` variable
	* @access public
	*/

	public function setTermsListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_ListID` variable
	* @access public
	*/

	public function getTermsListid() {
		return $this->Terms_ListID;
	}

	public function get_Terms_ListID() {
		return $this->Terms_ListID;
	}

	
// ------------------------------ End Field: Terms_ListID --------------------------------------


// ---------------------------- Start Field: Terms_FullName -------------------------------------- 

	/** 
	* Sets a value to `Terms_FullName` variable
	* @access public
	*/

	public function setTermsFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_FullName` variable
	* @access public
	*/

	public function getTermsFullname() {
		return $this->Terms_FullName;
	}

	public function get_Terms_FullName() {
		return $this->Terms_FullName;
	}

	
// ------------------------------ End Field: Terms_FullName --------------------------------------


// ---------------------------- Start Field: CreditLimit -------------------------------------- 

	/** 
	* Sets a value to `CreditLimit` variable
	* @access public
	*/

	public function setCreditlimit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditLimit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditLimit($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditLimit', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditLimit` variable
	* @access public
	*/

	public function getCreditlimit() {
		return $this->CreditLimit;
	}

	public function get_CreditLimit() {
		return $this->CreditLimit;
	}

	
// ------------------------------ End Field: CreditLimit --------------------------------------


// ---------------------------- Start Field: VendorTaxIdent -------------------------------------- 

	/** 
	* Sets a value to `VendorTaxIdent` variable
	* @access public
	*/

	public function setVendortaxident($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorTaxIdent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorTaxIdent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorTaxIdent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorTaxIdent` variable
	* @access public
	*/

	public function getVendortaxident() {
		return $this->VendorTaxIdent;
	}

	public function get_VendorTaxIdent() {
		return $this->VendorTaxIdent;
	}

	
// ------------------------------ End Field: VendorTaxIdent --------------------------------------


// ---------------------------- Start Field: IsVendorEligibleFor1099 -------------------------------------- 

	/** 
	* Sets a value to `IsVendorEligibleFor1099` variable
	* @access public
	*/

	public function setIsvendoreligiblefor1099($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsVendorEligibleFor1099', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsVendorEligibleFor1099($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsVendorEligibleFor1099', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsVendorEligibleFor1099` variable
	* @access public
	*/

	public function getIsvendoreligiblefor1099() {
		return $this->IsVendorEligibleFor1099;
	}

	public function get_IsVendorEligibleFor1099() {
		return $this->IsVendorEligibleFor1099;
	}

	
// ------------------------------ End Field: IsVendorEligibleFor1099 --------------------------------------


// ---------------------------- Start Field: Balance -------------------------------------- 

	/** 
	* Sets a value to `Balance` variable
	* @access public
	*/

	public function setBalance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Balance($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Balance', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Balance` variable
	* @access public
	*/

	public function getBalance() {
		return $this->Balance;
	}

	public function get_Balance() {
		return $this->Balance;
	}

	
// ------------------------------ End Field: Balance --------------------------------------


// ---------------------------- Start Field: BillingRate_ListID -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_ListID` variable
	* @access public
	*/

	public function setBillingrateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_ListID` variable
	* @access public
	*/

	public function getBillingrateListid() {
		return $this->BillingRate_ListID;
	}

	public function get_BillingRate_ListID() {
		return $this->BillingRate_ListID;
	}

	
// ------------------------------ End Field: BillingRate_ListID --------------------------------------


// ---------------------------- Start Field: BillingRate_FullName -------------------------------------- 

	/** 
	* Sets a value to `BillingRate_FullName` variable
	* @access public
	*/

	public function setBillingrateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillingRate_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillingRate_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillingRate_FullName` variable
	* @access public
	*/

	public function getBillingrateFullname() {
		return $this->BillingRate_FullName;
	}

	public function get_BillingRate_FullName() {
		return $this->BillingRate_FullName;
	}

	
// ------------------------------ End Field: BillingRate_FullName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'CompanyName' => (object) array(
										'Field'=>'CompanyName',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Salutation' => (object) array(
										'Field'=>'Salutation',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FirstName' => (object) array(
										'Field'=>'FirstName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'MiddleName' => (object) array(
										'Field'=>'MiddleName',
										'Type'=>'varchar(5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LastName' => (object) array(
										'Field'=>'LastName',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr1' => (object) array(
										'Field'=>'VendorAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr2' => (object) array(
										'Field'=>'VendorAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr3' => (object) array(
										'Field'=>'VendorAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr4' => (object) array(
										'Field'=>'VendorAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr5' => (object) array(
										'Field'=>'VendorAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_City' => (object) array(
										'Field'=>'VendorAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_State' => (object) array(
										'Field'=>'VendorAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_PostalCode' => (object) array(
										'Field'=>'VendorAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Country' => (object) array(
										'Field'=>'VendorAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Note' => (object) array(
										'Field'=>'VendorAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr1' => (object) array(
										'Field'=>'VendorAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr2' => (object) array(
										'Field'=>'VendorAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr3' => (object) array(
										'Field'=>'VendorAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr4' => (object) array(
										'Field'=>'VendorAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr5' => (object) array(
										'Field'=>'VendorAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Phone' => (object) array(
										'Field'=>'Phone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltPhone' => (object) array(
										'Field'=>'AltPhone',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Fax' => (object) array(
										'Field'=>'Fax',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Email' => (object) array(
										'Field'=>'Email',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Contact' => (object) array(
										'Field'=>'Contact',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AltContact' => (object) array(
										'Field'=>'AltContact',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'NameOnCheck' => (object) array(
										'Field'=>'NameOnCheck',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AccountNumber' => (object) array(
										'Field'=>'AccountNumber',
										'Type'=>'varchar(99)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Notes' => (object) array(
										'Field'=>'Notes',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorType_ListID' => (object) array(
										'Field'=>'VendorType_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorType_FullName' => (object) array(
										'Field'=>'VendorType_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_ListID' => (object) array(
										'Field'=>'Terms_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_FullName' => (object) array(
										'Field'=>'Terms_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditLimit' => (object) array(
										'Field'=>'CreditLimit',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorTaxIdent' => (object) array(
										'Field'=>'VendorTaxIdent',
										'Type'=>'varchar(15)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsVendorEligibleFor1099' => (object) array(
										'Field'=>'IsVendorEligibleFor1099',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Balance' => (object) array(
										'Field'=>'Balance',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillingRate_ListID' => (object) array(
										'Field'=>'BillingRate_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillingRate_FullName' => (object) array(
										'Field'=>'BillingRate_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_vendor` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_vendor` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_vendor` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_vendor` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_vendor` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_vendor` ADD  `Name` varchar(41) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_vendor` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'CompanyName' => "ALTER TABLE  `qb_vendor` ADD  `CompanyName` varchar(41) NULL   ;",
			'Salutation' => "ALTER TABLE  `qb_vendor` ADD  `Salutation` varchar(15) NULL   ;",
			'FirstName' => "ALTER TABLE  `qb_vendor` ADD  `FirstName` varchar(25) NULL   ;",
			'MiddleName' => "ALTER TABLE  `qb_vendor` ADD  `MiddleName` varchar(5) NULL   ;",
			'LastName' => "ALTER TABLE  `qb_vendor` ADD  `LastName` varchar(25) NULL   ;",
			'VendorAddress_Addr1' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr1` varchar(41) NULL   ;",
			'VendorAddress_Addr2' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr2` varchar(41) NULL   ;",
			'VendorAddress_Addr3' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr3` varchar(41) NULL   ;",
			'VendorAddress_Addr4' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr4` varchar(41) NULL   ;",
			'VendorAddress_Addr5' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Addr5` varchar(41) NULL   ;",
			'VendorAddress_City' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_City` varchar(31) NULL   ;",
			'VendorAddress_State' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_State` varchar(21) NULL   ;",
			'VendorAddress_PostalCode' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_PostalCode` varchar(13) NULL   ;",
			'VendorAddress_Country' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Country` varchar(31) NULL   ;",
			'VendorAddress_Note' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddress_Note` varchar(41) NULL   ;",
			'VendorAddressBlock_Addr1' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr1` text NULL   ;",
			'VendorAddressBlock_Addr2' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr2` text NULL   ;",
			'VendorAddressBlock_Addr3' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr3` text NULL   ;",
			'VendorAddressBlock_Addr4' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr4` text NULL   ;",
			'VendorAddressBlock_Addr5' => "ALTER TABLE  `qb_vendor` ADD  `VendorAddressBlock_Addr5` text NULL   ;",
			'Phone' => "ALTER TABLE  `qb_vendor` ADD  `Phone` varchar(21) NULL   ;",
			'AltPhone' => "ALTER TABLE  `qb_vendor` ADD  `AltPhone` varchar(21) NULL   ;",
			'Fax' => "ALTER TABLE  `qb_vendor` ADD  `Fax` varchar(21) NULL   ;",
			'Email' => "ALTER TABLE  `qb_vendor` ADD  `Email` text NULL   ;",
			'Contact' => "ALTER TABLE  `qb_vendor` ADD  `Contact` varchar(41) NULL   ;",
			'AltContact' => "ALTER TABLE  `qb_vendor` ADD  `AltContact` varchar(41) NULL   ;",
			'NameOnCheck' => "ALTER TABLE  `qb_vendor` ADD  `NameOnCheck` varchar(41) NULL   ;",
			'AccountNumber' => "ALTER TABLE  `qb_vendor` ADD  `AccountNumber` varchar(99) NULL   ;",
			'Notes' => "ALTER TABLE  `qb_vendor` ADD  `Notes` text NULL   ;",
			'VendorType_ListID' => "ALTER TABLE  `qb_vendor` ADD  `VendorType_ListID` varchar(40) NULL   ;",
			'VendorType_FullName' => "ALTER TABLE  `qb_vendor` ADD  `VendorType_FullName` varchar(255) NULL   ;",
			'Terms_ListID' => "ALTER TABLE  `qb_vendor` ADD  `Terms_ListID` varchar(40) NULL   ;",
			'Terms_FullName' => "ALTER TABLE  `qb_vendor` ADD  `Terms_FullName` varchar(255) NULL   ;",
			'CreditLimit' => "ALTER TABLE  `qb_vendor` ADD  `CreditLimit` decimal(10,2) NULL   ;",
			'VendorTaxIdent' => "ALTER TABLE  `qb_vendor` ADD  `VendorTaxIdent` varchar(15) NULL   ;",
			'IsVendorEligibleFor1099' => "ALTER TABLE  `qb_vendor` ADD  `IsVendorEligibleFor1099` tinyint(1) NULL   DEFAULT '0';",
			'Balance' => "ALTER TABLE  `qb_vendor` ADD  `Balance` decimal(10,2) NULL   ;",
			'BillingRate_ListID' => "ALTER TABLE  `qb_vendor` ADD  `BillingRate_ListID` varchar(40) NULL   ;",
			'BillingRate_FullName' => "ALTER TABLE  `qb_vendor` ADD  `BillingRate_FullName` varchar(255) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setCompanyname() - CompanyName
//setSalutation() - Salutation
//setFirstname() - FirstName
//setMiddlename() - MiddleName
//setLastname() - LastName
//setVendoraddressAddr1() - VendorAddress_Addr1
//setVendoraddressAddr2() - VendorAddress_Addr2
//setVendoraddressAddr3() - VendorAddress_Addr3
//setVendoraddressAddr4() - VendorAddress_Addr4
//setVendoraddressAddr5() - VendorAddress_Addr5
//setVendoraddressCity() - VendorAddress_City
//setVendoraddressState() - VendorAddress_State
//setVendoraddressPostalcode() - VendorAddress_PostalCode
//setVendoraddressCountry() - VendorAddress_Country
//setVendoraddressNote() - VendorAddress_Note
//setVendoraddressblockAddr1() - VendorAddressBlock_Addr1
//setVendoraddressblockAddr2() - VendorAddressBlock_Addr2
//setVendoraddressblockAddr3() - VendorAddressBlock_Addr3
//setVendoraddressblockAddr4() - VendorAddressBlock_Addr4
//setVendoraddressblockAddr5() - VendorAddressBlock_Addr5
//setPhone() - Phone
//setAltphone() - AltPhone
//setFax() - Fax
//setEmail() - Email
//setContact() - Contact
//setAltcontact() - AltContact
//setNameoncheck() - NameOnCheck
//setAccountnumber() - AccountNumber
//setNotes() - Notes
//setVendortypeListid() - VendorType_ListID
//setVendortypeFullname() - VendorType_FullName
//setTermsListid() - Terms_ListID
//setTermsFullname() - Terms_FullName
//setCreditlimit() - CreditLimit
//setVendortaxident() - VendorTaxIdent
//setIsvendoreligiblefor1099() - IsVendorEligibleFor1099
//setBalance() - Balance
//setBillingrateListid() - BillingRate_ListID
//setBillingrateFullname() - BillingRate_FullName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_CompanyName() - CompanyName
//set_Salutation() - Salutation
//set_FirstName() - FirstName
//set_MiddleName() - MiddleName
//set_LastName() - LastName
//set_VendorAddress_Addr1() - VendorAddress_Addr1
//set_VendorAddress_Addr2() - VendorAddress_Addr2
//set_VendorAddress_Addr3() - VendorAddress_Addr3
//set_VendorAddress_Addr4() - VendorAddress_Addr4
//set_VendorAddress_Addr5() - VendorAddress_Addr5
//set_VendorAddress_City() - VendorAddress_City
//set_VendorAddress_State() - VendorAddress_State
//set_VendorAddress_PostalCode() - VendorAddress_PostalCode
//set_VendorAddress_Country() - VendorAddress_Country
//set_VendorAddress_Note() - VendorAddress_Note
//set_VendorAddressBlock_Addr1() - VendorAddressBlock_Addr1
//set_VendorAddressBlock_Addr2() - VendorAddressBlock_Addr2
//set_VendorAddressBlock_Addr3() - VendorAddressBlock_Addr3
//set_VendorAddressBlock_Addr4() - VendorAddressBlock_Addr4
//set_VendorAddressBlock_Addr5() - VendorAddressBlock_Addr5
//set_Phone() - Phone
//set_AltPhone() - AltPhone
//set_Fax() - Fax
//set_Email() - Email
//set_Contact() - Contact
//set_AltContact() - AltContact
//set_NameOnCheck() - NameOnCheck
//set_AccountNumber() - AccountNumber
//set_Notes() - Notes
//set_VendorType_ListID() - VendorType_ListID
//set_VendorType_FullName() - VendorType_FullName
//set_Terms_ListID() - Terms_ListID
//set_Terms_FullName() - Terms_FullName
//set_CreditLimit() - CreditLimit
//set_VendorTaxIdent() - VendorTaxIdent
//set_IsVendorEligibleFor1099() - IsVendorEligibleFor1099
//set_Balance() - Balance
//set_BillingRate_ListID() - BillingRate_ListID
//set_BillingRate_FullName() - BillingRate_FullName

*/
/* End of file Qb_vendor_model.php */
/* Location: ./application/models/Qb_vendor_model.php */
