<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_itemservice_model Class
 *
 * Manipulates `qb_itemservice` table on database

CREATE TABLE `qb_itemservice` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` varchar(31) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT '0',
  `Parent_ListID` varchar(40) DEFAULT NULL,
  `Parent_FullName` varchar(255) DEFAULT NULL,
  `Sublevel` int(10) unsigned DEFAULT '0',
  `UnitOfMeasureSet_ListID` varchar(40) DEFAULT NULL,
  `UnitOfMeasureSet_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `SalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `SalesOrPurchase_Desc` text,
  `SalesOrPurchase_Price` decimal(13,5) DEFAULT NULL,
  `SalesOrPurchase_PricePercent` decimal(12,5) DEFAULT NULL,
  `SalesOrPurchase_Account_ListID` varchar(40) DEFAULT NULL,
  `SalesOrPurchase_Account_FullName` varchar(255) DEFAULT NULL,
  `SalesAndPurchase_SalesDesc` text,
  `SalesAndPurchase_SalesPrice` decimal(13,5) DEFAULT NULL,
  `SalesAndPurchase_IncomeAccount_ListID` varchar(40) DEFAULT NULL,
  `SalesAndPurchase_IncomeAccount_FullName` varchar(255) DEFAULT NULL,
  `SalesAndPurchase_PurchaseDesc` text,
  `SalesAndPurchase_PurchaseCost` decimal(13,5) DEFAULT NULL,
  `SalesAndPurchase_ExpenseAccount_ListID` varchar(40) DEFAULT NULL,
  `SalesAndPurchase_ExpenseAccount_FullName` varchar(255) DEFAULT NULL,
  `SalesAndPurchase_PrefVendor_ListID` varchar(40) DEFAULT NULL,
  `SalesAndPurchase_PrefVendor_FullName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `FullName` (`FullName`),
  KEY `IsActive` (`IsActive`),
  KEY `Parent_ListID` (`Parent_ListID`),
  KEY `UnitOfMeasureSet_ListID` (`UnitOfMeasureSet_ListID`),
  KEY `SalesTaxCode_ListID` (`SalesTaxCode_ListID`),
  KEY `SalesOrPurchase_Account_ListID` (`SalesOrPurchase_Account_ListID`),
  KEY `SalesAndPurchase_IncomeAccount_ListID` (`SalesAndPurchase_IncomeAccount_ListID`),
  KEY `SalesAndPurchase_ExpenseAccount_ListID` (`SalesAndPurchase_ExpenseAccount_ListID`),
  KEY `SalesAndPurchase_PrefVendor_ListID` (`SalesAndPurchase_PrefVendor_ListID`),
  KEY `ListID` (`ListID`),
  KEY `Name` (`Name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_itemservice` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_itemservice` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `Name` varchar(31) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_itemservice` ADD  `Parent_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `Parent_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_itemservice` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Desc` text NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Price` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_PricePercent` decimal(12,5) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_SalesDesc` text NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_SalesPrice` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_IncomeAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_IncomeAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PurchaseDesc` text NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PurchaseCost` decimal(13,5) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_ExpenseAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_ExpenseAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PrefVendor_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PrefVendor_FullName` varchar(255) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_itemservice_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $FullName;
	protected $IsActive;
	protected $Parent_ListID;
	protected $Parent_FullName;
	protected $Sublevel;
	protected $UnitOfMeasureSet_ListID;
	protected $UnitOfMeasureSet_FullName;
	protected $SalesTaxCode_ListID;
	protected $SalesTaxCode_FullName;
	protected $SalesOrPurchase_Desc;
	protected $SalesOrPurchase_Price;
	protected $SalesOrPurchase_PricePercent;
	protected $SalesOrPurchase_Account_ListID;
	protected $SalesOrPurchase_Account_FullName;
	protected $SalesAndPurchase_SalesDesc;
	protected $SalesAndPurchase_SalesPrice;
	protected $SalesAndPurchase_IncomeAccount_ListID;
	protected $SalesAndPurchase_IncomeAccount_FullName;
	protected $SalesAndPurchase_PurchaseDesc;
	protected $SalesAndPurchase_PurchaseCost;
	protected $SalesAndPurchase_ExpenseAccount_ListID;
	protected $SalesAndPurchase_ExpenseAccount_FullName;
	protected $SalesAndPurchase_PrefVendor_ListID;
	protected $SalesAndPurchase_PrefVendor_FullName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_itemservice';
		$this->_short_name = 'qb_itemservice';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","FullName","IsActive","Parent_ListID","Parent_FullName","Sublevel","UnitOfMeasureSet_ListID","UnitOfMeasureSet_FullName","SalesTaxCode_ListID","SalesTaxCode_FullName","SalesOrPurchase_Desc","SalesOrPurchase_Price","SalesOrPurchase_PricePercent","SalesOrPurchase_Account_ListID","SalesOrPurchase_Account_FullName","SalesAndPurchase_SalesDesc","SalesAndPurchase_SalesPrice","SalesAndPurchase_IncomeAccount_ListID","SalesAndPurchase_IncomeAccount_FullName","SalesAndPurchase_PurchaseDesc","SalesAndPurchase_PurchaseCost","SalesAndPurchase_ExpenseAccount_ListID","SalesAndPurchase_ExpenseAccount_FullName","SalesAndPurchase_PrefVendor_ListID","SalesAndPurchase_PrefVendor_FullName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: FullName -------------------------------------- 

	/** 
	* Sets a value to `FullName` variable
	* @access public
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FullName` variable
	* @access public
	*/

	public function getFullname() {
		return $this->FullName;
	}

	public function get_FullName() {
		return $this->FullName;
	}

	
// ------------------------------ End Field: FullName --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: Parent_ListID -------------------------------------- 

	/** 
	* Sets a value to `Parent_ListID` variable
	* @access public
	*/

	public function setParentListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_ListID` variable
	* @access public
	*/

	public function getParentListid() {
		return $this->Parent_ListID;
	}

	public function get_Parent_ListID() {
		return $this->Parent_ListID;
	}

	
// ------------------------------ End Field: Parent_ListID --------------------------------------


// ---------------------------- Start Field: Parent_FullName -------------------------------------- 

	/** 
	* Sets a value to `Parent_FullName` variable
	* @access public
	*/

	public function setParentFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Parent_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Parent_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Parent_FullName` variable
	* @access public
	*/

	public function getParentFullname() {
		return $this->Parent_FullName;
	}

	public function get_Parent_FullName() {
		return $this->Parent_FullName;
	}

	
// ------------------------------ End Field: Parent_FullName --------------------------------------


// ---------------------------- Start Field: Sublevel -------------------------------------- 

	/** 
	* Sets a value to `Sublevel` variable
	* @access public
	*/

	public function setSublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Sublevel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Sublevel', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Sublevel` variable
	* @access public
	*/

	public function getSublevel() {
		return $this->Sublevel;
	}

	public function get_Sublevel() {
		return $this->Sublevel;
	}

	
// ------------------------------ End Field: Sublevel --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function setUnitofmeasuresetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_ListID` variable
	* @access public
	*/

	public function getUnitofmeasuresetListid() {
		return $this->UnitOfMeasureSet_ListID;
	}

	public function get_UnitOfMeasureSet_ListID() {
		return $this->UnitOfMeasureSet_ListID;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_ListID --------------------------------------


// ---------------------------- Start Field: UnitOfMeasureSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function setUnitofmeasuresetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasureSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasureSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasureSet_FullName` variable
	* @access public
	*/

	public function getUnitofmeasuresetFullname() {
		return $this->UnitOfMeasureSet_FullName;
	}

	public function get_UnitOfMeasureSet_FullName() {
		return $this->UnitOfMeasureSet_FullName;
	}

	
// ------------------------------ End Field: UnitOfMeasureSet_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function setSalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_ListID` variable
	* @access public
	*/

	public function getSalestaxcodeListid() {
		return $this->SalesTaxCode_ListID;
	}

	public function get_SalesTaxCode_ListID() {
		return $this->SalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: SalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: SalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function setSalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxCode_FullName` variable
	* @access public
	*/

	public function getSalestaxcodeFullname() {
		return $this->SalesTaxCode_FullName;
	}

	public function get_SalesTaxCode_FullName() {
		return $this->SalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: SalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: SalesOrPurchase_Desc -------------------------------------- 

	/** 
	* Sets a value to `SalesOrPurchase_Desc` variable
	* @access public
	*/

	public function setSalesorpurchaseDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Desc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrPurchase_Desc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Desc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrPurchase_Desc` variable
	* @access public
	*/

	public function getSalesorpurchaseDesc() {
		return $this->SalesOrPurchase_Desc;
	}

	public function get_SalesOrPurchase_Desc() {
		return $this->SalesOrPurchase_Desc;
	}

	
// ------------------------------ End Field: SalesOrPurchase_Desc --------------------------------------


// ---------------------------- Start Field: SalesOrPurchase_Price -------------------------------------- 

	/** 
	* Sets a value to `SalesOrPurchase_Price` variable
	* @access public
	*/

	public function setSalesorpurchasePrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Price', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrPurchase_Price($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Price', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrPurchase_Price` variable
	* @access public
	*/

	public function getSalesorpurchasePrice() {
		return $this->SalesOrPurchase_Price;
	}

	public function get_SalesOrPurchase_Price() {
		return $this->SalesOrPurchase_Price;
	}

	
// ------------------------------ End Field: SalesOrPurchase_Price --------------------------------------


// ---------------------------- Start Field: SalesOrPurchase_PricePercent -------------------------------------- 

	/** 
	* Sets a value to `SalesOrPurchase_PricePercent` variable
	* @access public
	*/

	public function setSalesorpurchasePricepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_PricePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrPurchase_PricePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_PricePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrPurchase_PricePercent` variable
	* @access public
	*/

	public function getSalesorpurchasePricepercent() {
		return $this->SalesOrPurchase_PricePercent;
	}

	public function get_SalesOrPurchase_PricePercent() {
		return $this->SalesOrPurchase_PricePercent;
	}

	
// ------------------------------ End Field: SalesOrPurchase_PricePercent --------------------------------------


// ---------------------------- Start Field: SalesOrPurchase_Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesOrPurchase_Account_ListID` variable
	* @access public
	*/

	public function setSalesorpurchaseAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrPurchase_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrPurchase_Account_ListID` variable
	* @access public
	*/

	public function getSalesorpurchaseAccountListid() {
		return $this->SalesOrPurchase_Account_ListID;
	}

	public function get_SalesOrPurchase_Account_ListID() {
		return $this->SalesOrPurchase_Account_ListID;
	}

	
// ------------------------------ End Field: SalesOrPurchase_Account_ListID --------------------------------------


// ---------------------------- Start Field: SalesOrPurchase_Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesOrPurchase_Account_FullName` variable
	* @access public
	*/

	public function setSalesorpurchaseAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrPurchase_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrPurchase_Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrPurchase_Account_FullName` variable
	* @access public
	*/

	public function getSalesorpurchaseAccountFullname() {
		return $this->SalesOrPurchase_Account_FullName;
	}

	public function get_SalesOrPurchase_Account_FullName() {
		return $this->SalesOrPurchase_Account_FullName;
	}

	
// ------------------------------ End Field: SalesOrPurchase_Account_FullName --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_SalesDesc -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_SalesDesc` variable
	* @access public
	*/

	public function setSalesandpurchaseSalesdesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_SalesDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_SalesDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_SalesDesc` variable
	* @access public
	*/

	public function getSalesandpurchaseSalesdesc() {
		return $this->SalesAndPurchase_SalesDesc;
	}

	public function get_SalesAndPurchase_SalesDesc() {
		return $this->SalesAndPurchase_SalesDesc;
	}

	
// ------------------------------ End Field: SalesAndPurchase_SalesDesc --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_SalesPrice -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_SalesPrice` variable
	* @access public
	*/

	public function setSalesandpurchaseSalesprice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_SalesPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_SalesPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_SalesPrice` variable
	* @access public
	*/

	public function getSalesandpurchaseSalesprice() {
		return $this->SalesAndPurchase_SalesPrice;
	}

	public function get_SalesAndPurchase_SalesPrice() {
		return $this->SalesAndPurchase_SalesPrice;
	}

	
// ------------------------------ End Field: SalesAndPurchase_SalesPrice --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_IncomeAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_IncomeAccount_ListID` variable
	* @access public
	*/

	public function setSalesandpurchaseIncomeaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_IncomeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_IncomeAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_IncomeAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_IncomeAccount_ListID` variable
	* @access public
	*/

	public function getSalesandpurchaseIncomeaccountListid() {
		return $this->SalesAndPurchase_IncomeAccount_ListID;
	}

	public function get_SalesAndPurchase_IncomeAccount_ListID() {
		return $this->SalesAndPurchase_IncomeAccount_ListID;
	}

	
// ------------------------------ End Field: SalesAndPurchase_IncomeAccount_ListID --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_IncomeAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_IncomeAccount_FullName` variable
	* @access public
	*/

	public function setSalesandpurchaseIncomeaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_IncomeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_IncomeAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_IncomeAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_IncomeAccount_FullName` variable
	* @access public
	*/

	public function getSalesandpurchaseIncomeaccountFullname() {
		return $this->SalesAndPurchase_IncomeAccount_FullName;
	}

	public function get_SalesAndPurchase_IncomeAccount_FullName() {
		return $this->SalesAndPurchase_IncomeAccount_FullName;
	}

	
// ------------------------------ End Field: SalesAndPurchase_IncomeAccount_FullName --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_PurchaseDesc -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_PurchaseDesc` variable
	* @access public
	*/

	public function setSalesandpurchasePurchasedesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_PurchaseDesc($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PurchaseDesc', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_PurchaseDesc` variable
	* @access public
	*/

	public function getSalesandpurchasePurchasedesc() {
		return $this->SalesAndPurchase_PurchaseDesc;
	}

	public function get_SalesAndPurchase_PurchaseDesc() {
		return $this->SalesAndPurchase_PurchaseDesc;
	}

	
// ------------------------------ End Field: SalesAndPurchase_PurchaseDesc --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_PurchaseCost -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_PurchaseCost` variable
	* @access public
	*/

	public function setSalesandpurchasePurchasecost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_PurchaseCost($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PurchaseCost', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_PurchaseCost` variable
	* @access public
	*/

	public function getSalesandpurchasePurchasecost() {
		return $this->SalesAndPurchase_PurchaseCost;
	}

	public function get_SalesAndPurchase_PurchaseCost() {
		return $this->SalesAndPurchase_PurchaseCost;
	}

	
// ------------------------------ End Field: SalesAndPurchase_PurchaseCost --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_ExpenseAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_ExpenseAccount_ListID` variable
	* @access public
	*/

	public function setSalesandpurchaseExpenseaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_ExpenseAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_ExpenseAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_ExpenseAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_ExpenseAccount_ListID` variable
	* @access public
	*/

	public function getSalesandpurchaseExpenseaccountListid() {
		return $this->SalesAndPurchase_ExpenseAccount_ListID;
	}

	public function get_SalesAndPurchase_ExpenseAccount_ListID() {
		return $this->SalesAndPurchase_ExpenseAccount_ListID;
	}

	
// ------------------------------ End Field: SalesAndPurchase_ExpenseAccount_ListID --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_ExpenseAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_ExpenseAccount_FullName` variable
	* @access public
	*/

	public function setSalesandpurchaseExpenseaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_ExpenseAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_ExpenseAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_ExpenseAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_ExpenseAccount_FullName` variable
	* @access public
	*/

	public function getSalesandpurchaseExpenseaccountFullname() {
		return $this->SalesAndPurchase_ExpenseAccount_FullName;
	}

	public function get_SalesAndPurchase_ExpenseAccount_FullName() {
		return $this->SalesAndPurchase_ExpenseAccount_FullName;
	}

	
// ------------------------------ End Field: SalesAndPurchase_ExpenseAccount_FullName --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_PrefVendor_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_PrefVendor_ListID` variable
	* @access public
	*/

	public function setSalesandpurchasePrefvendorListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PrefVendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_PrefVendor_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PrefVendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_PrefVendor_ListID` variable
	* @access public
	*/

	public function getSalesandpurchasePrefvendorListid() {
		return $this->SalesAndPurchase_PrefVendor_ListID;
	}

	public function get_SalesAndPurchase_PrefVendor_ListID() {
		return $this->SalesAndPurchase_PrefVendor_ListID;
	}

	
// ------------------------------ End Field: SalesAndPurchase_PrefVendor_ListID --------------------------------------


// ---------------------------- Start Field: SalesAndPurchase_PrefVendor_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesAndPurchase_PrefVendor_FullName` variable
	* @access public
	*/

	public function setSalesandpurchasePrefvendorFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PrefVendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesAndPurchase_PrefVendor_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesAndPurchase_PrefVendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesAndPurchase_PrefVendor_FullName` variable
	* @access public
	*/

	public function getSalesandpurchasePrefvendorFullname() {
		return $this->SalesAndPurchase_PrefVendor_FullName;
	}

	public function get_SalesAndPurchase_PrefVendor_FullName() {
		return $this->SalesAndPurchase_PrefVendor_FullName;
	}

	
// ------------------------------ End Field: SalesAndPurchase_PrefVendor_FullName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FullName' => (object) array(
										'Field'=>'FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'Parent_ListID' => (object) array(
										'Field'=>'Parent_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Parent_FullName' => (object) array(
										'Field'=>'Parent_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Sublevel' => (object) array(
										'Field'=>'Sublevel',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'UnitOfMeasureSet_ListID' => (object) array(
										'Field'=>'UnitOfMeasureSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'UnitOfMeasureSet_FullName' => (object) array(
										'Field'=>'UnitOfMeasureSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_ListID' => (object) array(
										'Field'=>'SalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxCode_FullName' => (object) array(
										'Field'=>'SalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesOrPurchase_Desc' => (object) array(
										'Field'=>'SalesOrPurchase_Desc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesOrPurchase_Price' => (object) array(
										'Field'=>'SalesOrPurchase_Price',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesOrPurchase_PricePercent' => (object) array(
										'Field'=>'SalesOrPurchase_PricePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesOrPurchase_Account_ListID' => (object) array(
										'Field'=>'SalesOrPurchase_Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesOrPurchase_Account_FullName' => (object) array(
										'Field'=>'SalesOrPurchase_Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_SalesDesc' => (object) array(
										'Field'=>'SalesAndPurchase_SalesDesc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_SalesPrice' => (object) array(
										'Field'=>'SalesAndPurchase_SalesPrice',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_IncomeAccount_ListID' => (object) array(
										'Field'=>'SalesAndPurchase_IncomeAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_IncomeAccount_FullName' => (object) array(
										'Field'=>'SalesAndPurchase_IncomeAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_PurchaseDesc' => (object) array(
										'Field'=>'SalesAndPurchase_PurchaseDesc',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_PurchaseCost' => (object) array(
										'Field'=>'SalesAndPurchase_PurchaseCost',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_ExpenseAccount_ListID' => (object) array(
										'Field'=>'SalesAndPurchase_ExpenseAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_ExpenseAccount_FullName' => (object) array(
										'Field'=>'SalesAndPurchase_ExpenseAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_PrefVendor_ListID' => (object) array(
										'Field'=>'SalesAndPurchase_PrefVendor_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesAndPurchase_PrefVendor_FullName' => (object) array(
										'Field'=>'SalesAndPurchase_PrefVendor_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_itemservice` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_itemservice` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_itemservice` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_itemservice` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_itemservice` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_itemservice` ADD  `Name` varchar(31) NULL   ;",
			'FullName' => "ALTER TABLE  `qb_itemservice` ADD  `FullName` varchar(255) NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_itemservice` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'Parent_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `Parent_ListID` varchar(40) NULL   ;",
			'Parent_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `Parent_FullName` varchar(255) NULL   ;",
			'Sublevel' => "ALTER TABLE  `qb_itemservice` ADD  `Sublevel` int(10) unsigned NULL   DEFAULT '0';",
			'UnitOfMeasureSet_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `UnitOfMeasureSet_ListID` varchar(40) NULL   ;",
			'UnitOfMeasureSet_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `UnitOfMeasureSet_FullName` varchar(255) NULL   ;",
			'SalesTaxCode_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `SalesTaxCode_ListID` varchar(40) NULL   ;",
			'SalesTaxCode_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `SalesTaxCode_FullName` varchar(255) NULL   ;",
			'SalesOrPurchase_Desc' => "ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Desc` text NULL   ;",
			'SalesOrPurchase_Price' => "ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Price` decimal(13,5) NULL   ;",
			'SalesOrPurchase_PricePercent' => "ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_PricePercent` decimal(12,5) NULL   ;",
			'SalesOrPurchase_Account_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Account_ListID` varchar(40) NULL   ;",
			'SalesOrPurchase_Account_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `SalesOrPurchase_Account_FullName` varchar(255) NULL   ;",
			'SalesAndPurchase_SalesDesc' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_SalesDesc` text NULL   ;",
			'SalesAndPurchase_SalesPrice' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_SalesPrice` decimal(13,5) NULL   ;",
			'SalesAndPurchase_IncomeAccount_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_IncomeAccount_ListID` varchar(40) NULL   ;",
			'SalesAndPurchase_IncomeAccount_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_IncomeAccount_FullName` varchar(255) NULL   ;",
			'SalesAndPurchase_PurchaseDesc' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PurchaseDesc` text NULL   ;",
			'SalesAndPurchase_PurchaseCost' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PurchaseCost` decimal(13,5) NULL   ;",
			'SalesAndPurchase_ExpenseAccount_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_ExpenseAccount_ListID` varchar(40) NULL   ;",
			'SalesAndPurchase_ExpenseAccount_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_ExpenseAccount_FullName` varchar(255) NULL   ;",
			'SalesAndPurchase_PrefVendor_ListID' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PrefVendor_ListID` varchar(40) NULL   ;",
			'SalesAndPurchase_PrefVendor_FullName' => "ALTER TABLE  `qb_itemservice` ADD  `SalesAndPurchase_PrefVendor_FullName` varchar(255) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setFullname() - FullName
//setIsactive() - IsActive
//setParentListid() - Parent_ListID
//setParentFullname() - Parent_FullName
//setSublevel() - Sublevel
//setUnitofmeasuresetListid() - UnitOfMeasureSet_ListID
//setUnitofmeasuresetFullname() - UnitOfMeasureSet_FullName
//setSalestaxcodeListid() - SalesTaxCode_ListID
//setSalestaxcodeFullname() - SalesTaxCode_FullName
//setSalesorpurchaseDesc() - SalesOrPurchase_Desc
//setSalesorpurchasePrice() - SalesOrPurchase_Price
//setSalesorpurchasePricepercent() - SalesOrPurchase_PricePercent
//setSalesorpurchaseAccountListid() - SalesOrPurchase_Account_ListID
//setSalesorpurchaseAccountFullname() - SalesOrPurchase_Account_FullName
//setSalesandpurchaseSalesdesc() - SalesAndPurchase_SalesDesc
//setSalesandpurchaseSalesprice() - SalesAndPurchase_SalesPrice
//setSalesandpurchaseIncomeaccountListid() - SalesAndPurchase_IncomeAccount_ListID
//setSalesandpurchaseIncomeaccountFullname() - SalesAndPurchase_IncomeAccount_FullName
//setSalesandpurchasePurchasedesc() - SalesAndPurchase_PurchaseDesc
//setSalesandpurchasePurchasecost() - SalesAndPurchase_PurchaseCost
//setSalesandpurchaseExpenseaccountListid() - SalesAndPurchase_ExpenseAccount_ListID
//setSalesandpurchaseExpenseaccountFullname() - SalesAndPurchase_ExpenseAccount_FullName
//setSalesandpurchasePrefvendorListid() - SalesAndPurchase_PrefVendor_ListID
//setSalesandpurchasePrefvendorFullname() - SalesAndPurchase_PrefVendor_FullName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_FullName() - FullName
//set_IsActive() - IsActive
//set_Parent_ListID() - Parent_ListID
//set_Parent_FullName() - Parent_FullName
//set_Sublevel() - Sublevel
//set_UnitOfMeasureSet_ListID() - UnitOfMeasureSet_ListID
//set_UnitOfMeasureSet_FullName() - UnitOfMeasureSet_FullName
//set_SalesTaxCode_ListID() - SalesTaxCode_ListID
//set_SalesTaxCode_FullName() - SalesTaxCode_FullName
//set_SalesOrPurchase_Desc() - SalesOrPurchase_Desc
//set_SalesOrPurchase_Price() - SalesOrPurchase_Price
//set_SalesOrPurchase_PricePercent() - SalesOrPurchase_PricePercent
//set_SalesOrPurchase_Account_ListID() - SalesOrPurchase_Account_ListID
//set_SalesOrPurchase_Account_FullName() - SalesOrPurchase_Account_FullName
//set_SalesAndPurchase_SalesDesc() - SalesAndPurchase_SalesDesc
//set_SalesAndPurchase_SalesPrice() - SalesAndPurchase_SalesPrice
//set_SalesAndPurchase_IncomeAccount_ListID() - SalesAndPurchase_IncomeAccount_ListID
//set_SalesAndPurchase_IncomeAccount_FullName() - SalesAndPurchase_IncomeAccount_FullName
//set_SalesAndPurchase_PurchaseDesc() - SalesAndPurchase_PurchaseDesc
//set_SalesAndPurchase_PurchaseCost() - SalesAndPurchase_PurchaseCost
//set_SalesAndPurchase_ExpenseAccount_ListID() - SalesAndPurchase_ExpenseAccount_ListID
//set_SalesAndPurchase_ExpenseAccount_FullName() - SalesAndPurchase_ExpenseAccount_FullName
//set_SalesAndPurchase_PrefVendor_ListID() - SalesAndPurchase_PrefVendor_ListID
//set_SalesAndPurchase_PrefVendor_FullName() - SalesAndPurchase_PrefVendor_FullName

*/
/* End of file Qb_itemservice_model.php */
/* Location: ./application/models/Qb_itemservice_model.php */
