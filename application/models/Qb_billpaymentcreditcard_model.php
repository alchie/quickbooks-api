<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_billpaymentcreditcard_model Class
 *
 * Manipulates `qb_billpaymentcreditcard` table on database

CREATE TABLE `qb_billpaymentcreditcard` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `PayeeEntity_ListID` varchar(40) DEFAULT NULL,
  `PayeeEntity_FullName` varchar(255) DEFAULT NULL,
  `APAccount_ListID` varchar(40) DEFAULT NULL,
  `APAccount_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `CreditCardAccount_ListID` varchar(40) DEFAULT NULL,
  `CreditCardAccount_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `Memo` text,
  PRIMARY KEY (`qbxml_id`),
  KEY `PayeeEntity_ListID` (`PayeeEntity_ListID`),
  KEY `APAccount_ListID` (`APAccount_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `CreditCardAccount_ListID` (`CreditCardAccount_ListID`),
  KEY `RefNumber` (`RefNumber`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_billpaymentcreditcard` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `PayeeEntity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `PayeeEntity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `APAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `APAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `CreditCardAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `CreditCardAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_billpaymentcreditcard` ADD  `Memo` text NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_billpaymentcreditcard_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $PayeeEntity_ListID;
	protected $PayeeEntity_FullName;
	protected $APAccount_ListID;
	protected $APAccount_FullName;
	protected $TxnDate;
	protected $CreditCardAccount_ListID;
	protected $CreditCardAccount_FullName;
	protected $Amount;
	protected $RefNumber;
	protected $Memo;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_billpaymentcreditcard';
		$this->_short_name = 'qb_billpaymentcreditcard';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","PayeeEntity_ListID","PayeeEntity_FullName","APAccount_ListID","APAccount_FullName","TxnDate","CreditCardAccount_ListID","CreditCardAccount_FullName","Amount","RefNumber","Memo");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: PayeeEntity_ListID -------------------------------------- 

	/** 
	* Sets a value to `PayeeEntity_ListID` variable
	* @access public
	*/

	public function setPayeeentityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayeeEntity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayeeEntity_ListID` variable
	* @access public
	*/

	public function getPayeeentityListid() {
		return $this->PayeeEntity_ListID;
	}

	public function get_PayeeEntity_ListID() {
		return $this->PayeeEntity_ListID;
	}

	
// ------------------------------ End Field: PayeeEntity_ListID --------------------------------------


// ---------------------------- Start Field: PayeeEntity_FullName -------------------------------------- 

	/** 
	* Sets a value to `PayeeEntity_FullName` variable
	* @access public
	*/

	public function setPayeeentityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PayeeEntity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PayeeEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PayeeEntity_FullName` variable
	* @access public
	*/

	public function getPayeeentityFullname() {
		return $this->PayeeEntity_FullName;
	}

	public function get_PayeeEntity_FullName() {
		return $this->PayeeEntity_FullName;
	}

	
// ------------------------------ End Field: PayeeEntity_FullName --------------------------------------


// ---------------------------- Start Field: APAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `APAccount_ListID` variable
	* @access public
	*/

	public function setApaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_ListID` variable
	* @access public
	*/

	public function getApaccountListid() {
		return $this->APAccount_ListID;
	}

	public function get_APAccount_ListID() {
		return $this->APAccount_ListID;
	}

	
// ------------------------------ End Field: APAccount_ListID --------------------------------------


// ---------------------------- Start Field: APAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `APAccount_FullName` variable
	* @access public
	*/

	public function setApaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_APAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('APAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `APAccount_FullName` variable
	* @access public
	*/

	public function getApaccountFullname() {
		return $this->APAccount_FullName;
	}

	public function get_APAccount_FullName() {
		return $this->APAccount_FullName;
	}

	
// ------------------------------ End Field: APAccount_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: CreditCardAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `CreditCardAccount_ListID` variable
	* @access public
	*/

	public function setCreditcardaccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardAccount_ListID` variable
	* @access public
	*/

	public function getCreditcardaccountListid() {
		return $this->CreditCardAccount_ListID;
	}

	public function get_CreditCardAccount_ListID() {
		return $this->CreditCardAccount_ListID;
	}

	
// ------------------------------ End Field: CreditCardAccount_ListID --------------------------------------


// ---------------------------- Start Field: CreditCardAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `CreditCardAccount_FullName` variable
	* @access public
	*/

	public function setCreditcardaccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CreditCardAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CreditCardAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CreditCardAccount_FullName` variable
	* @access public
	*/

	public function getCreditcardaccountFullname() {
		return $this->CreditCardAccount_FullName;
	}

	public function get_CreditCardAccount_FullName() {
		return $this->CreditCardAccount_FullName;
	}

	
// ------------------------------ End Field: CreditCardAccount_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'PayeeEntity_ListID' => (object) array(
										'Field'=>'PayeeEntity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'PayeeEntity_FullName' => (object) array(
										'Field'=>'PayeeEntity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_ListID' => (object) array(
										'Field'=>'APAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'APAccount_FullName' => (object) array(
										'Field'=>'APAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardAccount_ListID' => (object) array(
										'Field'=>'CreditCardAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CreditCardAccount_FullName' => (object) array(
										'Field'=>'CreditCardAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'PayeeEntity_ListID' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `PayeeEntity_ListID` varchar(40) NULL   ;",
			'PayeeEntity_FullName' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `PayeeEntity_FullName` varchar(255) NULL   ;",
			'APAccount_ListID' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `APAccount_ListID` varchar(40) NULL   ;",
			'APAccount_FullName' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `APAccount_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `TxnDate` date NULL   ;",
			'CreditCardAccount_ListID' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `CreditCardAccount_ListID` varchar(40) NULL   ;",
			'CreditCardAccount_FullName' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `CreditCardAccount_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `Amount` decimal(10,2) NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `RefNumber` varchar(11) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_billpaymentcreditcard` ADD  `Memo` text NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setPayeeentityListid() - PayeeEntity_ListID
//setPayeeentityFullname() - PayeeEntity_FullName
//setApaccountListid() - APAccount_ListID
//setApaccountFullname() - APAccount_FullName
//setTxndate() - TxnDate
//setCreditcardaccountListid() - CreditCardAccount_ListID
//setCreditcardaccountFullname() - CreditCardAccount_FullName
//setAmount() - Amount
//setRefnumber() - RefNumber
//setMemo() - Memo

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_PayeeEntity_ListID() - PayeeEntity_ListID
//set_PayeeEntity_FullName() - PayeeEntity_FullName
//set_APAccount_ListID() - APAccount_ListID
//set_APAccount_FullName() - APAccount_FullName
//set_TxnDate() - TxnDate
//set_CreditCardAccount_ListID() - CreditCardAccount_ListID
//set_CreditCardAccount_FullName() - CreditCardAccount_FullName
//set_Amount() - Amount
//set_RefNumber() - RefNumber
//set_Memo() - Memo

*/
/* End of file Qb_billpaymentcreditcard_model.php */
/* Location: ./application/models/Qb_billpaymentcreditcard_model.php */
