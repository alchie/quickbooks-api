<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_check_expenseline_model Class
 *
 * Manipulates `qb_check_expenseline` table on database

CREATE TABLE `qb_check_expenseline` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Check_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `Account_ListID` varchar(40) DEFAULT NULL,
  `Account_FullName` varchar(255) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `CurrencyRef` text,
  `ExchangeRate` text,
  `AmountInHomeCurrency` decimal(10,2) DEFAULT NULL,
  `Memo` text,
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `BillableStatus` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Check_TxnID` (`Check_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `Account_ListID` (`Account_ListID`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `Class_ListID` (`Class_ListID`)
) ENGINE=MyISAM AUTO_INCREMENT=749 DEFAULT CHARSET=latin;

ALTER TABLE  `qb_check_expenseline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_check_expenseline` ADD  `Check_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_check_expenseline` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Account_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Account_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `CurrencyRef` text NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `AmountInHomeCurrency` decimal(10,2) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_check_expenseline` ADD  `BillableStatus` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_check_expenseline_model extends MY_Model {

	protected $qbxml_id;
	protected $Check_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $Account_ListID;
	protected $Account_FullName;
	protected $Amount;
	protected $CurrencyRef;
	protected $ExchangeRate;
	protected $AmountInHomeCurrency;
	protected $Memo;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $BillableStatus;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_check_expenseline';
		$this->_short_name = 'qb_check_expenseline';
		$this->_fields = array("qbxml_id","Check_TxnID","SortOrder","TxnLineID","Account_ListID","Account_FullName","Amount","CurrencyRef","ExchangeRate","AmountInHomeCurrency","Memo","Customer_ListID","Customer_FullName","Class_ListID","Class_FullName","BillableStatus");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: Check_TxnID -------------------------------------- 

	/** 
	* Sets a value to `Check_TxnID` variable
	* @access public
	*/

	public function setCheckTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Check_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Check_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Check_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Check_TxnID` variable
	* @access public
	*/

	public function getCheckTxnid() {
		return $this->Check_TxnID;
	}

	public function get_Check_TxnID() {
		return $this->Check_TxnID;
	}

	
// ------------------------------ End Field: Check_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: Account_ListID -------------------------------------- 

	/** 
	* Sets a value to `Account_ListID` variable
	* @access public
	*/

	public function setAccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_ListID` variable
	* @access public
	*/

	public function getAccountListid() {
		return $this->Account_ListID;
	}

	public function get_Account_ListID() {
		return $this->Account_ListID;
	}

	
// ------------------------------ End Field: Account_ListID --------------------------------------


// ---------------------------- Start Field: Account_FullName -------------------------------------- 

	/** 
	* Sets a value to `Account_FullName` variable
	* @access public
	*/

	public function setAccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Account_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Account_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Account_FullName` variable
	* @access public
	*/

	public function getAccountFullname() {
		return $this->Account_FullName;
	}

	public function get_Account_FullName() {
		return $this->Account_FullName;
	}

	
// ------------------------------ End Field: Account_FullName --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: CurrencyRef -------------------------------------- 

	/** 
	* Sets a value to `CurrencyRef` variable
	* @access public
	*/

	public function setCurrencyref($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrencyRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CurrencyRef($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrencyRef', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CurrencyRef` variable
	* @access public
	*/

	public function getCurrencyref() {
		return $this->CurrencyRef;
	}

	public function get_CurrencyRef() {
		return $this->CurrencyRef;
	}

	
// ------------------------------ End Field: CurrencyRef --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: AmountInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `AmountInHomeCurrency` variable
	* @access public
	*/

	public function setAmountinhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AmountInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AmountInHomeCurrency` variable
	* @access public
	*/

	public function getAmountinhomecurrency() {
		return $this->AmountInHomeCurrency;
	}

	public function get_AmountInHomeCurrency() {
		return $this->AmountInHomeCurrency;
	}

	
// ------------------------------ End Field: AmountInHomeCurrency --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: BillableStatus -------------------------------------- 

	/** 
	* Sets a value to `BillableStatus` variable
	* @access public
	*/

	public function setBillablestatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillableStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillableStatus', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillableStatus` variable
	* @access public
	*/

	public function getBillablestatus() {
		return $this->BillableStatus;
	}

	public function get_BillableStatus() {
		return $this->BillableStatus;
	}

	
// ------------------------------ End Field: BillableStatus --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'Check_TxnID' => (object) array(
										'Field'=>'Check_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_ListID' => (object) array(
										'Field'=>'Account_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Account_FullName' => (object) array(
										'Field'=>'Account_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CurrencyRef' => (object) array(
										'Field'=>'CurrencyRef',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AmountInHomeCurrency' => (object) array(
										'Field'=>'AmountInHomeCurrency',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillableStatus' => (object) array(
										'Field'=>'BillableStatus',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_check_expenseline` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'Check_TxnID' => "ALTER TABLE  `qb_check_expenseline` ADD  `Check_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_check_expenseline` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_check_expenseline` ADD  `TxnLineID` varchar(40) NULL   ;",
			'Account_ListID' => "ALTER TABLE  `qb_check_expenseline` ADD  `Account_ListID` varchar(40) NULL   ;",
			'Account_FullName' => "ALTER TABLE  `qb_check_expenseline` ADD  `Account_FullName` varchar(255) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_check_expenseline` ADD  `Amount` decimal(10,2) NULL   ;",
			'CurrencyRef' => "ALTER TABLE  `qb_check_expenseline` ADD  `CurrencyRef` text NULL   ;",
			'ExchangeRate' => "ALTER TABLE  `qb_check_expenseline` ADD  `ExchangeRate` text NULL   ;",
			'AmountInHomeCurrency' => "ALTER TABLE  `qb_check_expenseline` ADD  `AmountInHomeCurrency` decimal(10,2) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_check_expenseline` ADD  `Memo` text NULL   ;",
			'Customer_ListID' => "ALTER TABLE  `qb_check_expenseline` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_check_expenseline` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_check_expenseline` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_check_expenseline` ADD  `Class_FullName` varchar(255) NULL   ;",
			'BillableStatus' => "ALTER TABLE  `qb_check_expenseline` ADD  `BillableStatus` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setCheckTxnid() - Check_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setAccountListid() - Account_ListID
//setAccountFullname() - Account_FullName
//setAmount() - Amount
//setCurrencyref() - CurrencyRef
//setExchangerate() - ExchangeRate
//setAmountinhomecurrency() - AmountInHomeCurrency
//setMemo() - Memo
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setBillablestatus() - BillableStatus

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_Check_TxnID() - Check_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_Account_ListID() - Account_ListID
//set_Account_FullName() - Account_FullName
//set_Amount() - Amount
//set_CurrencyRef() - CurrencyRef
//set_ExchangeRate() - ExchangeRate
//set_AmountInHomeCurrency() - AmountInHomeCurrency
//set_Memo() - Memo
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_BillableStatus() - BillableStatus

*/
/* End of file Qb_check_expenseline_model.php */
/* Location: ./application/models/Qb_check_expenseline_model.php */
