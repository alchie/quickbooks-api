<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_company_model Class
 *
 * Manipulates `qb_company` table on database

CREATE TABLE `qb_company` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsSampleCompany` tinyint(1) DEFAULT '0',
  `CompanyName` text,
  `LegalCompanyName` text,
  `Address_Addr1` text,
  `Address_Addr2` text,
  `Address_Addr3` text,
  `Address_Addr4` text,
  `Address_Addr5` text,
  `Address_City` text,
  `Address_State` text,
  `Address_PostalCode` text,
  `Address_Country` text,
  `Address_Note` text,
  `AddressBlock_Addr1` text,
  `AddressBlock_Addr2` text,
  `AddressBlock_Addr3` text,
  `AddressBlock_Addr4` text,
  `AddressBlock_Addr5` text,
  `LegalAddress_Addr1` text,
  `LegalAddress_Addr2` text,
  `LegalAddress_Addr3` text,
  `LegalAddress_Addr4` text,
  `LegalAddress_Addr5` text,
  `LegalAddress_City` text,
  `LegalAddress_State` text,
  `LegalAddress_PostalCode` text,
  `LegalAddress_Country` text,
  `LegalAddress_Note` text,
  `Company_CompanyAddressForCustomer_Addr1` text,
  `Company_CompanyAddressForCustomer_Addr2` text,
  `Company_CompanyAddressForCustomer_Addr3` text,
  `Company_CompanyAddressForCustomer_Addr4` text,
  `Company_CompanyAddressForCustomer_Addr5` text,
  `Company_CompanyAddressForCustomer_City` text,
  `Company_CompanyAddressForCustomer_State` text,
  `Company_CompanyAddressForCustomer_PostalCode` text,
  `Company_CompanyAddressForCustomer_Country` text,
  `Company_CompanyAddressForCustomer_Note` text,
  `Company_CompanyAddressBlockForCustomer_Addr1` text,
  `Company_CompanyAddressBlockForCustomer_Addr2` text,
  `Company_CompanyAddressBlockForCustomer_Addr3` text,
  `Company_CompanyAddressBlockForCustomer_Addr4` text,
  `Company_CompanyAddressBlockForCustomer_Addr5` text,
  `Phone` text,
  `Fax` text,
  `Email` text,
  `CompanyWebSite` text,
  `FirstMonthFiscalYear` varchar(40) DEFAULT NULL,
  `FirstMonthIncomeTaxYear` varchar(40) DEFAULT NULL,
  `CompanyType` text,
  `EIN` text,
  `SSN` text,
  `TaxForm` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_company` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_company` ADD  `IsSampleCompany` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_company` ADD  `CompanyName` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalCompanyName` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Addr1` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Addr2` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Addr3` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Addr4` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Addr5` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_City` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_State` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_PostalCode` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Country` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Address_Note` text NULL   ;
ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr1` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr2` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr3` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr4` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr5` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_City` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_State` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_PostalCode` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Country` text NULL   ;
ALTER TABLE  `qb_company` ADD  `LegalAddress_Note` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr1` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr2` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr3` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr4` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr5` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_City` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_State` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_PostalCode` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Country` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Note` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr1` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr2` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr3` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr4` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr5` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Phone` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Fax` text NULL   ;
ALTER TABLE  `qb_company` ADD  `Email` text NULL   ;
ALTER TABLE  `qb_company` ADD  `CompanyWebSite` text NULL   ;
ALTER TABLE  `qb_company` ADD  `FirstMonthFiscalYear` varchar(40) NULL   ;
ALTER TABLE  `qb_company` ADD  `FirstMonthIncomeTaxYear` varchar(40) NULL   ;
ALTER TABLE  `qb_company` ADD  `CompanyType` text NULL   ;
ALTER TABLE  `qb_company` ADD  `EIN` text NULL   ;
ALTER TABLE  `qb_company` ADD  `SSN` text NULL   ;
ALTER TABLE  `qb_company` ADD  `TaxForm` varchar(40) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_company_model extends MY_Model {

	protected $qbxml_id;
	protected $IsSampleCompany;
	protected $CompanyName;
	protected $LegalCompanyName;
	protected $Address_Addr1;
	protected $Address_Addr2;
	protected $Address_Addr3;
	protected $Address_Addr4;
	protected $Address_Addr5;
	protected $Address_City;
	protected $Address_State;
	protected $Address_PostalCode;
	protected $Address_Country;
	protected $Address_Note;
	protected $AddressBlock_Addr1;
	protected $AddressBlock_Addr2;
	protected $AddressBlock_Addr3;
	protected $AddressBlock_Addr4;
	protected $AddressBlock_Addr5;
	protected $LegalAddress_Addr1;
	protected $LegalAddress_Addr2;
	protected $LegalAddress_Addr3;
	protected $LegalAddress_Addr4;
	protected $LegalAddress_Addr5;
	protected $LegalAddress_City;
	protected $LegalAddress_State;
	protected $LegalAddress_PostalCode;
	protected $LegalAddress_Country;
	protected $LegalAddress_Note;
	protected $Company_CompanyAddressForCustomer_Addr1;
	protected $Company_CompanyAddressForCustomer_Addr2;
	protected $Company_CompanyAddressForCustomer_Addr3;
	protected $Company_CompanyAddressForCustomer_Addr4;
	protected $Company_CompanyAddressForCustomer_Addr5;
	protected $Company_CompanyAddressForCustomer_City;
	protected $Company_CompanyAddressForCustomer_State;
	protected $Company_CompanyAddressForCustomer_PostalCode;
	protected $Company_CompanyAddressForCustomer_Country;
	protected $Company_CompanyAddressForCustomer_Note;
	protected $Company_CompanyAddressBlockForCustomer_Addr1;
	protected $Company_CompanyAddressBlockForCustomer_Addr2;
	protected $Company_CompanyAddressBlockForCustomer_Addr3;
	protected $Company_CompanyAddressBlockForCustomer_Addr4;
	protected $Company_CompanyAddressBlockForCustomer_Addr5;
	protected $Phone;
	protected $Fax;
	protected $Email;
	protected $CompanyWebSite;
	protected $FirstMonthFiscalYear;
	protected $FirstMonthIncomeTaxYear;
	protected $CompanyType;
	protected $EIN;
	protected $SSN;
	protected $TaxForm;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_company';
		$this->_short_name = 'qb_company';
		$this->_fields = array("qbxml_id","IsSampleCompany","CompanyName","LegalCompanyName","Address_Addr1","Address_Addr2","Address_Addr3","Address_Addr4","Address_Addr5","Address_City","Address_State","Address_PostalCode","Address_Country","Address_Note","AddressBlock_Addr1","AddressBlock_Addr2","AddressBlock_Addr3","AddressBlock_Addr4","AddressBlock_Addr5","LegalAddress_Addr1","LegalAddress_Addr2","LegalAddress_Addr3","LegalAddress_Addr4","LegalAddress_Addr5","LegalAddress_City","LegalAddress_State","LegalAddress_PostalCode","LegalAddress_Country","LegalAddress_Note","Company_CompanyAddressForCustomer_Addr1","Company_CompanyAddressForCustomer_Addr2","Company_CompanyAddressForCustomer_Addr3","Company_CompanyAddressForCustomer_Addr4","Company_CompanyAddressForCustomer_Addr5","Company_CompanyAddressForCustomer_City","Company_CompanyAddressForCustomer_State","Company_CompanyAddressForCustomer_PostalCode","Company_CompanyAddressForCustomer_Country","Company_CompanyAddressForCustomer_Note","Company_CompanyAddressBlockForCustomer_Addr1","Company_CompanyAddressBlockForCustomer_Addr2","Company_CompanyAddressBlockForCustomer_Addr3","Company_CompanyAddressBlockForCustomer_Addr4","Company_CompanyAddressBlockForCustomer_Addr5","Phone","Fax","Email","CompanyWebSite","FirstMonthFiscalYear","FirstMonthIncomeTaxYear","CompanyType","EIN","SSN","TaxForm");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: IsSampleCompany -------------------------------------- 

	/** 
	* Sets a value to `IsSampleCompany` variable
	* @access public
	*/

	public function setIssamplecompany($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsSampleCompany', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsSampleCompany($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsSampleCompany', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsSampleCompany` variable
	* @access public
	*/

	public function getIssamplecompany() {
		return $this->IsSampleCompany;
	}

	public function get_IsSampleCompany() {
		return $this->IsSampleCompany;
	}

	
// ------------------------------ End Field: IsSampleCompany --------------------------------------


// ---------------------------- Start Field: CompanyName -------------------------------------- 

	/** 
	* Sets a value to `CompanyName` variable
	* @access public
	*/

	public function setCompanyname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CompanyName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CompanyName` variable
	* @access public
	*/

	public function getCompanyname() {
		return $this->CompanyName;
	}

	public function get_CompanyName() {
		return $this->CompanyName;
	}

	
// ------------------------------ End Field: CompanyName --------------------------------------


// ---------------------------- Start Field: LegalCompanyName -------------------------------------- 

	/** 
	* Sets a value to `LegalCompanyName` variable
	* @access public
	*/

	public function setLegalcompanyname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalCompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalCompanyName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalCompanyName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalCompanyName` variable
	* @access public
	*/

	public function getLegalcompanyname() {
		return $this->LegalCompanyName;
	}

	public function get_LegalCompanyName() {
		return $this->LegalCompanyName;
	}

	
// ------------------------------ End Field: LegalCompanyName --------------------------------------


// ---------------------------- Start Field: Address_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr1` variable
	* @access public
	*/

	public function setAddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr1` variable
	* @access public
	*/

	public function getAddressAddr1() {
		return $this->Address_Addr1;
	}

	public function get_Address_Addr1() {
		return $this->Address_Addr1;
	}

	
// ------------------------------ End Field: Address_Addr1 --------------------------------------


// ---------------------------- Start Field: Address_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr2` variable
	* @access public
	*/

	public function setAddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr2` variable
	* @access public
	*/

	public function getAddressAddr2() {
		return $this->Address_Addr2;
	}

	public function get_Address_Addr2() {
		return $this->Address_Addr2;
	}

	
// ------------------------------ End Field: Address_Addr2 --------------------------------------


// ---------------------------- Start Field: Address_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr3` variable
	* @access public
	*/

	public function setAddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr3` variable
	* @access public
	*/

	public function getAddressAddr3() {
		return $this->Address_Addr3;
	}

	public function get_Address_Addr3() {
		return $this->Address_Addr3;
	}

	
// ------------------------------ End Field: Address_Addr3 --------------------------------------


// ---------------------------- Start Field: Address_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr4` variable
	* @access public
	*/

	public function setAddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr4` variable
	* @access public
	*/

	public function getAddressAddr4() {
		return $this->Address_Addr4;
	}

	public function get_Address_Addr4() {
		return $this->Address_Addr4;
	}

	
// ------------------------------ End Field: Address_Addr4 --------------------------------------


// ---------------------------- Start Field: Address_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `Address_Addr5` variable
	* @access public
	*/

	public function setAddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Addr5` variable
	* @access public
	*/

	public function getAddressAddr5() {
		return $this->Address_Addr5;
	}

	public function get_Address_Addr5() {
		return $this->Address_Addr5;
	}

	
// ------------------------------ End Field: Address_Addr5 --------------------------------------


// ---------------------------- Start Field: Address_City -------------------------------------- 

	/** 
	* Sets a value to `Address_City` variable
	* @access public
	*/

	public function setAddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_City` variable
	* @access public
	*/

	public function getAddressCity() {
		return $this->Address_City;
	}

	public function get_Address_City() {
		return $this->Address_City;
	}

	
// ------------------------------ End Field: Address_City --------------------------------------


// ---------------------------- Start Field: Address_State -------------------------------------- 

	/** 
	* Sets a value to `Address_State` variable
	* @access public
	*/

	public function setAddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_State` variable
	* @access public
	*/

	public function getAddressState() {
		return $this->Address_State;
	}

	public function get_Address_State() {
		return $this->Address_State;
	}

	
// ------------------------------ End Field: Address_State --------------------------------------


// ---------------------------- Start Field: Address_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `Address_PostalCode` variable
	* @access public
	*/

	public function setAddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_PostalCode` variable
	* @access public
	*/

	public function getAddressPostalcode() {
		return $this->Address_PostalCode;
	}

	public function get_Address_PostalCode() {
		return $this->Address_PostalCode;
	}

	
// ------------------------------ End Field: Address_PostalCode --------------------------------------


// ---------------------------- Start Field: Address_Country -------------------------------------- 

	/** 
	* Sets a value to `Address_Country` variable
	* @access public
	*/

	public function setAddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Country` variable
	* @access public
	*/

	public function getAddressCountry() {
		return $this->Address_Country;
	}

	public function get_Address_Country() {
		return $this->Address_Country;
	}

	
// ------------------------------ End Field: Address_Country --------------------------------------


// ---------------------------- Start Field: Address_Note -------------------------------------- 

	/** 
	* Sets a value to `Address_Note` variable
	* @access public
	*/

	public function setAddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Address_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Address_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Address_Note` variable
	* @access public
	*/

	public function getAddressNote() {
		return $this->Address_Note;
	}

	public function get_Address_Note() {
		return $this->Address_Note;
	}

	
// ------------------------------ End Field: Address_Note --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr1` variable
	* @access public
	*/

	public function setAddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr1` variable
	* @access public
	*/

	public function getAddressblockAddr1() {
		return $this->AddressBlock_Addr1;
	}

	public function get_AddressBlock_Addr1() {
		return $this->AddressBlock_Addr1;
	}

	
// ------------------------------ End Field: AddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr2` variable
	* @access public
	*/

	public function setAddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr2` variable
	* @access public
	*/

	public function getAddressblockAddr2() {
		return $this->AddressBlock_Addr2;
	}

	public function get_AddressBlock_Addr2() {
		return $this->AddressBlock_Addr2;
	}

	
// ------------------------------ End Field: AddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr3` variable
	* @access public
	*/

	public function setAddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr3` variable
	* @access public
	*/

	public function getAddressblockAddr3() {
		return $this->AddressBlock_Addr3;
	}

	public function get_AddressBlock_Addr3() {
		return $this->AddressBlock_Addr3;
	}

	
// ------------------------------ End Field: AddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr4` variable
	* @access public
	*/

	public function setAddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr4` variable
	* @access public
	*/

	public function getAddressblockAddr4() {
		return $this->AddressBlock_Addr4;
	}

	public function get_AddressBlock_Addr4() {
		return $this->AddressBlock_Addr4;
	}

	
// ------------------------------ End Field: AddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: AddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `AddressBlock_Addr5` variable
	* @access public
	*/

	public function setAddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AddressBlock_Addr5` variable
	* @access public
	*/

	public function getAddressblockAddr5() {
		return $this->AddressBlock_Addr5;
	}

	public function get_AddressBlock_Addr5() {
		return $this->AddressBlock_Addr5;
	}

	
// ------------------------------ End Field: AddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: LegalAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Addr1` variable
	* @access public
	*/

	public function setLegaladdressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Addr1` variable
	* @access public
	*/

	public function getLegaladdressAddr1() {
		return $this->LegalAddress_Addr1;
	}

	public function get_LegalAddress_Addr1() {
		return $this->LegalAddress_Addr1;
	}

	
// ------------------------------ End Field: LegalAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: LegalAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Addr2` variable
	* @access public
	*/

	public function setLegaladdressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Addr2` variable
	* @access public
	*/

	public function getLegaladdressAddr2() {
		return $this->LegalAddress_Addr2;
	}

	public function get_LegalAddress_Addr2() {
		return $this->LegalAddress_Addr2;
	}

	
// ------------------------------ End Field: LegalAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: LegalAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Addr3` variable
	* @access public
	*/

	public function setLegaladdressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Addr3` variable
	* @access public
	*/

	public function getLegaladdressAddr3() {
		return $this->LegalAddress_Addr3;
	}

	public function get_LegalAddress_Addr3() {
		return $this->LegalAddress_Addr3;
	}

	
// ------------------------------ End Field: LegalAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: LegalAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Addr4` variable
	* @access public
	*/

	public function setLegaladdressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Addr4` variable
	* @access public
	*/

	public function getLegaladdressAddr4() {
		return $this->LegalAddress_Addr4;
	}

	public function get_LegalAddress_Addr4() {
		return $this->LegalAddress_Addr4;
	}

	
// ------------------------------ End Field: LegalAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: LegalAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Addr5` variable
	* @access public
	*/

	public function setLegaladdressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Addr5` variable
	* @access public
	*/

	public function getLegaladdressAddr5() {
		return $this->LegalAddress_Addr5;
	}

	public function get_LegalAddress_Addr5() {
		return $this->LegalAddress_Addr5;
	}

	
// ------------------------------ End Field: LegalAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: LegalAddress_City -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_City` variable
	* @access public
	*/

	public function setLegaladdressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_City` variable
	* @access public
	*/

	public function getLegaladdressCity() {
		return $this->LegalAddress_City;
	}

	public function get_LegalAddress_City() {
		return $this->LegalAddress_City;
	}

	
// ------------------------------ End Field: LegalAddress_City --------------------------------------


// ---------------------------- Start Field: LegalAddress_State -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_State` variable
	* @access public
	*/

	public function setLegaladdressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_State` variable
	* @access public
	*/

	public function getLegaladdressState() {
		return $this->LegalAddress_State;
	}

	public function get_LegalAddress_State() {
		return $this->LegalAddress_State;
	}

	
// ------------------------------ End Field: LegalAddress_State --------------------------------------


// ---------------------------- Start Field: LegalAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_PostalCode` variable
	* @access public
	*/

	public function setLegaladdressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_PostalCode` variable
	* @access public
	*/

	public function getLegaladdressPostalcode() {
		return $this->LegalAddress_PostalCode;
	}

	public function get_LegalAddress_PostalCode() {
		return $this->LegalAddress_PostalCode;
	}

	
// ------------------------------ End Field: LegalAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: LegalAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Country` variable
	* @access public
	*/

	public function setLegaladdressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Country` variable
	* @access public
	*/

	public function getLegaladdressCountry() {
		return $this->LegalAddress_Country;
	}

	public function get_LegalAddress_Country() {
		return $this->LegalAddress_Country;
	}

	
// ------------------------------ End Field: LegalAddress_Country --------------------------------------


// ---------------------------- Start Field: LegalAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `LegalAddress_Note` variable
	* @access public
	*/

	public function setLegaladdressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_LegalAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('LegalAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `LegalAddress_Note` variable
	* @access public
	*/

	public function getLegaladdressNote() {
		return $this->LegalAddress_Note;
	}

	public function get_LegalAddress_Note() {
		return $this->LegalAddress_Note;
	}

	
// ------------------------------ End Field: LegalAddress_Note --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Addr1` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Addr1` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerAddr1() {
		return $this->Company_CompanyAddressForCustomer_Addr1;
	}

	public function get_Company_CompanyAddressForCustomer_Addr1() {
		return $this->Company_CompanyAddressForCustomer_Addr1;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Addr1 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Addr2` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Addr2` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerAddr2() {
		return $this->Company_CompanyAddressForCustomer_Addr2;
	}

	public function get_Company_CompanyAddressForCustomer_Addr2() {
		return $this->Company_CompanyAddressForCustomer_Addr2;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Addr2 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Addr3` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Addr3` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerAddr3() {
		return $this->Company_CompanyAddressForCustomer_Addr3;
	}

	public function get_Company_CompanyAddressForCustomer_Addr3() {
		return $this->Company_CompanyAddressForCustomer_Addr3;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Addr3 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Addr4` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Addr4` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerAddr4() {
		return $this->Company_CompanyAddressForCustomer_Addr4;
	}

	public function get_Company_CompanyAddressForCustomer_Addr4() {
		return $this->Company_CompanyAddressForCustomer_Addr4;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Addr4 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Addr5` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Addr5` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerAddr5() {
		return $this->Company_CompanyAddressForCustomer_Addr5;
	}

	public function get_Company_CompanyAddressForCustomer_Addr5() {
		return $this->Company_CompanyAddressForCustomer_Addr5;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Addr5 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_City -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_City` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_City` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerCity() {
		return $this->Company_CompanyAddressForCustomer_City;
	}

	public function get_Company_CompanyAddressForCustomer_City() {
		return $this->Company_CompanyAddressForCustomer_City;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_City --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_State -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_State` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_State` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerState() {
		return $this->Company_CompanyAddressForCustomer_State;
	}

	public function get_Company_CompanyAddressForCustomer_State() {
		return $this->Company_CompanyAddressForCustomer_State;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_State --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_PostalCode` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_PostalCode` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerPostalcode() {
		return $this->Company_CompanyAddressForCustomer_PostalCode;
	}

	public function get_Company_CompanyAddressForCustomer_PostalCode() {
		return $this->Company_CompanyAddressForCustomer_PostalCode;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_PostalCode --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Country -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Country` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Country` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerCountry() {
		return $this->Company_CompanyAddressForCustomer_Country;
	}

	public function get_Company_CompanyAddressForCustomer_Country() {
		return $this->Company_CompanyAddressForCustomer_Country;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Country --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressForCustomer_Note -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressForCustomer_Note` variable
	* @access public
	*/

	public function setCompanyCompanyaddressforcustomerNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressForCustomer_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressForCustomer_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressForCustomer_Note` variable
	* @access public
	*/

	public function getCompanyCompanyaddressforcustomerNote() {
		return $this->Company_CompanyAddressForCustomer_Note;
	}

	public function get_Company_CompanyAddressForCustomer_Note() {
		return $this->Company_CompanyAddressForCustomer_Note;
	}

	
// ------------------------------ End Field: Company_CompanyAddressForCustomer_Note --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressBlockForCustomer_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressBlockForCustomer_Addr1` variable
	* @access public
	*/

	public function setCompanyCompanyaddressblockforcustomerAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressBlockForCustomer_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressBlockForCustomer_Addr1` variable
	* @access public
	*/

	public function getCompanyCompanyaddressblockforcustomerAddr1() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr1;
	}

	public function get_Company_CompanyAddressBlockForCustomer_Addr1() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr1;
	}

	
// ------------------------------ End Field: Company_CompanyAddressBlockForCustomer_Addr1 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressBlockForCustomer_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressBlockForCustomer_Addr2` variable
	* @access public
	*/

	public function setCompanyCompanyaddressblockforcustomerAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressBlockForCustomer_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressBlockForCustomer_Addr2` variable
	* @access public
	*/

	public function getCompanyCompanyaddressblockforcustomerAddr2() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr2;
	}

	public function get_Company_CompanyAddressBlockForCustomer_Addr2() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr2;
	}

	
// ------------------------------ End Field: Company_CompanyAddressBlockForCustomer_Addr2 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressBlockForCustomer_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressBlockForCustomer_Addr3` variable
	* @access public
	*/

	public function setCompanyCompanyaddressblockforcustomerAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressBlockForCustomer_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressBlockForCustomer_Addr3` variable
	* @access public
	*/

	public function getCompanyCompanyaddressblockforcustomerAddr3() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr3;
	}

	public function get_Company_CompanyAddressBlockForCustomer_Addr3() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr3;
	}

	
// ------------------------------ End Field: Company_CompanyAddressBlockForCustomer_Addr3 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressBlockForCustomer_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressBlockForCustomer_Addr4` variable
	* @access public
	*/

	public function setCompanyCompanyaddressblockforcustomerAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressBlockForCustomer_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressBlockForCustomer_Addr4` variable
	* @access public
	*/

	public function getCompanyCompanyaddressblockforcustomerAddr4() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr4;
	}

	public function get_Company_CompanyAddressBlockForCustomer_Addr4() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr4;
	}

	
// ------------------------------ End Field: Company_CompanyAddressBlockForCustomer_Addr4 --------------------------------------


// ---------------------------- Start Field: Company_CompanyAddressBlockForCustomer_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `Company_CompanyAddressBlockForCustomer_Addr5` variable
	* @access public
	*/

	public function setCompanyCompanyaddressblockforcustomerAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Company_CompanyAddressBlockForCustomer_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Company_CompanyAddressBlockForCustomer_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Company_CompanyAddressBlockForCustomer_Addr5` variable
	* @access public
	*/

	public function getCompanyCompanyaddressblockforcustomerAddr5() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr5;
	}

	public function get_Company_CompanyAddressBlockForCustomer_Addr5() {
		return $this->Company_CompanyAddressBlockForCustomer_Addr5;
	}

	
// ------------------------------ End Field: Company_CompanyAddressBlockForCustomer_Addr5 --------------------------------------


// ---------------------------- Start Field: Phone -------------------------------------- 

	/** 
	* Sets a value to `Phone` variable
	* @access public
	*/

	public function setPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Phone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Phone', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Phone` variable
	* @access public
	*/

	public function getPhone() {
		return $this->Phone;
	}

	public function get_Phone() {
		return $this->Phone;
	}

	
// ------------------------------ End Field: Phone --------------------------------------


// ---------------------------- Start Field: Fax -------------------------------------- 

	/** 
	* Sets a value to `Fax` variable
	* @access public
	*/

	public function setFax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Fax($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Fax', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Fax` variable
	* @access public
	*/

	public function getFax() {
		return $this->Fax;
	}

	public function get_Fax() {
		return $this->Fax;
	}

	
// ------------------------------ End Field: Fax --------------------------------------


// ---------------------------- Start Field: Email -------------------------------------- 

	/** 
	* Sets a value to `Email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Email($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->Email;
	}

	public function get_Email() {
		return $this->Email;
	}

	
// ------------------------------ End Field: Email --------------------------------------


// ---------------------------- Start Field: CompanyWebSite -------------------------------------- 

	/** 
	* Sets a value to `CompanyWebSite` variable
	* @access public
	*/

	public function setCompanywebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyWebSite', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CompanyWebSite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyWebSite', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CompanyWebSite` variable
	* @access public
	*/

	public function getCompanywebsite() {
		return $this->CompanyWebSite;
	}

	public function get_CompanyWebSite() {
		return $this->CompanyWebSite;
	}

	
// ------------------------------ End Field: CompanyWebSite --------------------------------------


// ---------------------------- Start Field: FirstMonthFiscalYear -------------------------------------- 

	/** 
	* Sets a value to `FirstMonthFiscalYear` variable
	* @access public
	*/

	public function setFirstmonthfiscalyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstMonthFiscalYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FirstMonthFiscalYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstMonthFiscalYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FirstMonthFiscalYear` variable
	* @access public
	*/

	public function getFirstmonthfiscalyear() {
		return $this->FirstMonthFiscalYear;
	}

	public function get_FirstMonthFiscalYear() {
		return $this->FirstMonthFiscalYear;
	}

	
// ------------------------------ End Field: FirstMonthFiscalYear --------------------------------------


// ---------------------------- Start Field: FirstMonthIncomeTaxYear -------------------------------------- 

	/** 
	* Sets a value to `FirstMonthIncomeTaxYear` variable
	* @access public
	*/

	public function setFirstmonthincometaxyear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstMonthIncomeTaxYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FirstMonthIncomeTaxYear($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FirstMonthIncomeTaxYear', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FirstMonthIncomeTaxYear` variable
	* @access public
	*/

	public function getFirstmonthincometaxyear() {
		return $this->FirstMonthIncomeTaxYear;
	}

	public function get_FirstMonthIncomeTaxYear() {
		return $this->FirstMonthIncomeTaxYear;
	}

	
// ------------------------------ End Field: FirstMonthIncomeTaxYear --------------------------------------


// ---------------------------- Start Field: CompanyType -------------------------------------- 

	/** 
	* Sets a value to `CompanyType` variable
	* @access public
	*/

	public function setCompanytype($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CompanyType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CompanyType', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CompanyType` variable
	* @access public
	*/

	public function getCompanytype() {
		return $this->CompanyType;
	}

	public function get_CompanyType() {
		return $this->CompanyType;
	}

	
// ------------------------------ End Field: CompanyType --------------------------------------


// ---------------------------- Start Field: EIN -------------------------------------- 

	/** 
	* Sets a value to `EIN` variable
	* @access public
	*/

	public function setEin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EIN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EIN($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EIN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EIN` variable
	* @access public
	*/

	public function getEin() {
		return $this->EIN;
	}

	public function get_EIN() {
		return $this->EIN;
	}

	
// ------------------------------ End Field: EIN --------------------------------------


// ---------------------------- Start Field: SSN -------------------------------------- 

	/** 
	* Sets a value to `SSN` variable
	* @access public
	*/

	public function setSsn($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SSN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SSN($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SSN', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SSN` variable
	* @access public
	*/

	public function getSsn() {
		return $this->SSN;
	}

	public function get_SSN() {
		return $this->SSN;
	}

	
// ------------------------------ End Field: SSN --------------------------------------


// ---------------------------- Start Field: TaxForm -------------------------------------- 

	/** 
	* Sets a value to `TaxForm` variable
	* @access public
	*/

	public function setTaxform($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxForm', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TaxForm($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TaxForm', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TaxForm` variable
	* @access public
	*/

	public function getTaxform() {
		return $this->TaxForm;
	}

	public function get_TaxForm() {
		return $this->TaxForm;
	}

	
// ------------------------------ End Field: TaxForm --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'IsSampleCompany' => (object) array(
										'Field'=>'IsSampleCompany',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'CompanyName' => (object) array(
										'Field'=>'CompanyName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalCompanyName' => (object) array(
										'Field'=>'LegalCompanyName',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr1' => (object) array(
										'Field'=>'Address_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr2' => (object) array(
										'Field'=>'Address_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr3' => (object) array(
										'Field'=>'Address_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr4' => (object) array(
										'Field'=>'Address_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Addr5' => (object) array(
										'Field'=>'Address_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_City' => (object) array(
										'Field'=>'Address_City',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_State' => (object) array(
										'Field'=>'Address_State',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_PostalCode' => (object) array(
										'Field'=>'Address_PostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Country' => (object) array(
										'Field'=>'Address_Country',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Address_Note' => (object) array(
										'Field'=>'Address_Note',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr1' => (object) array(
										'Field'=>'AddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr2' => (object) array(
										'Field'=>'AddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr3' => (object) array(
										'Field'=>'AddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr4' => (object) array(
										'Field'=>'AddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AddressBlock_Addr5' => (object) array(
										'Field'=>'AddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Addr1' => (object) array(
										'Field'=>'LegalAddress_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Addr2' => (object) array(
										'Field'=>'LegalAddress_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Addr3' => (object) array(
										'Field'=>'LegalAddress_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Addr4' => (object) array(
										'Field'=>'LegalAddress_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Addr5' => (object) array(
										'Field'=>'LegalAddress_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_City' => (object) array(
										'Field'=>'LegalAddress_City',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_State' => (object) array(
										'Field'=>'LegalAddress_State',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_PostalCode' => (object) array(
										'Field'=>'LegalAddress_PostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Country' => (object) array(
										'Field'=>'LegalAddress_Country',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'LegalAddress_Note' => (object) array(
										'Field'=>'LegalAddress_Note',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Addr1' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Addr2' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Addr3' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Addr4' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Addr5' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_City' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_City',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_State' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_State',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_PostalCode' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_PostalCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Country' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Country',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressForCustomer_Note' => (object) array(
										'Field'=>'Company_CompanyAddressForCustomer_Note',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressBlockForCustomer_Addr1' => (object) array(
										'Field'=>'Company_CompanyAddressBlockForCustomer_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressBlockForCustomer_Addr2' => (object) array(
										'Field'=>'Company_CompanyAddressBlockForCustomer_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressBlockForCustomer_Addr3' => (object) array(
										'Field'=>'Company_CompanyAddressBlockForCustomer_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressBlockForCustomer_Addr4' => (object) array(
										'Field'=>'Company_CompanyAddressBlockForCustomer_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Company_CompanyAddressBlockForCustomer_Addr5' => (object) array(
										'Field'=>'Company_CompanyAddressBlockForCustomer_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Phone' => (object) array(
										'Field'=>'Phone',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Fax' => (object) array(
										'Field'=>'Fax',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Email' => (object) array(
										'Field'=>'Email',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CompanyWebSite' => (object) array(
										'Field'=>'CompanyWebSite',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FirstMonthFiscalYear' => (object) array(
										'Field'=>'FirstMonthFiscalYear',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FirstMonthIncomeTaxYear' => (object) array(
										'Field'=>'FirstMonthIncomeTaxYear',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CompanyType' => (object) array(
										'Field'=>'CompanyType',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EIN' => (object) array(
										'Field'=>'EIN',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SSN' => (object) array(
										'Field'=>'SSN',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TaxForm' => (object) array(
										'Field'=>'TaxForm',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_company` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'IsSampleCompany' => "ALTER TABLE  `qb_company` ADD  `IsSampleCompany` tinyint(1) NULL   DEFAULT '0';",
			'CompanyName' => "ALTER TABLE  `qb_company` ADD  `CompanyName` text NULL   ;",
			'LegalCompanyName' => "ALTER TABLE  `qb_company` ADD  `LegalCompanyName` text NULL   ;",
			'Address_Addr1' => "ALTER TABLE  `qb_company` ADD  `Address_Addr1` text NULL   ;",
			'Address_Addr2' => "ALTER TABLE  `qb_company` ADD  `Address_Addr2` text NULL   ;",
			'Address_Addr3' => "ALTER TABLE  `qb_company` ADD  `Address_Addr3` text NULL   ;",
			'Address_Addr4' => "ALTER TABLE  `qb_company` ADD  `Address_Addr4` text NULL   ;",
			'Address_Addr5' => "ALTER TABLE  `qb_company` ADD  `Address_Addr5` text NULL   ;",
			'Address_City' => "ALTER TABLE  `qb_company` ADD  `Address_City` text NULL   ;",
			'Address_State' => "ALTER TABLE  `qb_company` ADD  `Address_State` text NULL   ;",
			'Address_PostalCode' => "ALTER TABLE  `qb_company` ADD  `Address_PostalCode` text NULL   ;",
			'Address_Country' => "ALTER TABLE  `qb_company` ADD  `Address_Country` text NULL   ;",
			'Address_Note' => "ALTER TABLE  `qb_company` ADD  `Address_Note` text NULL   ;",
			'AddressBlock_Addr1' => "ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr1` text NULL   ;",
			'AddressBlock_Addr2' => "ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr2` text NULL   ;",
			'AddressBlock_Addr3' => "ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr3` text NULL   ;",
			'AddressBlock_Addr4' => "ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr4` text NULL   ;",
			'AddressBlock_Addr5' => "ALTER TABLE  `qb_company` ADD  `AddressBlock_Addr5` text NULL   ;",
			'LegalAddress_Addr1' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr1` text NULL   ;",
			'LegalAddress_Addr2' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr2` text NULL   ;",
			'LegalAddress_Addr3' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr3` text NULL   ;",
			'LegalAddress_Addr4' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr4` text NULL   ;",
			'LegalAddress_Addr5' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Addr5` text NULL   ;",
			'LegalAddress_City' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_City` text NULL   ;",
			'LegalAddress_State' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_State` text NULL   ;",
			'LegalAddress_PostalCode' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_PostalCode` text NULL   ;",
			'LegalAddress_Country' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Country` text NULL   ;",
			'LegalAddress_Note' => "ALTER TABLE  `qb_company` ADD  `LegalAddress_Note` text NULL   ;",
			'Company_CompanyAddressForCustomer_Addr1' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr1` text NULL   ;",
			'Company_CompanyAddressForCustomer_Addr2' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr2` text NULL   ;",
			'Company_CompanyAddressForCustomer_Addr3' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr3` text NULL   ;",
			'Company_CompanyAddressForCustomer_Addr4' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr4` text NULL   ;",
			'Company_CompanyAddressForCustomer_Addr5' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Addr5` text NULL   ;",
			'Company_CompanyAddressForCustomer_City' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_City` text NULL   ;",
			'Company_CompanyAddressForCustomer_State' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_State` text NULL   ;",
			'Company_CompanyAddressForCustomer_PostalCode' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_PostalCode` text NULL   ;",
			'Company_CompanyAddressForCustomer_Country' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Country` text NULL   ;",
			'Company_CompanyAddressForCustomer_Note' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressForCustomer_Note` text NULL   ;",
			'Company_CompanyAddressBlockForCustomer_Addr1' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr1` text NULL   ;",
			'Company_CompanyAddressBlockForCustomer_Addr2' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr2` text NULL   ;",
			'Company_CompanyAddressBlockForCustomer_Addr3' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr3` text NULL   ;",
			'Company_CompanyAddressBlockForCustomer_Addr4' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr4` text NULL   ;",
			'Company_CompanyAddressBlockForCustomer_Addr5' => "ALTER TABLE  `qb_company` ADD  `Company_CompanyAddressBlockForCustomer_Addr5` text NULL   ;",
			'Phone' => "ALTER TABLE  `qb_company` ADD  `Phone` text NULL   ;",
			'Fax' => "ALTER TABLE  `qb_company` ADD  `Fax` text NULL   ;",
			'Email' => "ALTER TABLE  `qb_company` ADD  `Email` text NULL   ;",
			'CompanyWebSite' => "ALTER TABLE  `qb_company` ADD  `CompanyWebSite` text NULL   ;",
			'FirstMonthFiscalYear' => "ALTER TABLE  `qb_company` ADD  `FirstMonthFiscalYear` varchar(40) NULL   ;",
			'FirstMonthIncomeTaxYear' => "ALTER TABLE  `qb_company` ADD  `FirstMonthIncomeTaxYear` varchar(40) NULL   ;",
			'CompanyType' => "ALTER TABLE  `qb_company` ADD  `CompanyType` text NULL   ;",
			'EIN' => "ALTER TABLE  `qb_company` ADD  `EIN` text NULL   ;",
			'SSN' => "ALTER TABLE  `qb_company` ADD  `SSN` text NULL   ;",
			'TaxForm' => "ALTER TABLE  `qb_company` ADD  `TaxForm` varchar(40) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setIssamplecompany() - IsSampleCompany
//setCompanyname() - CompanyName
//setLegalcompanyname() - LegalCompanyName
//setAddressAddr1() - Address_Addr1
//setAddressAddr2() - Address_Addr2
//setAddressAddr3() - Address_Addr3
//setAddressAddr4() - Address_Addr4
//setAddressAddr5() - Address_Addr5
//setAddressCity() - Address_City
//setAddressState() - Address_State
//setAddressPostalcode() - Address_PostalCode
//setAddressCountry() - Address_Country
//setAddressNote() - Address_Note
//setAddressblockAddr1() - AddressBlock_Addr1
//setAddressblockAddr2() - AddressBlock_Addr2
//setAddressblockAddr3() - AddressBlock_Addr3
//setAddressblockAddr4() - AddressBlock_Addr4
//setAddressblockAddr5() - AddressBlock_Addr5
//setLegaladdressAddr1() - LegalAddress_Addr1
//setLegaladdressAddr2() - LegalAddress_Addr2
//setLegaladdressAddr3() - LegalAddress_Addr3
//setLegaladdressAddr4() - LegalAddress_Addr4
//setLegaladdressAddr5() - LegalAddress_Addr5
//setLegaladdressCity() - LegalAddress_City
//setLegaladdressState() - LegalAddress_State
//setLegaladdressPostalcode() - LegalAddress_PostalCode
//setLegaladdressCountry() - LegalAddress_Country
//setLegaladdressNote() - LegalAddress_Note
//setCompanyCompanyaddressforcustomerAddr1() - Company_CompanyAddressForCustomer_Addr1
//setCompanyCompanyaddressforcustomerAddr2() - Company_CompanyAddressForCustomer_Addr2
//setCompanyCompanyaddressforcustomerAddr3() - Company_CompanyAddressForCustomer_Addr3
//setCompanyCompanyaddressforcustomerAddr4() - Company_CompanyAddressForCustomer_Addr4
//setCompanyCompanyaddressforcustomerAddr5() - Company_CompanyAddressForCustomer_Addr5
//setCompanyCompanyaddressforcustomerCity() - Company_CompanyAddressForCustomer_City
//setCompanyCompanyaddressforcustomerState() - Company_CompanyAddressForCustomer_State
//setCompanyCompanyaddressforcustomerPostalcode() - Company_CompanyAddressForCustomer_PostalCode
//setCompanyCompanyaddressforcustomerCountry() - Company_CompanyAddressForCustomer_Country
//setCompanyCompanyaddressforcustomerNote() - Company_CompanyAddressForCustomer_Note
//setCompanyCompanyaddressblockforcustomerAddr1() - Company_CompanyAddressBlockForCustomer_Addr1
//setCompanyCompanyaddressblockforcustomerAddr2() - Company_CompanyAddressBlockForCustomer_Addr2
//setCompanyCompanyaddressblockforcustomerAddr3() - Company_CompanyAddressBlockForCustomer_Addr3
//setCompanyCompanyaddressblockforcustomerAddr4() - Company_CompanyAddressBlockForCustomer_Addr4
//setCompanyCompanyaddressblockforcustomerAddr5() - Company_CompanyAddressBlockForCustomer_Addr5
//setPhone() - Phone
//setFax() - Fax
//setEmail() - Email
//setCompanywebsite() - CompanyWebSite
//setFirstmonthfiscalyear() - FirstMonthFiscalYear
//setFirstmonthincometaxyear() - FirstMonthIncomeTaxYear
//setCompanytype() - CompanyType
//setEin() - EIN
//setSsn() - SSN
//setTaxform() - TaxForm

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_IsSampleCompany() - IsSampleCompany
//set_CompanyName() - CompanyName
//set_LegalCompanyName() - LegalCompanyName
//set_Address_Addr1() - Address_Addr1
//set_Address_Addr2() - Address_Addr2
//set_Address_Addr3() - Address_Addr3
//set_Address_Addr4() - Address_Addr4
//set_Address_Addr5() - Address_Addr5
//set_Address_City() - Address_City
//set_Address_State() - Address_State
//set_Address_PostalCode() - Address_PostalCode
//set_Address_Country() - Address_Country
//set_Address_Note() - Address_Note
//set_AddressBlock_Addr1() - AddressBlock_Addr1
//set_AddressBlock_Addr2() - AddressBlock_Addr2
//set_AddressBlock_Addr3() - AddressBlock_Addr3
//set_AddressBlock_Addr4() - AddressBlock_Addr4
//set_AddressBlock_Addr5() - AddressBlock_Addr5
//set_LegalAddress_Addr1() - LegalAddress_Addr1
//set_LegalAddress_Addr2() - LegalAddress_Addr2
//set_LegalAddress_Addr3() - LegalAddress_Addr3
//set_LegalAddress_Addr4() - LegalAddress_Addr4
//set_LegalAddress_Addr5() - LegalAddress_Addr5
//set_LegalAddress_City() - LegalAddress_City
//set_LegalAddress_State() - LegalAddress_State
//set_LegalAddress_PostalCode() - LegalAddress_PostalCode
//set_LegalAddress_Country() - LegalAddress_Country
//set_LegalAddress_Note() - LegalAddress_Note
//set_Company_CompanyAddressForCustomer_Addr1() - Company_CompanyAddressForCustomer_Addr1
//set_Company_CompanyAddressForCustomer_Addr2() - Company_CompanyAddressForCustomer_Addr2
//set_Company_CompanyAddressForCustomer_Addr3() - Company_CompanyAddressForCustomer_Addr3
//set_Company_CompanyAddressForCustomer_Addr4() - Company_CompanyAddressForCustomer_Addr4
//set_Company_CompanyAddressForCustomer_Addr5() - Company_CompanyAddressForCustomer_Addr5
//set_Company_CompanyAddressForCustomer_City() - Company_CompanyAddressForCustomer_City
//set_Company_CompanyAddressForCustomer_State() - Company_CompanyAddressForCustomer_State
//set_Company_CompanyAddressForCustomer_PostalCode() - Company_CompanyAddressForCustomer_PostalCode
//set_Company_CompanyAddressForCustomer_Country() - Company_CompanyAddressForCustomer_Country
//set_Company_CompanyAddressForCustomer_Note() - Company_CompanyAddressForCustomer_Note
//set_Company_CompanyAddressBlockForCustomer_Addr1() - Company_CompanyAddressBlockForCustomer_Addr1
//set_Company_CompanyAddressBlockForCustomer_Addr2() - Company_CompanyAddressBlockForCustomer_Addr2
//set_Company_CompanyAddressBlockForCustomer_Addr3() - Company_CompanyAddressBlockForCustomer_Addr3
//set_Company_CompanyAddressBlockForCustomer_Addr4() - Company_CompanyAddressBlockForCustomer_Addr4
//set_Company_CompanyAddressBlockForCustomer_Addr5() - Company_CompanyAddressBlockForCustomer_Addr5
//set_Phone() - Phone
//set_Fax() - Fax
//set_Email() - Email
//set_CompanyWebSite() - CompanyWebSite
//set_FirstMonthFiscalYear() - FirstMonthFiscalYear
//set_FirstMonthIncomeTaxYear() - FirstMonthIncomeTaxYear
//set_CompanyType() - CompanyType
//set_EIN() - EIN
//set_SSN() - SSN
//set_TaxForm() - TaxForm

*/
/* End of file Qb_company_model.php */
/* Location: ./application/models/Qb_company_model.php */
