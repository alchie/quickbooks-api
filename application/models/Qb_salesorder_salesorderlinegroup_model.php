<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_salesorder_salesorderlinegroup_model Class
 *
 * Manipulates `qb_salesorder_salesorderlinegroup` table on database

CREATE TABLE `qb_salesorder_salesorderlinegroup` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SalesOrder_TxnID` varchar(40) DEFAULT NULL,
  `SortOrder` int(10) unsigned DEFAULT '0',
  `TxnLineID` varchar(40) DEFAULT NULL,
  `ItemGroup_ListID` varchar(40) DEFAULT NULL,
  `ItemGroup_FullName` varchar(255) DEFAULT NULL,
  `Descrip` text,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `UnitOfMeasure` text,
  `OverrideUOMSet_ListID` varchar(40) DEFAULT NULL,
  `OverrideUOMSet_FullName` varchar(255) DEFAULT NULL,
  `IsPrintItemsInGroup` tinyint(1) DEFAULT '0',
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `SalesOrder_TxnID` (`SalesOrder_TxnID`),
  KEY `TxnLineID` (`TxnLineID`),
  KEY `ItemGroup_ListID` (`ItemGroup_ListID`),
  KEY `OverrideUOMSet_ListID` (`OverrideUOMSet_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `SalesOrder_TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `TxnLineID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `ItemGroup_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `ItemGroup_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `UnitOfMeasure` text NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `IsPrintItemsInGroup` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `TotalAmount` decimal(10,2) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_salesorder_salesorderlinegroup_model extends MY_Model {

	protected $qbxml_id;
	protected $SalesOrder_TxnID;
	protected $SortOrder;
	protected $TxnLineID;
	protected $ItemGroup_ListID;
	protected $ItemGroup_FullName;
	protected $Descrip;
	protected $Quantity;
	protected $UnitOfMeasure;
	protected $OverrideUOMSet_ListID;
	protected $OverrideUOMSet_FullName;
	protected $IsPrintItemsInGroup;
	protected $TotalAmount;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_salesorder_salesorderlinegroup';
		$this->_short_name = 'qb_salesorder_salesorderlinegroup';
		$this->_fields = array("qbxml_id","SalesOrder_TxnID","SortOrder","TxnLineID","ItemGroup_ListID","ItemGroup_FullName","Descrip","Quantity","UnitOfMeasure","OverrideUOMSet_ListID","OverrideUOMSet_FullName","IsPrintItemsInGroup","TotalAmount");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: SalesOrder_TxnID -------------------------------------- 

	/** 
	* Sets a value to `SalesOrder_TxnID` variable
	* @access public
	*/

	public function setSalesorderTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrder_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesOrder_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesOrder_TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesOrder_TxnID` variable
	* @access public
	*/

	public function getSalesorderTxnid() {
		return $this->SalesOrder_TxnID;
	}

	public function get_SalesOrder_TxnID() {
		return $this->SalesOrder_TxnID;
	}

	
// ------------------------------ End Field: SalesOrder_TxnID --------------------------------------


// ---------------------------- Start Field: SortOrder -------------------------------------- 

	/** 
	* Sets a value to `SortOrder` variable
	* @access public
	*/

	public function setSortorder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SortOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SortOrder', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SortOrder` variable
	* @access public
	*/

	public function getSortorder() {
		return $this->SortOrder;
	}

	public function get_SortOrder() {
		return $this->SortOrder;
	}

	
// ------------------------------ End Field: SortOrder --------------------------------------


// ---------------------------- Start Field: TxnLineID -------------------------------------- 

	/** 
	* Sets a value to `TxnLineID` variable
	* @access public
	*/

	public function setTxnlineid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnLineID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnLineID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnLineID` variable
	* @access public
	*/

	public function getTxnlineid() {
		return $this->TxnLineID;
	}

	public function get_TxnLineID() {
		return $this->TxnLineID;
	}

	
// ------------------------------ End Field: TxnLineID --------------------------------------


// ---------------------------- Start Field: ItemGroup_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemGroup_ListID` variable
	* @access public
	*/

	public function setItemgroupListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemGroup_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemGroup_ListID` variable
	* @access public
	*/

	public function getItemgroupListid() {
		return $this->ItemGroup_ListID;
	}

	public function get_ItemGroup_ListID() {
		return $this->ItemGroup_ListID;
	}

	
// ------------------------------ End Field: ItemGroup_ListID --------------------------------------


// ---------------------------- Start Field: ItemGroup_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemGroup_FullName` variable
	* @access public
	*/

	public function setItemgroupFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemGroup_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemGroup_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemGroup_FullName` variable
	* @access public
	*/

	public function getItemgroupFullname() {
		return $this->ItemGroup_FullName;
	}

	public function get_ItemGroup_FullName() {
		return $this->ItemGroup_FullName;
	}

	
// ------------------------------ End Field: ItemGroup_FullName --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: UnitOfMeasure -------------------------------------- 

	/** 
	* Sets a value to `UnitOfMeasure` variable
	* @access public
	*/

	public function setUnitofmeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_UnitOfMeasure($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('UnitOfMeasure', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `UnitOfMeasure` variable
	* @access public
	*/

	public function getUnitofmeasure() {
		return $this->UnitOfMeasure;
	}

	public function get_UnitOfMeasure() {
		return $this->UnitOfMeasure;
	}

	
// ------------------------------ End Field: UnitOfMeasure --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_ListID -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function setOverrideuomsetListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_ListID` variable
	* @access public
	*/

	public function getOverrideuomsetListid() {
		return $this->OverrideUOMSet_ListID;
	}

	public function get_OverrideUOMSet_ListID() {
		return $this->OverrideUOMSet_ListID;
	}

	
// ------------------------------ End Field: OverrideUOMSet_ListID --------------------------------------


// ---------------------------- Start Field: OverrideUOMSet_FullName -------------------------------------- 

	/** 
	* Sets a value to `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function setOverrideuomsetFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_OverrideUOMSet_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('OverrideUOMSet_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `OverrideUOMSet_FullName` variable
	* @access public
	*/

	public function getOverrideuomsetFullname() {
		return $this->OverrideUOMSet_FullName;
	}

	public function get_OverrideUOMSet_FullName() {
		return $this->OverrideUOMSet_FullName;
	}

	
// ------------------------------ End Field: OverrideUOMSet_FullName --------------------------------------


// ---------------------------- Start Field: IsPrintItemsInGroup -------------------------------------- 

	/** 
	* Sets a value to `IsPrintItemsInGroup` variable
	* @access public
	*/

	public function setIsprintitemsingroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPrintItemsInGroup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPrintItemsInGroup($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPrintItemsInGroup', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPrintItemsInGroup` variable
	* @access public
	*/

	public function getIsprintitemsingroup() {
		return $this->IsPrintItemsInGroup;
	}

	public function get_IsPrintItemsInGroup() {
		return $this->IsPrintItemsInGroup;
	}

	
// ------------------------------ End Field: IsPrintItemsInGroup --------------------------------------


// ---------------------------- Start Field: TotalAmount -------------------------------------- 

	/** 
	* Sets a value to `TotalAmount` variable
	* @access public
	*/

	public function setTotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmount` variable
	* @access public
	*/

	public function getTotalamount() {
		return $this->TotalAmount;
	}

	public function get_TotalAmount() {
		return $this->TotalAmount;
	}

	
// ------------------------------ End Field: TotalAmount --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'SalesOrder_TxnID' => (object) array(
										'Field'=>'SalesOrder_TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SortOrder' => (object) array(
										'Field'=>'SortOrder',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TxnLineID' => (object) array(
										'Field'=>'TxnLineID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemGroup_ListID' => (object) array(
										'Field'=>'ItemGroup_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemGroup_FullName' => (object) array(
										'Field'=>'ItemGroup_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'UnitOfMeasure' => (object) array(
										'Field'=>'UnitOfMeasure',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_ListID' => (object) array(
										'Field'=>'OverrideUOMSet_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'OverrideUOMSet_FullName' => (object) array(
										'Field'=>'OverrideUOMSet_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPrintItemsInGroup' => (object) array(
										'Field'=>'IsPrintItemsInGroup',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'TotalAmount' => (object) array(
										'Field'=>'TotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'SalesOrder_TxnID' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `SalesOrder_TxnID` varchar(40) NULL   ;",
			'SortOrder' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `SortOrder` int(10) unsigned NULL   DEFAULT '0';",
			'TxnLineID' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `TxnLineID` varchar(40) NULL   ;",
			'ItemGroup_ListID' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `ItemGroup_ListID` varchar(40) NULL   ;",
			'ItemGroup_FullName' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `ItemGroup_FullName` varchar(255) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `Descrip` text NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'UnitOfMeasure' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `UnitOfMeasure` text NULL   ;",
			'OverrideUOMSet_ListID' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `OverrideUOMSet_ListID` varchar(40) NULL   ;",
			'OverrideUOMSet_FullName' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `OverrideUOMSet_FullName` varchar(255) NULL   ;",
			'IsPrintItemsInGroup' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `IsPrintItemsInGroup` tinyint(1) NULL   DEFAULT '0';",
			'TotalAmount' => "ALTER TABLE  `qb_salesorder_salesorderlinegroup` ADD  `TotalAmount` decimal(10,2) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setSalesorderTxnid() - SalesOrder_TxnID
//setSortorder() - SortOrder
//setTxnlineid() - TxnLineID
//setItemgroupListid() - ItemGroup_ListID
//setItemgroupFullname() - ItemGroup_FullName
//setDescrip() - Descrip
//setQuantity() - Quantity
//setUnitofmeasure() - UnitOfMeasure
//setOverrideuomsetListid() - OverrideUOMSet_ListID
//setOverrideuomsetFullname() - OverrideUOMSet_FullName
//setIsprintitemsingroup() - IsPrintItemsInGroup
//setTotalamount() - TotalAmount

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_SalesOrder_TxnID() - SalesOrder_TxnID
//set_SortOrder() - SortOrder
//set_TxnLineID() - TxnLineID
//set_ItemGroup_ListID() - ItemGroup_ListID
//set_ItemGroup_FullName() - ItemGroup_FullName
//set_Descrip() - Descrip
//set_Quantity() - Quantity
//set_UnitOfMeasure() - UnitOfMeasure
//set_OverrideUOMSet_ListID() - OverrideUOMSet_ListID
//set_OverrideUOMSet_FullName() - OverrideUOMSet_FullName
//set_IsPrintItemsInGroup() - IsPrintItemsInGroup
//set_TotalAmount() - TotalAmount

*/
/* End of file Qb_salesorder_salesorderlinegroup_model.php */
/* Location: ./application/models/Qb_salesorder_salesorderlinegroup_model.php */
