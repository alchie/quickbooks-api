<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_purchaseorder_model Class
 *
 * Manipulates `qb_purchaseorder` table on database

CREATE TABLE `qb_purchaseorder` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Vendor_ListID` varchar(40) DEFAULT NULL,
  `Vendor_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `ShipToEntity_ListID` varchar(40) DEFAULT NULL,
  `ShipToEntity_FullName` varchar(255) DEFAULT NULL,
  `Template_ListID` varchar(40) DEFAULT NULL,
  `Template_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `VendorAddress_Addr1` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr2` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr3` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr4` varchar(41) DEFAULT NULL,
  `VendorAddress_Addr5` varchar(41) DEFAULT NULL,
  `VendorAddress_City` varchar(31) DEFAULT NULL,
  `VendorAddress_State` varchar(21) DEFAULT NULL,
  `VendorAddress_PostalCode` varchar(13) DEFAULT NULL,
  `VendorAddress_Country` varchar(31) DEFAULT NULL,
  `VendorAddress_Note` varchar(41) DEFAULT NULL,
  `VendorAddressBlock_Addr1` text,
  `VendorAddressBlock_Addr2` text,
  `VendorAddressBlock_Addr3` text,
  `VendorAddressBlock_Addr4` text,
  `VendorAddressBlock_Addr5` text,
  `ShipAddress_Addr1` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr2` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr3` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr4` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr5` varchar(41) DEFAULT NULL,
  `ShipAddress_City` varchar(31) DEFAULT NULL,
  `ShipAddress_State` varchar(21) DEFAULT NULL,
  `ShipAddress_PostalCode` varchar(13) DEFAULT NULL,
  `ShipAddress_Country` varchar(31) DEFAULT NULL,
  `ShipAddress_Note` varchar(41) DEFAULT NULL,
  `ShipAddressBlock_Addr1` text,
  `ShipAddressBlock_Addr2` text,
  `ShipAddressBlock_Addr3` text,
  `ShipAddressBlock_Addr4` text,
  `ShipAddressBlock_Addr5` text,
  `Terms_ListID` varchar(40) DEFAULT NULL,
  `Terms_FullName` varchar(255) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `ExpectedDate` date DEFAULT NULL,
  `ShipMethod_ListID` varchar(40) DEFAULT NULL,
  `ShipMethod_FullName` varchar(255) DEFAULT NULL,
  `FOB` varchar(13) DEFAULT NULL,
  `TotalAmount` decimal(10,2) DEFAULT NULL,
  `Currency_ListID` varchar(40) DEFAULT NULL,
  `Currency_FullName` varchar(255) DEFAULT NULL,
  `ExchangeRate` text,
  `TotalAmountInHomeCurrency` decimal(10,2) DEFAULT NULL,
  `IsManuallyClosed` tinyint(1) DEFAULT NULL,
  `IsFullyReceived` tinyint(1) DEFAULT NULL,
  `Memo` text,
  `VendorMsg` varchar(99) DEFAULT NULL,
  `IsToBePrinted` tinyint(1) DEFAULT NULL,
  `IsToBeEmailed` tinyint(1) DEFAULT NULL,
  `Other1` varchar(25) DEFAULT NULL,
  `Other2` varchar(29) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Vendor_ListID` (`Vendor_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `ShipToEntity_ListID` (`ShipToEntity_ListID`),
  KEY `Template_ListID` (`Template_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `ShipAddress_Country` (`ShipAddress_Country`),
  KEY `Terms_ListID` (`Terms_ListID`),
  KEY `ShipMethod_ListID` (`ShipMethod_ListID`),
  KEY `Currency_ListID` (`Currency_ListID`),
  KEY `IsManuallyClosed` (`IsManuallyClosed`),
  KEY `IsFullyReceived` (`IsFullyReceived`),
  KEY `IsToBePrinted` (`IsToBePrinted`),
  KEY `IsToBeEmailed` (`IsToBeEmailed`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_purchaseorder` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_purchaseorder` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_purchaseorder` ADD  `Vendor_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Vendor_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipToEntity_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipToEntity_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Template_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Template_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Terms_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Terms_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `DueDate` date NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ExpectedDate` date NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ShipMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `FOB` varchar(13) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TotalAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Currency_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Currency_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `TotalAmountInHomeCurrency` decimal(10,2) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `IsManuallyClosed` tinyint(1) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `IsFullyReceived` tinyint(1) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `VendorMsg` varchar(99) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `IsToBePrinted` tinyint(1) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `IsToBeEmailed` tinyint(1) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Other1` varchar(25) NULL   ;
ALTER TABLE  `qb_purchaseorder` ADD  `Other2` varchar(29) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_purchaseorder_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Vendor_ListID;
	protected $Vendor_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $ShipToEntity_ListID;
	protected $ShipToEntity_FullName;
	protected $Template_ListID;
	protected $Template_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $VendorAddress_Addr1;
	protected $VendorAddress_Addr2;
	protected $VendorAddress_Addr3;
	protected $VendorAddress_Addr4;
	protected $VendorAddress_Addr5;
	protected $VendorAddress_City;
	protected $VendorAddress_State;
	protected $VendorAddress_PostalCode;
	protected $VendorAddress_Country;
	protected $VendorAddress_Note;
	protected $VendorAddressBlock_Addr1;
	protected $VendorAddressBlock_Addr2;
	protected $VendorAddressBlock_Addr3;
	protected $VendorAddressBlock_Addr4;
	protected $VendorAddressBlock_Addr5;
	protected $ShipAddress_Addr1;
	protected $ShipAddress_Addr2;
	protected $ShipAddress_Addr3;
	protected $ShipAddress_Addr4;
	protected $ShipAddress_Addr5;
	protected $ShipAddress_City;
	protected $ShipAddress_State;
	protected $ShipAddress_PostalCode;
	protected $ShipAddress_Country;
	protected $ShipAddress_Note;
	protected $ShipAddressBlock_Addr1;
	protected $ShipAddressBlock_Addr2;
	protected $ShipAddressBlock_Addr3;
	protected $ShipAddressBlock_Addr4;
	protected $ShipAddressBlock_Addr5;
	protected $Terms_ListID;
	protected $Terms_FullName;
	protected $DueDate;
	protected $ExpectedDate;
	protected $ShipMethod_ListID;
	protected $ShipMethod_FullName;
	protected $FOB;
	protected $TotalAmount;
	protected $Currency_ListID;
	protected $Currency_FullName;
	protected $ExchangeRate;
	protected $TotalAmountInHomeCurrency;
	protected $IsManuallyClosed;
	protected $IsFullyReceived;
	protected $Memo;
	protected $VendorMsg;
	protected $IsToBePrinted;
	protected $IsToBeEmailed;
	protected $Other1;
	protected $Other2;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_purchaseorder';
		$this->_short_name = 'qb_purchaseorder';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Vendor_ListID","Vendor_FullName","Class_ListID","Class_FullName","ShipToEntity_ListID","ShipToEntity_FullName","Template_ListID","Template_FullName","TxnDate","RefNumber","VendorAddress_Addr1","VendorAddress_Addr2","VendorAddress_Addr3","VendorAddress_Addr4","VendorAddress_Addr5","VendorAddress_City","VendorAddress_State","VendorAddress_PostalCode","VendorAddress_Country","VendorAddress_Note","VendorAddressBlock_Addr1","VendorAddressBlock_Addr2","VendorAddressBlock_Addr3","VendorAddressBlock_Addr4","VendorAddressBlock_Addr5","ShipAddress_Addr1","ShipAddress_Addr2","ShipAddress_Addr3","ShipAddress_Addr4","ShipAddress_Addr5","ShipAddress_City","ShipAddress_State","ShipAddress_PostalCode","ShipAddress_Country","ShipAddress_Note","ShipAddressBlock_Addr1","ShipAddressBlock_Addr2","ShipAddressBlock_Addr3","ShipAddressBlock_Addr4","ShipAddressBlock_Addr5","Terms_ListID","Terms_FullName","DueDate","ExpectedDate","ShipMethod_ListID","ShipMethod_FullName","FOB","TotalAmount","Currency_ListID","Currency_FullName","ExchangeRate","TotalAmountInHomeCurrency","IsManuallyClosed","IsFullyReceived","Memo","VendorMsg","IsToBePrinted","IsToBeEmailed","Other1","Other2");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Vendor_ListID -------------------------------------- 

	/** 
	* Sets a value to `Vendor_ListID` variable
	* @access public
	*/

	public function setVendorListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vendor_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vendor_ListID` variable
	* @access public
	*/

	public function getVendorListid() {
		return $this->Vendor_ListID;
	}

	public function get_Vendor_ListID() {
		return $this->Vendor_ListID;
	}

	
// ------------------------------ End Field: Vendor_ListID --------------------------------------


// ---------------------------- Start Field: Vendor_FullName -------------------------------------- 

	/** 
	* Sets a value to `Vendor_FullName` variable
	* @access public
	*/

	public function setVendorFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Vendor_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Vendor_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Vendor_FullName` variable
	* @access public
	*/

	public function getVendorFullname() {
		return $this->Vendor_FullName;
	}

	public function get_Vendor_FullName() {
		return $this->Vendor_FullName;
	}

	
// ------------------------------ End Field: Vendor_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: ShipToEntity_ListID -------------------------------------- 

	/** 
	* Sets a value to `ShipToEntity_ListID` variable
	* @access public
	*/

	public function setShiptoentityListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipToEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipToEntity_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipToEntity_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipToEntity_ListID` variable
	* @access public
	*/

	public function getShiptoentityListid() {
		return $this->ShipToEntity_ListID;
	}

	public function get_ShipToEntity_ListID() {
		return $this->ShipToEntity_ListID;
	}

	
// ------------------------------ End Field: ShipToEntity_ListID --------------------------------------


// ---------------------------- Start Field: ShipToEntity_FullName -------------------------------------- 

	/** 
	* Sets a value to `ShipToEntity_FullName` variable
	* @access public
	*/

	public function setShiptoentityFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipToEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipToEntity_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipToEntity_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipToEntity_FullName` variable
	* @access public
	*/

	public function getShiptoentityFullname() {
		return $this->ShipToEntity_FullName;
	}

	public function get_ShipToEntity_FullName() {
		return $this->ShipToEntity_FullName;
	}

	
// ------------------------------ End Field: ShipToEntity_FullName --------------------------------------


// ---------------------------- Start Field: Template_ListID -------------------------------------- 

	/** 
	* Sets a value to `Template_ListID` variable
	* @access public
	*/

	public function setTemplateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_ListID` variable
	* @access public
	*/

	public function getTemplateListid() {
		return $this->Template_ListID;
	}

	public function get_Template_ListID() {
		return $this->Template_ListID;
	}

	
// ------------------------------ End Field: Template_ListID --------------------------------------


// ---------------------------- Start Field: Template_FullName -------------------------------------- 

	/** 
	* Sets a value to `Template_FullName` variable
	* @access public
	*/

	public function setTemplateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_FullName` variable
	* @access public
	*/

	public function getTemplateFullname() {
		return $this->Template_FullName;
	}

	public function get_Template_FullName() {
		return $this->Template_FullName;
	}

	
// ------------------------------ End Field: Template_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr1` variable
	* @access public
	*/

	public function setVendoraddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr1` variable
	* @access public
	*/

	public function getVendoraddressAddr1() {
		return $this->VendorAddress_Addr1;
	}

	public function get_VendorAddress_Addr1() {
		return $this->VendorAddress_Addr1;
	}

	
// ------------------------------ End Field: VendorAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr2` variable
	* @access public
	*/

	public function setVendoraddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr2` variable
	* @access public
	*/

	public function getVendoraddressAddr2() {
		return $this->VendorAddress_Addr2;
	}

	public function get_VendorAddress_Addr2() {
		return $this->VendorAddress_Addr2;
	}

	
// ------------------------------ End Field: VendorAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr3` variable
	* @access public
	*/

	public function setVendoraddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr3` variable
	* @access public
	*/

	public function getVendoraddressAddr3() {
		return $this->VendorAddress_Addr3;
	}

	public function get_VendorAddress_Addr3() {
		return $this->VendorAddress_Addr3;
	}

	
// ------------------------------ End Field: VendorAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr4` variable
	* @access public
	*/

	public function setVendoraddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr4` variable
	* @access public
	*/

	public function getVendoraddressAddr4() {
		return $this->VendorAddress_Addr4;
	}

	public function get_VendorAddress_Addr4() {
		return $this->VendorAddress_Addr4;
	}

	
// ------------------------------ End Field: VendorAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: VendorAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Addr5` variable
	* @access public
	*/

	public function setVendoraddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Addr5` variable
	* @access public
	*/

	public function getVendoraddressAddr5() {
		return $this->VendorAddress_Addr5;
	}

	public function get_VendorAddress_Addr5() {
		return $this->VendorAddress_Addr5;
	}

	
// ------------------------------ End Field: VendorAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: VendorAddress_City -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_City` variable
	* @access public
	*/

	public function setVendoraddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_City` variable
	* @access public
	*/

	public function getVendoraddressCity() {
		return $this->VendorAddress_City;
	}

	public function get_VendorAddress_City() {
		return $this->VendorAddress_City;
	}

	
// ------------------------------ End Field: VendorAddress_City --------------------------------------


// ---------------------------- Start Field: VendorAddress_State -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_State` variable
	* @access public
	*/

	public function setVendoraddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_State` variable
	* @access public
	*/

	public function getVendoraddressState() {
		return $this->VendorAddress_State;
	}

	public function get_VendorAddress_State() {
		return $this->VendorAddress_State;
	}

	
// ------------------------------ End Field: VendorAddress_State --------------------------------------


// ---------------------------- Start Field: VendorAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_PostalCode` variable
	* @access public
	*/

	public function setVendoraddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_PostalCode` variable
	* @access public
	*/

	public function getVendoraddressPostalcode() {
		return $this->VendorAddress_PostalCode;
	}

	public function get_VendorAddress_PostalCode() {
		return $this->VendorAddress_PostalCode;
	}

	
// ------------------------------ End Field: VendorAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: VendorAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Country` variable
	* @access public
	*/

	public function setVendoraddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Country` variable
	* @access public
	*/

	public function getVendoraddressCountry() {
		return $this->VendorAddress_Country;
	}

	public function get_VendorAddress_Country() {
		return $this->VendorAddress_Country;
	}

	
// ------------------------------ End Field: VendorAddress_Country --------------------------------------


// ---------------------------- Start Field: VendorAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `VendorAddress_Note` variable
	* @access public
	*/

	public function setVendoraddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddress_Note` variable
	* @access public
	*/

	public function getVendoraddressNote() {
		return $this->VendorAddress_Note;
	}

	public function get_VendorAddress_Note() {
		return $this->VendorAddress_Note;
	}

	
// ------------------------------ End Field: VendorAddress_Note --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr1` variable
	* @access public
	*/

	public function setVendoraddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr1` variable
	* @access public
	*/

	public function getVendoraddressblockAddr1() {
		return $this->VendorAddressBlock_Addr1;
	}

	public function get_VendorAddressBlock_Addr1() {
		return $this->VendorAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr2` variable
	* @access public
	*/

	public function setVendoraddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr2` variable
	* @access public
	*/

	public function getVendoraddressblockAddr2() {
		return $this->VendorAddressBlock_Addr2;
	}

	public function get_VendorAddressBlock_Addr2() {
		return $this->VendorAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr3` variable
	* @access public
	*/

	public function setVendoraddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr3` variable
	* @access public
	*/

	public function getVendoraddressblockAddr3() {
		return $this->VendorAddressBlock_Addr3;
	}

	public function get_VendorAddressBlock_Addr3() {
		return $this->VendorAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr4` variable
	* @access public
	*/

	public function setVendoraddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr4` variable
	* @access public
	*/

	public function getVendoraddressblockAddr4() {
		return $this->VendorAddressBlock_Addr4;
	}

	public function get_VendorAddressBlock_Addr4() {
		return $this->VendorAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: VendorAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `VendorAddressBlock_Addr5` variable
	* @access public
	*/

	public function setVendoraddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorAddressBlock_Addr5` variable
	* @access public
	*/

	public function getVendoraddressblockAddr5() {
		return $this->VendorAddressBlock_Addr5;
	}

	public function get_VendorAddressBlock_Addr5() {
		return $this->VendorAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: VendorAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr1` variable
	* @access public
	*/

	public function setShipaddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr1` variable
	* @access public
	*/

	public function getShipaddressAddr1() {
		return $this->ShipAddress_Addr1;
	}

	public function get_ShipAddress_Addr1() {
		return $this->ShipAddress_Addr1;
	}

	
// ------------------------------ End Field: ShipAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr2` variable
	* @access public
	*/

	public function setShipaddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr2` variable
	* @access public
	*/

	public function getShipaddressAddr2() {
		return $this->ShipAddress_Addr2;
	}

	public function get_ShipAddress_Addr2() {
		return $this->ShipAddress_Addr2;
	}

	
// ------------------------------ End Field: ShipAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr3` variable
	* @access public
	*/

	public function setShipaddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr3` variable
	* @access public
	*/

	public function getShipaddressAddr3() {
		return $this->ShipAddress_Addr3;
	}

	public function get_ShipAddress_Addr3() {
		return $this->ShipAddress_Addr3;
	}

	
// ------------------------------ End Field: ShipAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr4` variable
	* @access public
	*/

	public function setShipaddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr4` variable
	* @access public
	*/

	public function getShipaddressAddr4() {
		return $this->ShipAddress_Addr4;
	}

	public function get_ShipAddress_Addr4() {
		return $this->ShipAddress_Addr4;
	}

	
// ------------------------------ End Field: ShipAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr5` variable
	* @access public
	*/

	public function setShipaddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr5` variable
	* @access public
	*/

	public function getShipaddressAddr5() {
		return $this->ShipAddress_Addr5;
	}

	public function get_ShipAddress_Addr5() {
		return $this->ShipAddress_Addr5;
	}

	
// ------------------------------ End Field: ShipAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_City -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_City` variable
	* @access public
	*/

	public function setShipaddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_City` variable
	* @access public
	*/

	public function getShipaddressCity() {
		return $this->ShipAddress_City;
	}

	public function get_ShipAddress_City() {
		return $this->ShipAddress_City;
	}

	
// ------------------------------ End Field: ShipAddress_City --------------------------------------


// ---------------------------- Start Field: ShipAddress_State -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_State` variable
	* @access public
	*/

	public function setShipaddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_State` variable
	* @access public
	*/

	public function getShipaddressState() {
		return $this->ShipAddress_State;
	}

	public function get_ShipAddress_State() {
		return $this->ShipAddress_State;
	}

	
// ------------------------------ End Field: ShipAddress_State --------------------------------------


// ---------------------------- Start Field: ShipAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function setShipaddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function getShipaddressPostalcode() {
		return $this->ShipAddress_PostalCode;
	}

	public function get_ShipAddress_PostalCode() {
		return $this->ShipAddress_PostalCode;
	}

	
// ------------------------------ End Field: ShipAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: ShipAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Country` variable
	* @access public
	*/

	public function setShipaddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Country` variable
	* @access public
	*/

	public function getShipaddressCountry() {
		return $this->ShipAddress_Country;
	}

	public function get_ShipAddress_Country() {
		return $this->ShipAddress_Country;
	}

	
// ------------------------------ End Field: ShipAddress_Country --------------------------------------


// ---------------------------- Start Field: ShipAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Note` variable
	* @access public
	*/

	public function setShipaddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Note` variable
	* @access public
	*/

	public function getShipaddressNote() {
		return $this->ShipAddress_Note;
	}

	public function get_ShipAddress_Note() {
		return $this->ShipAddress_Note;
	}

	
// ------------------------------ End Field: ShipAddress_Note --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function setShipaddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function getShipaddressblockAddr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	public function get_ShipAddressBlock_Addr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function setShipaddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function getShipaddressblockAddr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	public function get_ShipAddressBlock_Addr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function setShipaddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function getShipaddressblockAddr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	public function get_ShipAddressBlock_Addr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function setShipaddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function getShipaddressblockAddr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	public function get_ShipAddressBlock_Addr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function setShipaddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function getShipaddressblockAddr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	public function get_ShipAddressBlock_Addr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: Terms_ListID -------------------------------------- 

	/** 
	* Sets a value to `Terms_ListID` variable
	* @access public
	*/

	public function setTermsListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_ListID` variable
	* @access public
	*/

	public function getTermsListid() {
		return $this->Terms_ListID;
	}

	public function get_Terms_ListID() {
		return $this->Terms_ListID;
	}

	
// ------------------------------ End Field: Terms_ListID --------------------------------------


// ---------------------------- Start Field: Terms_FullName -------------------------------------- 

	/** 
	* Sets a value to `Terms_FullName` variable
	* @access public
	*/

	public function setTermsFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_FullName` variable
	* @access public
	*/

	public function getTermsFullname() {
		return $this->Terms_FullName;
	}

	public function get_Terms_FullName() {
		return $this->Terms_FullName;
	}

	
// ------------------------------ End Field: Terms_FullName --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------


// ---------------------------- Start Field: ExpectedDate -------------------------------------- 

	/** 
	* Sets a value to `ExpectedDate` variable
	* @access public
	*/

	public function setExpecteddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExpectedDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExpectedDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExpectedDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExpectedDate` variable
	* @access public
	*/

	public function getExpecteddate() {
		return $this->ExpectedDate;
	}

	public function get_ExpectedDate() {
		return $this->ExpectedDate;
	}

	
// ------------------------------ End Field: ExpectedDate --------------------------------------


// ---------------------------- Start Field: ShipMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_ListID` variable
	* @access public
	*/

	public function setShipmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_ListID` variable
	* @access public
	*/

	public function getShipmethodListid() {
		return $this->ShipMethod_ListID;
	}

	public function get_ShipMethod_ListID() {
		return $this->ShipMethod_ListID;
	}

	
// ------------------------------ End Field: ShipMethod_ListID --------------------------------------


// ---------------------------- Start Field: ShipMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_FullName` variable
	* @access public
	*/

	public function setShipmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_FullName` variable
	* @access public
	*/

	public function getShipmethodFullname() {
		return $this->ShipMethod_FullName;
	}

	public function get_ShipMethod_FullName() {
		return $this->ShipMethod_FullName;
	}

	
// ------------------------------ End Field: ShipMethod_FullName --------------------------------------


// ---------------------------- Start Field: FOB -------------------------------------- 

	/** 
	* Sets a value to `FOB` variable
	* @access public
	*/

	public function setFob($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FOB($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FOB` variable
	* @access public
	*/

	public function getFob() {
		return $this->FOB;
	}

	public function get_FOB() {
		return $this->FOB;
	}

	
// ------------------------------ End Field: FOB --------------------------------------


// ---------------------------- Start Field: TotalAmount -------------------------------------- 

	/** 
	* Sets a value to `TotalAmount` variable
	* @access public
	*/

	public function setTotalamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmount` variable
	* @access public
	*/

	public function getTotalamount() {
		return $this->TotalAmount;
	}

	public function get_TotalAmount() {
		return $this->TotalAmount;
	}

	
// ------------------------------ End Field: TotalAmount --------------------------------------


// ---------------------------- Start Field: Currency_ListID -------------------------------------- 

	/** 
	* Sets a value to `Currency_ListID` variable
	* @access public
	*/

	public function setCurrencyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_ListID` variable
	* @access public
	*/

	public function getCurrencyListid() {
		return $this->Currency_ListID;
	}

	public function get_Currency_ListID() {
		return $this->Currency_ListID;
	}

	
// ------------------------------ End Field: Currency_ListID --------------------------------------


// ---------------------------- Start Field: Currency_FullName -------------------------------------- 

	/** 
	* Sets a value to `Currency_FullName` variable
	* @access public
	*/

	public function setCurrencyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_FullName` variable
	* @access public
	*/

	public function getCurrencyFullname() {
		return $this->Currency_FullName;
	}

	public function get_Currency_FullName() {
		return $this->Currency_FullName;
	}

	
// ------------------------------ End Field: Currency_FullName --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: TotalAmountInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `TotalAmountInHomeCurrency` variable
	* @access public
	*/

	public function setTotalamountinhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TotalAmountInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TotalAmountInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TotalAmountInHomeCurrency` variable
	* @access public
	*/

	public function getTotalamountinhomecurrency() {
		return $this->TotalAmountInHomeCurrency;
	}

	public function get_TotalAmountInHomeCurrency() {
		return $this->TotalAmountInHomeCurrency;
	}

	
// ------------------------------ End Field: TotalAmountInHomeCurrency --------------------------------------


// ---------------------------- Start Field: IsManuallyClosed -------------------------------------- 

	/** 
	* Sets a value to `IsManuallyClosed` variable
	* @access public
	*/

	public function setIsmanuallyclosed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsManuallyClosed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsManuallyClosed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsManuallyClosed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsManuallyClosed` variable
	* @access public
	*/

	public function getIsmanuallyclosed() {
		return $this->IsManuallyClosed;
	}

	public function get_IsManuallyClosed() {
		return $this->IsManuallyClosed;
	}

	
// ------------------------------ End Field: IsManuallyClosed --------------------------------------


// ---------------------------- Start Field: IsFullyReceived -------------------------------------- 

	/** 
	* Sets a value to `IsFullyReceived` variable
	* @access public
	*/

	public function setIsfullyreceived($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsFullyReceived', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsFullyReceived($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsFullyReceived', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsFullyReceived` variable
	* @access public
	*/

	public function getIsfullyreceived() {
		return $this->IsFullyReceived;
	}

	public function get_IsFullyReceived() {
		return $this->IsFullyReceived;
	}

	
// ------------------------------ End Field: IsFullyReceived --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: VendorMsg -------------------------------------- 

	/** 
	* Sets a value to `VendorMsg` variable
	* @access public
	*/

	public function setVendormsg($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorMsg', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_VendorMsg($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('VendorMsg', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `VendorMsg` variable
	* @access public
	*/

	public function getVendormsg() {
		return $this->VendorMsg;
	}

	public function get_VendorMsg() {
		return $this->VendorMsg;
	}

	
// ------------------------------ End Field: VendorMsg --------------------------------------


// ---------------------------- Start Field: IsToBePrinted -------------------------------------- 

	/** 
	* Sets a value to `IsToBePrinted` variable
	* @access public
	*/

	public function setIstobeprinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBePrinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBePrinted` variable
	* @access public
	*/

	public function getIstobeprinted() {
		return $this->IsToBePrinted;
	}

	public function get_IsToBePrinted() {
		return $this->IsToBePrinted;
	}

	
// ------------------------------ End Field: IsToBePrinted --------------------------------------


// ---------------------------- Start Field: IsToBeEmailed -------------------------------------- 

	/** 
	* Sets a value to `IsToBeEmailed` variable
	* @access public
	*/

	public function setIstobeemailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBeEmailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBeEmailed` variable
	* @access public
	*/

	public function getIstobeemailed() {
		return $this->IsToBeEmailed;
	}

	public function get_IsToBeEmailed() {
		return $this->IsToBeEmailed;
	}

	
// ------------------------------ End Field: IsToBeEmailed --------------------------------------


// ---------------------------- Start Field: Other1 -------------------------------------- 

	/** 
	* Sets a value to `Other1` variable
	* @access public
	*/

	public function setOther1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other1` variable
	* @access public
	*/

	public function getOther1() {
		return $this->Other1;
	}

	public function get_Other1() {
		return $this->Other1;
	}

	
// ------------------------------ End Field: Other1 --------------------------------------


// ---------------------------- Start Field: Other2 -------------------------------------- 

	/** 
	* Sets a value to `Other2` variable
	* @access public
	*/

	public function setOther2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other2` variable
	* @access public
	*/

	public function getOther2() {
		return $this->Other2;
	}

	public function get_Other2() {
		return $this->Other2;
	}

	
// ------------------------------ End Field: Other2 --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Vendor_ListID' => (object) array(
										'Field'=>'Vendor_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Vendor_FullName' => (object) array(
										'Field'=>'Vendor_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipToEntity_ListID' => (object) array(
										'Field'=>'ShipToEntity_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipToEntity_FullName' => (object) array(
										'Field'=>'ShipToEntity_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_ListID' => (object) array(
										'Field'=>'Template_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_FullName' => (object) array(
										'Field'=>'Template_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr1' => (object) array(
										'Field'=>'VendorAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr2' => (object) array(
										'Field'=>'VendorAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr3' => (object) array(
										'Field'=>'VendorAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr4' => (object) array(
										'Field'=>'VendorAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Addr5' => (object) array(
										'Field'=>'VendorAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_City' => (object) array(
										'Field'=>'VendorAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_State' => (object) array(
										'Field'=>'VendorAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_PostalCode' => (object) array(
										'Field'=>'VendorAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Country' => (object) array(
										'Field'=>'VendorAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddress_Note' => (object) array(
										'Field'=>'VendorAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr1' => (object) array(
										'Field'=>'VendorAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr2' => (object) array(
										'Field'=>'VendorAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr3' => (object) array(
										'Field'=>'VendorAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr4' => (object) array(
										'Field'=>'VendorAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorAddressBlock_Addr5' => (object) array(
										'Field'=>'VendorAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr1' => (object) array(
										'Field'=>'ShipAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr2' => (object) array(
										'Field'=>'ShipAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr3' => (object) array(
										'Field'=>'ShipAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr4' => (object) array(
										'Field'=>'ShipAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr5' => (object) array(
										'Field'=>'ShipAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_City' => (object) array(
										'Field'=>'ShipAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_State' => (object) array(
										'Field'=>'ShipAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_PostalCode' => (object) array(
										'Field'=>'ShipAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Country' => (object) array(
										'Field'=>'ShipAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Note' => (object) array(
										'Field'=>'ShipAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr1' => (object) array(
										'Field'=>'ShipAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr2' => (object) array(
										'Field'=>'ShipAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr3' => (object) array(
										'Field'=>'ShipAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr4' => (object) array(
										'Field'=>'ShipAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr5' => (object) array(
										'Field'=>'ShipAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_ListID' => (object) array(
										'Field'=>'Terms_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_FullName' => (object) array(
										'Field'=>'Terms_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExpectedDate' => (object) array(
										'Field'=>'ExpectedDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_ListID' => (object) array(
										'Field'=>'ShipMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_FullName' => (object) array(
										'Field'=>'ShipMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FOB' => (object) array(
										'Field'=>'FOB',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalAmount' => (object) array(
										'Field'=>'TotalAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_ListID' => (object) array(
										'Field'=>'Currency_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_FullName' => (object) array(
										'Field'=>'Currency_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TotalAmountInHomeCurrency' => (object) array(
										'Field'=>'TotalAmountInHomeCurrency',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsManuallyClosed' => (object) array(
										'Field'=>'IsManuallyClosed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsFullyReceived' => (object) array(
										'Field'=>'IsFullyReceived',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'VendorMsg' => (object) array(
										'Field'=>'VendorMsg',
										'Type'=>'varchar(99)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBePrinted' => (object) array(
										'Field'=>'IsToBePrinted',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBeEmailed' => (object) array(
										'Field'=>'IsToBeEmailed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Other1' => (object) array(
										'Field'=>'Other1',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other2' => (object) array(
										'Field'=>'Other2',
										'Type'=>'varchar(29)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_purchaseorder` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_purchaseorder` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_purchaseorder` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_purchaseorder` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_purchaseorder` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_purchaseorder` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Vendor_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `Vendor_ListID` varchar(40) NULL   ;",
			'Vendor_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `Vendor_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `Class_FullName` varchar(255) NULL   ;",
			'ShipToEntity_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipToEntity_ListID` varchar(40) NULL   ;",
			'ShipToEntity_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipToEntity_FullName` varchar(255) NULL   ;",
			'Template_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `Template_ListID` varchar(40) NULL   ;",
			'Template_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `Template_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_purchaseorder` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_purchaseorder` ADD  `RefNumber` varchar(11) NULL   ;",
			'VendorAddress_Addr1' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr1` varchar(41) NULL   ;",
			'VendorAddress_Addr2' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr2` varchar(41) NULL   ;",
			'VendorAddress_Addr3' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr3` varchar(41) NULL   ;",
			'VendorAddress_Addr4' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr4` varchar(41) NULL   ;",
			'VendorAddress_Addr5' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Addr5` varchar(41) NULL   ;",
			'VendorAddress_City' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_City` varchar(31) NULL   ;",
			'VendorAddress_State' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_State` varchar(21) NULL   ;",
			'VendorAddress_PostalCode' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_PostalCode` varchar(13) NULL   ;",
			'VendorAddress_Country' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Country` varchar(31) NULL   ;",
			'VendorAddress_Note' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddress_Note` varchar(41) NULL   ;",
			'VendorAddressBlock_Addr1' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr1` text NULL   ;",
			'VendorAddressBlock_Addr2' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr2` text NULL   ;",
			'VendorAddressBlock_Addr3' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr3` text NULL   ;",
			'VendorAddressBlock_Addr4' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr4` text NULL   ;",
			'VendorAddressBlock_Addr5' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorAddressBlock_Addr5` text NULL   ;",
			'ShipAddress_Addr1' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;",
			'ShipAddress_Addr2' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;",
			'ShipAddress_Addr3' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;",
			'ShipAddress_Addr4' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;",
			'ShipAddress_Addr5' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;",
			'ShipAddress_City' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_City` varchar(31) NULL   ;",
			'ShipAddress_State' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_State` varchar(21) NULL   ;",
			'ShipAddress_PostalCode' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;",
			'ShipAddress_Country' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Country` varchar(31) NULL   ;",
			'ShipAddress_Note' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddress_Note` varchar(41) NULL   ;",
			'ShipAddressBlock_Addr1' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr1` text NULL   ;",
			'ShipAddressBlock_Addr2' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr2` text NULL   ;",
			'ShipAddressBlock_Addr3' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr3` text NULL   ;",
			'ShipAddressBlock_Addr4' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr4` text NULL   ;",
			'ShipAddressBlock_Addr5' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipAddressBlock_Addr5` text NULL   ;",
			'Terms_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `Terms_ListID` varchar(40) NULL   ;",
			'Terms_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `Terms_FullName` varchar(255) NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_purchaseorder` ADD  `DueDate` date NULL   ;",
			'ExpectedDate' => "ALTER TABLE  `qb_purchaseorder` ADD  `ExpectedDate` date NULL   ;",
			'ShipMethod_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipMethod_ListID` varchar(40) NULL   ;",
			'ShipMethod_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `ShipMethod_FullName` varchar(255) NULL   ;",
			'FOB' => "ALTER TABLE  `qb_purchaseorder` ADD  `FOB` varchar(13) NULL   ;",
			'TotalAmount' => "ALTER TABLE  `qb_purchaseorder` ADD  `TotalAmount` decimal(10,2) NULL   ;",
			'Currency_ListID' => "ALTER TABLE  `qb_purchaseorder` ADD  `Currency_ListID` varchar(40) NULL   ;",
			'Currency_FullName' => "ALTER TABLE  `qb_purchaseorder` ADD  `Currency_FullName` varchar(255) NULL   ;",
			'ExchangeRate' => "ALTER TABLE  `qb_purchaseorder` ADD  `ExchangeRate` text NULL   ;",
			'TotalAmountInHomeCurrency' => "ALTER TABLE  `qb_purchaseorder` ADD  `TotalAmountInHomeCurrency` decimal(10,2) NULL   ;",
			'IsManuallyClosed' => "ALTER TABLE  `qb_purchaseorder` ADD  `IsManuallyClosed` tinyint(1) NULL   ;",
			'IsFullyReceived' => "ALTER TABLE  `qb_purchaseorder` ADD  `IsFullyReceived` tinyint(1) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_purchaseorder` ADD  `Memo` text NULL   ;",
			'VendorMsg' => "ALTER TABLE  `qb_purchaseorder` ADD  `VendorMsg` varchar(99) NULL   ;",
			'IsToBePrinted' => "ALTER TABLE  `qb_purchaseorder` ADD  `IsToBePrinted` tinyint(1) NULL   ;",
			'IsToBeEmailed' => "ALTER TABLE  `qb_purchaseorder` ADD  `IsToBeEmailed` tinyint(1) NULL   ;",
			'Other1' => "ALTER TABLE  `qb_purchaseorder` ADD  `Other1` varchar(25) NULL   ;",
			'Other2' => "ALTER TABLE  `qb_purchaseorder` ADD  `Other2` varchar(29) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setVendorListid() - Vendor_ListID
//setVendorFullname() - Vendor_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setShiptoentityListid() - ShipToEntity_ListID
//setShiptoentityFullname() - ShipToEntity_FullName
//setTemplateListid() - Template_ListID
//setTemplateFullname() - Template_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setVendoraddressAddr1() - VendorAddress_Addr1
//setVendoraddressAddr2() - VendorAddress_Addr2
//setVendoraddressAddr3() - VendorAddress_Addr3
//setVendoraddressAddr4() - VendorAddress_Addr4
//setVendoraddressAddr5() - VendorAddress_Addr5
//setVendoraddressCity() - VendorAddress_City
//setVendoraddressState() - VendorAddress_State
//setVendoraddressPostalcode() - VendorAddress_PostalCode
//setVendoraddressCountry() - VendorAddress_Country
//setVendoraddressNote() - VendorAddress_Note
//setVendoraddressblockAddr1() - VendorAddressBlock_Addr1
//setVendoraddressblockAddr2() - VendorAddressBlock_Addr2
//setVendoraddressblockAddr3() - VendorAddressBlock_Addr3
//setVendoraddressblockAddr4() - VendorAddressBlock_Addr4
//setVendoraddressblockAddr5() - VendorAddressBlock_Addr5
//setShipaddressAddr1() - ShipAddress_Addr1
//setShipaddressAddr2() - ShipAddress_Addr2
//setShipaddressAddr3() - ShipAddress_Addr3
//setShipaddressAddr4() - ShipAddress_Addr4
//setShipaddressAddr5() - ShipAddress_Addr5
//setShipaddressCity() - ShipAddress_City
//setShipaddressState() - ShipAddress_State
//setShipaddressPostalcode() - ShipAddress_PostalCode
//setShipaddressCountry() - ShipAddress_Country
//setShipaddressNote() - ShipAddress_Note
//setShipaddressblockAddr1() - ShipAddressBlock_Addr1
//setShipaddressblockAddr2() - ShipAddressBlock_Addr2
//setShipaddressblockAddr3() - ShipAddressBlock_Addr3
//setShipaddressblockAddr4() - ShipAddressBlock_Addr4
//setShipaddressblockAddr5() - ShipAddressBlock_Addr5
//setTermsListid() - Terms_ListID
//setTermsFullname() - Terms_FullName
//setDuedate() - DueDate
//setExpecteddate() - ExpectedDate
//setShipmethodListid() - ShipMethod_ListID
//setShipmethodFullname() - ShipMethod_FullName
//setFob() - FOB
//setTotalamount() - TotalAmount
//setCurrencyListid() - Currency_ListID
//setCurrencyFullname() - Currency_FullName
//setExchangerate() - ExchangeRate
//setTotalamountinhomecurrency() - TotalAmountInHomeCurrency
//setIsmanuallyclosed() - IsManuallyClosed
//setIsfullyreceived() - IsFullyReceived
//setMemo() - Memo
//setVendormsg() - VendorMsg
//setIstobeprinted() - IsToBePrinted
//setIstobeemailed() - IsToBeEmailed
//setOther1() - Other1
//setOther2() - Other2

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Vendor_ListID() - Vendor_ListID
//set_Vendor_FullName() - Vendor_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_ShipToEntity_ListID() - ShipToEntity_ListID
//set_ShipToEntity_FullName() - ShipToEntity_FullName
//set_Template_ListID() - Template_ListID
//set_Template_FullName() - Template_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_VendorAddress_Addr1() - VendorAddress_Addr1
//set_VendorAddress_Addr2() - VendorAddress_Addr2
//set_VendorAddress_Addr3() - VendorAddress_Addr3
//set_VendorAddress_Addr4() - VendorAddress_Addr4
//set_VendorAddress_Addr5() - VendorAddress_Addr5
//set_VendorAddress_City() - VendorAddress_City
//set_VendorAddress_State() - VendorAddress_State
//set_VendorAddress_PostalCode() - VendorAddress_PostalCode
//set_VendorAddress_Country() - VendorAddress_Country
//set_VendorAddress_Note() - VendorAddress_Note
//set_VendorAddressBlock_Addr1() - VendorAddressBlock_Addr1
//set_VendorAddressBlock_Addr2() - VendorAddressBlock_Addr2
//set_VendorAddressBlock_Addr3() - VendorAddressBlock_Addr3
//set_VendorAddressBlock_Addr4() - VendorAddressBlock_Addr4
//set_VendorAddressBlock_Addr5() - VendorAddressBlock_Addr5
//set_ShipAddress_Addr1() - ShipAddress_Addr1
//set_ShipAddress_Addr2() - ShipAddress_Addr2
//set_ShipAddress_Addr3() - ShipAddress_Addr3
//set_ShipAddress_Addr4() - ShipAddress_Addr4
//set_ShipAddress_Addr5() - ShipAddress_Addr5
//set_ShipAddress_City() - ShipAddress_City
//set_ShipAddress_State() - ShipAddress_State
//set_ShipAddress_PostalCode() - ShipAddress_PostalCode
//set_ShipAddress_Country() - ShipAddress_Country
//set_ShipAddress_Note() - ShipAddress_Note
//set_ShipAddressBlock_Addr1() - ShipAddressBlock_Addr1
//set_ShipAddressBlock_Addr2() - ShipAddressBlock_Addr2
//set_ShipAddressBlock_Addr3() - ShipAddressBlock_Addr3
//set_ShipAddressBlock_Addr4() - ShipAddressBlock_Addr4
//set_ShipAddressBlock_Addr5() - ShipAddressBlock_Addr5
//set_Terms_ListID() - Terms_ListID
//set_Terms_FullName() - Terms_FullName
//set_DueDate() - DueDate
//set_ExpectedDate() - ExpectedDate
//set_ShipMethod_ListID() - ShipMethod_ListID
//set_ShipMethod_FullName() - ShipMethod_FullName
//set_FOB() - FOB
//set_TotalAmount() - TotalAmount
//set_Currency_ListID() - Currency_ListID
//set_Currency_FullName() - Currency_FullName
//set_ExchangeRate() - ExchangeRate
//set_TotalAmountInHomeCurrency() - TotalAmountInHomeCurrency
//set_IsManuallyClosed() - IsManuallyClosed
//set_IsFullyReceived() - IsFullyReceived
//set_Memo() - Memo
//set_VendorMsg() - VendorMsg
//set_IsToBePrinted() - IsToBePrinted
//set_IsToBeEmailed() - IsToBeEmailed
//set_Other1() - Other1
//set_Other2() - Other2

*/
/* End of file Qb_purchaseorder_model.php */
/* Location: ./application/models/Qb_purchaseorder_model.php */
