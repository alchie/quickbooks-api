<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_invoice_model Class
 *
 * Manipulates `qb_invoice` table on database

CREATE TABLE `qb_invoice` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `ARAccount_ListID` varchar(40) DEFAULT NULL,
  `ARAccount_FullName` varchar(255) DEFAULT NULL,
  `Template_ListID` varchar(40) DEFAULT NULL,
  `Template_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` varchar(11) DEFAULT NULL,
  `BillAddress_Addr1` varchar(41) DEFAULT NULL,
  `BillAddress_Addr2` varchar(41) DEFAULT NULL,
  `BillAddress_Addr3` varchar(41) DEFAULT NULL,
  `BillAddress_Addr4` varchar(41) DEFAULT NULL,
  `BillAddress_Addr5` varchar(41) DEFAULT NULL,
  `BillAddress_City` varchar(31) DEFAULT NULL,
  `BillAddress_State` varchar(21) DEFAULT NULL,
  `BillAddress_PostalCode` varchar(13) DEFAULT NULL,
  `BillAddress_Country` varchar(31) DEFAULT NULL,
  `BillAddress_Note` varchar(41) DEFAULT NULL,
  `BillAddressBlock_Addr1` text,
  `BillAddressBlock_Addr2` text,
  `BillAddressBlock_Addr3` text,
  `BillAddressBlock_Addr4` text,
  `BillAddressBlock_Addr5` text,
  `ShipAddress_Addr1` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr2` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr3` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr4` varchar(41) DEFAULT NULL,
  `ShipAddress_Addr5` varchar(41) DEFAULT NULL,
  `ShipAddress_City` varchar(31) DEFAULT NULL,
  `ShipAddress_State` varchar(21) DEFAULT NULL,
  `ShipAddress_PostalCode` varchar(13) DEFAULT NULL,
  `ShipAddress_Country` varchar(31) DEFAULT NULL,
  `ShipAddress_Note` varchar(41) DEFAULT NULL,
  `ShipAddressBlock_Addr1` text,
  `ShipAddressBlock_Addr2` text,
  `ShipAddressBlock_Addr3` text,
  `ShipAddressBlock_Addr4` text,
  `ShipAddressBlock_Addr5` text,
  `IsPending` tinyint(1) DEFAULT NULL,
  `IsFinanceCharge` tinyint(1) DEFAULT NULL,
  `PONumber` varchar(25) DEFAULT NULL,
  `Terms_ListID` varchar(40) DEFAULT NULL,
  `Terms_FullName` varchar(255) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `SalesRep_ListID` varchar(40) DEFAULT NULL,
  `SalesRep_FullName` varchar(255) DEFAULT NULL,
  `FOB` varchar(13) DEFAULT NULL,
  `ShipDate` date DEFAULT NULL,
  `ShipMethod_ListID` varchar(40) DEFAULT NULL,
  `ShipMethod_FullName` varchar(255) DEFAULT NULL,
  `Subtotal` decimal(10,2) DEFAULT NULL,
  `ItemSalesTax_ListID` varchar(40) DEFAULT NULL,
  `ItemSalesTax_FullName` varchar(255) DEFAULT NULL,
  `SalesTaxPercentage` decimal(12,5) DEFAULT NULL,
  `SalesTaxTotal` decimal(10,2) DEFAULT NULL,
  `AppliedAmount` decimal(10,2) DEFAULT NULL,
  `BalanceRemaining` decimal(10,2) DEFAULT NULL,
  `Memo` text,
  `IsPaid` tinyint(1) DEFAULT NULL,
  `Currency_ListID` varchar(40) DEFAULT NULL,
  `Currency_FullName` varchar(255) DEFAULT NULL,
  `ExchangeRate` text,
  `BalanceRemainingInHomeCurrency` decimal(10,2) DEFAULT NULL,
  `CustomerMsg_ListID` varchar(40) DEFAULT NULL,
  `CustomerMsg_FullName` varchar(255) DEFAULT NULL,
  `IsToBePrinted` tinyint(1) DEFAULT NULL,
  `IsToBeEmailed` tinyint(1) DEFAULT NULL,
  `CustomerSalesTaxCode_ListID` varchar(40) DEFAULT NULL,
  `CustomerSalesTaxCode_FullName` varchar(255) DEFAULT NULL,
  `SuggestedDiscountAmount` decimal(10,2) DEFAULT NULL,
  `SuggestedDiscountDate` date DEFAULT NULL,
  `Other` varchar(29) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `ARAccount_ListID` (`ARAccount_ListID`),
  KEY `Template_ListID` (`Template_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `RefNumber` (`RefNumber`),
  KEY `BillAddress_Country` (`BillAddress_Country`),
  KEY `ShipAddress_Country` (`ShipAddress_Country`),
  KEY `IsPending` (`IsPending`),
  KEY `Terms_ListID` (`Terms_ListID`),
  KEY `SalesRep_ListID` (`SalesRep_ListID`),
  KEY `ShipMethod_ListID` (`ShipMethod_ListID`),
  KEY `ItemSalesTax_ListID` (`ItemSalesTax_ListID`),
  KEY `IsPaid` (`IsPaid`),
  KEY `Currency_ListID` (`Currency_ListID`),
  KEY `CustomerMsg_ListID` (`CustomerMsg_ListID`),
  KEY `IsToBePrinted` (`IsToBePrinted`),
  KEY `IsToBeEmailed` (`IsToBeEmailed`),
  KEY `CustomerSalesTaxCode_ListID` (`CustomerSalesTaxCode_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_invoice` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_invoice` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_invoice` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_invoice` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_invoice` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ARAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ARAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Template_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Template_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_invoice` ADD  `RefNumber` varchar(11) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_City` varchar(31) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_State` varchar(21) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Country` varchar(31) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Note` varchar(41) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr1` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr2` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr3` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr4` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr5` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `IsPending` tinyint(1) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `IsFinanceCharge` tinyint(1) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `PONumber` varchar(25) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Terms_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Terms_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `DueDate` date NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SalesRep_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SalesRep_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `FOB` varchar(13) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipDate` date NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipMethod_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ShipMethod_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Subtotal` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SalesTaxPercentage` decimal(12,5) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SalesTaxTotal` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `AppliedAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BalanceRemaining` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Memo` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `IsPaid` tinyint(1) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Currency_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Currency_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_invoice` ADD  `BalanceRemainingInHomeCurrency` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `CustomerMsg_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `CustomerMsg_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `IsToBePrinted` tinyint(1) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `IsToBeEmailed` tinyint(1) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `CustomerSalesTaxCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `CustomerSalesTaxCode_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SuggestedDiscountAmount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_invoice` ADD  `SuggestedDiscountDate` date NULL   ;
ALTER TABLE  `qb_invoice` ADD  `Other` varchar(29) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_invoice_model extends MY_Model {

	protected $qbxml_id;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $ARAccount_ListID;
	protected $ARAccount_FullName;
	protected $Template_ListID;
	protected $Template_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $BillAddress_Addr1;
	protected $BillAddress_Addr2;
	protected $BillAddress_Addr3;
	protected $BillAddress_Addr4;
	protected $BillAddress_Addr5;
	protected $BillAddress_City;
	protected $BillAddress_State;
	protected $BillAddress_PostalCode;
	protected $BillAddress_Country;
	protected $BillAddress_Note;
	protected $BillAddressBlock_Addr1;
	protected $BillAddressBlock_Addr2;
	protected $BillAddressBlock_Addr3;
	protected $BillAddressBlock_Addr4;
	protected $BillAddressBlock_Addr5;
	protected $ShipAddress_Addr1;
	protected $ShipAddress_Addr2;
	protected $ShipAddress_Addr3;
	protected $ShipAddress_Addr4;
	protected $ShipAddress_Addr5;
	protected $ShipAddress_City;
	protected $ShipAddress_State;
	protected $ShipAddress_PostalCode;
	protected $ShipAddress_Country;
	protected $ShipAddress_Note;
	protected $ShipAddressBlock_Addr1;
	protected $ShipAddressBlock_Addr2;
	protected $ShipAddressBlock_Addr3;
	protected $ShipAddressBlock_Addr4;
	protected $ShipAddressBlock_Addr5;
	protected $IsPending;
	protected $IsFinanceCharge;
	protected $PONumber;
	protected $Terms_ListID;
	protected $Terms_FullName;
	protected $DueDate;
	protected $SalesRep_ListID;
	protected $SalesRep_FullName;
	protected $FOB;
	protected $ShipDate;
	protected $ShipMethod_ListID;
	protected $ShipMethod_FullName;
	protected $Subtotal;
	protected $ItemSalesTax_ListID;
	protected $ItemSalesTax_FullName;
	protected $SalesTaxPercentage;
	protected $SalesTaxTotal;
	protected $AppliedAmount;
	protected $BalanceRemaining;
	protected $Memo;
	protected $IsPaid;
	protected $Currency_ListID;
	protected $Currency_FullName;
	protected $ExchangeRate;
	protected $BalanceRemainingInHomeCurrency;
	protected $CustomerMsg_ListID;
	protected $CustomerMsg_FullName;
	protected $IsToBePrinted;
	protected $IsToBeEmailed;
	protected $CustomerSalesTaxCode_ListID;
	protected $CustomerSalesTaxCode_FullName;
	protected $SuggestedDiscountAmount;
	protected $SuggestedDiscountDate;
	protected $Other;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_invoice';
		$this->_short_name = 'qb_invoice';
		$this->_fields = array("qbxml_id","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Customer_ListID","Customer_FullName","Class_ListID","Class_FullName","ARAccount_ListID","ARAccount_FullName","Template_ListID","Template_FullName","TxnDate","RefNumber","BillAddress_Addr1","BillAddress_Addr2","BillAddress_Addr3","BillAddress_Addr4","BillAddress_Addr5","BillAddress_City","BillAddress_State","BillAddress_PostalCode","BillAddress_Country","BillAddress_Note","BillAddressBlock_Addr1","BillAddressBlock_Addr2","BillAddressBlock_Addr3","BillAddressBlock_Addr4","BillAddressBlock_Addr5","ShipAddress_Addr1","ShipAddress_Addr2","ShipAddress_Addr3","ShipAddress_Addr4","ShipAddress_Addr5","ShipAddress_City","ShipAddress_State","ShipAddress_PostalCode","ShipAddress_Country","ShipAddress_Note","ShipAddressBlock_Addr1","ShipAddressBlock_Addr2","ShipAddressBlock_Addr3","ShipAddressBlock_Addr4","ShipAddressBlock_Addr5","IsPending","IsFinanceCharge","PONumber","Terms_ListID","Terms_FullName","DueDate","SalesRep_ListID","SalesRep_FullName","FOB","ShipDate","ShipMethod_ListID","ShipMethod_FullName","Subtotal","ItemSalesTax_ListID","ItemSalesTax_FullName","SalesTaxPercentage","SalesTaxTotal","AppliedAmount","BalanceRemaining","Memo","IsPaid","Currency_ListID","Currency_FullName","ExchangeRate","BalanceRemainingInHomeCurrency","CustomerMsg_ListID","CustomerMsg_FullName","IsToBePrinted","IsToBeEmailed","CustomerSalesTaxCode_ListID","CustomerSalesTaxCode_FullName","SuggestedDiscountAmount","SuggestedDiscountDate","Other");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: ARAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_ListID` variable
	* @access public
	*/

	public function setAraccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_ListID` variable
	* @access public
	*/

	public function getAraccountListid() {
		return $this->ARAccount_ListID;
	}

	public function get_ARAccount_ListID() {
		return $this->ARAccount_ListID;
	}

	
// ------------------------------ End Field: ARAccount_ListID --------------------------------------


// ---------------------------- Start Field: ARAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_FullName` variable
	* @access public
	*/

	public function setAraccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_FullName` variable
	* @access public
	*/

	public function getAraccountFullname() {
		return $this->ARAccount_FullName;
	}

	public function get_ARAccount_FullName() {
		return $this->ARAccount_FullName;
	}

	
// ------------------------------ End Field: ARAccount_FullName --------------------------------------


// ---------------------------- Start Field: Template_ListID -------------------------------------- 

	/** 
	* Sets a value to `Template_ListID` variable
	* @access public
	*/

	public function setTemplateListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_ListID` variable
	* @access public
	*/

	public function getTemplateListid() {
		return $this->Template_ListID;
	}

	public function get_Template_ListID() {
		return $this->Template_ListID;
	}

	
// ------------------------------ End Field: Template_ListID --------------------------------------


// ---------------------------- Start Field: Template_FullName -------------------------------------- 

	/** 
	* Sets a value to `Template_FullName` variable
	* @access public
	*/

	public function setTemplateFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Template_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Template_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Template_FullName` variable
	* @access public
	*/

	public function getTemplateFullname() {
		return $this->Template_FullName;
	}

	public function get_Template_FullName() {
		return $this->Template_FullName;
	}

	
// ------------------------------ End Field: Template_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr1` variable
	* @access public
	*/

	public function setBilladdressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr1` variable
	* @access public
	*/

	public function getBilladdressAddr1() {
		return $this->BillAddress_Addr1;
	}

	public function get_BillAddress_Addr1() {
		return $this->BillAddress_Addr1;
	}

	
// ------------------------------ End Field: BillAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr2` variable
	* @access public
	*/

	public function setBilladdressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr2` variable
	* @access public
	*/

	public function getBilladdressAddr2() {
		return $this->BillAddress_Addr2;
	}

	public function get_BillAddress_Addr2() {
		return $this->BillAddress_Addr2;
	}

	
// ------------------------------ End Field: BillAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr3` variable
	* @access public
	*/

	public function setBilladdressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr3` variable
	* @access public
	*/

	public function getBilladdressAddr3() {
		return $this->BillAddress_Addr3;
	}

	public function get_BillAddress_Addr3() {
		return $this->BillAddress_Addr3;
	}

	
// ------------------------------ End Field: BillAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr4` variable
	* @access public
	*/

	public function setBilladdressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr4` variable
	* @access public
	*/

	public function getBilladdressAddr4() {
		return $this->BillAddress_Addr4;
	}

	public function get_BillAddress_Addr4() {
		return $this->BillAddress_Addr4;
	}

	
// ------------------------------ End Field: BillAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Addr5` variable
	* @access public
	*/

	public function setBilladdressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Addr5` variable
	* @access public
	*/

	public function getBilladdressAddr5() {
		return $this->BillAddress_Addr5;
	}

	public function get_BillAddress_Addr5() {
		return $this->BillAddress_Addr5;
	}

	
// ------------------------------ End Field: BillAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: BillAddress_City -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_City` variable
	* @access public
	*/

	public function setBilladdressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_City` variable
	* @access public
	*/

	public function getBilladdressCity() {
		return $this->BillAddress_City;
	}

	public function get_BillAddress_City() {
		return $this->BillAddress_City;
	}

	
// ------------------------------ End Field: BillAddress_City --------------------------------------


// ---------------------------- Start Field: BillAddress_State -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_State` variable
	* @access public
	*/

	public function setBilladdressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_State` variable
	* @access public
	*/

	public function getBilladdressState() {
		return $this->BillAddress_State;
	}

	public function get_BillAddress_State() {
		return $this->BillAddress_State;
	}

	
// ------------------------------ End Field: BillAddress_State --------------------------------------


// ---------------------------- Start Field: BillAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_PostalCode` variable
	* @access public
	*/

	public function setBilladdressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_PostalCode` variable
	* @access public
	*/

	public function getBilladdressPostalcode() {
		return $this->BillAddress_PostalCode;
	}

	public function get_BillAddress_PostalCode() {
		return $this->BillAddress_PostalCode;
	}

	
// ------------------------------ End Field: BillAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: BillAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Country` variable
	* @access public
	*/

	public function setBilladdressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Country` variable
	* @access public
	*/

	public function getBilladdressCountry() {
		return $this->BillAddress_Country;
	}

	public function get_BillAddress_Country() {
		return $this->BillAddress_Country;
	}

	
// ------------------------------ End Field: BillAddress_Country --------------------------------------


// ---------------------------- Start Field: BillAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `BillAddress_Note` variable
	* @access public
	*/

	public function setBilladdressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddress_Note` variable
	* @access public
	*/

	public function getBilladdressNote() {
		return $this->BillAddress_Note;
	}

	public function get_BillAddress_Note() {
		return $this->BillAddress_Note;
	}

	
// ------------------------------ End Field: BillAddress_Note --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function setBilladdressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr1` variable
	* @access public
	*/

	public function getBilladdressblockAddr1() {
		return $this->BillAddressBlock_Addr1;
	}

	public function get_BillAddressBlock_Addr1() {
		return $this->BillAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function setBilladdressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr2` variable
	* @access public
	*/

	public function getBilladdressblockAddr2() {
		return $this->BillAddressBlock_Addr2;
	}

	public function get_BillAddressBlock_Addr2() {
		return $this->BillAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function setBilladdressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr3` variable
	* @access public
	*/

	public function getBilladdressblockAddr3() {
		return $this->BillAddressBlock_Addr3;
	}

	public function get_BillAddressBlock_Addr3() {
		return $this->BillAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function setBilladdressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr4` variable
	* @access public
	*/

	public function getBilladdressblockAddr4() {
		return $this->BillAddressBlock_Addr4;
	}

	public function get_BillAddressBlock_Addr4() {
		return $this->BillAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: BillAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function setBilladdressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BillAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BillAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BillAddressBlock_Addr5` variable
	* @access public
	*/

	public function getBilladdressblockAddr5() {
		return $this->BillAddressBlock_Addr5;
	}

	public function get_BillAddressBlock_Addr5() {
		return $this->BillAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: BillAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr1` variable
	* @access public
	*/

	public function setShipaddressAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr1` variable
	* @access public
	*/

	public function getShipaddressAddr1() {
		return $this->ShipAddress_Addr1;
	}

	public function get_ShipAddress_Addr1() {
		return $this->ShipAddress_Addr1;
	}

	
// ------------------------------ End Field: ShipAddress_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr2` variable
	* @access public
	*/

	public function setShipaddressAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr2` variable
	* @access public
	*/

	public function getShipaddressAddr2() {
		return $this->ShipAddress_Addr2;
	}

	public function get_ShipAddress_Addr2() {
		return $this->ShipAddress_Addr2;
	}

	
// ------------------------------ End Field: ShipAddress_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr3` variable
	* @access public
	*/

	public function setShipaddressAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr3` variable
	* @access public
	*/

	public function getShipaddressAddr3() {
		return $this->ShipAddress_Addr3;
	}

	public function get_ShipAddress_Addr3() {
		return $this->ShipAddress_Addr3;
	}

	
// ------------------------------ End Field: ShipAddress_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr4` variable
	* @access public
	*/

	public function setShipaddressAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr4` variable
	* @access public
	*/

	public function getShipaddressAddr4() {
		return $this->ShipAddress_Addr4;
	}

	public function get_ShipAddress_Addr4() {
		return $this->ShipAddress_Addr4;
	}

	
// ------------------------------ End Field: ShipAddress_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddress_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Addr5` variable
	* @access public
	*/

	public function setShipaddressAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Addr5` variable
	* @access public
	*/

	public function getShipaddressAddr5() {
		return $this->ShipAddress_Addr5;
	}

	public function get_ShipAddress_Addr5() {
		return $this->ShipAddress_Addr5;
	}

	
// ------------------------------ End Field: ShipAddress_Addr5 --------------------------------------


// ---------------------------- Start Field: ShipAddress_City -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_City` variable
	* @access public
	*/

	public function setShipaddressCity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_City($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_City', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_City` variable
	* @access public
	*/

	public function getShipaddressCity() {
		return $this->ShipAddress_City;
	}

	public function get_ShipAddress_City() {
		return $this->ShipAddress_City;
	}

	
// ------------------------------ End Field: ShipAddress_City --------------------------------------


// ---------------------------- Start Field: ShipAddress_State -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_State` variable
	* @access public
	*/

	public function setShipaddressState($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_State($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_State', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_State` variable
	* @access public
	*/

	public function getShipaddressState() {
		return $this->ShipAddress_State;
	}

	public function get_ShipAddress_State() {
		return $this->ShipAddress_State;
	}

	
// ------------------------------ End Field: ShipAddress_State --------------------------------------


// ---------------------------- Start Field: ShipAddress_PostalCode -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function setShipaddressPostalcode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_PostalCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_PostalCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_PostalCode` variable
	* @access public
	*/

	public function getShipaddressPostalcode() {
		return $this->ShipAddress_PostalCode;
	}

	public function get_ShipAddress_PostalCode() {
		return $this->ShipAddress_PostalCode;
	}

	
// ------------------------------ End Field: ShipAddress_PostalCode --------------------------------------


// ---------------------------- Start Field: ShipAddress_Country -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Country` variable
	* @access public
	*/

	public function setShipaddressCountry($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Country($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Country', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Country` variable
	* @access public
	*/

	public function getShipaddressCountry() {
		return $this->ShipAddress_Country;
	}

	public function get_ShipAddress_Country() {
		return $this->ShipAddress_Country;
	}

	
// ------------------------------ End Field: ShipAddress_Country --------------------------------------


// ---------------------------- Start Field: ShipAddress_Note -------------------------------------- 

	/** 
	* Sets a value to `ShipAddress_Note` variable
	* @access public
	*/

	public function setShipaddressNote($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddress_Note($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddress_Note', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddress_Note` variable
	* @access public
	*/

	public function getShipaddressNote() {
		return $this->ShipAddress_Note;
	}

	public function get_ShipAddress_Note() {
		return $this->ShipAddress_Note;
	}

	
// ------------------------------ End Field: ShipAddress_Note --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr1 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function setShipaddressblockAddr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr1($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr1', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr1` variable
	* @access public
	*/

	public function getShipaddressblockAddr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	public function get_ShipAddressBlock_Addr1() {
		return $this->ShipAddressBlock_Addr1;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr1 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr2 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function setShipaddressblockAddr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr2($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr2', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr2` variable
	* @access public
	*/

	public function getShipaddressblockAddr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	public function get_ShipAddressBlock_Addr2() {
		return $this->ShipAddressBlock_Addr2;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr2 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr3 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function setShipaddressblockAddr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr3($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr3', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr3` variable
	* @access public
	*/

	public function getShipaddressblockAddr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	public function get_ShipAddressBlock_Addr3() {
		return $this->ShipAddressBlock_Addr3;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr3 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr4 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function setShipaddressblockAddr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr4($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr4', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr4` variable
	* @access public
	*/

	public function getShipaddressblockAddr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	public function get_ShipAddressBlock_Addr4() {
		return $this->ShipAddressBlock_Addr4;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr4 --------------------------------------


// ---------------------------- Start Field: ShipAddressBlock_Addr5 -------------------------------------- 

	/** 
	* Sets a value to `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function setShipaddressblockAddr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipAddressBlock_Addr5($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipAddressBlock_Addr5', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipAddressBlock_Addr5` variable
	* @access public
	*/

	public function getShipaddressblockAddr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	public function get_ShipAddressBlock_Addr5() {
		return $this->ShipAddressBlock_Addr5;
	}

	
// ------------------------------ End Field: ShipAddressBlock_Addr5 --------------------------------------


// ---------------------------- Start Field: IsPending -------------------------------------- 

	/** 
	* Sets a value to `IsPending` variable
	* @access public
	*/

	public function setIspending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPending($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPending', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPending` variable
	* @access public
	*/

	public function getIspending() {
		return $this->IsPending;
	}

	public function get_IsPending() {
		return $this->IsPending;
	}

	
// ------------------------------ End Field: IsPending --------------------------------------


// ---------------------------- Start Field: IsFinanceCharge -------------------------------------- 

	/** 
	* Sets a value to `IsFinanceCharge` variable
	* @access public
	*/

	public function setIsfinancecharge($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsFinanceCharge', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsFinanceCharge($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsFinanceCharge', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsFinanceCharge` variable
	* @access public
	*/

	public function getIsfinancecharge() {
		return $this->IsFinanceCharge;
	}

	public function get_IsFinanceCharge() {
		return $this->IsFinanceCharge;
	}

	
// ------------------------------ End Field: IsFinanceCharge --------------------------------------


// ---------------------------- Start Field: PONumber -------------------------------------- 

	/** 
	* Sets a value to `PONumber` variable
	* @access public
	*/

	public function setPonumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PONumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PONumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PONumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PONumber` variable
	* @access public
	*/

	public function getPonumber() {
		return $this->PONumber;
	}

	public function get_PONumber() {
		return $this->PONumber;
	}

	
// ------------------------------ End Field: PONumber --------------------------------------


// ---------------------------- Start Field: Terms_ListID -------------------------------------- 

	/** 
	* Sets a value to `Terms_ListID` variable
	* @access public
	*/

	public function setTermsListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_ListID` variable
	* @access public
	*/

	public function getTermsListid() {
		return $this->Terms_ListID;
	}

	public function get_Terms_ListID() {
		return $this->Terms_ListID;
	}

	
// ------------------------------ End Field: Terms_ListID --------------------------------------


// ---------------------------- Start Field: Terms_FullName -------------------------------------- 

	/** 
	* Sets a value to `Terms_FullName` variable
	* @access public
	*/

	public function setTermsFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Terms_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Terms_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Terms_FullName` variable
	* @access public
	*/

	public function getTermsFullname() {
		return $this->Terms_FullName;
	}

	public function get_Terms_FullName() {
		return $this->Terms_FullName;
	}

	
// ------------------------------ End Field: Terms_FullName --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------


// ---------------------------- Start Field: SalesRep_ListID -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_ListID` variable
	* @access public
	*/

	public function setSalesrepListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_ListID` variable
	* @access public
	*/

	public function getSalesrepListid() {
		return $this->SalesRep_ListID;
	}

	public function get_SalesRep_ListID() {
		return $this->SalesRep_ListID;
	}

	
// ------------------------------ End Field: SalesRep_ListID --------------------------------------


// ---------------------------- Start Field: SalesRep_FullName -------------------------------------- 

	/** 
	* Sets a value to `SalesRep_FullName` variable
	* @access public
	*/

	public function setSalesrepFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesRep_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesRep_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesRep_FullName` variable
	* @access public
	*/

	public function getSalesrepFullname() {
		return $this->SalesRep_FullName;
	}

	public function get_SalesRep_FullName() {
		return $this->SalesRep_FullName;
	}

	
// ------------------------------ End Field: SalesRep_FullName --------------------------------------


// ---------------------------- Start Field: FOB -------------------------------------- 

	/** 
	* Sets a value to `FOB` variable
	* @access public
	*/

	public function setFob($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FOB($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FOB', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FOB` variable
	* @access public
	*/

	public function getFob() {
		return $this->FOB;
	}

	public function get_FOB() {
		return $this->FOB;
	}

	
// ------------------------------ End Field: FOB --------------------------------------


// ---------------------------- Start Field: ShipDate -------------------------------------- 

	/** 
	* Sets a value to `ShipDate` variable
	* @access public
	*/

	public function setShipdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipDate` variable
	* @access public
	*/

	public function getShipdate() {
		return $this->ShipDate;
	}

	public function get_ShipDate() {
		return $this->ShipDate;
	}

	
// ------------------------------ End Field: ShipDate --------------------------------------


// ---------------------------- Start Field: ShipMethod_ListID -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_ListID` variable
	* @access public
	*/

	public function setShipmethodListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_ListID` variable
	* @access public
	*/

	public function getShipmethodListid() {
		return $this->ShipMethod_ListID;
	}

	public function get_ShipMethod_ListID() {
		return $this->ShipMethod_ListID;
	}

	
// ------------------------------ End Field: ShipMethod_ListID --------------------------------------


// ---------------------------- Start Field: ShipMethod_FullName -------------------------------------- 

	/** 
	* Sets a value to `ShipMethod_FullName` variable
	* @access public
	*/

	public function setShipmethodFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ShipMethod_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ShipMethod_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ShipMethod_FullName` variable
	* @access public
	*/

	public function getShipmethodFullname() {
		return $this->ShipMethod_FullName;
	}

	public function get_ShipMethod_FullName() {
		return $this->ShipMethod_FullName;
	}

	
// ------------------------------ End Field: ShipMethod_FullName --------------------------------------


// ---------------------------- Start Field: Subtotal -------------------------------------- 

	/** 
	* Sets a value to `Subtotal` variable
	* @access public
	*/

	public function setSubtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Subtotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Subtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Subtotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Subtotal` variable
	* @access public
	*/

	public function getSubtotal() {
		return $this->Subtotal;
	}

	public function get_Subtotal() {
		return $this->Subtotal;
	}

	
// ------------------------------ End Field: Subtotal --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function setItemsalestaxListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_ListID` variable
	* @access public
	*/

	public function getItemsalestaxListid() {
		return $this->ItemSalesTax_ListID;
	}

	public function get_ItemSalesTax_ListID() {
		return $this->ItemSalesTax_ListID;
	}

	
// ------------------------------ End Field: ItemSalesTax_ListID --------------------------------------


// ---------------------------- Start Field: ItemSalesTax_FullName -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function setItemsalestaxFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTax_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTax_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTax_FullName` variable
	* @access public
	*/

	public function getItemsalestaxFullname() {
		return $this->ItemSalesTax_FullName;
	}

	public function get_ItemSalesTax_FullName() {
		return $this->ItemSalesTax_FullName;
	}

	
// ------------------------------ End Field: ItemSalesTax_FullName --------------------------------------


// ---------------------------- Start Field: SalesTaxPercentage -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxPercentage` variable
	* @access public
	*/

	public function setSalestaxpercentage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPercentage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxPercentage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxPercentage', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxPercentage` variable
	* @access public
	*/

	public function getSalestaxpercentage() {
		return $this->SalesTaxPercentage;
	}

	public function get_SalesTaxPercentage() {
		return $this->SalesTaxPercentage;
	}

	
// ------------------------------ End Field: SalesTaxPercentage --------------------------------------


// ---------------------------- Start Field: SalesTaxTotal -------------------------------------- 

	/** 
	* Sets a value to `SalesTaxTotal` variable
	* @access public
	*/

	public function setSalestaxtotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SalesTaxTotal($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SalesTaxTotal', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SalesTaxTotal` variable
	* @access public
	*/

	public function getSalestaxtotal() {
		return $this->SalesTaxTotal;
	}

	public function get_SalesTaxTotal() {
		return $this->SalesTaxTotal;
	}

	
// ------------------------------ End Field: SalesTaxTotal --------------------------------------


// ---------------------------- Start Field: AppliedAmount -------------------------------------- 

	/** 
	* Sets a value to `AppliedAmount` variable
	* @access public
	*/

	public function setAppliedamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AppliedAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AppliedAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AppliedAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AppliedAmount` variable
	* @access public
	*/

	public function getAppliedamount() {
		return $this->AppliedAmount;
	}

	public function get_AppliedAmount() {
		return $this->AppliedAmount;
	}

	
// ------------------------------ End Field: AppliedAmount --------------------------------------


// ---------------------------- Start Field: BalanceRemaining -------------------------------------- 

	/** 
	* Sets a value to `BalanceRemaining` variable
	* @access public
	*/

	public function setBalanceremaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BalanceRemaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BalanceRemaining` variable
	* @access public
	*/

	public function getBalanceremaining() {
		return $this->BalanceRemaining;
	}

	public function get_BalanceRemaining() {
		return $this->BalanceRemaining;
	}

	
// ------------------------------ End Field: BalanceRemaining --------------------------------------


// ---------------------------- Start Field: Memo -------------------------------------- 

	/** 
	* Sets a value to `Memo` variable
	* @access public
	*/

	public function setMemo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Memo($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Memo', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Memo` variable
	* @access public
	*/

	public function getMemo() {
		return $this->Memo;
	}

	public function get_Memo() {
		return $this->Memo;
	}

	
// ------------------------------ End Field: Memo --------------------------------------


// ---------------------------- Start Field: IsPaid -------------------------------------- 

	/** 
	* Sets a value to `IsPaid` variable
	* @access public
	*/

	public function setIspaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPaid` variable
	* @access public
	*/

	public function getIspaid() {
		return $this->IsPaid;
	}

	public function get_IsPaid() {
		return $this->IsPaid;
	}

	
// ------------------------------ End Field: IsPaid --------------------------------------


// ---------------------------- Start Field: Currency_ListID -------------------------------------- 

	/** 
	* Sets a value to `Currency_ListID` variable
	* @access public
	*/

	public function setCurrencyListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_ListID` variable
	* @access public
	*/

	public function getCurrencyListid() {
		return $this->Currency_ListID;
	}

	public function get_Currency_ListID() {
		return $this->Currency_ListID;
	}

	
// ------------------------------ End Field: Currency_ListID --------------------------------------


// ---------------------------- Start Field: Currency_FullName -------------------------------------- 

	/** 
	* Sets a value to `Currency_FullName` variable
	* @access public
	*/

	public function setCurrencyFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_FullName` variable
	* @access public
	*/

	public function getCurrencyFullname() {
		return $this->Currency_FullName;
	}

	public function get_Currency_FullName() {
		return $this->Currency_FullName;
	}

	
// ------------------------------ End Field: Currency_FullName --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: BalanceRemainingInHomeCurrency -------------------------------------- 

	/** 
	* Sets a value to `BalanceRemainingInHomeCurrency` variable
	* @access public
	*/

	public function setBalanceremaininginhomecurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemainingInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BalanceRemainingInHomeCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemainingInHomeCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BalanceRemainingInHomeCurrency` variable
	* @access public
	*/

	public function getBalanceremaininginhomecurrency() {
		return $this->BalanceRemainingInHomeCurrency;
	}

	public function get_BalanceRemainingInHomeCurrency() {
		return $this->BalanceRemainingInHomeCurrency;
	}

	
// ------------------------------ End Field: BalanceRemainingInHomeCurrency --------------------------------------


// ---------------------------- Start Field: CustomerMsg_ListID -------------------------------------- 

	/** 
	* Sets a value to `CustomerMsg_ListID` variable
	* @access public
	*/

	public function setCustomermsgListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerMsg_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerMsg_ListID` variable
	* @access public
	*/

	public function getCustomermsgListid() {
		return $this->CustomerMsg_ListID;
	}

	public function get_CustomerMsg_ListID() {
		return $this->CustomerMsg_ListID;
	}

	
// ------------------------------ End Field: CustomerMsg_ListID --------------------------------------


// ---------------------------- Start Field: CustomerMsg_FullName -------------------------------------- 

	/** 
	* Sets a value to `CustomerMsg_FullName` variable
	* @access public
	*/

	public function setCustomermsgFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerMsg_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerMsg_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerMsg_FullName` variable
	* @access public
	*/

	public function getCustomermsgFullname() {
		return $this->CustomerMsg_FullName;
	}

	public function get_CustomerMsg_FullName() {
		return $this->CustomerMsg_FullName;
	}

	
// ------------------------------ End Field: CustomerMsg_FullName --------------------------------------


// ---------------------------- Start Field: IsToBePrinted -------------------------------------- 

	/** 
	* Sets a value to `IsToBePrinted` variable
	* @access public
	*/

	public function setIstobeprinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBePrinted($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBePrinted', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBePrinted` variable
	* @access public
	*/

	public function getIstobeprinted() {
		return $this->IsToBePrinted;
	}

	public function get_IsToBePrinted() {
		return $this->IsToBePrinted;
	}

	
// ------------------------------ End Field: IsToBePrinted --------------------------------------


// ---------------------------- Start Field: IsToBeEmailed -------------------------------------- 

	/** 
	* Sets a value to `IsToBeEmailed` variable
	* @access public
	*/

	public function setIstobeemailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsToBeEmailed($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsToBeEmailed', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsToBeEmailed` variable
	* @access public
	*/

	public function getIstobeemailed() {
		return $this->IsToBeEmailed;
	}

	public function get_IsToBeEmailed() {
		return $this->IsToBeEmailed;
	}

	
// ------------------------------ End Field: IsToBeEmailed --------------------------------------


// ---------------------------- Start Field: CustomerSalesTaxCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `CustomerSalesTaxCode_ListID` variable
	* @access public
	*/

	public function setCustomersalestaxcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerSalesTaxCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerSalesTaxCode_ListID` variable
	* @access public
	*/

	public function getCustomersalestaxcodeListid() {
		return $this->CustomerSalesTaxCode_ListID;
	}

	public function get_CustomerSalesTaxCode_ListID() {
		return $this->CustomerSalesTaxCode_ListID;
	}

	
// ------------------------------ End Field: CustomerSalesTaxCode_ListID --------------------------------------


// ---------------------------- Start Field: CustomerSalesTaxCode_FullName -------------------------------------- 

	/** 
	* Sets a value to `CustomerSalesTaxCode_FullName` variable
	* @access public
	*/

	public function setCustomersalestaxcodeFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomerSalesTaxCode_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomerSalesTaxCode_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomerSalesTaxCode_FullName` variable
	* @access public
	*/

	public function getCustomersalestaxcodeFullname() {
		return $this->CustomerSalesTaxCode_FullName;
	}

	public function get_CustomerSalesTaxCode_FullName() {
		return $this->CustomerSalesTaxCode_FullName;
	}

	
// ------------------------------ End Field: CustomerSalesTaxCode_FullName --------------------------------------


// ---------------------------- Start Field: SuggestedDiscountAmount -------------------------------------- 

	/** 
	* Sets a value to `SuggestedDiscountAmount` variable
	* @access public
	*/

	public function setSuggesteddiscountamount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SuggestedDiscountAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SuggestedDiscountAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SuggestedDiscountAmount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SuggestedDiscountAmount` variable
	* @access public
	*/

	public function getSuggesteddiscountamount() {
		return $this->SuggestedDiscountAmount;
	}

	public function get_SuggestedDiscountAmount() {
		return $this->SuggestedDiscountAmount;
	}

	
// ------------------------------ End Field: SuggestedDiscountAmount --------------------------------------


// ---------------------------- Start Field: SuggestedDiscountDate -------------------------------------- 

	/** 
	* Sets a value to `SuggestedDiscountDate` variable
	* @access public
	*/

	public function setSuggesteddiscountdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SuggestedDiscountDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_SuggestedDiscountDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('SuggestedDiscountDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `SuggestedDiscountDate` variable
	* @access public
	*/

	public function getSuggesteddiscountdate() {
		return $this->SuggestedDiscountDate;
	}

	public function get_SuggestedDiscountDate() {
		return $this->SuggestedDiscountDate;
	}

	
// ------------------------------ End Field: SuggestedDiscountDate --------------------------------------


// ---------------------------- Start Field: Other -------------------------------------- 

	/** 
	* Sets a value to `Other` variable
	* @access public
	*/

	public function setOther($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Other($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Other', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Other` variable
	* @access public
	*/

	public function getOther() {
		return $this->Other;
	}

	public function get_Other() {
		return $this->Other;
	}

	
// ------------------------------ End Field: Other --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_ListID' => (object) array(
										'Field'=>'ARAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_FullName' => (object) array(
										'Field'=>'ARAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_ListID' => (object) array(
										'Field'=>'Template_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Template_FullName' => (object) array(
										'Field'=>'Template_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'varchar(11)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr1' => (object) array(
										'Field'=>'BillAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr2' => (object) array(
										'Field'=>'BillAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr3' => (object) array(
										'Field'=>'BillAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr4' => (object) array(
										'Field'=>'BillAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Addr5' => (object) array(
										'Field'=>'BillAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_City' => (object) array(
										'Field'=>'BillAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_State' => (object) array(
										'Field'=>'BillAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_PostalCode' => (object) array(
										'Field'=>'BillAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Country' => (object) array(
										'Field'=>'BillAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddress_Note' => (object) array(
										'Field'=>'BillAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr1' => (object) array(
										'Field'=>'BillAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr2' => (object) array(
										'Field'=>'BillAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr3' => (object) array(
										'Field'=>'BillAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr4' => (object) array(
										'Field'=>'BillAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BillAddressBlock_Addr5' => (object) array(
										'Field'=>'BillAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr1' => (object) array(
										'Field'=>'ShipAddress_Addr1',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr2' => (object) array(
										'Field'=>'ShipAddress_Addr2',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr3' => (object) array(
										'Field'=>'ShipAddress_Addr3',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr4' => (object) array(
										'Field'=>'ShipAddress_Addr4',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Addr5' => (object) array(
										'Field'=>'ShipAddress_Addr5',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_City' => (object) array(
										'Field'=>'ShipAddress_City',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_State' => (object) array(
										'Field'=>'ShipAddress_State',
										'Type'=>'varchar(21)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_PostalCode' => (object) array(
										'Field'=>'ShipAddress_PostalCode',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Country' => (object) array(
										'Field'=>'ShipAddress_Country',
										'Type'=>'varchar(31)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddress_Note' => (object) array(
										'Field'=>'ShipAddress_Note',
										'Type'=>'varchar(41)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr1' => (object) array(
										'Field'=>'ShipAddressBlock_Addr1',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr2' => (object) array(
										'Field'=>'ShipAddressBlock_Addr2',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr3' => (object) array(
										'Field'=>'ShipAddressBlock_Addr3',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr4' => (object) array(
										'Field'=>'ShipAddressBlock_Addr4',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipAddressBlock_Addr5' => (object) array(
										'Field'=>'ShipAddressBlock_Addr5',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPending' => (object) array(
										'Field'=>'IsPending',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsFinanceCharge' => (object) array(
										'Field'=>'IsFinanceCharge',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'PONumber' => (object) array(
										'Field'=>'PONumber',
										'Type'=>'varchar(25)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_ListID' => (object) array(
										'Field'=>'Terms_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Terms_FullName' => (object) array(
										'Field'=>'Terms_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_ListID' => (object) array(
										'Field'=>'SalesRep_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesRep_FullName' => (object) array(
										'Field'=>'SalesRep_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'FOB' => (object) array(
										'Field'=>'FOB',
										'Type'=>'varchar(13)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipDate' => (object) array(
										'Field'=>'ShipDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_ListID' => (object) array(
										'Field'=>'ShipMethod_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ShipMethod_FullName' => (object) array(
										'Field'=>'ShipMethod_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Subtotal' => (object) array(
										'Field'=>'Subtotal',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_ListID' => (object) array(
										'Field'=>'ItemSalesTax_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ItemSalesTax_FullName' => (object) array(
										'Field'=>'ItemSalesTax_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxPercentage' => (object) array(
										'Field'=>'SalesTaxPercentage',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SalesTaxTotal' => (object) array(
										'Field'=>'SalesTaxTotal',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AppliedAmount' => (object) array(
										'Field'=>'AppliedAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BalanceRemaining' => (object) array(
										'Field'=>'BalanceRemaining',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Memo' => (object) array(
										'Field'=>'Memo',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsPaid' => (object) array(
										'Field'=>'IsPaid',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_ListID' => (object) array(
										'Field'=>'Currency_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_FullName' => (object) array(
										'Field'=>'Currency_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BalanceRemainingInHomeCurrency' => (object) array(
										'Field'=>'BalanceRemainingInHomeCurrency',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerMsg_ListID' => (object) array(
										'Field'=>'CustomerMsg_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerMsg_FullName' => (object) array(
										'Field'=>'CustomerMsg_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBePrinted' => (object) array(
										'Field'=>'IsToBePrinted',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'IsToBeEmailed' => (object) array(
										'Field'=>'IsToBeEmailed',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerSalesTaxCode_ListID' => (object) array(
										'Field'=>'CustomerSalesTaxCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomerSalesTaxCode_FullName' => (object) array(
										'Field'=>'CustomerSalesTaxCode_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SuggestedDiscountAmount' => (object) array(
										'Field'=>'SuggestedDiscountAmount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'SuggestedDiscountDate' => (object) array(
										'Field'=>'SuggestedDiscountDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Other' => (object) array(
										'Field'=>'Other',
										'Type'=>'varchar(29)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_invoice` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'TxnID' => "ALTER TABLE  `qb_invoice` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_invoice` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_invoice` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_invoice` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_invoice` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Customer_ListID' => "ALTER TABLE  `qb_invoice` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_invoice` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_invoice` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_invoice` ADD  `Class_FullName` varchar(255) NULL   ;",
			'ARAccount_ListID' => "ALTER TABLE  `qb_invoice` ADD  `ARAccount_ListID` varchar(40) NULL   ;",
			'ARAccount_FullName' => "ALTER TABLE  `qb_invoice` ADD  `ARAccount_FullName` varchar(255) NULL   ;",
			'Template_ListID' => "ALTER TABLE  `qb_invoice` ADD  `Template_ListID` varchar(40) NULL   ;",
			'Template_FullName' => "ALTER TABLE  `qb_invoice` ADD  `Template_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_invoice` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_invoice` ADD  `RefNumber` varchar(11) NULL   ;",
			'BillAddress_Addr1' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr1` varchar(41) NULL   ;",
			'BillAddress_Addr2' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr2` varchar(41) NULL   ;",
			'BillAddress_Addr3' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr3` varchar(41) NULL   ;",
			'BillAddress_Addr4' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr4` varchar(41) NULL   ;",
			'BillAddress_Addr5' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Addr5` varchar(41) NULL   ;",
			'BillAddress_City' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_City` varchar(31) NULL   ;",
			'BillAddress_State' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_State` varchar(21) NULL   ;",
			'BillAddress_PostalCode' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_PostalCode` varchar(13) NULL   ;",
			'BillAddress_Country' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Country` varchar(31) NULL   ;",
			'BillAddress_Note' => "ALTER TABLE  `qb_invoice` ADD  `BillAddress_Note` varchar(41) NULL   ;",
			'BillAddressBlock_Addr1' => "ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr1` text NULL   ;",
			'BillAddressBlock_Addr2' => "ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr2` text NULL   ;",
			'BillAddressBlock_Addr3' => "ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr3` text NULL   ;",
			'BillAddressBlock_Addr4' => "ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr4` text NULL   ;",
			'BillAddressBlock_Addr5' => "ALTER TABLE  `qb_invoice` ADD  `BillAddressBlock_Addr5` text NULL   ;",
			'ShipAddress_Addr1' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr1` varchar(41) NULL   ;",
			'ShipAddress_Addr2' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr2` varchar(41) NULL   ;",
			'ShipAddress_Addr3' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr3` varchar(41) NULL   ;",
			'ShipAddress_Addr4' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr4` varchar(41) NULL   ;",
			'ShipAddress_Addr5' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Addr5` varchar(41) NULL   ;",
			'ShipAddress_City' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_City` varchar(31) NULL   ;",
			'ShipAddress_State' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_State` varchar(21) NULL   ;",
			'ShipAddress_PostalCode' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_PostalCode` varchar(13) NULL   ;",
			'ShipAddress_Country' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Country` varchar(31) NULL   ;",
			'ShipAddress_Note' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddress_Note` varchar(41) NULL   ;",
			'ShipAddressBlock_Addr1' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr1` text NULL   ;",
			'ShipAddressBlock_Addr2' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr2` text NULL   ;",
			'ShipAddressBlock_Addr3' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr3` text NULL   ;",
			'ShipAddressBlock_Addr4' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr4` text NULL   ;",
			'ShipAddressBlock_Addr5' => "ALTER TABLE  `qb_invoice` ADD  `ShipAddressBlock_Addr5` text NULL   ;",
			'IsPending' => "ALTER TABLE  `qb_invoice` ADD  `IsPending` tinyint(1) NULL   ;",
			'IsFinanceCharge' => "ALTER TABLE  `qb_invoice` ADD  `IsFinanceCharge` tinyint(1) NULL   ;",
			'PONumber' => "ALTER TABLE  `qb_invoice` ADD  `PONumber` varchar(25) NULL   ;",
			'Terms_ListID' => "ALTER TABLE  `qb_invoice` ADD  `Terms_ListID` varchar(40) NULL   ;",
			'Terms_FullName' => "ALTER TABLE  `qb_invoice` ADD  `Terms_FullName` varchar(255) NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_invoice` ADD  `DueDate` date NULL   ;",
			'SalesRep_ListID' => "ALTER TABLE  `qb_invoice` ADD  `SalesRep_ListID` varchar(40) NULL   ;",
			'SalesRep_FullName' => "ALTER TABLE  `qb_invoice` ADD  `SalesRep_FullName` varchar(255) NULL   ;",
			'FOB' => "ALTER TABLE  `qb_invoice` ADD  `FOB` varchar(13) NULL   ;",
			'ShipDate' => "ALTER TABLE  `qb_invoice` ADD  `ShipDate` date NULL   ;",
			'ShipMethod_ListID' => "ALTER TABLE  `qb_invoice` ADD  `ShipMethod_ListID` varchar(40) NULL   ;",
			'ShipMethod_FullName' => "ALTER TABLE  `qb_invoice` ADD  `ShipMethod_FullName` varchar(255) NULL   ;",
			'Subtotal' => "ALTER TABLE  `qb_invoice` ADD  `Subtotal` decimal(10,2) NULL   ;",
			'ItemSalesTax_ListID' => "ALTER TABLE  `qb_invoice` ADD  `ItemSalesTax_ListID` varchar(40) NULL   ;",
			'ItemSalesTax_FullName' => "ALTER TABLE  `qb_invoice` ADD  `ItemSalesTax_FullName` varchar(255) NULL   ;",
			'SalesTaxPercentage' => "ALTER TABLE  `qb_invoice` ADD  `SalesTaxPercentage` decimal(12,5) NULL   ;",
			'SalesTaxTotal' => "ALTER TABLE  `qb_invoice` ADD  `SalesTaxTotal` decimal(10,2) NULL   ;",
			'AppliedAmount' => "ALTER TABLE  `qb_invoice` ADD  `AppliedAmount` decimal(10,2) NULL   ;",
			'BalanceRemaining' => "ALTER TABLE  `qb_invoice` ADD  `BalanceRemaining` decimal(10,2) NULL   ;",
			'Memo' => "ALTER TABLE  `qb_invoice` ADD  `Memo` text NULL   ;",
			'IsPaid' => "ALTER TABLE  `qb_invoice` ADD  `IsPaid` tinyint(1) NULL   ;",
			'Currency_ListID' => "ALTER TABLE  `qb_invoice` ADD  `Currency_ListID` varchar(40) NULL   ;",
			'Currency_FullName' => "ALTER TABLE  `qb_invoice` ADD  `Currency_FullName` varchar(255) NULL   ;",
			'ExchangeRate' => "ALTER TABLE  `qb_invoice` ADD  `ExchangeRate` text NULL   ;",
			'BalanceRemainingInHomeCurrency' => "ALTER TABLE  `qb_invoice` ADD  `BalanceRemainingInHomeCurrency` decimal(10,2) NULL   ;",
			'CustomerMsg_ListID' => "ALTER TABLE  `qb_invoice` ADD  `CustomerMsg_ListID` varchar(40) NULL   ;",
			'CustomerMsg_FullName' => "ALTER TABLE  `qb_invoice` ADD  `CustomerMsg_FullName` varchar(255) NULL   ;",
			'IsToBePrinted' => "ALTER TABLE  `qb_invoice` ADD  `IsToBePrinted` tinyint(1) NULL   ;",
			'IsToBeEmailed' => "ALTER TABLE  `qb_invoice` ADD  `IsToBeEmailed` tinyint(1) NULL   ;",
			'CustomerSalesTaxCode_ListID' => "ALTER TABLE  `qb_invoice` ADD  `CustomerSalesTaxCode_ListID` varchar(40) NULL   ;",
			'CustomerSalesTaxCode_FullName' => "ALTER TABLE  `qb_invoice` ADD  `CustomerSalesTaxCode_FullName` varchar(255) NULL   ;",
			'SuggestedDiscountAmount' => "ALTER TABLE  `qb_invoice` ADD  `SuggestedDiscountAmount` decimal(10,2) NULL   ;",
			'SuggestedDiscountDate' => "ALTER TABLE  `qb_invoice` ADD  `SuggestedDiscountDate` date NULL   ;",
			'Other' => "ALTER TABLE  `qb_invoice` ADD  `Other` varchar(29) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setAraccountListid() - ARAccount_ListID
//setAraccountFullname() - ARAccount_FullName
//setTemplateListid() - Template_ListID
//setTemplateFullname() - Template_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setBilladdressAddr1() - BillAddress_Addr1
//setBilladdressAddr2() - BillAddress_Addr2
//setBilladdressAddr3() - BillAddress_Addr3
//setBilladdressAddr4() - BillAddress_Addr4
//setBilladdressAddr5() - BillAddress_Addr5
//setBilladdressCity() - BillAddress_City
//setBilladdressState() - BillAddress_State
//setBilladdressPostalcode() - BillAddress_PostalCode
//setBilladdressCountry() - BillAddress_Country
//setBilladdressNote() - BillAddress_Note
//setBilladdressblockAddr1() - BillAddressBlock_Addr1
//setBilladdressblockAddr2() - BillAddressBlock_Addr2
//setBilladdressblockAddr3() - BillAddressBlock_Addr3
//setBilladdressblockAddr4() - BillAddressBlock_Addr4
//setBilladdressblockAddr5() - BillAddressBlock_Addr5
//setShipaddressAddr1() - ShipAddress_Addr1
//setShipaddressAddr2() - ShipAddress_Addr2
//setShipaddressAddr3() - ShipAddress_Addr3
//setShipaddressAddr4() - ShipAddress_Addr4
//setShipaddressAddr5() - ShipAddress_Addr5
//setShipaddressCity() - ShipAddress_City
//setShipaddressState() - ShipAddress_State
//setShipaddressPostalcode() - ShipAddress_PostalCode
//setShipaddressCountry() - ShipAddress_Country
//setShipaddressNote() - ShipAddress_Note
//setShipaddressblockAddr1() - ShipAddressBlock_Addr1
//setShipaddressblockAddr2() - ShipAddressBlock_Addr2
//setShipaddressblockAddr3() - ShipAddressBlock_Addr3
//setShipaddressblockAddr4() - ShipAddressBlock_Addr4
//setShipaddressblockAddr5() - ShipAddressBlock_Addr5
//setIspending() - IsPending
//setIsfinancecharge() - IsFinanceCharge
//setPonumber() - PONumber
//setTermsListid() - Terms_ListID
//setTermsFullname() - Terms_FullName
//setDuedate() - DueDate
//setSalesrepListid() - SalesRep_ListID
//setSalesrepFullname() - SalesRep_FullName
//setFob() - FOB
//setShipdate() - ShipDate
//setShipmethodListid() - ShipMethod_ListID
//setShipmethodFullname() - ShipMethod_FullName
//setSubtotal() - Subtotal
//setItemsalestaxListid() - ItemSalesTax_ListID
//setItemsalestaxFullname() - ItemSalesTax_FullName
//setSalestaxpercentage() - SalesTaxPercentage
//setSalestaxtotal() - SalesTaxTotal
//setAppliedamount() - AppliedAmount
//setBalanceremaining() - BalanceRemaining
//setMemo() - Memo
//setIspaid() - IsPaid
//setCurrencyListid() - Currency_ListID
//setCurrencyFullname() - Currency_FullName
//setExchangerate() - ExchangeRate
//setBalanceremaininginhomecurrency() - BalanceRemainingInHomeCurrency
//setCustomermsgListid() - CustomerMsg_ListID
//setCustomermsgFullname() - CustomerMsg_FullName
//setIstobeprinted() - IsToBePrinted
//setIstobeemailed() - IsToBeEmailed
//setCustomersalestaxcodeListid() - CustomerSalesTaxCode_ListID
//setCustomersalestaxcodeFullname() - CustomerSalesTaxCode_FullName
//setSuggesteddiscountamount() - SuggestedDiscountAmount
//setSuggesteddiscountdate() - SuggestedDiscountDate
//setOther() - Other

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_ARAccount_ListID() - ARAccount_ListID
//set_ARAccount_FullName() - ARAccount_FullName
//set_Template_ListID() - Template_ListID
//set_Template_FullName() - Template_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_BillAddress_Addr1() - BillAddress_Addr1
//set_BillAddress_Addr2() - BillAddress_Addr2
//set_BillAddress_Addr3() - BillAddress_Addr3
//set_BillAddress_Addr4() - BillAddress_Addr4
//set_BillAddress_Addr5() - BillAddress_Addr5
//set_BillAddress_City() - BillAddress_City
//set_BillAddress_State() - BillAddress_State
//set_BillAddress_PostalCode() - BillAddress_PostalCode
//set_BillAddress_Country() - BillAddress_Country
//set_BillAddress_Note() - BillAddress_Note
//set_BillAddressBlock_Addr1() - BillAddressBlock_Addr1
//set_BillAddressBlock_Addr2() - BillAddressBlock_Addr2
//set_BillAddressBlock_Addr3() - BillAddressBlock_Addr3
//set_BillAddressBlock_Addr4() - BillAddressBlock_Addr4
//set_BillAddressBlock_Addr5() - BillAddressBlock_Addr5
//set_ShipAddress_Addr1() - ShipAddress_Addr1
//set_ShipAddress_Addr2() - ShipAddress_Addr2
//set_ShipAddress_Addr3() - ShipAddress_Addr3
//set_ShipAddress_Addr4() - ShipAddress_Addr4
//set_ShipAddress_Addr5() - ShipAddress_Addr5
//set_ShipAddress_City() - ShipAddress_City
//set_ShipAddress_State() - ShipAddress_State
//set_ShipAddress_PostalCode() - ShipAddress_PostalCode
//set_ShipAddress_Country() - ShipAddress_Country
//set_ShipAddress_Note() - ShipAddress_Note
//set_ShipAddressBlock_Addr1() - ShipAddressBlock_Addr1
//set_ShipAddressBlock_Addr2() - ShipAddressBlock_Addr2
//set_ShipAddressBlock_Addr3() - ShipAddressBlock_Addr3
//set_ShipAddressBlock_Addr4() - ShipAddressBlock_Addr4
//set_ShipAddressBlock_Addr5() - ShipAddressBlock_Addr5
//set_IsPending() - IsPending
//set_IsFinanceCharge() - IsFinanceCharge
//set_PONumber() - PONumber
//set_Terms_ListID() - Terms_ListID
//set_Terms_FullName() - Terms_FullName
//set_DueDate() - DueDate
//set_SalesRep_ListID() - SalesRep_ListID
//set_SalesRep_FullName() - SalesRep_FullName
//set_FOB() - FOB
//set_ShipDate() - ShipDate
//set_ShipMethod_ListID() - ShipMethod_ListID
//set_ShipMethod_FullName() - ShipMethod_FullName
//set_Subtotal() - Subtotal
//set_ItemSalesTax_ListID() - ItemSalesTax_ListID
//set_ItemSalesTax_FullName() - ItemSalesTax_FullName
//set_SalesTaxPercentage() - SalesTaxPercentage
//set_SalesTaxTotal() - SalesTaxTotal
//set_AppliedAmount() - AppliedAmount
//set_BalanceRemaining() - BalanceRemaining
//set_Memo() - Memo
//set_IsPaid() - IsPaid
//set_Currency_ListID() - Currency_ListID
//set_Currency_FullName() - Currency_FullName
//set_ExchangeRate() - ExchangeRate
//set_BalanceRemainingInHomeCurrency() - BalanceRemainingInHomeCurrency
//set_CustomerMsg_ListID() - CustomerMsg_ListID
//set_CustomerMsg_FullName() - CustomerMsg_FullName
//set_IsToBePrinted() - IsToBePrinted
//set_IsToBeEmailed() - IsToBeEmailed
//set_CustomerSalesTaxCode_ListID() - CustomerSalesTaxCode_ListID
//set_CustomerSalesTaxCode_FullName() - CustomerSalesTaxCode_FullName
//set_SuggestedDiscountAmount() - SuggestedDiscountAmount
//set_SuggestedDiscountDate() - SuggestedDiscountDate
//set_Other() - Other

*/
/* End of file Qb_invoice_model.php */
/* Location: ./application/models/Qb_invoice_model.php */
