<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_itemsalestaxgroup_itemsalestax_model Class
 *
 * Manipulates `qb_itemsalestaxgroup_itemsalestax` table on database

CREATE TABLE `qb_itemsalestaxgroup_itemsalestax` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ItemSalesTaxGroup_ListID` varchar(40) DEFAULT NULL,
  `ListID` varchar(40) DEFAULT NULL,
  `FullName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `ItemSalesTaxGroup_ListID` (`ItemSalesTaxGroup_ListID`),
  KEY `FullName` (`FullName`),
  KEY `ListID` (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `ItemSalesTaxGroup_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `FullName` varchar(255) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_itemsalestaxgroup_itemsalestax_model extends MY_Model {

	protected $qbxml_id;
	protected $ItemSalesTaxGroup_ListID;
	protected $ListID;
	protected $FullName;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_itemsalestaxgroup_itemsalestax';
		$this->_short_name = 'qb_itemsalestaxgroup_itemsalestax';
		$this->_fields = array("qbxml_id","ItemSalesTaxGroup_ListID","ListID","FullName");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ItemSalesTaxGroup_ListID -------------------------------------- 

	/** 
	* Sets a value to `ItemSalesTaxGroup_ListID` variable
	* @access public
	*/

	public function setItemsalestaxgroupListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTaxGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ItemSalesTaxGroup_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ItemSalesTaxGroup_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ItemSalesTaxGroup_ListID` variable
	* @access public
	*/

	public function getItemsalestaxgroupListid() {
		return $this->ItemSalesTaxGroup_ListID;
	}

	public function get_ItemSalesTaxGroup_ListID() {
		return $this->ItemSalesTaxGroup_ListID;
	}

	
// ------------------------------ End Field: ItemSalesTaxGroup_ListID --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: FullName -------------------------------------- 

	/** 
	* Sets a value to `FullName` variable
	* @access public
	*/

	public function setFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `FullName` variable
	* @access public
	*/

	public function getFullname() {
		return $this->FullName;
	}

	public function get_FullName() {
		return $this->FullName;
	}

	
// ------------------------------ End Field: FullName --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ItemSalesTaxGroup_ListID' => (object) array(
										'Field'=>'ItemSalesTaxGroup_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'FullName' => (object) array(
										'Field'=>'FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ItemSalesTaxGroup_ListID' => "ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `ItemSalesTaxGroup_ListID` varchar(40) NULL   ;",
			'ListID' => "ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `ListID` varchar(40) NULL   ;",
			'FullName' => "ALTER TABLE  `qb_itemsalestaxgroup_itemsalestax` ADD  `FullName` varchar(255) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setItemsalestaxgroupListid() - ItemSalesTaxGroup_ListID
//setListid() - ListID
//setFullname() - FullName

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ItemSalesTaxGroup_ListID() - ItemSalesTaxGroup_ListID
//set_ListID() - ListID
//set_FullName() - FullName

*/
/* End of file Qb_itemsalestaxgroup_itemsalestax_model.php */
/* Location: ./application/models/Qb_itemsalestaxgroup_itemsalestax_model.php */
