<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_workerscompcode_ratehistory_model Class
 *
 * Manipulates `qb_workerscompcode_ratehistory` table on database

CREATE TABLE `qb_workerscompcode_ratehistory` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WorkersCompCode_ListID` varchar(40) DEFAULT NULL,
  `Rate` decimal(13,5) DEFAULT NULL,
  `EffectiveDate` date DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `WorkersCompCode_ListID` (`WorkersCompCode_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `WorkersCompCode_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `EffectiveDate` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_workerscompcode_ratehistory_model extends MY_Model {

	protected $qbxml_id;
	protected $WorkersCompCode_ListID;
	protected $Rate;
	protected $EffectiveDate;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_workerscompcode_ratehistory';
		$this->_short_name = 'qb_workerscompcode_ratehistory';
		$this->_fields = array("qbxml_id","WorkersCompCode_ListID","Rate","EffectiveDate");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: WorkersCompCode_ListID -------------------------------------- 

	/** 
	* Sets a value to `WorkersCompCode_ListID` variable
	* @access public
	*/

	public function setWorkerscompcodeListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('WorkersCompCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_WorkersCompCode_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('WorkersCompCode_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `WorkersCompCode_ListID` variable
	* @access public
	*/

	public function getWorkerscompcodeListid() {
		return $this->WorkersCompCode_ListID;
	}

	public function get_WorkersCompCode_ListID() {
		return $this->WorkersCompCode_ListID;
	}

	
// ------------------------------ End Field: WorkersCompCode_ListID --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: EffectiveDate -------------------------------------- 

	/** 
	* Sets a value to `EffectiveDate` variable
	* @access public
	*/

	public function setEffectivedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EffectiveDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EffectiveDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EffectiveDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EffectiveDate` variable
	* @access public
	*/

	public function getEffectivedate() {
		return $this->EffectiveDate;
	}

	public function get_EffectiveDate() {
		return $this->EffectiveDate;
	}

	
// ------------------------------ End Field: EffectiveDate --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'WorkersCompCode_ListID' => (object) array(
										'Field'=>'WorkersCompCode_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EffectiveDate' => (object) array(
										'Field'=>'EffectiveDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'WorkersCompCode_ListID' => "ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `WorkersCompCode_ListID` varchar(40) NULL   ;",
			'Rate' => "ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `Rate` decimal(13,5) NULL   ;",
			'EffectiveDate' => "ALTER TABLE  `qb_workerscompcode_ratehistory` ADD  `EffectiveDate` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setWorkerscompcodeListid() - WorkersCompCode_ListID
//setRate() - Rate
//setEffectivedate() - EffectiveDate

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_WorkersCompCode_ListID() - WorkersCompCode_ListID
//set_Rate() - Rate
//set_EffectiveDate() - EffectiveDate

*/
/* End of file Qb_workerscompcode_ratehistory_model.php */
/* Location: ./application/models/Qb_workerscompcode_ratehistory_model.php */
