<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_charge_model Class
 *
 * Manipulates `qb_charge` table on database

CREATE TABLE `qb_charge` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IsPaid` text,
  `TxnID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `TxnNumber` int(10) unsigned DEFAULT '0',
  `Customer_ListID` varchar(40) DEFAULT NULL,
  `Customer_FullName` varchar(255) DEFAULT NULL,
  `TxnDate` date DEFAULT NULL,
  `RefNumber` text,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `Quantity` decimal(12,5) DEFAULT '0.00000',
  `Rate` decimal(13,5) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT NULL,
  `BalanceRemaining` decimal(10,2) DEFAULT NULL,
  `Descrip` text,
  `ARAccount_ListID` varchar(40) DEFAULT NULL,
  `ARAccount_FullName` varchar(255) DEFAULT NULL,
  `Class_ListID` varchar(40) DEFAULT NULL,
  `Class_FullName` varchar(255) DEFAULT NULL,
  `BilledDate` date DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `Customer_ListID` (`Customer_ListID`),
  KEY `TxnDate` (`TxnDate`),
  KEY `Item_ListID` (`Item_ListID`),
  KEY `ARAccount_ListID` (`ARAccount_ListID`),
  KEY `Class_ListID` (`Class_ListID`),
  KEY `TxnID` (`TxnID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_charge` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_charge` ADD  `IsPaid` text NULL   ;
ALTER TABLE  `qb_charge` ADD  `TxnID` varchar(40) NULL   ;
ALTER TABLE  `qb_charge` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_charge` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_charge` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_charge` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';
ALTER TABLE  `qb_charge` ADD  `Customer_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Customer_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_charge` ADD  `TxnDate` date NULL   ;
ALTER TABLE  `qb_charge` ADD  `RefNumber` text NULL   ;
ALTER TABLE  `qb_charge` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';
ALTER TABLE  `qb_charge` ADD  `Rate` decimal(13,5) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Amount` decimal(10,2) NULL   ;
ALTER TABLE  `qb_charge` ADD  `BalanceRemaining` decimal(10,2) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Descrip` text NULL   ;
ALTER TABLE  `qb_charge` ADD  `ARAccount_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_charge` ADD  `ARAccount_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Class_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_charge` ADD  `Class_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_charge` ADD  `BilledDate` date NULL   ;
ALTER TABLE  `qb_charge` ADD  `DueDate` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_charge_model extends MY_Model {

	protected $qbxml_id;
	protected $IsPaid;
	protected $TxnID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $TxnNumber;
	protected $Customer_ListID;
	protected $Customer_FullName;
	protected $TxnDate;
	protected $RefNumber;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $Quantity;
	protected $Rate;
	protected $Amount;
	protected $BalanceRemaining;
	protected $Descrip;
	protected $ARAccount_ListID;
	protected $ARAccount_FullName;
	protected $Class_ListID;
	protected $Class_FullName;
	protected $BilledDate;
	protected $DueDate;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_charge';
		$this->_short_name = 'qb_charge';
		$this->_fields = array("qbxml_id","IsPaid","TxnID","TimeCreated","TimeModified","EditSequence","TxnNumber","Customer_ListID","Customer_FullName","TxnDate","RefNumber","Item_ListID","Item_FullName","Quantity","Rate","Amount","BalanceRemaining","Descrip","ARAccount_ListID","ARAccount_FullName","Class_ListID","Class_FullName","BilledDate","DueDate");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: IsPaid -------------------------------------- 

	/** 
	* Sets a value to `IsPaid` variable
	* @access public
	*/

	public function setIspaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsPaid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsPaid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsPaid` variable
	* @access public
	*/

	public function getIspaid() {
		return $this->IsPaid;
	}

	public function get_IsPaid() {
		return $this->IsPaid;
	}

	
// ------------------------------ End Field: IsPaid --------------------------------------


// ---------------------------- Start Field: TxnID -------------------------------------- 

	/** 
	* Sets a value to `TxnID` variable
	* @access public
	*/

	public function setTxnid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnID` variable
	* @access public
	*/

	public function getTxnid() {
		return $this->TxnID;
	}

	public function get_TxnID() {
		return $this->TxnID;
	}

	
// ------------------------------ End Field: TxnID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: TxnNumber -------------------------------------- 

	/** 
	* Sets a value to `TxnNumber` variable
	* @access public
	*/

	public function setTxnnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnNumber` variable
	* @access public
	*/

	public function getTxnnumber() {
		return $this->TxnNumber;
	}

	public function get_TxnNumber() {
		return $this->TxnNumber;
	}

	
// ------------------------------ End Field: TxnNumber --------------------------------------


// ---------------------------- Start Field: Customer_ListID -------------------------------------- 

	/** 
	* Sets a value to `Customer_ListID` variable
	* @access public
	*/

	public function setCustomerListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_ListID` variable
	* @access public
	*/

	public function getCustomerListid() {
		return $this->Customer_ListID;
	}

	public function get_Customer_ListID() {
		return $this->Customer_ListID;
	}

	
// ------------------------------ End Field: Customer_ListID --------------------------------------


// ---------------------------- Start Field: Customer_FullName -------------------------------------- 

	/** 
	* Sets a value to `Customer_FullName` variable
	* @access public
	*/

	public function setCustomerFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Customer_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Customer_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Customer_FullName` variable
	* @access public
	*/

	public function getCustomerFullname() {
		return $this->Customer_FullName;
	}

	public function get_Customer_FullName() {
		return $this->Customer_FullName;
	}

	
// ------------------------------ End Field: Customer_FullName --------------------------------------


// ---------------------------- Start Field: TxnDate -------------------------------------- 

	/** 
	* Sets a value to `TxnDate` variable
	* @access public
	*/

	public function setTxndate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TxnDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TxnDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TxnDate` variable
	* @access public
	*/

	public function getTxndate() {
		return $this->TxnDate;
	}

	public function get_TxnDate() {
		return $this->TxnDate;
	}

	
// ------------------------------ End Field: TxnDate --------------------------------------


// ---------------------------- Start Field: RefNumber -------------------------------------- 

	/** 
	* Sets a value to `RefNumber` variable
	* @access public
	*/

	public function setRefnumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_RefNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('RefNumber', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `RefNumber` variable
	* @access public
	*/

	public function getRefnumber() {
		return $this->RefNumber;
	}

	public function get_RefNumber() {
		return $this->RefNumber;
	}

	
// ------------------------------ End Field: RefNumber --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: Quantity -------------------------------------- 

	/** 
	* Sets a value to `Quantity` variable
	* @access public
	*/

	public function setQuantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Quantity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Quantity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Quantity` variable
	* @access public
	*/

	public function getQuantity() {
		return $this->Quantity;
	}

	public function get_Quantity() {
		return $this->Quantity;
	}

	
// ------------------------------ End Field: Quantity --------------------------------------


// ---------------------------- Start Field: Rate -------------------------------------- 

	/** 
	* Sets a value to `Rate` variable
	* @access public
	*/

	public function setRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Rate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Rate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Rate` variable
	* @access public
	*/

	public function getRate() {
		return $this->Rate;
	}

	public function get_Rate() {
		return $this->Rate;
	}

	
// ------------------------------ End Field: Rate --------------------------------------


// ---------------------------- Start Field: Amount -------------------------------------- 

	/** 
	* Sets a value to `Amount` variable
	* @access public
	*/

	public function setAmount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Amount($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Amount', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Amount` variable
	* @access public
	*/

	public function getAmount() {
		return $this->Amount;
	}

	public function get_Amount() {
		return $this->Amount;
	}

	
// ------------------------------ End Field: Amount --------------------------------------


// ---------------------------- Start Field: BalanceRemaining -------------------------------------- 

	/** 
	* Sets a value to `BalanceRemaining` variable
	* @access public
	*/

	public function setBalanceremaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BalanceRemaining($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BalanceRemaining', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BalanceRemaining` variable
	* @access public
	*/

	public function getBalanceremaining() {
		return $this->BalanceRemaining;
	}

	public function get_BalanceRemaining() {
		return $this->BalanceRemaining;
	}

	
// ------------------------------ End Field: BalanceRemaining --------------------------------------


// ---------------------------- Start Field: Descrip -------------------------------------- 

	/** 
	* Sets a value to `Descrip` variable
	* @access public
	*/

	public function setDescrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Descrip($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Descrip', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Descrip` variable
	* @access public
	*/

	public function getDescrip() {
		return $this->Descrip;
	}

	public function get_Descrip() {
		return $this->Descrip;
	}

	
// ------------------------------ End Field: Descrip --------------------------------------


// ---------------------------- Start Field: ARAccount_ListID -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_ListID` variable
	* @access public
	*/

	public function setAraccountListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_ListID` variable
	* @access public
	*/

	public function getAraccountListid() {
		return $this->ARAccount_ListID;
	}

	public function get_ARAccount_ListID() {
		return $this->ARAccount_ListID;
	}

	
// ------------------------------ End Field: ARAccount_ListID --------------------------------------


// ---------------------------- Start Field: ARAccount_FullName -------------------------------------- 

	/** 
	* Sets a value to `ARAccount_FullName` variable
	* @access public
	*/

	public function setAraccountFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ARAccount_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ARAccount_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ARAccount_FullName` variable
	* @access public
	*/

	public function getAraccountFullname() {
		return $this->ARAccount_FullName;
	}

	public function get_ARAccount_FullName() {
		return $this->ARAccount_FullName;
	}

	
// ------------------------------ End Field: ARAccount_FullName --------------------------------------


// ---------------------------- Start Field: Class_ListID -------------------------------------- 

	/** 
	* Sets a value to `Class_ListID` variable
	* @access public
	*/

	public function setClassListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_ListID` variable
	* @access public
	*/

	public function getClassListid() {
		return $this->Class_ListID;
	}

	public function get_Class_ListID() {
		return $this->Class_ListID;
	}

	
// ------------------------------ End Field: Class_ListID --------------------------------------


// ---------------------------- Start Field: Class_FullName -------------------------------------- 

	/** 
	* Sets a value to `Class_FullName` variable
	* @access public
	*/

	public function setClassFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Class_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Class_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Class_FullName` variable
	* @access public
	*/

	public function getClassFullname() {
		return $this->Class_FullName;
	}

	public function get_Class_FullName() {
		return $this->Class_FullName;
	}

	
// ------------------------------ End Field: Class_FullName --------------------------------------


// ---------------------------- Start Field: BilledDate -------------------------------------- 

	/** 
	* Sets a value to `BilledDate` variable
	* @access public
	*/

	public function setBilleddate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BilledDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_BilledDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('BilledDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `BilledDate` variable
	* @access public
	*/

	public function getBilleddate() {
		return $this->BilledDate;
	}

	public function get_BilledDate() {
		return $this->BilledDate;
	}

	
// ------------------------------ End Field: BilledDate --------------------------------------


// ---------------------------- Start Field: DueDate -------------------------------------- 

	/** 
	* Sets a value to `DueDate` variable
	* @access public
	*/

	public function setDuedate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_DueDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('DueDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `DueDate` variable
	* @access public
	*/

	public function getDuedate() {
		return $this->DueDate;
	}

	public function get_DueDate() {
		return $this->DueDate;
	}

	
// ------------------------------ End Field: DueDate --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'IsPaid' => (object) array(
										'Field'=>'IsPaid',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnID' => (object) array(
										'Field'=>'TxnID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnNumber' => (object) array(
										'Field'=>'TxnNumber',
										'Type'=>'int(10) unsigned',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'Customer_ListID' => (object) array(
										'Field'=>'Customer_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Customer_FullName' => (object) array(
										'Field'=>'Customer_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TxnDate' => (object) array(
										'Field'=>'TxnDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'RefNumber' => (object) array(
										'Field'=>'RefNumber',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Quantity' => (object) array(
										'Field'=>'Quantity',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0.00000',
										'Extra'=>''
									),

			'Rate' => (object) array(
										'Field'=>'Rate',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Amount' => (object) array(
										'Field'=>'Amount',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BalanceRemaining' => (object) array(
										'Field'=>'BalanceRemaining',
										'Type'=>'decimal(10,2)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Descrip' => (object) array(
										'Field'=>'Descrip',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_ListID' => (object) array(
										'Field'=>'ARAccount_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'ARAccount_FullName' => (object) array(
										'Field'=>'ARAccount_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_ListID' => (object) array(
										'Field'=>'Class_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Class_FullName' => (object) array(
										'Field'=>'Class_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'BilledDate' => (object) array(
										'Field'=>'BilledDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'DueDate' => (object) array(
										'Field'=>'DueDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_charge` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'IsPaid' => "ALTER TABLE  `qb_charge` ADD  `IsPaid` text NULL   ;",
			'TxnID' => "ALTER TABLE  `qb_charge` ADD  `TxnID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_charge` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_charge` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_charge` ADD  `EditSequence` text NULL   ;",
			'TxnNumber' => "ALTER TABLE  `qb_charge` ADD  `TxnNumber` int(10) unsigned NULL   DEFAULT '0';",
			'Customer_ListID' => "ALTER TABLE  `qb_charge` ADD  `Customer_ListID` varchar(40) NULL   ;",
			'Customer_FullName' => "ALTER TABLE  `qb_charge` ADD  `Customer_FullName` varchar(255) NULL   ;",
			'TxnDate' => "ALTER TABLE  `qb_charge` ADD  `TxnDate` date NULL   ;",
			'RefNumber' => "ALTER TABLE  `qb_charge` ADD  `RefNumber` text NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_charge` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_charge` ADD  `Item_FullName` varchar(255) NULL   ;",
			'Quantity' => "ALTER TABLE  `qb_charge` ADD  `Quantity` decimal(12,5) NULL   DEFAULT '0.00000';",
			'Rate' => "ALTER TABLE  `qb_charge` ADD  `Rate` decimal(13,5) NULL   ;",
			'Amount' => "ALTER TABLE  `qb_charge` ADD  `Amount` decimal(10,2) NULL   ;",
			'BalanceRemaining' => "ALTER TABLE  `qb_charge` ADD  `BalanceRemaining` decimal(10,2) NULL   ;",
			'Descrip' => "ALTER TABLE  `qb_charge` ADD  `Descrip` text NULL   ;",
			'ARAccount_ListID' => "ALTER TABLE  `qb_charge` ADD  `ARAccount_ListID` varchar(40) NULL   ;",
			'ARAccount_FullName' => "ALTER TABLE  `qb_charge` ADD  `ARAccount_FullName` varchar(255) NULL   ;",
			'Class_ListID' => "ALTER TABLE  `qb_charge` ADD  `Class_ListID` varchar(40) NULL   ;",
			'Class_FullName' => "ALTER TABLE  `qb_charge` ADD  `Class_FullName` varchar(255) NULL   ;",
			'BilledDate' => "ALTER TABLE  `qb_charge` ADD  `BilledDate` date NULL   ;",
			'DueDate' => "ALTER TABLE  `qb_charge` ADD  `DueDate` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setIspaid() - IsPaid
//setTxnid() - TxnID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setTxnnumber() - TxnNumber
//setCustomerListid() - Customer_ListID
//setCustomerFullname() - Customer_FullName
//setTxndate() - TxnDate
//setRefnumber() - RefNumber
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setQuantity() - Quantity
//setRate() - Rate
//setAmount() - Amount
//setBalanceremaining() - BalanceRemaining
//setDescrip() - Descrip
//setAraccountListid() - ARAccount_ListID
//setAraccountFullname() - ARAccount_FullName
//setClassListid() - Class_ListID
//setClassFullname() - Class_FullName
//setBilleddate() - BilledDate
//setDuedate() - DueDate

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_IsPaid() - IsPaid
//set_TxnID() - TxnID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_TxnNumber() - TxnNumber
//set_Customer_ListID() - Customer_ListID
//set_Customer_FullName() - Customer_FullName
//set_TxnDate() - TxnDate
//set_RefNumber() - RefNumber
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_Quantity() - Quantity
//set_Rate() - Rate
//set_Amount() - Amount
//set_BalanceRemaining() - BalanceRemaining
//set_Descrip() - Descrip
//set_ARAccount_ListID() - ARAccount_ListID
//set_ARAccount_FullName() - ARAccount_FullName
//set_Class_ListID() - Class_ListID
//set_Class_FullName() - Class_FullName
//set_BilledDate() - BilledDate
//set_DueDate() - DueDate

*/
/* End of file Qb_charge_model.php */
/* Location: ./application/models/Qb_charge_model.php */
