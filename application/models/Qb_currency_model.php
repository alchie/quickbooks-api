<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_currency_model Class
 *
 * Manipulates `qb_currency` table on database

CREATE TABLE `qb_currency` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ListID` varchar(40) DEFAULT NULL,
  `TimeCreated` datetime DEFAULT NULL,
  `TimeModified` datetime DEFAULT NULL,
  `EditSequence` text,
  `Name` text,
  `IsActive` tinyint(1) DEFAULT '0',
  `CurrencyCode` text,
  `Currency_CurrencyFormat_ThousandSeparator` varchar(40) DEFAULT NULL,
  `Currency_CurrencyFormat_ThousandSeparatorGrouping` varchar(40) DEFAULT NULL,
  `Currency_CurrencyFormat_DecimalPlaces` varchar(40) DEFAULT NULL,
  `Currency_CurrencyFormat_DecimalSeparator` varchar(40) DEFAULT NULL,
  `IsUserDefinedCurrency` tinyint(1) DEFAULT '0',
  `ExchangeRate` text,
  `AsOfDate` date DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `IsActive` (`IsActive`),
  KEY `ListID` (`ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_currency` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_currency` ADD  `ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_currency` ADD  `TimeCreated` datetime NULL   ;
ALTER TABLE  `qb_currency` ADD  `TimeModified` datetime NULL   ;
ALTER TABLE  `qb_currency` ADD  `EditSequence` text NULL   ;
ALTER TABLE  `qb_currency` ADD  `Name` text NULL   ;
ALTER TABLE  `qb_currency` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_currency` ADD  `CurrencyCode` text NULL   ;
ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_ThousandSeparator` varchar(40) NULL   ;
ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_ThousandSeparatorGrouping` varchar(40) NULL   ;
ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_DecimalPlaces` varchar(40) NULL   ;
ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_DecimalSeparator` varchar(40) NULL   ;
ALTER TABLE  `qb_currency` ADD  `IsUserDefinedCurrency` tinyint(1) NULL   DEFAULT '0';
ALTER TABLE  `qb_currency` ADD  `ExchangeRate` text NULL   ;
ALTER TABLE  `qb_currency` ADD  `AsOfDate` date NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_currency_model extends MY_Model {

	protected $qbxml_id;
	protected $ListID;
	protected $TimeCreated;
	protected $TimeModified;
	protected $EditSequence;
	protected $Name;
	protected $IsActive;
	protected $CurrencyCode;
	protected $Currency_CurrencyFormat_ThousandSeparator;
	protected $Currency_CurrencyFormat_ThousandSeparatorGrouping;
	protected $Currency_CurrencyFormat_DecimalPlaces;
	protected $Currency_CurrencyFormat_DecimalSeparator;
	protected $IsUserDefinedCurrency;
	protected $ExchangeRate;
	protected $AsOfDate;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_currency';
		$this->_short_name = 'qb_currency';
		$this->_fields = array("qbxml_id","ListID","TimeCreated","TimeModified","EditSequence","Name","IsActive","CurrencyCode","Currency_CurrencyFormat_ThousandSeparator","Currency_CurrencyFormat_ThousandSeparatorGrouping","Currency_CurrencyFormat_DecimalPlaces","Currency_CurrencyFormat_DecimalSeparator","IsUserDefinedCurrency","ExchangeRate","AsOfDate");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: ListID -------------------------------------- 

	/** 
	* Sets a value to `ListID` variable
	* @access public
	*/

	public function setListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ListID` variable
	* @access public
	*/

	public function getListid() {
		return $this->ListID;
	}

	public function get_ListID() {
		return $this->ListID;
	}

	
// ------------------------------ End Field: ListID --------------------------------------


// ---------------------------- Start Field: TimeCreated -------------------------------------- 

	/** 
	* Sets a value to `TimeCreated` variable
	* @access public
	*/

	public function setTimecreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeCreated($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeCreated', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeCreated` variable
	* @access public
	*/

	public function getTimecreated() {
		return $this->TimeCreated;
	}

	public function get_TimeCreated() {
		return $this->TimeCreated;
	}

	
// ------------------------------ End Field: TimeCreated --------------------------------------


// ---------------------------- Start Field: TimeModified -------------------------------------- 

	/** 
	* Sets a value to `TimeModified` variable
	* @access public
	*/

	public function setTimemodified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_TimeModified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('TimeModified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `TimeModified` variable
	* @access public
	*/

	public function getTimemodified() {
		return $this->TimeModified;
	}

	public function get_TimeModified() {
		return $this->TimeModified;
	}

	
// ------------------------------ End Field: TimeModified --------------------------------------


// ---------------------------- Start Field: EditSequence -------------------------------------- 

	/** 
	* Sets a value to `EditSequence` variable
	* @access public
	*/

	public function setEditsequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_EditSequence($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('EditSequence', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `EditSequence` variable
	* @access public
	*/

	public function getEditsequence() {
		return $this->EditSequence;
	}

	public function get_EditSequence() {
		return $this->EditSequence;
	}

	
// ------------------------------ End Field: EditSequence --------------------------------------


// ---------------------------- Start Field: Name -------------------------------------- 

	/** 
	* Sets a value to `Name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Name($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Name` variable
	* @access public
	*/

	public function getName() {
		return $this->Name;
	}

	public function get_Name() {
		return $this->Name;
	}

	
// ------------------------------ End Field: Name --------------------------------------


// ---------------------------- Start Field: IsActive -------------------------------------- 

	/** 
	* Sets a value to `IsActive` variable
	* @access public
	*/

	public function setIsactive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsActive', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsActive` variable
	* @access public
	*/

	public function getIsactive() {
		return $this->IsActive;
	}

	public function get_IsActive() {
		return $this->IsActive;
	}

	
// ------------------------------ End Field: IsActive --------------------------------------


// ---------------------------- Start Field: CurrencyCode -------------------------------------- 

	/** 
	* Sets a value to `CurrencyCode` variable
	* @access public
	*/

	public function setCurrencycode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrencyCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CurrencyCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CurrencyCode', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CurrencyCode` variable
	* @access public
	*/

	public function getCurrencycode() {
		return $this->CurrencyCode;
	}

	public function get_CurrencyCode() {
		return $this->CurrencyCode;
	}

	
// ------------------------------ End Field: CurrencyCode --------------------------------------


// ---------------------------- Start Field: Currency_CurrencyFormat_ThousandSeparator -------------------------------------- 

	/** 
	* Sets a value to `Currency_CurrencyFormat_ThousandSeparator` variable
	* @access public
	*/

	public function setCurrencyCurrencyformatThousandseparator($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_ThousandSeparator', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_CurrencyFormat_ThousandSeparator($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_ThousandSeparator', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_CurrencyFormat_ThousandSeparator` variable
	* @access public
	*/

	public function getCurrencyCurrencyformatThousandseparator() {
		return $this->Currency_CurrencyFormat_ThousandSeparator;
	}

	public function get_Currency_CurrencyFormat_ThousandSeparator() {
		return $this->Currency_CurrencyFormat_ThousandSeparator;
	}

	
// ------------------------------ End Field: Currency_CurrencyFormat_ThousandSeparator --------------------------------------


// ---------------------------- Start Field: Currency_CurrencyFormat_ThousandSeparatorGrouping -------------------------------------- 

	/** 
	* Sets a value to `Currency_CurrencyFormat_ThousandSeparatorGrouping` variable
	* @access public
	*/

	public function setCurrencyCurrencyformatThousandseparatorgrouping($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_ThousandSeparatorGrouping', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_CurrencyFormat_ThousandSeparatorGrouping($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_ThousandSeparatorGrouping', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_CurrencyFormat_ThousandSeparatorGrouping` variable
	* @access public
	*/

	public function getCurrencyCurrencyformatThousandseparatorgrouping() {
		return $this->Currency_CurrencyFormat_ThousandSeparatorGrouping;
	}

	public function get_Currency_CurrencyFormat_ThousandSeparatorGrouping() {
		return $this->Currency_CurrencyFormat_ThousandSeparatorGrouping;
	}

	
// ------------------------------ End Field: Currency_CurrencyFormat_ThousandSeparatorGrouping --------------------------------------


// ---------------------------- Start Field: Currency_CurrencyFormat_DecimalPlaces -------------------------------------- 

	/** 
	* Sets a value to `Currency_CurrencyFormat_DecimalPlaces` variable
	* @access public
	*/

	public function setCurrencyCurrencyformatDecimalplaces($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_DecimalPlaces', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_CurrencyFormat_DecimalPlaces($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_DecimalPlaces', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_CurrencyFormat_DecimalPlaces` variable
	* @access public
	*/

	public function getCurrencyCurrencyformatDecimalplaces() {
		return $this->Currency_CurrencyFormat_DecimalPlaces;
	}

	public function get_Currency_CurrencyFormat_DecimalPlaces() {
		return $this->Currency_CurrencyFormat_DecimalPlaces;
	}

	
// ------------------------------ End Field: Currency_CurrencyFormat_DecimalPlaces --------------------------------------


// ---------------------------- Start Field: Currency_CurrencyFormat_DecimalSeparator -------------------------------------- 

	/** 
	* Sets a value to `Currency_CurrencyFormat_DecimalSeparator` variable
	* @access public
	*/

	public function setCurrencyCurrencyformatDecimalseparator($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_DecimalSeparator', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Currency_CurrencyFormat_DecimalSeparator($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Currency_CurrencyFormat_DecimalSeparator', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Currency_CurrencyFormat_DecimalSeparator` variable
	* @access public
	*/

	public function getCurrencyCurrencyformatDecimalseparator() {
		return $this->Currency_CurrencyFormat_DecimalSeparator;
	}

	public function get_Currency_CurrencyFormat_DecimalSeparator() {
		return $this->Currency_CurrencyFormat_DecimalSeparator;
	}

	
// ------------------------------ End Field: Currency_CurrencyFormat_DecimalSeparator --------------------------------------


// ---------------------------- Start Field: IsUserDefinedCurrency -------------------------------------- 

	/** 
	* Sets a value to `IsUserDefinedCurrency` variable
	* @access public
	*/

	public function setIsuserdefinedcurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsUserDefinedCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_IsUserDefinedCurrency($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('IsUserDefinedCurrency', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `IsUserDefinedCurrency` variable
	* @access public
	*/

	public function getIsuserdefinedcurrency() {
		return $this->IsUserDefinedCurrency;
	}

	public function get_IsUserDefinedCurrency() {
		return $this->IsUserDefinedCurrency;
	}

	
// ------------------------------ End Field: IsUserDefinedCurrency --------------------------------------


// ---------------------------- Start Field: ExchangeRate -------------------------------------- 

	/** 
	* Sets a value to `ExchangeRate` variable
	* @access public
	*/

	public function setExchangerate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_ExchangeRate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('ExchangeRate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `ExchangeRate` variable
	* @access public
	*/

	public function getExchangerate() {
		return $this->ExchangeRate;
	}

	public function get_ExchangeRate() {
		return $this->ExchangeRate;
	}

	
// ------------------------------ End Field: ExchangeRate --------------------------------------


// ---------------------------- Start Field: AsOfDate -------------------------------------- 

	/** 
	* Sets a value to `AsOfDate` variable
	* @access public
	*/

	public function setAsofdate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AsOfDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_AsOfDate($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('AsOfDate', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `AsOfDate` variable
	* @access public
	*/

	public function getAsofdate() {
		return $this->AsOfDate;
	}

	public function get_AsOfDate() {
		return $this->AsOfDate;
	}

	
// ------------------------------ End Field: AsOfDate --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'ListID' => (object) array(
										'Field'=>'ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeCreated' => (object) array(
										'Field'=>'TimeCreated',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'TimeModified' => (object) array(
										'Field'=>'TimeModified',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'EditSequence' => (object) array(
										'Field'=>'EditSequence',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Name' => (object) array(
										'Field'=>'Name',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsActive' => (object) array(
										'Field'=>'IsActive',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'0',
										'Extra'=>''
									),

			'CurrencyCode' => (object) array(
										'Field'=>'CurrencyCode',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_CurrencyFormat_ThousandSeparator' => (object) array(
										'Field'=>'Currency_CurrencyFormat_ThousandSeparator',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_CurrencyFormat_ThousandSeparatorGrouping' => (object) array(
										'Field'=>'Currency_CurrencyFormat_ThousandSeparatorGrouping',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_CurrencyFormat_DecimalPlaces' => (object) array(
										'Field'=>'Currency_CurrencyFormat_DecimalPlaces',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'Currency_CurrencyFormat_DecimalSeparator' => (object) array(
										'Field'=>'Currency_CurrencyFormat_DecimalSeparator',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'IsUserDefinedCurrency' => (object) array(
										'Field'=>'IsUserDefinedCurrency',
										'Type'=>'tinyint(1)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'ExchangeRate' => (object) array(
										'Field'=>'ExchangeRate',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'AsOfDate' => (object) array(
										'Field'=>'AsOfDate',
										'Type'=>'date',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_currency` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'ListID' => "ALTER TABLE  `qb_currency` ADD  `ListID` varchar(40) NULL   ;",
			'TimeCreated' => "ALTER TABLE  `qb_currency` ADD  `TimeCreated` datetime NULL   ;",
			'TimeModified' => "ALTER TABLE  `qb_currency` ADD  `TimeModified` datetime NULL   ;",
			'EditSequence' => "ALTER TABLE  `qb_currency` ADD  `EditSequence` text NULL   ;",
			'Name' => "ALTER TABLE  `qb_currency` ADD  `Name` text NULL   ;",
			'IsActive' => "ALTER TABLE  `qb_currency` ADD  `IsActive` tinyint(1) NULL   DEFAULT '0';",
			'CurrencyCode' => "ALTER TABLE  `qb_currency` ADD  `CurrencyCode` text NULL   ;",
			'Currency_CurrencyFormat_ThousandSeparator' => "ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_ThousandSeparator` varchar(40) NULL   ;",
			'Currency_CurrencyFormat_ThousandSeparatorGrouping' => "ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_ThousandSeparatorGrouping` varchar(40) NULL   ;",
			'Currency_CurrencyFormat_DecimalPlaces' => "ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_DecimalPlaces` varchar(40) NULL   ;",
			'Currency_CurrencyFormat_DecimalSeparator' => "ALTER TABLE  `qb_currency` ADD  `Currency_CurrencyFormat_DecimalSeparator` varchar(40) NULL   ;",
			'IsUserDefinedCurrency' => "ALTER TABLE  `qb_currency` ADD  `IsUserDefinedCurrency` tinyint(1) NULL   DEFAULT '0';",
			'ExchangeRate' => "ALTER TABLE  `qb_currency` ADD  `ExchangeRate` text NULL   ;",
			'AsOfDate' => "ALTER TABLE  `qb_currency` ADD  `AsOfDate` date NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setListid() - ListID
//setTimecreated() - TimeCreated
//setTimemodified() - TimeModified
//setEditsequence() - EditSequence
//setName() - Name
//setIsactive() - IsActive
//setCurrencycode() - CurrencyCode
//setCurrencyCurrencyformatThousandseparator() - Currency_CurrencyFormat_ThousandSeparator
//setCurrencyCurrencyformatThousandseparatorgrouping() - Currency_CurrencyFormat_ThousandSeparatorGrouping
//setCurrencyCurrencyformatDecimalplaces() - Currency_CurrencyFormat_DecimalPlaces
//setCurrencyCurrencyformatDecimalseparator() - Currency_CurrencyFormat_DecimalSeparator
//setIsuserdefinedcurrency() - IsUserDefinedCurrency
//setExchangerate() - ExchangeRate
//setAsofdate() - AsOfDate

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_ListID() - ListID
//set_TimeCreated() - TimeCreated
//set_TimeModified() - TimeModified
//set_EditSequence() - EditSequence
//set_Name() - Name
//set_IsActive() - IsActive
//set_CurrencyCode() - CurrencyCode
//set_Currency_CurrencyFormat_ThousandSeparator() - Currency_CurrencyFormat_ThousandSeparator
//set_Currency_CurrencyFormat_ThousandSeparatorGrouping() - Currency_CurrencyFormat_ThousandSeparatorGrouping
//set_Currency_CurrencyFormat_DecimalPlaces() - Currency_CurrencyFormat_DecimalPlaces
//set_Currency_CurrencyFormat_DecimalSeparator() - Currency_CurrencyFormat_DecimalSeparator
//set_IsUserDefinedCurrency() - IsUserDefinedCurrency
//set_ExchangeRate() - ExchangeRate
//set_AsOfDate() - AsOfDate

*/
/* End of file Qb_currency_model.php */
/* Location: ./application/models/Qb_currency_model.php */
