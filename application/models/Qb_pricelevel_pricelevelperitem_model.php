<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Qb_pricelevel_pricelevelperitem_model Class
 *
 * Manipulates `qb_pricelevel_pricelevelperitem` table on database

CREATE TABLE `qb_pricelevel_pricelevelperitem` (
  `qbxml_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PriceLevel_ListID` varchar(40) DEFAULT NULL,
  `Item_ListID` varchar(40) DEFAULT NULL,
  `Item_FullName` varchar(255) DEFAULT NULL,
  `CustomPrice` decimal(13,5) DEFAULT NULL,
  `CustomPricePercent` decimal(12,5) DEFAULT NULL,
  PRIMARY KEY (`qbxml_id`),
  KEY `PriceLevel_ListID` (`PriceLevel_ListID`),
  KEY `Item_ListID` (`Item_ListID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin;

ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `PriceLevel_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `Item_ListID` varchar(40) NULL   ;
ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `Item_FullName` varchar(255) NULL   ;
ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `CustomPrice` decimal(13,5) NULL   ;
ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `CustomPricePercent` decimal(12,5) NULL   ;


 * @package			        Model
 * @version_number	        6.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Qb_pricelevel_pricelevelperitem_model extends MY_Model {

	protected $qbxml_id;
	protected $PriceLevel_ListID;
	protected $Item_ListID;
	protected $Item_FullName;
	protected $CustomPrice;
	protected $CustomPricePercent;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'qb_pricelevel_pricelevelperitem';
		$this->_short_name = 'qb_pricelevel_pricelevelperitem';
		$this->_fields = array("qbxml_id","PriceLevel_ListID","Item_ListID","Item_FullName","CustomPrice","CustomPricePercent");
		$this->_required = array("");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: qbxml_id -------------------------------------- 

	/** 
	* Sets a value to `qbxml_id` variable
	* @access public
	*/

	public function setQbxmlId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_qbxml_id($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('qbxml_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `qbxml_id` variable
	* @access public
	*/

	public function getQbxmlId() {
		return $this->qbxml_id;
	}

	public function get_qbxml_id() {
		return $this->qbxml_id;
	}

	
// ------------------------------ End Field: qbxml_id --------------------------------------


// ---------------------------- Start Field: PriceLevel_ListID -------------------------------------- 

	/** 
	* Sets a value to `PriceLevel_ListID` variable
	* @access public
	*/

	public function setPricelevelListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_PriceLevel_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('PriceLevel_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `PriceLevel_ListID` variable
	* @access public
	*/

	public function getPricelevelListid() {
		return $this->PriceLevel_ListID;
	}

	public function get_PriceLevel_ListID() {
		return $this->PriceLevel_ListID;
	}

	
// ------------------------------ End Field: PriceLevel_ListID --------------------------------------


// ---------------------------- Start Field: Item_ListID -------------------------------------- 

	/** 
	* Sets a value to `Item_ListID` variable
	* @access public
	*/

	public function setItemListid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_ListID($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_ListID', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_ListID` variable
	* @access public
	*/

	public function getItemListid() {
		return $this->Item_ListID;
	}

	public function get_Item_ListID() {
		return $this->Item_ListID;
	}

	
// ------------------------------ End Field: Item_ListID --------------------------------------


// ---------------------------- Start Field: Item_FullName -------------------------------------- 

	/** 
	* Sets a value to `Item_FullName` variable
	* @access public
	*/

	public function setItemFullname($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_Item_FullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('Item_FullName', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `Item_FullName` variable
	* @access public
	*/

	public function getItemFullname() {
		return $this->Item_FullName;
	}

	public function get_Item_FullName() {
		return $this->Item_FullName;
	}

	
// ------------------------------ End Field: Item_FullName --------------------------------------


// ---------------------------- Start Field: CustomPrice -------------------------------------- 

	/** 
	* Sets a value to `CustomPrice` variable
	* @access public
	*/

	public function setCustomprice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomPrice($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomPrice', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomPrice` variable
	* @access public
	*/

	public function getCustomprice() {
		return $this->CustomPrice;
	}

	public function get_CustomPrice() {
		return $this->CustomPrice;
	}

	
// ------------------------------ End Field: CustomPrice --------------------------------------


// ---------------------------- Start Field: CustomPricePercent -------------------------------------- 

	/** 
	* Sets a value to `CustomPricePercent` variable
	* @access public
	*/

	public function setCustompricepercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomPricePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}

	public function set_CustomPricePercent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('CustomPricePercent', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `CustomPricePercent` variable
	* @access public
	*/

	public function getCustompricepercent() {
		return $this->CustomPricePercent;
	}

	public function get_CustomPricePercent() {
		return $this->CustomPricePercent;
	}

	
// ------------------------------ End Field: CustomPricePercent --------------------------------------



	
	public function get_table_options() {
		return array(
			'qbxml_id' => (object) array(
										'Field'=>'qbxml_id',
										'Type'=>'int(10) unsigned',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'PriceLevel_ListID' => (object) array(
										'Field'=>'PriceLevel_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_ListID' => (object) array(
										'Field'=>'Item_ListID',
										'Type'=>'varchar(40)',
										'Null'=>'YES',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'Item_FullName' => (object) array(
										'Field'=>'Item_FullName',
										'Type'=>'varchar(255)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomPrice' => (object) array(
										'Field'=>'CustomPrice',
										'Type'=>'decimal(13,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'CustomPricePercent' => (object) array(
										'Field'=>'CustomPricePercent',
										'Type'=>'decimal(12,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'qbxml_id' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `qbxml_id` int(10) unsigned NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'PriceLevel_ListID' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `PriceLevel_ListID` varchar(40) NULL   ;",
			'Item_ListID' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `Item_ListID` varchar(40) NULL   ;",
			'Item_FullName' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `Item_FullName` varchar(255) NULL   ;",
			'CustomPrice' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `CustomPrice` decimal(13,5) NULL   ;",
			'CustomPricePercent' => "ALTER TABLE  `qb_pricelevel_pricelevelperitem` ADD  `CustomPricePercent` decimal(12,5) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->_db->query( $column[$field_name] );
		}
	}

}
/*
//setQbxmlId() - qbxml_id
//setPricelevelListid() - PriceLevel_ListID
//setItemListid() - Item_ListID
//setItemFullname() - Item_FullName
//setCustomprice() - CustomPrice
//setCustompricepercent() - CustomPricePercent

--------------------------------------

//set_qbxml_id() - qbxml_id
//set_PriceLevel_ListID() - PriceLevel_ListID
//set_Item_ListID() - Item_ListID
//set_Item_FullName() - Item_FullName
//set_CustomPrice() - CustomPrice
//set_CustomPricePercent() - CustomPricePercent

*/
/* End of file Qb_pricelevel_pricelevelperitem_model.php */
/* Location: ./application/models/Qb_pricelevel_pricelevelperitem_model.php */
