<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('qbapi_get_settings'))
{
    function qbapi_get_settings($section, $key)
    {
       $CI =& get_instance();
       $CI->load->model('Qb_api_settings_model');
       $settings = new $CI->Qb_api_settings_model();
       $settings->setSection($section,true);
       $settings->setSkey($key,true);
        if( $settings->nonEmpty() ) {
          $data = $settings->get();
          return $data->svalue;
        } else {
          return '';
        }
    }
}

if ( ! function_exists('qbapi_save_settings'))
{
    function qbapi_save_settings($section, $key, $value='')
    {
       $CI =& get_instance();
       $CI->load->model('Qb_api_settings_model');
       $settings = new $CI->Qb_api_settings_model();
       $settings->setSection($section,true);
       $settings->setSkey($key,true);
       $settings->setSvalue($value);
       if( $settings->nonEmpty() ) {
          $settings->update();
       } else {
          $settings->insert();
       }
    }
}

if ( ! function_exists('php_error_log'))
{
    function php_error_log($var, $name='Error')
    {
        // Debug
        ob_start();
        echo "- - - - {$name} start - - - -\n\n";
        echo "-------------------------------------print_r\n";
        print_r( $var );
        echo "\n-------------------------------------print_r\n\n";
        echo "-------------------------------------var_dump\n";
        var_dump( $var );
        echo "\n-------------------------------------var_dump\n\n";
        echo "- - - - {$name} end - - - -\n\n";
        $results = ob_get_clean();
        error_log($results,0);
        // Debug
    }
}

// ------------------------------------------------------------------------