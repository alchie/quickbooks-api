<?php

$package_bonus = (1*$this->config->item('mlm_package1_bonus'));
$watch_bonus = (1*$this->config->item('mlm_package1_watch_bonus')*$this->config->item('mlm_package1_total_videos'));
$unilevel_bonus = ($current_package->total_package_network*$this->config->item('mlm_package1_level_bonus'));
$paring_number = ($current_package->total_package_network_right <= $current_package->total_package_network_left) ? $current_package->total_package_network_right : $current_package->total_package_network_left;
$pairing_bonus = ($paring_number*$this->config->item('mlm_package1_pairing_bonus'));

$total_earnings = ($package_bonus+$watch_bonus+$unilevel_bonus+$pairing_bonus);
$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;
?>
<!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $current_package->Item_FullName; ?></h3>
              <h5 class="widget-user-desc">
                Package ID: <span class="badge"><?php echo $current_package->TxnLineID; ?></span><br>
                <!-- Team: <span class="badge"><?php echo $current_package->team; ?></span> -->
              </h5>
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo number_format($total_balance,2); ?></h5>
                    <span class="description-text">TOTAL PACKAGE BONUS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="<?php echo site_url("mlm_members/package/{$current_package->TxnLineID}"); ?>">Dashboard</a></li>
                <li><a href="<?php echo site_url("mlm_packages/genealogy/{$current_package->TxnLineID}"); ?>">Genealogy</a></li>
 
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->