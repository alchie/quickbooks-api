<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Account Summary
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li class="active"><strong>Account Summary</strong></li>
      </ol>
    </section>

<?php
$signup_bonus = $customer->signup_bonus; //$this->config->item('mlm_signup_bonus');
$watch_bonus = $customer->watch_bonus; //($this->config->item('mlm_watch_bonus')*$this->config->item('mlm_total_videos'));
$referral_bonus = ($customer->referrals*$this->config->item('mlm_referral_bonus'));
$unilevel_bonus = ($customer->total_network*$this->config->item('mlm_level_bonus'));
$paring_number = ($customer->total_network_right <= $customer->total_network_left) ? $customer->total_network_right : $customer->total_network_left;
$pairing_bonus = ($paring_number*$this->config->item('mlm_pairing_bonus'));

$sales_bonus = $customer->sales_bonus;
$unilevel_sales_bonus = $customer->mlm_sales_bonus;
$random_bonus = $customer->random_bonus;

$package_bonus = 0;
foreach($packages as $package) { 
  $package_bonus += $this->config->item('mlm_package1_bonus');
  $package_bonus += ($this->config->item('mlm_package1_watch_bonus') * $this->config->item('mlm_package1_total_videos') );

  $unilevel_package_bonus = ($package->total_package_network*$this->config->item('mlm_package1_level_bonus'));
  $paring_package_number = ($package->total_package_network_right <= $package->total_package_network_left) ? $package->total_package_network_right : $package->total_package_network_left;
  $pairing_package_bonus = ($paring_package_number*$this->config->item('mlm_package1_pairing_bonus'));

  $package_bonus += $unilevel_package_bonus + $pairing_package_bonus;

}

$total_earnings = ($signup_bonus+$package_bonus+$watch_bonus+$referral_bonus+$unilevel_bonus+$pairing_bonus+$sales_bonus+$unilevel_sales_bonus+$random_bonus);

$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;

?>
    <!-- Main content -->
   <section class="content">
      
<div class="row">
        <div class="col-md-3">
          
          <?php $this->load->view('mlm/members/member_left_widget'); ?>
          
        </div>
        <div class="col-md-9">
            
<div class="row">
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-handshake-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SIGNUP BONUS</span>
              <span class="info-box-number"><?php echo number_format($signup_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-play"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">WATCH BONUS</span>
              <span class="info-box-number"><?php echo number_format($watch_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">REFERRAL BONUS</span>
              <span class="info-box-number"><?php echo number_format($referral_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sitemap"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MULTI-LEVEL BONUS</span>
              <span class="info-box-number"><?php echo number_format($unilevel_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-braille"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PAIRING BONUS</span>
              <span class="info-box-number"><?php echo number_format($pairing_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PACKAGE BONUS</span>
              <span class="info-box-number"><?php echo number_format($package_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
    <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">SALES BONUS</span>
              <span class="info-box-number"><?php echo number_format($customer->sales_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
    <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cart-arrow-down"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MULTI-LEVEL SALES BONUS</span>
              <span class="info-box-number"><?php echo number_format($customer->mlm_sales_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
    <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-buysellads"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">RANDOM BONUS</span>
              <span class="info-box-number"><?php echo number_format($customer->random_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>

      <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sitemap"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_members/recalculate_network/{$customer->ListID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">TOTAL DOWNLINES</span>
              <span class="info-box-number"><?php echo $customer->total_network; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

      <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-braille"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_members/recalculate_left_side/{$customer->ListID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">LEFT DOWNLINES</span>
              <span class="info-box-number"><?php echo $customer->total_network_left; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

      <div class="col-md-4">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-braille"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_members/recalculate_right_side/{$customer->ListID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">RIGHT DOWNLINES</span>
              <span class="info-box-number"><?php echo $customer->total_network_right; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

</div>





        </div>
    </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>