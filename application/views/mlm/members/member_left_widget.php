<?php
$signup_bonus = $customer->signup_bonus; //$this->config->item('mlm_signup_bonus');
$watch_bonus = $customer->watch_bonus; //($this->config->item('mlm_watch_bonus')*$this->config->item('mlm_total_videos'));
$referral_bonus = ($customer->referrals*$this->config->item('mlm_referral_bonus'));
$unilevel_bonus = ($customer->total_network*$this->config->item('mlm_level_bonus'));
$paring_number = ($customer->total_network_right <= $customer->total_network_left) ? $customer->total_network_right : $customer->total_network_left;
$pairing_bonus = ($paring_number*$this->config->item('mlm_pairing_bonus'));

$sales_bonus = $customer->sales_bonus;
$unilevel_sales_bonus = $customer->mlm_sales_bonus;
$random_bonus = $customer->random_bonus;

$package_bonus = 0;
foreach($packages as $package) {
  $package_bonus += $this->config->item('mlm_package1_bonus');
  $package_bonus += ($this->config->item('mlm_package1_watch_bonus')*$this->config->item('mlm_package1_total_videos'));

    $unilevel_package_bonus = ($package->total_package_network*$this->config->item('mlm_package1_level_bonus'));
  $paring_package_number = ($package->total_package_network_right <= $package->total_package_network_left) ? $package->total_package_network_right : $package->total_package_network_left;
  $pairing_package_bonus = ($paring_package_number*$this->config->item('mlm_package1_pairing_bonus'));

  $package_bonus += $unilevel_package_bonus + $pairing_package_bonus;
  
}

$total_earnings = ($signup_bonus+$package_bonus+$watch_bonus+$referral_bonus+$unilevel_bonus+$pairing_bonus+$sales_bonus+$unilevel_sales_bonus+$random_bonus);

$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;

?>
<!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $customer->Name; ?></h3>
              <h5 class="widget-user-desc">
                ID: <span class="badge"><?php echo $customer->ListID; ?></span><br>
                Team: <span class="badge"><?php echo $customer->team; ?></span>
              </h5>
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-12">
                  <div class="description-block">
                    <h5 class="description-header"><?php echo number_format($total_balance,2); ?></h5>
                    <span class="description-text">ACCOUNT BALANCE</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>">Dashboard</a></li>
                <li><a href="<?php echo site_url("mlm_members/genealogy/{$customer->ListID}"); ?>">Genealogy</a></li>
                <li><a href="<?php echo site_url("mlm_members/packages/{$customer->ListID}"); ?>">Packages <span class="pull-right badge bg-blue"><?php echo $customer->packages; ?></span></a></li>
                <li><a href="<?php echo site_url("mlm_members/referrals/{$customer->ListID}"); ?>">Referrals <span class="pull-right badge bg-red"><?php echo $customer->referrals; ?></span></a></li>
                <li><a href="<?php echo site_url("mlm_members/payouts/{$customer->ListID}"); ?>">Payouts</a></li>
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->