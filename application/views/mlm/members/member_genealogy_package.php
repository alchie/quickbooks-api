<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Genealogy
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li><a href="<?php echo site_url("mlm_members/packages/{$customer->ListID}"); ?>">Packages</a></li>
        <li class="active"><strong><?php echo $current_package->Item_FullName; ?></strong></li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
<style>
  .current_package {
    margin-top: 30px;
  }
  .center-line {
    margin-top: -15px;
  }
  .line-left {
    border-left: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-right {
    border-right: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-top {
    border-top: 1px solid #000;
  }
</style>
<div class="row">
        <div class="col-md-12">
            
<div class="row current_package">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">
                <?php if( $sublevel_packages ) { ?>
                <a class="pull-right" href="<?php echo site_url(uri_string()); ?>"><i class="fa fa-sitemap"></i></a>
              <?php } ?>
                <span class="info-box-text">MAIN PACKAGE</span>
                <span class="info-box-number"><?php echo $current_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $current_package->team; ?></span> <span class="badge">ID: <?php echo $current_package->TxnLineID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>

<?php if( $left_package || $right_package ) { ?>
<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>
<?php } ?>

<?php 
$sublvl = 0;
$subs = array();
if( $sublevel_packages ) { 
  foreach($sublevel_packages as $sublvl=>$subpack) {
    $subs[] = $subpack->TxnLineID;
?>

<div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($subpack->team!=$current_package->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">
                <a class="pull-right" href="<?php echo site_url(uri_string()) . "?sub=" . implode(",",$subs); ?>"><i class="fa fa-sitemap"></i></a>
                <span class="info-box-text">SUB PACKAGE (LEVEL <?php echo $sublvl; ?>)</span>
                <span class="info-box-number"><?php echo $subpack->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $subpack->team; ?></span> <span class="badge">ID: <?php echo $subpack->TxnLineID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>

<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>


<?php } } ?>

<div class="row hidden-xs hidden-sm">
<?php if( $left_package ) { ?>
        <div class="col-md-3 col-md-offset-3 line-left line-top"></div>
<?php } else { ?>
        <div class="col-md-3 col-md-offset-3"></div>
<?php } ?>
<?php if( $right_package ) { ?>
        <div class="col-md-3 line-right line-top"></div>
<?php } ?>
</div>
<div class="row">
        <div class="col-md-4 col-md-offset-1">
<?php if( $left_package ) { ?>
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($left_package->team!=$current_package->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">
                <a class="pull-right" href="<?php echo site_url("mlm_members/packages/{$left_package->Customer_ListID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl + 1; ?> (LEFT)</span>
                <span class="info-box-number"><?php echo $left_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $left_package->team; ?></span> <span class="badge">ID: <?php echo $left_package->TxnLineID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-2">
<?php if( $right_package ) { ?>
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($right_package->team!=$current_package->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-cube"></i></span>
              <div class="info-box-content">
                <a class="pull-right" href="<?php echo site_url("mlm_members/packages/{$right_package->Customer_ListID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl + 1; ?> (RIGHT)</span>
                <span class="info-box-number"><?php echo $right_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $right_package->team; ?></span> <span class="badge">ID: <?php echo $right_package->TxnLineID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
</div>

<div class="row hidden-xs hidden-sm">
<?php if( $left_package && ($left_package->downlines > 0) ) { ?>
        <div class="col-md-3 col-md-offset-3 line-left center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_members/genealogy/{$current_package->TxnLineID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $left_package->TxnLineID; ?>" class="badge" style="position: absolute;left: -10px;bottom: -10px;"><?php echo $left_package->downlines; ?></a>
        </div>
<?php } else { ?>
  <div class="col-md-3 col-md-offset-3" style="position: relative;">
  </div>
<?php } ?>
<?php if( $right_package && ($right_package->downlines > 0) ) { ?>
        <div class="col-md-3 line-right center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_members/genealogy/{$current_package->TxnLineID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $right_package->TxnLineID; ?>" class="badge" style="position: absolute;right: -10px;bottom: -10px;"><?php echo $right_package->downlines; ?></a>
        </div>
<?php } ?>
</div>


        </div>
</div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>