<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Genealogy
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li class="active"><strong>Genealogy</strong></li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
<style>
  .current_package {
    margin-top: 30px;
  }
  .center-line {
    margin-top: -15px;
  }
  .line-left {
    border-left: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-right {
    border-right: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-top {
    border-top: 1px solid #000;
  }
</style>
<div class="row">
        <div class="col-md-12">
            
<div class="row current_package">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>

              <div class="info-box-content">

              

              <a style="margin-left: 3px;" class="pull-right" href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><i class="fa fa-user"></i></a>

              <?php if( $sublevels ) { ?>
                <a class="pull-right" href="<?php echo site_url(uri_string()); ?>"><i class="fa fa-sitemap"></i></a>
              <?php } ?>
                <span class="info-box-text">YOU 
                  <?php if( ($customer->parent_id) && ($customer->parent_id !== 'ROOT_MEMBER') ) { ?>
                  <a style="margin-left: 3px;" href="<?php echo site_url("mlm_members/genealogy/{$customer->parent_id}"); ?>"><i class="fa fa-arrow-up"></i></a>
                <?php } ?>
                </span>
                <span class="info-box-number"><?php echo $customer->Name; ?></span>
                <p><span class="badge">TEAM: <?php echo $customer->team; ?></span> <span class="badge">ID: <?php echo $customer->ListID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>

<?php if( $left_downline || $right_downline ) { ?>
<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>
<?php } ?>

<?php 
$sublvl = 1;
$subs = array();
if( $sublevels ) { 
  foreach($sublevels as $sublvl=>$subl) { 
    $subs[] = $subl->ListID;
    $sublvl++;
?>

<div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($subl->team!=$customer->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-user"></i></span>

              <div class="info-box-content">
                <a style="margin-left: 3px;" class="pull-right" href="<?php echo site_url("mlm_members/summary/{$subl->ListID}"); ?>"><i class="fa fa-user"></i></a>
                <a class="pull-right" href="<?php echo site_url(uri_string()) . "?sub=" . implode(",",$subs); ?>"><i class="fa fa-sitemap"></i></a>
                <span class="info-box-text">SUB (LEVEL <?php echo $sublvl; ?>)
<a href="<?php echo site_url("mlm_members/genealogy/{$subl->ListID}"); ?>"><i class="fa fa-play"></i></a>
                </span>
                <span class="info-box-number"><?php echo $subl->Name; ?></span>
                <p><span class="badge">TEAM: <?php echo $subl->team; ?></span> <span class="badge">ID: <?php echo $subl->ListID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>
<?php if( $left_downline || $right_downline ) { ?>
<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>
<?php } ?>

<?php } } ?>

<div class="row hidden-xs hidden-sm">
<?php if( $left_downline ) { ?>
        <div class="col-md-3 col-md-offset-3 line-left line-top"></div>
<?php } else { ?>
        <div class="col-md-3 col-md-offset-3"></div>
<?php } ?>
<?php if( $right_downline ) { ?>
        <div class="col-md-3 line-right line-top"></div>
<?php } ?>
</div>
<div class="row">
        <div class="col-md-4 col-md-offset-1">
<?php if( $left_downline ) {  ?>
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($left_downline->team!=$customer->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-user"></i></span>

              <div class="info-box-content">
                <a class="pull-right" href="<?php echo site_url("mlm_members/summary/{$left_downline->ListID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl + 1; ?> (LEFT)
<a href="<?php echo site_url("mlm_members/genealogy/{$left_downline->ListID}"); ?>"><i class="fa fa-play"></i></a>
                </span>
                <span class="info-box-number"><?php echo $left_downline->Name; ?></span>
                <p><span class="badge">TEAM: <?php echo $left_downline->team; ?></span> <span class="badge">ID: <?php echo $left_downline->ListID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-2">
<?php if( $right_downline ) {  ?>
            <div class="info-box">
              <span class="info-box-icon bg-<?php echo ($right_downline->team!=$customer->team) ? 'red' : 'aqua'; ?>"><i class="fa fa-user"></i></span>
              <div class="info-box-content">
                <a class="pull-right" href="<?php echo site_url("mlm_members/summary/{$right_downline->ListID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl + 1; ?> (RIGHT)
<a href="<?php echo site_url("mlm_members/genealogy/{$right_downline->ListID}"); ?>"><i class="fa fa-play"></i></a>
                </span>
                <span class="info-box-number"><?php echo $right_downline->Name; ?></span>
                <p><span class="badge">TEAM: <?php echo $right_downline->team; ?></span> <span class="badge">ID: <?php echo $right_downline->ListID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
</div>

<div class="row hidden-xs hidden-sm">
<?php if( $left_downline && ($left_downline->downlines > 0) ) { ?>
        <div class="col-md-3 col-md-offset-3 line-left center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_members/genealogy/{$customer->ListID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $left_downline->ListID; ?>" class="badge" style="position: absolute;left: -10px;bottom: -10px;"><?php echo $left_downline->downlines; ?></a>
        </div>
<?php } else { ?>
  <div class="col-md-3 col-md-offset-3" style="position: relative;">
  </div>
<?php } ?>

<?php if( $right_downline && ($right_downline->downlines > 0) ) { ?>
        <div class="col-md-3 line-right center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_members/genealogy/{$customer->ListID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $right_downline->ListID; ?>" class="badge" style="position: absolute;right: -10px;bottom: -10px;"><?php echo $right_downline->downlines; ?></a>
        </div>
<?php } ?>

</div>


        </div>
</div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>