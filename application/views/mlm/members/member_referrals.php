<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Referrals
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li class="active">Packages</li>
      </ol>
    </section>

<?php
$signup_bonus = $this->config->item('mlm_signup_bonus');
$referral_bonus = ($customer->referrals*$this->config->item('mlm_package_bonus'));
$watch_bonus = ($customer->referrals*$this->config->item('mlm_watch_bonus')*$this->config->item('mlm_total_videos'));
$referral_bonus = ($customer->referrals*$this->config->item('mlm_referral_bonus'));

$total_earnings = ($signup_bonus+$referral_bonus+$watch_bonus+$referral_bonus);
$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;
?>
    <!-- Main content -->
   <section class="content">
      
<div class="row">
        <div class="col-md-3">
          
          <?php $this->load->view('mlm/members/member_left_widget'); ?>
          
        </div>
        <div class="col-md-9">
            
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $referrals ) { ?>
              <h3 class="box-title">Referrals <small class="badge"><?php echo number_format($count,0); ?></small></h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("mlm_members/referrals/{$customer->ListID}"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get("q"); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
<?php if( $referrals ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>Customer Name</th>
                  <th>Customer ID</th>
                </tr>
<?php foreach($referrals as $referral) { //print_r($referral); ?>
                <tr>
                  <td><?php echo $referral->Name; ?></td>
                  <td><span class="badge"><?php echo $referral->ListID; ?></span></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Referral Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div><!--end-->


        </div>
    </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>