<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        MLM Members
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">MLM Members</li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $mlm_members ) { ?>
              <h3 class="box-title">MLM Members <small>(<?php echo number_format($count,0); ?>)</small> <a href="<?php echo site_url(uri_string()); ?>?show=main_package"><span class="fa fa-exclamation-triangle"></span></a></h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("mlm_members"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get("q"); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
<?php if( $mlm_members ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>Name</th>
                  <th class="text-center">Downlines</th>
                  <th class="text-center">Packages</th>
                  <th class="text-center">Referrals</th>
                  <th class="text-right">Actions</th>
                </tr>
<?php foreach($mlm_members as $member) { //print_r($member); ?>
                <tr class="<?php echo ($member->parent) ? '': 'success'; ?>">
                  <td><?php echo $member->Customer_FullName; ?></td>

                  <td class="text-center"><a href="<?php echo site_url("mlm_members/genealogy/{$member->Customer_ListID}"); ?>"><?php echo $member->downlines; ?></a></td>
                  
                  <td class="text-center"><a href="<?php echo site_url("mlm_members/packages/{$member->Customer_ListID}"); ?>"><?php echo $member->packages; ?></a></td>
                  
                  <td class="text-center"><a href="<?php echo site_url("mlm_members/referrals/{$member->Customer_ListID}"); ?>"><?php echo $member->referrals; ?></a></td>
                  <td class="text-right"><a href="<?php echo site_url("mlm_members/summary/{$member->Customer_ListID}"); ?>" class="btn btn-success btn-xs">View</a></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Member Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>