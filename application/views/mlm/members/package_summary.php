<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Package Summary
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li class="active">Package Summary</li>
      </ol>
    </section>

<?php

$package_bonus = (1*$this->config->item('mlm_package1_bonus'));
$watch_bonus = (1*$this->config->item('mlm_package1_watch_bonus')*$this->config->item('mlm_package1_total_videos'));
$referral_bonus = ($customer->referrals*$this->config->item('mlm_referral_bonus'));
$unilevel_bonus = ($current_package->total_package_network*$this->config->item('mlm_package1_level_bonus'));
$paring_number = ($current_package->total_package_network_right <= $current_package->total_package_network_left) ? $current_package->total_package_network_right : $current_package->total_package_network_left;
$pairing_bonus = ($paring_number*$this->config->item('mlm_package1_pairing_bonus'));

$total_earnings = ($package_bonus+$watch_bonus+$referral_bonus+$unilevel_bonus+$pairing_bonus);
$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;
?>
    <!-- Main content -->
   <section class="content">
      
<div class="row">
        <div class="col-md-3">
          
          <?php $this->load->view('mlm/members/member_left_widget'); ?>
          
        </div>
        <div class="col-md-3">
          
          <?php $this->load->view('mlm/members/package_left_widget'); ?>
          
        </div>
        <div class="col-md-6">
            
<div class="row">

  <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PACKAGE BONUS</span>
              <span class="info-box-number"><?php echo number_format($package_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-play"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">WATCH BONUS</span>
              <span class="info-box-number"><?php echo number_format($watch_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>

  <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">MULTI-LEVEL BONUS</span>
              <span class="info-box-number"><?php echo number_format($unilevel_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>
  <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">PAIRING BONUS</span>
              <span class="info-box-number"><?php echo number_format($pairing_bonus,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
  </div>

       <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-sitemap"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_packages/recalculate_network/{$current_package->TxnLineID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">TOTAL DOWNLINES</span>
              <span class="info-box-number"><?php echo $current_package->total_package_network; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

      <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-braille"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_packages/recalculate_left_side/{$current_package->TxnLineID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">LEFT DOWNLINES</span>
              <span class="info-box-number"><?php echo $current_package->total_package_network_left; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>

      <div class="col-md-6">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-braille"></i></span>

            <div class="info-box-content">

              <a class="pull-right" href="<?php echo site_url("mlm_packages/recalculate_right_side/{$current_package->TxnLineID}") ; ?>"><i class="fa fa-refresh"></i></a>

              <span class="info-box-text">RIGHT DOWNLINES</span>
              <span class="info-box-number"><?php echo $current_package->total_package_network_right; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>
</div>





        </div>
    </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>