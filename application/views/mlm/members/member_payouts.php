<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payouts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li class="active">Payouts</li>
      </ol>
    </section>

<?php
$signup_bonus = $this->config->item('mlm_signup_bonus');
$package_bonus = ($customer->packages*$this->config->item('mlm_package_bonus'));
$watch_bonus = ($customer->packages*$this->config->item('mlm_watch_bonus')*$this->config->item('mlm_total_videos'));
$referral_bonus = ($customer->referrals*$this->config->item('mlm_referral_bonus'));

$total_earnings = ($signup_bonus+$package_bonus+$watch_bonus+$referral_bonus);
$total_cashouts = 0;
$total_balance = $total_earnings-$total_cashouts;
?>
    <!-- Main content -->
   <section class="content">
      
<div class="row">
        <div class="col-md-3">
          
          <?php $this->load->view('mlm/members/member_left_widget'); ?>
          
        </div>
        <div class="col-md-9">
            



        </div>
    </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>