<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        MLM Packages
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">MLM Packages</li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $mlm_packages ) { ?>
              <h3 class="box-title">MLM Packages <small>(<?php echo number_format($count,0); ?>)</small> <a href="<?php echo site_url(uri_string()); ?>?show=main_package"><span class="fa fa-exclamation-triangle"></span></a></h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("mlm_members"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get("q"); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
<?php if( $mlm_packages ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>Customer Name</th>
                  <th>Package</th>
                  <th class="text-center">Team</th>
                  <th class="text-center">Downlines</th>
                  <th class="text-right"></th>
                </tr>
<?php foreach($mlm_packages as $package) { //print_r($package); ?>
                <tr class="<?php echo ($package->parent_id) ? '' : 'danger'; ?>">
                  <td><a href="<?php echo site_url("mlm_members/summary/{$package->Customer_ListID}"); ?>"><?php echo $package->Customer_FullName; ?></a></td>
                  <td><a href="<?php echo site_url("mlm_members/package/{$package->TxnLineID}"); ?>"><?php echo $package->Item_FullName; ?></a> <span class="badge"><?php echo $package->TxnLineID; ?></span></td>
                  <td class="text-center"><a href="<?php echo site_url(uri_string()) . "?team=" . $package->team; ?>"><?php echo $package->team; ?></a></td>
                  <td class="text-center"><a href="<?php echo site_url("mlm_packages/downlines/{$package->TxnLineID}"); ?>"><?php echo $package->downlines; ?></a></td>
                  <td class="text-right">
<?php //if( $package->downlines ) { ?>
                    <a class="pull-right" href="<?php echo site_url("mlm_packages/genealogy/{$package->TxnLineID}"); ?>"><i class="fa fa-sitemap"></i></a>
<?php //} ?>
                  </td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Package Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>