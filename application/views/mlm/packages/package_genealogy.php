<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Genealogy
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("mlm_members"); ?>">MLM Members</a></li>
        <li><a href="<?php echo site_url("mlm_members/summary/{$customer->ListID}"); ?>"><?php echo $customer->Name; ?></a></li>
        <li><a href="<?php echo site_url("mlm_members/packages/{$customer->ListID}"); ?>">Packages</a></li>
        <li class="active"><strong><?php echo $current_package->Item_FullName; ?></strong></li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
<style>
  .current_package {
    margin-top: 30px;
  }
  .center-line {
    margin-top: -15px;
  }
  .line-left {
    border-left: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-right {
    border-right: 1px solid #000;
    height: 30px;
    margin-bottom: 0px;
  }
  .line-top {
    border-top: 1px solid #000;
  }
</style>
<div class="row">
        <div class="col-md-12">
            
<div class="row current_package">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-aqua"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">

              <?php if( ($current_package->parent) && ($current_package->parent!=='ROOT_PACKAGE')) { ?>
                <a  style="margin-left:3px;" class="pull-right" href="<?php echo site_url("mlm_packages/genealogy/{$current_package->parent}"); ?>"><i class="fa fa-arrow-circle-up"></i></a>
              <?php } ?>
              <?php if( $sublevel_packages ) { ?>
                <a style="margin-left:3px;" class="pull-right" href="<?php echo site_url(uri_string()); ?>"><i class="fa fa-sitemap"></i></a>
              <?php } ?>
              <a class="pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo $current_package->Customer_FullName; ?>" href="<?php echo site_url("mlm_members/package/{$current_package->TxnLineID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">MAIN PACKAGE

                </span>
                <span class="info-box-number"><?php echo $current_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $current_package->team; ?></span> <span class="badge">ID: <?php echo $current_package->TxnLineID; ?></span></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>

<?php if( $left_package || $right_package ) { ?>
<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>
<?php } ?>

<?php 
$sublvl_n = 1;
$subs = array();
if( $sublevel_packages ) { 
  foreach($sublevel_packages as $sublvl=>$subpack) {
    $subs[] = $subpack->TxnLineID;
    $sublvl_n++;

?>

<div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="info-box">
              <span class="info-box-icon bg-aqua bg-<?php echo $subpack->team; ?>"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">
                <?php if(count($sublevel_packages) != $sublvl ) { ?>
                <a style="margin-left:3px;" class="pull-right" href="<?php echo site_url(uri_string()) . "?sub=" . implode(",",$subs); ?>"><i class="fa fa-sitemap"></i></a>
              <?php } ?>
                <a class="pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo $subpack->Customer_FullName; ?>" href="<?php echo site_url("mlm_members/package/{$subpack->TxnLineID}"); ?>"><i class="fa fa-user"></i></a>

                <span class="info-box-text">SUB PACKAGE (LEVEL <?php echo $sublvl_n; ?>)
<a href="<?php echo site_url("mlm_packages/genealogy/{$subpack->TxnLineID}"); ?>"><i class="fa fa-play"></i></a>
                </span>
                <span class="info-box-number"><?php echo $subpack->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $subpack->team; ?></span> <a href="#" class="badge">ID: <?php echo $subpack->TxnLineID; ?></a></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
</div>

<div class="row hidden-xs hidden-sm">
        <div class="col-md-6 line-right center-line">

        </div>
</div>


<?php } } ?>

<div class="row hidden-xs hidden-sm">
<?php if( $left_package ) { ?>
  <?php if( $left_package->total_side > 1 ) { ?>
    <div class="col-md-1"></div>
          <div class="col-md-2 line-top"><a href="<?php echo site_url("mlm_packages/downlines/{$left_package->parent}") . "?side=LEFT"; ?>" class="badge pull-left" style="background-color:red;margin-left: -35px;margin-top: -10px;"><?php echo ($left_package->total_side-1); ?></a></div>
        <?php } else { ?>
          <div class="col-md-3"></div>
        <?php } ?>
        <div class="col-md-3 line-left line-top"></div>
<?php } else { ?>
        <div class="col-md-3 col-md-offset-3"></div>
<?php } ?>
<?php if( $right_package ) { ?>
        <div class="col-md-3 line-right line-top"></div>
        <?php if( $right_package->total_side > 1 ) { ?>
          <div class="col-md-2 line-top"><a href="<?php echo site_url("mlm_packages/downlines/{$right_package->parent}") . "?side=RIGHT"; ?>" class="badge pull-right" style="background-color:red;margin-right: -35px;margin-top: -10px;"><?php echo ($right_package->total_side-1); ?></a></div>
        <?php } ?>
<?php } ?>
</div>
<div class="row">
        <div class="col-md-4 col-md-offset-1">
<?php if( $left_package ) { ?>
            <div class="info-box">
              <span class="info-box-icon bg-aqua bg-<?php echo $left_package->team; ?>"><i class="fa fa-cube"></i></span>

              <div class="info-box-content">
                <a class="pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo $left_package->Customer_FullName; ?>" href="<?php echo site_url("mlm_members/package/{$left_package->TxnLineID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl_n + 1; ?> (LEFT)
<a href="<?php echo site_url("mlm_packages/genealogy/{$left_package->TxnLineID}"); ?>"><i class="fa fa-play"></i></a>
                </span>
                <span class="info-box-number"><?php echo $left_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $left_package->team; ?></span> <a href="<?php echo site_url("mlm_packages/genealogy/{$left_package->TxnLineID}"); ?>" class="badge">ID: <?php echo $left_package->TxnLineID; ?></a></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
        <div class="col-md-4 col-md-offset-2">
<?php if( $right_package ) { ?>
            <div class="info-box">
              <span class="info-box-icon bg-aqua bg-<?php echo $right_package->team; ?>"><i class="fa fa-cube"></i></span>
              <div class="info-box-content">
                <a class="pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo $right_package->Customer_FullName; ?>" href="<?php echo site_url("mlm_members/package/{$right_package->TxnLineID}"); ?>"><i class="fa fa-user"></i></a>
                <span class="info-box-text">LEVEL <?php echo $sublvl_n + 1; ?> (RIGHT)
<a href="<?php echo site_url("mlm_packages/genealogy/{$right_package->TxnLineID}"); ?>"><i class="fa fa-play"></i></a>
                </span>

                <span class="info-box-number"><?php echo $right_package->Item_FullName; ?></span>
                <p><span class="badge">TEAM: <?php echo $right_package->team; ?></span> <a href="<?php echo site_url("mlm_packages/genealogy/{$right_package->TxnLineID}"); ?>" class="badge">ID: <?php echo $right_package->TxnLineID; ?></a></p>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
<?php } ?>
        </div>
</div>

<div class="row hidden-xs hidden-sm">
<?php if( $left_package && ($left_package->downlines > 0) ) { ?>
        <div class="col-md-3 col-md-offset-3 line-left center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_packages/genealogy/{$current_package->TxnLineID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $left_package->TxnLineID; ?>" class="badge" style="position: absolute;left: -10px;bottom: -10px;"><?php echo $left_package->downlines; ?></a>
        </div>
<?php } else { ?>
  <div class="col-md-3 col-md-offset-3" style="position: relative;">
  </div>
<?php } ?>
<?php if( $right_package && ($right_package->downlines > 0) ) { ?>
        <div class="col-md-3 line-right center-line" style="position: relative;">
          <a href="<?php echo site_url("mlm_packages/genealogy/{$current_package->TxnLineID}") . "?sub=" . (($this->input->get('sub'))?$this->input->get('sub') . "," : '') . $right_package->TxnLineID; ?>" class="badge" style="position: absolute;right: -10px;bottom: -10px;"><?php echo $right_package->downlines; ?></a>
        </div>
<?php } ?>
</div>


        </div>
</div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>