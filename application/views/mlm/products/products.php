<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Products
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url("mlm_dashboard"); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Products</li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
      
<div class="row">

<?php foreach($items as $item) { ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <a href="<?php echo site_url("mlm_products/view/{$item->ListID}"); ?>">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-apple"></i></span>

            <div class="info-box-content">
              <span class="info-box-number"><?php echo $item->Name; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          </a>
          <!-- /.info-box -->
        </div>
<?php } ?>

      </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>