<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <?php if($previous_check) { ?>
  <a href="<?php echo site_url( "checks/voucher/{$previous_check->TxnNumber}" ) . "?back=" . $this->input->get('back'); ?>">&laquo; prev</a>  &middot;
<?php } ?>
  <a href="<?php echo site_url( $this->input->get('back') ); ?>">Back</a>  &middot;
   <a class="active" href="#">Check Voucher</a>  &middot; <a href="<?php echo site_url("checks/print_check/{$check->TxnNumber}"); ?>?back=<?php echo $this->input->get('back'); ?>">Print Check</a>
   <?php if($next_check) { ?>
  &middot; <a href="<?php echo site_url( "checks/voucher/{$next_check->TxnNumber}" ) . "?back=" . $this->input->get('back'); ?>">next &raquo;</a>  
<?php } ?>
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th><?php echo qbapi_get_settings("print_header","mother_company"); ?></th>
    </tr>

    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;"><?php echo qbapi_get_settings("print_header","company_name"); ?></strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small><?php echo qbapi_get_settings("print_header","company_address"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_phone"); ?><br>
<?php echo qbapi_get_settings("print_header","company_website"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_email"); ?></small>
      </th>
    </tr>
    <tr>
      <th>
<span style="top: 105px; right: 15px; position: absolute; font-size: 16px; color: red;">CV #: <strong><?php echo date("Y", strtotime($check->TxnDate)); ?>-<?php echo $check->TxnNumber; ?></strong></span>
        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px;">CHECK VOUCHER</strong><br><br>

      </th>
    </tr>

</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        
<table width="100%" cellpadding="3" cellspacing="0" class="bordered">
  <tr>
    <td width="70%"><small>Paid To:</small> <strong class="bigger allcaps"><?php echo $check->PayeeEntityRef_FullName; ?></strong></td>
    <td width="30%">Date: <strong><?php echo date("F d, Y", strtotime($check->TxnDate)); ?></strong></td>
  </tr>
<tr>
    <td rowspan="3" valign="top"><small>Address:</small> <br>
      <strong><?php echo $check->Address_Addr1; ?></strong>
      <strong><?php echo $check->Address_Addr2; ?></strong>
      <strong><?php echo $check->Address_Addr3; ?></strong>
    </td>
  </tr>
<tr>
    <td class="bigger">Check #: <strong><?php echo $check->RefNumber; ?></strong></td>
  </tr>
<tr>
    <td class="bigger">Amount: <strong>&#8369; <?php echo number_format($check->Amount,2); ?></strong></td>
  </tr>
</table>
<?php 
$total = 0;
if($expenses) { ?>
<div style="min-height: 100px;">
<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
 <tr class="green_bg">
    <td width="10%"><strong>#</strong></td>
    <td width="30%"><strong>ACCOUNT TITLE</strong></td>
    <td width="20%"><strong>ASSIGN TO</strong></td>
    <td width="20%"><strong>MEMO</strong></td>
    <td width="20%" align="right"><strong>AMOUNT</strong></td>
  </tr>
<?php 
foreach($expenses as $expense) { //print_r($expense); ?>
 <tr>
    <td class="footer"><?php echo $expense->aNumber; ?></td>
    <td class="footer"><?php echo $expense->aName; ?></td>
    <td class="footer"><?php echo $expense->Customer_FullName; ?></td>
    <td class="footer"><?php echo $expense->Memo; ?></td>
    <td class="footer" align="right"><?php echo number_format($expense->Amount,2); $total += $expense->Amount; ?></td>
  </tr>
<?php } ?>
</table>
</div>
<?php } ?>

      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">
    <tr><td valign="bottom" height="100%" colspan="4">

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
<tr  class="green_bg">
    <td width="22%">Check #: <strong><?php echo $check->RefNumber; ?></strong></td>
    <td width="22%">Date: <strong><?php echo date("F d, Y", strtotime($check->TxnDate)); ?></strong></td>
    <td width="34%" align="right"><strong>TOTAL</strong></td>
    <td width="22%" align="right"><strong style="font-size: 14px;">&#8369; <?php echo number_format($total,2); ?></strong></td>
  </tr>
</table>
Total Amount in Words: <strong style="text-transform: capitalize;"><?php echo number2word( number_format($total, 2) ); ?></strong>


<br><br>

    </td></tr>

        <tr>
      <td width="25%" class="footer">Prepared by:</td>
      <td width="25%" class="footer">Checked by:</td>
      <td width="25%" class="footer">Approved By:</td>
      <td width="25%" class="footer">Received in full amount by:</td>
    </tr>
    <tr>
      <td width="25%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","preparedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","preparedby_position"); ?></td>
      <td width="25%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","checkedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","checkedby_position"); ?></td>
      <td width="25%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","approvedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","approvedby_position"); ?></td>
      <td width="25%" class="footer"><br><br>________________________________<br><small>signature over printed name / date</small></td>
    </tr>

  </tfoot>
</table>

<?php $this->load->view('print_footer'); ?>