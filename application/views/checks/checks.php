<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Checks
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Checks</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $checks ) { ?>
              <h3 class="box-title">Checks</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">

<?php if( $checks ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>CV#</th>
                  <th>Date</th>
                  <th>Number</th>
                  <th>Payee</th>
                  <th>Amount</th>
                  <th>Memo</th>
                  <th width="115px" class="text-right">Action</th>
                </tr>
<?php foreach($checks as $check) { ?>
                <tr>
                  <td><?php echo $check->TxnNumber; ?></td>
                  <td><?php echo date("F d, Y", strtotime($check->TxnDate)); ?></td>
                  <td><?php echo $check->RefNumber; ?></td>
                  <td>
                    <a href="<?php echo site_url("checks/index/{$check->PayeeEntityRef_ListID}"); ?>">
                      <?php echo $check->PayeeEntityRef_FullName; ?>
                    </a>
                  </td>
                  <td><?php echo number_format($check->Amount,2); ?></td>
                  <td><?php echo $check->Memo; ?></td>
                  <td class="text-right"><a href="<?php echo site_url("checks/voucher/{$check->TxnNumber}"); ?>?back=<?php echo uri_string(); ?>" class="btn btn-success btn-xs">Voucher</a> <a href="<?php echo site_url("checks/print_check/{$check->TxnNumber}"); ?>?back=<?php echo uri_string(); ?>" class="btn btn-warning btn-xs">Print</a></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Check Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $checks ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>