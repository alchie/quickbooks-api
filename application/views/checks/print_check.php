<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <?php if($previous_check) { ?>
  <a href="<?php echo site_url( "checks/print_check/{$previous_check->TxnNumber}" ) . "?back=" . $this->input->get('back'); ?>">&laquo; prev</a>  &middot;
<?php } ?>
  <a href="<?php echo site_url( $this->input->get('back') ); ?>">Back</a>  &middot; <a href="<?php echo site_url("checks/voucher/{$check->TxnNumber}"); ?>?back=<?php echo $this->input->get('back'); ?>">Check Voucher</a>  &middot; <a class="active" href="#">Print Check</a>
   <?php if($next_check) { ?>
  &middot; <a href="<?php echo site_url( "checks/print_check/{$next_check->TxnNumber}" ) . "?back=" . $this->input->get('back'); ?>">next &raquo;</a>  
<?php } ?>
</div>



<div class="print">
            <div id="check_print" class="">
             <div class="elm payee allcaps"><?php echo strtoupper( $check->PayeeEntityRef_FullName ); ?></div>
             <div class="elm date allcaps"><?php echo date("F d, Y", strtotime($check->TxnDate)); ?></div>
             <div class="elm amount allcaps"><?php echo number_format($check->Amount,2); ?></div>
             <div class="elm amount-words allcaps"><?php echo number2word( number_format($check->Amount, 2) ); ?></div>
                      </div>
<script>
<!--
  //window.print();
-->
</script>         

</div>

<?php $this->load->view('print_footer'); ?>