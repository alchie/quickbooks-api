<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <a href="<?php echo site_url( $this->input->get('back') ); ?>">Back</a> 
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th><?php echo qbapi_get_settings("print_header","mother_company"); ?></th>
    </tr>

    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;"><?php echo qbapi_get_settings("print_header","company_name"); ?></strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small><?php echo qbapi_get_settings("print_header","company_address"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_phone"); ?><br>
<?php echo qbapi_get_settings("print_header","company_website"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_email"); ?></small>
      </th>
    </tr>
    <tr>
      <th>
<span style="top: 105px; right: 15px; position: absolute; font-size: 16px; color: red;">JV #: <strong><?php echo date("Y", strtotime($journal->TxnDate)); ?>-<?php echo $journal->TxnNumber; ?></strong></span>
        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px;">JOURNAL VOUCHER</strong><br><br>

      </th>
    </tr>

</table>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered">
  <tr>
    <td width="70%"><small>REFERRENCE NUMBER:</small> <strong class="bigger"><?php echo $journal->RefNumber; ?></strong></td>
    <td width="30%" align="right">Voucher Date: <strong><?php echo date("F d, Y", strtotime($journal->TxnDate)); ?></strong></td>
  </tr>
</table>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
 <tr class="green_bg">
    <td width="20%"><strong>ACCOUNT NAME</strong></td>
    <td width="20%"><strong>CUSTOMER NAME</strong></td>
    <td width="20%"><strong>MEMO</strong></td>
    <td width="20%" align="right"><strong>DEBIT</strong></td>
    <td width="20%" align="right"><strong>CREDIT</strong></td>
  </tr>
</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        
<?php 
$total_debit = 0;
$total_credit = 0;
if($credit_entries||$debit_entries) { ?>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
<?php foreach($credit_entries as $credit_entry) { //print_r($credit_entry); ?>
 <tr class="footer">
    <td width="20%"><?php echo $credit_entry->Account_FullName; ?></td>
    <td width="20%"><?php echo $credit_entry->Entity_FullName; ?></td>
    <td width="20%"><?php echo $credit_entry->Memo; ?></td>
    <td width="20%" align="right"></td>
    <td width="20%" align="right"><?php echo number_format($credit_entry->Amount,2);  ?></td>
  </tr>
<?php 
$total_credit += $credit_entry->Amount;
} ?>

<?php foreach($debit_entries as $debit_entry) { //print_r($debit_entry); ?>
 <tr class="footer">
    <td width="20%"><?php echo $debit_entry->Account_FullName; ?></td>
    <td width="20%"><?php echo $debit_entry->Entity_FullName; ?></td>
    <td width="20%"><?php echo $debit_entry->Memo; ?></td>
    <td width="20%" align="right"><?php echo number_format($debit_entry->Amount,2); ?></td>
    <td width="20%" align="right"></td>
  </tr>
<?php 
$total_debit += $debit_entry->Amount;
} ?>

</table>

<?php } ?>

      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">
    <tr>
      <td valign="bottom" height="100%" colspan="4">

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
<tr  class="green_bg">
    <td width="60%"><strong>TOTAL</strong></td>
    <td width="20%" align="right"><strong><?php echo number_format($total_debit,2); ?></strong></td>
    <td width="20%" align="right"><strong><?php echo number_format($total_credit,2); ?></strong></td>
  </tr>
</table>

<br><br>

    </td></tr>

       <tr>
      <td width="33.33%" class="footer">Prepared by:</td>
      <td width="33.33%" class="footer">Checked by:</td>
      <td width="33.33%" class="footer">Approved By:</td>
     
    </tr>
    <tr>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","preparedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","preparedby_position"); ?></td>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","checkedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","checkedby_position"); ?></td>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","approvedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","approvedby_position"); ?></td>
      
    </tr>

  </tfoot>
</table>

<?php $this->load->view('print_footer'); ?>