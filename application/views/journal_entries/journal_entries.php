<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Journal Entries
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Journal Entries</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Journal Entries</h3>

              <div class="box-tools">
                <form method="get">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>CV#</th>
                  <th>Date</th>
                  <th>Deposited to</th>

                  <th class="text-right">Action</th>
                </tr>
<?php foreach($journals as $journal) { ?>
                <tr>
                  <td><?php echo $journal->TxnNumber; ?></td>
                  <td><?php echo date("F d, Y", strtotime($journal->TxnDate)); ?></td>
                  <td><?php echo $journal->RefNumber; ?></td>
                  <td class="text-right"><a href="<?php echo site_url("journal_entries/details/{$journal->TxnNumber}") . "?back=" . uri_string(); ?>" class="btn btn-success btn-xs">Details</a></td>
                </tr>
<?php } ?>
              </table>


            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>