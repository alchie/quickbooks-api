<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Deposits
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Deposits</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Deposits</h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("deposits/index/{$payee_id}"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>CV#</th>
                  <th>Date</th>
                  <th>Deposited to</th>
                  <th>Amount</th>
                  <th>Memo</th>
                  <th class="text-right">Action</th>
                </tr>
<?php foreach($deposits as $deposit) { ?>
                <tr>
                  <td><?php echo $deposit->TxnNumber; ?></td>
                  <td><?php echo date("F d, Y", strtotime($deposit->TxnDate)); ?></td>
                  <td><?php echo $deposit->DepositToAccount_FullName; ?></td>
                  <td><?php echo number_format($deposit->DepositTotal,2); ?></td>
                  <td><?php echo $deposit->Memo; ?></td>
                  <td class="text-right"><a href="<?php echo site_url("deposits/details/{$deposit->TxnID}"); ?>?back=<?php echo uri_string(); ?>" class="btn btn-success btn-xs">Details</a></td>
                </tr>
<?php } ?>
              </table>


            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>