<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <?php if($previous_deposit) { ?>
  <a href="<?php echo site_url( "deposits/details/{$previous_deposit->TxnID}" ) . "?back=" . $this->input->get('back'); ?>">&laquo; prev</a>  &middot;
<?php } ?>
  <a href="<?php echo site_url( $this->input->get('back') ); ?>">Back</a> 
   <?php if($next_deposit) { ?>
  &middot; <a href="<?php echo site_url( "deposits/details/{$next_deposit->TxnID}" ) . "?back=" . $this->input->get('back'); ?>">next &raquo;</a>  
<?php } ?>
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th><?php echo qbapi_get_settings("print_header","mother_company"); ?></th>
    </tr>

    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;"><?php echo qbapi_get_settings("print_header","company_name"); ?></strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small><?php echo qbapi_get_settings("print_header","company_address"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_phone"); ?><br>
<?php echo qbapi_get_settings("print_header","company_website"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_email"); ?></small>
      </th>
    </tr>
    <tr>
      <th>
<span style="top: 105px; right: 15px; position: absolute; font-size: 16px; color: red;">DD #: <strong><?php echo date("Y", strtotime($deposit->TxnDate)); ?>-<?php echo $deposit->TxnNumber; ?></strong></span>
        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px;">DEPOSIT DETAILS</strong><br><br>

      </th>
    </tr>

</table>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered">
  <tr>
    <td width="70%"><small>Deposited To Account:</small> <strong class="bigger"><?php echo $deposit->DepositToAccount_FullName; ?></strong></td>
    <td width="30%" align="right">Date Deposited: <strong><?php echo date("F d, Y", strtotime($deposit->TxnDate)); ?></strong></td>
  </tr>
</table>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
 <tr class="green_bg">
    <td width="5%"><strong>DATE</strong></td>
    <td width="5%"><strong>OR #</strong></td>
    <td width="10%"><strong>METHOD</strong></td>
    <td width="25%"><strong>CUSTOMER NAME</strong></td>
    <td width="10%"><strong>CHECK NUMBER</strong></td>
    <td width="25%"><strong>MEMO</strong></td>
    <td width="20%" align="right"><strong>AMOUNT</strong></td>
  </tr>
</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        
<?php if($receipts) { ?>

<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
<?php 
$total = 0;
foreach($receipts as $receipt) { //print_r($receipt); ?>
 <tr class="footer">
    <td width="5%"><?php echo (isset($receipt->TxnDate)) ? date("m/d/Y", strtotime($receipt->TxnDate)) : ""; ?></td>
    <td width="5%"><?php echo (isset($receipt->sRefNumber)) ? $receipt->sRefNumber : ''; ?></td>
    <td width="10%"><?php echo $receipt->PaymentMethod_FullName; ?></td>
    <td width="25%"><?php echo $receipt->Entity_FullName; ?></td>
    <td width="10%"><?php echo $receipt->CheckNumber; ?></td>
    <td width="25%"><?php echo $receipt->Memo; ?><?php echo ($receipt->ddMemo) ? " | " . $receipt->ddMemo : ""; ?></td>
    <td width="20%" align="right"><?php echo number_format($receipt->Amount,2);  $total += $receipt->Amount; ?></td>
  </tr>
<?php } ?>
</table>

<?php } ?>

      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">
    <tr>
      <td valign="bottom" height="100%" colspan="4">
<?php if( $deposit->CashBackInfo_Amount > 0 ) { 
$total -= $deposit->CashBackInfo_Amount;
  ?>
<table width="100%" cellpadding="3" cellspacing="0" class="bordered">
  <tr>
    <td width="40%"><small>Cashback To Account:</small> <strong class="bigger"><?php echo $deposit->CashBackInfo_Account_FullName; ?></strong></td>
    <td width="30%" align="right">Amount: <strong>-<?php echo number_format($deposit->CashBackInfo_Amount,2); ?></strong></td>
    <td width="30%" align="right"></td>
  </tr>
</table>
<?php } ?>
<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
<tr  class="green_bg">
    <td width="44%">Date Deposited: <strong><?php echo date("F d, Y", strtotime($deposit->TxnDate)); ?></strong></td>
    <td width="34%" align="right"><strong>TOTAL</strong></td>
    <td width="22%" align="right"><strong style="font-size: 14px;">&#8369; <?php echo number_format($total,2); ?></strong></td>
  </tr>
</table>
Total Amount in Words: <strong style="text-transform: capitalize;"><?php echo number2word( number_format($total, 2) ); ?></strong>

<br><br>

    </td></tr>

       <tr>
      <td width="33.33%" class="footer">Prepared by:</td>
      <td width="33.33%" class="footer">Checked by:</td>
      <td width="33.33%" class="footer">Approved By:</td>
    </tr>
    <tr>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","preparedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","preparedby_position"); ?></td>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","checkedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","checkedby_position"); ?></td>
      <td width="33.33%" class="footer"><br><br><strong class="allcaps"><?php echo qbapi_get_settings("print_footer","approvedby_name"); ?></strong><br><?php echo qbapi_get_settings("print_footer","approvedby_position"); ?></td>
      
    </tr>

  </tfoot>
</table>

<?php $this->load->view('print_footer'); ?>