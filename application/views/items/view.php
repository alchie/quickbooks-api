<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); //print_r($current_item); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $current_item->Name; ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("items/{$breadcrumbs_uri}"); ?>"><?php echo $breadcrumbs_title; ?></a></li>
        <li class="active"><?php echo $current_item->Name; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
          <div class="box">
            <div class="box-header">
<?php if( $current_item ) { ?>
              <h3 class="box-title"><?php echo $current_item->FullName; ?></h3>
              <div class="box-tools">
<?php echo number_format($current_item->SalesOrPurchase_Price,2); ?>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

<?php echo $current_item->SalesOrPurchase_Desc; ?>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>