  <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="messages-menu">
            <a href="<?php echo site_url("quickbooks_api/settings"); ?>">
              <i class="glyphicon glyphicon-cog"></i>
            </a>
          </li>

          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-th"></i> </a>
          <ul class="dropdown-menu">

<?php 
$top_links = false;
?>
<?php if( $this->config->item('mlm_enable') ) { ?>
<?php if( $top_links ) { ?>
            <li role="separator" class="divider"></li>
<?php } ?>
            <li class="header">Networking</li>
            <li><a href="<?php echo site_url("mlm_dashboard"); ?>">Dashboard</a></li>
            <li><a href="<?php echo site_url("mlm_members"); ?>">Members</a></li>
            <li><a href="<?php echo site_url("mlm_packages"); ?>">Packages</a></li>
            <li><a href="<?php echo site_url("mlm_products"); ?>">Products</a></li>
            <li><a href="<?php echo site_url("mlm_services"); ?>">Services</a></li>
<?php $top_links = true;
} ?>
<?php if( $this->config->item('cooperative_enable') ) { ?>
<?php if( $top_links ) { ?>
            <li role="separator" class="divider"></li>
<?php } ?>
            <li class="header">Cooperative</li>
            <li><a href="<?php echo site_url("coop_dashboard"); ?>">Dashboard</a></li>
            <li><a href="<?php echo site_url("coop_members"); ?>">Members</a></li>
            <li><a href="<?php echo site_url("coop_loans"); ?>">Loans</a></li>
<?php $top_links = true;
} ?>
<?php if( $this->config->item('pledgers_enable') ) { ?>
<?php if( $top_links ) { ?>
            <li role="separator" class="divider"></li>
<?php } ?>
<li class="header">ANC</li>
            <li><a href="<?php echo site_url("pledgers"); ?>">Pledgers</a></li>
<?php } ?>
          </ul>
        </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-user"></i> <?php echo ($current_account->FirstName) ? $current_account->FirstName : $current_account->Name; ?></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url("account/logout"); ?>">Logout</a></li>
              </ul>
          </li>

        </ul>
      </div>

    </nav>