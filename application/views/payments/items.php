<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Receipts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("sales_receipts"); ?>">Sales Receipts</a></li>
        <li class="active">Item</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
          <div class="box">
            <div class="box-header">
<?php if( $sales_receipt ) { ?>
              <h3 class="box-title"><?php echo $sales_receipt->Customer_FullName; ?></h3>
              <div class="box-tools">
<?php echo number_format($sales_receipt->TotalAmount,2); ?>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">

<?php if( $items ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>Item</th>
                  <th class="text-right">Amount</th>
                </tr>
<?php foreach($items as $item) { ?>
                <tr>
                  <td><?php echo $item->Item_FullName; ?> <br><small><?php echo $item->Descrip; ?></small></td>
                  <td class="text-right"><?php echo number_format($item->Amount,2); ?></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Sales Receipt Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->

          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>