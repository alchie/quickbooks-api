<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pledgers
      </h1>
      <ol class="breadcrumb">
        <li><a href="#<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pledgers</li>
      </ol>
    </section>

    <!-- Main content -->
   <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $pledgers ) { ?>
              <h3 class="box-title">Pledgers (<?php echo number_format($count,0); ?>)</h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("pledgers/index/{$year}"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get("q"); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
                </form>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
<?php if( $pledgers ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>Name</th>

                  <?php foreach(array(1,2,3,4,5,6,7,8,9,10,11,12) as $month) { ?>
                    <th width="6%" class="text-right"><?php echo date("M Y", strtotime($month."/1/".$year)); ?></th>
                  <?php } ?>
                </tr>
<?php foreach($pledgers as $customer) { ?>
                <tr>
                  <td><?php echo $customer->Name; ?><br/>
<small><?php echo $customer->Phone; ?> &middot; <?php echo $customer->AltPhone; ?></small>
                  </td>
                  <?php foreach(array(1,2,3,4,5,6,7,8,9,10,11,12) as $month) { ?>
                    <td class="text-right"><span class="fa fa-check"></span></td>
                  <?php } ?>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Customer Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $pagination ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>