 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li class="<?php echo ($current_uri=='dashboard') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("dashboard"); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
<?php if( $this->config->item('QB_API_ACCOUNTQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='accounts') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("accounts"); ?>">
            <i class="glyphicon glyphicon-book"></i> <span>Chart of Accounts</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_SALESRECEIPTQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='sales_receipts') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("sales_receipts"); ?>">
            <i class="glyphicon glyphicon-shopping-cart"></i> <span>Sales Receipts</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_PAYMENTMETHODQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='payments') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("payments"); ?>">
            <i class="glyphicon glyphicon-shopping-cart"></i> <span>Payments</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_DEPOSITQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='deposits') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("deposits"); ?>">
            <i class="glyphicon glyphicon-usd"></i> <span>Deposits</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_CHECKQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='checks') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("checks"); ?>">
            <i class="glyphicon glyphicon-ok-circle"></i> <span>Checks</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_JOURNALENTRYQUERY') ) { ?>
        <li class="<?php echo ($current_uri=='journal_entries') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("journal_entries"); ?>">
            <i class="glyphicon glyphicon-usd"></i> <span>Journal Entries</span>
          </a>
        </li>
<?php } ?>
<?php if( $this->config->item('QB_API_CUSTOMERQUERY') 
  || $this->config->item('QB_API_VENDORQUERY')
  || $this->config->item('QB_API_EMPLOYEESQUERY')
  || $this->config->item('QB_API_OTHERNAMEQUERY')
 ) { ?>
<?php $name_active = (in_array($current_uri, array('names', 'customers', 'vendors', 'employees', 'other_names'))); ?>
        <li class="<?php echo ($name_active) ? 'active' : ''; ?>">
          <a href="<?php echo site_url("names"); ?>">
            <i class="fa fa-users"></i> <span>Names</span>
            <i class="fa fa-angle-<?php echo ($name_active) ? 'down' : 'left'; ?> pull-right"></i>
          </a>
          <?php if($name_active) { ?>
            <ul class="treeview-menu">
<?php if( $this->config->item('QB_API_CUSTOMERQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='customers') ? 'active' : ''; ?>"><a href="<?php echo site_url("customers"); ?>"><i class="fa fa-circle-o"></i> Customers</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_VENDORQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='vendors') ? 'active' : ''; ?>"><a href="<?php echo site_url("vendors"); ?>"><i class="fa fa-circle-o"></i> Vendors</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_EMPLOYEEQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='employees') ? 'active' : ''; ?>"><a href="<?php echo site_url("employees"); ?>"><i class="fa fa-circle-o"></i> Employees</a></li>
<?php } ?>
          </ul>
        <?php } ?>
        </li>
<?php } ?>

<?php if( $this->config->item('QB_API_ITEMDISCOUNTQUERY') 
  || $this->config->item('QB_API_ITEMFIXEDASSETQUERY')
  || $this->config->item('QB_API_ITEMGROUPQUERY')
  || $this->config->item('QB_API_ITEMINVENTORYASSEMBLYQUERY')
  || $this->config->item('QB_API_ITEMINVENTORYQUERY')
  || $this->config->item('QB_API_ITEMNONINVENTORYQUERY')
  || $this->config->item('QB_API_ITEMOTHERCHARGEQUERY')
  || $this->config->item('QB_API_ITEMPAYMENTQUERY')
 ) { ?>
<?php $item_active = (in_array($current_uri, array('items', 'discount', 'fixedasset', 'group', 'inventory', 'inventoryassembly', 'noninventory', 'othercharge', 'payment'))); ?>
        <li class="<?php echo ($item_active) ? 'active' : ''; ?>">
          <a href="<?php echo site_url("items"); ?>">
            <i class="glyphicon glyphicon-cutlery"></i> <span>Items</span>
            <i class="fa fa-angle-<?php echo ($item_active) ? 'down' : 'left'; ?> pull-right"></i>
          </a>
          <?php if($item_active) { ?>
            <ul class="treeview-menu">
<?php if( $this->config->item('QB_API_ITEMDISCOUNTQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='discount') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/discounts"); ?>"><i class="fa fa-circle-o"></i> Discounts</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMFIXEDASSETQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='fixedasset') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/fixedassets"); ?>"><i class="fa fa-circle-o"></i> Fixed Assets</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMGROUPQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='group') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/groups"); ?>"><i class="fa fa-circle-o"></i> Groups</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMINVENTORYASSEMBLYQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='inventory') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/inventories"); ?>"><i class="fa fa-circle-o"></i> Inventories</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMINVENTORYQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='inventoryassembly') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/inventoryassemblies"); ?>"><i class="fa fa-circle-o"></i> Inventory Assemblies</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMNONINVENTORYQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='noninventory') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/noninventories"); ?>"><i class="fa fa-circle-o"></i> Non-inventories</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMOTHERCHARGEQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='othercharge') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/othercharges"); ?>"><i class="fa fa-circle-o"></i> Other Charges</a></li>
<?php } ?>
<?php if( $this->config->item('QB_API_ITEMPAYMENTQUERY') ) { ?>
            <li class="<?php echo ($current_uri=='payment') ? 'active' : ''; ?>"><a href="<?php echo site_url("items/payments"); ?>"><i class="fa fa-circle-o"></i> Payments</a></li>
<?php } ?>
          </ul>
        <?php } ?>
        </li>
<?php } ?>
        <li class="<?php echo ($current_uri=='reports') ? 'active' : ''; ?>">
          <a href="<?php echo site_url("reports"); ?>">
            <i class="glyphicon glyphicon-blackboard"></i> <span>Reports</span>
          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
