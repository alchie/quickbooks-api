<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Sales Receipts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Sales Receipts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $sales_receipts ) { ?>
              <h3 class="box-title">Sales Receipts</h3>
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">

<?php if( $sales_receipts ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>OR#</th>
                  <th>Payee</th>
                  <th>Amount</th>
                  <th>Memo</th>
                  <th width="115px" class="text-right">Action</th>
                </tr>
<?php foreach($sales_receipts as $sales_receipt) { ?>
                <tr>
                  <td><?php echo $sales_receipt->TxnNumber; ?></td>
                  <td><?php echo date("F d, Y", strtotime($sales_receipt->TxnDate)); ?></td>
                  <td><?php echo $sales_receipt->RefNumber; ?></td>
                  <td>
                    <a href="<?php echo site_url("sales_receipts/index/{$sales_receipt->Customer_ListID}"); ?>">
                      <?php echo $sales_receipt->Customer_FullName; ?>
                    </a>
                  </td>
                  <td><?php echo number_format($sales_receipt->TotalAmount,2); ?></td>
                  <td><?php echo $sales_receipt->Memo; ?></td>
                  <td class="text-right"><a href="<?php echo site_url("sales_receipts/items/{$sales_receipt->TxnID}"); ?>" class="btn btn-success btn-xs">Items</a></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Sales Receipt Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $sales_receipts ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>