<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Fundraising : Concert 2019
      </h1>
      <ol class="breadcrumb">
        <li>Fr. Manoling Francisco, SJ and the Bukas Palad Music Ministry<br>
          6PM, Saturday, November 23, 2019, RSM Events Center, PWC
        </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Solicitation Letters <small>(<?php echo $solis_count; ?>)</small></h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url(uri_string()); ?>">
                <div class="input-group input-group-sm" style="width: 250px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search" value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover table-striped">
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th class="text-right">Action</th>
                </tr>
<?php foreach($solis as $soli) { ?>
                <tr>
                  <td><?php echo $soli->RefNumber; ?></td>                  
                  <td><?php echo $soli->Customer_FullName; ?></td>                  
                  <td class="text-right">

<div class="btn-group">
  <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Print <span class="caret"></span>
  </button>
  <ul class="dropdown-menu dropdown-menu-right">
       <li><a href="<?php echo site_url("reports_concert_2019/print_soli/{$soli->TxnID}"); ?>?back=<?php echo uri_string(); ?>">Letter</a></li>
       <li><a href="<?php echo site_url("reports_concert_2019/print_envelope/{$soli->TxnID}"); ?>?back=<?php echo uri_string(); ?>">Envelope</a></li>
       <li><a href="<?php echo site_url("reports_concert_2019/print_received/{$soli->TxnID}"); ?>?back=<?php echo uri_string(); ?>">Received Sheet</a></li>
  </ul>
</div>

                  </td>
                </tr>
<?php } ?>
              </table>


            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>