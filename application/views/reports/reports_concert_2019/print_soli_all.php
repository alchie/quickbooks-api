<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <a href="<?php echo site_url( 'reports_concert_2019/print_soli_all' ) . "?back=" . $this->input->get('back'); ?>">Print All</a>  &middot;
  <a href="<?php echo site_url( ($this->input->get('back'))?$this->input->get('back'):'reports_concert_2019' ); ?>">Back</a> 
</div>

<?php //print_r($recepient); ?>
<style>
h1 { font-family: "Gill Sans MT"; font-size: 11px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 12.1px; } h3 { font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 15.4px; } p { font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; } blockquote { font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif; font-size: 21px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 30px; } pre { font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px; }
  h3, p {
    padding:0;
    margin: 0;
  }
  .date {
    margin-bottom: 40px;
  }
  .soli_number {
    font-size:11px;
    font-family: "Gill Sans MT";
    color: red;
    font-weight: bold;
    float: right;
    text-transform: uppercase;
  }
  .salutation {
    margin-top: 40px;
  }
    .recepient_name {
    text-transform: uppercase;
    font-weight: bold;
  }
</style>

<?php foreach($recepients as $recepient) { //print_r($recepient); ?>
<div class="page">

<a class="hide-print" href="<?php echo site_url("reports_concert_2019/print_soli/{$recepient->TxnID}"); ?>"><?php echo $recepient->Name; ?></a>
<hr class="hide-print">
<span class="soli_number">control #: <?php echo $recepient->RefNumber; ?></span>
<p class="date"><?php echo date("F d, Y"); ?></p>
<?php if( $recepient->Salutation ) { ?>
<p class="recepient_name"><?php echo $recepient->Salutation; ?> <?php echo $recepient->FirstName; ?> <?php echo $recepient->LastName; ?></p>
<?php } ?>

<?php 
if($recepient->CompanyName) { ?>
<p class="<?php echo (!$recepient->Salutation) ? 'recepient_name' : ''; ?>"><?php echo $recepient->CompanyName; ?></p>
<?php }  ?>

<?php 
$show_line = false;
$is_empty = false;
if($recepient->ShipAddress_Addr1) { 
    $is_empty = false; 
    $show_line = false;
    ?>
    <p><?php echo $recepient->ShipAddress_Addr1; ?></p>
<?php } else { ?>
    <p>___________________________________</p>
<?php 
$show_line = true;
$is_empty = true;
}  ?>

<?php if( !$is_empty && !$show_line) { ?>
  <?php if($recepient->ShipAddress_Addr2) {
    $is_empty = false; 
    $show_line = false;
   ?>
    <p><?php echo $recepient->ShipAddress_Addr2; ?></p>
  <?php }  ?>
<?php } else { ?>
  <?php if( isset($show_line) && ($show_line)) { ?>
    <p>___________________________________</p>
  <?php }  ?>
<?php 
$show_line = true;
$is_empty = true;
}  ?>

<?php if( !$is_empty && !$show_line) { ?>
  <?php if($recepient->ShipAddress_Addr3) { 
    $is_empty = false; 
    $show_line = false;
    ?>
    <p><?php echo $recepient->ShipAddress_Addr3; ?></p>
  <?php }  ?>
<?php } else { ?>
  <?php if( isset($show_line) && ($show_line)) { ?>
    <p>___________________________________</p>
  <?php }  ?>
<?php 
$show_line = true;
$is_empty = true;

}  ?>
<?php if( !$is_empty && !$show_line) { ?>
  <?php if($recepient->ShipAddress_City) { 
    $is_empty = false; 
    $show_line = false;
    ?>
    <p><?php echo $recepient->ShipAddress_City; ?></p>
  <?php }  ?>
<?php 
$show_line = true;
$is_empty = true;

}  ?>

<p class="salutation">Dear <?php echo ($recepient->Salutation && $recepient->LastName) ? $recepient->Salutation . " " . $recepient->LastName : "_____________________"; ?>:</p>
</div>
<?php } ?>

<?php $this->load->view('print_footer'); ?>