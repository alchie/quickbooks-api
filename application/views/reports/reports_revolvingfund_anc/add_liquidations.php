<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Revolving Fund Replenishment
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("reports"); ?>">Reports</a></li>
        <li class="active">Revolving Fund</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<form method="post">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <a href="<?php echo site_url($this->input->get('back')); ?>" class="btn btn-danger btn-xs pull-right">Back</a>
              <h3 class="box-title">Add Liquidations</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

<table class="table table-condensed table-hover table-striped">
  <?php foreach($journals as $journal) { ?>
  <tr class="<?php echo ($journal->sCount > 0) ? 'danger' : ''; ?>">
    <td><input type="checkbox" name="journal[]" value="<?php echo $journal->TxnID; ?>" id="checkbox_<?php echo $journal->TxnID; ?>"></td>
    <td> <label for="checkbox_<?php echo $journal->TxnID; ?>" <?php echo ($journal->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                    <?php echo $journal->RefNumber; ?>
                  </label></td>

<td> <label for="checkbox_<?php echo $journal->TxnID; ?>" <?php echo ($journal->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                   <?php echo date("F d, Y", strtotime($journal->TxnDate)); ?>
                  </label></td>
<td align="right"> <label for="checkbox_<?php echo $journal->TxnID; ?>" <?php echo ($journal->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                   <?php echo number_format($journal->TotalAmount,2); ?>
                  </label></td>
  </tr>
  <?php } ?>
</table>

               

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="row">
                <div class="col-md-6"><button type="submit" class="btn btn-primary">Submit</button></div>
                <div class="col-md-6 text-right"><?php echo $pagination; ?></div>
            </div>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>