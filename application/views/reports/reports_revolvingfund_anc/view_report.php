<?php $this->load->view('print_header'); ?>

<div class="print-topnav hide-print text-center allcaps">
  <a href="<?php echo site_url( ($this->input->get('back'))?$this->input->get('back'):'reports_revolvingfund_anc' ); ?>">Back</a> 
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th><?php echo qbapi_get_settings("print_header","mother_company"); ?></th>
    </tr>

    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;"><?php echo qbapi_get_settings("print_header","company_name"); ?></strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small><?php echo qbapi_get_settings("print_header","company_address"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_phone"); ?><br>
<?php echo qbapi_get_settings("print_header","company_website"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_email"); ?></small>
      </th>
    </tr>
    <tr>
      <th>

        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px; text-transform: uppercase;">Replenishment Request Report </strong><br><br>

      </th>
    </tr>

</table>
<table width="100%" cellpadding="3" cellspacing="0" style="margin: 5px;">
<tr>
  <td> <strong>RRR # <?php echo date("Y", strtotime($revolving_fund->request_date)); ?>-<a class="edit_field" href="<?php echo site_url("reports_revolvingfund_anc/edit_field/{$revolving_fund->id}/id"); ?>"><?php echo sprintf('%04d', $revolving_fund->id); ?></a></strong>
</td>
  <td align="right"><strong><a class="edit_field" href="<?php echo site_url("reports_revolvingfund_anc/edit_field/{$revolving_fund->id}/request_date"); ?>"><?php echo date("F d, Y", strtotime($revolving_fund->request_date)); ?></a></strong></td>
</tr>
</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        <a class="hide-print edit_field" href="<?php echo site_url("reports_revolvingfund_anc/add_liquidations/{$revolving_fund->id}") . "?back=" . uri_string(); ?>" style="color:green;padding-top: 5px;">+ Add Liquidation</a>
<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr class="green_bg">
    <td width="20%"><strong>DATE</strong></td>
    <td width="20%"><strong>LIQUIDATION FORM #</strong></td>
    <td width="20%"><strong>LIQUIDATED BY</strong></td>
    <td width="20%"><strong>PARTICULARS</strong></td>
    <td width="20%" align="right"><strong>AMOUNT</strong></td>
  </tr>
<?php 
$total = 0;
foreach($liquidations as $liq) { //print_r($liq); ?>
 <tr>
    <td><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_revolvingfund_anc/remove_liquidation/{$liq->rfl_id}") . "?back=" . uri_string(); ?>">x</a> <?php echo date("F d, Y", strtotime($liq->TxnDate)); ?></td>
    <td><?php echo $liq->RefNumber; ?></td>
    <td><?php echo $liq->Entity_FullName; ?></td>
    <td><?php echo $liq->Memo; ?></td>
    <td align="right"><?php echo number_format($liq->TotalAmount,2); $total+=$liq->TotalAmount; ?></td>
  </tr>
<?php } ?>

 <tr>
    <td width="20%"></td>
    <td width="20%"></td>
    <td width="20%"></td>
    <td width="20%"><strong>TOTAL</strong></td>
    <td width="20%" align="right"><strong><?php echo number_format($total,2); ?></strong></td>
  </tr>

</table>



<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr>
    <td width="30%"></td>
    <td width="40%">
      
<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr>
    <td width="50%">REVOLVING FUND</td>
    <td width="50%" align="right"><a class="edit_field" href="<?php echo site_url("reports_revolvingfund_anc/edit_field/{$revolving_fund->id}/revolving_fund"); ?>"><?php echo number_format($revolving_fund->revolving_fund,2); ?></a></td>
  </tr>
 <tr>
    <td width="50%">TOTAL EXPENSES</td>
    <td width="50%" align="right"><?php echo number_format($total,2); ?></td>
  </tr>
 <tr>
    <td width="50%">BALANCE</td>
    <td width="50%" align="right"><?php echo number_format(($revolving_fund->revolving_fund - $total),2); ?></td>
  </tr>
</table>
    </td>
    <td width="30%"></td>
  </tr>

</table>


      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">

        <tr>
      <td style="padding-top: 10px;" width="35%" class="footer">Prepared by:</td>
      <td style="padding-top: 10px;" width="35%" class="footer">Checked by:</td>
      <td style="padding-top: 10px;" width="35%" class="footer">Approved By:</td>
    </tr>
    <tr>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Chester Alan Tagudin</strong><br>Finance In-Charge</td>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Sr. Ma. Felina Inting, LGC</strong><br>Admin Head</td>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Msgr. Paul A. Cuison</strong><br>Director</td>

    </tr>

  </tfoot>
</table>

<?php $this->load->view('print_footer'); ?>