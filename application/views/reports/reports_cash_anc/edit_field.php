<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cash Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("reports"); ?>">Reports</a></li>
        <li class="active">Cash Reports</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<form method="post">
      <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit Field (<?php echo $field; ?>)</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

              <div class="form-group">
                <label for="field1">Field Value</label>
                <input type="text" name="field_value" class="form-control" id="field1" placeholder="Value" value="<?php echo $cash_report->$field; ?>">
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
             <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>