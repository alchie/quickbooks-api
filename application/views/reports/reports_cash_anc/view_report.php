<?php $this->load->view('print_header'); 

// default variables
$project_dumpsite_beg_balance = 0;

if($report_metas) foreach($report_metas as $meta) {
    $_var = $meta->meta_key;
    $$_var = $meta->meta_value;
}
//print_r($next_report);
//print_r($previous_report);
?>

<div class="print-topnav hide-print text-center allcaps">
<?php if($previous_report) { ?>
  <a href="<?php echo site_url( "reports_cash_anc/view_report/{$previous_report->id}" ); ?>"><< Previous Report</a> &middot;
<?php } ?>
  <a href="<?php echo site_url( ($this->input->get('back'))?$this->input->get('back'):'reports_cash_anc' ); ?>">Back</a> 
<?php if($next_report) { ?>
  &middot; <a href="<?php echo site_url( "reports_cash_anc/view_report/{$next_report->id}" ); ?>">Next Report >></a>
<?php } ?>
</div>

<table width="100%" height="100%" cellpadding="0" cellspacing="0">
  <thead>

    <tr>
      <th colspan="4">
<table width="100%" cellpadding="3" cellspacing="0">

      <tr>
      <th><?php echo qbapi_get_settings("print_header","mother_company"); ?></th>
    </tr>

    <tr>
      <th><strong style="font-size: medium; text-decoration: underline;"><?php echo qbapi_get_settings("print_header","company_name"); ?></strong></th>
    </tr>
    <tr>
      <th style="font-weight: normal;"><small><?php echo qbapi_get_settings("print_header","company_address"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_phone"); ?><br>
<?php echo qbapi_get_settings("print_header","company_website"); ?> &middot; <?php echo qbapi_get_settings("print_header","company_email"); ?></small>
      </th>
    </tr>
    <tr>
      <th>

        <br><strong style="font-size: small;font-family: sans; letter-spacing: 10px;">CASH IN BANK REPORT</strong><br><br>

      </th>
    </tr>

</table>


<?php 
$total_operations = $cash_report->beg_operations;
$total_investments = $cash_report->beg_investments;
$total_fundraising = $cash_report->beg_fundraising;
$total_project_dumpsite = $project_dumpsite_beg_balance;
?>
<table width="100%" cellpadding="3" cellspacing="0" class="bordered" style="margin-top: 5px;">
 <tr class="green_bg">
    <td width="28%" colspan="3">

<?php if( $cash_report->show_operations == 0 ) { ?>
<a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/show_operations/{$cash_report->id}") . "?back=" . uri_string(); ?>">show operations</a>
<?php } ?>
<?php if( $cash_report->show_fundraising == 0 ) { ?>
<a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/show_fundraising/{$cash_report->id}") . "?back=" . uri_string(); ?>">show fundraising</a>
<?php } ?>
<?php if( $cash_report->show_investments == 0 ) { ?>
<a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/show_investments/{$cash_report->id}") . "?back=" . uri_string(); ?>">show investments</a>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==0)) ) { ?>
<a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/set_project_dumpsite/{$cash_report->id}/1") . "?back=" . uri_string(); ?>">show project dumpsite</a>
<?php } ?>

<?php if( $cash_report->show_total == 0 ) { ?>
<a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/show_total/{$cash_report->id}") . "?back=" . uri_string(); ?>">show total</a>
<?php } ?>

    </td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td width="12%" align="right"><strong>OPERATIONS</strong> <small>
      <a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/hide_operations/{$cash_report->id}") . "?back=" . uri_string(); ?>">x</a>
    </small></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td width="12%" align="right"><strong>FUNDRAISING</strong> <small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/hide_fundraising/{$cash_report->id}") . "?back=" . uri_string(); ?>">x</a></small></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td width="12%" align="right"><strong>PROJECT DUMPSITE</strong> <small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/set_project_dumpsite/{$cash_report->id}/0") . "?back=" . uri_string(); ?>">x</a></small></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td width="12%" align="right"><strong>INVESTMENTS</strong> <small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/hide_investments/{$cash_report->id}") . "?back=" . uri_string(); ?>">x</a></small></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td width="12%" align="right"><strong>TOTAL</strong> <small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/hide_total/{$cash_report->id}") . "?back=" . uri_string(); ?>">x</a></small></td>
<?php } ?>
  </tr>
 <tr class="green_bg">
    <td width="28%" colspan="3"><strong>Beginning Balance as of</strong><br>
<strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_field/{$cash_report->id}/beg_date") . "?back=" . uri_string(); ?>"><?php echo date("F d, Y", strtotime($cash_report->beg_date)); ?></a></strong>
    </td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td width="12%" align="right"><strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_field/{$cash_report->id}/beg_operations") . "?back=" . uri_string(); ?>"><?php echo number_format($cash_report->beg_operations,2); ?></a></strong>
    </td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td width="12%" align="right"><strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_field/{$cash_report->id}/beg_fundraising") . "?back=" . uri_string(); ?>"><?php echo number_format($cash_report->beg_fundraising,2); ?></a></strong></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td width="12%" align="right"><strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_meta/{$cash_report->id}/project_dumpsite_beg_balance") . "?back=" . uri_string(); ?>"><?php echo number_format($project_dumpsite_beg_balance,2); ?></a></strong></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td width="12%" align="right"><strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_field/{$cash_report->id}/beg_investments") . "?back=" . uri_string(); ?>"><?php echo number_format($cash_report->beg_investments,2); ?></a></strong></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format(($cash_report->beg_operations+$cash_report->beg_investments+$cash_report->beg_fundraising + $project_dumpsite_beg_balance),2); ?></strong></td>
<?php } ?>
  </tr>
</table>

      </th>
    </tr>

  </thead>
  <tbody style="vertical-align: top; height: 100%;" >
    <tr>
      <td colspan="4">
        
<table width="100%" cellpadding="3" cellspacing="0" class="bordered font85p" style="margin-top: 5px;">
 <tr>
    <td width="5%"><strong>COLLECTIONS</strong> <small><a class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/add_collections/{$cash_report->id}") . "?back=" . uri_string(); ?>" style="color:green">+ Add</a></small></td>
    <td width="7%"></td>
    <td width="12%"></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
  </tr>

<?php 
$total_collections_o = 0;
$total_collections_f = 0;
$total_collections_pd = 0;
$total_collections_i = 0;
foreach($collections as $collection) { //print_r($collection); 

  ?>

 <tr>
    <td>
      <small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/remove_collection/{$collection->rc_id}") . "?back=" . uri_string(); ?>">x</a></small> 
    <?php echo date("m/d/Y", strtotime($collection->TxnDate)); ?>
  </td>
    <td><?php echo $collection->RefNumber; ?><?php echo ($collection->postdated) ? '-PDC' : ''; ?></td>
    <td><?php echo $collection->Customer_FullName; ?>
<small style="float:right;"><a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/edit_collection/{$collection->rc_id}") . "?back=" . uri_string(); ?>">edit</a></small> 
    </td>
<?php if( $collection->fund=='operations' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right">
      <?php echo number_format($collection->TotalAmount,2); 
      $total_operations += $collection->TotalAmount; 
      $total_collections_o += $collection->TotalAmount; 
      ?>
      </td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>

<?php if( $collection->fund=='fundraising' ) { ?>

<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right">
<?php echo number_format($collection->TotalAmount,2); 
      $total_fundraising += $collection->TotalAmount;
      $total_collections_f += $collection->TotalAmount;    
?>
      </td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>

<?php if( $collection->fund=='project_dumpsite' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
  <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
<td align="right">
<?php echo number_format($collection->TotalAmount,2); 
      $total_project_dumpsite += $collection->TotalAmount;
      $total_collections_pd += $collection->TotalAmount;    
?>
      </td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>


<?php if( $collection->fund=='investments' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right">
<?php echo number_format($collection->TotalAmount,2); 
      $total_investments += $collection->TotalAmount;
      $total_collections_i += $collection->TotalAmount; 
?>
      </td>
<?php } ?>
<?php } ?>

<?php if( $cash_report->show_total == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
  </tr>

<?php } ?>

<tr>
    
    <td colspan="3"></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td></td>
<?php } ?>
    
  </tr>

 <tr>
    <td  colspan="3" align="right"><strong>TOTAL COLLECTIONS</strong></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td  align="right"><strong><?php echo number_format($total_collections_o,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td  align="right"><strong><?php echo number_format($total_collections_f,2); ?></strong></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td  align="right"><strong><?php echo number_format($total_collections_pd,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td  align="right"><strong><?php echo number_format($total_collections_i,2); ?></strong></td>    
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
  </tr>

<tr>
    
    <td colspan="3"></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td></td>
<?php } ?>
    
  </tr>

<tr>

    <td ><strong>EXPENSES</strong> <small><a class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/add_expenses/{$cash_report->id}") . "?back=" . uri_string(); ?>"  style="color:green">+ Add</a></small></td>
    <td ></td>
    <td ></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td  align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
  </tr>

<?php 
$total_expenses_o = 0;
$total_expenses_f = 0;
$total_expenses_pd = 0;
$total_expenses_i = 0;
foreach($expenses as $expense) { //print_r($expense); 
  ?>

 <tr>
    <td ><small><a style="color:red" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/remove_expense/{$expense->re_id}") . "?back=" . uri_string(); ?>">x</a></small> <?php echo date("m/d/Y", strtotime($expense->TxnDate)); ?></td>
    <td ><?php echo $expense->RefNumber; ?></td>
    <td ><?php echo $expense->PayeeEntityRef_FullName; ?>
      
      <small style="float: right;"><a style="color:green" class="hide-print edit_field" href="<?php echo site_url("reports_cash_anc/edit_expense/{$expense->re_id}") . "?back=" . uri_string(); ?>">edit</a></small>

    </td>
<?php if( $expense->fund=='operations' ) { ?>

<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"><?php echo number_format($expense->Amount,2); 
    $total_operations -= $expense->Amount;
    $total_expenses_o += $expense->Amount; ?></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>

<?php if( $expense->fund=='fundraising' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"><?php echo number_format($expense->Amount,2); 
    $total_fundraising -= $expense->Amount;
    $total_expenses_f += $expense->Amount; ?></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>

<?php if( $expense->fund=='project_dumpsite' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
  <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
        <td align="right"><?php echo number_format($expense->Amount,2); 
    $total_project_dumpsite -= $expense->Amount;
    $total_expenses_pd += $expense->Amount; ?></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php } ?>

<?php if( $expense->fund=='investment' ) { ?>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"><?php echo number_format($expense->Amount,2); 
    $total_investments -= $expense->Amount;
    $total_expenses_i += $expense->Amount; ?></td>
<?php } ?>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td align="right"></td>
<?php } ?>
  </tr>

<?php } 
?>

 <tr>
    <td width="28%" colspan="3" align="right"><strong>TOTAL EXPENSES</strong></td>

<?php if( $cash_report->show_operations == 1 ) { ?>
    <td align="right"><strong><?php echo number_format($total_expenses_o,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td align="right"><strong><?php echo number_format($total_expenses_f,2); ?></strong></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td align="right"><strong><?php echo number_format($total_expenses_pd,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td align="right"><strong><?php echo number_format($total_expenses_i,2); ?></strong></td>
<?php } ?>
    <?php if( $cash_report->show_total == 1 ) { ?>
    <td  align="right"></td>
<?php } ?>
  </tr>

</table>

      </td>
    </tr>
    <tr>
      <td valign="bottom" height="100%" colspan="4">

<table width="100%" cellpadding="3" cellspacing="0" class="" style="margin-top: 5px;">
 <tr>
    <td width="28%" colspan="3"></td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td width="12%" align="right"></td>
<?php } ?>
  </tr>
 <tr class="yellow_bg bordered">
    <td width="28%" colspan="3"><strong>Ending Balance as of</strong><br>
<strong><a class="edit_field" href="<?php echo site_url("reports_cash_anc/edit_field/{$cash_report->id}/end_date") . "?back=" . uri_string(); ?>"><?php echo date("F d, Y", strtotime($cash_report->end_date)); ?></a></strong>
    </td>
<?php if( $cash_report->show_operations == 1 ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format($total_operations,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_fundraising == 1 ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format($total_fundraising,2); ?></strong></td>
<?php } ?>
<?php if( (!isset($project_dumpsite_display)) || ((isset($project_dumpsite_display)) && ($project_dumpsite_display==1)) ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format($total_project_dumpsite,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_investments == 1 ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format($total_investments,2); ?></strong></td>
<?php } ?>
<?php if( $cash_report->show_total == 1 ) { ?>
    <td width="12%" align="right"><strong><?php echo number_format(($total_operations+$total_investments+$total_fundraising+$total_project_dumpsite),2); ?></strong></td>
<?php } ?>
  </tr>
</table>

      </td>
    </tr>
  </tbody>
  <tfoot style="vertical-align: bottom;">
        <tr>
      <td style="padding-top: 10px;" width="35%" class="footer">Prepared by:</td>
      <td style="padding-top: 10px;" width="35%" class="footer">Checked by:</td>
      <td style="padding-top: 10px;" width="35%" class="footer">Approved By:</td>
    </tr>
    <tr>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Chester Alan Tagudin</strong><br>Finance In-Charge</td>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Sr. Ma. Felina Inting, LGC</strong><br>Admin Head</td>
      <td width="35%" class="footer"><br><br><strong class="allcaps">Msgr. Paul A. Cuison</strong><br>Director</td>

    </tr>

  </tfoot>
</table>

<?php $this->load->view('print_footer'); ?>