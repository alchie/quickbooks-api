<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cash Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("reports"); ?>">Reports</a></li>
        <li class="active">Cash Reports</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<form method="post">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Expenses</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">

<table class="table table-condensed table-hover table-striped">
  <?php foreach($checks as $check) { ?>
  <tr class="<?php echo ($check->sCount > 0) ? 'danger' : ''; ?>">
    
    <td> <label for="checkbox_<?php echo $check->TxnID; ?>" <?php echo ($check->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                    #<?php echo $check->RefNumber; ?>
                  </label></td>
<td> <label for="checkbox_<?php echo $check->TxnID; ?>" <?php echo ($check->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                    <?php echo $check->PayeeEntityRef_FullName; ?>
                  </label></td>
<td> <label for="checkbox_<?php echo $check->TxnID; ?>" <?php echo ($check->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                   <?php echo number_format($check->Amount,2); ?>
                  </label></td>
<td> <label for="checkbox_<?php echo $check->TxnID; ?>" <?php echo ($check->sCount > 0) ? 'style="text-decoration: line-through;"' : ''; ?>>
                   <?php echo date("F d, Y", strtotime($check->TxnDate)); ?>
                  </label></td>
<td class="text-right form-group">
    <select name="fund[<?php echo $check->TxnID; ?>]">
      <option value="" selected>- None -</option>
      <option value="operations">Operations</option>
      <option value="fundraising">Fundraising</option>
      <option value="investment">Investment</option>
      <option value="project_dumpsite">Maa - Dumpsite</option>
    </select>
</td>
  </tr>
  <?php } ?>
</table>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="row">
                <div class="col-md-6"><button type="submit" class="btn btn-primary">Submit</button></div>
                <div class="col-md-6 text-right"><?php echo $pagination; ?></div>
              </div>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>