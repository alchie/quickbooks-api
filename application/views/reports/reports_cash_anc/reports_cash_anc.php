<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cash Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("reports"); ?>">Reports</a></li>
        <li class="active">Cash Reports</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Cash Reports</h3>

              <div class="box-tools">
                <form method="get" action="<?php echo site_url("reports_cash_anc/index"); ?>">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    <a href="<?php echo site_url("reports_cash_anc/create_new"); ?>" class="btn btn-success"><i class="fa fa-plus"></i></a>
                  </div>
                </div>
              </form>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>#</th>
                  <th>Beginning Date</th>
                  <th>Ending Date</th>
                  <th class="text-right">Action</th>
                </tr>
<?php foreach($cash_reports as $cash_report) { ?>
                <tr>
                  <td><?php echo $cash_report->id; ?></td>
                  <td><?php echo date("F d, Y", strtotime($cash_report->beg_date)); ?></td>
                  <td><?php echo date("F d, Y", strtotime($cash_report->end_date)); ?></td>
                  
                  <td class="text-right"><a href="<?php echo site_url("reports_cash_anc/view_report/{$cash_report->id}"); ?>?back=<?php echo uri_string(); ?>" class="btn btn-success btn-xs">View</a></td>
                </tr>
<?php } ?>
              </table>


            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>