<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Cash Reports
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("reports"); ?>">Reports</a></li>
        <li class="active">Cash Reports</li>
      </ol>
    </section>
<?php //print_r($current_check); ?>
<?php //print_r($current_data); ?>
    <!-- Main content -->
    <section class="content">
<form method="post">
      <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Edit Collection</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <div class="form-group">
                <div class="form-control"><?php echo date("F d, Y", strtotime($current_check->TxnDate)); ?></div>
                <div class="form-control"><?php echo $current_check->PayeeEntityRef_FullName; ?></div>
                <div class="form-control"><?php echo $current_check->RefNumber; ?></div>
                <div class="form-control"><?php echo number_format($current_check->Amount,2); ?></div>
              </div>
              <div class="form-group">
                <label for="field1">Fund</label>
                    <select name="fund" class="form-control">
                      <option value="" <?php echo ($current_data->fund=='') ? 'selected' : '';?>>- None -</option>
                      <option value="operations" <?php echo ($current_data->fund=='operations') ? 'selected' : '';?>>Operations</option>
                      <option value="fundraising" <?php echo ($current_data->fund=='fundraising') ? 'selected' : '';?>>Fundraising</option>
                      <option value="investment" <?php echo ($current_data->fund=='investment') ? 'selected' : '';?>>Investment</option>
                      <option value="project_dumpsite" <?php echo ($current_data->fund=='project_dumpsite') ? 'selected' : '';?>>Maa - Dumpsite</option>
                    </select>
    
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
             <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
</form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>