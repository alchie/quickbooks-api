<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Chart of Accounts
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Chart of Accounts</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
<?php if( $accounts ) { ?>
              <h3 class="box-title">Chart of Accounts</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
<?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
<?php if( $accounts ) { ?>
              <table class="table table-hover">
                <tr>
                  <th>ACCT#</th>
                  <th>Account Name</th>
                  <th style="text-align: right;">Balance Total</th>
                </tr>
<?php 
function level_dash($level) {
  if($level) {
    while($level > 0) {
      echo "- - ";
      $level--;
    }
  }
}
foreach($accounts as $account) { 
$totalbalance = '';
if( !in_array($account->AccountType, array('Expense', 'NonPosting', 'OtherExpense', 'Income'))) {
$totalbalance = number_format($account->TotalBalance,2);
}
  ?>
                <tr>
                  <td>
                    <?php echo $account->AccountNumber; ?></td>
                  <td><?php level_dash($account->Sublevel); ?><?php echo $account->Name; ?> <sub class="badge"><?php echo $account->AccountType; ?></sub> <sub class="badge"><?php echo $account->ListID; ?></sub></td>
                  <td align="right"><?php echo $totalbalance; ?></td>
                </tr>
<?php } ?>
              </table>
<?php } else { ?>
  <p class="text-center">No Account Found!</p>
<?php } ?>

            </div>
            <!-- /.box-body -->
<?php if( $accounts ) { ?>
            <div class="box-footer text-center">
              <?php echo $pagination; ?>
            </div>
<?php } ?>
          </div>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>