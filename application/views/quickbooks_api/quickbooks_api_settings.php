<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quickbooks API <a href="<?php echo site_url("quickbooks_api/qwc"); ?>"><span class="glyphicon glyphicon-download-alt"></span></a>
      </h1> 
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Quickbooks API</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-xs-12">

<form method="post">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <div class="panel panel-default">

    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <button class="pull-right btn btn-success btn-xs" type="submit">Save Changes</button>
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Print Header
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
<div class="row">
  <div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputEmail1">Company Name</label>
        <input type="text" class="form-control" placeholder="Company Name" name="print_header[company_name]" value="<?php echo qbapi_get_settings("print_header","company_name"); ?>">
      </div>
    </div>
  <div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputEmail1">Mother Company</label>
        <input type="text" class="form-control" placeholder="Mother Company" name="print_header[mother_company]" value="<?php echo qbapi_get_settings("print_header","mother_company"); ?>">
      </div>
    </div>
  </div>

<div class="row">
  <div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputEmail1">Company Address</label>
        <input type="text" class="form-control" placeholder="Company Address" name="print_header[company_address]" value="<?php echo qbapi_get_settings("print_header","company_address"); ?>">
      </div>
</div><div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputEmail1">Company Number</label>
        <input type="text" class="form-control" placeholder="Company Phone Number" name="print_header[company_phone]" value="<?php echo qbapi_get_settings("print_header","company_phone"); ?>">
      </div>
    </div>
  </div>
<div class="row">
  <div class="col-md-6">

      <div class="form-group">
        <label for="exampleInputEmail1">Company Website</label>
        <input type="text" class="form-control" placeholder="Company Website" name="print_header[company_website]" value="<?php echo qbapi_get_settings("print_header","company_website"); ?>">
      </div>
</div><div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputEmail1">Company Email Address</label>
        <input type="text" class="form-control" placeholder="Company Email Address" name="print_header[company_email]" value="<?php echo qbapi_get_settings("print_header","company_email"); ?>">
      </div>
    </div>
  </div>
      </div>
    </div>

  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <button class="pull-right btn btn-success btn-xs" type="submit">Save Changes</button>
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Print Footer
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Prepared By Name</label>
      <input type="text" class="form-control" placeholder="Prepared By Name" name="print_footer[preparedby_name]" value="<?php echo qbapi_get_settings("print_footer","preparedby_name"); ?>">
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Prepared By Position</label>
      <input type="text" class="form-control" placeholder="Prepared By Position" name="print_footer[preparedby_position]" value="<?php echo qbapi_get_settings("print_footer","preparedby_position"); ?>">
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Checked By Name</label>
      <input type="text" class="form-control" placeholder="Checked By Name" name="print_footer[checkedby_name]" value="<?php echo qbapi_get_settings("print_footer","checkedby_name"); ?>">
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Checked By Position</label>
      <input type="text" class="form-control" placeholder="Checked By Position" name="print_footer[checkedby_position]" value="<?php echo qbapi_get_settings("print_footer","checkedby_position"); ?>">
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Approved By Name</label>
      <input type="text" class="form-control" placeholder="Approved By Name" name="print_footer[approvedby_name]" value="<?php echo qbapi_get_settings("print_footer","approvedby_name"); ?>">
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="exampleInputEmail1">Approved By Position</label>
      <input type="text" class="form-control" placeholder="Approved By Position" name="print_footer[approvedby_position]" value="<?php echo qbapi_get_settings("print_footer","approvedby_position"); ?>">
    </div>
  </div>
</div>

      </div>
    </div>
  </div>

<?php if( $queue_count > 0) { ?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingQueue">
      <h4 class="panel-title">
        <a class="pull-right btn btn-danger btn-xs" href="<?php echo site_url("quickbooks_api/clear_queue"); ?>">Clear Queue List</a>
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseQueue" aria-expanded="false" aria-controls="collapseQueue">
          Queue (<?php echo $queue_count; ?>)
        </a>
      </h4>
    </div>
    <div id="collapseQueue" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingQueue">
      <div class="panel-body">

<ul class="list-group">
<?php foreach($queue_list as $queue) { ?>
    <li class="list-group-item">
      <a class="pull-right btn btn-danger btn-xs confirm confirm_button btn-confirm" href="<?php echo site_url("quickbooks_api/delete_queue/{$queue->id}"); ?>">Delete</a>
      <?php echo $queue->request_method; ?>
    </li>
<?php } ?>
</ul>


      </div>
    </div>
  </div>
<?php } ?>


</div>
</form>
          <!-- /.box -->
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>