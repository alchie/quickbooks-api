<?php $this->load->view('header'); ?>

<?php $this->load->view('sidebar'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo date('F d, Y', strtotime("{$current_month}/{$current_day}/{$current_year}")); ?> Transactions
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url("dashboard/index/{$current_month}/{$current_year}"); ?>">Dashboard</a></li>
        <li class="active">Transactions</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

<?php 
$collapse = true;
foreach($report as $key => $val) { 
if( count($val) == 0 ) { 
  continue;
}
 ?>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading<?php echo $key; ?>">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="collapseOne">
<?php switch($key) {
  case 'sales':
    echo 'Sales Receipts';
  break;
  case 'deposits':
    echo 'Deposits';
  break;
  case 'checks':
    echo 'Checks';
  break;
  case 'invoices':
    echo 'Invoices';
  break;
  case 'payments':
    echo 'Payments';
  break;
  case 'journals':
    echo 'Journals';
  break;
}
?>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse <?php echo ($collapse) ? 'in' : ''; $collapse=false; ?>" role="tabpanel" aria-labelledby="heading<?php echo $key; ?>">
      <div class="panel-body">
<ul class="list-group">
<?php foreach($val as $item) { //print_r($item); ?>
  <?php switch($key) {
  case 'sales':
    $url = site_url('sales_receipts/items/' . $item->TxnID); 
    $title = $item->Customer_FullName . "<span class='badge pull-right'>" . number_format($item->TotalAmount,2) . "</span>";
  break;
  case 'deposits':
    $url = site_url('deposits/details/' . $item->TxnID); 
    $title = $item->DepositToAccount_FullName . "<span class='badge pull-right'>" . number_format($item->DepositTotal,2) . "</span>";
  break;
  case 'checks':
    $url = site_url('checks/voucher/' . $item->TxnNumber); 
    $title = $item->PayeeEntityRef_FullName . "<span class='badge pull-right'>" . number_format($item->Amount,2) . "</span>";
  break;
  case 'invoices':
    $url = site_url('invoices/voucher/' . $item->TxnID); 
    $title = $item->PayeeEntityRef_FullName . "<span class='badge pull-right'>" . number_format($item->Amount,2) . "</span>";
  break;
  case 'payments':
    $url = site_url('payments/items/' . $item->TxnID); 
    $title = $item->PayeeEntityRef_FullName . "<span class='badge pull-right'>" . number_format($item->Amount,2) . "</span>";
  break;
  case 'journals':
    $url = site_url('journal_entries/details/' . $item->TxnID); 
    $title = $item->PayeeEntityRef_FullName . "<span class='badge pull-right'>" . number_format($item->Amount,2) . "</span>";
  break;
}
?>
  <a class="list-group-item" href="<?php echo $url; ?>"><?php echo $title; ?></a>
<?php } ?>
</ul>
      </div>
    </div>
  </div>
<?php } ?>

</div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>