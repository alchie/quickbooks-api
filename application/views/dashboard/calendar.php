

        <div class="panel panel-default">
        <div class="panel-heading">

<div class="btn-group pull-right" role="group">
<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $reports->monthName; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
  <?php for($i=1;$i<=12;$i++) { ?>
    <li class="<?php echo ($i==$reports->month) ? 'active' : ''; ?>"><a href="<?php echo site_url("dashboard/index/{$i}/{$reports->year}"); ?>"><?php echo date('F', strtotime($i . "/1/1990")); ?></a></li>
  <?php } ?>
  </ul>
</div>

<div class="btn-group">
  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <?php echo $reports->year; ?> <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
<?php 
for($i=$years['min_year'];$i<($years['max_year']+1);$i++) { ?>
    <li class="<?php echo ($i==$reports->year) ? 'active' : ''; ?>"><a href="<?php echo site_url("dashboard/index/{$reports->month}/{$i}"); ?>"><?php echo $i; ?></a></li>
<?php } ?>
  </ul>
</div>

</div>
          <h3 class="panel-title"><?php echo $reports->monthName; ?> <?php echo $reports->year; ?></h3>
        </div>
        <div class="panel-body">
            <table class="table table-default table-condensed dcpr-calendar">
              <thead>
                <tr>
                  <th colspan="2" class="text-center"><a href="<?php echo site_url("dashboard/index/" . date('m', strtotime($reports->previous_month)) . "/" . date('Y', strtotime($reports->previous_month)) ); ?>?f=<?php echo $this->input->get('f'); ?>"><< <?php echo date('F Y', strtotime($reports->previous_month)); ?></a></th>
                  <th colspan="3" class="text-center"><?php echo $reports->monthName; ?> <?php echo $reports->year; ?></th>
                  <th colspan="2" class="text-center"><a href="<?php echo site_url("dashboard/index/" . date('m', strtotime($reports->next_month)) . "/" . date('Y', strtotime($reports->next_month)) ); ?>?f=<?php echo $this->input->get('f'); ?>"><?php echo date('F Y', strtotime($reports->next_month)); ?> >></a></th>
                </tr>
                <tr class="warning">
                  <th class="text-center" width="14.25%">Sunday</th>
                  <th class="text-center" width="14.25%">Monday</th>
                  <th class="text-center" width="14.25%">Tuesday</th>
                  <th class="text-center" width="14.25%">Wednesday</th>
                  <th class="text-center" width="14.25%">Thursday</th>
                  <th class="text-center" width="14.25%">Friday</th>
                  <th class="text-center" width="14.25%">Saturday</th>
                </tr>
              </thead>
              <tbody>
                <tr>
<?php 
$first_day = mktime(0,0,0,$reports->month, 1, $reports->year) ;
$day_of_week = date('D', $first_day);
  switch($day_of_week) {   
    case "Sun": 
      $blank = 0; 
      break;   
    case "Mon": 
      $blank = 1; 
      break;   
    case "Tue": 
      $blank = 2; 
      break;   
    case "Wed": 
      $blank = 3; 
      break;   
    case "Thu": 
      $blank = 4; 
      break;   
    case "Fri": 
      $blank = 5; 
      break;   
    case "Sat": 
      $blank = 6; 
      break;   
  }

$monthDays2 = $reports->monthDays + (7 - (($reports->monthDays + $blank) % 7));
$n=1;
for($i=1;$i<=$blank;$i++) { 
  echo '<td></td>';
  $n++;
}
$d=1;
for($i=1;$i<=$monthDays2;$i++) { 
                if( $i > $reports->monthDays) {
                  echo '<td></td>';
                } else {
                  $rUrl = site_url("dashboard/view/{$reports->month}/{$d}/{$reports->year}") . "?f=". $this->input->get('f');

                  $deposits = (isset($reports->reports["{$reports->year}-{$reports->month}-{$d}"]->deposits)) ? $reports->reports["{$reports->year}-{$reports->month}-{$d}"]->deposits : 0;
                  
                  $disburse = (isset($reports->reports["{$reports->year}-{$reports->month}-{$d}"]->disburse)) ? $reports->reports["{$reports->year}-{$reports->month}-{$d}"]->disburse : 0;

                  $sales = (isset($reports->reports["{$reports->year}-{$reports->month}-{$d}"]->sales)) ? $reports->reports["{$reports->year}-{$reports->month}-{$d}"]->sales : 0;

                  if ( ($deposits > 0) || ($disburse > 0) || ($sales > 0) ) {
                    $hasReport = "has-report"; 
                  } else {
                    $hasReport = "no-report";
                  }
                 
                  $is_current = '';
                  if( date('Y-m-d') == date('Y-m-d', strtotime("{$reports->year}-{$reports->month}-{$d}"))) {
                    $is_current = ' is_current';
                  }
                  echo '<td class="calendar_day '.$hasReport.$is_current.'"><a href="'.$rUrl.'">'. $d .'</a></td>';
              }
echo (($n % 7)==0) ? "</tr><tr>" : "";
$d++;
$n++;
} 

?>
                </tr>
              </tbody>
            </table>
        </div>
      </div>

