<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_journalentry extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_journalentry_model');
        $journal = new $this->CI->Qb_journalentry_model();
        $journal->set_order('TimeModified', 'DESC');
        $journal_data = $journal->get();
        return (($journal_data) && isset($journal_data->TimeModified)) ? $journal_data->TimeModified : false;
    }

    public function request($queue) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_JOURNALENTRIESQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_JOURNALENTRIESQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<JournalEntryQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</JournalEntryQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>JournalEntry</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'Vendor saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_journalentry_model');
            
            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $journal = new $this->CI->Qb_journalentry_model();
                $journal->setTxnid($item_obj->TxnID,true);
                $journal->setTimecreated($item_obj->TimeCreated);
                $journal->setTimemodified($item_obj->TimeModified);
                $journal->setEditsequence($item_obj->EditSequence);
                $journal->setTxnnumber($item_obj->TxnNumber);
                $journal->setTxndate($item_obj->TxnDate);
                $journal->setRefnumber($item_obj->RefNumber);
                $journal->setIsadjustment($item_obj->IsAdjustment);

                if( $journal->nonEmpty() ) {
                  $journal->update();
                } else {
                  $journal->insert();
                }

                if( $item_obj->credit_lines ) {

                  foreach($item_obj->credit_lines as $credit_line_item) {
                    
                    $credit_line_item_obj = (object) $credit_line_item;
                    $this->CI->load->model('Qb_journalentry_journalcreditline_model');
                    $credit_line = new $this->CI->Qb_journalentry_journalcreditline_model();
                    $credit_line->setJournalentryTxnid($credit_line_item_obj->JournalEntry_TxnID,TRUE);
                    $credit_line->setSortorder($credit_line_item_obj->SortOrder);
                    $credit_line->setTxnlineid($credit_line_item_obj->TxnLineID,TRUE);
                    $credit_line->setAccountListid($credit_line_item_obj->Account_ListID);
                    $credit_line->setAccountFullname($credit_line_item_obj->Account_FullName);
                    $credit_line->setAmount($credit_line_item_obj->Amount);
                    $credit_line->setMemo($credit_line_item_obj->Memo);
                    $credit_line->setEntityListid($credit_line_item_obj->Entity_ListID);
                    $credit_line->setEntityFullname($credit_line_item_obj->Entity_FullName);
                    $credit_line->setClassListid($credit_line_item_obj->Class_ListID);
                    $credit_line->setClassFullname($credit_line_item_obj->Class_FullName);
                    $credit_line->setBillablestatus($credit_line_item_obj->BillableStatus);
                    if( $credit_line->nonEmpty() ) {
                      $credit_line->update();
                    } else {
                      $credit_line->insert();
                    }
                  }
                }

                if( $item_obj->debit_lines ) {

                  foreach($item_obj->debit_lines as $debit_line_item) {

                    $debit_line_item_obj = (object) $debit_line_item;
                    $this->CI->load->model('Qb_journalentry_journaldebitline_model');
                    $debit_line = new $this->CI->Qb_journalentry_journaldebitline_model();
                    $debit_line->setJournalentryTxnid($debit_line_item_obj->JournalEntry_TxnID,TRUE);
                    $debit_line->setSortorder($debit_line_item_obj->SortOrder);
                    $debit_line->setTxnlineid($debit_line_item_obj->TxnLineID,TRUE);
                    $debit_line->setAccountListid($debit_line_item_obj->Account_ListID);
                    $debit_line->setAccountFullname($debit_line_item_obj->Account_FullName);
                    $debit_line->setAmount($debit_line_item_obj->Amount);
                    $debit_line->setMemo($debit_line_item_obj->Memo);
                    $debit_line->setEntityListid($debit_line_item_obj->Entity_ListID);
                    $debit_line->setEntityFullname($debit_line_item_obj->Entity_FullName);
                    $debit_line->setClassListid($debit_line_item_obj->Class_ListID);
                    $debit_line->setClassFullname($debit_line_item_obj->Class_FullName);
                    $debit_line->setBillablestatus($debit_line_item_obj->BillableStatus);
                    if( $debit_line->nonEmpty() ) {
                      $debit_line->update();
                    } else {
                      $debit_line->insert();
                    }
                  }
                }

                $this->insert_dataext_items($item_obj);


            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'JournalEntryRet') {

          $JournalDebitLine = $item->getElementsByTagName('JournalCreditLine');
          $credit_lines = array();

          if( $JournalDebitLine->length > 0 ) {
            foreach($JournalDebitLine as $credit_line) {
              $credit_lines[] = array(
                'JournalEntry_TxnID' => $this->get_text_content($item, array('TxnID')), 
                'SortOrder' => $this->get_text_content($credit_line, array('SortOrder')), 
                'TxnLineID' => $this->get_text_content($credit_line, array('TxnLineID')), 
                'Account_ListID' => $this->get_text_content($credit_line, array('AccountRef','ListID')), 
                'Account_FullName' => $this->get_text_content($credit_line, array('AccountRef','FullName')), 
                'Amount' => $this->get_text_content($credit_line, array('Amount')), 
                'Memo' => $this->get_text_content($credit_line, array('Memo')), 
                'Entity_ListID' => $this->get_text_content($credit_line, array('EntityRef','ListID')), 
                'Entity_FullName' => $this->get_text_content($credit_line, array('EntityRef','FullName')), 
                'Class_ListID' => $this->get_text_content($credit_line, array('ClassRef','ListID')), 
                'Class_FullName' => $this->get_text_content($credit_line, array('ClassRef','FullName')), 
                'BillableStatus' => $this->get_text_content($credit_line, array('BillableStatus')),
              );
            }
          }

          $JournalDebitLine = $item->getElementsByTagName('JournalDebitLine');
          $debit_lines = array();

          if( $JournalDebitLine->length > 0 ) {
            foreach($JournalDebitLine as $debit_line) {
              $debit_lines[] = array(
                'JournalEntry_TxnID' => $this->get_text_content($item, array('TxnID')), 
                'SortOrder' => $this->get_text_content($debit_line, array('SortOrder')), 
                'TxnLineID' => $this->get_text_content($debit_line, array('TxnLineID')), 
                'Account_ListID' => $this->get_text_content($debit_line, array('AccountRef','ListID')), 
                'Account_FullName' => $this->get_text_content($debit_line, array('AccountRef','FullName')), 
                'Amount' => $this->get_text_content($debit_line, array('Amount')), 
                'Memo' => $this->get_text_content($debit_line, array('Memo')), 
                'Entity_ListID' => $this->get_text_content($debit_line, array('EntityRef','ListID')), 
                'Entity_FullName' => $this->get_text_content($debit_line, array('EntityRef','FullName')), 
                'Class_ListID' => $this->get_text_content($debit_line, array('ClassRef','ListID')), 
                'Class_FullName' => $this->get_text_content($debit_line, array('ClassRef','FullName')), 
                'BillableStatus' => $this->get_text_content($debit_line, array('BillableStatus')),
              );
            }
          }

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')), 
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')), 
            'TimeModified' => $this->get_text_content($item, array('TimeModified')), 
            'EditSequence' => $this->get_text_content($item, array('EditSequence')), 
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')), 
            'TxnDate' => $this->get_text_content($item, array('TxnDate')), 
            'RefNumber' => $this->get_text_content($item, array('RefNumber')), 
            'IsAdjustment' => $this->get_text_content($item, array('IsAdjustment')), 
            'credit_lines' => $credit_lines,
            'debit_lines' => $debit_lines,
            'DataExtItems' => $this->get_dataext_items($item, 'JournalEntry', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_journalentry_model');
        $query = new $this->CI->Qb_journalentry_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file Global_variables.php */