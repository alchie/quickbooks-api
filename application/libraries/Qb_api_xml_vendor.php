<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_vendor extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_vendor_model');
        $vendors = new $this->CI->Qb_vendor_model();
        $vendors->set_order('TimeModified', 'DESC');
        $vendors_data = $vendors->get();
        return (($vendors_data) && isset($vendors_data->TimeModified)) ? $vendors_data->TimeModified : false;
    }

    public function request($queue) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_VENDORQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VENDORQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<VendorQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</VendorQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>Vendor</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'Vendor saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_vendor_model');
            
            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $vendors = new $this->CI->Qb_vendor_model();
                $vendors->setListid($item_obj->ListID,true);
                $vendors->setTimecreated($item_obj->TimeCreated);
                $vendors->setTimemodified($item_obj->TimeModified);
                $vendors->setEditsequence($item_obj->EditSequence);
                $vendors->setName($item_obj->Name);
                $vendors->setIsactive(($item_obj->IsActive)?1:0);
                $vendors->setCompanyname($item_obj->CompanyName);
                $vendors->setSalutation($item_obj->Salutation);
                $vendors->setFirstname($item_obj->FirstName);
                $vendors->setMiddlename($item_obj->MiddleName);
                $vendors->setLastname($item_obj->LastName);
                $vendors->setVendoraddressAddr1($item_obj->VendorAddress_Addr1);
                $vendors->setVendoraddressAddr2($item_obj->VendorAddress_Addr2);
                $vendors->setVendoraddressAddr3($item_obj->VendorAddress_Addr3);
                $vendors->setVendoraddressAddr4($item_obj->VendorAddress_Addr4);
                $vendors->setVendoraddressAddr5($item_obj->VendorAddress_Addr5);
                $vendors->setVendoraddressCity($item_obj->VendorAddress_City);
                $vendors->setVendoraddressState($item_obj->VendorAddress_State);
                $vendors->setVendoraddressPostalcode($item_obj->VendorAddress_PostalCode);
                $vendors->setVendoraddressCountry($item_obj->VendorAddress_Country);
                $vendors->setVendoraddressNote($item_obj->VendorAddress_Note);
                $vendors->setVendoraddressblockAddr1($item_obj->VendorAddressBlock_Addr1);
                $vendors->setVendoraddressblockAddr2($item_obj->VendorAddressBlock_Addr2);
                $vendors->setVendoraddressblockAddr3($item_obj->VendorAddressBlock_Addr3);
                $vendors->setVendoraddressblockAddr4($item_obj->VendorAddressBlock_Addr4);
                $vendors->setVendoraddressblockAddr5($item_obj->VendorAddressBlock_Addr5);
                $vendors->setPhone($item_obj->Phone);
                $vendors->setAltphone($item_obj->AltPhone);
                $vendors->setFax($item_obj->Fax);
                $vendors->setEmail($item_obj->Email);
                $vendors->setContact($item_obj->Contact);
                $vendors->setAltcontact($item_obj->AltContact);
                $vendors->setNameoncheck($item_obj->NameOnCheck);
                $vendors->setAccountnumber($item_obj->AccountNumber);
                $vendors->setNotes($item_obj->Notes);
                $vendors->setVendortypeListid($item_obj->VendorType_ListID);
                $vendors->setVendortypeFullname($item_obj->VendorType_FullName);
                $vendors->setTermsListid($item_obj->Terms_ListID);
                $vendors->setTermsFullname($item_obj->Terms_FullName);
                $vendors->setCreditlimit($item_obj->CreditLimit);
                $vendors->setVendortaxident($item_obj->VendorTaxIdent);
                $vendors->setIsvendoreligiblefor1099($item_obj->IsVendorEligibleFor1099);
                $vendors->setBalance($item_obj->Balance);
                $vendors->setBillingrateListid($item_obj->BillingRate_ListID);
                $vendors->setBillingrateFullname($item_obj->BillingRate_FullName);

                if( $vendors->nonEmpty() ) {
                  $vendors->update();
                } else {
                  $vendors->insert();
                }

                
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'VendorRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')), 
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')), 
            'TimeModified' => $this->get_text_content($item, array('TimeModified')), 
            'EditSequence' => $this->get_text_content($item, array('EditSequence')), 
            'Name' => $this->get_text_content($item, array('Name')), 
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0), 
            'CompanyName' => $this->get_text_content($item, array('CompanyName')), 
            'Salutation' => $this->get_text_content($item, array('Salutation')), 
            'FirstName' => $this->get_text_content($item, array('FirstName')), 
            'MiddleName' => $this->get_text_content($item, array('MiddleName')), 
            'LastName' => $this->get_text_content($item, array('LastName')), 
            'VendorAddress_Addr1' => $this->get_text_content($item, array('VendorAddress','Addr1')), 
            'VendorAddress_Addr2' => $this->get_text_content($item, array('VendorAddress','Addr2')), 
            'VendorAddress_Addr3' => $this->get_text_content($item, array('VendorAddress','Addr3')), 
            'VendorAddress_Addr4' => $this->get_text_content($item, array('VendorAddress','Addr4')), 
            'VendorAddress_Addr5' => $this->get_text_content($item, array('VendorAddress','Addr5')), 
            'VendorAddress_City' => $this->get_text_content($item, array('VendorAddress','City')), 
            'VendorAddress_State' => $this->get_text_content($item, array('VendorAddress','State')), 
            'VendorAddress_PostalCode' => $this->get_text_content($item, array('VendorAddress','PostalCode')), 
            'VendorAddress_Country' => $this->get_text_content($item, array('VendorAddress','Country')), 
            'VendorAddress_Note' => $this->get_text_content($item, array('VendorAddress','Note')), 
            'VendorAddressBlock_Addr1' => $this->get_text_content($item, array('VendorAddressBlock','Addr1')), 
            'VendorAddressBlock_Addr2' => $this->get_text_content($item, array('VendorAddressBlock','Addr2')), 
            'VendorAddressBlock_Addr3' => $this->get_text_content($item, array('VendorAddressBlock','Addr3')), 
            'VendorAddressBlock_Addr4' => $this->get_text_content($item, array('VendorAddressBlock','Addr4')), 
            'VendorAddressBlock_Addr5' => $this->get_text_content($item, array('VendorAddressBlock','Addr5')), 
            'Phone' => $this->get_text_content($item, array('Phone')), 
            'AltPhone' => $this->get_text_content($item, array('AltPhone')), 
            'Fax' => $this->get_text_content($item, array('Fax')), 
            'Email' => $this->get_text_content($item, array('Email')), 
            'Contact' => $this->get_text_content($item, array('Contact')), 
            'AltContact' => $this->get_text_content($item, array('AltContact')), 
            'NameOnCheck' => $this->get_text_content($item, array('NameOnCheck')), 
            'AccountNumber' => $this->get_text_content($item, array('AccountNumber')), 
            'Notes' => $this->get_text_content($item, array('Notes')), 
            'VendorType_ListID' => $this->get_text_content($item, array('VendorTypeRef','ListID')), 
            'VendorType_FullName' => $this->get_text_content($item, array('VendorTypeRef','FullName')), 
            'Terms_ListID' => $this->get_text_content($item, array('TermsRef','ListID')), 
            'Terms_FullName' => $this->get_text_content($item, array('TermsRef','FullName')), 
            'CreditLimit' => $this->get_text_content($item, array('CreditLimit')), 
            'VendorTaxIdent' => $this->get_text_content($item, array('VendorTaxIdent')), 
            'IsVendorEligibleFor1099' => $this->get_text_content($item, array('IsVendorEligibleFor1099')), 
            'Balance' => $this->get_text_content($item, array('Balance')), 
            'BillingRate_ListID' => $this->get_text_content($item, array('BillingRateRef','ListID')), 
            'BillingRate_FullName' => $this->get_text_content($item, array('BillingRateRef','FullName')), 
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_vendor_model');
        $query = new $this->CI->Qb_vendor_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
     
}

/* End of file Global_variables.php */