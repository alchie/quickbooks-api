<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_account extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_account_model');
        $accounts = new $this->CI->Qb_account_model();
        $accounts->set_order('TimeModified', 'DESC');
        $accounts_data = $accounts->get();
        return (($accounts_data) && isset($accounts_data->TimeModified)) ? $accounts_data->TimeModified : false;
    }

    public function request($queue) {

        $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
        $requestID = $queue->id;
        $options = json_decode($queue->options);
        $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' .  "\n" .
'<?qbxml version="13.0"?>' .  "\n" .
'<QBXML>' .  "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' .  "\n" .
'<AccountQueryRq requestID="'.$requestID.'">' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</AccountQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>Account</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->data, 'Account saveItems');
        //php_error_log($this->items, 'Account saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_account_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $accounts = new $this->CI->Qb_account_model();
                $accounts->setListid($item_obj->ListID,true);
                $accounts->setTimecreated($item_obj->TimeCreated);
                $accounts->setTimemodified($item_obj->TimeModified);
                $accounts->setEditsequence($item_obj->EditSequence);
                $accounts->setName($item_obj->Name);
                $accounts->setFullname($item_obj->FullName);
                $accounts->setIsactive(($item_obj->IsActive)?1:0);
                $accounts->setParentListid($item_obj->Parent_ListID);
                $accounts->setParentFullname($item_obj->Parent_FullName);
                $accounts->setSublevel($item_obj->Sublevel);
                $accounts->setAccounttype($item_obj->AccountType);
                $accounts->setSpecialaccounttype($item_obj->SpecialAccountType);
                $accounts->setAccountnumber($item_obj->AccountNumber);
                $accounts->setBanknumber($item_obj->BankNumber);
                $accounts->setDescrip($item_obj->Descrip);
                $accounts->setBalance($item_obj->Balance);
                $accounts->setTotalbalance($item_obj->TotalBalance);
                $accounts->setCashflowclassification($item_obj->CashFlowClassification);

                if( $accounts->nonEmpty() ) {
                  $accounts->update();
                } else {
                  $accounts->insert();
                }

                $this->insert_dataext_items($item_obj);
                
            }
        }

    }

    protected function _populate_items() {

      foreach($this->data as $item) {

        if( $item->nodeName == 'AccountRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, 'ListID'), 
            'TimeCreated' => $this->get_text_content($item, 'TimeCreated'), 
            'TimeModified' => $this->get_text_content($item, 'TimeModified'), 
            'EditSequence' => $this->get_text_content($item, 'EditSequence'), 
            'Name' => $this->get_text_content($item, 'Name'), 
            'FullName' => $this->get_text_content($item, 'FullName'), 
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'Parent_ListID' => $this->get_text_content($item, array('ParentRef', 'ListID')), 
            'Parent_FullName' => $this->get_text_content($item, array('ParentRef', 'FullName')),
            'Sublevel' => $this->get_text_content($item, 'Sublevel'), 
            'AccountType' => $this->get_text_content($item, 'AccountType'), 
            'SpecialAccountType' => $this->get_text_content($item, 'SpecialAccountType'), 
            'AccountNumber' => $this->get_text_content($item, 'AccountNumber'), 
            'BankNumber' => $this->get_text_content($item, 'BankNumber'), 
            'Descrip' => $this->get_text_content($item, 'Descrip'), 
            'Balance' => $this->get_text_content($item, 'Balance'), 
            'TotalBalance' => $this->get_text_content($item, 'TotalBalance'), 
            'CashFlowClassification' => $this->get_text_content($item, 'CashFlowClassification'), 
            'DataExtItems' => $this->get_dataext_items($item, 'Account', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_account_model');
        $query = new $this->CI->Qb_account_model();
        $query->setListid($ListID,true);
        $query->delete();
    }

}

/* End of file Global_variables.php */