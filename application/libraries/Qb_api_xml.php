<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml {
    
    public $continueOnError = TRUE;
    
    public function get_text_content($item, $tags, $default='')
    {
       if( is_array($tags) ) {
        $n = 0;
        foreach($tags as $tag) {
          if( $item->childNodes->length > 0 ) {
            foreach($item->childNodes as $childNode) {
              if( $childNode->nodeName == $tag ) {
                $item = $childNode;
                $n++;
                break;
              }
            }
          }
        }
        if( count($tags) == $n ) {
          return $item->nodeValue;
        } else {
          return $default;
        }
       } else {
          $item = $item->getElementsByTagName($tags);
          if( ($item->length > 0) && ($item->item(0)) ) {
            return $item->item(0)->textContent;
          } 
       }
       return $default;
    }

    public function get_dataext_items($item, $Type, $ID) {

          $DataExtRet = $item->getElementsByTagName('DataExtRet');
            $DataExtItems = array();

            if( $DataExtRet ) {
              foreach($DataExtRet as $dataext_item) {
                $DataExtItems[] = array(
                  'EntityType' => $Type, 
                  'TxnType' => $Type, 
                  'Entity_ListID' => $this->get_text_content($item, $ID), 
                  'Txn_TxnID' => $this->get_text_content($item, $ID),
                  'OwnerID' => $this->get_text_content($dataext_item, array('OwnerID')), 
                  'DataExtName' => $this->get_text_content($dataext_item, array('DataExtName')), 
                  'DataExtType' => $this->get_text_content($dataext_item, array('DataExtType')), 
                  'DataExtValue' => $this->get_text_content($dataext_item, array('DataExtValue')), 
                );
              }
            }
            return $DataExtItems;
    }

    public function insert_dataext_items($item_obj, $delete_first=false, $type='', $ID='') {

        $this->CI->load->model('Qb_dataext_model');

        if( $delete_first ) {
                 
                  $lineItem = new $this->CI->Qb_dataext_model();
                  $lineItem->setEntitytype($type,TRUE);
                  $lineItem->setTxntype($type,TRUE);
                  $lineItem->setEntityListid($ID,TRUE);
                  $lineItem->setTxnTxnid($ID,TRUE);
                  $lineItem->delete();
        }

        if( count($item_obj->DataExtItems) > 0 ) {

            
            foreach( $item_obj->DataExtItems as $data_item ) {

                $data_item_obj = (object) $data_item;

                $dataext = new $this->CI->Qb_dataext_model();
                $dataext->setEntitytype($data_item_obj->EntityType,TRUE);
                $dataext->setTxntype($data_item_obj->TxnType);
                $dataext->setEntityListid($data_item_obj->Entity_ListID,TRUE);
                $dataext->setTxnTxnid($data_item_obj->Txn_TxnID);
                $dataext->setOwnerid($data_item_obj->OwnerID);
                $dataext->setDataextname($data_item_obj->DataExtName,TRUE);
                $dataext->setDataexttype($data_item_obj->DataExtType);
                $dataext->setDataextvalue($data_item_obj->DataExtValue);

                if( $dataext->nonEmpty() ) {
                  $dataext->update();
                } else {
                  $dataext->insert();
                }

            }
        }           
    }
   
}