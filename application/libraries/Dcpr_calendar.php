<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Dcpr_calendar {
    
    var $month;
    var $day;
    var $year;
    var $monthDays;
    var $monthName;
    var $currentDate;
    var $reports = array();

    public function __construct()
    {
       $this->month = date('m');
       $this->day = date('d');
       $this->year = date('Y');
       $this->monthDays = date('t');
       $this->monthName = date('F');
       $this->currentDate = date('Y-m-d');
       $this->next_month = date('Y-m-d', strtotime(date('Y-m-d')." +1 month"));
       $this->previous_month = date('Y-m-d', strtotime(date('Y-m-d')." -1 month"));
    }
    
    public function setMonth($m) {
        $this->month = $m;
        //$this->monthDays = cal_days_in_month(0, $this->month, $this->year);
        $this->_setDateValues();
    }

    public function setDay($d) {
        $this->day = $d;
        $this->_setDateValues();
    }

    public function setYear($y) {
        $this->year = $y;
        $this->_setDateValues();
    }

    public function setDate($d) {
        $this->month = date('m', strtotime($d));
        $this->day = date('d', strtotime($d));
        $this->year = date('Y', strtotime($d));
        $this->_setDateValues();
    }

    private function _setDateValues() {
        $this->monthDays = date('t', strtotime($this->month."/".$this->day."/".$this->year));
        $this->monthName = date('F', strtotime($this->month."/".$this->day."/".$this->year));
        $this->currentDate = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year));
        
        $this->next_month = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." +1 month"));
        $this->previous_month = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." -1 month"));

        $this->next_day = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." +1 day"));
        $this->previous_day = date('Y-m-d', strtotime($this->month."/".$this->day."/".$this->year." -1 day"));
    }

    public function getReport() {

        $ci = get_instance();
        $ci->load->model('Qb_deposit_model');
        $ci->load->model('Qb_check_model');
        $ci->load->model('Qb_salesreceipt_model');
        $ci->load->model('Qb_invoice_model');
        $ci->load->model('Qb_receivepayment_model');
        $ci->load->model('Qb_journalentry_model');

        $cdate = $this->year . '-' . $this->month . '-' . $this->day;
        $deposit = new $ci->Qb_deposit_model;
        $deposit->setTxndate($cdate,true);
        
        $checks = new $ci->Qb_check_model;
        $checks->setTxndate($cdate,true);
        
        $sales = new $ci->Qb_salesreceipt_model;
        $sales->setTxndate($cdate,true);

        $invoices = new $ci->Qb_invoice_model;
        $invoices->setTxndate($cdate,true);

        $payments = new $ci->Qb_receivepayment_model;
        $payments->setTxndate($cdate,true);
        
        $journals = new $ci->Qb_journalentry_model;
        $journals->setTxndate($cdate,true);

        $results = array(
            'sales' => $sales->count_all_results(),
            'checks' => $checks->count_all_results(),
            'deposits' => $deposit->count_all_results(),
            'invoices' => $invoices->count_all_results(),
            'payments' => $payments->count_all_results(),
            'journals' => $journals->count_all_results(),
        );

        return $results;
    }

    public function getReports() {

        $ci = get_instance();
        $ci->load->model('Qb_deposit_model');
        $ci->load->model('Qb_check_model');
        $ci->load->model('Qb_salesreceipt_model');
        $ci->load->model('Qb_invoice_model');
        $ci->load->model('Qb_receivepayment_model');
        $ci->load->model('Qb_journalentry_model');

        for($i=1;$i<=$this->monthDays;$i++) {
            
            $cdate = $this->year . '-' . $this->month . '-' . $i;
            $deposit = new $ci->Qb_deposit_model;
            $deposit->setTxndate($cdate,true);
            
            $checks = new $ci->Qb_check_model;
            $checks->setTxndate($cdate,true);
            
            $sales = new $ci->Qb_salesreceipt_model;
            $sales->setTxndate($cdate,true);

            $invoices = new $ci->Qb_invoice_model;
            $invoices->setTxndate($cdate,true);

            $payments = new $ci->Qb_receivepayment_model;
            $payments->setTxndate($cdate,true);
            
            $journals = new $ci->Qb_journalentry_model;
            $journals->setTxndate($cdate,true);

            $results = array(
                'sales' => $sales->count_all_results(),
                'checks' => $checks->count_all_results(),
                'deposits' => $deposit->count_all_results(),
                'invoices' => $invoices->count_all_results(),
                'payments' => $payments->count_all_results(),
                'journals' => $journals->count_all_results(),
                );

            $this->reports[$cdate] = (object) $results;
        }
    }

    public function getMaxMinYear() {
        $ci = get_instance();
        $ci->load->model('Qb_deposit_model');
        $reports = new $ci->Qb_deposit_model();

        $reports->set_select("(SELECT MAX(TxnDate) FROM  `qb_deposit`) as dep_max_date");
        $reports->set_select("(SELECT MIN(TxnDate) FROM  `qb_deposit`) as dep_min_date");

        $reports->set_select("(SELECT MAX(TxnDate) FROM  `qb_check`) as dis_max_date");
        $reports->set_select("(SELECT MIN(TxnDate) FROM  `qb_check`) as dis_min_date");

        $reports->set_select("(SELECT MAX(TxnDate) FROM  `qb_salesreceipt`) as sales_max_date");
        $reports->set_select("(SELECT MIN(TxnDate) FROM  `qb_salesreceipt`) as sales_min_date");
        
        $reports_data = $reports->get();

        if( $reports_data ) {
            $min_year = ( date( 'Y', strtotime($reports_data->dep_min_date) ) > date( 'Y', strtotime($reports_data->dis_min_date) ) ) ? date( 'Y', strtotime($reports_data->dis_min_date) ) : date( 'Y', strtotime($reports_data->dep_min_date) );
            $max_year = ( date( 'Y', strtotime($reports_data->dep_max_date) ) > date( 'Y', strtotime($reports_data->dis_max_date) ) ) ? date( 'Y', strtotime($reports_data->dep_max_date) ) : date( 'Y', strtotime($reports_data->dis_max_date) );
        } else {
            $min_year = date( 'Y' );
            $max_year = date( 'Y' );
        }
        return array(
            'min_year' => $min_year,
            'max_year' => $max_year
            );
    }

    public function init() {
        $this->getReports();
        return $this;
    }
}

/* End of file Global_variables.php */