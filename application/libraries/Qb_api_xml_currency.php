<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_currency extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_currency_model');
        $model = new $this->CI->Qb_currency_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<CurrencyQueryRq requestID="'.$requestID.'" >' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</CurrencyQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'CurrencyRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_currency_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_currency_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setIsactive(($item_obj->IsActive)?1:0);
                $model->setCurrencycode($item_obj->CurrencyCode);
                $model->setCurrencyCurrencyformatThousandseparator($item_obj->Currency_CurrencyFormat_ThousandSeparator);
                $model->setCurrencyCurrencyformatThousandseparatorgrouping($item_obj->Currency_CurrencyFormat_ThousandSeparatorGrouping);
                $model->setCurrencyCurrencyformatDecimalplaces($item_obj->Currency_CurrencyFormat_DecimalPlaces);
                $model->setCurrencyCurrencyformatDecimalseparator($item_obj->Currency_CurrencyFormat_DecimalSeparator);
                $model->setIsuserdefinedcurrency($item_obj->IsUserDefinedCurrency);
                $model->setExchangerate($item_obj->ExchangeRate);
                $model->setAsofdate($item_obj->AsOfDate);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'CurrencyRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'CurrencyCode' => $this->get_text_content($item, array('CurrencyCode')),
            'Currency_CurrencyFormat_ThousandSeparator' => $this->get_text_content($item, array('CurrencyFormat','ThousandSeparator')),
            'Currency_CurrencyFormat_ThousandSeparatorGrouping' => $this->get_text_content($item, array('CurrencyFormat','ThousandSeparatorGrouping')),
            'Currency_CurrencyFormat_DecimalPlaces' => $this->get_text_content($item, array('CurrencyFormat','DecimalPlaces')),
            'Currency_CurrencyFormat_DecimalSeparator' => $this->get_text_content($item, array('CurrencyFormat','DecimalSeparator')),
            'IsUserDefinedCurrency' => $this->get_text_content($item, array('IsUserDefinedCurrency')),
            'ExchangeRate' => $this->get_text_content($item, array('ExchangeRate')),
            'AsOfDate' => $this->get_text_content($item, array('AsOfDate')),
            'DataExtItems' => $this->get_dataext_items($item, 'Currency', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_currency_model');
        $query = new $this->CI->Qb_currency_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file */
