<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_host extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<HostQueryRq requestID="'.$requestID.'" >' .  "\n" .
'<IncludeListMetaData>' .  "\n" .
'<IncludeMaxCapacity>true</IncludeMaxCapacity>' .  "\n" .
'</IncludeListMetaData>' .  "\n" .
'</HostQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'HostRet--saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_host_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_host_model();
                $model->setProductname($item_obj->ProductName,TRUE);
                $model->setMajorversion($item_obj->MajorVersion);
                $model->setMinorversion($item_obj->MinorVersion);
                $model->setCountry($item_obj->Country);
                $model->setSupportedqbxmlversion($item_obj->SupportedQBXMLVersion);
                $model->setIsautomaticlogin(($item_obj->IsAutomaticLogin=='true')?1:0);
                $model->setQbfilemode($item_obj->QBFileMode);
                $model->setListmetadata($item_obj->ListMetaData);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'HostRet') {

          $this->items[] = array(
            'ProductName' => $this->get_text_content($item, array('ProductName')),
            'MajorVersion' => $this->get_text_content($item, array('MajorVersion')),
            'MinorVersion' => $this->get_text_content($item, array('MinorVersion')),
            'Country' => $this->get_text_content($item, array('Country')),
            'SupportedQBXMLVersion' => $this->get_text_content($item, array('SupportedQBXMLVersion')),
            'IsAutomaticLogin' => $this->get_text_content($item, array('IsAutomaticLogin')),
            'QBFileMode' => $this->get_text_content($item, array('QBFileMode')),
            'ListMetaData' => $this->get_text_content($item, array('ListMetaData')),

          );

        }
      }
    }
 
}

/* End of file */
