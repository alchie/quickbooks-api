<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_salesreceipt extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_salesreceipt_model');
        $sales = new $this->CI->Qb_salesreceipt_model();
        $sales->set_order('TimeModified', 'DESC');
        $sales_data = $sales->get();
        return (($sales_data) && isset($sales_data->TimeModified)) ? $sales_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_SALESRECEIPTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SALESRECEIPTQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<SalesReceiptQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</SalesReceiptQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>SalesReceipt</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'SalesReceiptRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_salesreceipt_model');
            $this->CI->load->model('Qb_salesreceipt_salesreceiptline_model');
            $this->CI->load->model('Qb_salesreceipt_salesreceiptlinegroup_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                //print_r( $item_obj );

                $receipt = new $this->CI->Qb_salesreceipt_model();
                $receipt->setTxnid($item_obj->TxnID,TRUE);
                $receipt->setTimecreated($item_obj->TimeCreated);
                $receipt->setTimemodified($item_obj->TimeModified);
                $receipt->setEditsequence($item_obj->EditSequence);
                $receipt->setTxnnumber($item_obj->TxnNumber);
                $receipt->setCustomerListid($item_obj->Customer_ListID);
                $receipt->setCustomerFullname($item_obj->Customer_FullName);
                $receipt->setClassListid($item_obj->Class_ListID);
                $receipt->setClassFullname($item_obj->Class_FullName);
                $receipt->setTemplateListid($item_obj->Template_ListID);
                $receipt->setTemplateFullname($item_obj->Template_FullName);
                $receipt->setTxndate($item_obj->TxnDate);
                $receipt->setRefnumber($item_obj->RefNumber);
                $receipt->setBilladdressAddr1($item_obj->BillAddress_Addr1);
                $receipt->setBilladdressAddr2($item_obj->BillAddress_Addr2);
                $receipt->setBilladdressAddr3($item_obj->BillAddress_Addr3);
                $receipt->setBilladdressAddr4($item_obj->BillAddress_Addr4);
                $receipt->setBilladdressAddr5($item_obj->BillAddress_Addr5);
                $receipt->setBilladdressCity($item_obj->BillAddress_City);
                $receipt->setBilladdressState($item_obj->BillAddress_State);
                $receipt->setBilladdressPostalcode($item_obj->BillAddress_PostalCode);
                $receipt->setBilladdressCountry($item_obj->BillAddress_Country);
                $receipt->setBilladdressNote($item_obj->BillAddress_Note);
                $receipt->setBilladdressblockAddr1($item_obj->BillAddressBlock_Addr1);
                $receipt->setBilladdressblockAddr2($item_obj->BillAddressBlock_Addr2);
                $receipt->setBilladdressblockAddr3($item_obj->BillAddressBlock_Addr3);
                $receipt->setBilladdressblockAddr4($item_obj->BillAddressBlock_Addr4);
                $receipt->setBilladdressblockAddr5($item_obj->BillAddressBlock_Addr5);
                $receipt->setShipaddressAddr1($item_obj->ShipAddress_Addr1);
                $receipt->setShipaddressAddr2($item_obj->ShipAddress_Addr2);
                $receipt->setShipaddressAddr3($item_obj->ShipAddress_Addr3);
                $receipt->setShipaddressAddr4($item_obj->ShipAddress_Addr4);
                $receipt->setShipaddressAddr5($item_obj->ShipAddress_Addr5);
                $receipt->setShipaddressCity($item_obj->ShipAddress_City);
                $receipt->setShipaddressState($item_obj->ShipAddress_State);
                $receipt->setShipaddressPostalcode($item_obj->ShipAddress_PostalCode);
                $receipt->setShipaddressCountry($item_obj->ShipAddress_Country);
                $receipt->setShipaddressNote($item_obj->ShipAddress_Note);
                $receipt->setShipaddressblockAddr1($item_obj->ShipAddressBlock_Addr1);
                $receipt->setShipaddressblockAddr2($item_obj->ShipAddressBlock_Addr2);
                $receipt->setShipaddressblockAddr3($item_obj->ShipAddressBlock_Addr3);
                $receipt->setShipaddressblockAddr4($item_obj->ShipAddressBlock_Addr4);
                $receipt->setShipaddressblockAddr5($item_obj->ShipAddressBlock_Addr5);
                $receipt->setIsPending($item_obj->IsPending);
                $receipt->setChecknumber($item_obj->CheckNumber);
                $receipt->setPaymentmethodListid($item_obj->PaymentMethod_ListID);
                $receipt->setPaymentMethodFullname($item_obj->PaymentMethod_FullName);
                $receipt->setDuedate($item_obj->DueDate);
                $receipt->setSalesrepListid($item_obj->SalesRep_ListID);
                $receipt->setSalesrepFullname($item_obj->SalesRep_FullName);
                $receipt->setShipdate($item_obj->ShipDate);
                $receipt->setShipmethodListid($item_obj->ShipMethod_ListID);
                $receipt->setShipmethodFullname($item_obj->ShipMethod_FullName);
                $receipt->setFob($item_obj->FOB);
                $receipt->setSubtotal($item_obj->Subtotal);
                $receipt->setItemsalestaxListid($item_obj->ItemSalesTax_ListID);
                $receipt->setItemsalestaxFullname($item_obj->ItemSalesTax_FullName);
                $receipt->setSalestaxpercentage($item_obj->SalesTaxPercentage);
                $receipt->setSalestaxtotal($item_obj->SalesTaxTotal);
                $receipt->setTotalamount($item_obj->TotalAmount);
                $receipt->setCurrencyListid($item_obj->Currency_ListID);
                $receipt->setCurrencyFullname($item_obj->Currency_FullName);
                $receipt->setExchangerate($item_obj->ExchangeRate);
                $receipt->setTotalamountinhomecurrency($item_obj->TotalAmountInHomeCurrency);
                $receipt->setMemo($item_obj->Memo);
                $receipt->setCustomermsgListid($item_obj->CustomerMsg_ListID);
                $receipt->setCustomermsgFullname($item_obj->CustomerMsg_FullName);
                $receipt->setIstobeprinted($item_obj->IsToBePrinted);
                $receipt->setIstobeemailed($item_obj->IsToBeEmailed);
                $receipt->setCustomersalestaxcodeListid($item_obj->CustomerSalesTaxCode_ListID);
                $receipt->setCustomersalestaxcodeFullname($item_obj->CustomerSalesTaxCode_FullName);
                $receipt->setDeposittoaccountListid($item_obj->DepositToAccount_ListID);
                $receipt->setDeposittoaccountFullname($item_obj->DepositToAccount_FullName);

                if( $receipt->nonEmpty() ) {
                  $receipt->update();
                } else {
                  $receipt->insert();
                }

                $this->insert_dataext_items($item_obj,true,'SalesReceipt',$item_obj->TxnID);

                $lineItem = new $this->CI->Qb_salesreceipt_salesreceiptline_model();
                $lineItem->setSalesreceiptTxnid($item_obj->TxnID,TRUE);
                $lineItem->delete();

                if( count($item_obj->LineItems) > 0 ) {
                    foreach( $item_obj->LineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;

                        $lineItem = new $this->CI->Qb_salesreceipt_salesreceiptline_model();
                        $lineItem->setSalesreceiptTxnid($item_obj->TxnID,TRUE);
                        $lineItem->setTxnlineid($line_item_obj->TxnLineID,TRUE);
                        $lineItem->setSortorder($line_item_obj->SortOrder);
                        $lineItem->setItemListid($line_item_obj->Item_ListID);
                        $lineItem->setItemFullname($line_item_obj->Item_FullName);
                        $lineItem->setDescrip($line_item_obj->Descrip);
                        $lineItem->setQuantity($line_item_obj->Quantity);
                        $lineItem->setUnitofmeasure($line_item_obj->UnitOfMeasure);
                        $lineItem->setOverrideuomsetListid($line_item_obj->OverrideUOMSet_ListID);
                        $lineItem->setOverrideuomsetFullname($line_item_obj->OverrideUOMSet_FullName);
                        $lineItem->setRate($line_item_obj->Rate);
                        $lineItem->setRatepercent($line_item_obj->RatePercent);
                        $lineItem->setClassListid($line_item_obj->Class_ListID);
                        $lineItem->setClassFullname($line_item_obj->Class_FullName);
                        $lineItem->setAmount($line_item_obj->Amount);
                        $lineItem->setInventorysiteListid($line_item_obj->InventorySite_ListID);
                        $lineItem->setInventorysiteFullname($line_item_obj->InventorySite_FullName);
                        $lineItem->setSerialnumber($line_item_obj->SerialNumber);
                        $lineItem->setLotnumber($line_item_obj->LotNumber);
                        $lineItem->setServicedate($line_item_obj->ServiceDate);
                        $lineItem->setSalestaxcodeListid($line_item_obj->SalesTaxCode_ListID);
                        $lineItem->setSalestaxcodeFullname($line_item_obj->SalesTaxCode_FullName);
                        $lineItem->setOther1($line_item_obj->Other1);
                        $lineItem->setOther2($line_item_obj->Other2);

                        if( $lineItem->nonEmpty() ) {
                          $lineItem->update();
                        } else {
                          $lineItem->insert();
                        }

                        $this->insert_dataext_items($line_item_obj,true,'SalesReceiptLine',$item_obj->TxnID);

                    }
                }

                $lineItem = new $this->CI->Qb_salesreceipt_salesreceiptlinegroup_model();
                $lineItem->setSalesreceiptTxnid($item_obj->TxnID,TRUE);
                $lineItem->delete();

                if( count($item_obj->LineGroupItems) > 0 ) {
                    foreach( $item_obj->LineGroupItems as $line_item ) {

                        $line_item_obj = (object) $line_item;

                        $lineItem->setSortorder($line_item_obj->SortOrder);
                        $lineItem->setSalesreceiptTxnid($item_obj->TxnID,TRUE);
                        $lineItem->setTxnlineid($line_item_obj->TxnLineID,TRUE);
                        $lineItem->setItemgroupListid($line_item_obj->ItemGroup_ListID);
                        $lineItem->setItemgroupFullname($line_item_obj->ItemGroup_FullName);
                        $lineItem->setDescrip($line_item_obj->Descrip);
                        $lineItem->setQuantity($line_item_obj->Quantity);
                        $lineItem->setUnitofmeasure($line_item_obj->UnitOfMeasure);
                        $lineItem->setOverrideuomsetListid($line_item_obj->OverrideUOMSet_ListID);
                        $lineItem->setOverrideuomsetFullname($line_item_obj->OverrideUOMSet_FullName);
                        $lineItem->setIsprintitemsingroup($line_item_obj->IsPrintItemsInGroup);
                        $lineItem->setTotalamount($line_item_obj->TotalAmount);
                        if( $lineItem->nonEmpty() ) {
                          $lineItem->update();
                        } else {
                          $lineItem->insert();
                        }

                        $this->insert_dataext_items($line_item_obj,true,'SalesReceiptLineGroup',$item_obj->TxnID);

                    }
                }


            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'SalesReceiptRet') {

            $SalesReceiptLineRet = $item->getElementsByTagName('SalesReceiptLineRet');
            $LineItems = array();

            if( $SalesReceiptLineRet ) {
                foreach($SalesReceiptLineRet as $line_item) {

                    $LineItems[] = array(
                        'SalesReceipt_TxnID' => $this->get_text_content($item, 'TxnID'), 
                        'SortOrder' => $this->get_text_content($line_item, 'SortOrder'), 
                        'TxnLineID' => $this->get_text_content($line_item, 'TxnLineID'), 
                        'Item_ListID' => $this->get_text_content($line_item, array('ItemRef','ListID')), 
                        'Item_FullName' => $this->get_text_content($line_item, array('ItemRef','FullName')), 
                        'Descrip' => $this->get_text_content($line_item, 'Desc'), 
                        'Quantity' => $this->get_text_content($line_item, 'Quantity'), 
                        'UnitOfMeasure' => $this->get_text_content($line_item, 'UnitOfMeasure'), 
                        'OverrideUOMSet_ListID' => $this->get_text_content($line_item, array('OverrideUOMSetRef','ListID')), 
                        'OverrideUOMSet_FullName' => $this->get_text_content($line_item, array('OverrideUOMSetRef','FullName')), 
                        'Rate' => $this->get_text_content($line_item, 'Rate'), 
                        'RatePercent' => $this->get_text_content($line_item, 'RatePercent'), 
                        'Class_ListID' => $this->get_text_content($line_item, array('ClassRef','ListID')), 
                        'Class_FullName' => $this->get_text_content($line_item, array('ClassRef','FullName')), 
                        'Amount' => $this->get_text_content($line_item, 'Amount'), 
                        'InventorySite_ListID' => $this->get_text_content($line_item, array('InventorySiteRef','ListID')), 
                        'InventorySite_FullName' => $this->get_text_content($line_item, array('InventorySiteRef','FullName')), 
                        'SerialNumber' => $this->get_text_content($line_item, 'SerialNumber'), 
                        'LotNumber' => $this->get_text_content($line_item, 'LotNumber'), 
                        'ServiceDate' => $this->get_text_content($line_item, 'ServiceDate'), 
                        'SalesTaxCode_ListID' => $this->get_text_content($line_item, array('SalesTaxCodeRef','ListID')), 
                        'SalesTaxCode_FullName' => $this->get_text_content($line_item, array('SalesTaxCodeRef','FullName')), 
                        'Other1' => $this->get_text_content($line_item, 'Other1'), 
                        'Other2' => $this->get_text_content($line_item, 'Other2'), 
                        'DataExtItems' => $this->get_dataext_items($line_item, 'SalesReceiptLine', 'TxnLineID'),
                    );
                }
            }

            $SalesReceiptLineGroupRet = $item->getElementsByTagName('SalesReceiptLineGroupRet');
            $LineGroupItems = array();

            if( $SalesReceiptLineGroupRet ) {
                foreach($SalesReceiptLineGroupRet as $line_item) {
                    $LineGroupItems[] = array(
                    'SortOrder' => $this->get_text_content($line_item, 'SortOrder'), 
                    'TxnLineID' => $this->get_text_content($line_item, 'TxnLineID'), 
                    'ItemGroup_ListID' => $this->get_text_content($line_item, array('ItemGroupRef','ListID')), 
                    'ItemGroup_FullName' => $this->get_text_content($line_item, array('ItemGroupRef','FullName')), 
                    'Descrip' => $this->get_text_content($line_item, 'Desc'), 
                    'Quantity' => $this->get_text_content($line_item, 'Quantity'), 
                    'UnitOfMeasure' => $this->get_text_content($line_item, 'UnitOfMeasure'), 
                    'OverrideUOMSet_ListID' => $this->get_text_content($line_item, array('OverrideUOMSetRef','ListID')), 
                    'OverrideUOMSet_FullName' => $this->get_text_content($line_item, array('OverrideUOMSetRef','FullName')), 
                    'IsPrintItemsInGroup' => $this->get_text_content($line_item, 'IsPrintItemsInGroup'), 
                    'TotalAmount' => $this->get_text_content($line_item, 'TotalAmount'), 
                    'DataExtItems' => $this->get_dataext_items($line_item, 'SalesReceiptLineGroup', 'TxnLineID'),
                    );
                }
            }


          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, 'TxnID'), 
            'TimeCreated' => $this->get_text_content($item, 'TimeCreated'), 
            'TimeModified' => $this->get_text_content($item, 'TimeModified'), 
            'EditSequence' => $this->get_text_content($item, 'EditSequence'), 
            'TxnNumber' => $this->get_text_content($item, 'TxnNumber'), 
            'TxnDate' => $this->get_text_content($item, 'TxnDate'), 

            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')), 
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')), 

            'CustomerMsg_ListID' => $this->get_text_content($item, array('CustomerMsgRef','ListID')), 
            'CustomerMsg_FullName' => $this->get_text_content($item, array('CustomerMsgRef','FullName')), 

            'Template_ListID' => $this->get_text_content($item, array('TemplateRef','ListID')), 
            'Template_FullName' => $this->get_text_content($item, array('TemplateRef','FullName')), 

            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')), 
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')), 

            'SalesRep_ListID' => $this->get_text_content($item, array('SalesRepRef','ListID')), 
            'SalesRep_FullName' => $this->get_text_content($item, array('SalesRepRef','FullName')), 

            'RefNumber' => $this->get_text_content($item, 'RefNumber'), 

            'BillAddress_Addr1' => $this->get_text_content($item, array('BillAddress','Addr1')), 
            'BillAddress_Addr2' => $this->get_text_content($item, array('BillAddress','Addr2')), 
            'BillAddress_Addr3' => $this->get_text_content($item, array('BillAddress','Addr3')), 
            'BillAddress_Addr4' => $this->get_text_content($item, array('BillAddress','Addr4')), 
            'BillAddress_Addr5' => $this->get_text_content($item, array('BillAddress','Addr5')), 
            'BillAddress_City' => $this->get_text_content($item, array('BillAddress','City')), 
            'BillAddress_State' => $this->get_text_content($item, array('BillAddress','State')), 
            'BillAddress_PostalCode' => $this->get_text_content($item, array('BillAddress','PostalCode')), 
            'BillAddress_Country' => $this->get_text_content($item, array('BillAddress','Country')), 
            'BillAddress_Note' => $this->get_text_content($item, array('BillAddress','Note')), 

            'BillAddressBlock_Addr1' => $this->get_text_content($item, array('BillAddressBlock','Addr1')), 
            'BillAddressBlock_Addr2' => $this->get_text_content($item, array('BillAddressBlock','Addr2')), 
            'BillAddressBlock_Addr3' => $this->get_text_content($item, array('BillAddressBlock','Addr3')), 
            'BillAddressBlock_Addr4' => $this->get_text_content($item, array('BillAddressBlock','Addr4')), 
            'BillAddressBlock_Addr5' => $this->get_text_content($item, array('BillAddressBlock','Addr5')), 

            'ShipAddress_Addr1' => $this->get_text_content($item, array('ShipAddress','Addr1')), 
            'ShipAddress_Addr2' => $this->get_text_content($item, array('ShipAddress','Addr2')), 
            'ShipAddress_Addr3' => $this->get_text_content($item, array('ShipAddress','Addr3')), 
            'ShipAddress_Addr4' => $this->get_text_content($item, array('ShipAddress','Addr4')), 
            'ShipAddress_Addr5' => $this->get_text_content($item, array('ShipAddress','Addr5')), 
            'ShipAddress_City' => $this->get_text_content($item, array('ShipAddress','City')), 
            'ShipAddress_State' => $this->get_text_content($item, array('ShipAddress','State')), 
            'ShipAddress_PostalCode' => $this->get_text_content($item, array('ShipAddress','PostalCode')), 
            'ShipAddress_Country' => $this->get_text_content($item, array('ShipAddress','Country')), 
            'ShipAddress_Note' => $this->get_text_content($item, array('ShipAddress','Note')), 

            'ShipAddressBlock_Addr1' => $this->get_text_content($item, array('ShipAddressBlock','Addr1')), 
            'ShipAddressBlock_Addr2' => $this->get_text_content($item, array('ShipAddressBlock','Addr2')), 
            'ShipAddressBlock_Addr3' => $this->get_text_content($item, array('ShipAddressBlock','Addr3')), 
            'ShipAddressBlock_Addr4' => $this->get_text_content($item, array('ShipAddressBlock','Addr4')), 
            'ShipAddressBlock_Addr5' => $this->get_text_content($item, array('ShipAddressBlock','Addr5')), 

            'IsPending' => $this->get_text_content($item, 'IsPending'), 
            'CheckNumber' => $this->get_text_content($item, 'CheckNumber'), 
            'FOB' => $this->get_text_content($item, 'FOB'), 
            'Memo' => $this->get_text_content($item, 'Memo'), 

            'PaymentMethod_ListID' => $this->get_text_content($item, array('PaymentMethodRef','ListID')), 
            'PaymentMethod_FullName' => $this->get_text_content($item, array('PaymentMethodRef','FullName')), 
            
            'ShipMethod_ListID' => $this->get_text_content($item, array('ShipMethodRef','ListID')), 
            'ShipMethod_FullName' => $this->get_text_content($item, array('ShipMethodRef','FullName')), 
            
            'ItemSalesTax_ListID' => $this->get_text_content($item, array('ItemSalesTaxRef','ListID')), 
            'ItemSalesTax_FullName' => $this->get_text_content($item, array('ItemSalesTaxRef','FullName')), 

            'DueDate' => $this->get_text_content($item, 'DueDate'), 
            'ShipDate' => $this->get_text_content($item, 'ShipDate'), 
            'Subtotal' => $this->get_text_content($item, 'Subtotal'), 
            'SalesTaxPercentage' => $this->get_text_content($item, 'SalesTaxPercentage'), 
            'SalesTaxTotal' => $this->get_text_content($item, 'SalesTaxTotal'), 
            'TotalAmount' => $this->get_text_content($item, 'TotalAmount'), 

            'Currency_ListID' => $this->get_text_content($item, array('CurrencyRef','ListID')), 
            'Currency_FullName' => $this->get_text_content($item, array('CurrencyRef','FullName')), 

            'CustomerSalesTaxCode_ListID' => $this->get_text_content($item, array('CustomerSalesTaxCodeRef','ListID')), 
            'CustomerSalesTaxCode_FullName' => $this->get_text_content($item, array('CustomerSalesTaxCodeRef','FullName')), 

            'ExchangeRate' => $this->get_text_content($item, 'ExchangeRate'), 
            'TotalAmountInHomeCurrency' => $this->get_text_content($item, 'TotalAmountInHomeCurrency'), 
            'IsToBePrinted' => $this->get_text_content($item, 'IsToBePrinted'), 
            'IsToBeEmailed' => $this->get_text_content($item, 'IsToBeEmailed'), 

            'DepositToAccount_ListID' => $this->get_text_content($item, array('DepositToAccountRef','ListID')), 
            'DepositToAccount_FullName' => $this->get_text_content($item, array('DepositToAccountRef','FullName')), 

            'LineItems' => $LineItems,
            'LineGroupItems' => $LineGroupItems,
            'DataExtItems' => $this->get_dataext_items($item, 'SalesReceipt', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_salesreceipt_model');
        $query = new $this->CI->Qb_salesreceipt_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file Global_variables.php */