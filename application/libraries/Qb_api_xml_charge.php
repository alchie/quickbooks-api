<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_charge extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_charge_model');
        $model = new $this->CI->Qb_charge_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_CHARGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CHARGEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ChargeQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ChargeQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>Charge</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'ChargeRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_charge_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_charge_model();
                $model->setIspaid($item_obj->IsPaid);
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setItemListid($item_obj->Item_ListID);
                $model->setItemFullname($item_obj->Item_FullName);
                $model->setQuantity($item_obj->Quantity);
                $model->setRate($item_obj->Rate);
                $model->setAmount($item_obj->Amount);
                $model->setBalanceremaining($item_obj->BalanceRemaining);
                $model->setDescrip($item_obj->Descrip);
                $model->setAraccountListid($item_obj->ARAccount_ListID);
                $model->setAraccountFullname($item_obj->ARAccount_FullName);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setBilleddate($item_obj->BilledDate);
                $model->setDuedate($item_obj->DueDate);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ChargeRet') {

          $this->items[] = array(
            'IsPaid' => $this->get_text_content($item, array('IsPaid')),
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'Item_ListID' => $this->get_text_content($item, array('ItemRef','ListID')),
            'Item_FullName' => $this->get_text_content($item, array('ItemRef','FullName')),
            'Quantity' => $this->get_text_content($item, array('Quantity')),
            'Rate' => $this->get_text_content($item, array('Rate')),
            'Amount' => $this->get_text_content($item, array('Amount')),
            'BalanceRemaining' => $this->get_text_content($item, array('BalanceRemaining')),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'ARAccount_ListID' => $this->get_text_content($item, array('ARAccountRef','ListID')),
            'ARAccount_FullName' => $this->get_text_content($item, array('ARAccountRef','FullName')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'BilledDate' => $this->get_text_content($item, array('BilledDate')),
            'DueDate' => $this->get_text_content($item, array('DueDate')),
            'DataExtItems' => $this->get_dataext_items($item, 'Charge', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_charge_model');
        $query = new $this->CI->Qb_charge_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file */
