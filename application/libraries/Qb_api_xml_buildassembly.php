<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_buildassembly extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_buildassembly_model');
        $model = new $this->CI->Qb_buildassembly_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_BUILDASSEMBLYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BUILDASSEMBLYQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified  . " +1 second") ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<BuildAssemblyQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</BuildAssemblyQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>BuildAssembly</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'BillRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_buildassembly_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_buildassembly_model();
                $model->setTxnid($item_obj->TxnID,true);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setIteminventoryassemblyListid($item_obj->ItemInventoryAssembly_ListID);
                $model->setIteminventoryassemblyFullname($item_obj->ItemInventoryAssembly_FullName);
                $model->setTxndate($item_obj->TxnDate);
                $model->setRefnumber($item_obj->RefNumber);
                $model->setMemo($item_obj->Memo);
                $model->setIspending($item_obj->IsPending);
                $model->setQuantitytobuild($item_obj->QuantityToBuild);
                $model->setQuantitycanbuild($item_obj->QuantityCanBuild);
                $model->setQuantityonhand($item_obj->QuantityOnHand);
                $model->setQuantityonsalesorder($item_obj->QuantityOnSalesOrder);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items( $item_obj );

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'BuildAssemblyRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'ItemInventoryAssembly_ListID' => $this->get_text_content($item, array('ItemInventoryAssemblyRef','ListID')),
            'ItemInventoryAssembly_FullName' => $this->get_text_content($item, array('ItemInventoryAssemblyRef','FullName')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'RefNumber' => $this->get_text_content($item, array('RefNumber')),
            'Memo' => $this->get_text_content($item, array('Memo')),
            'IsPending' => $this->get_text_content($item, array('IsPending')),
            'QuantityToBuild' => $this->get_text_content($item, array('QuantityToBuild')),
            'QuantityCanBuild' => $this->get_text_content($item, array('QuantityCanBuild')),
            'QuantityOnHand' => $this->get_text_content($item, array('QuantityOnHand')),
            'QuantityOnSalesOrder' => $this->get_text_content($item, array('QuantityOnSalesOrder')),
            'DataExtItems' => $this->get_dataext_items($item, 'BuildAssembly', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_buildassembly_model');
        $query = new $this->CI->Qb_buildassembly_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }

}

/* End of file */
