<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_timetracking extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_timetracking_model');
        $model = new $this->CI->Qb_timetracking_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_TIMETRACKINGQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_TIMETRACKINGQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<TimeTrackingQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'</TimeTrackingQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>TimeTracking</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'TimeTrackingRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_timetracking_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_timetracking_model();
                $model->setTxnid($item_obj->TxnID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setTxnnumber($item_obj->TxnNumber);
                $model->setTxndate($item_obj->TxnDate);
                $model->setEntityListid($item_obj->Entity_ListID);
                $model->setEntityFullname($item_obj->Entity_FullName);
                $model->setCustomerListid($item_obj->Customer_ListID);
                $model->setCustomerFullname($item_obj->Customer_FullName);
                $model->setItemserviceListid($item_obj->ItemService_ListID);
                $model->setItemserviceFullname($item_obj->ItemService_FullName);
                $model->setDuration($item_obj->Duration);
                $model->setClassListid($item_obj->Class_ListID);
                $model->setClassFullname($item_obj->Class_FullName);
                $model->setPayrollitemwageListid($item_obj->PayrollItemWage_ListID);
                $model->setPayrollitemwageFullname($item_obj->PayrollItemWage_FullName);
                $model->setNotes($item_obj->Notes);
                $model->setBillablestatus($item_obj->BillableStatus);
                $model->setIsbillable($item_obj->IsBillable);
                $model->setIsbilled($item_obj->IsBilled);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);
            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'TimeTrackingRet') {

          $this->items[] = array(
            'TxnID' => $this->get_text_content($item, array('TxnID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'TxnNumber' => $this->get_text_content($item, array('TxnNumber')),
            'TxnDate' => $this->get_text_content($item, array('TxnDate')),
            'Entity_ListID' => $this->get_text_content($item, array('EntityRef','ListID')),
            'Entity_FullName' => $this->get_text_content($item, array('EntityRef','FullName')),
            'Customer_ListID' => $this->get_text_content($item, array('CustomerRef','ListID')),
            'Customer_FullName' => $this->get_text_content($item, array('CustomerRef','FullName')),
            'ItemService_ListID' => $this->get_text_content($item, array('ItemServiceRef','ListID')),
            'ItemService_FullName' => $this->get_text_content($item, array('ItemServiceRef','FullName')),
            'Duration' => $this->get_text_content($item, array('Duration')),
            'Class_ListID' => $this->get_text_content($item, array('ClassRef','ListID')),
            'Class_FullName' => $this->get_text_content($item, array('ClassRef','FullName')),
            'PayrollItemWage_ListID' => $this->get_text_content($item, array('PayrollItemWageRef','ListID')),
            'PayrollItemWage_FullName' => $this->get_text_content($item, array('PayrollItemWageRef','FullName')),
            'Notes' => $this->get_text_content($item, array('Notes')),
            'BillableStatus' => $this->get_text_content($item, array('BillableStatus')),
            'IsBillable' => $this->get_text_content($item, array('IsBillable')),
            'IsBilled' => $this->get_text_content($item, array('IsBilled')),
            'DataExtItems' => $this->get_dataext_items($item, 'TimeTracking', 'TxnID'),
          );

        }
      }
    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_timetracking_model');
        $query = new $this->CI->Qb_timetracking_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
    
}

/* End of file */
