<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_parser {
    
    protected $CI;
    protected $xml;
    protected $objects;
    protected $currentNode;

    public function __construct($xml=null)
    {
       $this->CI =& get_instance();
       if( $xml ) {
       		$this->setXML($xml);
       		$this->parse();
       }
       return $this;
    }

    public function setXML($xml) {
      $xmlString = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $xml);
    	$this->xml = $xmlString;
      return $this;
    }

    public function parse() {
    	if( $this->xml ) {
    		$this->objects = new DOMDocument();
    		$this->objects->loadXML( $this->xml );
    	}
      return $this;
    }

    public function getObjects() {
    	return $this->objects;
    }

    public function setTagName($name) {
      if( $this->objects ) {
        $this->currentNode = $this->objects->getElementsByTagName($name);
      }
      return $this;
    }

    public function getByTagName($name) {
      if( $this->objects ) {
        $this->currentNode = $this->objects->getElementsByTagName($name);
      }
      return $this->currentNode;
    }

    public function getAll() {
      return $this->currentNode;
    }

    public function get($index=0) {
      if( ($this->currentNode) && (count($this->currentNode) > 0) ) {
        return $this->currentNode->item($index);
      }
      return false;
    }

    public function length() {
      if( $this->currentNode ) {
        return $this->currentNode->length;
      } else {
        return 0;
      }
      
    }

    public function getValue($index=0) {
      if( ($this->currentNode) &&  (count($this->currentNode) > 0) ) {
        return $this->currentNode->item($index)->nodeValue;
      } else {
        return false;
      }
    }

}

/* End of file Global_variables.php */