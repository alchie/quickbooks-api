<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_billingrate extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_billingrate_model');
        $model = new $this->CI->Qb_billingrate_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_BILLINGRATEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLINGRATEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<BillingRateQueryRq requestID="'.$requestID.'">' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</BillingRateQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>BillingRate</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'BillingrateRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_billingrate_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_billingrate_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setBillingratetype($item_obj->BillingRateType);
                $model->setFixedbillingrate($item_obj->FixedBillingRate);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'BillingRateRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'BillingRateType' => $this->get_text_content($item, array('BillingRateType')),
            'FixedBillingRate' => $this->get_text_content($item, array('FixedBillingRate')),

          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_billingrate_model');
        $query = new $this->CI->Qb_billingrate_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
 
}

/* End of file */
