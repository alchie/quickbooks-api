<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_customer extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_customer_model');
        $customers = new $this->CI->Qb_customer_model();
        $customers->set_order('TimeModified', 'DESC');
        $customers_data = $customers->get();
        return (($customers_data) && isset($customers_data->TimeModified)) ? $customers_data->TimeModified : false;
    }

    public function request($queue) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_CUSTOMERQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CUSTOMERQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<CustomerQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</CustomerQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>Customer</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        if( $this->items ) {

            
            $this->CI->load->model('Qb_customer_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;
                
                $customers = new $this->CI->Qb_customer_model();
                $customers->setListid($item_obj->ListID,true);
                $customers->setTimecreated($item_obj->TimeCreated);
                $customers->setTimemodified($item_obj->TimeModified);
                $customers->setEditsequence($item_obj->EditSequence);
                $customers->setName($item_obj->Name);
                $customers->setFullname($item_obj->FullName);
                $customers->setIsactive(($item_obj->IsActive)?1:0);
                $customers->setParentListid($item_obj->Parent_ListID);
                $customers->setParentFullname($item_obj->Parent_FullName);
                $customers->setSublevel($item_obj->Sublevel);
                $customers->setCompanyname($item_obj->CompanyName);
                $customers->setSalutation($item_obj->Salutation);
                $customers->setFirstname($item_obj->FirstName);
                $customers->setMiddlename($item_obj->MiddleName);
                $customers->setLastname($item_obj->LastName);
                $customers->setBilladdressAddr1($item_obj->BillAddress_Addr1);
                $customers->setBilladdressAddr2($item_obj->BillAddress_Addr2);
                $customers->setBilladdressAddr3($item_obj->BillAddress_Addr3);
                $customers->setBilladdressAddr4($item_obj->BillAddress_Addr4);
                $customers->setBilladdressAddr5($item_obj->BillAddress_Addr5);
                $customers->setBilladdressCity($item_obj->BillAddress_City);
                $customers->setBilladdressState($item_obj->BillAddress_State);
                $customers->setBilladdressPostalcode($item_obj->BillAddress_PostalCode);
                $customers->setBilladdressCountry($item_obj->BillAddress_Country);
                $customers->setBilladdressNote($item_obj->BillAddress_Note);
                $customers->setBilladdressblockAddr1($item_obj->BillAddressBlock_Addr1);
                $customers->setBilladdressblockAddr2($item_obj->BillAddressBlock_Addr2);
                $customers->setBilladdressblockAddr3($item_obj->BillAddressBlock_Addr3);
                $customers->setBilladdressblockAddr4($item_obj->BillAddressBlock_Addr4);
                $customers->setBilladdressblockAddr5($item_obj->BillAddressBlock_Addr5);
                $customers->setShipaddressAddr1($item_obj->ShipAddress_Addr1);
                $customers->setShipaddressAddr2($item_obj->ShipAddress_Addr2);
                $customers->setShipaddressAddr3($item_obj->ShipAddress_Addr3);
                $customers->setShipaddressAddr4($item_obj->ShipAddress_Addr4);
                $customers->setShipaddressAddr5($item_obj->ShipAddress_Addr5);
                $customers->setShipaddressCity($item_obj->ShipAddress_City);
                $customers->setShipaddressState($item_obj->ShipAddress_State);
                $customers->setShipaddressPostalcode($item_obj->ShipAddress_PostalCode);
                $customers->setShipaddressCountry($item_obj->ShipAddress_Country);
                $customers->setShipaddressNote($item_obj->ShipAddress_Note);
                $customers->setShipaddressblockAddr1($item_obj->ShipAddressBlock_Addr1);
                $customers->setShipaddressblockAddr2($item_obj->ShipAddressBlock_Addr2);
                $customers->setShipaddressblockAddr3($item_obj->ShipAddressBlock_Addr3);
                $customers->setShipaddressblockAddr4($item_obj->ShipAddressBlock_Addr4);
                $customers->setShipaddressblockAddr5($item_obj->ShipAddressBlock_Addr5);
                $customers->setPhone($item_obj->Phone);
                $customers->setAltphone($item_obj->AltPhone);
                $customers->setFax($item_obj->Fax);
                $customers->setEmail($item_obj->Email);
                $customers->setAltemail($item_obj->AltEmail);
                $customers->setContact($item_obj->Contact);
                $customers->setAltcontact($item_obj->AltContact);
                $customers->setCustomertypeListid($item_obj->CustomerType_ListID);
                $customers->setCustomertypeFullname($item_obj->CustomerType_FullName);
                $customers->setTermsListid($item_obj->Terms_ListID);
                $customers->setTermsFullname($item_obj->Terms_FullName);
                $customers->setSalesrepListid($item_obj->SalesRep_ListID);
                $customers->setSalesrepFullname($item_obj->SalesRep_FullName);
                $customers->setBalance($item_obj->Balance);
                $customers->setTotalbalance($item_obj->TotalBalance);
                $customers->setSalestaxcodeListid($item_obj->SalesTaxCode_ListID);
                $customers->setSalestaxcodeFullname($item_obj->SalesTaxCode_FullName);
                $customers->setItemsalestaxListid($item_obj->ItemSalesTax_ListID);
                $customers->setItemsalestaxFullname($item_obj->ItemSalesTax_FullName);
                $customers->setResalenumber($item_obj->ResaleNumber);
                $customers->setAccountnumber($item_obj->AccountNumber);
                $customers->setCreditlimit($item_obj->CreditLimit);
                $customers->setPreferredpaymentmethodListid($item_obj->PreferredPaymentMethod_ListID);
                $customers->setPreferredpaymentmethodFullname($item_obj->PreferredPaymentMethod_FullName);
                $customers->setCreditcardinfoCreditcardnumber($item_obj->CreditCardInfo_CreditCardNumber);
                $customers->setCreditcardinfoExpirationmonth($item_obj->CreditCardInfo_ExpirationMonth);
                $customers->setCreditcardinfoExpirationyear($item_obj->CreditCardInfo_ExpirationYear);
                $customers->setCreditcardinfoNameoncard($item_obj->CreditCardInfo_NameOnCard);
                $customers->setCreditcardinfoCreditcardaddress($item_obj->CreditCardInfo_CreditCardAddress);
                $customers->setCreditcardinfoCreditcardpostalcode($item_obj->CreditCardInfo_CreditCardPostalCode);
                $customers->setJobstatus($item_obj->JobStatus);
                $customers->setJobstartdate($item_obj->JobStartDate);
                $customers->setJobprojectedenddate($item_obj->JobProjectedEndDate);
                $customers->setJobenddate($item_obj->JobEndDate);
                $customers->setJobdesc($item_obj->JobDesc);
                $customers->setJobtypeListid($item_obj->JobType_ListID);
                $customers->setJobtypeFullname($item_obj->JobType_FullName);
                $customers->setNotes($item_obj->Notes);
                $customers->setPricelevelListid($item_obj->PriceLevel_ListID);
                $customers->setPricelevelFullname($item_obj->PriceLevel_FullName);
                
                if( $customers->nonEmpty() ) {
                  $customers->update();
                } else {
                  $customers->insert();
                }

                $this->insert_dataext_items($item_obj);

            }

        }

    }

    protected function _populate_items() {

      foreach($this->data as $item) {
        if( $item->nodeName == 'CustomerRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, 'ListID'), 
            'TimeCreated' => $this->get_text_content($item, 'TimeCreated'), 
            'TimeModified' => $this->get_text_content($item, 'TimeModified'), 
            'EditSequence' => $this->get_text_content($item, 'EditSequence'), 
            'Name' => $this->get_text_content($item, 'Name'), 
            'FullName' => $this->get_text_content($item, 'FullName'), 
            'IsActive' => $this->get_text_content($item, 'IsActive'), 
            'Parent_ListID' => $this->get_text_content($item, array('ParentRef','ListID')), 
            'Parent_FullName' => $this->get_text_content($item, array('ParentRef','FullName')), 
            'Sublevel' => $this->get_text_content($item, 'Sublevel'), 
            'CompanyName' => $this->get_text_content($item, 'CompanyName'), 
            'Salutation' => $this->get_text_content($item, 'Salutation'), 
            'FirstName' => $this->get_text_content($item, 'FirstName'), 
            'MiddleName' => $this->get_text_content($item, 'MiddleName'), 
            'LastName' => $this->get_text_content($item, 'LastName'), 
            'BillAddress_Addr1' => $this->get_text_content($item, array('BillAddress','Addr1')), 
            'BillAddress_Addr2' => $this->get_text_content($item, array('BillAddress','Addr2')), 
            'BillAddress_Addr3' => $this->get_text_content($item, array('BillAddress','Addr3')), 
            'BillAddress_Addr4' => $this->get_text_content($item, array('BillAddress','Addr4')), 
            'BillAddress_Addr5' => $this->get_text_content($item, array('BillAddress','Addr5')), 
            'BillAddress_City' => $this->get_text_content($item, array('BillAddress','City')), 
            'BillAddress_State' => $this->get_text_content($item, array('BillAddress','State')), 
            'BillAddress_PostalCode' => $this->get_text_content($item, array('BillAddress','PostalCode')), 
            'BillAddress_Country' => $this->get_text_content($item, array('BillAddress','Country')), 
            'BillAddress_Note' => $this->get_text_content($item, array('BillAddress','Note')), 
            'BillAddressBlock_Addr1' => $this->get_text_content($item, array('BillAddressBlock','Addr1')), 
            'BillAddressBlock_Addr2' => $this->get_text_content($item, array('BillAddressBlock','Addr2')), 
            'BillAddressBlock_Addr3' => $this->get_text_content($item, array('BillAddressBlock','Addr3')), 
            'BillAddressBlock_Addr4' => $this->get_text_content($item, array('BillAddressBlock','Addr4')), 
            'BillAddressBlock_Addr5' => $this->get_text_content($item, array('BillAddressBlock','Addr5')), 
            'ShipAddress_Addr1' => $this->get_text_content($item, array('ShipAddress','Addr1')), 
            'ShipAddress_Addr2' => $this->get_text_content($item, array('ShipAddress','Addr2')), 
            'ShipAddress_Addr3' => $this->get_text_content($item, array('ShipAddress','Addr3')), 
            'ShipAddress_Addr4' => $this->get_text_content($item, array('ShipAddress','Addr4')), 
            'ShipAddress_Addr5' => $this->get_text_content($item, array('ShipAddress','Addr5')), 
            'ShipAddress_City' => $this->get_text_content($item, array('ShipAddress','City')), 
            'ShipAddress_State' => $this->get_text_content($item, array('ShipAddress','State')), 
            'ShipAddress_PostalCode' => $this->get_text_content($item, array('ShipAddress','PostalCode')), 
            'ShipAddress_Country' => $this->get_text_content($item, array('ShipAddress','Country')), 
            'ShipAddress_Note' => $this->get_text_content($item, array('ShipAddress','Note')), 
            'ShipAddressBlock_Addr1' => $this->get_text_content($item, array('ShipAddressBlock','Addr1')), 
            'ShipAddressBlock_Addr2' => $this->get_text_content($item, array('ShipAddressBlock','Addr2')), 
            'ShipAddressBlock_Addr3' => $this->get_text_content($item, array('ShipAddressBlock','Addr3')), 
            'ShipAddressBlock_Addr4' => $this->get_text_content($item, array('ShipAddressBlock','Addr4')), 
            'ShipAddressBlock_Addr5' => $this->get_text_content($item, array('ShipAddressBlock','Addr5')), 
            'Phone' => $this->get_text_content($item, 'Phone'), 
            'AltPhone' => $this->get_text_content($item, 'AltPhone'), 
            'Fax' => $this->get_text_content($item, 'Fax'), 
            'Email' => $this->get_text_content($item, 'Email'), 
            'AltEmail' => $this->get_text_content($item, 'AltEmail'), 
            'Contact' => $this->get_text_content($item, 'Contact'), 
            'AltContact' => $this->get_text_content($item, 'AltContact'), 
            'CustomerType_ListID' => $this->get_text_content($item, array('CustomerTypeRef','ListID')), 
            'CustomerType_FullName' => $this->get_text_content($item, array('CustomerTypeRef','FullName')), 
            'Terms_ListID' => $this->get_text_content($item, array('TermsRef','ListID')), 
            'Terms_FullName' => $this->get_text_content($item, array('TermsRef','FullName')), 
            'SalesRep_ListID' => $this->get_text_content($item, array('SalesRepRef','ListID')), 
            'SalesRep_FullName' => $this->get_text_content($item, array('SalesRepRef','FullName')), 
            'Balance' => $this->get_text_content($item, 'Balance'), 
            'TotalBalance' => $this->get_text_content($item, 'TotalBalance'), 
            'SalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxCodeRef','ListID')), 
            'SalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxCodeRef','FullName')), 
            'ItemSalesTax_ListID' => $this->get_text_content($item, array('ItemSalesTaxRef','ListID')), 
            'ItemSalesTax_FullName' => $this->get_text_content($item, array('ItemSalesTaxRef','FullName')), 
            'ResaleNumber' => $this->get_text_content($item, 'ResaleNumber'), 
            'AccountNumber' => $this->get_text_content($item, 'AccountNumber'), 
            'CreditLimit' => $this->get_text_content($item, 'CreditLimit'), 
            'PreferredPaymentMethod_ListID' => $this->get_text_content($item, array('PreferredPaymentMethodRef','ListID')), 
            'PreferredPaymentMethod_FullName' => $this->get_text_content($item, array('PreferredPaymentMethodRef','FullName')), 
            'CreditCardInfo_CreditCardNumber' => $this->get_text_content($item, array('CreditCardInfoRef','CreditCardNumber')), 
            'CreditCardInfo_ExpirationMonth' => $this->get_text_content($item, array('CreditCardInfoRef','ExpirationMonth')), 
            'CreditCardInfo_ExpirationYear' => $this->get_text_content($item, array('CreditCardInfoRef','ExpirationYear')), 
            'CreditCardInfo_NameOnCard' => $this->get_text_content($item, array('CreditCardInfoRef','NameOnCard')), 
            'CreditCardInfo_CreditCardAddress' => $this->get_text_content($item, array('CreditCardInfoRef','CreditCardAddress')), 
            'CreditCardInfo_CreditCardPostalCode' => $this->get_text_content($item, array('CreditCardInfoRef','CreditCardPostalCode')), 
            'JobStatus' => $this->get_text_content($item, 'JobStatus'), 
            'JobStartDate' => $this->get_text_content($item, 'JobStartDate'), 
            'JobProjectedEndDate' => $this->get_text_content($item, 'JobProjectedEndDate'), 
            'JobEndDate' => $this->get_text_content($item, 'JobEndDate'), 
            'JobDesc' => $this->get_text_content($item, 'JobDesc'), 
            'JobType_ListID' => $this->get_text_content($item, array('JobTypeRef','ListID')), 
            'JobType_FullName' => $this->get_text_content($item, array('JobTypeRef','FullName')), 
            'Notes' => $this->get_text_content($item, 'Notes'), 
            'PriceLevel_ListID' => $this->get_text_content($item, array('PriceLevelRef','ListID')), 
            'PriceLevel_FullName' => $this->get_text_content($item, array('PriceLevelRef','FullName')), 
            'DataExtItems' => $this->get_dataext_items($item, 'Customer', 'ListID'),
          );

        }
      }

    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_customer_model');
        $customers = new $this->CI->Qb_customer_model();
        $customers->setListid($ListID,true);
        $customers->delete();
    }
 
}

/* End of file Global_variables.php */