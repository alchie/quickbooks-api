<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_workerscompcode extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_workerscompcode_model');
        $model = new $this->CI->Qb_workerscompcode_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $maxresults = ($this->CI->config->item('QB_API_WORKERSCOMPCODEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_WORKERSCOMPCODEQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<WorkersCompCodeQueryRq requestID="'.$requestID.'" >' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</WorkersCompCodeQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'WorkerscompcodeRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_workerscompcode_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_workerscompcode_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setIsactive(($item_obj->IsActive)?1:0);
                $model->setDescrip($item_obj->Descrip);
                $model->setCurrentrate($item_obj->CurrentRate);
                $model->setCurrenteffectivedate($item_obj->CurrentEffectiveDate);
                $model->setNextrate($item_obj->NextRate);
                $model->setNexteffectivedate($item_obj->NextEffectiveDate);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'WorkersCompCodeRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'Descrip' => $this->get_text_content($item, array('Descrip')),
            'CurrentRate' => $this->get_text_content($item, array('CurrentRate')),
            'CurrentEffectiveDate' => $this->get_text_content($item, array('CurrentEffectiveDate')),
            'NextRate' => $this->get_text_content($item, array('NextRate')),
            'NextEffectiveDate' => $this->get_text_content($item, array('NextEffectiveDate')),

            'DataExtItems' => $this->get_dataext_items($item, 'WorkersCompCode', 'ListID'),

          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_workerscompcode_model');
        $query = new $this->CI->Qb_workerscompcode_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file */
