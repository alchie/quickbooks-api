<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_itemnoninventory extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_itemnoninventory_model');
        $model = new $this->CI->Qb_itemnoninventory_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_ITEMNONINVENTORYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMNONINVENTORYQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ItemNonInventoryQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ItemNonInventoryQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>ItemNonInventory</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'ItemnoninventoryRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_itemnoninventory_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_itemnoninventory_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setFullname($item_obj->FullName);
                $model->setIsactive(($item_obj->IsActive=='true')?1:0);
                $model->setParentListid($item_obj->Parent_ListID);
                $model->setParentFullname($item_obj->Parent_FullName);
                $model->setSublevel($item_obj->Sublevel);
                $model->setManufacturerpartnumber($item_obj->ManufacturerPartNumber);
                $model->setUnitofmeasuresetListid($item_obj->UnitOfMeasureSet_ListID);
                $model->setUnitofmeasuresetFullname($item_obj->UnitOfMeasureSet_FullName);
                $model->setSalestaxcodeListid($item_obj->SalesTaxCode_ListID);
                $model->setSalestaxcodeFullname($item_obj->SalesTaxCode_FullName);
                $model->setSalesorpurchaseDesc($item_obj->SalesOrPurchase_Desc);
                $model->setSalesorpurchasePrice($item_obj->SalesOrPurchase_Price);
                $model->setSalesorpurchasePricepercent($item_obj->SalesOrPurchase_PricePercent);
                $model->setSalesorpurchaseAccountListid($item_obj->SalesOrPurchase_Account_ListID);
                $model->setSalesorpurchaseAccountFullname($item_obj->SalesOrPurchase_Account_FullName);
                $model->setSalesandpurchaseSalesdesc($item_obj->SalesAndPurchase_SalesDesc);
                $model->setSalesandpurchaseSalesprice($item_obj->SalesAndPurchase_SalesPrice);
                $model->setSalesandpurchaseIncomeaccountListid($item_obj->SalesAndPurchase_IncomeAccount_ListID);
                $model->setSalesandpurchaseIncomeaccountFullname($item_obj->SalesAndPurchase_IncomeAccount_FullName);
                $model->setSalesandpurchasePurchasedesc($item_obj->SalesAndPurchase_PurchaseDesc);
                $model->setSalesandpurchasePurchasecost($item_obj->SalesAndPurchase_PurchaseCost);
                $model->setSalesandpurchaseExpenseaccountListid($item_obj->SalesAndPurchase_ExpenseAccount_ListID);
                $model->setSalesandpurchaseExpenseaccountFullname($item_obj->SalesAndPurchase_ExpenseAccount_FullName);
                $model->setSalesandpurchasePrefvendorListid($item_obj->SalesAndPurchase_PrefVendor_ListID);
                $model->setSalesandpurchasePrefvendorFullname($item_obj->SalesAndPurchase_PrefVendor_FullName);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ItemNonInventoryRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'FullName' => $this->get_text_content($item, array('FullName')),
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'Parent_ListID' => $this->get_text_content($item, array('ParentRef','ListID')),
            'Parent_FullName' => $this->get_text_content($item, array('ParentRef','FullName')),
            'Sublevel' => $this->get_text_content($item, array('Sublevel')),
            'ManufacturerPartNumber' => $this->get_text_content($item, array('ManufacturerPartNumber')),
            'UnitOfMeasureSet_ListID' => $this->get_text_content($item, array('UnitOfMeasureSetRef','ListID')),
            'UnitOfMeasureSet_FullName' => $this->get_text_content($item, array('UnitOfMeasureSetRef','FullName')),
            'SalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxCodeRef','ListID')),
            'SalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxCodeRef','FullName')),
            'SalesOrPurchase_Desc' => $this->get_text_content($item, array('SalesOrPurchaseRef','Desc')),
            'SalesOrPurchase_Price' => $this->get_text_content($item, array('SalesOrPurchaseRef','Price')),
            'SalesOrPurchase_PricePercent' => $this->get_text_content($item, array('SalesOrPurchaseRef','PricePercent')),
            'SalesOrPurchase_Account_ListID' => $this->get_text_content($item, array('SalesOrPurchaseRef','AccountRef','ListID')),
            'SalesOrPurchase_Account_FullName' => $this->get_text_content($item, array('SalesOrPurchaseRef','AccountRef','FullName')),
            'SalesAndPurchase_SalesDesc' => $this->get_text_content($item, array('SalesAndPurchaseRef','SalesDesc')),
            'SalesAndPurchase_SalesPrice' => $this->get_text_content($item, array('SalesAndPurchaseRef','SalesPrice')),
            'SalesAndPurchase_IncomeAccount_ListID' => $this->get_text_content($item, array('SalesAndPurchaseRef','IncomeAccountRef','ListID')),
            'SalesAndPurchase_IncomeAccount_FullName' => $this->get_text_content($item, array('SalesAndPurchaseRef','IncomeAccountRef','FullName')),
            'SalesAndPurchase_PurchaseDesc' => $this->get_text_content($item, array('SalesAndPurchaseRef','PurchaseDesc')),
            'SalesAndPurchase_PurchaseCost' => $this->get_text_content($item, array('SalesAndPurchaseRef','PurchaseCost')),
            'SalesAndPurchase_ExpenseAccount_ListID' => $this->get_text_content($item, array('SalesAndPurchaseRef','ExpenseAccountRef','ListID')),
            'SalesAndPurchase_ExpenseAccount_FullName' => $this->get_text_content($item, array('SalesAndPurchaseRef','ExpenseAccountRef','FullName')),
            'SalesAndPurchase_PrefVendor_ListID' => $this->get_text_content($item, array('SalesAndPurchaseRef','PrefVendorRef','ListID')),
            'SalesAndPurchase_PrefVendor_FullName' => $this->get_text_content($item, array('SalesAndPurchaseRef','PrefVendorRef','FullName')),
            'DataExtItems' => $this->get_dataext_items($item, 'ItemNonInventory', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_itemnoninventory_model');
        $query = new $this->CI->Qb_itemnoninventory_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
     
}

/* End of file */
