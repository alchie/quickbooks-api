<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_itemdiscount extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_itemdiscount_model');
        $model = new $this->CI->Qb_itemdiscount_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_ITEMDISCOUNTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMDISCOUNTQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ItemDiscountQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ItemDiscountQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>ItemDiscount</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'ItemdiscountRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_itemdiscount_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_itemdiscount_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setFullname($item_obj->FullName);
                $model->setIsactive(($item_obj->IsActive)?1:0);
                $model->setParentListid($item_obj->Parent_ListID);
                $model->setParentFullname($item_obj->Parent_FullName);
                $model->setSublevel($item_obj->Sublevel);
                $model->setItemdesc($item_obj->ItemDesc);
                $model->setSalestaxcodeListid($item_obj->SalesTaxCode_ListID);
                $model->setSalestaxcodeFullname($item_obj->SalesTaxCode_FullName);
                $model->setDiscountrate($item_obj->DiscountRate);
                $model->setDiscountratepercent($item_obj->DiscountRatePercent);
                $model->setAccountListid($item_obj->Account_ListID);
                $model->setAccountFullname($item_obj->Account_FullName);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ItemDiscountRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'FullName' => $this->get_text_content($item, array('FullName')),
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'Parent_ListID' => $this->get_text_content($item, array('ParentRef','ListID')),
            'Parent_FullName' => $this->get_text_content($item, array('ParentRef','FullName')),
            'Sublevel' => $this->get_text_content($item, array('Sublevel')),
            'ItemDesc' => $this->get_text_content($item, array('ItemDesc')),
            'SalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxCodeRef','ListID')),
            'SalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxCodeRef','FullName')),
            'DiscountRate' => $this->get_text_content($item, array('DiscountRate')),
            'DiscountRatePercent' => $this->get_text_content($item, array('DiscountRatePercent')),
            'Account_ListID' => $this->get_text_content($item, array('AccountRef','ListID')),
            'Account_FullName' => $this->get_text_content($item, array('AccountRef','FullName')),

            'DataExtItems' => $this->get_dataext_items($item, 'ItemDiscount', 'ListID'),

          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_itemdiscount_model');
        $query = new $this->CI->Qb_itemdiscount_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file */
