<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_preferences extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_preferences_model');
        $model = new $this->CI->Qb_preferences_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<PreferencesQueryRq>' .  "\n" .
'</PreferencesQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'PreferencesRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_preferences_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_preferences_model();
                $model->setAccountingprefsIsusingaccountnumbers(($item_obj->AccountingPrefs_IsUsingAccountNumbers=='true')?1:0);
                $model->setAccountingprefsIsrequiringaccounts(($item_obj->AccountingPrefs_IsRequiringAccounts=='true')?1:0);
                $model->setAccountingprefsIsusingclasstracking(($item_obj->AccountingPrefs_IsUsingClassTracking=='true')?1:0);
                $model->setAccountingprefsIsusingaudittrail(($item_obj->AccountingPrefs_IsUsingAuditTrail=='true')?1:0);
                $model->setAccountingprefsIsassigningjournalentrynumbers(($item_obj->AccountingPrefs_IsAssigningJournalEntryNumbers=='true')?1:0);
                $model->setAccountingprefsClosingdate($item_obj->AccountingPrefs_ClosingDate);
                $model->setFinancechargeprefsAnnualinterestrate($item_obj->FinanceChargePrefs_AnnualInterestRate);
                $model->setFinancechargeprefsMinfinancecharge($item_obj->FinanceChargePrefs_MinFinanceCharge);
                $model->setFinancechargeprefsGraceperiod($item_obj->FinanceChargePrefs_GracePeriod);
                $model->setFinancechargeprefsFinancechargeaccountListid($item_obj->FinanceChargePrefs_FinanceChargeAccount_ListID);
                $model->setFinancechargeprefsFinancechargeaccountFullname($item_obj->FinanceChargePrefs_FinanceChargeAccount_FullName);
                $model->setFinancechargeprefsIsassessingforoverduecharges(($item_obj->FinanceChargePrefs_IsAssessingForOverdueCharges=='true')?1:0);
                $model->setFinancechargeprefsCalculatechargesfrom($item_obj->FinanceChargePrefs_CalculateChargesFrom);
                $model->setFinancechargeprefsIsmarkedtobeprinted(($item_obj->FinanceChargePrefs_IsMarkedToBePrinted=='true')?1:0);
                $model->setJobsandestimatesprefsIsusingestimates(($item_obj->JobsAndEstimatesPrefs_IsUsingEstimates=='true')?1:0);
                $model->setJobsandestimatesprefsIsusingprogressinvoicing(($item_obj->JobsAndEstimatesPrefs_IsUsingProgressInvoicing=='true')?1:0);
                $model->setJobsandestimatesprefsIsprintingitemswithzeroamounts(($item_obj->JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts=='true')?1:0);
                $model->setMulticurrencyprefsIsmulticurrencyon(($item_obj->MultiCurrencyPrefs_IsMultiCurrencyOn=='true')?1:0);
                $model->setMulticurrencyprefsHomecurrencyListid($item_obj->MultiCurrencyPrefs_HomeCurrency_ListID);
                $model->setMulticurrencyprefsHomecurrencyFullname($item_obj->MultiCurrencyPrefs_HomeCurrency_FullName);
                $model->setMultilocationinventoryprefsIsmultilocationinventoryavailable(($item_obj->MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable=='true')?1:0);
                $model->setMultilocationinventoryprefsIsmultilocationinventoryenabled(($item_obj->MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled=='true')?1:0);
                $model->setPurchasesandvendorsprefsIsusinginventory(($item_obj->PurchasesAndVendorsPrefs_IsUsingInventory=='true')?1:0);
                $model->setPurchasesandvendorsprefsDaysbillsaredue($item_obj->PurchasesAndVendorsPrefs_DaysBillsAreDue);
                $model->setPurchasesandvendorsprefsIsautomaticallyusingdiscounts(($item_obj->PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts=='true')?1:0);
                $model->setPurchasesandvendorsprefsDefaultdiscountaccountListid($item_obj->PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID);
                $model->setPurchasesandvendorsprefsDefaultdiscountaccountFullname($item_obj->PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName);
                $model->setReportsprefsAgingreportbasis($item_obj->ReportsPrefs_AgingReportBasis);
                $model->setReportsprefsSummaryreportbasis($item_obj->ReportsPrefs_SummaryReportBasis);
                $model->setSalesandcustomersprefsDefaultshipmethodListid($item_obj->SalesAndCustomersPrefs_DefaultShipMethod_ListID);
                $model->setSalesandcustomersprefsDefaultshipmethodFullname($item_obj->SalesAndCustomersPrefs_DefaultShipMethod_FullName);
                $model->setSalesandcustomersprefsDefaultfob($item_obj->SalesAndCustomersPrefs_DefaultFOB);
                $model->setSalesandcustomersprefsDefaultmarkup($item_obj->SalesAndCustomersPrefs_DefaultMarkup);
                $model->setSalesandcustomersprefsIstrackingreimbursedexpensesasincome(($item_obj->SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome=='true')?1:0);
                $model->setSalesandcustomersprefsIsautoapplyingpayments(($item_obj->SalesAndCustomersPrefs_IsAutoApplyingPayments=='true')?1:0);
                $model->setSalesandcustomersprefsPricelevelsIsusingpricelevels(($item_obj->SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels=='true')?1:0);
                $model->setSalesandcustomersprefsPricelevelsIsroundingsalespriceup(($item_obj->SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp=='true')?1:0);
                $model->setSalestaxprefsDefaultitemsalestaxListid($item_obj->SalesTaxPrefs_DefaultItemSalesTax_ListID);
                $model->setSalestaxprefsDefaultitemsalestaxFullname($item_obj->SalesTaxPrefs_DefaultItemSalesTax_FullName);
                $model->setSalestaxprefsPaysalestax($item_obj->SalesTaxPrefs_PaySalesTax);
                $model->setSalestaxprefsDefaulttaxablesalestaxcodeListid($item_obj->SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID);
                $model->setSalestaxprefsDefaulttaxablesalestaxcodeFullname($item_obj->SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName);
                $model->setSalestaxprefsDefaultnontaxablesalestaxcodeListid($item_obj->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID);
                $model->setSalestaxprefsDefaultnontaxablesalestaxcodeFullname($item_obj->SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName);
                $model->setSalestaxprefsIsusingvendortaxcode(($item_obj->SalesTaxPrefs_IsUsingVendorTaxCode=='true')?1:0);
                $model->setSalestaxprefsIsusingcustomertaxcode(($item_obj->SalesTaxPrefs_IsUsingCustomerTaxCode=='true')?1:0);
                $model->setSalestaxprefsIsusingamountsincludetax(($item_obj->SalesTaxPrefs_IsUsingAmountsIncludeTax=='true')?1:0);
                $model->setTimetrackingprefsFirstdayofweek($item_obj->TimeTrackingPrefs_FirstDayOfWeek);
                $model->setCurrentappaccessrightsIsautomaticloginallowed(($item_obj->CurrentAppAccessRights_IsAutomaticLoginAllowed=='true')?1:0);
                $model->setCurrentappaccessrightsAutomaticloginusername($item_obj->CurrentAppAccessRights_AutomaticLoginUserName);
                $model->setCurrentappaccessrightsIspersonaldataaccessallowed(($item_obj->CurrentAppAccessRights_IsPersonalDataAccessAllowed=='true')?1:0);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'PreferencesRet') {

          $this->items[] = array(
            'AccountingPrefs_IsUsingAccountNumbers' => $this->get_text_content($item, array('AccountingPrefsRef','IsUsingAccountNumbers')),
            'AccountingPrefs_IsRequiringAccounts' => $this->get_text_content($item, array('AccountingPrefsRef','IsRequiringAccounts')),
            'AccountingPrefs_IsUsingClassTracking' => $this->get_text_content($item, array('AccountingPrefsRef','IsUsingClassTracking')),
            'AccountingPrefs_IsUsingAuditTrail' => $this->get_text_content($item, array('AccountingPrefsRef','IsUsingAuditTrail')),
            'AccountingPrefs_IsAssigningJournalEntryNumbers' => $this->get_text_content($item, array('AccountingPrefsRef','IsAssigningJournalEntryNumbers')),
            'AccountingPrefs_ClosingDate' => $this->get_text_content($item, array('AccountingPrefsRef','ClosingDate')),
            'FinanceChargePrefs_AnnualInterestRate' => $this->get_text_content($item, array('FinanceChargePrefsRef','AnnualInterestRate')),
            'FinanceChargePrefs_MinFinanceCharge' => $this->get_text_content($item, array('FinanceChargePrefsRef','MinFinanceCharge')),
            'FinanceChargePrefs_GracePeriod' => $this->get_text_content($item, array('FinanceChargePrefsRef','GracePeriod')),
            'FinanceChargePrefs_FinanceChargeAccount_ListID' => $this->get_text_content($item, array('FinanceChargePrefsRef','FinanceChargeAccountRef','ListID')),
            'FinanceChargePrefs_FinanceChargeAccount_FullName' => $this->get_text_content($item, array('FinanceChargePrefsRef','FinanceChargeAccountRef','FullName')),
            'FinanceChargePrefs_IsAssessingForOverdueCharges' => $this->get_text_content($item, array('FinanceChargePrefsRef','IsAssessingForOverdueCharges')),
            'FinanceChargePrefs_CalculateChargesFrom' => $this->get_text_content($item, array('FinanceChargePrefsRef','CalculateChargesFrom')),
            'FinanceChargePrefs_IsMarkedToBePrinted' => $this->get_text_content($item, array('FinanceChargePrefsRef','IsMarkedToBePrinted')),
            'JobsAndEstimatesPrefs_IsUsingEstimates' => $this->get_text_content($item, array('JobsAndEstimatesPrefsRef','IsUsingEstimates')),
            'JobsAndEstimatesPrefs_IsUsingProgressInvoicing' => $this->get_text_content($item, array('JobsAndEstimatesPrefsRef','IsUsingProgressInvoicing')),
            'JobsAndEstimatesPrefs_IsPrintingItemsWithZeroAmounts' => $this->get_text_content($item, array('JobsAndEstimatesPrefsRef','IsPrintingItemsWithZeroAmounts')),
            'MultiCurrencyPrefs_IsMultiCurrencyOn' => $this->get_text_content($item, array('MultiCurrencyPrefsRef','IsMultiCurrencyOn')),
            'MultiCurrencyPrefs_HomeCurrency_ListID' => $this->get_text_content($item, array('MultiCurrencyPrefsRef','HomeCurrencyRef','ListID')),
            'MultiCurrencyPrefs_HomeCurrency_FullName' => $this->get_text_content($item, array('MultiCurrencyPrefsRef','HomeCurrencyRef','FullName')),
            'MultiLocationInventoryPrefs_IsMultiLocationInventoryAvailable' => $this->get_text_content($item, array('MultiLocationInventoryPrefsRef','IsMultiLocationInventoryAvailable')),
            'MultiLocationInventoryPrefs_IsMultiLocationInventoryEnabled' => $this->get_text_content($item, array('MultiLocationInventoryPrefsRef','IsMultiLocationInventoryEnabled')),
            'PurchasesAndVendorsPrefs_IsUsingInventory' => $this->get_text_content($item, array('PurchasesAndVendorsPrefsRef','IsUsingInventory')),
            'PurchasesAndVendorsPrefs_DaysBillsAreDue' => $this->get_text_content($item, array('PurchasesAndVendorsPrefsRef','DaysBillsAreDue')),
            'PurchasesAndVendorsPrefs_IsAutomaticallyUsingDiscounts' => $this->get_text_content($item, array('PurchasesAndVendorsPrefsRef','IsAutomaticallyUsingDiscounts')),
            'PurchasesAndVendorsPrefs_DefaultDiscountAccount_ListID' => $this->get_text_content($item, array('PurchasesAndVendorsPrefsRef','DefaultDiscountAccountRef','ListID')),
            'PurchasesAndVendorsPrefs_DefaultDiscountAccount_FullName' => $this->get_text_content($item, array('PurchasesAndVendorsPrefsRef','DefaultDiscountAccountRef','FullName')),
            'ReportsPrefs_AgingReportBasis' => $this->get_text_content($item, array('ReportsPrefsRef','AgingReportBasis')),
            'ReportsPrefs_SummaryReportBasis' => $this->get_text_content($item, array('ReportsPrefsRef','SummaryReportBasis')),
            'SalesAndCustomersPrefs_DefaultShipMethod_ListID' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','DefaultShipMethodRef','ListID')),
            'SalesAndCustomersPrefs_DefaultShipMethod_FullName' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','DefaultShipMethodRef','FullName')),
            'SalesAndCustomersPrefs_DefaultFOB' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','DefaultFOB')),
            'SalesAndCustomersPrefs_DefaultMarkup' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','DefaultMarkup')),
            'SalesAndCustomersPrefs_IsTrackingReimbursedExpensesAsIncome' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','IsTrackingReimbursedExpensesAsIncome')),
            'SalesAndCustomersPrefs_IsAutoApplyingPayments' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','IsAutoApplyingPayments')),
            'SalesAndCustomersPrefs_PriceLevels_IsUsingPriceLevels' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','PriceLevelsRef','IsUsingPriceLevels')),
            'SalesAndCustomersPrefs_PriceLevels_IsRoundingSalesPriceUp' => $this->get_text_content($item, array('SalesAndCustomersPrefsRef','PriceLevelsRef','IsRoundingSalesPriceUp')),
            'SalesTaxPrefs_DefaultItemSalesTax_ListID' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultItemSalesTaxRef','ListID')),
            'SalesTaxPrefs_DefaultItemSalesTax_FullName' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultItemSalesTaxRef','FullName')),
            'SalesTaxPrefs_PaySalesTax' => $this->get_text_content($item, array('SalesTaxPrefsRef','PaySalesTax')),
            'SalesTaxPrefs_DefaultTaxableSalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultTaxableSalesTaxCodeRef','ListID')),
            'SalesTaxPrefs_DefaultTaxableSalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultTaxableSalesTaxCodeRef','FullName')),
            'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultNonTaxableSalesTaxCodeRef','ListID')),
            'SalesTaxPrefs_DefaultNonTaxableSalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxPrefsRef','DefaultNonTaxableSalesTaxCodeRef','FullName')),
            'SalesTaxPrefs_IsUsingVendorTaxCode' => $this->get_text_content($item, array('SalesTaxPrefsRef','IsUsingVendorTaxCode')),
            'SalesTaxPrefs_IsUsingCustomerTaxCode' => $this->get_text_content($item, array('SalesTaxPrefsRef','IsUsingCustomerTaxCode')),
            'SalesTaxPrefs_IsUsingAmountsIncludeTax' => $this->get_text_content($item, array('SalesTaxPrefsRef','IsUsingAmountsIncludeTax')),
            'TimeTrackingPrefs_FirstDayOfWeek' => $this->get_text_content($item, array('TimeTrackingPrefsRef','FirstDayOfWeek')),
            'CurrentAppAccessRights_IsAutomaticLoginAllowed' => $this->get_text_content($item, array('CurrentAppAccessRightsRef','IsAutomaticLoginAllowed')),
            'CurrentAppAccessRights_AutomaticLoginUserName' => $this->get_text_content($item, array('CurrentAppAccessRightsRef','AutomaticLoginUserName')),
            'CurrentAppAccessRights_IsPersonalDataAccessAllowed' => $this->get_text_content($item, array('CurrentAppAccessRightsRef','IsPersonalDataAccessAllowed')),

          );

        }
      }
    }
 
}

/* End of file */
