<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_queue {

    public function __construct()
    {
       $this->CI =& get_instance();

       // load config
       $this->CI->load->config('quickbooks_api');
       
       // load queue model
       $this->CI->load->model('Qb_api_queue_model');


    }

    public function init($ticket)
    {

      if( $this->CI->config->item('QB_API_ACCOUNTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_account');
          $query = new $this->CI->qb_api_xml_account();
          $this->insert_queue($ticket->nodeValue, 'AccountQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_BILLINGRATEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_billingrate');
          $query = new $this->CI->qb_api_xml_billingrate();
          $this->insert_queue($ticket->nodeValue, 'BillingRateQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_BILLPAYMENTCHECKQUERY') ) {
          $this->CI->load->library('Qb_api_xml_billpaymentcheck');
          $query = new $this->CI->qb_api_xml_billpaymentcheck();
          $this->insert_queue($ticket->nodeValue, 'BillPaymentCheckQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_BILLPAYMENTCREDITCARDQUERY') ) {
          $this->CI->load->library('Qb_api_xml_billpaymentcreditcard');
          $query = new $this->CI->qb_api_xml_billpaymentcreditcard();
          $this->insert_queue($ticket->nodeValue, 'BillPaymentCreditCardQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_BILLQUERY') ) {
          $this->CI->load->library('Qb_api_xml_bill');
          $query = new $this->CI->qb_api_xml_bill();
          $this->insert_queue($ticket->nodeValue, 'BillQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_BILLTOPAYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_billtopay');
          $query = new $this->CI->qb_api_xml_billtopay();
          $this->insert_queue($ticket->nodeValue, 'BillToPayQuery', array());
      }

      if( $this->CI->config->item('QB_API_BUILDASSEMBLYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_buildassembly');
          $query = new $this->CI->qb_api_xml_buildassembly();
          $this->insert_queue($ticket->nodeValue, 'BuildAssemblyQuery', array());
      }

      if( $this->CI->config->item('QB_API_CHARGEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_charge');
          $query = new $this->CI->qb_api_xml_charge();
          $this->insert_queue($ticket->nodeValue, 'ChargeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CHECKQUERY') ) {
          $this->CI->load->library('Qb_api_xml_check');
          $query = new $this->CI->qb_api_xml_check();
          $this->insert_queue($ticket->nodeValue, 'CheckQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CLASSQUERY') ) {
          $this->CI->load->library('Qb_api_xml_class');
          $query = new $this->CI->qb_api_xml_class();
          $this->insert_queue($ticket->nodeValue, 'ClassQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CREDITCARDCHARGEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_creditcardcharge');
          $query = new $this->CI->qb_api_xml_creditcardcharge();
          $this->insert_queue($ticket->nodeValue, 'CreditCardChargeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CREDITCARDCREDITQUERY') ) {
          $this->CI->load->library('Qb_api_xml_creditcardcredit');
          $query = new $this->CI->qb_api_xml_creditcardcredit();
          $this->insert_queue($ticket->nodeValue, 'CreditCardCreditQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CREDITMEMOQUERY') ) {
          $this->CI->load->library('Qb_api_xml_creditmemo');
          $query = new $this->CI->qb_api_xml_creditmemo();
          $this->insert_queue($ticket->nodeValue, 'CreditMemoQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CURRENCYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_currency');
          $query = new $this->CI->qb_api_xml_currency();
          $this->insert_queue($ticket->nodeValue, 'CurrencyQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CUSTOMERMSGQUERY') ) {
          $this->CI->load->library('Qb_api_xml_customermsg');
          $query = new $this->CI->qb_api_xml_customermsg();
          $this->insert_queue($ticket->nodeValue, 'CustomerMsgQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CUSTOMERQUERY') ) {
          $this->CI->load->library('Qb_api_xml_customer');
          $query = new $this->CI->qb_api_xml_customer();
          $this->insert_queue($ticket->nodeValue, 'CustomerQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_CUSTOMERTYPEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_customertype');
          $query = new $this->CI->qb_api_xml_customertype();
          $this->insert_queue($ticket->nodeValue, 'CustomerTypeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_DATAEXTDEFQUERY') ) {
          $this->CI->load->library('Qb_api_xml_dataextdef');
          $query = new $this->CI->qb_api_xml_dataextdef();
          $this->insert_queue($ticket->nodeValue, 'DataExtDefQuery', array());
      }

      if( $this->CI->config->item('QB_API_DATEDRIVENTERMSQUERY') ) {
          $this->CI->load->library('Qb_api_xml_datedriventerms');
          $query = new $this->CI->qb_api_xml_datedriventerms();
          $this->insert_queue($ticket->nodeValue, 'DateDrivenTermsQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_DEPOSITQUERY') ) {
          $this->CI->load->library('Qb_api_xml_deposit');
          $query = new $this->CI->qb_api_xml_deposit();
          $this->insert_queue($ticket->nodeValue, 'DepositQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_EMPLOYEEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_employee');
          $query = new $this->CI->qb_api_xml_employee();
          $this->insert_queue($ticket->nodeValue, 'EmployeeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ESTIMATEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_estimate');
          $query = new $this->CI->qb_api_xml_estimate();
          $this->insert_queue($ticket->nodeValue, 'EstimateQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_INVENTORYADJUSTMENTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_inventoryadjustment');
          $query = new $this->CI->qb_api_xml_inventoryadjustment();
          $this->insert_queue($ticket->nodeValue, 'InventoryAdjustmentQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_INVOICEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_invoice');
          $query = new $this->CI->qb_api_xml_invoice();
          $this->insert_queue($ticket->nodeValue, 'InvoiceQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMDISCOUNTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemdiscount');
          $query = new $this->CI->qb_api_xml_itemdiscount();
          $this->insert_queue($ticket->nodeValue, 'ItemDiscountQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMFIXEDASSETQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemfixedasset');
          $query = new $this->CI->qb_api_xml_itemfixedasset();
          $this->insert_queue($ticket->nodeValue, 'ItemFixedAssetQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMGROUPQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemgroup');
          $query = new $this->CI->qb_api_xml_itemgroup();
          $this->insert_queue($ticket->nodeValue, 'ItemGroupQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMINVENTORYASSEMBLYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_iteminventoryassembly');
          $query = new $this->CI->qb_api_xml_iteminventoryassembly();
          $this->insert_queue($ticket->nodeValue, 'ItemInventoryAssemblyQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMINVENTORYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_iteminventory');
          $query = new $this->CI->qb_api_xml_iteminventory();
          $this->insert_queue($ticket->nodeValue, 'ItemInventoryQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMNONINVENTORYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemnoninventory');
          $query = new $this->CI->qb_api_xml_itemnoninventory();
          $this->insert_queue($ticket->nodeValue, 'ItemNonInventoryQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMOTHERCHARGEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemothercharge');
          $query = new $this->CI->qb_api_xml_itemothercharge();
          $this->insert_queue($ticket->nodeValue, 'ItemOtherChargeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMPAYMENTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itempayment');
          $query = new $this->CI->qb_api_xml_itempayment();
          $this->insert_queue($ticket->nodeValue, 'ItemPaymentQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMRECEIPTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemreceipt');
          $query = new $this->CI->qb_api_xml_itemreceipt();
          $this->insert_queue($ticket->nodeValue, 'ItemReceiptQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMSALESTAXQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemsalestax');
          $query = new $this->CI->qb_api_xml_itemsalestax();
          $this->insert_queue($ticket->nodeValue, 'ItemSalesTaxQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMSERVICEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemservice');
          $query = new $this->CI->qb_api_xml_itemservice();
          $this->insert_queue($ticket->nodeValue, 'ItemServiceQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_ITEMSUBTOTALQUERY') ) {
          $this->CI->load->library('Qb_api_xml_itemsubtotal');
          $query = new $this->CI->qb_api_xml_itemsubtotal();
          $this->insert_queue($ticket->nodeValue, 'ItemSubtotalQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_JOBTYPEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_jobtype');
          $query = new $this->CI->qb_api_xml_jobtype();
          $this->insert_queue($ticket->nodeValue, 'JobTypeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_JOURNALENTRYQUERY') ) {
          $this->CI->load->library('Qb_api_xml_journalentry');
          $query = new $this->CI->qb_api_xml_journalentry();
          $this->insert_queue($ticket->nodeValue, 'JournalEntryQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_PAYMENTMETHODQUERY') ) {
          $this->CI->load->library('Qb_api_xml_paymentmethod');
          $query = new $this->CI->qb_api_xml_paymentmethod();
          $this->insert_queue($ticket->nodeValue, 'PaymentMethodQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_PAYROLLITEMWAGEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_payrollitemwage');
          $query = new $this->CI->qb_api_xml_payrollitemwage();
          $this->insert_queue($ticket->nodeValue, 'PayrollItemWageQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_PREFERENCESQUERY') ) {
          $this->CI->load->library('Qb_api_xml_preferences');
          $query = new $this->CI->qb_api_xml_preferences();
          $this->insert_queue($ticket->nodeValue, 'PreferencesQuery', array());
      }

      if( $this->CI->config->item('QB_API_PRICELEVELQUERY') ) {
          $this->CI->load->library('Qb_api_xml_pricelevel');
          $query = new $this->CI->qb_api_xml_pricelevel();
          $this->insert_queue($ticket->nodeValue, 'PriceLevelQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_PURCHASEORDERQUERY') ) {
          $this->CI->load->library('Qb_api_xml_purchaseorder');
          $query = new $this->CI->qb_api_xml_purchaseorder();
          $this->insert_queue($ticket->nodeValue, 'PurchaseOrderQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_RECEIVEPAYMENTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_receivepayment');
          $query = new $this->CI->qb_api_xml_receivepayment();
          $this->insert_queue($ticket->nodeValue, 'ReceivePaymentQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_SALESORDERQUERY') ) {
          $this->CI->load->library('Qb_api_xml_salesorder');
          $query = new $this->CI->qb_api_xml_salesorder();
          $this->insert_queue($ticket->nodeValue, 'SalesOrderQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_SALESRECEIPTQUERY') ) {
          $this->CI->load->library('Qb_api_xml_salesreceipt');
          $query = new $this->CI->qb_api_xml_salesreceipt();
          $this->insert_queue($ticket->nodeValue, 'SalesReceiptQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_SALESREPQUERY') ) {
          $this->CI->load->library('Qb_api_xml_salesrep');
          $query = new $this->CI->qb_api_xml_salesrep();
          $this->insert_queue($ticket->nodeValue, 'SalesRepQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_SALESTAXCODEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_salestaxcode');
          $query = new $this->CI->qb_api_xml_salestaxcode();
          $this->insert_queue($ticket->nodeValue, 'SalesTaxCodeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_SHIPMETHODQUERY') ) {
          $this->CI->load->library('Qb_api_xml_shipmethod');
          $query = new $this->CI->qb_api_xml_shipmethod();
          $this->insert_queue($ticket->nodeValue, 'ShipMethodQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_STANDARDTERMSQUERY') ) {
          $this->CI->load->library('Qb_api_xml_standardterms');
          $query = new $this->CI->qb_api_xml_standardterms();
          $this->insert_queue($ticket->nodeValue, 'StandardTermsQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_TEMPLATEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_template');
          $query = new $this->CI->qb_api_xml_template();
          $this->insert_queue($ticket->nodeValue, 'TemplateQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_TERMQUERY') ) {
          $this->CI->load->library('Qb_api_xml_term');
          $query = new $this->CI->qb_api_xml_term();
          $this->insert_queue($ticket->nodeValue, 'TermQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_TIMETRACKINGQUERY') ) {
          $this->CI->load->library('Qb_api_xml_timetracking');
          $query = new $this->CI->qb_api_xml_timetracking();
          $this->insert_queue($ticket->nodeValue, 'TimeTrackingQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_UNITOFMEASURESETQUERY') ) {
          $this->CI->load->library('Qb_api_xml_unitofmeasureset');
          $query = new $this->CI->qb_api_xml_unitofmeasureset();
          $this->insert_queue($ticket->nodeValue, 'UnitOfMeasureSetQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_VEHICLEMILEAGEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_vehiclemileage');
          $query = new $this->CI->qb_api_xml_vehiclemileage();
          $this->insert_queue($ticket->nodeValue, 'VehicleMileageQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_VEHICLEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_vehicle');
          $query = new $this->CI->qb_api_xml_vehicle();
          $this->insert_queue($ticket->nodeValue, 'VehicleQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_VENDORCREDITQUERY') ) {
          $this->CI->load->library('Qb_api_xml_vendorcredit');
          $query = new $this->CI->qb_api_xml_vendorcredit();
          $this->insert_queue($ticket->nodeValue, 'VendorCreditQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_VENDORQUERY') ) {
          $this->CI->load->library('Qb_api_xml_vendor');
          $query = new $this->CI->qb_api_xml_vendor();
          $this->insert_queue($ticket->nodeValue, 'VendorQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_VENDORTYPEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_vendortype');
          $query = new $this->CI->qb_api_xml_vendortype();
          $this->insert_queue($ticket->nodeValue, 'VendorTypeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }

      if( $this->CI->config->item('QB_API_WORKERSCOMPCODEQUERY') ) {
          $this->CI->load->library('Qb_api_xml_workerscompcode');
          $query = new $this->CI->qb_api_xml_workerscompcode();
          $this->insert_queue($ticket->nodeValue, 'WorkersCompCodeQuery', array(
              'TimeModified' => $query->getLastTimeModified()
          ));
      }
    }

    public function insert_more_queue($requestID, $method, $ticket, $iteratorID, $remainingCount=1, $maxresults=25) {

        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setId($requestID,true);
        $queue_data = $queue->get();
        
        $options = (isset($queue_data->options) && ($queue_data->options)) ? json_decode($queue_data->options) : false;
        if( !isset( $options->iteratorID ) ) {
            $queue_more = ceil($remainingCount / $maxresults);
            if( $queue_more > 0 ) {
                for($i=0;$i<$queue_more;$i++) {
                    $this->insert_queue($ticket, $method, array(
                        'iteratorID' => $iteratorID, 
                        'TimeModified' => $options->TimeModified
                    ));
                }
            }
        }

    }

    public function insert_queue($ticket_id, $method, $options=array()) {
        // queue
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setRequestMethod($method);
        $queue->setTicketId($ticket_id);
        if( $options ) {
            $queue->setOptions( json_encode( $options ) );
        }
        $queue->setDone('0');
        $queue->insert();
    }

    public function get_queue($request_id) {
        // queue
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setId($request_id, true);
        return $queue->get();
    }

    public function fetch_queue($ticket_id) {
        // queue
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setTicketId($ticket_id, true);
        $queue->setDone('0',true);
        $queue->set_order('id', 'ASC');
        return $queue->get();
    }
    
    public function delete_queue($ticket_id, $method='', $done=1) {
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setTicketId($ticket_id, true);
        if( $method ) {
            $queue->setRequestMethod($method, true);
            $queue->setDone($done,true);
        }
        $queue->delete();
    }

    public function clear_queue() {
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setDone(0,true);
        $queue->delete();
    }

    public function delete_queue_id($id) {
        // queue
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setId($id, true);
        $queue->delete();
    }

    public function update_queue_options($id, $options='') {
        // queue
        $queue = new $this->CI->Qb_api_queue_model();
        $queue->setId($id, true);
        $queue->setOptions( json_encode($options) );
        $queue->update();
    }

}