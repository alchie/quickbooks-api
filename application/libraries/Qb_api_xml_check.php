<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_check extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }

    public function getLastTimeModified() {
        $this->CI->load->model('Qb_check_model');
        $checks = new $this->CI->Qb_check_model();
        $checks->set_order('TimeModified', 'DESC');
        $checks_data = $checks->get();
        return (($checks_data) && isset($checks_data->TimeModified)) ? $checks_data->TimeModified : false;
    }

    public function request($queue) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_CHECKQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CHECKQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified . " +1 second" ) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<CheckQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ModifiedDateRangeFilter>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'</ModifiedDateRangeFilter>' .  "\n" .
'<IncludeLineItems>true</IncludeLineItems>' .  "\n" .
'<IncludeLinkedTxns>true</IncludeLinkedTxns>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</CheckQueryRq>' .  "\n" .
'<TxnDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<TxnDelType>Check</TxnDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</TxnDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        if( $this->items ) {

              $this->CI->load->model('Qb_check_model');

              foreach($this->items as $item) {
                
                $item_obj = (object) $item;

                $checks = new $this->CI->Qb_check_model();
                $checks->setTxnid($item_obj->TxnID,true);
                $checks->setTimecreated($item_obj->TimeCreated);
                $checks->setTimemodified($item_obj->TimeModified);
                $checks->setEditsequence($item_obj->EditSequence);
                $checks->setTxnnumber($item_obj->TxnNumber);
                $checks->setAccountListid($item_obj->Account_ListID);
                $checks->setAccountFullname($item_obj->Account_FullName);
                $checks->setPayeeentityrefListid($item_obj->PayeeEntity_ListID);
                $checks->setPayeeentityrefFullname($item_obj->PayeeEntity_FullName);
                $checks->setRefnumber($item_obj->RefNumber);
                $checks->setTxndate($item_obj->TxnDate);
                $checks->setAmount($item_obj->Amount);
                $checks->setMemo($item_obj->Memo);
                $checks->setAddressAddr1($item_obj->Address_Addr1);
                $checks->setAddressAddr2($item_obj->Address_Addr2);
                $checks->setAddressAddr3($item_obj->Address_Addr3);
                $checks->setAddressAddr4($item_obj->Address_Addr4);
                $checks->setAddressAddr5($item_obj->Address_Addr5);
                $checks->setAddressCity($item_obj->Address_City);
                $checks->setAddressState($item_obj->Address_State);
                $checks->setAddressPostalcode($item_obj->Address_PostalCode);
                $checks->setAddressCountry($item_obj->Address_Country);
                $checks->setAddressNote($item_obj->Address_Note);
                $checks->setAddressblockAddr1($item_obj->AddressBlock_Addr1);
                $checks->setAddressblockAddr2($item_obj->AddressBlock_Addr2);
                $checks->setAddressblockAddr3($item_obj->AddressBlock_Addr3);
                $checks->setAddressblockAddr4($item_obj->AddressBlock_Addr4);
                $checks->setAddressblockAddr5($item_obj->AddressBlock_Addr5);
                $checks->setIstobeprinted($item_obj->IsToBePrinted);

                if( $checks->nonEmpty() ) {
                  $checks->update();
                } else {
                  $checks->insert();
                }

                $this->CI->load->model('Qb_check_expenseline_model');
                $this->CI->load->model('Qb_check_itemline_model');
                $this->CI->load->model('Qb_check_linkedtxn_model');
                $this->CI->load->model('Qb_check_itemgroupline_model');
                $this->CI->load->model('Qb_check_itemgroupline_itemline_model');

                $expenseline = new $this->CI->Qb_check_expenseline_model();
                $expenseline->setCheckTxnid($item_obj->TxnID,true);
                $expenseline->delete();

                $itemline = new $this->CI->Qb_check_itemline_model();
                $itemline->setCheckTxnid($item_obj->TxnID,true);
                $itemline->delete();

                $linkedtxn = new $this->CI->Qb_check_linkedtxn_model();
                $linkedtxn->setCheckTxnid($item_obj->TxnID,true);
                $linkedtxn->delete();

                $itemgroupline = new $this->CI->Qb_check_itemgroupline_model();
                $itemgroupline->setCheckTxnid($item_obj->TxnID,true);
                $itemgroupline->delete();

                $itemgroupline2 = new $this->CI->Qb_check_itemgroupline_itemline_model();
                $itemgroupline2->setCheckTxnid($item_obj->TxnID,true);
                $itemgroupline2->delete();

                if( count($item_obj->ExpenseLineItems) > 0 ) {

                    foreach( $item_obj->ExpenseLineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;

                        $expenseline = new $this->CI->Qb_check_expenseline_model();
                        $expenseline->setCheckTxnid($item_obj->TxnID,true);
                        $expenseline->setSortorder($line_item_obj->SortOrder);
                        $expenseline->setTxnlineid($line_item_obj->TxnLineID,true);
                        $expenseline->setAccountListid($line_item_obj->Account_ListID);
                        $expenseline->setAccountFullname($line_item_obj->Account_FullName);
                        $expenseline->setAmount($line_item_obj->Amount);
                        $expenseline->setCurrencyref($line_item_obj->Currency);
                        $expenseline->setExchangerate($line_item_obj->ExchangeRate);
                        $expenseline->setAmountinhomecurrency($line_item_obj->AmountInHomeCurrency);
                        $expenseline->setMemo($line_item_obj->Memo);
                        $expenseline->setCustomerListid($line_item_obj->Customer_ListID);
                        $expenseline->setCustomerFullname($line_item_obj->Customer_FullName);
                        $expenseline->setClassListid($line_item_obj->Class_ListID);
                        $expenseline->setClassFullname($line_item_obj->Class_FullName);
                        $expenseline->setBillablestatus($line_item_obj->BillableStatus);

                        if( $expenseline->nonEmpty() ) {
                          $expenseline->update();
                        } else {
                          $expenseline->insert();
                        }

                    }
                }
              

            if( count($item_obj->ItemLineItems) > 0 ) {

                    foreach( $item_obj->ItemLineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;

                        $itemline = new $this->CI->Qb_check_itemline_model();
                        $itemline->setCheckTxnid($item_obj->TxnID,true);
                        $itemline->setSortorder($line_item_obj->SortOrder);
                        $itemline->setTxnlineid($line_item_obj->TxnLineID,true);
                        $itemline->setItemListid($line_item_obj->Item_ListID);
                        $itemline->setItemFullname($line_item_obj->Item_FullName);
                        $itemline->setDescrip($line_item_obj->Descrip);
                        $itemline->setQuantity($line_item_obj->Quantity);
                        $itemline->setUnitofmeasure($line_item_obj->UnitOfMeasure);
                        $itemline->setOverrideuomsetListid($line_item_obj->OverrideUOMSet_ListID);
                        $itemline->setOverrideuomsetFullname($line_item_obj->OverrideUOMSet_FullName);
                        $itemline->setCost($line_item_obj->Cost);
                        $itemline->setAmount($line_item_obj->Amount);
                        $itemline->setCustomerListid($line_item_obj->Customer_ListID);
                        $itemline->setCustomerFullname($line_item_obj->Customer_FullName);
                        $itemline->setClassListid($line_item_obj->Class_ListID);
                        $itemline->setClassFullname($line_item_obj->Class_FullName);
                        $itemline->setBillablestatus($line_item_obj->BillableStatus);
                        if( $itemline->nonEmpty() ) {
                          $itemline->update();
                        } else {
                          $itemline->insert();
                        }

                    }
                }
            

            if( count($item_obj->ItemGroupLineItems) > 0 ) {

                    foreach( $item_obj->ItemGroupLineItems as $line_item ) {

                        $line_item_obj = (object) $line_item;

                        $itemgroupline = new $this->CI->Qb_check_itemline_model();
                        $itemgroupline->setCheckTxnid($item_obj->TxnID,true);
                        $itemgroupline->setSortorder($line_item_obj->SortOrder);
                        $itemgroupline->setTxnlineid($line_item_obj->TxnLineID,true);
                        $itemgroupline->setItemListid($line_item_obj->Item_ListID);
                        $itemgroupline->setItemFullname($line_item_obj->Item_FullName);
                        $itemgroupline->setDescrip($line_item_obj->Descrip);
                        $itemgroupline->setQuantity($line_item_obj->Quantity);
                        $itemgroupline->setUnitofmeasure($line_item_obj->UnitOfMeasure);
                        $itemgroupline->setOverrideuomsetListid($line_item_obj->OverrideUOMSet_ListID);
                        $itemgroupline->setOverrideuomsetFullname($line_item_obj->OverrideUOMSet_FullName);
                        $itemgroupline->setCost($line_item_obj->Cost);
                        $itemgroupline->setAmount($line_item_obj->Amount);
                        $itemgroupline->setCustomerListid($line_item_obj->Customer_ListID);
                        $itemgroupline->setCustomerFullname($line_item_obj->Customer_FullName);
                        $itemgroupline->setClassListid($line_item_obj->Class_ListID);
                        $itemgroupline->setClassFullname($line_item_obj->Class_FullName);
                        $itemgroupline->setBillablestatus($line_item_obj->BillableStatus);
                        
                        if( $itemgroupline->nonEmpty() ) {
                          $itemgroupline->update();
                        } else {
                          $itemgroupline->insert();
                        }

                    }
                }

            $this->insert_dataext_items($item_obj);
            
            }

        }

    }

    protected function _populate_items() {

foreach($this->data as $item) {
            if( $item->nodeName == 'CheckRet') {

              $ExpenseLineItems = array();
              $ItemLineItems = array();
              $ItemGroupLineItems = array();

              $ExpenseLineRet = $item->getElementsByTagName('ExpenseLineRet');
              $ItemLineRet = $item->getElementsByTagName('ItemLineRet');
              $ItemGroupLineRet = $item->getElementsByTagName('ItemGroupLineRet');
              
              if( $ExpenseLineRet ) {
                foreach($ExpenseLineRet as $expense_line) {
                  $ExpenseLineItems[] = array(
                    'TxnLineID' => $this->get_text_content($expense_line, 'TxnLineID'), 
                    'Amount' => $this->get_text_content($expense_line, 'Amount'), 
                    'Memo' => $this->get_text_content($expense_line, 'Memo'), 
                    'Account_ListID' => $this->get_text_content($expense_line, array('AccountRef','ListID')), 
                    'Account_FullName' => $this->get_text_content($expense_line, array('AccountRef','FullName')), 
                    'SortOrder' => $this->get_text_content($expense_line, 'SortOrder'), 
                    'Currency' => $this->get_text_content($expense_line, 'Currency'), 
                    'ExchangeRate' => $this->get_text_content($expense_line, 'ExchangeRate'), 
                    'AmountInHomeCurrency' => $this->get_text_content($expense_line, 'AmountInHomeCurrency'), 
                    'Customer_ListID' => $this->get_text_content($expense_line, array('CustomerRef','ListID')), 
                    'Customer_FullName' => $this->get_text_content($expense_line, array('CustomerRef','FullName')), 
                    'Class_ListID' => $this->get_text_content($expense_line, array('ClassRef','ListID')), 
                    'Class_FullName' => $this->get_text_content($expense_line, array('ClassRef','FullName')), 
                    'BillableStatus' => $this->get_text_content($expense_line, 'BillableStatus'), 
                  );
                }
              }

             if( $ItemLineRet ) {
                foreach($ItemLineRet as $item_line) {
                  $ItemLineItems[] = array(
                    'SortOrder' => $this->get_text_content($item_line, array('SortOrder')), 
                    'TxnLineID' => $this->get_text_content($item_line, array('TxnLineID')), 
                    'Item_ListID' => $this->get_text_content($item_line, array('ItemRef','ListID')), 
                    'Item_FullName' => $this->get_text_content($item_line, array('ItemRef','FullName')), 
                    'Descrip' => $this->get_text_content($item_line, array('Descrip')), 
                    'Quantity' => $this->get_text_content($item_line, array('Quantity')), 
                    'UnitOfMeasure' => $this->get_text_content($item_line, array('UnitOfMeasure')), 
                    'OverrideUOMSet_ListID' => $this->get_text_content($item_line, array('OverrideUOMSetRef','ListID')), 
                    'OverrideUOMSet_FullName' => $this->get_text_content($item_line, array('OverrideUOMSetRef','FullName')), 
                    'Cost' => $this->get_text_content($item_line, array('Cost')), 
                    'Amount' => $this->get_text_content($item_line, array('Amount')), 
                    'Customer_ListID' => $this->get_text_content($item_line, array('CustomerRef','ListID')), 
                    'Customer_FullName' => $this->get_text_content($item_line, array('CustomerRef','FullName')), 
                    'Class_ListID' => $this->get_text_content($item_line, array('ClassRef','ListID')), 
                    'Class_FullName' => $this->get_text_content($item_line, array('ClassRef','FullName')), 
                    'BillableStatus' => $this->get_text_content($item_line, array('BillableStatus')), 
                  );
                }
              }


             if( $ItemGroupLineRet ) {
                foreach($ItemGroupLineRet as $itemgroup_line) {
                  $ItemGroupLineItems[] = array(
                    'SortOrder' => $this->get_text_content($item_line, array('SortOrder')), 
                    'TxnLineID' => $this->get_text_content($item_line, array('TxnLineID')), 
                    'ItemGroup_ListID' => $this->get_text_content($item_line, array('ItemGroupRef','ListID')), 
                    'ItemGroup_FullName' => $this->get_text_content($item_line, array('ItemGroupRef','FullName')), 
                    'Descrip' => $this->get_text_content($item_line, array('Descrip')), 
                    'Quantity' => $this->get_text_content($item_line, array('Quantity')), 
                    'UnitOfMeasure' => $this->get_text_content($item_line, array('UnitOfMeasure')), 
                    'OverrideUOMSet_ListID' => $this->get_text_content($item_line, array('OverrideUOMSetRef','ListID')), 
                    'OverrideUOMSet_FullName' => $this->get_text_content($item_line, array('OverrideUOMSetRef','FullName')), 
                    'TotalAmount' => $this->get_text_content($item_line, array('TotalAmount')),
                  );
                }
              }

              $this->items[] = array(
                'TxnID' => $this->get_text_content($item, 'TxnID'), 
                'TimeCreated' => $this->get_text_content($item, 'TimeCreated'), 
                'TimeModified' => $this->get_text_content($item, 'TimeModified'), 
                'EditSequence' => $this->get_text_content($item, 'EditSequence'), 
                'TxnNumber' => $this->get_text_content($item, 'TxnNumber'), 
                'Account_ListID' => $this->get_text_content($item, array('AccountRef','ListID')), 
                'Account_FullName' => $this->get_text_content($item, array('AccountRef','FullName')), 
                'PayeeEntity_ListID' => $this->get_text_content($item, array('PayeeEntityRef','ListID')), 
                'PayeeEntity_FullName' => $this->get_text_content($item, array('PayeeEntityRef','FullName')), 
                'Currency_ListID' => $this->get_text_content($item, array('CurrencyRef','ListID')), 
                'Currency_FullName' => $this->get_text_content($item, array('CurrencyRef','FullName')), 
                'RefNumber' => $this->get_text_content($item, 'RefNumber'), 
                'TxnDate' => $this->get_text_content($item, 'TxnDate'), 
                'Amount' => $this->get_text_content($item, 'Amount'), 
                'Memo' => $this->get_text_content($item, 'Memo'), 
                'Address_Addr1' => $this->get_text_content($item, array('Address','Addr1')), 
                'Address_Addr2' => $this->get_text_content($item, array('Address','Addr2')), 
                'Address_Addr3' => $this->get_text_content($item, array('Address','Addr3')), 
                'Address_Addr4' => $this->get_text_content($item, array('Address','Addr4')), 
                'Address_Addr5' => $this->get_text_content($item, array('Address','Addr5')), 
                'Address_City' => $this->get_text_content($item, array('Address','City')), 
                'Address_State' => $this->get_text_content($item, array('Address','State')), 
                'Address_PostalCode' => $this->get_text_content($item, array('Address','PostalCode')), 
                'Address_Country' => $this->get_text_content($item, array('Address','Country')), 
                'Address_Note' => $this->get_text_content($item, array('Address','Note')), 
                'AddressBlock_Addr1' => $this->get_text_content($item, array('AddressBlock','Addr1')), 
                'AddressBlock_Addr2' => $this->get_text_content($item, array('AddressBlock','Addr2')), 
                'AddressBlock_Addr3' => $this->get_text_content($item, array('AddressBlock','Addr3')), 
                'AddressBlock_Addr4' => $this->get_text_content($item, array('AddressBlock','Addr4')), 
                'AddressBlock_Addr5' => $this->get_text_content($item, array('AddressBlock','Addr5')), 
                'AddressBlock_City' => $this->get_text_content($item, array('AddressBlock','City')), 
                'AddressBlock_State' => $this->get_text_content($item, array('AddressBlock','State')), 
                'AddressBlock_PostalCode' => $this->get_text_content($item, array('AddressBlock','PostalCode')), 
                'IsToBePrinted' => $this->get_text_content($item, 'IsToBePrinted'), 
                'ExpenseLineItems' => $ExpenseLineItems,
                'ItemLineItems' => $ItemLineItems,
                'ItemGroupLineItems' => $ItemGroupLineItems,
                'DataExtItems' => $this->get_dataext_items($item, 'Check', 'TxnID'),
              );
            }
        }

    }

    public function delete($TxnID) {
        $this->CI->load->model('Qb_check_model');
        $query = new $this->CI->Qb_check_model();
        $query->setTxnid($TxnID,true);
        $query->delete();
    }
     
}

/* End of file Global_variables.php */