<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_responses {

    public function __construct()
    {
       $this->CI =& get_instance();

       // load config
       $this->CI->load->config('quickbooks_api');
       
       // load queue model
       $this->CI->load->model('Qb_api_queue_model');

       $this->CI->load->library('Qb_api_queue');

    }

    public function init($respNode, $childNode, $ticket)
    {

      switch( $respNode ) 
      {
        case 'AccountQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ACCOUNTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ACCOUNTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'AccountQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_account');
            $query = new $this->CI->qb_api_xml_account();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BillingRateQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BILLINGRATEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLINGRATEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BillingRateQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_billingrate');
            $query = new $this->CI->qb_api_xml_billingrate();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BillPaymentCheckQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BILLPAYMENTCHECKQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLPAYMENTCHECKQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BillPaymentCheckQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_billpaymentcheck');
            $query = new $this->CI->qb_api_xml_billpaymentcheck();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BillPaymentCreditCardQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BILLPAYMENTCREDITCARDQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLPAYMENTCREDITCARDQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BillPaymentCreditCardQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_billpaymentcreditcard');
            $query = new $this->CI->qb_api_xml_billpaymentcreditcard();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BillQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BILLQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BillQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_bill');
            $query = new $this->CI->qb_api_xml_bill();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BillToPayQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BILLTOPAYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BILLTOPAYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BillToPayQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_billtopay');
            $query = new $this->CI->qb_api_xml_billtopay();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'BuildAssemblyQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_BUILDASSEMBLYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_BUILDASSEMBLYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'BuildAssemblyQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_buildassembly');
            $query = new $this->CI->qb_api_xml_buildassembly();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ChargeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CHARGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CHARGEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ChargeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_charge');
            $query = new $this->CI->qb_api_xml_charge();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CheckQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CHECKQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CHECKQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CheckQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_check');
            $query = new $this->CI->qb_api_xml_check();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ClassQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CLASSQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CLASSQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ClassQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_class');
            $query = new $this->CI->qb_api_xml_class();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CompanyQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_COMPANYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_COMPANYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CompanyQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_company');
            $query = new $this->CI->qb_api_xml_company();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CreditCardChargeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CREDITCARDCHARGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CREDITCARDCHARGEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CreditCardChargeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_creditcardcharge');
            $query = new $this->CI->qb_api_xml_creditcardcharge();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CreditCardCreditQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CREDITCARDCREDITQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CREDITCARDCREDITQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CreditCardCreditQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_creditcardcredit');
            $query = new $this->CI->qb_api_xml_creditcardcredit();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CreditMemoQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CREDITMEMOQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CREDITMEMOQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CreditMemoQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_creditmemo');
            $query = new $this->CI->qb_api_xml_creditmemo();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CurrencyQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CURRENCYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CURRENCYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CurrencyQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_currency');
            $query = new $this->CI->qb_api_xml_currency();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CustomerMsgQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CUSTOMERMSGQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CUSTOMERMSGQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CustomerMsgQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_customermsg');
            $query = new $this->CI->qb_api_xml_customermsg();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CustomerQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CUSTOMERQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CUSTOMERQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CustomerQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_customer');
            $query = new $this->CI->qb_api_xml_customer();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'CustomerTypeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_CUSTOMERTYPEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_CUSTOMERTYPEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'CustomerTypeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_customertype');
            $query = new $this->CI->qb_api_xml_customertype();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'DataExtDefQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_DATAEXTDEFQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_DATAEXTDEFQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'DataExtDefQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_dataextdef');
            $query = new $this->CI->qb_api_xml_dataextdef();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'DateDrivenTermsQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_DATEDRIVENTERMSQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_DATEDRIVENTERMSQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'DateDrivenTermsQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_datedriventerms');
            $query = new $this->CI->qb_api_xml_datedriventerms();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'DepositQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_DEPOSITQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_DEPOSITQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'DepositQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_deposit');
            $query = new $this->CI->qb_api_xml_deposit();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'EmployeeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_EMPLOYEEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_EMPLOYEEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'EmployeeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_employee');
            $query = new $this->CI->qb_api_xml_employee();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'EstimateQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ESTIMATEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ESTIMATEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'EstimateQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_estimate');
            $query = new $this->CI->qb_api_xml_estimate();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'HostQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_HOSTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_HOSTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'HostQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_host');
            $query = new $this->CI->qb_api_xml_host();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'InventoryAdjustmentQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_INVENTORYADJUSTMENTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_INVENTORYADJUSTMENTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'InventoryAdjustmentQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_inventoryadjustment');
            $query = new $this->CI->qb_api_xml_inventoryadjustment();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'InvoiceQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_INVOICEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_INVOICEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'InvoiceQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_invoice');
            $query = new $this->CI->qb_api_xml_invoice();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemDiscountQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMDISCOUNTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMDISCOUNTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemDiscountQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemdiscount');
            $query = new $this->CI->qb_api_xml_itemdiscount();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemFixedAssetQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMFIXEDASSETQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMFIXEDASSETQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemFixedAssetQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemfixedasset');
            $query = new $this->CI->qb_api_xml_itemfixedasset();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemGroupQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMGROUPQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMGROUPQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemGroupQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemgroup');
            $query = new $this->CI->qb_api_xml_itemgroup();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemInventoryAssemblyQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMINVENTORYASSEMBLYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMINVENTORYASSEMBLYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemInventoryAssemblyQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_iteminventoryassembly');
            $query = new $this->CI->qb_api_xml_iteminventoryassembly();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemInventoryQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMINVENTORYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMINVENTORYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemInventoryQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_iteminventory');
            $query = new $this->CI->qb_api_xml_iteminventory();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemNonInventoryQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMNONINVENTORYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMNONINVENTORYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemNonInventoryQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemnoninventory');
            $query = new $this->CI->qb_api_xml_itemnoninventory();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemOtherChargeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMOTHERCHARGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMOTHERCHARGEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemOtherChargeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemothercharge');
            $query = new $this->CI->qb_api_xml_itemothercharge();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemPaymentQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMPAYMENTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMPAYMENTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemPaymentQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itempayment');
            $query = new $this->CI->qb_api_xml_itempayment();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemReceiptQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMRECEIPTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMRECEIPTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemReceiptQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemreceipt');
            $query = new $this->CI->qb_api_xml_itemreceipt();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemSalesTaxQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMSALESTAXQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMSALESTAXQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemSalesTaxQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemsalestax');
            $query = new $this->CI->qb_api_xml_itemsalestax();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemServiceQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMSERVICEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMSERVICEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemServiceQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemservice');
            $query = new $this->CI->qb_api_xml_itemservice();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ItemSubtotalQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_ITEMSUBTOTALQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMSUBTOTALQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ItemSubtotalQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_itemsubtotal');
            $query = new $this->CI->qb_api_xml_itemsubtotal();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'JobTypeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_JOBTYPEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_JOBTYPEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'JobTypeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_jobtype');
            $query = new $this->CI->qb_api_xml_jobtype();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'JournalEntryQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_JOURNALENTRYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_JOURNALENTRYQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'JournalEntryQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_journalentry');
            $query = new $this->CI->qb_api_xml_journalentry();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'PaymentMethodQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_PAYMENTMETHODQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PAYMENTMETHODQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'PaymentMethodQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_paymentmethod');
            $query = new $this->CI->qb_api_xml_paymentmethod();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'PayrollItemWageQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_PAYROLLITEMWAGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PAYROLLITEMWAGEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'PayrollItemWageQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_payrollitemwage');
            $query = new $this->CI->qb_api_xml_payrollitemwage();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'PreferencesQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_PREFERENCESQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PREFERENCESQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'PreferencesQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_preferences');
            $query = new $this->CI->qb_api_xml_preferences();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'PriceLevelQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_PRICELEVELQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PRICELEVELQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'PriceLevelQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_pricelevel');
            $query = new $this->CI->qb_api_xml_pricelevel();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'PurchaseOrderQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_PURCHASEORDERQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_PURCHASEORDERQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'PurchaseOrderQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_purchaseorder');
            $query = new $this->CI->qb_api_xml_purchaseorder();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ReceivePaymentQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_RECEIVEPAYMENTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_RECEIVEPAYMENTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ReceivePaymentQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_receivepayment');
            $query = new $this->CI->qb_api_xml_receivepayment();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'SalesOrderQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_SALESORDERQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SALESORDERQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'SalesOrderQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_salesorder');
            $query = new $this->CI->qb_api_xml_salesorder();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'SalesReceiptQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_SALESRECEIPTQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SALESRECEIPTQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'SalesReceiptQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_salesreceipt');
            $query = new $this->CI->qb_api_xml_salesreceipt();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'SalesRepQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_SALESREPQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SALESREPQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'SalesRepQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_salesrep');
            $query = new $this->CI->qb_api_xml_salesrep();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'SalesTaxCodeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_SALESTAXCODEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SALESTAXCODEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'SalesTaxCodeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_salestaxcode');
            $query = new $this->CI->qb_api_xml_salestaxcode();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'ShipMethodQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_SHIPMETHODQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_SHIPMETHODQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'ShipMethodQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_shipmethod');
            $query = new $this->CI->qb_api_xml_shipmethod();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'StandardTermsQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_STANDARDTERMSQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_STANDARDTERMSQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'StandardTermsQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_standardterms');
            $query = new $this->CI->qb_api_xml_standardterms();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'TemplateQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_TEMPLATEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_TEMPLATEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'TemplateQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_template');
            $query = new $this->CI->qb_api_xml_template();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'TermQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_TERMQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_TERMQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'TermQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_term');
            $query = new $this->CI->qb_api_xml_term();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'TimeTrackingQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_TIMETRACKINGQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_TIMETRACKINGQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'TimeTrackingQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_timetracking');
            $query = new $this->CI->qb_api_xml_timetracking();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'UnitOfMeasureSetQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_UNITOFMEASURESETQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_UNITOFMEASURESETQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'UnitOfMeasureSetQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_unitofmeasureset');
            $query = new $this->CI->qb_api_xml_unitofmeasureset();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'VehicleMileageQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_VEHICLEMILEAGEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VEHICLEMILEAGEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'VehicleMileageQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_vehiclemileage');
            $query = new $this->CI->qb_api_xml_vehiclemileage();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'VehicleQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_VEHICLEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VEHICLEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'VehicleQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_vehicle');
            $query = new $this->CI->qb_api_xml_vehicle();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'VendorCreditQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_VENDORCREDITQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VENDORCREDITQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'VendorCreditQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_vendorcredit');
            $query = new $this->CI->qb_api_xml_vendorcredit();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'VendorQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_VENDORQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VENDORQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'VendorQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_vendor');
            $query = new $this->CI->qb_api_xml_vendor();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'VendorTypeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_VENDORTYPEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_VENDORTYPEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'VendorTypeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_vendortype');
            $query = new $this->CI->qb_api_xml_vendortype();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;
        case 'WorkersCompCodeQueryRs':

            $requestID = $childNode->getAttribute('requestID');
            $iteratorID = $childNode->getAttribute('iteratorID');
            $iteratorRemainingCount = $childNode->getAttribute('iteratorRemainingCount');

            $maxresults = ($this->CI->config->item('QB_API_WORKERSCOMPCODEQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_WORKERSCOMPCODEQUERY_MAXRESULTS') : 25;
            $this->CI->qb_api_queue->insert_more_queue($requestID, 'WorkersCompCodeQuery', $ticket->nodeValue, $iteratorID, $iteratorRemainingCount, $maxresults);

            $this->CI->load->library('Qb_api_xml_workerscompcode');
            $query = new $this->CI->qb_api_xml_workerscompcode();
            $query->setRaw( $childNode->childNodes );
            $query->saveItems();

            $this->CI->qb_api_queue->delete_queue_id( $requestID );
            
        break;

        case 'ListDeletedQueryRs':
            
            foreach($childNode->childNodes as $item) {
                if( $item->nodeName == 'ListDeletedRet') {
                    $ListDelType = $item->getElementsByTagName('ListDelType');
                    $ListID = $item->getElementsByTagName('ListID');
                    switch($ListDelType->item(0)->textContent) {
                        case 'Account':
                            $this->CI->load->library('Qb_api_xml_account');
                            $query = new $this->CI->qb_api_xml_account();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'BillingRate':
                            $this->CI->load->library('Qb_api_xml_billingrate');
                            $query = new $this->CI->qb_api_xml_billingrate();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'Class':
                            $this->CI->load->library('Qb_api_xml_class');
                            $query = new $this->CI->qb_api_xml_class();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'Customer':
                            $this->CI->load->library('Qb_api_xml_customer');
                            $query = new $this->CI->qb_api_xml_customer();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'CustomerMsg':
                            $this->CI->load->library('Qb_api_xml_customermsg');
                            $query = new $this->CI->qb_api_xml_customermsg();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'CustomerType':
                            $this->CI->load->library('Qb_api_xml_customertype');
                            $query = new $this->CI->qb_api_xml_customertype();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'DateDrivenTerms':
                            $this->CI->load->library('Qb_api_xml_datedriventerms');
                            $query = new $this->CI->qb_api_xml_datedriventerms();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'Employee':
                            $this->CI->load->library('Qb_api_xml_employee');
                            $query = new $this->CI->qb_api_xml_employee();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemDiscount':
                            $this->CI->load->library('Qb_api_xml_itemdiscount');
                            $query = new $this->CI->qb_api_xml_itemdiscount();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemFixedAsset':
                            $this->CI->load->library('Qb_api_xml_itemfixedasset');
                            $query = new $this->CI->qb_api_xml_itemfixedasset();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemGroup':
                            $this->CI->load->library('Qb_api_xml_itemgroup');
                            $query = new $this->CI->qb_api_xml_itemgroup();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemInventory':
                            $this->CI->load->library('Qb_api_xml_iteminventory');
                            $query = new $this->CI->qb_api_xml_iteminventory();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemInventoryAssembly':
                            $this->CI->load->library('Qb_api_xml_iteminventoryassembly');
                            $query = new $this->CI->qb_api_xml_iteminventoryassembly();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemNonInventory':
                            $this->CI->load->library('Qb_api_xml_itemnoninventory');
                            $query = new $this->CI->qb_api_xml_itemnoninventory();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemOtherCharge':
                            $this->CI->load->library('Qb_api_xml_itemothercharge');
                            $query = new $this->CI->qb_api_xml_itemothercharge();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemPayment':
                            $this->CI->load->library('Qb_api_xml_itempayment');
                            $query = new $this->CI->qb_api_xml_itempayment();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemSalesTax':
                            $this->CI->load->library('Qb_api_xml_itemsalestax');
                            $query = new $this->CI->qb_api_xml_itemsalestax();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemSalesTaxGroup':
                            $this->CI->load->library('Qb_api_xml_itemsalestaxgroup');
                            $query = new $this->CI->qb_api_xml_itemsalestaxgroup();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemService':
                            $this->CI->load->library('Qb_api_xml_itemservice');
                            $query = new $this->CI->qb_api_xml_itemservice();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ItemSubtotal':
                            $this->CI->load->library('Qb_api_xml_itemsubtotal');
                            $query = new $this->CI->qb_api_xml_itemsubtotal();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'JobType':
                            $this->CI->load->library('Qb_api_xml_jobtype');
                            $query = new $this->CI->qb_api_xml_jobtype();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'OtherName':
                            $this->CI->load->library('Qb_api_xml_othername');
                            $query = new $this->CI->qb_api_xml_othername();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'PaymentMethod':
                            $this->CI->load->library('Qb_api_xml_paymentmethod');
                            $query = new $this->CI->qb_api_xml_paymentmethod();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'PayrollItemNonWage':
                            $this->CI->load->library('Qb_api_xml_payrollitemnonwage');
                            $query = new $this->CI->qb_api_xml_payrollitemnonwage();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'PayrollItemWage':
                            $this->CI->load->library('Qb_api_xml_payrollitemwage');
                            $query = new $this->CI->qb_api_xml_payrollitemwage();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'PriceLevel':
                            $this->CI->load->library('Qb_api_xml_pricelevel');
                            $query = new $this->CI->qb_api_xml_pricelevel();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'SalesRep':
                            $this->CI->load->library('Qb_api_xml_salesrep');
                            $query = new $this->CI->qb_api_xml_salesrep();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'SalesTaxCode':
                            $this->CI->load->library('Qb_api_xml_salestaxcode');
                            $query = new $this->CI->qb_api_xml_salestaxcode();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ShipMethod':
                            $this->CI->load->library('Qb_api_xml_shipmethod');
                            $query = new $this->CI->qb_api_xml_shipmethod();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'StandardTerms':
                            $this->CI->load->library('Qb_api_xml_standardterms');
                            $query = new $this->CI->qb_api_xml_standardterms();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'ToDo':
                            $this->CI->load->library('Qb_api_xml_todo');
                            $query = new $this->CI->qb_api_xml_todo();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'UnitOfMeasureSet':
                            $this->CI->load->library('Qb_api_xml_unitofmeasureset');
                            $query = new $this->CI->qb_api_xml_unitofmeasureset();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'Vehicle':
                            $this->CI->load->library('Qb_api_xml_vehicle');
                            $query = new $this->CI->qb_api_xml_vehicle();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'Vendor':
                            $this->CI->load->library('Qb_api_xml_vendor');
                            $query = new $this->CI->qb_api_xml_vendor();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                        case 'VendorType':
                            $this->CI->load->library('Qb_api_xml_vendortype');
                            $query = new $this->CI->qb_api_xml_vendortype();
                            $query->delete( $ListID->item(0)->textContent );
                        break;
                    }
                }
            }
            
        break;

        case 'TxnDeletedQueryRs':
            
            foreach($childNode->childNodes as $item) {
                if( $item->nodeName == 'TxnDeletedRet') {
                    $TxnDelType = $item->getElementsByTagName('TxnDelType');
                    $TxnID = $item->getElementsByTagName('TxnID');
                    switch($TxnDelType->item(0)->textContent) {
                        case 'ARRefundCreditCard':
                            $this->CI->load->library('Qb_api_xml_arrefundcreditcard');
                            $query = new $this->CI->qb_api_xml_arrefundcreditcard();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Bill':
                            $this->CI->load->library('Qb_api_xml_bill');
                            $query = new $this->CI->qb_api_xml_bill();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'BillPaymentCheck':
                            $this->CI->load->library('Qb_api_xml_billpaymentcheck');
                            $query = new $this->CI->qb_api_xml_billpaymentcheck();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'BillPaymentCreditCard':
                            $this->CI->load->library('Qb_api_xml_billpaymentcreditcard');
                            $query = new $this->CI->qb_api_xml_billpaymentcreditcard();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'BuildAssembly':
                            $this->CI->load->library('Qb_api_xml_buildassembly');
                            $query = new $this->CI->qb_api_xml_buildassembly();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Charge':
                            $this->CI->load->library('Qb_api_xml_charge');
                            $query = new $this->CI->qb_api_xml_charge();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Check':
                            $this->CI->load->library('Qb_api_xml_check');
                            $query = new $this->CI->qb_api_xml_check();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'CreditCardCharge':
                            $this->CI->load->library('Qb_api_xml_creditcardcharge');
                            $query = new $this->CI->qb_api_xml_creditcardcharge();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'CreditCardCredit':
                            $this->CI->load->library('Qb_api_xml_creditcardcredit');
                            $query = new $this->CI->qb_api_xml_creditcardcredit();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'CreditMemo':
                            $this->CI->load->library('Qb_api_xml_creditmemo');
                            $query = new $this->CI->qb_api_xml_creditmemo();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Deposit':
                            $this->CI->load->library('Qb_api_xml_deposit');
                            $query = new $this->CI->qb_api_xml_deposit();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Estimate':
                            $this->CI->load->library('Qb_api_xml_estimate');
                            $query = new $this->CI->qb_api_xml_estimate();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'InventoryAdjustment':
                            $this->CI->load->library('Qb_api_xml_inventoryadjustment');
                            $query = new $this->CI->qb_api_xml_inventoryadjustment();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'Invoice':
                            $this->CI->load->library('Qb_api_xml_invoice');
                            $query = new $this->CI->qb_api_xml_invoice();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'ItemReceipt':
                            $this->CI->load->library('Qb_api_xml_itemreceipt');
                            $query = new $this->CI->qb_api_xml_itemreceipt();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'JournalEntry':
                            $this->CI->load->library('Qb_api_xml_journalentry');
                            $query = new $this->CI->qb_api_xml_journalentry();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'PurchaseOrder':
                            $this->CI->load->library('Qb_api_xml_purchaseorder');
                            $query = new $this->CI->qb_api_xml_purchaseorder();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'ReceivePayment':
                            $this->CI->load->library('Qb_api_xml_receivepayment');
                            $query = new $this->CI->qb_api_xml_receivepayment();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'SalesOrder':
                            $this->CI->load->library('Qb_api_xml_salesorder');
                            $query = new $this->CI->qb_api_xml_salesorder();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'SalesReceipt':
                            $this->CI->load->library('Qb_api_xml_salesreceipt');
                            $query = new $this->CI->qb_api_xml_salesreceipt();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'SalesTaxPaymentCheck':
                            $this->CI->load->library('Qb_api_xml_salestaxpaymentcheck');
                            $query = new $this->CI->qb_api_xml_salestaxpaymentcheck();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'TimeTracking':
                            $this->CI->load->library('Qb_api_xml_timetracking');
                            $query = new $this->CI->qb_api_xml_timetracking();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'VehicleMileage':
                            $this->CI->load->library('Qb_api_xml_vehiclemileage');
                            $query = new $this->CI->qb_api_xml_vehiclemileage();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                        case 'VendorCredit':
                            $this->CI->load->library('Qb_api_xml_vendorcredit');
                            $query = new $this->CI->qb_api_xml_vendorcredit();
                            $query->delete( $TxnID->item(0)->textContent );
                        break;
                    }
                }
            }
            
        break;

      }

    }


}