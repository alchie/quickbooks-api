<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Qb_api_xml_iteminventory extends Qb_api_xml {
    
    protected $CI;
    protected $method;
    protected $items;

    public function __construct()
    {
       $this->CI =& get_instance();
    }


    public function getLastTimeModified() {
        $this->CI->load->model('Qb_iteminventory_model');
        $model = new $this->CI->Qb_iteminventory_model();
        $model->set_order('TimeModified', 'DESC');
        $model_data = $model->get();
        return (($model_data) && isset($model_data->TimeModified)) ? $model_data->TimeModified : false;
    }

    public function request( $queue ) {

      $continueOnError = (isset($this->continueOnError) && ($this->continueOnError)) ? 'continueOnError' : 'stopOnError';
      $requestID = $queue->id;
      $options = (isset($queue->options) && ($queue->options)) ? json_decode($queue->options) : false;
      $iteratorID = ($options && (isset($options->iteratorID))) ? $options->iteratorID : false;
      $iterator = (isset($iteratorID) && ($iteratorID!='')) ? 'Continue' : 'Start';
      $iteratorID_attr = ($iteratorID!="") ? 'iteratorID="'.$iteratorID.'"' : '';
      $maxresults = ($this->CI->config->item('QB_API_ITEMINVENTORYQUERY_MAXRESULTS')) ? $this->CI->config->item('QB_API_ITEMINVENTORYQUERY_MAXRESULTS') : 25;
      $FromModifiedDate = ($options && (isset($options->TimeModified)) && ($options->TimeModified)) ? date( 'Y-m-d\TH:i:s', strtotime($options->TimeModified) ) : '1994-02-09T14:10:35';

$xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n" .
'<?qbxml version="' . $this->CI->config->item('QB_XML_VERSION') . '"?>' . "\n" .
'<QBXML>' . "\n" .
'<QBXMLMsgsRq onError="'.$continueOnError.'">' . "\n" .
'<ItemInventoryQueryRq requestID="'.$requestID.'"  iterator="' . $iterator . '" ' . $iteratorID_attr . '>' .  "\n" .
'<MaxReturned>'.$maxresults.'</MaxReturned>' .  "\n" .
'<ActiveStatus>All</ActiveStatus>' .  "\n" .
'<FromModifiedDate>'.$FromModifiedDate.'</FromModifiedDate>' .  "\n" .
'<OwnerID>0</OwnerID>' .  "\n" .
'</ItemInventoryQueryRq>' .  "\n" .
'<ListDeletedQueryRq requestID="'.$requestID.'">' .  "\n" .
'<ListDelType>ItemInventory</ListDelType>' .  "\n" .
'<DeletedDateRangeFilter>' .  "\n" .
'<FromDeletedDate>'.$FromModifiedDate.'</FromDeletedDate>' .  "\n" .
'</DeletedDateRangeFilter>' .  "\n" .
'</ListDeletedQueryRq>' .  "\n" .
'</QBXMLMsgsRq>' .  "\n" .
'</QBXML>';

      return $xml;

    }

    public function setRaw($data) {
        $this->data = $data;
    }

    public function saveItems() {
        
        $this->_populate_items();

        //php_error_log($this->items, 'IteminventoryRet saveItems');

        if( $this->items ) {

            $this->CI->load->model('Qb_iteminventory_model');

            foreach($this->items as $item) {

                $item_obj = (object) $item;

                $model = new $this->CI->Qb_iteminventory_model();
                $model->setListid($item_obj->ListID,TRUE);
                $model->setTimecreated($item_obj->TimeCreated);
                $model->setTimemodified($item_obj->TimeModified);
                $model->setEditsequence($item_obj->EditSequence);
                $model->setName($item_obj->Name);
                $model->setFullname($item_obj->FullName);
                $model->setIsactive(($item_obj->IsActive=='true')?1:0);
                $model->setParentListid($item_obj->Parent_ListID);
                $model->setParentFullname($item_obj->Parent_FullName);
                $model->setSublevel($item_obj->Sublevel);
                $model->setManufacturerpartnumber($item_obj->ManufacturerPartNumber);
                $model->setUnitofmeasuresetListid($item_obj->UnitOfMeasureSet_ListID);
                $model->setUnitofmeasuresetFullname($item_obj->UnitOfMeasureSet_FullName);
                $model->setSalestaxcodeListid($item_obj->SalesTaxCode_ListID);
                $model->setSalestaxcodeFullname($item_obj->SalesTaxCode_FullName);
                $model->setSalesdesc($item_obj->SalesDesc);
                $model->setSalesprice($item_obj->SalesPrice);
                $model->setIncomeaccountListid($item_obj->IncomeAccount_ListID);
                $model->setIncomeaccountFullname($item_obj->IncomeAccount_FullName);
                $model->setPurchasedesc($item_obj->PurchaseDesc);
                $model->setPurchasecost($item_obj->PurchaseCost);
                $model->setCogsaccountListid($item_obj->COGSAccount_ListID);
                $model->setCogsaccountFullname($item_obj->COGSAccount_FullName);
                $model->setPrefvendorListid($item_obj->PrefVendor_ListID);
                $model->setPrefvendorFullname($item_obj->PrefVendor_FullName);
                $model->setAssetaccountListid($item_obj->AssetAccount_ListID);
                $model->setAssetaccountFullname($item_obj->AssetAccount_FullName);
                $model->setReorderpoint($item_obj->ReorderPoint);
                $model->setQuantityonhand($item_obj->QuantityOnHand);
                $model->setAveragecost($item_obj->AverageCost);
                $model->setQuantityonorder($item_obj->QuantityOnOrder);
                $model->setQuantityonsalesorder($item_obj->QuantityOnSalesOrder);

                if( $model->nonEmpty() ) {
                  $model->update();
                } else {
                  $model->insert();
                }

                $this->insert_dataext_items($item_obj);

            }
        }

    }

    protected function _populate_items() {
      foreach($this->data as $item) {
        if( $item->nodeName == 'ItemInventoryRet') {

          $this->items[] = array(
            'ListID' => $this->get_text_content($item, array('ListID')),
            'TimeCreated' => $this->get_text_content($item, array('TimeCreated')),
            'TimeModified' => $this->get_text_content($item, array('TimeModified')),
            'EditSequence' => $this->get_text_content($item, array('EditSequence')),
            'Name' => $this->get_text_content($item, array('Name')),
            'FullName' => $this->get_text_content($item, array('FullName')),
            'IsActive' => (($this->get_text_content($item, 'IsActive')=='true') ? 1 : 0),
            'Parent_ListID' => $this->get_text_content($item, array('ParentRef','ListID')),
            'Parent_FullName' => $this->get_text_content($item, array('ParentRef','FullName')),
            'Sublevel' => $this->get_text_content($item, array('Sublevel')),
            'ManufacturerPartNumber' => $this->get_text_content($item, array('ManufacturerPartNumber')),
            'UnitOfMeasureSet_ListID' => $this->get_text_content($item, array('UnitOfMeasureSetRef','ListID')),
            'UnitOfMeasureSet_FullName' => $this->get_text_content($item, array('UnitOfMeasureSetRef','FullName')),
            'SalesTaxCode_ListID' => $this->get_text_content($item, array('SalesTaxCodeRef','ListID')),
            'SalesTaxCode_FullName' => $this->get_text_content($item, array('SalesTaxCodeRef','FullName')),
            'SalesDesc' => $this->get_text_content($item, array('SalesDesc')),
            'SalesPrice' => $this->get_text_content($item, array('SalesPrice')),
            'IncomeAccount_ListID' => $this->get_text_content($item, array('IncomeAccountRef','ListID')),
            'IncomeAccount_FullName' => $this->get_text_content($item, array('IncomeAccountRef','FullName')),
            'PurchaseDesc' => $this->get_text_content($item, array('PurchaseDesc')),
            'PurchaseCost' => $this->get_text_content($item, array('PurchaseCost')),
            'COGSAccount_ListID' => $this->get_text_content($item, array('COGSAccountRef','ListID')),
            'COGSAccount_FullName' => $this->get_text_content($item, array('COGSAccountRef','FullName')),
            'PrefVendor_ListID' => $this->get_text_content($item, array('PrefVendorRef','ListID')),
            'PrefVendor_FullName' => $this->get_text_content($item, array('PrefVendorRef','FullName')),
            'AssetAccount_ListID' => $this->get_text_content($item, array('AssetAccountRef','ListID')),
            'AssetAccount_FullName' => $this->get_text_content($item, array('AssetAccountRef','FullName')),
            'ReorderPoint' => $this->get_text_content($item, array('ReorderPoint')),
            'QuantityOnHand' => $this->get_text_content($item, array('QuantityOnHand')),
            'AverageCost' => $this->get_text_content($item, array('AverageCost')),
            'QuantityOnOrder' => $this->get_text_content($item, array('QuantityOnOrder')),
            'QuantityOnSalesOrder' => $this->get_text_content($item, array('QuantityOnSalesOrder')),
            'DataExtItems' => $this->get_dataext_items($item, 'ItemInventory', 'ListID'),
          );

        }
      }
    }

    public function delete($ListID) {
        $this->CI->load->model('Qb_iteminventory_model');
        $query = new $this->CI->Qb_iteminventory_model();
        $query->setListid($ListID,true);
        $query->delete();
    }
    
}

/* End of file */
