<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
        
        public function __construct()
        {
            parent::__construct();

            if( !$this->session->userdata('account') ) {
            	redirect("account/login");
            }

            $this->template_data->set('current_account', $this->session->userdata('account'));

            $this->template_data->set('page_title', 'Quickbooks Support');
            $this->template_data->set('current_uri', 'dashboard');
        }
}