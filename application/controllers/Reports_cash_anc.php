<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_cash_anc extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'reports');

		$this->load->model('reports/Cash_report_model');
		$this->load->model('reports/Cash_report_meta_model');
		$this->load->model('reports/Cash_report_collections_model');
		$this->load->model('reports/Cash_report_expenses_model');
		
	}

    private function _next_report($id) {
        $next = new $this->Cash_report_model('cr', 'reports');

            $where = new $this->Cash_report_model('w', 'reports');
            $where->set_select('MIN(w.id)');
            $where->set_where("w.id > " . $id);
            $where->set_limit(1);

        $next->set_limit(1);
        //$next->set_select("cr.id");
        $next->set_where('cr.id = ('. $where->get_compiled_select() . ')');
        return $next->get();
    }

    private function _previous_report($id) {
        $previous = new $this->Cash_report_model('cr', 'reports');

            $where = new $this->Cash_report_model('w', 'reports');
            $where->set_select('MAX(w.id)');
            $where->set_where("w.id < " . $id);
            $where->set_limit(1);

        $previous->set_limit(1);
        //$previous->set_select("cr.id");
        $previous->set_where('cr.id = ('. $where->get_compiled_select() . ')');
        return $previous->get();
    }

	public function create_new() {

		$recent = new $this->Cash_report_model('cr', 'reports');
		$recent->set_order('id', 'DESC');
		$recent->set_select('cr.*');
		
	
		$rReceipts = new $this->Cash_report_collections_model('rc', 'reports');
		$rReceipts->set_join('qb_salesreceipt qbs', 'qbs.TxnID=rc.receipt_id', NULL, $this->db->database);
		$rReceipts->set_limit(0);
		$rReceipts->set_select("SUM(qbs.Subtotal)");
		$rReceipts->set_where("rc.report_id=cr.id");
		$rReceipts->set_where("rc.fund='operations'");
		$recent->set_select('('.$rReceipts->get_compiled_select().') as operational_collections');
		

		$rReceipts = new $this->Cash_report_collections_model('rc', 'reports');
		$rReceipts->set_join('qb_salesreceipt qbs', 'qbs.TxnID=rc.receipt_id', NULL, $this->db->database);
		$rReceipts->set_limit(0);
		$rReceipts->set_select("SUM(qbs.Subtotal)");
		$rReceipts->set_where("rc.report_id=cr.id");
		$rReceipts->set_where("rc.fund='fundraising'");
		$recent->set_select('('.$rReceipts->get_compiled_select().') as fundraising_collections');

		$rReceipts = new $this->Cash_report_collections_model('rc', 'reports');
		$rReceipts->set_join('qb_salesreceipt qbs', 'qbs.TxnID=rc.receipt_id', NULL, $this->db->database);
		$rReceipts->set_limit(0);
		$rReceipts->set_select("SUM(qbs.Subtotal)");
		$rReceipts->set_where("rc.report_id=cr.id");
		$rReceipts->set_where("rc.fund='investment'");
		$recent->set_select('('.$rReceipts->get_compiled_select().') as investment_collections');

		$rExpenses = new $this->Cash_report_expenses_model('re', 'reports');
		$rExpenses->set_join('qb_check qbc', 'qbc.TxnID=re.check_id', NULL, $this->db->database);
		$rExpenses->set_limit(0);
		$rExpenses->set_select("SUM(qbc.Amount)");
		$rExpenses->set_where("re.report_id=cr.id");
		$rExpenses->set_where("re.fund='operations'");
		$recent->set_select('('.$rExpenses->get_compiled_select().') as operational_expenses');

		$rExpenses = new $this->Cash_report_expenses_model('re', 'reports');
		$rExpenses->set_join('qb_check qbc', 'qbc.TxnID=re.check_id', NULL, $this->db->database);
		$rExpenses->set_limit(0);
		$rExpenses->set_select("SUM(qbc.Amount)");
		$rExpenses->set_where("re.report_id=cr.id");
		$rExpenses->set_where("re.fund='fundraising'");
		$recent->set_select('('.$rExpenses->get_compiled_select().') as fundraising_expenses');

		$rExpenses = new $this->Cash_report_expenses_model('re', 'reports');
		$rExpenses->set_join('qb_check qbc', 'qbc.TxnID=re.check_id', NULL, $this->db->database);
		$rExpenses->set_limit(0);
		$rExpenses->set_select("SUM(qbc.Amount)");
		$rExpenses->set_where("re.report_id=cr.id");
		$rExpenses->set_where("re.fund='investment'");
		$recent->set_select('('.$rExpenses->get_compiled_select().') as investment_expenses');

		$recent_data = $recent->get();

		$cash_reports = new $this->Cash_report_model('cr', 'reports');
		
		$beg_date = ($recent_data) ? $recent_data->end_date : date('Y-m-d');
		$cash_reports->setBegDate( $beg_date );
		$cash_reports->setEndDate(date("Y-m-d"));
		$cash_reports->setBegOperations( (($recent_data->beg_operations + $recent_data->operational_collections) - $recent_data->operational_expenses) );
		$cash_reports->setBegFundraising( (($recent_data->beg_fundraising + $recent_data->fundraising_collections) - $recent_data->fundraising_expenses) );
		$cash_reports->setBegInvestments( (($recent_data->beg_investments + $recent_data->investment_collections) - $recent_data->investment_expenses) );
		
		$cash_reports->insert();

		redirect("reports_cash_anc/view_report/" . $cash_reports->get_inserted_id());
	}

	public function index($start=0)
	{
		$cash_reports = new $this->Cash_report_model('cr', 'reports');
		$cash_reports->set_order('id', 'DESC');
		$cash_reports->set_start($start);
		$this->template_data->set('cash_reports', $cash_reports->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_cash_anc/index"),
			'total_rows' => $cash_reports->count_all_results(),
			'per_page' => $cash_reports->get_limit(),
		)));

		$this->load->view('reports/reports_cash_anc/reports_cash_anc', $this->template_data->get_data());
	}

	public function view_report($id)
	{

		$cash_reports = new $this->Cash_report_model('cr', 'reports');
		$cash_reports->setId($id,true);
		$this->template_data->set('cash_report', $cash_reports->get());

		$rReceipts = new $this->Cash_report_collections_model('rc', 'reports');
		$rReceipts->set_join('qb_salesreceipt qbs', 'qbs.TxnID=rc.receipt_id', NULL, $this->db->database);
		$rReceipts->setReportId($id,true);
		$rReceipts->set_order('rc.postdated', 'ASC');
		$rReceipts->set_order('qbs.TxnDate', 'ASC');
		$rReceipts->set_order('qbs.RefNumber', 'ASC');
		$rReceipts->set_limit(0);
		$rReceipts->set_select("*");
		$rReceipts->set_select("rc.id as rc_id");
		$this->template_data->set('collections', $rReceipts->populate());

		$rExpenses = new $this->Cash_report_expenses_model('re', 'reports');
		$rExpenses->setReportId($id,true);
		$rExpenses->set_join('qb_check qbc', 'qbc.TxnID=re.check_id', NULL, $this->db->database);
		$rExpenses->set_order('qbc.TxnDate', 'ASC');
		$rExpenses->set_order('qbc.RefNumber', 'ASC');
		$rExpenses->set_limit(0);
		$rExpenses->set_select("*");
		$rExpenses->set_select("re.id as re_id");
		$this->template_data->set('expenses', $rExpenses->populate());

		$report_metas = new $this->Cash_report_meta_model('cm', 'reports');
		$report_metas->setCrId($id,true);
		$this->template_data->set('report_metas', $report_metas->populate());

		$this->template_data->set('next_report', $this->_next_report($id));
		$this->template_data->set('previous_report', $this->_previous_report($id));

		$this->load->view('reports/reports_cash_anc/view_report', $this->template_data->get_data());
	}

	public function hide_operations($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowOperations('0',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function hide_fundraising($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowFundraising('0',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function hide_investments($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowInvestments('0',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function hide_total($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowTotal('0',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function show_operations($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowOperations('1',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function show_fundraising($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowFundraising('1',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function show_investments($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowInvestments('1',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function set_project_dumpsite($id,$display='1') {
		$cash_reports = new $this->Cash_report_meta_model(NULL, 'reports');
		$cash_reports->setCrId($id,true);
		$cash_reports->setMetaKey('project_dumpsite_display',true);
		$cash_reports->setMetaValue($display);
		if( $cash_reports->nonEmpty() ) {
			$cash_reports->update();
		} else {
			$cash_reports->insert();
		}
		redirect($this->input->get('back'));
	}

	public function show_total($id) {
		$cash_reports = new $this->Cash_report_model(NULL, 'reports');
		$cash_reports->setId($id,true,false);
		$cash_reports->setShowTotal('1',false,true);
		$cash_reports->update();
		redirect($this->input->get('back'));
	}

	public function edit_meta($id, $field) {
		$this->template_data->set('field', $field);
		$cash_reports = new $this->Cash_report_meta_model(NULL, 'reports');
		$cash_reports->setCrId($id,true);
		$cash_reports->setMetaKey($field,true);
		if( $this->input->post('field_value') ) {
			$cash_reports->setMetaValue( $this->input->post('field_value') );
			if( $cash_reports->nonEmpty() ) {
				$cash_reports->update();
			} else {
				$cash_reports->insert();
			}
			redirect("reports_cash_anc/view_report/{$id}");
		}
		$this->template_data->set('cash_report', $cash_reports->get());
		$this->load->view('reports/reports_cash_anc/edit_meta', $this->template_data->get_data());
	}

	public function edit_field($id, $field) {
		$this->template_data->set('field', $field);

		$cash_reports = new $this->Cash_report_model('cr', 'reports');
		$cash_reports->setId($id,true);
		if( $this->input->post('field_value') ) {
			switch ($field) {
				case 'beg_date':
					$field_value = $this->input->post('field_value');
					$cash_reports->setBegDate($field_value,false,true);
					break;
				case 'beg_operations':
					$field_value = str_replace(",", "", $this->input->post('field_value'));
					$cash_reports->setBegOperations($field_value,false,true);
					break;
				case 'beg_investments':
					$field_value = str_replace(",", "", $this->input->post('field_value'));
					$cash_reports->setBegInvestments($field_value,false,true);
					break;
				case 'beg_fundraising':
					$field_value = str_replace(",", "", $this->input->post('field_value'));
					$cash_reports->setBegFundraising($field_value,false,true);
					break;
				case 'end_date':
					$cash_reports->setEndDate($this->input->post('field_value'),false,true);
					break;
			}
			$cash_reports->update();
			redirect("reports_cash_anc/view_report/{$id}");
		}
		$this->template_data->set('cash_report', $cash_reports->get());

		$this->load->view('reports/reports_cash_anc/edit_field', $this->template_data->get_data());
	}

	public function add_collections($id,$start=0) {

		if( $this->input->post('fund') ) {
			foreach($this->input->post('fund') as $rId => $fund) {
				if( $fund == '' ) {
					continue;
				}
				$rReceipts = new $this->Cash_report_collections_model(NULL, 'reports');
				$rReceipts->setReportId($id, true);
				$rReceipts->setReceiptId($rId, true);
				$rReceipts->setType( 'salesreceipt' );
				$rReceipts->setFund( $fund );
				$postdated = (isset($this->input->post('postdated')[$rId])) ? 1 : 0;
				$rReceipts->setPostdated( $postdated );
				if( $rReceipts->nonEmpty() ) {
					$rReceipts->update();
				} else {
					$rReceipts->insert();
				}
			}
			redirect( $this->input->get('back') );
		}

		$this->load->model('Qb_salesreceipt_model');
		$receipts = new $this->Qb_salesreceipt_model('s');
		$receipts->set_select("s.*");
		$db = $this->load->database( 'reports',TRUE,TRUE);
		$receipts->set_select("(SELECT COUNT(*) FROM {$db->database}.cash_report_collections crc WHERE crc.receipt_id=s.TxnID) as sCount");
		$receipts->set_order('TxnDate', 'DESC');
		$receipts->set_order('TxnNumber', 'DESC');
		$receipts->set_limit(10);
		$receipts->set_start($start);
		
		$receipts->set_where('s.TxnDate <= "'.date('Y-m-d').'"');
		$receipts->set_where("((SELECT COUNT(*) FROM {$db->database}.cash_report_collections crc WHERE crc.receipt_id=s.TxnID) = 0)");

		$this->template_data->set('receipts', $receipts->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_cash_anc/add_collections/{$id}"),
			'total_rows' => $receipts->count_all_results(),
			'per_page' => $receipts->get_limit(),
		), '?back=' . $this->input->get('back') ));

		$this->load->view('reports/reports_cash_anc/add_collections', $this->template_data->get_data());
	}

	public function edit_collection($id) {
		$rReceipts = new $this->Cash_report_collections_model(NULL, 'reports');
		$rReceipts->setId($id,true);	

		if( $this->input->post() ) {
				$rReceipts->setFund( $this->input->post('fund'), false,true );
				$postdated = ($this->input->post('postdated')) ? 1 : 0;
				$rReceipts->setPostdated( $postdated, false,true );
				$rReceipts->update();
			redirect( $this->input->get('back') );
		}

		$rReceipts_data = $rReceipts->get();

		$this->template_data->set('current_data', $rReceipts_data);

		$this->load->model('Qb_salesreceipt_model');
		$receipts = new $this->Qb_salesreceipt_model('s');
		$receipts->setTxnid($rReceipts_data->receipt_id,true);
		$this->template_data->set('current_receipt', $receipts->get());

		$this->load->view('reports/reports_cash_anc/edit_collection', $this->template_data->get_data());

	}

	public function remove_collection($id) {
		$rReceipts = new $this->Cash_report_collections_model(NULL, 'reports');
		$rReceipts->setId($id,true);
		$rReceipts->delete();
		redirect( $this->input->get('back') );
	}

	public function remove_expense($id) {
		$rExpenses = new $this->Cash_report_expenses_model(NULL, 'reports');
		$rExpenses->setId($id,true);
		$rExpenses->delete();
		redirect( $this->input->get('back') );
	}

	public function add_expenses($id,$start=0) {
		if( $this->input->post('fund') ) {
			foreach($this->input->post('fund') as $cId => $fund) {
				if( $fund == '' ) {
					continue;
				}
				$rExpenses = new $this->Cash_report_expenses_model(NULL, 'reports');
				$rExpenses->setReportId($id,true);
				$rExpenses->setCheckId($cId,true);
				$rExpenses->setType('check');
				$rExpenses->setFund( $fund );
				if( $rExpenses->nonEmpty() ) {
					$rExpenses->update();
				} else {
					$rExpenses->insert();
				}
			}
			redirect( $this->input->get('back') );
		}

		$this->load->model('Qb_check_model');
		$checks = new $this->Qb_check_model('c');
		$checks->set_select("c.*");
		$db = $this->load->database( 'reports',TRUE,TRUE);
		$checks->set_select("(SELECT COUNT(*) FROM {$db->database}.cash_report_expenses cre WHERE cre.check_id=c.TxnID) as sCount");
		$checks->set_order('TxnDate', 'DESC');
		$checks->set_order('TxnNumber', 'DESC');
		$checks->set_limit(10);
		$checks->set_start($start);

		$checks->set_where("((SELECT COUNT(*) FROM {$db->database}.cash_report_expenses cre WHERE cre.check_id=c.TxnID) = 0 )");

		$this->template_data->set('checks', $checks->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_cash_anc/add_expenses/{$id}"),
			'total_rows' => $checks->count_all_results(),
			'per_page' => $checks->get_limit(),
		), '?back=' . $this->input->get('back') ));

		$this->load->view('reports/reports_cash_anc/add_expenses', $this->template_data->get_data());
	}

	public function edit_expense($id) {
		$rExpenses = new $this->Cash_report_expenses_model(NULL, 'reports');
		$rExpenses->setId($id,true);	

		if( $this->input->post() ) {
				$rExpenses->setFund( $this->input->post('fund'),false,true );
				$rExpenses->update();
			redirect( $this->input->get('back') );
		}

		$rExpenses_data = $rExpenses->get();

		$this->template_data->set('current_data', $rExpenses_data);

		$this->load->model('Qb_check_model');
		$checks = new $this->Qb_check_model('c');
		$checks->setTxnid($rExpenses_data->check_id,true);
		$this->template_data->set('current_check', $checks->get());

		$this->load->view('reports/reports_cash_anc/edit_expense', $this->template_data->get_data());

	}

}
