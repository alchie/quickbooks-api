<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'reports');
	}
	
	public function index()
	{
		$this->load->view('reports/reports', $this->template_data->get_data());
	}

}
