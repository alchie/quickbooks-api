<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'employees');
	}

	public function index($start=0)
	{

		$this->load->model('Qb_employee_model');
		
		$employees = new $this->Qb_employee_model('c');
		$employees->set_order('c.Name', 'ASC');
		$employees->set_start($start);

		if( $this->input->get('q') ) {
			$employees->set_where('c.Name LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('employees', $employees->populate());
		$this->template_data->set('count', $employees->count_all_results());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/customers/index"),
			'total_rows' => $employees->count_all_results(),
			'per_page' => $employees->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('employees/employees', $this->template_data->get_data());
	}

	public function view($employee_id) {

		$this->load->model('Qb_employee_model');
		$employees = new $this->Qb_employee_model('c');
		$employees->setListid($employee_id,true);
		$this->template_data->set('employee', $employees->get());

		$this->load->view('employees/view', $this->template_data->get_data());
	}

}
