<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'accounts');
		$this->template_data->set('page_title', 'Chart of Accounts');
	}

	public function index()
	{

		$this->load->model('Qb_account_model');

		$accounts = new $this->Qb_account_model('c');
		//$accounts->setSubLevel('0', true);
		$accounts->setIsActive('1', true);
		$accounts->set_order('c.AccountNumber', 'ASC');
		$accounts->set_order('c.AccountType', 'ASC');
		$accounts->set_limit(0);
		$accounts_data = $accounts->populate();
		//print_r($accounts_data);
		$this->template_data->set('accounts', $accounts_data);

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/accounts/index"),
			'total_rows' => $accounts->count_all_results(),
			'per_page' => $accounts->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('accounts/accounts', $this->template_data->get_data());
	}

}
