<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pledgers extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'pledgers');

		$this->load->config('pledgers');
	}

	public function index($year=0, $start=0)
	{

		if( !$year ) {
			$year = date("Y");
		}
		$this->template_data->set('year', $year);
		$this->load->model('Qb_customer_model');
		
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setCustomertypeListid($this->config->item('pledgers_type_id'),true);
		$pledgers->set_order('c.Name', 'ASC');
		$pledgers->set_start($start);

		$pledgers->set_select('c.*');

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$january = new $this->Qb_salesreceipt_salesreceiptline_model('j');
		$january->setItemListid($this->config->item('pledgers_type_id'),true);
		$january->set_join('qb_salesreceipt s', 's.TxnID=j.SalesReceipt_TxnID');
		$january->set_where('s.Customer_ListID=c.ListID');
		$january->set_limit(1);
		$january->set_select('j.Amount as jAmount');
		$pledgers->set_select('('.$january->get_compiled_select().') as january');

		if( $this->input->get('q') ) {
			$pledgers->set_where('c.Name LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('pledgers', $pledgers->populate());
		$this->template_data->set('count', $pledgers->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/pledgers/index"),
			'total_rows' => $pledgers->count_all_results(),
			'per_page' => $pledgers->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('pledgers/pledgers', $this->template_data->get_data());
	}

	public function view($customer_id) {

		$this->load->model('Qb_customer_model');
		$pledgers = new $this->Qb_customer_model('c');
		$pledgers->setListid($customer_id,true);
		$this->template_data->set('customer', $pledgers->get());

		$this->load->view('pledgers/view', $this->template_data->get_data());
	}

}
