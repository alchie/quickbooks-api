<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlm_members extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'mlm_members');

		$this->load->config('mlm');

		$this->load->model('Qb_customer_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$this->load->model('Qb_dataext_model');

	}

	public function index($start=0)
	{

		$mlm_members = new $this->Qb_salesreceipt_salesreceiptline_model('c');
		$mlm_members->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$mlm_members->set_join('qb_salesreceipt s', 's.TxnID=c.SalesReceipt_TxnID');
		$mlm_members->set_order('parent', 'DESC');
		$mlm_members->set_order('s.Customer_FullName', 'ASC');
		$mlm_members->set_group_by('s.Customer_ListID');
		$mlm_members->set_start($start);
		$mlm_members->set_select('c.*');
		$mlm_members->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		//$mlm_members->set_select('COUNT(*) as packages');

		$mlm_members->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.EntityType= "Customer" AND d.Entity_ListID<>s.Customer_ListID AND d.DataExtValue=s.Customer_ListID AND d.DataExtName="'.$this->config->item('mlm_referral_key').'") as referrals');

		$mlm_members->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.EntityType= "Customer" AND d.Entity_ListID<>s.Customer_ListID AND d.DataExtValue=s.Customer_ListID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as downlines');
		
		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceipt" AND d.Txn_TxnID=s.TxnID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as parent');

		if( $this->input->get('show') == 'main_package') {
			//$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceipt" AND d.Txn_TxnID=s.TxnID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as parent_id');
			$mlm_members->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceipt" AND d.Txn_TxnID=s.TxnID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") IS NULL)');
		}
		$mlm_members->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$mlm_members->set_select('s.*');

		$mlm_packages = new $this->Qb_salesreceipt_salesreceiptline_model('p');
		$mlm_packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$mlm_packages->set_join('qb_salesreceipt ss', 'ss.TxnID=p.SalesReceipt_TxnID');
		$mlm_packages->set_select('COUNT(*)');
		$mlm_packages->set_limit(1);
		$mlm_packages->set_where('s.Customer_ListID=ss.Customer_ListID');
		$mlm_packages->set_where('ss.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');
		$mlm_members->set_select('('.$mlm_packages->get_compiled_select().') as packages');

		if( $this->input->get('q') ) {
			$mlm_members->set_where('s.Customer_FullName LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('mlm_members', $mlm_members->populate());
		$this->template_data->set('count', $mlm_members->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_members/index"),
			'total_rows' => $mlm_members->count_all_results(),
			'per_page' => $mlm_members->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('mlm/members/members', $this->template_data->get_data());
	}

	private function _load_customer($customer_id) {
		$customer = $this->_get_customer($customer_id);
		$this->template_data->set('customer', $customer);
		return $customer;
	}

	private function _get_customer($customer_id) {

		
		$mlm_members = new $this->Qb_customer_model('c');
		$mlm_members->setListid($customer_id,true);
		$mlm_members->set_select("c.*");
		
		$mlm_members->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID<>c.ListID AND d.DataExtValue=c.ListID AND d.DataExtName="'.$this->config->item('mlm_referral_key').'") as referrals');

		//$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_team_key').'") as team');

		$mlm_members->set_select('c.CustomerType_FullName as team');

		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_membership_network_key').'") as total_network');

		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_membership_left_network_key').'") as total_network_left');

		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_membership_right_network_key').'") as total_network_right');

		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as parent_id');

		
		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s.Customer_ListID=c.ListID)');
		$membership_fee->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$mlm_members->set_where('(('.$membership_fee->get_compiled_select().') > 0)');
		
		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_where('s.Customer_ListID=c.ListID');
		$packages->set_select('COUNT(*)');
		$packages->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');
		$mlm_members->set_select('('.$packages->get_compiled_select().') as packages');

		$signup_bonus = new $this->Qb_salesreceipt_salesreceiptline_model('p');
		$signup_bonus->setItemListid($this->config->item('mlm_signupbonus_item_id'),true);
		$signup_bonus->set_join('qb_salesreceipt ss', 'ss.TxnID=p.SalesReceipt_TxnID');
		$signup_bonus->set_select('SUM(p.Amount)');
		$signup_bonus->set_where('c.ListID=ss.Customer_ListID');
		$signup_bonus->set_where('ss.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$mlm_members->set_select('('.$signup_bonus->get_compiled_select().') as signup_bonus');

		$watch_bonus = new $this->Qb_salesreceipt_salesreceiptline_model('p');
		$watch_bonus->setItemListid($this->config->item('mlm_watchbonus_item_id'),true);
		$watch_bonus->set_join('qb_salesreceipt ss', 'ss.TxnID=p.SalesReceipt_TxnID');
		$watch_bonus->set_select('SUM(p.Amount)');
		$watch_bonus->set_where('c.ListID=ss.Customer_ListID');
		$watch_bonus->set_where('ss.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$mlm_members->set_select('('.$watch_bonus->get_compiled_select().') as watch_bonus');

		$sales_bonus = new $this->Qb_dataext_model('d');
		$sales_bonus->setDataextname($this->config->item('mlm_itemagent_key'),true);
		$sales_bonus->setDataextvalue($customer_id,true);
		$sales_bonus->setTxntype('SalesReceiptLine',true);
		$sales_bonus->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$sales_bonus->set_where('ss.Item_ListID="'.$this->config->item('mlm_salesbonus_item_id').'"');
		$sales_bonus->set_select('SUM(ss.Amount)');
		$mlm_members->set_select('('.$sales_bonus->get_compiled_select().') as sales_bonus');

		$mlm_sales_bonus = new $this->Qb_dataext_model('d');
		$mlm_sales_bonus->setDataextname($this->config->item('mlm_itemagent_key'),true);
		$mlm_sales_bonus->setDataextvalue($customer_id,true);
		$mlm_sales_bonus->setTxntype('SalesReceiptLine',true);
		$mlm_sales_bonus->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$mlm_sales_bonus->set_where('ss.Item_ListID="'.$this->config->item('mlm_mlmsalesbonus_item_id').'"');
		$mlm_sales_bonus->set_select('SUM(ss.Amount)');
		$mlm_members->set_select('('.$mlm_sales_bonus->get_compiled_select().') as mlm_sales_bonus');

		$random_bonus = new $this->Qb_dataext_model('d');
		$random_bonus->setDataextname($this->config->item('mlm_itemagent_key'),true);
		$random_bonus->setDataextvalue($customer_id,true);
		$random_bonus->setTxntype('SalesReceiptLine',true);
		$random_bonus->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$random_bonus->set_where('ss.Item_ListID="'.$this->config->item('mlm_randombonus_item_id').'"');
		$random_bonus->set_select('SUM(ss.Amount)');
		$mlm_members->set_select('('.$random_bonus->get_compiled_select().') as random_bonus');

		return $mlm_members->get();
	}

	public function summary($customer_id) {

		$customer = $this->_load_customer($customer_id);

		if( !$customer ) {
			redirect( "mlm_dashboard" );
		}

		$packages = $this->_get_packages($customer_id, 0);
		$this->template_data->set('packages', $packages);

		$this->load->view('mlm/members/member_summary', $this->template_data->get_data());
	}

	public function packages($customer_id, $start=0) {

		$customer = $this->_load_customer($customer_id);

		if( !$customer ) {
			redirect( "mlm_dashboard" );
		}

		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_where("s.Customer_ListID='{$customer_id}'");
		$packages->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');
		$packages->set_select('*');
		$packages->set_order('ss.TxnLineID', 'ASC');
		
		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_team_key').'") as team');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as parent_id');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');


		$packages->set_select('(SELECT COUNT(d.DataExtValue) FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as downlines');

		//$packages->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="SalesReceipt" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as downlines');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_network_key').'") as total_package_network');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_left_network_key').'") as total_package_network_left');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_right_network_key').'") as total_package_network_right');

		$packages->set_start($start);
		if( $this->input->get('q') ) {
			$packages->set_where('(ss.TxnLineID LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Item_FullName LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Amount LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('s.RefNumber LIKE "%'.$this->input->get('q').'%")');
		}
		$this->template_data->set('packages2', $packages->populate());
		$this->template_data->set('count', $packages->count_all_results());



		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_members/packages/{$customer_id}"),
			'total_rows' => $packages->count_all_results(),
			'per_page' => $packages->get_limit(),
			'ajax'=>true
		)));

		$packages = $this->_get_packages($customer_id, 0);
		$this->template_data->set('packages', $packages);

		$this->load->view('mlm/members/member_packages', $this->template_data->get_data());
	}

	public function package($txn_id) {

		$package = $this->_get_current_package($txn_id);
		$this->template_data->set('current_package', $package);

		$customer = $this->_load_customer($package->Customer_ListID);
		if( !$customer ) {
			redirect("mlm_members/summary/{$customer->Customer_ListID}");
		}

		$packages = $this->_get_packages($package->Customer_ListID, 0);
		$this->template_data->set('packages', $packages);
		
		$this->load->view('mlm/members/package_summary', $this->template_data->get_data());
	}

	private function _get_current_package($txn_id) {
		
		$current_package = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$current_package->setTxnlineid($txn_id,true);
		$current_package->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$current_package->set_select('ss.*');
		//$current_package->set_select('s.*');
		$current_package->set_select('s.Customer_ListID as Customer_ListID');
		$current_package->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE  d.Txn_TxnID=s.TxnID AND d.DataExtName="'.$this->config->item('mlm_team_key').'") as team');

		$current_package->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as downlines');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_network_key').'") as total_package_network');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_left_network_key').'") as total_package_network_left');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_right_network_key').'") as total_package_network_right');

		return $current_package->get();
	}

	private function _get_packages($Customer_ListID, $limit=10) {
		
		$current_package = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$current_package->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$current_package->set_select('ss.*');
		$current_package->set_limit($limit);
		$current_package->set_where('ss.Item_ListID="'.$this->config->item('mlm_package1_item_id').'"');
		$current_package->set_where('s.Customer_ListID="'.$Customer_ListID.'"');
		$current_package->set_select('s.Customer_ListID as Customer_ListID');
		$current_package->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_team_key').'") as team');

		$current_package->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="SalesReceipt" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as downlines');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_network_key').'") as total_package_network');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_left_network_key').'") as total_package_network_left');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="SalesReceiptLine" AND d.Entity_ListID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_right_network_key').'") as total_package_network_right');

		return $current_package->populate();
	}
	public function genealogy($customer_id) {
		
		$customer = $this->_load_customer($customer_id);

		if( !$customer ) {
			redirect( "mlm_dashboard" );
		}

		$sublevel_data = array();
		$sub_level_list_id = $customer_id;
		$sub_level = 1;
		if( $this->input->get('sub') ) {
			$sublevels = explode(",", $this->input->get('sub'));
			foreach($sublevels as $sub) {
				if( $sub ) {
					$sub_level_list_id = $sub;
					$sublevel_data[$sub_level] = $this->_get_customer($sub);
					$sub_level++;
				}
			}
		}
		$this->template_data->set('sublevels', $sublevel_data);


		$left_downline = new $this->Qb_dataext_model('d');
		$left_downline->setDataextname($this->config->item('mlm_parent_key'),true);
		$left_downline->setDataextvalue($sub_level_list_id,true);
		$left_downline->setTxntype('Customer',true);
		$left_downline->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$left_downline->set_select('*');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s.Customer_ListID=d.Entity_ListID)');
		$membership_fee->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$left_downline->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		//$left_downline->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_team_key').'") as team');

		$left_downline->set_select('c.CustomerType_FullName as team');

		$left_downline->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_side_key').'") = "LEFT")');

		//$left_downline->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="Customer" AND d.DataExtValue=c.ListID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as downlines');
		
		$downlines = new $this->Qb_dataext_model('d1');
		$downlines->setDataextname($this->config->item('mlm_parent_key'),true);
		$downlines->setTxntype('Customer',true);
		$downlines->set_where('d1.DataExtValue=c.ListID');
		$downlines->set_select('COUNT(*)');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s.Customer_ListID=d1.Entity_ListID)');
		$membership_fee->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$left_downline->set_select('('.$downlines->get_compiled_select().') as downlines');

		$left_downline_data = $left_downline->get();

		$this->template_data->set('left_downline', $left_downline_data);

		$right_downline = new $this->Qb_dataext_model('d');
		$right_downline->setDataextname($this->config->item('mlm_parent_key'),true);
		$right_downline->setDataextvalue($sub_level_list_id,true);
		$right_downline->setTxntype('Customer',true);
		$right_downline->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$right_downline->set_select('*');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s.Customer_ListID=d.Entity_ListID)');
		$membership_fee->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$right_downline->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		//$right_downline->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_team_key').'") as team');

		$right_downline->set_select('c.CustomerType_FullName as team');

		$right_downline->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.EntityType="Customer" AND d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_side_key').'") = "RIGHT")');

		//$right_downline->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="Customer" AND d.DataExtValue=c.ListID AND d.DataExtName="'.$this->config->item('mlm_parent_key').'") as downlines');

		$downlines = new $this->Qb_dataext_model('d1');
		$downlines->setDataextname($this->config->item('mlm_parent_key'),true);
		$downlines->setTxntype('Customer',true);
		$downlines->set_where('d1.DataExtValue=c.ListID');
		$downlines->set_select('COUNT(*)');
		
		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s.Customer_ListID=d1.Entity_ListID)');
		$membership_fee->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$right_downline->set_select('('.$downlines->get_compiled_select().') as downlines');

		$right_downline_data = $right_downline->get();

		$this->template_data->set('right_downline', $right_downline_data);

		$this->load->view('mlm/members/member_genealogy', $this->template_data->get_data());
	}

	public function referrals($customer_id,$start=0) {
		$this->_load_customer($customer_id);
		
		$packages = $this->_get_packages($customer_id, 0);
		$this->template_data->set('packages', $packages);

		
		$referrals = new $this->Qb_dataext_model('d');
		$referrals->setDataextname($this->config->item('mlm_referral_key'),true);
		$referrals->setTxntype('Customer',true);
		$referrals->setDataextvalue($customer_id,true);
		$referrals->set_join('qb_customer c', 'd.Entity_ListID=c.ListID');
		$referrals->set_start($start);
		if( $this->input->get('q') ) {
			$referrals->set_where('(c.Name LIKE "%'.$this->input->get('q').'%"');
			$referrals->set_where_or('c.ListID LIKE "%'.$this->input->get('q').'%")');
		}
		$this->template_data->set('referrals', $referrals->populate());
		$this->template_data->set('count', $referrals->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_members/referrals/{$customer_id}"),
			'total_rows' => $referrals->count_all_results(),
			'per_page' => $referrals->get_limit(),
			'ajax'=>true
		)));


		$this->load->view('mlm/members/member_referrals', $this->template_data->get_data());
	}

	public function earnings($customer_id) {
		$this->_load_customer($customer_id);
		$this->load->view('mlm/members/member_earnings', $this->template_data->get_data());
	}

	public function payouts($customer_id) {
		$this->_load_customer($customer_id);

		$packages = $this->_get_packages($customer_id, 0);
		$this->template_data->set('packages', $packages);

		$this->load->view('mlm/members/member_payouts', $this->template_data->get_data());
	}

	public function recalculate_network($customer_id,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_parent_key'),true);
		//$downlines->setDataextvalue($customer_id,true);
		$downlines->setEntitytype('Customer',true);
		$downlines->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$downlines->set_select('d.*');
		//$downlines->set_select('c.Name');
		//$downlines->set_select('COUNT(*) as downlines');
		//print_r($downlines->populate());
		$total = $downlines->recursive_count('DataExtValue', $customer_id, 'Entity_ListID', 0, 'Entity_ListID');

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_membership_network_key'),true);
		$total_network->setEntitytype('Customer',true);
		$total_network->setTxntype('Customer',true);
		$total_network->setEntityListid($customer_id,true);
		$total_network->setTxnTxnid($customer_id,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}
		$this->recalculate_left_side($customer_id,0);
		$this->recalculate_right_side($customer_id,0);
		if( $redirect ) {
			redirect("mlm_members/summary/{$customer_id}");
		}
		
	}

	public function recalculate_left_side($customer_id,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_parent_key'),true);
		//$downlines->setDataextvalue($customer_id,true);
		$downlines->setEntitytype('Customer',true);
		$downlines->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$downlines->set_select('d.*');
		//$downlines->set_select('c.Name');
		//$downlines->set_select('COUNT(*) as downlines');
		//print_r($downlines->populate());

		$downlines->set_select('(SELECT d1.DataExtValue FROM qb_dataext d1 WHERE d1.EntityType="Customer" AND d1.Entity_ListID=d.Entity_ListID AND d1.DataExtName="'.$this->config->item('mlm_side_key').'") as side');

		$total = $downlines->recursive_filter_count('DataExtValue', $customer_id, 'Entity_ListID', 0, 'Entity_ListID', 'side', 'LEFT');

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_membership_left_network_key'),true);
		$total_network->setEntitytype('Customer',true);
		$total_network->setTxntype('Customer',true);
		$total_network->setEntityListid($customer_id,true);
		$total_network->setTxnTxnid($customer_id,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}
		if( $redirect ) {
			redirect("mlm_members/summary/{$customer_id}");
		}
		
	}

	public function recalculate_right_side($customer_id,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_parent_key'),true);
		//$downlines->setDataextvalue($customer_id,true);
		$downlines->setEntitytype('Customer',true);
		$downlines->set_join('qb_customer c', 'c.ListID=d.Entity_ListID');
		$downlines->set_select('d.*');
		//$downlines->set_select('c.Name');
		//$downlines->set_select('COUNT(*) as downlines');
		//print_r($downlines->populate());

		$downlines->set_select('(SELECT d1.DataExtValue FROM qb_dataext d1 WHERE d1.EntityType="Customer" AND d1.Entity_ListID=d.Entity_ListID AND d1.DataExtName="'.$this->config->item('mlm_side_key').'") as side');

		$total = $downlines->recursive_filter_count('DataExtValue', $customer_id, 'Entity_ListID', 0, 'Entity_ListID', 'side', 'RIGHT');

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_membership_right_network_key'),true);
		$total_network->setEntitytype('Customer',true);
		$total_network->setTxntype('Customer',true);
		$total_network->setEntityListid($customer_id,true);
		$total_network->setTxnTxnid($customer_id,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}
		if( $redirect ) {
			redirect("mlm_members/summary/{$customer_id}");
		}
	}
}
