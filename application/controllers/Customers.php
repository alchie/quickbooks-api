<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'customers');
	}

	public function index($start=0)
	{

		$this->load->model('Qb_customer_model');
		
		$customers = new $this->Qb_customer_model('c');
		$customers->set_order('c.Name', 'ASC');
		$customers->set_start($start);

		if( $this->input->get('q') ) {
			$customers->set_where('c.Name LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('customers', $customers->populate());
		$this->template_data->set('count', $customers->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/customers/index"),
			'total_rows' => $customers->count_all_results(),
			'per_page' => $customers->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('customers/customers', $this->template_data->get_data());
	}

	public function view($customer_id) {

		$this->load->model('Qb_customer_model');
		$customers = new $this->Qb_customer_model('c');
		$customers->setListid($customer_id,true);
		$customer_data = $customers->get();

		if( !$customer_data ) {
			redirect("customers");
		}
		$this->template_data->set('customer', $customer_data);

		$this->load->view('customers/view', $this->template_data->get_data());
	}

}
