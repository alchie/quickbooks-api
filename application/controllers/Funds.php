<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funds extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'funds');
	}

	public function index()
	{
		$this->load->view('funds/funds', $this->template_data->get_data());
	}

}
