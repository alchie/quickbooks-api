<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Names extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_uri', 'names');
	}

	public function index($start=0)
	{
		$limit = 10;
		$sql = "SELECT * FROM (";
		$sql .= "SELECT c.ListID, c.Name, 'customer' as type FROM qb_customer c UNION ";
		$sql .= "SELECT v.ListID, v.Name, 'vendor' as type FROM qb_vendor v UNION ";
		$sql .= "SELECT e.ListID, e.Name, 'employee' as type FROM qb_employee e) n";
		if( $this->input->get('q') ) {
			$sql .= ' WHERE ( ';
			$sql .= ' (Name LIKE "%'.$this->input->get('q').'%") ';
			$sql .= ' OR (ListID LIKE "%'.$this->input->get('q').'%") ';
			$sql .= ' ) ';
		}
		$query1 = $this->db->query($sql);

		$sql .= " ORDER BY Name ASC";
		$sql .= " LIMIT {$start}, 10";
		$sql .= ";";
		
		$query2 = $this->db->query($sql);
		$results = array();
		foreach($query2->result() as $row) {
			$results[] = $row;
		}
		$this->template_data->set('names', $results);
		$this->template_data->set('count', $query1->num_rows());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/names/index"),
			'total_rows' => $query1->num_rows(),
			'per_page' => $limit,
		)));

		$this->load->view('names/names', $this->template_data->get_data());
	}

	public function view($name_id) {
		$this->load->view('names/view', $this->template_data->get_data());
	}

}
