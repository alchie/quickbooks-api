<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendors extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'vendors');
	}

	public function index($start=0)
	{

		$this->load->model('Qb_vendor_model');
		
		$vendors = new $this->Qb_vendor_model('c');
		$vendors->set_order('c.Name', 'ASC');
		$vendors->set_start($start);

		if( $this->input->get('q') ) {
			$vendors->set_where('c.Name LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('vendors', $vendors->populate());
		$this->template_data->set('count', $vendors->count_all_results());
		
		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/vendors/index"),
			'total_rows' => $vendors->count_all_results(),
			'per_page' => $vendors->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('vendors/vendors', $this->template_data->get_data());
	}

	public function view($vendor_id) {

		$this->load->model('Qb_vendor_model');
		$vendors = new $this->Qb_vendor_model('c');
		$vendors->setListid($vendor_id,true);
		$this->template_data->set('vendor', $vendors->get());

		$this->load->view('vendors/view', $this->template_data->get_data());
	}

}
