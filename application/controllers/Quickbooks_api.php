<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quickbooks_api extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('Qb_api');

		$this->template_data->set('page_title', 'Quickbooks Support');
        $this->template_data->set('current_uri', 'quickbooks_api');
	}

	public function index()
	{

		$qb_api = new $this->qb_api();
		$qb_api->handle();
	}


	
	public function qwc()
	{
		$qb_api = new $this->qb_api();
		$qb_api->qwc(
			array(
				'username' => $this->config->item('QB_XML_USERNAME'),
				
			)
		);
	}

	public function support() {

	}

	public function settings() {

		if( !$this->session->userdata('account') ) {
           	redirect("account/login");
        }

        $this->template_data->set('current_account', $this->session->userdata('account'));

		if( $this->input->post() ) {
			if( $this->input->post('print_header') ) {
				foreach($this->input->post('print_header') as $key => $value) {
					qbapi_save_settings('print_header', $key, $value);
				}
			}
			if( $this->input->post('print_footer') ) {
				foreach($this->input->post('print_footer') as $key => $value) {
					qbapi_save_settings('print_footer', $key, $value);
				}
			}
		}

		$queue_list = new $this->Qb_api_queue_model;
		$queue_list->set_order('id', 'ASC');
		$queue_list->set_limit(0);
		$this->template_data->set('queue_list', $queue_list->populate());
		$this->template_data->set('queue_count', $queue_list->count_all());

		$this->load->view('quickbooks_api/quickbooks_api_settings',  $this->template_data->get_data());
	}

	public function clear_queue() {

		if( !$this->session->userdata('account') ) {
           	redirect("account/login");
        }

		$queue_list = new $this->Qb_api_queue_model;
		$queue_list->truncate();

		redirect("quickbooks_api/settings");
	}

	public function delete_queue($id) {

		if( !$this->session->userdata('account') ) {
           	redirect("account/login");
        }

		$queue_list = new $this->Qb_api_queue_model;
		$queue_list->setId($id,true);
		$queue_list->delete();
		redirect("quickbooks_api/settings");
	}
}
