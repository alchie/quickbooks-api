<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlm_services extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'services');
		$this->load->config('mlm');

		$this->load->model('Qb_dataext_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$this->load->model('Qb_itemothercharge_model');
	}

	public function index($start=0)
	{
		$items = new $this->Qb_itemothercharge_model('i');
		$items->set_where_in('i.ListID', $this->config->item('mlm_services_items'));
		$this->template_data->set('items', $items->populate());

		$this->load->view('mlm/services/services', $this->template_data->get_data());
	}

	public function view($ListID, $start=0)
	{

		$item = new $this->Qb_itemothercharge_model('i');
		$item->setListid($ListID,true);
		$this->template_data->set('current_item', $item->get());

		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($ListID,true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_select('*');
		$packages->set_where('s.Template_ListID="'.$this->config->item('mlm_services_template_id').'"');

		if( $this->input->get('team') ) {
			$packages->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_team_key').'") = "'.$this->input->get('team').'")');
		}

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_team_key').'") as team');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_parent_key').'") as parent_id');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_side_key').'") as side');


		$downlines = new $this->Qb_dataext_model('d5');
		$downlines->setTxntype("SalesReceiptLine", true);
		$downlines->setDataextname($this->config->item('mlm_item_parent_key'),true);
		$downlines->set_where('d5.DataExtValue=ss.TxnLineID');
		$downlines->set_select('COUNT(*)');
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss5', 'ss5.TxnLineID=d5.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s5', 's5.TxnID=ss5.SalesReceipt_TxnID');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss15');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s15', 's15.TxnID=ss15.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s15.Customer_ListID=s5.Customer_ListID)');
		$membership_fee->set_where('s15.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$packages->set_select('('.$downlines->get_compiled_select().') as downlines');

		$packages->set_start($start);
		if( $this->input->get('q') ) {
			$packages->set_where('(ss.TxnLineID LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Item_FullName LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Amount LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('s.RefNumber LIKE "%'.$this->input->get('q').'%")');
		}
		//echo $packages->get_compiled_select();

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$packages->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$this->template_data->set('mlm_packages', $packages->populate());
		$this->template_data->set('count', $packages->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_services/view/{$ListID}"),
			'total_rows' => $packages->count_all_results(),
			'per_page' => $packages->get_limit(),
			'ajax'=>true
		)));
		$this->load->view('mlm/services/services_view', $this->template_data->get_data());
	}	

}
