<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Journal_entries extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'journal_entries');
	}

	public function index($start=0)
	{
		$this->load->model('Qb_journalentry_model');

		$journals = new $this->Qb_journalentry_model('c');
		$journals->set_order('c.TxnDate', 'DESC');
		$journals->set_order('c.TxnNumber', 'DESC');
		$journals->set_start($start);
		
		$this->template_data->set('journals', $journals->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/journal_entries/index"),
			'total_rows' => $journals->count_all_results(),
			'per_page' => $journals->get_limit(),
		)));

		$this->load->view('journal_entries/journal_entries',  $this->template_data->get_data());
	}

	public function details($txn_number)
	{
		$this->load->model('Qb_journalentry_model');
		$journal = new $this->Qb_journalentry_model('c');
		$journal->setTxnnumber($txn_number,true);
		$journal_data = $journal->get();
		$this->template_data->set('journal', $journal_data);

		$this->load->model('Qb_journalentry_journalcreditline_model');
		$credit = new $this->Qb_journalentry_journalcreditline_model('jc');
		$credit->setJournalentryTxnid($journal_data->TxnID,true);
		$credit->set_limit(0);
		$credit_entries = $credit->populate();
		$this->template_data->set('credit_entries', $credit_entries);


		$this->load->model('Qb_journalentry_journaldebitline_model');
		$debit = new $this->Qb_journalentry_journaldebitline_model('jc');
		$debit->setJournalentryTxnid($journal_data->TxnID,true);
		$debit->set_limit(0);
		$debit_entries = $debit->populate();
		$this->template_data->set('debit_entries', $debit_entries);


		$this->load->view('journal_entries/details',  $this->template_data->get_data());
	}
}
