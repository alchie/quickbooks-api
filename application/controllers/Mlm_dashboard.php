<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlm_dashboard extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_uri', 'mlm_dashboard');
		$this->load->config('mlm');
	}

	public function index()
	{

		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$mlm_members = new $this->Qb_salesreceipt_salesreceiptline_model('c');
		$mlm_members->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$mlm_members->set_join('qb_salesreceipt s', 's.TxnID=c.SalesReceipt_TxnID');
		$mlm_members->set_order('s.Customer_FullName', 'ASC');
		$mlm_members->set_group_by('s.Customer_ListID');
		$mlm_members->set_select('c.*');
		$mlm_members->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$this->template_data->set('total_members', $mlm_members->count_all_results());

		
		$mlm_packages = new $this->Qb_salesreceipt_salesreceiptline_model('c');
		$mlm_packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$mlm_packages->set_join('qb_salesreceipt s', 's.TxnID=c.SalesReceipt_TxnID');
		$mlm_packages->set_order('s.Customer_FullName', 'ASC');
		$mlm_packages->set_select('*');
		$mlm_packages->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss15');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s15', 's15.TxnID=ss15.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s15.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s15.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$mlm_packages->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$this->template_data->set('total_packages', $mlm_packages->count_all_results());
		
		$mlm_members = new $this->Qb_salesreceipt_salesreceiptline_model('c');
		$mlm_members->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$mlm_members->set_join('qb_salesreceipt s', 's.TxnID=c.SalesReceipt_TxnID');
		$mlm_members->set_order('s.Customer_FullName', 'ASC');
		$mlm_members->set_select('SUM(c.Amount) as total_cash_in');
		$mlm_members->set_where('s.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$this->template_data->set('total_cash_in', $mlm_members->get());

		$this->load->model('Qb_check_expenseline_model');
		$cashout = new $this->Qb_check_expenseline_model('e');
		$cashout->setAccountListid($this->config->item('mlm_cashout_list_id'),true);
		$cashout->set_select('SUM(e.Amount) as total_cash_out');
		$this->template_data->set('total_cash_out', $cashout->get());

		$this->load->view('mlm/dashboard/dashboard', $this->template_data->get_data());
	}

}
