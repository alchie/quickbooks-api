<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_receipts extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'sales_receipts');
	}

	public function index($customer_id=0, $start=0)
	{
		$this->load->model('Qb_salesreceipt_model');

		$sales_receipts = new $this->Qb_salesreceipt_model('c');
		$sales_receipts->set_order('c.TxnDate', 'DESC');
		$sales_receipts->set_order('c.RefNumber', 'DESC');
		$sales_receipts->set_start($start);
		
		if( $customer_id ) {
			$sales_receipts->set_where("c.Customer_ListID='{$customer_id}'");
		}

		$this->template_data->set('sales_receipts', $sales_receipts->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/sales_receipts/index/{$customer_id}"),
			'total_rows' => $sales_receipts->count_all_results(),
			'per_page' => $sales_receipts->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('sales_receipts/sales_receipts',  $this->template_data->get_data());
	}

	public function items($txn_id, $start=0)
	{
		$this->load->model('Qb_salesreceipt_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');

		$sales_receipts = new $this->Qb_salesreceipt_model('c');
		$sales_receipts->setTxnid($txn_id,true);
		$this->template_data->set('sales_receipt', $sales_receipts->get());

		$items = new $this->Qb_salesreceipt_salesreceiptline_model();
		$items->setSalesreceiptTxnid($txn_id,true);
		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/sales_receipts/items/{$txn_id}"),
			'total_rows' => $sales_receipts->count_all_results(),
			'per_page' => $sales_receipts->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('sales_receipts/items',  $this->template_data->get_data());
	}

}
