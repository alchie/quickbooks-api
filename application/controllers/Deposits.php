<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deposits extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'deposits');
	}

    private function _next($qbxml_id) {
    	$this->load->model('Qb_deposit_model');
        $next = new $this->Qb_deposit_model('n');

            $where = new $this->Qb_deposit_model('w');
            $where->set_select('MIN(w.qbxml_id)');
            $where->set_where("w.qbxml_id > " . $qbxml_id);
            $where->set_limit(1);

        $next->set_limit(1);
        $next->set_where('n.qbxml_id = ('. $where->get_compiled_select() . ')');
        return $next->get();
    }

    private function _previous($qbxml_id) {
    	$this->load->model('Qb_deposit_model');
        $previous = new $this->Qb_deposit_model('p');

            $where = new $this->Qb_deposit_model('w');
            $where->set_select('MAX(w.qbxml_id)');
            $where->set_where("w.qbxml_id < " . $qbxml_id);
            $where->set_limit(1);

        $previous->set_limit(1);
        $previous->set_where('p.qbxml_id = ('. $where->get_compiled_select() . ')');
        return $previous->get();
    }

	public function index($payee_id=0, $start=0)
	{
		$this->load->model('Qb_deposit_model');

		$deposits = new $this->Qb_deposit_model('c');
		$deposits->set_order('c.TxnDate', 'DESC');
		$deposits->set_order('c.TxnNumber', 'DESC');
		$deposits->set_start($start);
		
		if( $payee_id ) {
			$deposits->set_where("c.PayeeEntityRef_ListID='{$payee_id}'");
		}

		$this->template_data->set('deposits', $deposits->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/deposits/index/{$payee_id}"),
			'total_rows' => $deposits->count_all_results(),
			'per_page' => $deposits->get_limit(),
		)));

		$this->template_data->set('payee_id', $payee_id);
		$this->load->view('deposits/deposits',  $this->template_data->get_data());
	}

	public function details($TxnID)
	{
		$this->load->model('Qb_deposit_model');
		$deposit = new $this->Qb_deposit_model('c');
		$deposit->setTxnid($TxnID,true);
		$deposit_data = $deposit->get();
		$this->template_data->set('deposit', $deposit_data);

		$this->load->model('Qb_deposit_depositline_model');
		$receipts = new $this->Qb_deposit_depositline_model('dd');
		$receipts->setDepositTxnid($deposit_data->TxnID,true);
		//$receipts->set_order('dd.Amount', 'DESC');
		$receipts->set_limit(0);

		$receipts->set_join('qb_salesreceipt s','s.TxnID=dd.TxnID AND dd.TxnType="SalesReceipt"');
		//$receipts->set_join('qb_journalentry j','j.TxnID=dd.TxnID AND dd.TxnType="JournalEntry"');
		$receipts->set_select('s.*');
		$receipts->set_select('s.RefNumber as sRefNumber');
		$receipts->set_order('s.RefNumber', 'ASC');

		$receipts->set_select('dd.*');
		$receipts->set_select('dd.Entity_FullName as ddEntity_FullName');
		$receipts->set_select('dd.Amount as ddAmount');
		$receipts->set_select('dd.Memo as ddMemo');
		$receipts_data = $receipts->populate();

		$this->template_data->set('receipts', $receipts_data);

		$this->template_data->set('next_deposit', $this->_next($deposit_data->qbxml_id));
		$this->template_data->set('previous_deposit', $this->_previous($deposit_data->qbxml_id));

		$this->load->view('deposits/details',  $this->template_data->get_data());
	}
}
