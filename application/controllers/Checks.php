<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checks extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'checks');
	}

    private function _next($qbxml_id) {
    	$this->load->model('Qb_check_model');
        $next = new $this->Qb_check_model('n');

            $where = new $this->Qb_check_model('w');
            $where->set_select('MIN(w.qbxml_id)');
            $where->set_where("w.qbxml_id > " . $qbxml_id);
            $where->set_limit(1);

        $next->set_limit(1);
        $next->set_where('n.qbxml_id = ('. $where->get_compiled_select() . ')');
        return $next->get();
    }

    private function _previous($qbxml_id) {
    	$this->load->model('Qb_check_model');
        $previous = new $this->Qb_check_model('p');

            $where = new $this->Qb_check_model('w');
            $where->set_select('MAX(w.qbxml_id)');
            $where->set_where("w.qbxml_id < " . $qbxml_id);
            $where->set_limit(1);

        $previous->set_limit(1);
        $previous->set_where('p.qbxml_id = ('. $where->get_compiled_select() . ')');
        return $previous->get();
    }

	public function index($payee_id=0, $start=0)
	{
		$this->load->model('Qb_check_model');

		$checks = new $this->Qb_check_model('c');
		$checks->set_order('c.TxnDate', 'DESC');
		$checks->set_order('c.RefNumber', 'DESC');
		$checks->set_start($start);
		
		if( $payee_id ) {
			$checks->set_where("c.PayeeEntity_ListID='{$payee_id}'");
		}

		$this->template_data->set('checks', $checks->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/checks/index/{$payee_id}"),
			'total_rows' => $checks->count_all_results(),
			'per_page' => $checks->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('checks/checks',  $this->template_data->get_data());
	}

	public function voucher($txn_number)
	{
		$this->load->model('Qb_check_model');
		$check = new $this->Qb_check_model('c');
		$check->setTxnnumber($txn_number,true);
		$check_data = $check->get();
		$this->template_data->set('check', $check_data);

		$this->load->model('Qb_check_expenseline_model');
		$expenses = new $this->Qb_check_expenseline_model('e');
		$expenses->set_select('e.*');
		$expenses->setCheckTxnid($check_data->TxnID,true);
		$expenses->set_order('e.Customer_FullName', 'ASC');
		$expenses->set_order('e.Amount', 'DESC');
		$expenses->set_limit(0);
		$expenses->set_join('qb_account a', 'e.Account_ListID=a.ListID');
		$expenses->set_select('a.Name as aName');
		$expenses->set_select('a.AccountNumber as aNumber');
		$this->template_data->set('expenses', $expenses->populate());

		$this->template_data->set('next_check', $this->_next($check_data->qbxml_id));
		$this->template_data->set('previous_check', $this->_previous($check_data->qbxml_id));

		$this->load->view('checks/voucher',  $this->template_data->get_data());
	}

	public function print_check($txn_number)
	{
		$this->load->model('Qb_check_model');
		$check = new $this->Qb_check_model('c');
		$check->setTxnnumber($txn_number,true);
		$check_data = $check->get();
		$this->template_data->set('check', $check_data);

		$this->template_data->set('next_check', $this->_next($check_data->qbxml_id));
		$this->template_data->set('previous_check', $this->_previous($check_data->qbxml_id));
		
		$this->load->view('checks/print_check',  $this->template_data->get_data());
	}
}
