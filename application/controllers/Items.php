<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Items extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->template_data->set('current_uri', 'items');
	}

	public function index($start=0)
	{
		$limit = 10;
		$sql = "SELECT * FROM (";
		$sql .= "SELECT a.ListID, a.Name, 'discount' as type FROM qb_itemdiscount a ";
		$sql .= " UNION ";
		$sql .= "SELECT b.ListID, b.Name, 'fixedasset' as type FROM qb_itemfixedasset b ";
		$sql .= " UNION ";
		$sql .= "SELECT c.ListID, c.Name, 'group' as type FROM qb_itemgroup c";
		$sql .= " UNION ";
		$sql .= "SELECT d.ListID, d.Name, 'inventory' as type FROM qb_iteminventory d";
		$sql .= " UNION ";
		$sql .= "SELECT e.ListID, e.Name, 'inventoryassembly' as type FROM qb_iteminventoryassembly e";
		$sql .= " UNION ";
		$sql .= "SELECT f.ListID, f.Name, 'noninventory' as type FROM qb_itemnoninventory f";
		$sql .= " UNION ";
		$sql .= "SELECT g.ListID, g.Name, 'othercharge' as type FROM qb_itemothercharge g";
		$sql .= " UNION ";
		$sql .= "SELECT h.ListID, h.Name, 'payment' as type FROM qb_itempayment h";
		$sql .= ") z";
		if( $this->input->get('q') ) {
			$sql .= ' WHERE (Name LIKE "%'.$this->input->get('q').'%") ';
		}
		$query1 = $this->db->query($sql);

		$sql .= " ORDER BY Name ASC";
		$sql .= " LIMIT {$start}, 10";
		$sql .= ";";
		
		$query2 = $this->db->query($sql);
		$results = array();
		foreach($query2->result() as $row) {
			$results[] = $row;
		}
		$this->template_data->set('items', $results);
		$this->template_data->set('count', $query1->num_rows());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/index"),
			'total_rows' => $query1->num_rows(),
			'per_page' => $limit,
		)));

		$this->load->view('items/items', $this->template_data->get_data());
	}

	public function view($type, $list_id) {
		$this->template_data->set('current_uri', $type);

		switch($type) {
			case 'noninventory':
				$this->load->model('Qb_itemnoninventory_model');
				$results = new $this->Qb_itemnoninventory_model();
				$results->setListId($list_id,true);
				$this->template_data->set('current_item', $results->get());

				$this->template_data->set('breadcrumbs_uri', 'noninventories');
				$this->template_data->set('breadcrumbs_title', 'Items : Non-inventory');

			break;
			case 'discount':
				$this->load->model('Qb_itemdiscount_model');
				$results = new $this->Qb_itemdiscount_model();
				$results->setListId($list_id,true);
				$this->template_data->set('current_item', $results->get());

				$this->template_data->set('breadcrumbs_uri', 'discounts');
				$this->template_data->set('breadcrumbs_title', 'Items : Discount');

			break;
			case 'othercharge':
				$this->load->model('Qb_itemothercharge_model');
				$results = new $this->Qb_itemothercharge_model();
				$results->setListId($list_id,true);
				$this->template_data->set('current_item', $results->get());

				$this->template_data->set('breadcrumbs_uri', 'othercharges');
				$this->template_data->set('breadcrumbs_title', 'Items : Other Charge');
			break;
			case 'payment':
				$this->load->model('Qb_itempayment_model');
				$results = new $this->Qb_itempayment_model();
				$results->setListId($list_id,true);
				$this->template_data->set('current_item', $results->get());

				$this->template_data->set('breadcrumbs_uri', 'payments');
				$this->template_data->set('breadcrumbs_title', 'Items : Payment');
			break;
		}

		$this->load->view('items/view', $this->template_data->get_data());
	}

	public function discounts($start=0) {

		$this->template_data->set('current_uri', 'discount');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/discounts"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/discounts', $this->template_data->get_data());
	}

	public function fixedassets($start=0) {

		$this->template_data->set('current_uri', 'fixedasset');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/fixedassets"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/fixedassets', $this->template_data->get_data());
	}

	public function groups($start=0) {

		$this->template_data->set('current_uri', 'group');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/groups"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/groups', $this->template_data->get_data());
	}

	public function inventories($start=0) {

		$this->template_data->set('current_uri', 'inventory');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/inventories"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/inventories', $this->template_data->get_data());
	}

	public function inventoryassemblies($start=0) {

		$this->template_data->set('current_uri', 'inventoryassembly');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/inventoryassemblies"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/inventoryassemblies', $this->template_data->get_data());
	}

	public function noninventories($start=0) {

		$this->template_data->set('current_uri', 'noninventory');

		$this->load->model('Qb_itemdiscount_model');
		$results = new $this->Qb_itemdiscount_model();
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/noninventories"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/noninventories', $this->template_data->get_data());
	}

	public function othercharges($start=0) {

		$this->template_data->set('current_uri', 'othercharge');

		$this->load->model('Qb_itemothercharge_model');
		$results = new $this->Qb_itemothercharge_model();
		$results->set_select('*');
		$results->set_select('"othercharge" as type');
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/othercharges"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/othercharges', $this->template_data->get_data());
	}

	public function payments($start=0) {

		$this->template_data->set('current_uri', 'payment');

		$this->load->model('Qb_itempayment_model');
		$results = new $this->Qb_itempayment_model();
		$results->set_select('*');
		$results->set_select('"payment" as type');
		$results->set_start($start);
		$this->template_data->set('items', $results->populate());
		$this->template_data->set('count', $results->count_all());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/items/payments"),
			'total_rows' => $results->count_all(),
			'per_page' => $results->get_limit(),
		)));

		$this->load->view('items/payments', $this->template_data->get_data());
	}

}
