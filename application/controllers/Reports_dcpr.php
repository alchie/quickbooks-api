<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_dcpr extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'reports_dcpr');
	}
	
	public function index($m=false,$y=false) {
		
		$this->load->library('Dcpr_calendar');

		if($m) {
			$this->dcpr_calendar->setMonth($m);
		}

		if($y) {
			$this->dcpr_calendar->setYear($y);
		}

		$this->template_data->set('reports', $this->dcpr_calendar->init());

		$this->template_data->set('years', $this->dcpr_calendar->getMaxMinYear());

		$this->load->view('reports/dcpr/dcpr_index', $this->template_data->get_data());
	}



	public function view($m,$d,$y) {

		$current_date = date("Y-m-d", strtotime( $m."/".$d."/".$y ) );
		$this->template_data->set('current_date', $current_date);

		$this->load->view('reports/dcpr/dcpr_view', $this->template_data->get_data());
	}

}
