<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function index()
	{
		redirect("account/login");
	}
	
	public function login() {

		if( $this->input->post('username') && $this->input->post('password') ) {
			$this->load->model('Qb_dataext_model');
			$username = new $this->Qb_dataext_model('u');
			$username->setEntitytype('Employee',true);
			$username->setDataextname('qbsupport_username',true);
			$username->setDataextvalue($this->input->post('username'),true);
			$username->set_select('COUNT(*) as username');

			$password = new $this->Qb_dataext_model('p');
			$password->setEntitytype('Employee',true);
			$password->setDataextname('qbsupport_password',true);
			$password->setDataextvalue($this->input->post('password'),true);
			$password->set_select('COUNT(*)');
			$password->set_where('p.Entity_ListID=u.Entity_ListID');
			$username->set_select('('.$password->get_compiled_select().') as password');

			$username->set_join('qb_employee e', 'e.ListID=u.Entity_ListID');
			$username->set_select("e.*");
			$account_data = $username->get();
			
			if( $account_data->username && $account_data->password ) {
				$this->session->set_userdata('account', $account_data);
				redirect("dashboard");
			}
			
		}
		$this->load->view('dashboard/login', $this->template_data->get_data());
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("account/login");
	}

}
