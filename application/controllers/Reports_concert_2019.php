<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_concert_2019 extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'reports');

		$this->load->model('Qb_invoice_model');
	}
	
	public function index($start=0)
	{

		$solis = new $this->Qb_invoice_model('s');
		$solis->set_order('RefNumber', 'DESC');
		$solis->set_start($start);
		$solis->setTemplateListid('80000020-1565091777',true);
		$solis->set_join('qb_customer c', 'c.ListID=s.Customer_ListID');
		$solis->set_select("s.*");

		if( $this->input->get('q') ) {
			$solis->set_where('c.FullName LIKE "%'.$this->input->get('q').'%"');
			$solis->set_where_or('c.LastName LIKE "%'.$this->input->get('q').'%"');
			$solis->set_where_or('c.FirstName LIKE "%'.$this->input->get('q').'%"');
			$solis->set_where_or('c.MiddleName LIKE "%'.$this->input->get('q').'%"');
			$solis->set_where_or('c.Name LIKE "%'.$this->input->get('q').'%"');
		}

		$this->template_data->set('solis', $solis->populate());
		$this->template_data->set('solis_count', $solis->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_concert_2019/index"),
			'total_rows' => $solis->count_all_results(),
			'per_page' => $solis->get_limit(),
		)));

		$this->load->view('reports/reports_concert_2019/reports_concert_2019', $this->template_data->get_data());
	}

	public function print_soli($TxnID)
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('RefNumber', 'ASC');
		$recepient->setTxnid($TxnID,true);
		$recepient->set_select("i.RefNumber");

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");

		$this->template_data->set('recepient', $recepient->get());
		
		$this->load->view('reports/reports_concert_2019/print_soli', $this->template_data->get_data());
	}
	
	public function print_soli_all()
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('i.RefNumber', 'ASC');
		$recepient->set_select("i.RefNumber");
		$recepient->set_select("i.TxnID");
		$recepient->set_limit(0);

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");
		$recepient->setTemplateListid('80000020-1565091777',true);

		$this->template_data->set('recepients', $recepient->populate());
		
		$this->load->view('reports/reports_concert_2019/print_soli_all', $this->template_data->get_data());
	}

	public function print_envelope($TxnID)
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('RefNumber', 'ASC');
		$recepient->setTxnid($TxnID,true);
		$recepient->set_select("i.RefNumber");

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");

		$this->template_data->set('recepient', $recepient->get());
		
		$this->load->view('reports/reports_concert_2019/print_envelope', $this->template_data->get_data());
	}

	public function print_envelope_all()
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('i.RefNumber', 'ASC');
		$recepient->set_select("i.RefNumber");
		$recepient->set_select("i.TxnID");
		$recepient->set_limit(0);

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");
		$recepient->setTemplateListid('80000020-1565091777',true);

		$this->template_data->set('recepients', $recepient->populate());
		
		$this->load->view('reports/reports_concert_2019/print_envelope_all', $this->template_data->get_data());
	}

	public function print_received($TxnID)
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('RefNumber', 'ASC');
		$recepient->setTxnid($TxnID,true);
		$recepient->set_select("i.RefNumber");

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");

		$this->template_data->set('recepient', $recepient->get());
		
		$this->load->view('reports/reports_concert_2019/print_received', $this->template_data->get_data());
	}

	public function print_received_all()
	{

		$recepient = new $this->Qb_invoice_model('i');
		$recepient->set_order('i.RefNumber', 'ASC');
		$recepient->set_select("i.RefNumber");
		$recepient->set_select("i.TxnID");
		$recepient->set_limit(0);

		$recepient->set_join('qb_customer c', 'c.ListID=i.Customer_ListID');
		$recepient->set_select("c.*");
		$recepient->setTemplateListid('80000020-1565091777',true);

		$this->template_data->set('recepients', $recepient->populate());
		
		$this->load->view('reports/reports_concert_2019/print_received_all', $this->template_data->get_data());
	}

}
