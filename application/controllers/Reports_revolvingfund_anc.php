<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_revolvingfund_anc extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'reports');

		$this->load->model('reports/Revolving_fund_model');
		$this->load->model('reports/Revolving_fund_liquidations_model');
		
	}
	
	public function create_new() {
		$revolving_fund = new $this->Revolving_fund_model('rf', 'reports');
		$revolving_fund->setRequestDate(date("Y-m-d"));
		$revolving_fund->insert();
		redirect("reports_revolvingfund_anc/view_report/" . $revolving_fund->get_inserted_id());
	}

	public function index($start=0)
	{

		
		$revolving_fund = new $this->Revolving_fund_model('rf', 'reports');
		$revolving_fund->set_order('id', 'DESC');
		$revolving_fund->set_start($start);
		$this->template_data->set('revolving_funds', $revolving_fund->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_revolvingfund_anc/index"),
			'total_rows' => $revolving_fund->count_all_results(),
			'per_page' => $revolving_fund->get_limit(),
		)));

		$this->load->view('reports/reports_revolvingfund_anc/reports_revolvingfund_anc', $this->template_data->get_data());
	}

	public function view_report($id)
	{

		$revolving_fund = new $this->Revolving_fund_model('rf', 'reports');
		$revolving_fund->setId($id,true);
		$this->template_data->set('revolving_fund', $revolving_fund->get());

		$liquidations = new $this->Revolving_fund_liquidations_model('rfl', 'reports');
		$liquidations->setReportId($id,true);
		$liquidations->set_join('qb_journalentry qbj', 'qbj.TxnID=rfl.journal_id', NULL, $this->db->database);
		$liquidations->set_order('qbj.TxnDate', 'ASC');
		$liquidations->set_order('qbj.RefNumber', 'ASC');
		$liquidations->set_limit(0);
		$liquidations->set_select("*");
		$liquidations->set_select("rfl.id as rfl_id");
		$liquidations->set_select("(SELECT SUM(cl.Amount) FROM {$this->db->database}.qb_journalentry_journalcreditline cl WHERE cl.JournalEntry_TxnID=qbj.TxnID) as TotalAmount");
		$liquidations->set_select("(SELECT cl.Entity_FullName FROM {$this->db->database}.qb_journalentry_journalcreditline cl WHERE cl.JournalEntry_TxnID=qbj.TxnID LIMIT 1) as Entity_FullName");
		$liquidations->set_select("(SELECT cl.Memo FROM {$this->db->database}.qb_journalentry_journalcreditline cl WHERE cl.JournalEntry_TxnID=qbj.TxnID LIMIT 1) as Memo");
		$this->template_data->set('liquidations', $liquidations->populate());

		$this->load->view('reports/reports_revolvingfund_anc/view_report', $this->template_data->get_data());
	}

	public function edit_field($id, $field) {
		$this->template_data->set('field', $field);

		$revolving_fund = new $this->Revolving_fund_model('rf', 'reports');
		$revolving_fund->setId($id,true);
		if( $this->input->post('field_value') ) {
			switch ($field) {
				case 'id':
					$field_value = $this->input->post('field_value');
					$revolving_fund->setId($field_value,false,true);
					$id = $field_value;
					break;
				case 'request_date':
					$revolving_fund->setRequestDate($this->input->post('field_value'),false,true);
					break;
				case 'revolving_fund':
					$revolving_fund->setRevolvingFund($this->input->post('field_value'),false,true);
					break;
			}
			$revolving_fund->update();
			redirect("reports_revolvingfund_anc/view_report/{$id}");
		}
		$this->template_data->set('revolving_fund', $revolving_fund->get());

		$this->load->view('reports/reports_revolvingfund_anc/edit_field', $this->template_data->get_data());
	}

	public function remove_liquidation($id) {
		$liquidations = new $this->Revolving_fund_liquidations_model(NULL, 'reports');
		$liquidations->setId($id,true);
		$liquidations->delete();
		redirect( $this->input->get('back') );
	}

	public function add_liquidations($id,$start=0) {

		if( $this->input->post('journal') ) {
			foreach($this->input->post('journal') as $jvId) {
				$liquidations = new $this->Revolving_fund_liquidations_model(NULL, 'reports');
				$liquidations->setReportId($id,true);
				$liquidations->setJournalId($jvId,true);
				if( $liquidations->nonEmpty() ) {
					$liquidations->update();
				} else {
					$liquidations->insert();
				}
			}
			redirect( $this->input->get('back') );
		}

		$this->load->model('Qb_journalentry_model');
		$journals = new $this->Qb_journalentry_model('s');
		$journals->set_select("s.*");
		$db = $this->load->database( 'reports',TRUE,TRUE);
		$journals->set_select("(SELECT COUNT(*) FROM {$db->database}.revolving_fund_liquidations rfl WHERE rfl.journal_id=s.TxnID) as sCount");
		$journals->set_select("(SELECT SUM(cl.Amount) FROM qb_journalentry_journalcreditline cl WHERE cl.JournalEntry_TxnID=s.TxnID) as TotalAmount");
		$journals->set_order('TxnDate', 'DESC');
		$journals->set_order('TxnNumber', 'DESC');
		$journals->set_limit(50);
		$journals->set_start($start);
		$this->template_data->set('journals', $journals->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_revolvingfund_anc/add_collections/{$id}"),
			'total_rows' => $journals->count_all_results(),
			'per_page' => $journals->get_limit(),
		), '?back=' . $this->input->get('back') ));

		$this->load->view('reports/reports_revolvingfund_anc/add_liquidations', $this->template_data->get_data());
	}

	public function remove_expense($id) {
		$rExpenses = new $this->Cash_report_expenses_model(NULL, 'reports');
		$rExpenses->setId($id,true);
		$rExpenses->delete();
		redirect( $this->input->get('back') );
	}

	public function add_expenses($id,$start=0) {
		if( $this->input->post('check') ) {
			foreach($this->input->post('check') as $cId) {
				$rExpenses = new $this->Cash_report_expenses_model(NULL, 'reports');
				$rExpenses->setReportId($id,true);
				$rExpenses->setCheckId($cId,true);
				$rExpenses->setType('check');
				if( $rExpenses->nonEmpty() ) {
					$rExpenses->update();
				} else {
					$rExpenses->insert();
				}
			}
			redirect( $this->input->get('back') );
		}

		$this->load->model('Qb_check_model');
		$checks = new $this->Qb_check_model('c');
		$checks->set_select("c.*");
		$db = $this->load->database( 'reports',TRUE,TRUE);
		$checks->set_select("(SELECT COUNT(*) FROM {$db->database}.cash_report_expenses cre WHERE cre.check_id=c.TxnID) as sCount");
		$checks->set_order('TxnDate', 'DESC');
		$checks->set_order('TxnNumber', 'DESC');
		$checks->set_limit(50);
		$checks->set_start($start);
		$this->template_data->set('checks', $checks->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 4,
			'base_url' => base_url( $this->config->item('index_page') . "/reports_revolvingfund_anc/add_expenses/{$id}"),
			'total_rows' => $checks->count_all_results(),
			'per_page' => $checks->get_limit(),
		), '?back=' . $this->input->get('back') ));

		$this->load->view('reports/reports_revolvingfund_anc/add_expenses', $this->template_data->get_data());
	}
}
