<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mlm_products extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'products');
		$this->load->config('mlm');

		$this->load->model('Qb_dataext_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');
		$this->load->model('Qb_itemothercharge_model');
	}

	public function index($start=0)
	{
		
		$items = new $this->Qb_itemothercharge_model('i');
		$items->set_where_in('i.ListID', $this->config->item('mlm_products_items'));
		$this->template_data->set('items', $items->populate());

		$this->load->view('mlm/products/products', $this->template_data->get_data());
	}

	public function view($ListID, $start=0)
	{

		$item = new $this->Qb_itemothercharge_model('i');
		$item->setListid($ListID,true);
		$this->template_data->set('current_item', $item->get());

		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($ListID,true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_select('*');
		$packages->set_where('s.Template_ListID="'.$this->config->item('mlm_products_template_id').'"');

		if( $this->input->get('team') ) {
			$packages->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_team_key').'") = "'.$this->input->get('team').'")');
		}

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_team_key').'") as team');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_parent_key').'") as parent_id');

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_item_side_key').'") as side');


		$downlines = new $this->Qb_dataext_model('d5');
		$downlines->setTxntype("SalesReceiptLine", true);
		$downlines->setDataextname($this->config->item('mlm_item_parent_key'),true);
		$downlines->set_where('d5.DataExtValue=ss.TxnLineID');
		$downlines->set_select('COUNT(*)');
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss5', 'ss5.TxnLineID=d5.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s5', 's5.TxnID=ss5.SalesReceipt_TxnID');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss15');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s15', 's15.TxnID=ss15.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s15.Customer_ListID=s5.Customer_ListID)');
		$membership_fee->set_where('s15.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$packages->set_select('('.$downlines->get_compiled_select().') as downlines');

		$packages->set_start($start);
		if( $this->input->get('q') ) {
			$packages->set_where('(ss.TxnLineID LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Item_FullName LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Amount LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('s.RefNumber LIKE "%'.$this->input->get('q').'%")');
		}
		//echo $packages->get_compiled_select();

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$packages->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$this->template_data->set('mlm_packages', $packages->populate());
		$this->template_data->set('count', $packages->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_products/view/{$ListID}"),
			'total_rows' => $packages->count_all_results(),
			'per_page' => $packages->get_limit(),
			'ajax'=>true
		)));
		$this->load->view('mlm/products/products_view', $this->template_data->get_data());
	}	

	public function downlines($package_id, $start=0)
	{
		
		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_select('*');
		
		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_team_key').'") as team');

		if( $this->input->get('team') ) {
			$packages->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_team_key').'") = "'.$this->input->get('team').'")');
		}

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');

		if( $this->input->get('side') ) {
			$packages->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_side_key').'") = "'.$this->input->get('side').'")');
		}

		$packages->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as parent_id');

		$packages->set_where('((SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") = "'.$package_id.'")');

		$packages->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as downlines');

		$packages->set_start($start);
		if( $this->input->get('q') ) {
			$packages->set_where('(ss.TxnLineID LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Item_FullName LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('ss.Amount LIKE "%'.$this->input->get('q').'%"');
			$packages->set_where_or('s.RefNumber LIKE "%'.$this->input->get('q').'%")');
		}
		//echo $packages->get_compiled_select();

		$this->template_data->set('mlm_packages', $packages->populate());
		$this->template_data->set('count', $packages->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/mlm_packages/index"),
			'total_rows' => $packages->count_all_results(),
			'per_page' => $packages->get_limit(),
			'ajax'=>true
		)));
		$this->load->view('mlm/products/products_downlines', $this->template_data->get_data());
	}

	private function _load_customer($customer_id) {
		$customer = $this->_get_customer($customer_id);
		$this->template_data->set('customer', $customer);
		return $customer;
	}

	private function _get_customer($customer_id) {

		$this->load->model('Qb_customer_model');
		$mlm_members = new $this->Qb_customer_model('c');
		$mlm_members->setListid($customer_id,true);
		$mlm_members->set_select("c.*");
		
		$mlm_members->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.Entity_ListID<>c.ListID AND d.DataExtValue=c.ListID AND d.DataExtName="'.$this->config->item('mlm_referral_key').'") as referrals');

		$mlm_members->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE  d.Entity_ListID=c.ListID AND d.DataExtName="'.$this->config->item('mlm_team_key').'") as team');

		$mlm_members->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.DataExtValue=c.ListID AND d.DataExtName="'.$this->config->item('mlm_total_network_key').'") as total_network');
		
		
		$packages = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$packages->setItemListid($this->config->item('mlm_package1_item_id'),true);
		$packages->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$packages->set_where('s.Customer_ListID=c.ListID');
		$packages->set_select('COUNT(*)');
		$mlm_members->set_select('('.$packages->get_compiled_select().') as packages');

		return $mlm_members->get();
	}
	
	private function _get_current_package($txn_id) {
		
		$current_package = new $this->Qb_salesreceipt_salesreceiptline_model('ss');
		$current_package->setTxnlineid($txn_id,true);
		$current_package->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$current_package->set_select('ss.*');
		$current_package->set_select('s.Customer_ListID as Customer_ListID');
		$current_package->set_select('s.Customer_FullName as Customer_FullName');
		$current_package->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_team_key').'") as team');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as parent');

		$current_package->set_select('(SELECT d.DataExtValue FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.Txn_TxnID=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');

		$current_package->set_select('(SELECT COUNT(*) FROM qb_dataext d WHERE d.TxnType="SalesReceiptLine" AND d.DataExtValue=ss.TxnLineID AND d.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as downlines');

		return $current_package->get();
	}

	private function _get_side_package($TxnID, $side='LEFT') {

		$side_package = new $this->Qb_dataext_model('d');
		$side_package->setDataextname($this->config->item('mlm_package1_parent_key'),true);
		$side_package->setDataextvalue($TxnID,true);
		$side_package->setTxntype('SalesReceiptLine',true);
		$side_package->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$side_package->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$side_package->set_where('ss.Item_ListID = "'.$this->config->item('mlm_package1_item_id').'"');
		
		$side_package->set_select('COUNT(*) as total_side');
		$side_package->set_having('(total_side > 0)');

		$side_package->set_select('d.*');
		$side_package->set_select('ss.*');
		$side_package->set_select('s.*');

		$side_package->set_where('s.Template_ListID="'.$this->config->item('mlm_package1_template_id').'"');

		$side_package->set_select('(SELECT d1.DataExtValue FROM qb_dataext d1 WHERE d1.TxnType="SalesReceiptLine" AND d1.Txn_TxnID=ss.TxnLineID AND d1.DataExtName="'.$this->config->item('mlm_package1_team_key').'") as team');

		$side_package->set_select('(SELECT d2.DataExtValue FROM qb_dataext d2 WHERE d2.TxnType="SalesReceiptLine" AND d2.Txn_TxnID=ss.TxnLineID AND d2.DataExtName="'.$this->config->item('mlm_package1_parent_key').'") as parent');

		$side_package->set_select('(SELECT d3.DataExtValue FROM qb_dataext d3 WHERE d3.TxnType="SalesReceiptLine" AND d3.Txn_TxnID=ss.TxnLineID AND d3.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');

		$side_package->set_where('((SELECT d4.DataExtValue FROM qb_dataext d4 WHERE d4.TxnType="SalesReceiptLine" AND d4.Txn_TxnID=ss.TxnLineID AND d4.DataExtName="'.$this->config->item('mlm_package1_side_key').'") = "'.$side.'")');

		$downlines = new $this->Qb_dataext_model('d5');
		$downlines->setTxntype("SalesReceiptLine", true);
		$downlines->setDataextname($this->config->item('mlm_package1_parent_key'),true);
		$downlines->set_where('d5.DataExtValue=ss.TxnLineID');
		$downlines->set_select('COUNT(*)');
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss5', 'ss5.TxnLineID=d5.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s5', 's5.TxnID=ss5.SalesReceipt_TxnID');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss15');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s15', 's15.TxnID=ss15.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s15.Customer_ListID=s5.Customer_ListID)');
		$membership_fee->set_where('s15.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$side_package->set_select('('.$downlines->get_compiled_select().') as downlines');

		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$side_package->set_where('(('.$membership_fee->get_compiled_select().') > 0)');
		
		return $side_package->get();

	}

	public function genealogy($txn_id) {
		
		
		

		$current_package_data = $this->_get_current_package($txn_id);
		$this->template_data->set('current_package', $current_package_data);

		$sublevel_package_data = array();
		$sub_level_txn_id = $txn_id;
		$sub_level = 1;
		if( $this->input->get('sub') ) {
			$sublevels = explode(",", $this->input->get('sub'));
			foreach($sublevels as $sub) {
				if( $sub ) {
					$sub_level_txn_id = $sub;
					$sublevel_package_data[$sub_level] = $this->_get_current_package($sub);
					$sub_level++;
				}
			}
		}

		$this->template_data->set('sublevel_packages', $sublevel_package_data);

		$this->template_data->set('left_package', $this->_get_side_package($sub_level_txn_id, 'LEFT') );
		$this->template_data->set('right_package', $this->_get_side_package($sub_level_txn_id, 'RIGHT') );

		$this->_load_customer($current_package_data->Customer_ListID);
		$this->load->view('mlm/products/products_genealogy', $this->template_data->get_data());
	}

	public function recalculate_network($TxnLineID,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_package1_parent_key'),true);
		$downlines->setEntitytype('SalesReceiptLine',true);
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$downlines->set_select('d.*');

		//$downlines->set_select('COUNT(*) as downlines');
		//$downlines->setDataextvalue($TxnLineID,true);
		//print_r($downlines->populate()); exit;

		
		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$total = $downlines->recursive_count('DataExtValue', $TxnLineID, 'Txn_TxnID', 0, 'Txn_TxnID');
		//print_r($total); exit;

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_package1_network_key'),true);
		$total_network->setEntitytype('SalesReceiptLine',true);
		$total_network->setTxntype('SalesReceiptLine',true);
		$total_network->setEntityListid($TxnLineID,true);
		$total_network->setTxnTxnid($TxnLineID,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}

		$this->recalculate_left_side($TxnLineID,0);
		$this->recalculate_right_side($TxnLineID,0);
		if( $redirect ) {
			redirect("mlm_members/package/{$TxnLineID}");
		}
		
	}

	public function recalculate_left_side($TxnLineID,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_package1_parent_key'),true);
		$downlines->setEntitytype('SalesReceiptLine',true);
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$downlines->set_select('d.*');

		$downlines->set_select('(SELECT d1.DataExtValue FROM qb_dataext d1 WHERE d1.EntityType="SalesReceiptLine" AND d1.Txn_TxnID=d.Txn_TxnID AND d1.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');

		//$downlines->set_select('COUNT(*) as downlines');
		//$downlines->setDataextvalue($TxnLineID,true);
		//print_r($downlines->populate()); exit;	

		
		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$total = $downlines->recursive_filter_count('DataExtValue', $TxnLineID, 'Txn_TxnID', 0, 'Txn_TxnID', 'side', 'LEFT');

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_package1_left_network_key'),true);
		$total_network->setEntitytype('SalesReceiptLine',true);
		$total_network->setTxntype('SalesReceiptLine',true);
		$total_network->setEntityListid($TxnLineID,true);
		$total_network->setTxnTxnid($TxnLineID,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}
		if( $redirect ) {
			redirect("mlm_members/package/{$TxnLineID}");
		}
		
	}

	public function recalculate_right_side($TxnLineID,$redirect=1) {

		

		$downlines = new $this->Qb_dataext_model('d');
		$downlines->setDataextname($this->config->item('mlm_package1_parent_key'),true);
		$downlines->setEntitytype('SalesReceiptLine',true);
		$downlines->set_join('qb_salesreceipt_salesreceiptline ss', 'ss.TxnLineID=d.Txn_TxnID');
		$downlines->set_join('qb_salesreceipt s', 's.TxnID=ss.SalesReceipt_TxnID');
		$downlines->set_select('d.*');

		$downlines->set_select('(SELECT d1.DataExtValue FROM qb_dataext d1 WHERE d1.EntityType="SalesReceiptLine" AND d1.Txn_TxnID=d.Txn_TxnID AND d1.DataExtName="'.$this->config->item('mlm_package1_side_key').'") as side');

		//$downlines->set_select('COUNT(*) as downlines');
		//$downlines->setDataextvalue($TxnLineID,true);
		//print_r($downlines->populate()); exit;	

		
		$membership_fee = new $this->Qb_salesreceipt_salesreceiptline_model('ss1');
		$membership_fee->setItemListid($this->config->item('mlm_membership_item_id'),true);
		$membership_fee->set_join('qb_salesreceipt s1', 's1.TxnID=ss1.SalesReceipt_TxnID');
		$membership_fee->set_select('COUNT(*)');
		$membership_fee->set_where('(s1.Customer_ListID=s.Customer_ListID)');
		$membership_fee->set_where('s1.Template_ListID="'.$this->config->item('mlm_membership_template_id').'"');
		$downlines->set_where('(('.$membership_fee->get_compiled_select().') > 0)');

		$total = $downlines->recursive_filter_count('DataExtValue', $TxnLineID, 'Txn_TxnID', 0, 'Txn_TxnID', 'side', 'RIGHT');

		$total_network = new $this->Qb_dataext_model();
		$total_network->setDataextname($this->config->item('mlm_package1_right_network_key'),true);
		$total_network->setEntitytype('SalesReceiptLine',true);
		$total_network->setTxntype('SalesReceiptLine',true);
		$total_network->setEntityListid($TxnLineID,true);
		$total_network->setTxnTxnid($TxnLineID,true);
		$total_network->setDataextvalue($total);
		if( $total_network->nonEmpty() ) {
			$total_network->update();
		} else {
			$total_network->insert();
		}
		if( $redirect ) {
			redirect("mlm_members/package/{$TxnLineID}");
		}
		
	}

}
