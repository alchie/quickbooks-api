<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends MY_Controller {

	public function __construct() {
		parent::__construct();
		
		$this->template_data->set('current_uri', 'payments');
	}

	public function index($customer_id=0, $start=0)
	{
		$this->load->model('Qb_receivepayment_model');

		$payments = new $this->Qb_receivepayment_model('c');
		$payments->set_order('c.TxnDate', 'DESC');
		$payments->set_order('c.RefNumber', 'DESC');
		$payments->set_start($start);
		
		if( $customer_id ) {
			$payments->set_where("c.Customer_ListID='{$customer_id}'");
		}

		$this->template_data->set('payments', $payments->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/payments/index/{$customer_id}"),
			'total_rows' => $payments->count_all_results(),
			'per_page' => $payments->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('payments/payments',  $this->template_data->get_data());
	}

	public function items($txn_id, $start=0)
	{
		$this->load->model('Qb_salesreceipt_model');
		$this->load->model('Qb_salesreceipt_salesreceiptline_model');

		$payments = new $this->Qb_salesreceipt_model('c');
		$payments->setTxnid($txn_id,true);
		$this->template_data->set('sales_receipt', $payments->get());

		$items = new $this->Qb_salesreceipt_salesreceiptline_model();
		$items->setSalesreceiptTxnid($txn_id,true);
		$this->template_data->set('items', $items->populate());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			//'uri_segment' => 3,
			'base_url' => base_url( $this->config->item('index_page') . "/payments/items/{$txn_id}"),
			'total_rows' => $payments->count_all_results(),
			'per_page' => $payments->get_limit(),
			'ajax'=>true
		)));

		$this->load->view('payments/items',  $this->template_data->get_data());
	}

}
