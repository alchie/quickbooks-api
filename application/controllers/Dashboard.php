<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function index($m=false,$y=false)
	{

		$this->load->model('Qb_customer_model');
		$customers = new $this->Qb_customer_model('c');
		$customers->set_select('COUNT(*) as total');
		$this->template_data->set('customers_count', $customers->get());

		$this->load->model('Qb_vendor_model');
		$vendors = new $this->Qb_vendor_model('c');
		$vendors->set_select('COUNT(*) as total');
		$this->template_data->set('vendors_count', $vendors->get());

		$this->load->model('Qb_employee_model');
		$employees = new $this->Qb_employee_model('c');
		$employees->set_select('COUNT(*) as total');
		$this->template_data->set('employees_count', $employees->get());

		$this->load->library('Dcpr_calendar');
		if($m) {
			$this->dcpr_calendar->setMonth($m);
		}
		if($y) {
			$this->dcpr_calendar->setYear($y);
		}

		$this->template_data->set('reports', $this->dcpr_calendar->init());
		$this->template_data->set('years', $this->dcpr_calendar->getMaxMinYear());


		$this->load->view('dashboard/dashboard', $this->template_data->get_data());
	}

	public function view($m,$d,$y)
	{
		$this->template_data->set('current_month', $m);
		$this->template_data->set('current_day', $d);
		$this->template_data->set('current_year', $y);

		$this->load->library('Dcpr_calendar');
		$this->dcpr_calendar->setMonth($m);
		$this->dcpr_calendar->setYear($y);
		$this->dcpr_calendar->setDay($d);
		$report = $this->dcpr_calendar->getReport();

		foreach($report as $key=>$val) {
			switch($key) {
				case 'deposits':
					$this->load->model('Qb_deposit_model');
					$deposits = new $this->Qb_deposit_model();
					$deposits->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $deposits->populate();
				break;
				case 'sales':
					$this->load->model('Qb_salesreceipt_model');
					$sales = new $this->Qb_salesreceipt_model();
					$sales->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $sales->populate();
				break;
				case 'checks':
					$this->load->model('Qb_check_model');
					$checks = new $this->Qb_check_model();
					$checks->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $checks->populate();
				break;
				case 'invoices':
					$this->load->model('Qb_invoice_model');
					$invoices = new $this->Qb_invoice_model();
					$invoices->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $invoices->populate();
				break;
				case 'payments':
					$this->load->model('Qb_receivepayment_model');
					$payments = new $this->Qb_receivepayment_model();
					$payments->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $payments->populate();
				break;
				case 'journals':
					$this->load->model('Qb_journalentry_model');
					$journals = new $this->Qb_journalentry_model();
					$journals->setTxndate(date("Y-m-d", strtotime("{$m}/{$d}/{$y}")),true);
					$report[$key] = $journals->populate();
				break;
			}
		}
		$this->template_data->set('report', $report);

		$this->load->view('dashboard/view', $this->template_data->get_data());
	}
	
	public function login() {
		$this->load->view('dashboard/login', $this->template_data->get_data());
	}
}
