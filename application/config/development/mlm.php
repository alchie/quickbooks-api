<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['mlm_enable'] = TRUE;

// defined fields
$config['mlm_referral_key'] = 'mlm_referral_id'; 
$config['mlm_team_key'] = 'mlm_team'; 
$config['mlm_parent_key'] = 'mlm_parent'; 
$config['mlm_side_key'] = 'mlm_side';
$config['mlm_itemagent_key'] = 'mlm_item_parent';

$config['mlm_membership_template_id'] = '80000020-1554606959'; 
$config['mlm_membership_item_id'] = '80000023-1555847422'; // item list id - item type must be `other charge`

$config['mlm_signupbonus_item_id'] = '80000029-1559482911'; // item list id - item type must be `other charge`
$config['mlm_watchbonus_item_id'] = '8000002A-1559482928'; // item list id - item type must be `other charge`

$config['mlm_salesbonus_item_id'] = '80000025-1557923599'; // item list id - item type must be `other charge`
$config['mlm_mlmsalesbonus_item_id'] = '80000027-1557924278'; // item list id - item type must be `other charge`
$config['mlm_randombonus_item_id'] = '80000028-1557929406'; // item list id - item type must be `other charge`

$config['mlm_membership_network_key'] = 'mlm_membership_downlines';
$config['mlm_membership_left_network_key'] = 'mlm_membership_downlines_left';
$config['mlm_membership_right_network_key'] = 'mlm_membership_downlines_right';

// rates
$config['mlm_signup_bonus'] = '200.00';
$config['mlm_watch_bonus'] = '100.00';
$config['mlm_total_videos'] = '10';
$config['mlm_referral_bonus'] = '10.00';
$config['mlm_level_bonus'] = '1.00';
$config['mlm_pairing_bonus'] = '1.00';

$config['mlm_package1_template_id'] = '80000021-1556506555';
$config['mlm_package1_item_id'] = '80000022-1554606866'; // item list id - item type must be `other charge`
$config['mlm_package1_bonus'] = '200.00';
$config['mlm_package1_watch_bonus'] = '100.00';
$config['mlm_package1_total_videos'] = '8';
$config['mlm_package1_level_bonus'] = '1.00';
$config['mlm_package1_pairing_bonus'] = '1.00';
$config['mlm_package1_team_key'] = 'mlm_item_team';
$config['mlm_package1_parent_key'] = 'mlm_item_parent';
$config['mlm_package1_side_key'] = 'mlm_item_side';

$config['mlm_package1_network_key'] = 'mlm_package1_downlines';
$config['mlm_package1_left_network_key'] = 'mlm_package1_downlines_left';
$config['mlm_package1_right_network_key'] = 'mlm_package1_downlines_right';

// payout settings
$config['mlm_cashout_list_id'] = '80000089-1555076023'; // chart of accounts - expense type

// products and services
$config['mlm_item_team_key'] = 'mlm_item_team';
$config['mlm_item_parent_key'] = 'mlm_item_parent';
$config['mlm_item_side_key'] = 'mlm_item_side';

$config['mlm_products_template_id'] = '80000021-1556506555';
$config['mlm_products_items'] = array('80000022-1554606866');

$config['mlm_services_template_id'] = '80000021-1556506555';
$config['mlm_services_items'] = array('80000022-1554606866');